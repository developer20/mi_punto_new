<?php
require "../config/mainModel.php";

class funciones {
  public function __construct ()	{		
    $BD = new BD ();
    $this->BD = $BD;
    $this->BD->conectar ();
    $this->idpos = 0;
    $this->id_distri = 0;
    if(isset($_SESSION[$GLOBALS["SESION_POS"]])){
      $oSession = unserialize ($_SESSION[$GLOBALS["SESION_POS"]]);
      $this->idpos=$oSession->VSidpos;
      $this->id_distri=$oSession->VSid_distri;
    }
  }

  public function __destruct () {
    $this->BD->desconectar();
  }
  
  public function encodeArray ($array = array(), $sourceEncoding = "", $destinationEncoding = "UTF-8") {
    if ($sourceEncoding === $destinationEncoding) {
      return $array;
    }
    
    array_walk_recursive($array, function (&$array) use ($sourceEncoding, $destinationEncoding) {
      $array = mb_convert_encoding($array, $sourceEncoding, mb_detect_encoding($array, "auto"));
    });
    
    return $array;
  }
  
  //Espacio Para declara las funciones que retornan los datos de la DB.
  public function Retornar_Departamento_Ciudad () {    
    //Si $id esta vacio devuelve todos los perfiles.
    $Consulta_D = "SELECT id,nombre FROM {$GLOBALS["BD_NAME"]}.departamentos";
    $datos_D = $this->BD->devolver_array($Consulta_D);
    
    $Consulta_C = "SELECT id,departamento id_departamento, nombre FROM {$GLOBALS["BD_NAME"]}.municipios order by 3";
    $datos_C = $this->BD->devolver_array($Consulta_C);
    
    $datos = array(
      "departamento" => $datos_D,
      "ciudad" => $datos_C
    );
    
    return $datos;
  }


  public function geo_detalle_visita ($id_check, $repor_super) {
    $datos_ventas = array();
    $evidencia = "";

    if ($repor_super === "true") {
      $c_vis_act = "SELECT u.nombre_usuario,c.fecha AS fecha_check, c.hora AS hora_ini, c.hora_cierre AS hora_fin,SEC_TO_TIME(TIMESTAMPDIFF(SECOND , CONCAT(c.fecha,' ',c.hora), CONCAT(c.fecha,' ',c.hora_cierre) )) AS tiempo_visi,IF(vd.tipo_visita IS NOT NULL,mv.motivo,'N/A') AS motivo_visi,IF(vd.tipo_visita IS NOT NULL,vd.observa,'') AS obs_visi,'' AS evidencia,c.venta,c.punto,d.nombre AS nom_distri,c.lat,c.lng,c.lat_pdv,c.long_pdv,c.precisiongps AS margen_e,'' AS evidencia_eliminada,'' AS img_evidencia,p.razon as nombre_pdv,t.nombre as circuito,z.nombre as ruta,z.id as id_zona,c.id_distri,c.usuario,if(c.fecha_sincroniza is not null,c.fecha_sincroniza,'N/A') AS fecha_sincroniza,if(c.hora_sincroniza is not null,c.hora_sincroniza,'N/A') AS hora_sincroniza, c.posicion_simulada,
        'N/A' as tipo,(SELECT evidencia FROM {$GLOBALS["BD_NAME"]}.configuracion_general) evidencia_validar, IF(c.visitaEnPunto = 0, 'NO', 'SI') as visitaEnPunto, IF(c.qr = 0, 'NO', 'SI') as qr, IF(c.observacion_qr IS NULL, '', c.observacion_qr) as observacion_qr
        FROM {$GLOBALS["BD_POS"]}.visitas_super AS c
        LEFT JOIN {$GLOBALS["BD_NAME"]}.usuarios_distri AS u ON (u.id_usuario = c.usuario AND u.id_distri = c.id_distri)
        LEFT JOIN {$GLOBALS["BD_NAME"]}.distribuidores AS d ON (d.id = c.id_distri)
        LEFT JOIN {$GLOBALS["BD_POS"]}.visitas_detalle_super AS vd ON (vd.id_check = c.id AND vd.tipo_visita > 0)
        LEFT JOIN {$GLOBALS["BD_POS"]}.motivo_visita AS mv ON (mv.id = vd.tipo_visita)

        LEFT JOIN {$GLOBALS["BD_POS"]}.puntos AS p ON (c.punto = p.idpos)
        LEFT JOIN {$GLOBALS["BD_POS"]}.rutero_planificado AS rp  ON (rp.id_pos = p.idpos and rp.tipo!=-1 and rp.fecha=c.fecha and rp.id_vendedor=u.id_usuario)
        LEFT JOIN {$GLOBALS["BD_NAME"]}.territorios AS t ON (c.territorio = t.id)
        LEFT JOIN {$GLOBALS["BD_NAME"]}.zonas AS z ON (c.zona = z.id)
        WHERE c.id = $id_check AND c.reparto = 0";
    } else {
      $c_vis_act = "SELECT u.nombre_usuario,c.fecha AS fecha_check, c.hora AS hora_ini, c.hora_cierre AS hora_fin,SEC_TO_TIME(TIMESTAMPDIFF(SECOND , CONCAT(c.fecha,' ',c.hora), CONCAT(c.fecha,' ',c.hora_cierre) )) AS tiempo_visi,IF(vd.tipo_visita IS NOT NULL,mv.motivo,'N/A') AS motivo_visi,IF(vd.tipo_visita IS NOT NULL,vd.observa,'') AS obs_visi,c.evidencia,c.venta,c.punto,d.nombre AS nom_distri,c.lat,c.lng,c.lat_pdv,c.long_pdv,c.precisiongps AS margen_e,c.evidencia_eliminada,'' AS img_evidencia,p.razon as nombre_pdv,t.nombre as circuito,z.nombre as ruta,z.id as id_zona,c.id_distri,c.usuario,if(c.fecha_sincroniza is not null,c.fecha_sincroniza,'N/A') AS fecha_sincroniza,if(c.hora_sincroniza is not null,c.hora_sincroniza,'N/A') AS hora_sincroniza, c.posicion_simulada,
        if(c.duplicado=0 and c.extraruta=0,1,if(c.extraruta=0 and c.duplicado=1,3,if((c.extraruta=1 and c.duplicado=1) || (c.extraruta=1 and c.duplicado=0),2,'N/A'))) as tipo,(SELECT evidencia FROM {$GLOBALS["BD_NAME"]}.configuracion_general) evidencia_validar, IF(c.visitaEnPunto = 0, 'NO', 'SI') as visitaEnPunto, IF(c.qr = 0, 'NO', 'SI') as qr,  IF(c.observacion_qr IS NULL, '', c.observacion_qr) as observacion_qr
        FROM {$GLOBALS["BD_POS"]}.visitas__check AS c
        INNER JOIN {$GLOBALS["BD_NAME"]}.usuarios_distri AS u ON (u.id_usuario = c.usuario AND u.id_distri = c.id_distri)
        INNER JOIN {$GLOBALS["BD_NAME"]}.distribuidores AS d ON (d.id = c.id_distri)
        LEFT JOIN {$GLOBALS["BD_POS"]}.visitas_detalle AS vd ON (vd.id_check = c.id AND vd.tipo_visita > 0)
        LEFT JOIN {$GLOBALS["BD_POS"]}.motivo_visita AS mv ON (mv.id = vd.tipo_visita)

        LEFT JOIN {$GLOBALS["BD_POS"]}.puntos AS p ON (c.punto = p.idpos)
        /*LEFT JOIN {$GLOBALS["BD_POS"]}.rutero_planificado AS rp  ON (rp.id_pos = p.idpos and rp.tipo!=-1 and rp.fecha=c.fecha and rp.id_vendedor=u.id_usuario)*/
        LEFT JOIN {$GLOBALS["BD_NAME"]}.territorios AS t ON (c.territorio = t.id)
        LEFT JOIN {$GLOBALS["BD_NAME"]}.zonas AS z ON (c.zona = z.id)
        WHERE c.id = $id_check AND c.reparto = 0";
        
    }

    //echo $c_vis_act;exit;
    mysql_set_charset("utf8");
    $array_visita = $this->BD->devolver_array($c_vis_act);
                   

    if (count($array_visita) > 0) {
      if ($array_visita[0]["venta"] == 1) {
        $sql = "SELECT r.pn,r.producto,pro.cantidad,'Autoventa' AS tipo_compra,'N/A' AS pedido 
               FROM {$GLOBALS["BD_NAME"]}.referencias AS r 
               INNER JOIN (

               SELECT id_referencia,COUNT(id) AS cantidad 
               FROM {$GLOBALS["BD_NAME"]}.inventario__punto 
               WHERE visita = $id_check AND nro_pedido = 0 AND id_combo = 0 and tipo=1
               GROUP BY id_referencia UNION ALL SELECT id_referencia,COUNT(id) AS cantidad FROM {$GLOBALS["BD_NAME"]}.inventario__punto WHERE visita = $id_check AND nro_pedido = 0 and tipo=2 GROUP BY id_referencia

               ) AS pro ON (pro.id_referencia = r.id) GROUP BY r.id UNION ALL SELECT r.pn,r.producto,pi.cantidad, 'Toma Pedido' AS tipo_compra,pc.id AS pedido FROM {$GLOBALS["BD_NAME"]}.pedidos__carga AS pc INNER JOIN {$GLOBALS["BD_NAME"]}.pedidos__item AS pi ON (pi.id_pedido = pc.id) INNER JOIN {$GLOBALS["BD_NAME"]}.referencias AS r ON (r.id = pi.id_refe) WHERE pc.id_visita = $id_check AND pc.vendedor = {$array_visita[0]["usuario"]} AND pc.id_distri = {$array_visita[0]["id_distri"]} AND pc.id_pos = {$array_visita[0]["punto"]} UNION ALL SELECT 'N/A' AS pn,'N/A' AS producto,'0' AS cantidad,'Venta Saldo' AS tipo_compra,'N/A' AS pedido FROM {$GLOBALS["BD_NAME"]}.recargas__venta_saldo as rvs
              where rvs.id_visita=$id_check group by rvs.id";

              
        /*
         UNION ALL SELECT 'N/A' AS pn,'N/A' AS producto,'N/A' AS cantidad,'Venta Saldo' AS tipo_compra,'N/A' AS pedido,CONCAT('$',rvs.valor_acreditado)  as valor_acreditado FROM {$GLOBALS["BD_POS"]}.visitas_detalle as vd LEFT JOIN {$GLOBALS["BD_NAME"]}.recargas__venta_saldo as rvs ON (rvs.id_visita=vd.id_check) where vd.id_check=$id_check and vd.proceso=6
        */
        mysql_set_charset("utf8");
        $datos_ventas = $this->BD->devolver_array($sql);
      }

      if ($array_visita[0]["evidencia"] == 1) {
        $file_ori = $GLOBALS["RUTA_IMG_EVIDENCIA"].$id_check.".jpg";
        //$file_des = "img/".$id_check.".jpg";
        if (file_exists($file_ori)) {
          //copy($file_ori, $file_des);
          $array_visita[0]["img_evidencia"] = base64_encode(file_get_contents($file_ori));
        }
      }
    }

    /*foreach ($array_visita as $key => $value) {
      $array_visita[$key] = array_map ("utf8_encode", $array_visita[$key]);
    }*/

    foreach ($datos_ventas as $key => $value) {
      // $datos_ventas[$key] = array_map ("utf8_encode", $datos_ventas[$key]);
      $datos_ventas[$key]["cantidad"] = number_format($value["cantidad"]);
    }
     
    return array(
      "datos_visita" => $array_visita,
      "datos_ventas" => $datos_ventas
    );
  }


  public function obtener_proceso ($check) {
    $sql = "SELECT CASE proceso WHEN 0 THEN 'No venta' WHEN 1 THEN 'Toma Pedido' WHEN 2 THEN 'Entrega Pedido' WHEN 3 THEN 'Autoventa' WHEN 4 THEN 'Baja Manual' WHEN 5 THEN 'Supervisor' WHEN 6 THEN 'Venta Saldo'  WHEN 7 THEN 'Encuesta' ELSE 'N/A' END AS PROCESO, hora as hora_proceso from {$GLOBALS["BD_POS"]}.visitas_detalle where id_check=$check order by hora";
    $proceso = $this->BD->devolver_array($sql);

    return $proceso;
  }

  public function obtener_frecuencia ($zona) {
    $sql = "SELECT fre.lunes,fre.martes,fre.miercoles,fre.jueves,fre.viernes,fre.sabado,fre.domingo,'' as fecha FROM {$GLOBALS["BD_NAME"]}.frecuencia_zonas fre WHERE id_zona=$zona";
    $frecuencia = $this->BD->devolver_array($sql);
   
    
    if (($frecuencia == false) || ($frecuencia[0]['lunes'] == 0 && $frecuencia[0]['martes'] == 0 && $frecuencia[0]['miercoles'] == 0 && $frecuencia[0]['jueves'] == 0 && $frecuencia[0]['viernes'] == 0 && $frecuencia[0]['sabado'] == 0 && $frecuencia[0]['domingo'] == 0)) {
      $fecha_actual = date('Y-m-d');
      $sql1 = "SELECT pv.fecha,'' as lunes,'' as martes,'' as miercoles,'' as jueves,'' as viernes,'' as sabado,'' as domingo FROM {$GLOBALS["BD_NAME"]}.planificacion_visitas pv WHERE ruta=$zona AND pv.fecha >= '$fecha_actual'";
    
      $frecuencia = $this->BD->devolver_array($sql1);
    }
    if ($frecuencia == false) {
      $frecuencia[0] = array(
        'lunes' => '',
        'martes' => '',
        'miercoles' => '',
        'jueves' => '',
        'viernes' => '',
        'sabado' => '',
        'domingo' => '',
        'fecha' => 'No tiene frecuencia programadas'
      );
    }
      
    return $frecuencia;
  }


  public function datos_referencia($id){

    if(!isset($_SESSION)) { 
        session_start(); 
    } 
    if(isset($_SESSION["SESION_POS_FUNCIONES"])){
      $oSessionnueva = $_SESSION["SESION_POS_FUNCIONES"];
      $id_distri     = $oSessionnueva['VSid_distri'];
    }

    $sql_dis = "SELECT nombre_corto FROM {$GLOBALS["BD_NAME"]}.distribuidores WHERE id = $id_distri";
		$query_dis = $this->BD->devolver_array($sql_dis);
		$nombre_corto = $query_dis[0]["nombre_corto"];
    $bd = "{$GLOBALS["BD_DIS"]}" . $nombre_corto . "_" . $id_distri;

    $sql = "SELECT
                    r.id AS id_referencia,r.pn,r.producto,lis.id AS id,
                    m.nombre AS marca,r.color,r.descripcion,
                    COALESCE((SELECT GROUP_CONCAT(valor) FROM $bd.referencias_subpro_atributos WHERE id_referencia = r.id),'') AS atributos
            FROM 
                    $bd.referencias r
            INNER JOIN 
                    $bd.lista_market_place lis
                       ON r.id = lis.id_referencia
            LEFT JOIN 
                    $bd.tipo_pro_marcas m
                      ON r.id_tipo_marca = m.id
            WHERE 
                lis.estado = 1 AND lis.id = $id ";

        mysql_set_charset("utf8");
        $res = $this->BD->devolver_array($sql);


        $datos = array();
        foreach($res as $all_data => $row) {
          $array = array();
          $precio_dcs = $this->Valor_ListaP($row['id_referencia']);
          $array['id']        = $row['id'];
          $array['atributos']   = $row['atributos'];
          $array['valor_referencia']  = $precio_dcs[0]['valor_referencia'];
          $array['producto']  = $row['producto'];
          $array['marca']        = $row['marca'];
          $array['descripcion']  = $row['descripcion'];
          $array['color']        = $row['color'];
          $array['id_referencia'] = $row['id_referencia'];
          $datos[] = $array;
        }

        return $datos;
  }

  public function cargar_imagenes($id){
    if(!isset($_SESSION)) { 
      session_start(); 
    }
    if(isset($_SESSION["SESION_POS_FUNCIONES"])){
      $oSessionnueva = $_SESSION["SESION_POS_FUNCIONES"];
      $id_distri     = $oSessionnueva['VSid_distri'];
    }

    $sql_dis = "SELECT nombre_corto FROM {$GLOBALS["BD_NAME"]}.distribuidores WHERE id = $id_distri";
		$query_dis = $this->BD->devolver_array($sql_dis);
		$nombre_corto = $query_dis[0]["nombre_corto"];
    $bd = "{$GLOBALS["BD_DIS"]}" . $nombre_corto . "_" . $id_distri;

      $sql = "SELECT 
                    i_m.id,nombre
              FROM
                    $bd.tipo_imagenes_submarcas i_m
              WHERE 
                    i_m.id_tipo = $id AND i_m.tipo_submarca = 3 ";

         $result = $this->BD->devolver_array($sql);
         return $result;
  }


  public function hacer_pedido($data){
    

    if(isset($_SESSION["SESION_POS_FUNCIONES"])){
      $oSessionnueva = $_SESSION["SESION_POS_FUNCIONES"];
      $id_distri     = $oSessionnueva['VSid_distri'];
      $punto         = $oSessionnueva['VSidpos'];
      $id_usuario    = $oSessionnueva['VSidpos'];
        
    }

    $sql_dis = "SELECT nombre_corto FROM {$GLOBALS["BD_NAME"]}.distribuidores WHERE id = $id_distri";
		$query_dis = $this->BD->devolver_array($sql_dis);
		$nombre_corto = $query_dis[0]["nombre_corto"];
    $bd = "{$GLOBALS["BD_DIS"]}" . $nombre_corto . "_" . $id_distri;

    $consulta = "SELECT idpos, territorio, zona FROM {$GLOBALS["BD_POS"]}.puntos WHERE idpos = '$punto'";
      $datos_punto = $this->BD->devolver_array($consulta);

      $consulta = "SELECT id_regional FROM {$GLOBALS["BD_NAME"]}.territorios_distribuidor 
      WHERE id_territorio = ".$datos_punto[0]["territorio"]." GROUP BY id_regional;";
       $datos_reg = $this->BD->devolver_array($consulta);

    $total_pedido = 0;
    $cantidades_ped = 0;
    $total_all = 0;
    // saco totales de cantidades y valor para la cabecera y parceo los datos
    for($i=0; $i<count($data); $i ++){
      if($data[$i] != null){ 
       $total_pedido += intval(preg_replace('/[^0-9]+/', '', $data[$i]['precio']), 10); 
       $cantidades_ped += intval(preg_replace('/[^0-9]+/', '', $data[$i]['cantidad']), 10);
       $data[$i]['precio']   = preg_replace('/[^0-9]+/', '', $data[$i]['precio']);
       $data[$i]['cantidad'] = preg_replace('/[^0-9]+/', '', $data[$i]['cantidad']);
       $total_all += $data[$i]['cantidad'] * $data[$i]['precio'];
      }
    }
    
    
    // inserto cabecera 
    $sql_inser = "INSERT INTO $bd.marketplace__pedido (cantidad,total,fecha,hora,id_pos,id_zona,id_territorio,id_regional) values (".$cantidades_ped.",".$total_all.",CURDATE(),TIME(NOW()),".$punto.",".$datos_punto[0]['zona'].",".$datos_punto[0]['territorio'].",".$datos_reg[0]['id_regional'].")";
    $this->BD->consultar($sql_inser);
    $id_registro=$this->BD->last_insert_id();

     // inserto auditoria 
     $sql_inser_audi = "INSERT INTO $bd.marketplace__pedido_auditoria (id_pedido,cantidad,total,fecha,hora,id_pos,id_zona,id_territorio,id_regional,id_usuario_accion) values (".$id_registro.",".$cantidades_ped.",".$total_all.",CURDATE(),TIME(NOW()),".$punto.",".$datos_punto[0]['zona'].",".$datos_punto[0]['territorio'].",".$datos_reg[0]['id_regional'].",$id_usuario)";
     $this->BD->consultar($sql_inser_audi);


    // inserto detalle   
    for($i=0; $i<count($data); $i ++){
      if($data[$i] != null){
        $sql_inser_detalle = "INSERT INTO $bd.marketplace__detalle_pedido (id_pedido,id_referencia,cantidad,total) values 
        (".$id_registro.",".$data[$i]['id_referencia'].",".$data[$i]['cantidad'].",".$data[$i]['precio'].")";
         $this->BD->consultar($sql_inser_detalle); 
        
      }
    } 
    
    if($id_registro){ 
        unset($_SESSION['carrito']);
        return 0;
    }else{
        return 1;
    }  
  

  }


  public function Valor_ListaP($referencia){

	  if(!isset($_SESSION)) { 
      session_start(); 
     }
    $oSessionnueva = $_SESSION["SESION_POS_FUNCIONES"];
    $id_distri     = $oSessionnueva['VSid_distri'];
    $punto         = $oSessionnueva['VSidpos'];
        
  

    $lista = true;
		$r_referencia = array();
		
		$sql_dis = "SELECT nombre_corto FROM {$GLOBALS["BD_NAME"]}.distribuidores WHERE id = $id_distri";
		$query_dis = $this->BD->devolver_array($sql_dis);
		$nombre_corto = $query_dis[0]["nombre_corto"];
		$bd = "{$GLOBALS["BD_DIS"]}" . $nombre_corto . "_" . $id_distri;

        $consulta = "SELECT 
		                   idpos, territorio, zona 
					 FROM 
					       {$GLOBALS["BD_POS"]}.puntos 
					 WHERE 
							idpos = '$punto'";
				 
       $datos_punto = $this->BD->devolver_array($consulta);

        if(count($datos_punto)>0){
			
			$datos_reg = null;
			$datos_distri = null;
			$r_consulta_lista = null;
			$tipo_nivel = 3;
			$regional =0;
			// La funcion contiene los niveles de los cuales puede salir el precio de la referencia
			// Consulta para devolver los datos del punto.
            while ($lista) {
              switch ($tipo_nivel) {
				case 3://RUTA
					 $consulta_lista ="SELECT
												d.id_lista
										FROM 
										       $bd.lista_precio_market_place l
										INNER JOIN 
										       $bd.items_lista_market_place d
												ON l.id = d.id_lista
										INNER JOIN 
										       $bd.niveles_listaprecios_market_place n
												ON l.id = n.id_lista
										WHERE
											   d.estado = 1 AND l.estado = 1 AND  l.estado_vigencia=1 AND 
											   n.tipo_nivel = $tipo_nivel AND 
											   d.id_referencia  = $referencia AND 
											   n.id_nivel = ".$datos_punto[0]["zona"]." ";

                     $r_consulta_lista = $this->BD->devolver_array($consulta_lista);

					if(count($r_consulta_lista) > 0){
					    $lista = false;
					}else{
					    $tipo_nivel--;
					}

          break;
          case 2://CIRCUITO
			$consulta_lista ="SELECT
									d.id_lista
							FROM 
								$bd.lista_precio_market_place l
							INNER JOIN 
								$bd.items_lista_market_place d
									ON l.id = d.id_lista
							INNER JOIN 
								$bd.niveles_listaprecios_market_place n
									ON l.id = n.id_lista
							WHERE
								d.estado = 1 AND l.estado = 1 AND  l.estado_vigencia=1 AND 
								n.tipo_nivel = $tipo_nivel AND 
								d.id_referencia  = $referencia AND 
								n.id_nivel = ".$datos_punto[0]["territorio"]." ";
						 		
             $r_consulta_lista = $this->BD->devolver_array($consulta_lista);

			 if(count($r_consulta_lista)> 0){
				 $lista = false;
			 }else{
				$tipo_nivel--;
			 }

          break;
          case 1://REGIONAL OPERADOR

             $consulta = "SELECT 
			                    id_regional 
						  FROM 
						        {$GLOBALS["BD_NAME"]}.territorios_distribuidor 
						  WHERE 
								id_territorio = ".$datos_punto[0]["territorio"]."  
						  GROUP BY 
						        id_regional;";
			$datos_reg = $this->BD->devolver_array($consulta);

			$consulta_lista ="SELECT
									d.id_lista
							FROM 
								$bd.lista_precio_market_place l
							INNER JOIN 
								$bd.items_lista_market_place d
									ON l.id = d.id_lista
							INNER JOIN 
								$bd.niveles_listaprecios_market_place n
									ON l.id = n.id_lista
							WHERE
								d.estado = 1 AND l.estado = 1 AND  l.estado_vigencia=1 AND 
								n.tipo_nivel = $tipo_nivel AND 
								d.id_referencia  = $referencia AND 
								n.id_nivel = ".$datos_reg[0]["id_regional"]." ";
			
            $r_consulta_lista = $this->BD->devolver_array($consulta_lista);

            if(count($r_consulta_lista)> 0){
			  $lista = false;
            }else{
              $tipo_nivel--;
            }

          break;

          default:

            $r_consulta_lista = 0;
            $lista = false;

          break;
        }



      }
      //echo "si";
      if($r_consulta_lista != 0){


		$consulta_ref = "SELECT 
								d.precio_venta AS valor_referencia
						FROM 
								$bd.lista_market_place l 
						INNER JOIN 
								$bd.items_lista_market_place d
								ON l.id_referencia = d.id_referencia
						WHERE 
		                          l.estado = 1 AND d.id_lista = 21 AND
								  d.id_lista=".$r_consulta_lista[0]["id_lista"]." AND
								  d.id_referencia = $referencia";
      }else{
		
        $consulta_ref = "SELECT id,pn,precio AS valor_referencia FROM $bd.lista_market_place lm WHERE lm.id_referencia = $referencia";
      }
	 
	  $r_referencia =  $this->BD->devolver_array($consulta_ref);
	
    }else{
   
      //$this->Valor_ListaP_opeador($referencia,$punto,$id_distri);
      $consulta_ref = "SELECT id,pn,precio AS valor_referencia,0 AS iva_referencia,0 AS valor_directo FROM $bd.lista_market_place lm WHERE lm.id_referencia = $referencia";
    
      $r_referencia =  $this->BD->devolver_array($consulta_ref);

    }

    return $r_referencia;

}


  
} // Fin clase
?>