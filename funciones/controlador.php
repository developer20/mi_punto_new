<?php
include_once("../funciones/modelo.php");  // Incluye el Modelo.
$modelo = new funciones(); // Instancia a la clase del modelo

// Manejo de errores
try {
  $metodo = $_SERVER['REQUEST_METHOD'];
  $tipo_res = "";
  $response = null;
  $variables = $_POST;
  
  if (!isset($_POST['accion'])) {
    echo "0";
    
    return;
  }
  
  // Evita que ocurra un error si no manda accion.
  $accion = $variables['accion'];
  
  // Dependiendo de la accion se ejecutaran las tareas y se definira el tipo de respuesta.
  switch ($accion) {
    case 'Retornar_Departamento_Ciudad':
      $tipo_res = 'JSON'; //Definir tipo de respuesta;
      $response = $modelo->Retornar_Departamento_Ciudad();
      break;
    case 'geo_detalle_visita':
      $tipo_res = 'JSON'; //Definir tipo de respuesta;
      $id_check = $variables["id_check"];
      $repor_super = (isset($variables["repor_super"])) ? $variables["repor_super"] : "";
      $response = $modelo->geo_detalle_visita($id_check, $repor_super);
      break;
      case 'cargar_frecuencia':
        $tipo_res = 'JSON'; //Definir tipo de respuesta;
        $zona = $variables["zona"];
      
        $response = $modelo->obtener_frecuencia($zona);
        break;
      case 'obtener_proceso':
        $tipo_res = 'JSON'; //Definir tipo de respuesta;
        $id_check = $variables["id_check"];
        $response = $modelo->obtener_proceso($id_check);
        break;

        case "datos_referencia":
          $tipo_res = "JSON";
          $id = $variables["id"];
          $response = $modelo->datos_referencia($id);
        break;

        case 'cargar_imagenes': 
          $tipo_res = 'JSON'; //Definir tipo de respuesta;
          $id = $variables["id"];
          $response = $modelo->cargar_imagenes($id);
        break;

        case 'vaciar_carrito':
          if(!isset($_SESSION)) { 
            session_start(); 
          }
            
              if(isset($_SESSION['carrito']) ){
                unset($_SESSION['carrito']);
                echo "0";
              }else{
                echo "1";
              }
        break;

        case 'add_carrito':
          session_start();
          // si solo entra aqui es el 1 producto y no hay SESSION creada todavia
          if(isset($_POST['id'])){
            $id=$_POST['id'];
            $id_referencia=$_POST['id_referencia'];
            $nombre=$_POST['nombre'];
            $precio=$_POST['precio'];
            $cantidad=$_POST['cantidad'];
            $marca=$_POST['marca'];
            $imagen=$_POST['imagen'];
            $mi_carrito[]=array('id'=>$id,'id_referencia' => $id_referencia, 'nombre'=>$nombre,'precio'=>$precio,'cantidad'=>$cantidad ,'marca' => $marca, 'imagen' => $imagen);
            
          }
          if(isset($_SESSION['carrito'])){
            $mi_carrito=$_SESSION['carrito'];
             if(isset($_POST['id'])){
               $id=$_POST['id'];
               $id_referencia=$_POST['id_referencia'];
               $nombre=$_POST['nombre'];
               $precio=$_POST['precio'];
               $marca=$_POST['marca'];
               $cantidad=$_POST['cantidad'];
               $imagen = (!empty($_POST['imagen'])) ? $_POST['imagen'] : "";
           
            /*aumente la cantidad en vez k repita todo (enter mas la logica de eso)*/
            $pos=-1;
             for($i=0;$i<count($mi_carrito);$i++){
                if($id==$mi_carrito[$i]['id']){
                   $pos=$i;
                }
             }
          
             if($pos<>-1){
                  $cuanto=$mi_carrito[$pos]['cantidad']+$cantidad;
                   /*Si entra aqui qiere decir k ya tiene ese producto en el carrito asi k aumentara la cantidad*/
                   $mi_carrito[$pos]=array('id'=>$id,'id_referencia' => $id_referencia, 'nombre'=>$nombre,'precio'=>$precio,'cantidad'=>$cuanto ,'marca' => $marca, 'imagen' => $imagen);
             
             }else{
                   /*de lo contrario lo uncluyo´por 1 ves */
                   $mi_carrito[]=array('id'=>$id, 'id_referencia' => $id_referencia, 'nombre'=>$nombre,'precio'=>$precio,'cantidad'=>$cantidad , 'marca' => $marca, 'imagen' => $imagen); 
             }
           }
          }

           /*cambio cantidades */
           if(isset($_POST['id2'])){
              $posicion=-1;
              for($i=0;$i<count($mi_carrito);$i++){
                 if($_POST['id2'] == $mi_carrito[$i]['id']){ 
                    $posicion=$i;
                 }
              }

              $cuanto=$_POST['cantidad2'];
              $mi_carrito[$posicion]['cantidad']=$cuanto;
             
           }
     
           /*eliminar producto*/
           if(isset($_POST['id3'])){
              $posicion_delete=-1;
              for($i=0;$i<count($mi_carrito);$i++){
                if($_POST['id3'] == $mi_carrito[$i]['id']){ 
                    $posicion_delete=$i;
                }
              }

              $mi_carrito[$posicion_delete] = null;
           }
           
           
          if(isset($mi_carrito)){ 
            $_SESSION['carrito']=$mi_carrito;
            $cantidades_ped = 0;
            for($i=0;$i<count($mi_carrito);$i++){
               $cantidades_ped += intval(preg_replace('/[^0-9]+/', '', $mi_carrito[$i]['cantidad']), 10); 
            }
          }
          
          echo $cantidades_ped;
        break;


        case 'ver_carrito':
          $tipo_res = "JSON";
          if(!isset($_SESSION)) { 
            session_start(); 
          }   
        
          if(isset($_SESSION['carrito'])){
            $mi_carrito=$_SESSION['carrito'];
          }else{
            $mi_carrito=0;
          }
          $response = $mi_carrito;

        break;

        case 'hacer_pedido':
          $tipo_res = "HTML";
          if(!isset($_SESSION)) { 
            session_start(); 
          }
          $mi_carrito=$_SESSION['carrito'];
          $response = $modelo->hacer_pedido($mi_carrito);
          
        break;

        case 'cantidad_carrito':
          if(!isset($_SESSION)) { 
            session_start(); 
          }
          if(isset($_SESSION['carrito'])){ 
            $cantidades_ped = 0;
            for($i=0;$i<count($_SESSION['carrito']);$i++){
              $cantidades_ped += intval(preg_replace('/[^0-9]+/', '', $_SESSION['carrito'][$i]['cantidad']), 10); 
             }
             echo $cantidades_ped;
          }else{
             echo 0;
          }
        break;

      
  }
  
  // Respuestas del Controlador
  if ($tipo_res == "JSON") {
    echo json_encode($response, true); // $response será un array con los datos de nuestra respuesta.
  } else if ($tipo_res == "HTML") {
    echo $response; // $response será un html con el string de nuestra respuesta.
  }
} catch (Exception $e) {}