<?php /*
 *  Archivo: frm_venta_servicios.php
 *  Versión: 1 
 *  Fecha: 08/11/2013 
 *  Hora: 12:23 p.m    
 *  Usuario: Juan David Trujillo
 */

//require '../config/Session.php';
require '../config/var_config.php';
require '../config/mainModel.php';
require '../config/mainTemplate.php';

/*
if(isset($_SESSION[$GLOBALS["SESION_POS"]]))
{
	session_start();	
}*/

/*
if(!$oSession)
{
	header("Location: ../../index.php");
}
else
{*/
	$oSession = unserialize($_SESSION[$GLOBALS["SESION_POS"]]);	 	  
	$idpos=$oSession->VSidpos;
		  
	$BD= new BD();
	$BD->conectar();
		
	include ('class.ezpdf.php');
	

    $id=$_REQUEST["registro"];

	$sql="SELECT venta__servicios.id,fecha,horatransaccion,transacionidresp,movil,valor,operador id_servicio,operadores.descripcion,venta__servicios.id_punto FROM venta__servicios,operadores WHERE venta__servicios.id_punto=$idpos AND operadores.id=operador AND venta__servicios.id = $id";
	$resp=$BD->consultar($sql);
	
	$id_serv = $resp->fields["id_servicio"];

	$y = 170;

	if($id_serv == 6){
		$pdf = new Cezpdf($size = array(0,0,200,230));
		$pdf->selectFont('fonts/Courier.afm');
		$y = 200;
	}else{
		$pdf = new Cezpdf($size = array(0,0,200,200));
		$pdf->selectFont('fonts/Courier.afm');
	}

	/*
	$sql="SELECT telefono FROM operadores WHERE id=$id_serv AND estado_aten = 1";
	$resp2=$BD->consultar($sql);*/
	
	$sql="SELECT razon as nombres FROM puntos WHERE idpos=".$idpos;
	$resp3=$BD->consultar($sql);

	$pdf->addText(5,$y,10,"<b>"." Recibo Venta Nro ".$resp->fields["id"]);
	$pdf->addText(5,$y-=20,9,"<b>"." Fecha Venta :");
	$pdf->addText(88,$y,8,"<b>".$resp->fields["fecha"]);
	
	$pdf->addText(5,$y-=10,9,"<b>"." Hora Venta :");
	$pdf->addText(85,$y,8,"<b>".$resp->fields["horatransaccion"]);
	
	$pdf->addText(5,$y-=10,9,"<b>"." IDTransa :");
	$pdf->addText(85,$y,8,"<b>".$resp->fields["transacionidresp"]);
	
	$pdf->addText(5,$y-=10,9,"<b>"." Concepto :");
	$pdf->addText(85,$y,8,"<b>"."Recarga");
	
	$pdf->addText(5,$y-=10,9,"<b>"." Nro Movil :");
	$pdf->addText(85,$y,8,"<b>".$resp->fields["movil"]);
	
	$pdf->addText(5,$y-=10,9,"<b>"." Valor Venta :");
	$pdf->addText(85,$y,8,"<b>"."$".number_format($resp->fields["valor"], 0, ',', '.'));
	
	$pdf->addText(5,$y-=10,9,"<b>"." Descripcion :");
	$pdf->addText(85,$y,8,"<b>".$resp->fields["descripcion"]);
	
	$pdf->addText(5,$y-=10,9,"<b>"." Vendedor :");
	$pdf->addText(85,$y,8,"<b>".$resp3->fields["nombres"]);
	
	/*
	if($BD->numreg($resp2)!=0){
		$pdf->addText(5,50,9,"<b>"." Atencion :");
	    $pdf->addText(85,50,8,"<b>".$resp2->fields["telefono"]);
	}*/

	if($resp->fields["id_servicio"]==6){

		$pdf->addText(10,$y-=20,9,"<b>"."---------------------------------");

		$pdf->addText(10,$y-=20,9,"<b>"."TU RECARGA NO VENCE");
	    $pdf->addText(5,$y-=10,8,"<b> Siempre y cuando la linea no este");
	    $pdf->addText(5,$y-=10,8,"<b> inactiva durante tres (3) meses Ver");
	    $pdf->addText(5,$y-=10,8,"<b> condiciones www.movistar.co");
	    $pdf->addText(5,$y-=10,8,"<b> Tasacion por segundos Linea de");
	    $pdf->addText(5,$y-=10,8,"<b> Atencion 01800 930930 y/o *611");

	}

	ob_end_clean();
	$pdf->ezStream();

//}
?>