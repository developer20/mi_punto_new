<?php /*
 *  Archivo: frm_venta_servicios.php
 *  Versión: 1
 *  Fecha: 08/11/2013
 *  Hora: 12:23 p.m
 *  Usuario: Juan David Trujillo
 */

//require '../config/Session.php';
require '../config/var_config.php';
require '../config/mainModel.php';
require '../config/mainTemplate.php';

/*
if(isset($_SESSION[$GLOBALS["SESION_POS"]]))
{
	session_start();
}*/

/*
if(!$oSession)
{
	header("Location: ../../index.php");
}
else
{*/
	$oSession = unserialize($_SESSION[$GLOBALS["SESION_POS"]]);
	$idpos=$oSession->VSidpos;

	$BD= new BD();
	$BD->conectar();

	include ('class.ezpdf.php');

    $id=$_REQUEST["registro"];

	$sql="SELECT venta__servicios.id,venta__servicios.fecha,venta__servicios.horatransaccion,venta__servicios.RRN AS transacionidresp,venta__servicios.movil,venta__servicios.valor,venta__servicios.operador id_servicio,operadores.descripcion,venta__servicios.id_punto,puntos.razon as nombres,puntos.detalle_direccion,departamentos.nombre AS nom_depa,venta__servicios.terminal,distribuidores.nombre AS nom_distri FROM venta__servicios INNER JOIN operadores ON (operadores.id=venta__servicios.operador) INNER JOIN puntos ON (puntos.idpos = venta__servicios.id_punto) INNER JOIN {$GLOBALS["BD_NAME"]}.departamentos ON (departamentos.id = puntos.depto) INNER JOIN {$GLOBALS["BD_NAME"]}.distribuidores ON (distribuidores.id = venta__servicios.id_distri) WHERE venta__servicios.id_punto=$idpos AND venta__servicios.id = $id AND venta__servicios.estado = 1";

	$resp=$BD->consultar($sql);

	if($BD->numreg($resp)>0){
		$id_serv = $resp->fields["id_servicio"];
		$direccion1 = $resp->fields["detalle_direccion"].", ".$resp->fields["nom_depa"];
		$direccion2 = "";

		if(strlen($direccion1) > 16)
		{
			$direccion1 = substr ($resp->fields["detalle_direccion"], 0, 16)."-";
			$direccion2 = substr ($resp->fields["detalle_direccion"],16).", ".$resp->fields["nom_depa"];
		}

		$pdf = new Cezpdf($size = array(0,0,200,340));
		$pdf->selectFont('fonts/Courier.afm');
		$y = 320;

		$pdf->addText(5,$y,5,""."---------------------------------------------------------------");
		//$pdf->addText(5,$y-=15,15,"<b>"."MOVISTAR");
		$pdf->addTextWrap(5,$y-=17,175,15,utf8_decode("<b>".utf8_decode(strtoupper($resp->fields["descripcion"]))."</b>"),"left",0);
		$pdf->addText(5,$y-=14,5,""."---------------------------------------------------------------");
		//$pdf->addText(20,$y-=10,12,"<b>"."COMPROBANTE DE RECARGA");
		$pdf->addTextWrap(10,$y-=10,175,12,utf8_decode("COMPROBANTE DE RECARGA"),"center",0);

		//$pdf->addText(5,$y-=17,9,"<b>"."MONTO         $");
		$pdf->addTextWrap(5,$y-=17,150,9,utf8_decode("<b>MONTO          $</b>"),"left",0);
		$pdf->addTextWrap(5,$y=$y,190,9,utf8_decode("<b>".number_format($resp->fields["valor"], 0, ',', '.')."</b>"),"right",0);
		$pdf->addTextWrap(5,$y-=10,150,9,utf8_decode("<b>CELULAR        :</b>"),"left",0);
		$pdf->addTextWrap(5,$y=$y,190,9,utf8_decode("<b>".$resp->fields["movil"]."</b>"),"right",0);
		$pdf->addTextWrap(5,$y-=10,150,9,utf8_decode("<b>C.AUTORIZACIÓN :</b>"),"left",0);
		$pdf->addTextWrap(5,$y=$y,190,9,utf8_decode("<b>".$resp->fields["transacionidresp"]."</b>"),"right",0);

		$pdf->addTextWrap(5,$y-=20,150,9,utf8_decode("FECHA          :"),"left",0);
		$pdf->addTextWrap(5,$y=$y,190,9,$resp->fields["fecha"],"right",0);
		$pdf->addTextWrap(5,$y-=10,150,9,utf8_decode("HORA           :"),"left",0);
		$pdf->addTextWrap(5,$y=$y,190,9,$resp->fields["horatransaccion"],"right",0);
		$pdf->addTextWrap(5,$y-=10,150,9,utf8_decode("COMERCIO       :"),"left",0);
		$pdf->addTextWrap(5,$y=$y,190,9,utf8_decode($resp->fields["nombres"]),"right",0);
		$pdf->addTextWrap(5,$y-=10,150,9,utf8_decode("LOCAL          :"),"left",0);
		$pdf->addTextWrap(5,$y=$y,190,9,utf8_decode($direccion1),"right",0);
		if($direccion2 != ""){
			$pdf->addTextWrap(5,$y-=10,230,9,utf8_decode($direccion2),"left",0);
		}
		$pdf->addTextWrap(5,$y-=10,150,9,utf8_decode("TERMINAL       :"),"left",0);
		$pdf->addTextWrap(5,$y=$y,190,9,$resp->fields["terminal"],"right",0);
		$pdf->addTextWrap(5,$y-=10,150,9,utf8_decode("VENDEDOR       :"),"left",0);
		$pdf->addTextWrap(5,$y=$y,190,9,$resp->fields["id_punto"],"right",0);
		$pdf->addTextWrap(5,$y-=10,150,9,utf8_decode("PRODUCTO       :"),"left",0);
		$pdf->addTextWrap(5,$y=$y,190,9,"00000001","right",0);
		$pdf->addTextWrap(5,$y-=10,150,9,utf8_decode("CONCENTRADOR   :"),"left",0);
		$pdf->addTextWrap(5,$y=$y,190,9,utf8_decode($resp->fields["nom_distri"]),"right",0);

		$pdf->addTextWrap(0,$y-=20,200,9,utf8_decode("Su celular ha sido recargado, consulte"),"center",0);
		$pdf->addTextWrap(0,$y-=10,200,9,utf8_decode("consulte su saldo enviando un"),"center",0);
		$pdf->addTextWrap(0,$y-=10,200,9,utf8_decode("mensajes con la letra S al 300"),"center",0);
		$pdf->addTextWrap(0,$y-=10,200,9,utf8_decode("desde tu celular."),"center",0);
		$pdf->addTextWrap(0,$y-=10,200,9,utf8_decode("Ahorra con tu bolsa de minutos o"),"center",0);
		$pdf->addTextWrap(0,$y-=10,200,9,utf8_decode("mensajes llama al 103 opcion 5 ó"),"center",0);
		$pdf->addTextWrap(0,$y-=10,200,9,utf8_decode("marca el *210# y sigue las"),"center",0);
		$pdf->addTextWrap(0,$y-=10,200,9,utf8_decode("instrucciones que aparecen en"),"center",0);
		$pdf->addTextWrap(0,$y-=10,200,9,utf8_decode("pantalla."),"center",0);

		$pdf->addTextWrap(0,$y-=10,200,9,utf8_decode("Revisa las ofertas de tu operador"),"center",0);
		$pdf->addTextWrap(0,$y-=10,200,9,utf8_decode(utf8_decode(strtoupper($resp->fields["descripcion"]))),"center",0);

	}


	/*if($resp->fields["id_servicio"]==6){

		$pdf->addText(10,$y-=20,9,"<b>"."---------------------------------");

		$pdf->addText(10,$y-=20,9,"<b>"."TU RECARGA NO VENCE");
	    $pdf->addText(5,$y-=10,8,"<b> Siempre y cuando la linea no este");
	    $pdf->addText(5,$y-=10,8,"<b> inactiva durante tres (3) meses Ver");
	    $pdf->addText(5,$y-=10,8,"<b> condiciones www.movistar.co");
	    $pdf->addText(5,$y-=10,8,"<b> Tasacion por segundos Linea de");
	    $pdf->addText(5,$y-=10,8,"<b> Atencion 01800 930930 y/o *611");

	}*/

	ob_end_clean();
	$pdf->ezStream();

//}
?>