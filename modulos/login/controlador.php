<?php
//include("../../config/mainController.php");
//$controller = new mainController;
$metodo = $_SERVER['REQUEST_METHOD'];
$recurso = $_SERVER['REQUEST_URI'];
$tipo_res = "";
$response = ""; 
// Se manejaran dos tipos JSON y HTML
// Dependiendo del método de la petición ejecutaremos la acción correspondiente.
// Por ahora solo POST, todas las llamadas se haran por POST
	$variables = $_POST;

	if(!isset($_POST['accion'])) {
		print json_encode(0);
		return;
	}
    $accion = $variables['accion'];

	function __autoload($class){
		include_once("../../modulos/login/modelo.php");	
	}

	$login = new Login(); // Instancia a la clase del modelo

	switch($accion) {
		case 'autenticacion':
			 $tipo_res = 'JSON'; //Definir tipo de respuesta;
			 $usuario = $variables['usuario'];
			 $pass = $variables['pass'];
			 $response = $login->Autenticar_Usuario($usuario, $pass);
		break;
		case 'Recover':
			 $tipo_res = 'HTML'; //Definir tipo de respuesta;
			 $idPos = $variables['idPos'];
			 $response = $login->Restablecer_Pass($idPos);
		break;
		case 'validarCorreo':
			$tipo_res = 'HTML'; //Definir tipo de respuesta;
			$idPos = $variables['idPos'];
			$response = $login->validarCorreo($idPos);
	   break;
	}

	function hexToStr($hex){
	    $string='';
	    for ($i=0; $i < strlen($hex)-1; $i+=2){
	        $string .= chr(hexdec($hex[$i].$hex[$i+1]));
	    }
	    return $string;
	}
if($tipo_res == "JSON")
{
  echo json_encode($response,true); // $response será un array con los datos de nuestra respuesta.
}
elseif ($tipo_res == "HTML") {
  echo $response; // $response será un html con el string de nuestra respuesta.
}
exit();
