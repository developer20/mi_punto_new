$(document).ready(function() {

    $("#form-login").submit(function() {
        event.preventDefault();
        autenticarUsuario();
    });

    $("#recover-password").click(function() {
        frmrecover();
    });

    $("#btn-cancel-recover").click(function() {
        frmlogin();
    });

    $("#form-recover").submit(function() {
        event.preventDefault();
        reco_pss();
    });
});

function toHex(str) {
    var hex = '';
    for (var i = 0; i < str.length; i++) {
        hex += '' + str.charCodeAt(i).toString(16);
    }
    return hex;
}

function autenticarUsuario() {
    //console.log($("#g-recaptcha-response").val());
    var usuario = $("#usuario").val();
    var password = $("#password").val();
    //var c_cha = toHex($("#g-recaptcha-response").val());
    if (usuario == "") {
        notif({
            type: "error",
            msg: "El nombre de usuario es obligatorio",
            position: "center",
            width: 400,
            height: 60,
            autohide: true
        });
    } else if (password == "") {
        notif({
            type: "error",
            msg: "La contraseña del usuario es obligatoria",
            position: "center",
            width: 400,
            height: 60,
            autohide: true
        });
    } else {

        $.post('modulos/login/controlador.php', {
                accion: 'autenticacion',
                usuario: usuario,
                pass: password
            },
            function(data, textStatus) {

                data = JSON.parse(data);
                if (data.estado == 1) {
                    localStorage.setItem('est_pss_pos', data.cp);
                    //validarPass();

                    localStorage.setItem('psid', data.idp);
                    localStorage.setItem('welcome', 0);


                    if (data.cp == 1) {
                        location.href = "?mod=principal";
                        localStorage.setItem('est_enc', data.est_enc)
                    } else {
                        //location.href = "?mod=changePass";
                        validarCorreo(data.idp);
                        localStorage.setItem('est_enc', 0);
                    }

                    //location.href = "?mod=principal";
                } else if (data.estado == -1) {
                    //grecaptcha.reset();
                    notif({
                        type: "error",
                        msg: data.mns,
                        position: "center",
                        width: 400,
                        height: 60,
                        autohide: true
                    });

                }
            }
        );
    }
}

function frmrecover() {
    $(".form-login").hide();
    $(".form-recover").show();
}

function reco_pss() {

    var idPos = $('#id-pos').val();

    if (idPos != "") {
        $.post('modulos/login/controlador.php', {
                accion: 'Recover',
                idPos: idPos
            },
            function(data, textStatus) {
                if (confirma_rest(data, 'Recover')) {
                    if (data != 0 || data != "") {
                        var info = ''
                        if (data == "1") {
                            info = '<h4><p style="color:#fff;">Se ha enviado un correo asociado al id : <strong>' + idPos + '</strong>  con el link de restablecimiento de contraseña.</p></h4>'
                            info += '<button type="button" name="btn_cancel" id="btn_cancel" class="button_logueo_cancel">VOLVER</button>'
                            $('#form-recover').html(info);

                            $("#btn_cancel").click(function() {
                                frmlogin();
                            });

                        } else if (data == "2") {
                            notif({
                                type: "error",
                                msg: "El id punto: <strong>" + idPos + "</strong> no puede realizar esta solicitud",
                                position: "center",
                                width: 550,
                                height: 60,
                                autohide: true
                            });
                        } else if (data == "3") {
                            notif({
                                type: "error",
                                msg: "El id punto enviado no es un id punto valido.",
                                position: "center",
                                width: 550,
                                height: 60,
                                autohide: true
                            });
                        } else if (data == "0") {
                            notif({
                                type: "error",
                                msg: "Ha ocurrido un error en la operación.",
                                position: "center",
                                width: 500,
                                height: 60,
                                autohide: true
                            });
                        }
                    }
                }
            });

    }
}

function frmlogin() {

    $(".form-recover").hide();
    $(".form-login").show();
}

function validarCorreo(idPos) {

    $('#forms-login').html("Se está enviando un correo electrónico, por favor ten paciencia...");

    $.post('modulos/login/controlador.php', {
            accion: 'validarCorreo',
            idPos: idPos
        },
        function(data, textStatus) {
            if (confirma_rest(data, 'Recover')) {
                if (data != 0 || data != "") {
                    var info = ''
                    if (data == "1") {
                        info = '<h4><p style="color:#c4473b;">Se ha enviado un correo para autenticar el acceso con el link de restablecimiento de contraseña.</p></h4>'
                        $('#forms-login').html(info);
                    } else if (data == "0") {
                        notif({
                            type: "error",
                            msg: "Ha ocurrido un error en la operación.",
                            position: "center",
                            width: 500,
                            height: 60,
                            autohide: true
                        });
                    }
                }
            }
        });


}