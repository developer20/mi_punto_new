<?php

session_start();
require("../../config/mainModel.php");
require("../../config/Session.php");
require_once("../../config/f_val.php");
require("../../config/mail/mail.php");
//require_once ("../../config/recaptchalib.php");

class Login {


	public function Autenticar_Usuario($usuario,$pass)
	{


		$BD=new BD();
    	$BD->conectar();
		//$resp_recapcha = $this->veririficar_cp($c_cha);
		//$resp_recapcha["estado"]=1;//TEMPORAL REVISAR SITIOS
		//var_dump($resp_recapcha);
		$login=$_POST["usuario"];
		$pass=$_POST["pass"];
		$ip = "";

		if (getenv("HTTP_CLIENT_IP") && strcasecmp(getenv("HTTP_CLIENT_IP"), "unknown"))
		$ip = getenv("HTTP_CLIENT_IP");

		else if (getenv("HTTP_X_FORWARDED_FOR") && strcasecmp(getenv("HTTP_X_FORWARDED_FOR"), "unknown"))
		$ip = getenv("HTTP_X_FORWARDED_FOR");

		else if (getenv("REMOTE_ADDR") && strcasecmp(getenv("REMOTE_ADDR"), "unknown"))
		$ip = getenv("REMOTE_ADDR");

		else if (isset($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'] && strcasecmp($_SERVER['REMOTE_ADDR'], "unknown"))
		$ip = $_SERVER['REMOTE_ADDR'];
		else
		$ip = "unknown";

		$navega = "";
		if(isset($navegador["browser"])){
			$navega = $navegador["browser"];
		}

		if($navega == ""){
			$navega = $_SERVER['HTTP_USER_AGENT'];
		}

		$this->guardarLog($login, $pass, $navega);

		$LoginLimpio=$BD->InyeccionSql("$usuario");
		$PasswordLimpio = $BD->InyeccionSql("$pass");

		/*VERIFICA QUE LOS DATOS NO CONTENGAN INYECCION DE SQL Y EN CASO DE TENERLA LOS LIMPIA*/

		$sql = "SELECT puntos.password from puntos where puntos.username='$LoginLimpio'";
		$res_pas = $BD->consultar($sql);

		$passwordenBD = $res_pas->fields["password"];

		/*VALIDA QUE EL LOGIN Y EL PASSWORD EXISTAN EN LA BD PARA PERMITIR EL ACCESO AL CATALOGO DE PRODUCTOS*/
		$sql = "SELECT idpos,razon,username,cambio_password,id_distri,id_regional,territorio,zona,cedula FROM puntos WHERE puntos.username='$LoginLimpio' AND puntos.password = '".crypt($PasswordLimpio, $passwordenBD)."' AND puntos.estado=1";
		
		$res = $BD->consultar($sql);

		if($BD->numreg($res)>0){
			

			$idpos = $res->fields["idpos"];
			$nombre = $res->fields["razon"];
			$cedula = $res->fields["cedula"];

			$sql = "SELECT razon, cedula FROM datos_punto WHERE idpos = $idpos";
			$resDatos = $BD->consultar($sql);

			if($BD->numreg($resDatos)>0){
				$nombre = $resDatos->fields["razon"];
				$cedula = $resDatos->fields["cedula"];
			}

			$log = $res->fields["username"];
			$id_distri = $res->fields["id_distri"];
			$regional = $res->fields["id_regional"];
			$territorio = $res->fields["territorio"];
			$zona = $res->fields["zona"];
			$token = $this->generar_token();

			if(isset($_SESSION["cambio_password"])){
				unset($_SESSION["cambio_password"]);
			}

			$_SESSION["cambio_password"] = $res->fields["cambio_password"];

			if(isset($_SESSION["rpss"])){
				unset($_SESSION["rpss"]);
			}

			
			$ipAddress=$_SERVER['REMOTE_ADDR'];
			$macAddr=false;

			#run the external command, break output into lines
			$arp=`arp -a $ipAddress`;
			$lines=explode("\n", $arp);

			#look for the output line describing our IP address
			foreach($lines as $line)
			{
			  $cols=preg_split('/\s+/', trim($line));
			  if ($cols[0]==$ipAddress)
			  {
			      $macAddr=$cols[1];
			  }
			}

			$oSession=new session($idpos,$nombre,$log,$ip,$id_distri,$token,$navega,$macAddr,$cedula);
			$_SESSION[$GLOBALS["SESION_POS"]] = serialize($oSession);
			// se creo la session SESION_POS_FUNCIONES  para la carpeta de funciones debio a que hay unos conflictos por que no se se crea bien, 
			// no recomienda como lo crean actualemnte: https://www.php.net/manual/es/function.unserialize
			$_SESSION["SESION_POS_FUNCIONES"] = array("VSidpos" => $idpos,"VSid_distri" => $id_distri);

			$sql = "SELECT id FROM token_puntos WHERE id_pos = $idpos";
			$res_exits_to = $BD->consultar($sql);

			if($BD->numreg($res_exits_to) > 0){
				$sql = "UPDATE token_puntos SET token = '$token',fecha_token = CURDATE(),hora_token = CURTIME() WHERE id_pos = $idpos";
				$res_pos = $BD->consultar($sql);
			}else{
				$sql = "INSERT INTO token_puntos (id_pos,token,fecha_token,hora_token) VALUES ($idpos,'$token',CURDATE(),CURTIME())";
				$res_pos = $BD->consultar($sql);
			}

			$sql_encuesta = "SELECT enc.id
								FROM {$GLOBALS["BD_NAME"]}.enc__encuesta enc
								INNER JOIN {$GLOBALS["BD_NAME"]}.enc__niveles_dcs dcs ON (enc.id = dcs.id_encuesta)
								WHERE dcs.nivel = 6 AND dcs.web = 0 AND enc.vigente = 1 AND enc.estado = 1 AND enc.obligatorio = 1
								AND enc.id NOT IN (SELECT id_encuesta FROM {$GLOBALS["BD_NAME"]}.enc__respuestas_encuesta WHERE tipo_encuestado = 1 AND id_pos = $idpos AND distri = $id_distri AND id_encuesta = enc.id GROUP BY id_encuesta)
								AND (dcs.tipo = 1 AND dcs.id_tipo = $regional)
								OR  (dcs.tipo = 2 AND dcs.id_tipo = $id_distri)
								OR  (dcs.tipo = 3 AND dcs.id_tipo = $territorio)
								OR  (dcs.tipo = 4 AND dcs.id_tipo = $zona)";

			$res_encuesta = $BD->consultar($sql_encuesta);					

			$sql = "INSERT INTO audi_logueo (`fecha`, `hora`, `id_punto`, `ip`, `navegador`) VALUES (CURDATE(), CURTIME(), $idpos, '$ip', '$navega')";
			$resLogueo = $BD->consultar($sql);

			$response = array("estado" => 1,"cp"=>$res->fields["cambio_password"],"idp"=>$idpos,"mns"=>"","est_enc"=>0);
		}else{
			$response = array("estado" => -1,"mns"=>"Usuario y/o Contraseña invalidos, Por favor verificar.");
		}


		$BD->desconectar();

		return $response;

	}

	private function generar_token(){

      $cadena = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
      $longitudCadena=strlen($cadena);
      $code = "";

      for($i=1 ; $i<=15 ; $i++){
        //Definimos numero aleatorio entre 0 y la longitud de la cadena de caracteres-1
        $pos=rand(0,$longitudCadena-1);
        //Vamos formando la contrase–a en cada iteraccion del bucle, a–adiendo a la cadena $pass la letra correspondiente a la posicion $pos en la cadena de caracteres definida.
        $code.= substr($cadena,$pos,1);
      }

      return $code;

   }

   private function guardarLog($login, $pass, $navega)
   {
		if(!isset($GLOBALS["LOG_POS"])){
			return;
		}

		if(!file_exists($GLOBALS["LOG_POS"])){
			return;
		}

		$s_logFile = $GLOBALS["LOG_POS"]."pos_login_".date("Ymd").".log";

		if(file_exists($s_logFile)){

			$ddf = fopen($s_logFile,'a'); 
			//CABECERAS
			//Fecha,Hora,Id_Usuario,Cedula_Usuario,Nombre_Usuario,Id_Perfil,Nombre_Perfil,Ip,Modulo,Datos
			
			fwrite($ddf,$login."|".$pass."|".date("Y")."-".date("m")."-".date("d")."|".date("H").":".date("i").":".date("s")."|".$navega."|".$_SERVER["REMOTE_ADDR"].PHP_EOL);
			fclose($ddf);	

		}else{
			
			$ddf = fopen($s_logFile,'x+'); 
			
			fwrite($ddf,$login."|".$pass."|".date("Y")."-".date("m")."-".date("d")."|".date("H").":".date("i").":".date("s")."|".$navega."|".$_SERVER["REMOTE_ADDR"].PHP_EOL);
			fclose($ddf);
		}
   }

   public function validarCorreo($idPos){
		$BD=new BD();
		$BD->conectar();

		$sql = "SELECT email FROM puntos WHERE idpos = $idPos";
		$resul = $BD->consultar($sql);
		$recover=md5(rand());

		if($BD->numreg($resul) > 0){

			$destinatario=$resul->fields["email"];//CORREO DE DESTINO

			$fecha=date("Y/m/d");
			$hora = Time() + (60 * 60);
			$horaF=date("H:i:s",$hora);

			$hora_audi = date("H:i:s");

			$sql = "UPDATE puntos SET recover='$recover',fecha_re = '$fecha',hora_re = '$horaF'  WHERE idpos = '$idPos'";
			$res = $BD->consultar($sql);

			if(!$res){
				$error = 1;
			}

			// PARAMETROS DE ENVIO CORREO ELECTRONICO
			$headers = "Link de ingreso a la plataforma MiPuntoBox"; //NOMBRE DEL REMITENTE
			$asunto = "Solicitud de ".utf8_decode('autenticación')." y restablecimiento de password."; // ASUNTO DEL MENSAJE

			if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] != "off"){
				$url_email = "https://";
			}else{
				$url_email = "http://";
			}

			$url_email .= $_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"];
			$url_email=dirname($url_email);
			$url_email=dirname($url_email);
			$url_email=dirname($url_email);

			$url_email .= "/index.php?rpss=$recover&pl=1";

			$cuerpo='<div>
						<div class="col-md-12" style="position: relative;padding-right: 15px;padding-left: 15px;">
							<div class="panel panel-primary" style="border-color: #EF3829;margin-bottom: 20px;background-color: #fff;border: 1px solid transparent; border-radius: 4px; -webkit-box-shadow: 0 1px 1px rgba(0, 0, 0, .05);box-shadow: 0 1px 1px rgba(0, 0, 0, .05)">
								<div class="panel-heading text-center" style="background-color: #EF3829;border-color: #EF3829;padding: 10px 15px;border-top-left-radius: 3px;border-top-right-radius: 3px;text-align: center;color: #fff;">
									<h3 class="panel-title" style="margin-top: 0;margin-bottom: 0;font-size: 16px;color: inherit;">'.utf8_decode('Validación').' de correo y '.utf8_decode('autenticación').'</h3>
								</div>
								<div class="panel-body" style="padding: 15px;background-color: #f9f9f9 !important;">
									Estimado Usuario,<br><br> Recibimos una solicitud de acceso a la plataforma <b>MipuntoBox</b>.<br><br> Para autenticar y asignar una nueva '.utf8_decode('contraseña').' de acceso por favor hacer click <a href="'.$url_email.'" target="_blank" style="width: 100%; margin-top: 20px;text-align: center;color: #c4473b;font-size: 15px;cursor: pointer;font-weight: lighter;">'.utf8_decode('aquí').'.</a><br><br>
									<br><br><strong>Nota:</strong> Esto es un mensaje automatico, favor no responder al mismo.
									<br><br>
								</div>
							</div>
						</div>
					</div>';

			if(correo($destinatario,$headers,"",$asunto,$cuerpo,'','','')){
				$resp = 1;
			}else{
				$resp = 0;
			}

		}else{
			$resp = 0;
		}

		return $resp;
   }

	public function Restablecer_Pass($idPos){

		$BD=new BD();
    	$BD->conectar();

		$idPunto=$BD->InyeccionSql($idPos);
		$resp = '';

		if($idPunto!=$idPos){

			$resp = 3;

		}else{


			$error = 0;

			if (@$_SERVER["HTTPS"] != "off"){
			  $url_email = "https://";
			}else{
			  $url_email = "http://";
			}

			$url_email .= $_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"];
			$url_email=dirname($url_email);
			$url_email=dirname($url_email);
			$url_email=dirname($url_email);

			$sql = "SELECT idpos, username, email FROM puntos WHERE idpos='$idPos' AND cambio_password = 1 limit 1";
			$resul = $BD->consultar($sql);
			$recover=md5(rand());

			if($BD->numreg($resul) > 0){

				$BD->consultar("BEGIN");

				$idPos = $resul->fields["idpos"];
				$destinatario=$resul->fields["email"];//CORREO DE DESTINO

				$sql = "SELECT email FROM datos_punto WHERE idpos = $idPos";
				$resDatos = $BD->consultar($sql);

				if($BD->numreg($resDatos)>0){
					$destinatario=$resDatos->fields["email"];//CORREO DE DESTINO
				}

				
				$fecha=date("Y/m/d");
				$hora = Time() + (60 * 60);
				$horaF=date("H:i:s",$hora);

				$hora_audi = date("H:i:s");

				$sql = "UPDATE puntos SET recover='$recover',fecha_re = '$fecha',hora_re = '$horaF'  WHERE idpos = $idPos";
				$res = $BD->consultar($sql);

				if(!$res){
					$error = 1;
				}

				// PARAMETROS DE ENVIO CORREO ELECTRONICO
				$headers = "Link de ingreso a la plataforma Pos"; //NOMBRE DEL REMITENTE
				$asunto = "Solicitud de restablecimiento de password."; // ASUNTO DEL MENSAJE


				$url_email .= "/index.php?rpss=$recover";

				$cuerpo="Estimado Usuario,<br><br>
				Hemos recibido una solicitud para restablecer su password de acceso.<br><br>
				<strong>Usuario:</strong>".$resul->fields["username"]."<br><br>
				Para asignar un nuevo password de acceso, por favor <a href='".$url_email."' target='_blank'> hacer click aqui.</a><br><br>
				Usted tiene hasta las: $horaF para realizar esta operacion.<br><br><strong>Nota:</strong> Esto es un mensaje automatico, favor no responder al mismo.
				<br><br>";

				if($error==1){
					$BD->consultar("ROLLBACK");
					$resp = 0;
				}elseif($error==0){
					$BD->consultar("COMMIT");

					if(correo($destinatario,$headers,"",$asunto,$cuerpo,'','','')){
						$resp = 1;
					}else{
						$resp = 0;
					}
				}

			}else{
				$resp = 2;
			}

			$BD->desconectar();

		}
		return $resp;
	}

	public function veririficar_cp($capcha){
   //$clavep = "6LdrcA4UAAAAAJSbENbIYNGIQtKRwu9H2cXb1sjU";
   $clavep = "6LeIxAcTAAAAAGG-vFI1TnRWxMZNFuojJ4WifJWe";
   if($capcha != "" ){
       $verify_response = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$clavep.'&response='.$capcha);
       $response_data = json_decode($verify_response);
       if(!$response_data->success){
          return array("estado" => -1,"mns"=>"El reCAPTCHA es invalido.");

       }else{
          return array("estado" => 1,"mns"=>"");
       }
   }else{
      return array("estado" => -1,"mns"=>"Seleccione el reCAPTCHA.");
   }
  }



}
?>