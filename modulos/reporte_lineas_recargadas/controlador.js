$(document).ready(function() {
	$('.fecha').datepicker({
		format: 'yyyy-mm-dd',
		todayHighlight: true,
		todayBtn: true
	});

	$("#frmConsulta").submit(function(event) {
	 	event.preventDefault();
	 	Cargar_Reporte();
    });

    
});

function Cargar_Reporte(){
	
	 f_ini = $("#f_ini").val();
	 f_fin = $("#f_fin").val();
	
   if ( ! $.fn.DataTable.isDataTable( '#t_reporte' ) ) {
	  dtable = $("#t_reporte").DataTable({
			"ajax": {
		    "url": "modulos/reporte_lineas_recargadas/controlador.php",
		    "type": "POST",
		    "deferRender": false,
		    "data":{accion:'Cargar_Informe',f_ini:f_ini, f_fin:f_fin}
		  },
		  "bFilter": false,
		  "dom": '<"top"fl>rt<"bottom"<"toolbar">p><"clear">',
		  "responsive":false,
		  "columns": [
            { "data": "fecha_v"},
			{ "data": "cant_lin"},
			{ "data": "cant_rec"},
			{ "data": "total"},
			{ "data": "fecha_v"}
        ],
         "columnDefs": [
			{
				"targets": 4,
				"data": "fecha_v",
				 render: function ( data, type, row ) {
					if (typeof(row[0]) !== "undefined") {
						cant_total = row[0];
						$("div.toolbar").html('Total Articulos: '+cant_total).addClass("text-left");
					}
					return '<i class="glyphicon glyphicon-plus-sign DetaRef" id = "id_ref_'+row.fecha_v+'" style="color: #10628A;font-size: 20px;cursor: pointer;position:relative;"></i>';
				 }
				 
			}
		 ],
	    fnDrawCallback: function () {
			
	    	$(".DetaRef").unbind("click");
	    	$(".DetaRef").click(function() {
				
	    		var data = dtable.row( $(this).parents('tr') ).data();
				var fecha_v = String(data['fecha_v']);
				var elemento = $(this);
				
				var tr = $(this).closest('tr');
				var row = dtable.row( tr );
				var tabla = "";
				
				if(elemento.hasClass( "DetaRef" )){
					
					elemento.attr("class","glyphicon glyphicon-minus-sign CloseDeta");

					$.post('modulos/reporte_lineas_recargadas/controlador.php',
						{
							accion: 'Consultar_Lineas',
							fecha_v:fecha_v
							
						},
						function(data, textStatus) {
							
							if(data != 0 || data != "")
							{	
							
								var datos = JSON.parse(data);
								if(datos != ""){
									tabla += '<table id="tblPed2" align="center" class="table dt-responsive nowrap" cellspacing="0" width="100%" style="border: 1px solid #DADADA;">';
									tabla += '<thead>';
									tabla += '<tr>';
									tabla += '<th align="center">Fecha Activación</th>';
									tabla += '<th align="center">Hora Activación</th>';
									tabla += '<th align="center">Serial</th>';
									tabla += '<th align="center">Línea</th>';
									tabla += '<th align="center">Cantidad Recargas</th>';
									tabla += '<th align="center">Total</th>';
									tabla += '<th align="center">Detalle</th>';
									tabla += '</tr>';
									tabla += '</thead>';
									$.each(datos,function(index, fila) {
										tabla += '<tr>';
										tabla += '<td >'+fila.fecha_v+'</td>';
										tabla += '<td >'+fila.hora_v+'</td>';
										tabla += '<td >'+fila.serie+'</td>';
										tabla += '<td>'+fila.linea+'</td>';
										tabla += '<td>'+fila.cant_rec+'</td>';
										tabla += '<td>'+fila.total+'</td>';
										var fecha = "'"+fecha_v+"'";
										var serial = "'"+fila.serie+"'";
										tabla += '<td><i class="glyphicon glyphicon-plus-sign" style="color: #10628A;font-size: 20px;cursor: pointer;" id = "boton_de'+fecha_v+'_'+fila.serie+'" class="viewDetaSerie" onclick = "chargeDetail('+fecha+','+serial+')"></i></td>';
										
										tabla += '</tr>';
										tabla += '<tr id = "verDeta_'+fecha_v+'_'+fila.serie+'" style="display:none" ><td colspan = "7" id = "posDeta_'+fecha_v+'_'+fila.serie+'" class = "desDetaSerie"></td></tr>';
									});
									
									tabla += '</table>';
									
								}
								
								row.child(tabla).show();
								tr.addClass('shown');
							}
						}
					);
				
				}else{
				
					elemento.attr("class","glyphicon glyphicon-plus-sign DetaRef");
					row.child.hide();
					tr.removeClass('shown');
				}
				
	    	});
			
	    }

	  });
	}else{
		dtable.destroy();
		Cargar_Reporte();
	}
	
	$("#CSV").unbind("click");
	$("#CSV").click(function() {
		Datos_CSV();
	});
	$("#consulta").show();
	
}


function chargeDetail(date,serie){
	var tabla = "";
	var elemento = $("#posDeta_"+date+"_"+serie);
	var icono = $("#boton_de"+date+"_"+serie);
	if(elemento.hasClass( "desDetaSerie" )){
		$("#verDeta_"+date+"_"+serie).show();
		icono.attr("class","glyphicon glyphicon-minus-sign");
		elemento.attr("class","CloseDetaSerie");
		elemento.show();
		$.post('modulos/reporte_lineas_recargadas/controlador.php',
			{
				accion: 'Consultar_Det_Recarga',
				fecha_v:date,
				serie:serie
				
			},
			function(data, textStatus) {
				if(data != 0 || data != "")
				{	
					var datos = JSON.parse(data);
					
					tabla += '<table id="tblPed2" align="center" class="table dt-responsive nowrap" cellspacing="0" width="100%" style="border: 1px solid #DADADA;">';
					tabla += '<thead>';
					tabla += '<tr>';
					tabla += '<th style="text-align: center;" width="25%">Fecha Recarga</th>';
					tabla += '<th style="text-align: center;" width="25%">Serial </th>';
					tabla += '<th style="text-align: center;" width="25%">Línea</th>';
					tabla += '<th style="text-align: center;" width="25%">Valor</th>';
					tabla += '</tr>';
					tabla += '</thead>';
					$.each(datos,function(index, fila) {
						tabla += '<tr>';
						tabla += '<td style="text-align: center;">'+fila.fecha_r+'</td>';
						tabla += '<td style="text-align: center;">'+fila.serie+'</td>';
						if(fila.nro_movil == 0){
							tabla += '<td style="text-align: center;">Sin Número</td>';
						}else{
							tabla += '<td style="text-align: center;">'+fila.movil+'</td>';
						}
						tabla += '<td style="text-align: center;">'+fila.valor+'</td>';
						
						tabla += '</tr>';
					});
					tabla += '</table>';
					elemento.html(tabla);
					
				}
			}
		);
		
	}else{
		$("#verDeta_"+date+"_"+serie).hide();
		icono.attr("class","glyphicon glyphicon-plus-sign");
		elemento.attr("class","desDetaSerie");
		elemento.hide();
		elemento.html("");
		
	}
	
}

function Datos_CSV() {
	
	f_ini = $("#f_ini").val();
	 f_fin = $("#f_fin").val();
	
	$("#CSV").unbind("click");
	$.post('modulos/reporte_lineas_recargadas/controlador.php',
	{
		accion: 'Datos_CSV',
		f_ini: f_ini,
		f_fin: f_fin
	},
	function(data, textStatus) {
		if(data != ""){
			data = JSON.parse(data);
			var header = "FECHA_ACTIVACION,SERIAL,NUMERO_LINEA,FECHA_RECARGA,VALOR_RECARGA,ASESOR,DISTRIBUIDOR";
			var columnas = "fecha_a,serie,movil,fecha_r,valor_r,asesor,distri";
			ExportarCSV(data, header,"Reporte Lineas Recargadas",columnas);
			
			$("#CSV").click(function() {
				Datos_CSV();
			});
		}
	});
	
}

	
	

