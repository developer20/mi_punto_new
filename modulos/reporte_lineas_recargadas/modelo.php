<?php
require("../../config/mainModel.php");
class reporte_lineas_recargadas{
	

   public function Cargar_Informe($f_ini,$f_fin)
   {
	   $oSession = unserialize($_SESSION[$GLOBALS["SESION_POS"]]);
	   $idpos=$oSession->VSidpos;
		
	   $array_response = array();
		
	   $BD=new BD();
	   $BD->conectar();
		
		$sql = "SELECT COUNT(s.id) cant_lin, s.fecha_ac fecha_v, COUNT(cr.NUM_ICC) cant_rec, SUM(cr.VAL_RECARGA) total FROM 
		{$GLOBALS["BD_NAME"]}.csv_recarga cr, {$GLOBALS["BD_NAME"]}.simcards s WHERE s.activo = 1 AND cr.NUM_ICC = s.iccid AND s.id_pos = '$idpos' AND s.fecha_ac BETWEEN '$f_ini' AND '$f_fin' GROUP BY s.fecha_ac";
		
		$res = $BD->devolver_array($sql);
		
		/*
		if($BD->numreg($res)>0){
			while(!$res ->EOF){
				$array_response[] = array("fecha_v" => $res->fields['fecha_v'], "cant_lin" => $res->fields['cant_lin'], "cant_rec" => $res->fields['cant_rec'], "total" => $res->fields['total']);
				$res ->MoveNext();	
			}
		}*/
			
		return $res;
		
   }
   
   public function Cargar_Informe_Lineas($fecha_v)
   {
	   $oSession = unserialize($_SESSION[$GLOBALS["SESION_POS"]]);
	   $idpos=$oSession->VSidpos;
		
	   $array_response = array();
		
	   $BD=new BD();
	   $BD->conectar();
		
		$sql = "SELECT CONCAT('89',s.iccid) serie, s.movil linea, s.fecha_ac fecha_v, s.hora_ac hora_v, COUNT(cr.NUM_ICC) cant_rec, SUM(cr.VAL_RECARGA) total FROM {$GLOBALS["BD_NAME"]}.csv_recarga cr, {$GLOBALS["BD_NAME"]}.simcards s WHERE s.activo = 1 AND cr.NUM_ICC = s.iccid AND s.estado = 1 AND s.id_pos = '$idpos' AND s.fecha_ac = '$fecha_v' GROUP BY s.fecha_ac,s.iccid";
		$res = $BD->devolver_array($sql);
		
		/*
		if($BD->numreg($res)>0){
				while(!$res ->EOF){
					
					$array_response[] = array('fecha_v' => $res->fields['fecha_v'], 'hora_v' => $res->fields['hora_v'], 'serie' => $res->fields['NUMERO_ICC'], 'linea' => $res->fields['CELULAR'], 'cant_rec' => $res->fields['cant_rec'], 'total' => $res->fields['total']);
					$res ->MoveNext();
				}
		}*/
			
		return $res;
		
   }
   
   public function Consultar_Det_Recarga($fecha_v,$serie)
   {
	   $oSession = unserialize($_SESSION[$GLOBALS["SESION_POS"]]);
	   $idpos=$oSession->VSidpos;
		
	   $array_response = array();
		
	   $BD=new BD();
	   $BD->conectar();
		
	    $sql = "SELECT CONCAT('89',s.iccid) serie, s.movil,cr.FEC_RECARGA fecha_r, cr.VAL_RECARGA valor FROM {$GLOBALS["BD_NAME"]}.csv_recarga cr, {$GLOBALS["BD_NAME"]}.simcards s WHERE cr.NUM_ICC = SUBSTRING('$serie',3) AND s.activo = 1 AND s.id_pos = '$idpos' AND s.fecha_ac = '$fecha_v' GROUP BY cr.id";
		$res = $BD->devolver_array($sql);
		
		/*
		if($BD->numreg($res)>0){
				while(!$res ->EOF){
					
					$array_response[] = array('fecha_r' => $res->fields['FEC_RECARGA'], 'serie' => $res->fields['NUMERO_ICC'], 'movil' => $res->fields['NUM_CELULAR'], 'valor' => $res->fields['VAL_RECARGA']);
					$res ->MoveNext();
				}
		}*/
			
		return $res;
		
   }
   
    public function Datos_CSV($f_ini,$f_fin)
   {
	    $oSession = unserialize($_SESSION[$GLOBALS["SESION_POS"]]);
	    $idpos=$oSession->VSidpos;

	    $cont1 = '="';
        $cont2 = '"';
		
	    $array_response = array();
		
	    $BD=new BD();
	    $BD->conectar();
		
		$sql = "SELECT CONCAT('$cont1','89',s.iccid,'$cont2') serie, d.nombre distri, cr.NUM_CELULAR movil, cr.FEC_RECARGA fecha_r, s.fecha_ac fecha_a, cr.VAL_RECARGA valor_r, usu.nombre_usuario asesor FROM {$GLOBALS["BD_NAME"]}.simcards s, {$GLOBALS["BD_NAME"]}.csv_recarga cr, {$GLOBALS["BD_NAME"]}.distribuidores d, {$GLOBALS["BD_NAME"]}.usuarios_distri usu WHERE s.activo = 1 AND cr.NUM_ICC = s.iccid AND s.id_pos = '$idpos' AND s.distri = d.id AND s.fecha_ac BETWEEN '$f_ini' AND '$f_fin' AND usu.id_usuario = s.id_vendedor AND usu.id_distri = s.distri";             
		$res = $BD->devolver_array($sql);
		/*
	    $con = 0;
		if($BD->numreg($res)>0){
				while(!$res ->EOF){
					$con++;
					$id_asesor = $res->fields['id_vendedor'];
					$id_distri = $res->fields['distri'];
					$db_corto = $res->fields['n_db'];
					$nm_db = 'distri_'.$db_corto.'_'.$id_distri;
					$db = $nm_db.'.usuarios';
					
					$sq2 = "SELECT CONCAT(nombre,' ',apellido) nom_as FROM $db WHERE id = '$id_asesor' ";
					$r2 = $BD->consultar($sq2);
					$nom_as = $r2->fields['nom_as'];
					$array_response[] = array('fecha_a' => $res->fields['FECHA_VENTA'], 'serie' => $res->fields['serie'], 'movil' => $res->fields['NUM_CELULAR'], 'fecha_r' => $res->fields['FEC_RECARGA'], 'valor_r' => $res->fields['VAL_RECARGA'], 'asesor' => $nom_as, 'distri' => $res->fields['nombre']);
					$res ->MoveNext();
				}
				return $array_response;
		}else{
			return "";	
		}*/

		return $res;
		
   }
   
}// Fin clase
?>