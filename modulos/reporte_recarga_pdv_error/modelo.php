<?php
require("../../config/mainModel.php");
class reporte_recarga_pdv {
/*
  METODOS DE LA BD.
  $this->BD->consultar($query); // Ejecuta la consulta y devuelve string.!
  $this->BD->devolver_array($query); // Ejecuta la consulta y devuelve array asociativo.!
  $this->BD->consultar("BEGIN"); // Antes de transacciones.!
  $this->BD->consultar("COMMIT"); // Commit para guardar datos.!
  $this->BD->consultar("ROLLBACK"); // Devolver datos si hay error.!
  $this->BD->numreg($query); // Devuelve el numero de registros de la consulta.!
*/

/*
VARIABLES GLOBALES SISTEMA
$GLOBALS["BD_NAME"];
$GLOBALS["SERVER"];
$GLOBALS["SERVER_USER"];
$GLOBALS["SERVER_PASS"];
$GLOBALS["BD_DIS"];
$GLOBALS["BD_POS"];
$GLOBALS["SESION_OP"];
$GLOBALS["SESION_DIS"];

*/
	public function __construct()
    {
      $BD=new BD();
      $this->BD = $BD;
      $this->BD->conectar();
      $oSession = unserialize($_SESSION[$GLOBALS["SESION_POS"]]);
      $this->idpos=$oSession->VSidpos;
    }
    public function __destruct()
    {
      $this->BD->desconectar();
    }


  function cargar_operadores(){
    $c_operador = "SELECT id,descripcion FROM operadores WHERE estado = 1;";
    return $this->BD->devolver_array($c_operador);
  }

  function consulta_recarga($f_ini,$f_fin,$operador,$movil){
      $condicion = "";

      if($operador != ""){
        $condicion .= " AND vs.operador = $operador ";
      }

      if($movil != ""){
        $condicion .= " AND vs.movil = $movil ";
      }

      $c_recarga_pdv = "SELECT vs.fecha, SUM(IF(vs.estado != 1,1,0)) AS cant_errores
                        FROM venta__servicios vs
                        WHERE vs.id_punto = $this->idpos AND vs.fecha >= '$f_ini' AND vs.fecha <= '$f_fin' AND vs.estado != 1  $condicion
                        GROUP BY vs.fecha";

      $r_recarga_pdv = $this->BD->devolver_array($c_recarga_pdv);

      return $r_recarga_pdv;

  }

  function listar_detalle_rec($fecha,$operador,$movil){

      $condicion = "";

      if($operador != ""){
        $condicion .= " AND vs.operador = $operador ";
      }

      if($movil != ""){
        $condicion .= " AND vs.movil = $movil ";
      }

      $c_recarga_pdv_final = "SELECT vs.horaregistro,vs.movil,op.descripcion as operador,vs.code_error,vs.mens_error,'' as mns_error,vs.operador as op_id,pro.nombre AS proveedor,IF(vs.acceso = 0,'Web',IF(vs.acceso = 1,'App','Indefinido')) AS acceso
                              FROM venta__servicios vs
                              INNER JOIN operadores op ON (op.id = vs.operador)
                              INNER JOIN proveedores AS pro ON (pro.id = vs.id_proveedor)
                              WHERE vs.id_punto =  $this->idpos AND vs.fecha = '$fecha'  AND vs.estado != 1 $condicion";

      $r_recarga_pdv_final = $this->BD->devolver_array($c_recarga_pdv_final);

      foreach ($r_recarga_pdv_final as $key => $value) {

       $array_error = explode("ERROR:",$value["mens_error"]);
        if(isset($array_error[1])){
          $sub_cod = str_replace("[","",$array_error[1]);
          $sub_cod = str_replace("]","",$sub_cod);
          $sub_cod = str_replace(" ","",$sub_cod);
          if(is_numeric($sub_cod)){
            $c_error_mns = "SELECT msg FROM {$GLOBALS["BD_POS"]}.venta__msg_errores WHERE codigo = {$value["code_error"]} AND sub_codigo = {$sub_cod} AND id_operador = {$value["op_id"]};";
            $r_error_mns = $this->BD->devolver_array($c_error_mns);
            if(count($r_error_mns)>0){
              $r_recarga_pdv_final[$key]["mns_error"] = $r_error_mns[0]["msg"];
            }else{
              $c_error_mns = "SELECT msg FROM {$GLOBALS["BD_POS"]}.venta__msg_errores WHERE codigo = {$value["code_error"]};";
              $r_error_mns = $this->BD->devolver_array($c_error_mns);
              if(count($r_error_mns)>0){
                $r_recarga_pdv_final[$key]["mns_error"] = $r_error_mns[0]["msg"];
              }
            }
          }else{
            $c_error_mns = "SELECT msg FROM {$GLOBALS["BD_POS"]}.venta__msg_errores WHERE codigo = {$value["code_error"]};";
            $r_error_mns = $this->BD->devolver_array($c_error_mns);
            if(count($r_error_mns)>0){
              $r_recarga_pdv_final[$key]["mns_error"] = $r_error_mns[0]["msg"];
            }

          }
        }else{
          if($value["code_error"] != null){
              $c_error_mns = "SELECT msg FROM {$GLOBALS["BD_POS"]}.venta__msg_errores WHERE codigo = {$value["code_error"]};";
              $r_error_mns = $this->BD->devolver_array($c_error_mns);
              if(count($r_error_mns)>0){
                 $r_recarga_pdv_final[$key]["mns_error"] = $r_error_mns[0]["msg"];
              }else{
                $r_recarga_pdv_final[$key]["mns_error"] = "N/A";
              }
          }else{
            $r_recarga_pdv_final[$key]["mns_error"] = "N/A";
          }

        }

      }

      return $r_recarga_pdv_final;
  }

  function getCsvData($f_ini,$f_fin,$operador,$movil){

      $condicion = "";

      if($operador != ""){
        $condicion .= " AND vs.operador = $operador ";
      }

      if($movil != ""){
        $condicion .= " AND vs.movil = $movil ";
      }

      $c_recarga_pdv_final = "SELECT vs.fecha,vs.horaregistro,vs.movil,op.descripcion as operador,vs.code_error,vs.mens_error,'' as mns_error,vs.operador as op_id,pro.nombre AS proveedor,IF(vs.acceso = 0,'Web',IF(vs.acceso = 1,'App','Indefinido')) AS acceso
                              FROM venta__servicios vs
                              INNER JOIN operadores op ON (op.id = vs.operador)
                              INNER JOIN proveedores AS pro ON (pro.id = vs.id_proveedor)
                              WHERE vs.id_punto =  $this->idpos AND vs.fecha >= '$f_ini' AND vs.fecha <= '$f_fin' AND vs.estado != 1 $condicion";

      $r_recarga_pdv_final = $this->BD->devolver_array($c_recarga_pdv_final);

      foreach ($r_recarga_pdv_final as $key => $value) {

       $array_error = explode("ERROR:",$value["mens_error"]);
        if(isset($array_error[1])){
          $sub_cod = str_replace("[","",$array_error[1]);
          $sub_cod = str_replace("]","",$sub_cod);
          $sub_cod = str_replace(" ","",$sub_cod);
          if(is_numeric($sub_cod)){
            $c_error_mns = "SELECT msg FROM {$GLOBALS["BD_POS"]}.venta__msg_errores WHERE codigo = {$value["code_error"]} AND sub_codigo = {$sub_cod} AND id_operador = {$value["op_id"]};";
            $r_error_mns = $this->BD->devolver_array($c_error_mns);
            if(count($r_error_mns)>0){
              $r_recarga_pdv_final[$key]["mns_error"] = $r_error_mns[0]["msg"];
            }else{
              $c_error_mns = "SELECT msg FROM {$GLOBALS["BD_POS"]}.venta__msg_errores WHERE codigo = {$value["code_error"]};";
              $r_error_mns = $this->BD->devolver_array($c_error_mns);
              if(count($r_error_mns)>0){
                $r_recarga_pdv_final[$key]["mns_error"] = $r_error_mns[0]["msg"];
              }
            }
          }else{
            $c_error_mns = "SELECT msg FROM {$GLOBALS["BD_POS"]}.venta__msg_errores WHERE codigo = {$value["code_error"]};";
            $r_error_mns = $this->BD->devolver_array($c_error_mns);
            if(count($r_error_mns)>0){
              $r_recarga_pdv_final[$key]["mns_error"] = $r_error_mns[0]["msg"];
            }
          }
        }else{
          if($value["code_error"] != null){
              $c_error_mns = "SELECT msg FROM {$GLOBALS["BD_POS"]}.venta__msg_errores WHERE codigo = {$value["code_error"]};";
              $r_error_mns = $this->BD->devolver_array($c_error_mns);
              if(count($r_error_mns)>0){
                 $r_recarga_pdv_final[$key]["mns_error"] = $r_error_mns[0]["msg"];
              }else{
                $r_recarga_pdv_final[$key]["mns_error"] = "N/A";
              }
          }else{
            $r_recarga_pdv_final[$key]["mns_error"] = "N/A";
          }

        }

      }

      return $r_recarga_pdv_final;

  }


}// Fin clase
?>