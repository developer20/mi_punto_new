var f_ini;
var f_fin;
var idpos;
var distri;
var regional;
var territorio;
var zona;
var operador;
var dtable;
$(document).ready(function() {
	cargar_operadores();
	$('.fecha').datepicker({
		format: 'yyyy-mm-dd',
		todayHighlight: true,
		todayBtn: true
	});
	$("#reg").change(function(e){
		if($(this).val() != ""){
			cargar_territorio($(this).val());
		}else{
			$("#ter").html("<option value=''>Seleccione..</option>").change();
		}
	});

	$("#ter").change(function(e){
		if($(this).val()!=""){
			cargar_zona($(this).val());
		}else{
			$("#rut").html("<option value=''>Seleccione..</option>").change();
		}
	});

	$("#btn_buscar").click(function(){
		Retornar_Informacion("tbl_reporte");
	});

	$("#CSV").click(function(e){
		getCsvData();
	});
});


function cargar_operadores(){

	$.post('modulos/reporte_recarga_pdv_error/controlador.php',
	{
	  	accion:'cargar_operadores'
	},
	function(data, textStatus) {
		 var html_select = "<option value=''>Seleccione..</option>";
		 data = JSON.parse(data);
		 $.each(data,function(index,row){
		 	html_select += "<option value="+row.id+" >"+row.descripcion+"</option>";
		 });
		 $("#operador").html(html_select).change();
	});
}

function Retornar_Informacion(id) {
	f_ini = $("#f_ini").val();
	f_fin = $("#f_fin").val();
	movil = $("#movil").val();
	operador = $("#operador").val();
	if(f_ini != "" && f_fin != ""){
				if ($.fn.DataTable.isDataTable('#'+id))
				{
					dtable.destroy();
				}

				dtable = $('#'+id).DataTable({
					"ajax": {
						"url": "modulos/reporte_recarga_pdv_error/controlador.php",
						"type": "POST",
						"deferRender": false,
						"data":{
									accion:'consulta_recarga',
									f_ini:f_ini,
									f_fin:f_fin,
									movil:movil,
									operador:operador
								}
					},
					  "bFilter": false,
					  "responsive":false,
					  "columns":[
			  				{ "data": "fecha"},
			  				{ "data": "cant_errores"},
			  				{ "data": "fecha"}
						],
						"columnDefs": [
							{
								"targets": 2,
								"data": "fecha",
								 render: function ( data, type, row ) {
								 	return "<i style='cursor:pointer; font-size:19px;' data-fecha='"+data+"' data-movil='"+movil+"' data-operador='"+operador+"' class='glyphicon glyphicon-plus-sign detalle_rec_f text-primary'></i>";
								 }
							}
						],
					    fnDrawCallback: function () {

					    	$(".detalle_rec_f").unbind("click");
					    	$(".detalle_rec_f").click(function() {
					    		var tr = $(this).parent().parent();
								var fecha = $(this).attr("data-fecha");
								var movil = $(this).attr("data-movil");
								var operador = $(this).attr("data-operador");
								var tab;

								tab = $("body").find("#fecr"+fecha);

								if(!$.contains(window.document, tab[0]))
						       	{
						          $( "<tr id='fecr"+fecha+"'><td colspan='3'></td></tr>" ).insertAfter($(tr));
								  $(this).removeClass( 'glyphicon-plus-sign' );
				                  $(this).addClass( 'glyphicon-minus-sign' );
				                 listar_detalle_rec(fecha,operador,movil);
								}
								else
								{
								  $("#fecr"+fecha).remove();
								  $(this).addClass( 'glyphicon-plus-sign' );
				                  $(this).removeClass('glyphicon-minus-sign');
								}


					    	});

					    }
				});

				$("#consulta").show();
	}else{
		Notificacion("La fecha inicial y la fecha final son obligatorias.")
	}
}

function listar_detalle_rec(fecha,operador,movil){

	$.post('modulos/reporte_recarga_pdv_error/controlador.php',
		{
			accion: 'listar_detalle_rec',
			fecha:fecha,
			operador:operador,
			movil:movil
		},

		function(data, textStatus) {

			if(data != "" || data != 0){
				var reporte='';
				var data = JSON.parse(data);
				reporte += "<div class='col-md-12' style='border:2px #ccc solid; border-radius:2px;'>";
				reporte += "<table class='table table-hovered table-condensed dt-responsive nowrap'>";
				reporte += "<thead><tr><th>HORA</th><th>MOVIL</th><th>OPERADOR</th></th><th>PROVEEDOR</th></th><th>ACCESO</th><th>ERROR</th></thead><tbody>";
			 	$.each(data,function(index,row) {
			 		reporte += "<tr>";
			 		reporte += "<td>"+row.horaregistro+"</td>";
			 		reporte += "<td>"+row.movil+"</td>";
			 		reporte += "<td>"+row.operador+"</td>";
			 		reporte += "<td>"+row.proveedor+"</td>";
			 		reporte += "<td>"+row.acceso+"</td>";
			 		reporte += "<td>"+row.mns_error+"</td>";
			 		reporte += "</tr>";
			 	})
			 	reporte +="</tbody></table></div>";

			 	$("#fecr"+fecha).find("td").html(reporte);
			}

		}
	);
}

function getCsvData(){

	f_ini = $("#f_ini").val();
	f_fin = $("#f_fin").val();
	movil = $("#movil").val();
	operador = $("#operador").val();

	if(f_ini != "" && f_fin != ""){
		$.post('modulos/reporte_recarga_pdv_error/controlador.php',
			{
				accion: 'getCsvData',
				f_ini:f_ini,
				f_fin: f_fin,
				movil: movil,
				operador: operador
			},
			function(data, textStatus) {
				if(data != "" || data != 0){
					data = JSON.parse(data);
					var columnas = "Fecha,Hora,Operador,Proveedor,Acceso,Movil,Error";
					var cols = "fecha,horaregistro,operador,proveedor,acceso,movil,mns_error";
					ExportarCSV(data,columnas,"Reporte_recargas_pdv_error",cols);
				}
			}
		);
	}else{
		Notificacion("La fecha inicial y la fecha final son obligatorias.","error");
	}

}

