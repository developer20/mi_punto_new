<?php
require("../../config/mainModel.php");
class val_permiso {
    //Espacio Para declara las funciones que retornan los datos de la DB.

  public function consultar_noti()
  {
      
      $BD=new BD();
      $BD->conectar();

      if(isset($_SESSION[$GLOBALS["SESION_POS"]])){

		$oSession = unserialize($_SESSION[$GLOBALS["SESION_POS"]]);
		$idpos=$oSession->VSidpos;
		$token=$oSession->VStoken;

		$sql = "SELECT zona,territorio,id_regional,id_distri FROM puntos WHERE idpos = $idpos";
		$resPos = $BD->consultar($sql);

		if(count($resPos)>0){

			$permZonas = $resPos->fields["zona"];
		    $permTerri = $resPos->fields["territorio"];
		    $permReg = $resPos->fields["id_regional"];
		    $id_distri = $resPos->fields["id_distri"];

			$sql = "SELECT token FROM token_puntos WHERE id_pos = $idpos";
			$res_exits_to = $BD->consultar($sql);

			if($BD->numreg($res_exits_to) > 0){

				if(strcmp($token,$res_exits_to->fields["token"]) == 0){

					$sql = "SELECT COUNT(DISTINCT ndn.id) as cant_noticias
		                FROM {$GLOBALS["BD_NAME"]}.noticias_promociones AS ndn 
		                INNER JOIN {$GLOBALS["BD_NAME"]}.niveles_dcs_noticias AS np ON (np.id_noticia = ndn.id)
		                WHERE np.nivel=4 AND ndn.tipo in(1,2) AND ndn.estado=1 AND ndn.vigente=1 AND np.web = 1
		                  AND ((np.tipo=1 and np.id_tipo IN ($permReg))
		                  OR(np.tipo=2 and np.id_tipo = $id_distri)
		                  OR(np.tipo=3 and np.id_tipo IN ($permTerri))
		                          OR(np.tipo=4 and np.id_tipo IN ($permZonas))) AND
		                  ndn.id NOT IN (SELECT nl.id_noticia FROM {$GLOBALS["BD_NAME"]}.noticias_leidas nl WHERE id_pos = $idpos AND (nl.leido = 1 AND ndn.estado_mo = 0 || ndn.estado_mo = 1 AND nl.leido_mo = 1) GROUP BY id_noticia)";

					$res_noti = $BD->devolver_array($sql);

					$sql = "SELECT COUNT(DISTINCT enc.id) as cant_encuesta
		                FROM {$GLOBALS["BD_NAME"]}.enc__encuesta as enc
		                INNER JOIN {$GLOBALS["BD_NAME"]}.enc__niveles_dcs AS dcs ON (dcs.id_encuesta=enc.id)
		                WHERE enc.estado=1 AND enc.vigente=1 AND dcs.nivel=6 AND dcs.web = 0
		                AND ((dcs.tipo=1 and dcs.id_tipo IN ($permReg))
		                    OR(dcs.tipo=2 and dcs.id_tipo = $id_distri)
		                    OR(dcs.tipo=3 and dcs.id_tipo IN ($permTerri))
		                    OR(dcs.tipo=4 and dcs.id_tipo IN ($permZonas)))
		                    AND enc.id NOT IN (SELECT id_encuesta FROM {$GLOBALS["BD_NAME"]}.enc__respuestas_encuesta WHERE id_pos = $idpos AND id_encuesta = enc.id GROUP BY id_encuesta)";

					$res_enc = $BD->devolver_array($sql);

					return array("estado"=>1,"msg"=>"","cant_enc" => $res_enc[0]["cant_encuesta"], "cant_noti" => $res_noti[0]["cant_noticias"]);
				
				}else{
			       return array("estado"=>0,"msg"=>"No es posible que puedas realizar ninguna acción porque has iniciado sesión en otro dispositivo","cant_enc" => 0, "cant_noti" => 0);
			    }

			}else{
		       return array("estado"=>0,"msg"=>"No cuentas con permiso para ejecutar ninguna acción","cant_enc" => 0, "cant_noti" => 0);
		    }

		}else{
	       return array("estado"=>0,"msg"=>"No cuentas con permiso para ejecutar ninguna acción","cant_enc" => 0, "cant_noti" => 0);
	    }

      }else{
      	return array("estado"=>0,"msg"=>"Tu sesión ha cerrado, por favor inicia sesión nuevamente y vuelve a intentarlo","cant_enc" => 0, "cant_noti" => 0);
      }

  }

  public function consultar_noti2()
  {
      
      $BD=new BD();
      $BD->conectar();

      if(isset($_SESSION[$GLOBALS["SESION_POS"]])){

	      $oSession = unserialize($_SESSION[$GLOBALS["SESION_POS"]]);
	      $id_user=$oSession->VSid;
	      $id_distri=$oSession->VSidDistri;

	      $sql = "SELECT COUNT(id) cant_noticias FROM {$GLOBALS["BD_NAME"]}.notificacion__push WHERE id_destino = $id_user AND id_distri = $id_distri AND (estado = 0 OR estado = 2) AND success = 1 AND fecha_leido IS NULL AND TIMESTAMPDIFF(DAY,fecha_envio,CURDATE()) <= 3";
	      
	      $res_noti = $BD->devolver_array($sql);

	      $sql = "SELECT id,IF(LENGTH(titulo) > 30,CONCAT(SUBSTRING(titulo,1,25),'...'),titulo) titulo,msg,estado,fecha_envio,hora_envio,titulo titulo_d FROM {$GLOBALS["BD_NAME"]}.notificacion__push WHERE id_destino = $id_user AND id_distri = $id_distri AND success = 1 AND TIMESTAMPDIFF(DAY,fecha_envio,CURDATE()) <= 3 ORDER BY id desc";
	      
	      $carga_noti = $BD->devolver_array($sql);

	      return array("cant_noti" => $res_noti[0]["cant_noticias"],"carga_noti" => $carga_noti);

      }else{
      	return array();
      }

  }

  public function leer_noti($id_notif)
  {
      
      $BD=new BD();
      $BD->conectar();

      if(isset($_SESSION[$GLOBALS["SESION_POS"]])){

	      $oSession = unserialize($_SESSION[$GLOBALS["SESION_POS"]]);
	      $id_user=$oSession->VSid;
	      $id_distri=$oSession->VSidDistri;

	      $sql_notif = "";

	      if($id_notif > 0){
	      	$sql_notif = "AND id = $id_notif";
	      }

	      
	      $sql = "UPDATE {$GLOBALS["BD_NAME"]}.notificacion__push SET estado = 1,fecha_leido = CURDATE(), hora_leido = CURTIME() WHERE id_destino = $id_user AND id_distri = $id_distri AND (estado = 0 OR estado = 2) AND success = 1 AND fecha_leido IS NULL $sql_notif ";
	      $res_noti = $BD->consultar($sql);

	      if($BD->numupdate() == 0){
	      	 return 0;
	      }
	      else if($id_notif > 0){
	      	 return $id_notif;
	      }else{
	      	 return 1;
	      }

      }else{
      	return array();
      }

  }
	

}

?>