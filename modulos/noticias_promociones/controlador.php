<?php
include_once("../../config/mainController.php"); // Incluye el Controlador Principal
include_once("../../modulos/noticias_promociones/modelo.php");
$controller = new mainController;
$modelo = new principal(); 

try // Try, manejo de Errores
{
	$metodo = $_SERVER['REQUEST_METHOD'];
	$tipo_res = "";
	$response = null; 
	
    $variables = $_POST;
	if(!isset($_POST['accion'])){echo "0"; return;}
	$accion = $variables['accion'];
	
		switch($accion) {
			case 'noticias':
				$tipo_res = 'JSON'; //Definir tipo de respuesta;
				$parametro = $variables['param'];
				$response = $modelo->noticias($parametro);
			break;
			case 'detalles_noticias':
				$tipo_res = 'JSON'; //Definir tipo de respuesta;
				$idNoticia = $variables['id'];
				$response = $modelo->detalles_noticias($idNoticia);
			break;	
			case 'Cargar_Banner_Mbox':		 
				 $tipo_res = 'JSON'; //Definir tipo de respuesta;
				 $response = $modelo->Cargar_Banner_Mbox(); 
			break;
		}

	// Respuestas del Controlador
	if($tipo_res == "JSON")
	{
	  echo json_encode($response,true); 
	}
	elseif ($tipo_res == "HTML") {
	  echo $response; 
	}
} // Fin Try
catch (Exception $e) {}
