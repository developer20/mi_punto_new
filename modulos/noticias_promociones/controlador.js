var url_ar = "http://prueba.movilbox.net:88/movistar_colombia/files_noti";

$(document).ready(function() {
	// l = Ladda.create(document.querySelector('button[type=submit]'));
	mostrar_noticias();

	$('#form_busqueda').submit(function(event){
		event.preventDefault();
		mostrar_noticias();
	});

});

function mostrar_noticias(){
	
	var Dhtml = ''; 
	$.post('modulos/noticias_promociones/controlador.php',
	{
		accion: 'noticias',
		param : $("#param_busq").val()
	},
	function(data, textStatus) {
		
		data = JSON.parse(data);

		if(data.length>0)
		{
			$('.bsqSearch').css({'visibility':'visible'});
			$('.section-noticias').css({'visibility':'visible'});
			$('.contents-nav').css({'visibility':'visible'});
		}
			
			$.each(data,function(index, fila) 
			{
				
				Dhtml += '<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 div-noticia">';				
				Dhtml += '<div class="panel panel-primary" style="-webkit-box-shadow: 0 1px 4px 0 rgba(0,0,0,0.14);box-shadow: 0 1px 4px 0 rgba(0,0,0,0.14);border-color: rgb(226, 226, 226);">';

				if(fila.tipo == 1)
				{
					tipo = "Noticia";
				}
				else
				{
					tipo = "Promoción";
				}

				var opac = "";
				if(fila.leido==0 || fila.leido == null){
			      opac = "opacity:0.2; float: right;";
			    }
			    else if(fila.leido==0 && fila.leido_mo==0 && fila.estado_mo==0){
			    	opac = "opacity:0.2; float: right;";
			    }
			    else if(fila.leido==1 && fila.leido_mo==0 && fila.estado_mo==0){
			    	opac = "color: lightgreen; opacity:1; float: right;";
			    }
			    else if(fila.leido==1 && fila.leido_mo==0 && fila.estado_mo==1){
			    	opac = "opacity:0.2; float: right;";
			    }
			    else if(fila.leido==1 && fila.leido_mo==1 && fila.estado_mo==1){
			    	opac = "color: lightgreen; opacity:1; float: right;";
			    }else{
			    	opac = "color: lightgreen; opacity:1; float: right;";
			    }

			  	Dhtml += '<div class="panel-heading"><div style="text-align:right;font-size: 11px;font-weight: bold;">'+tipo+' <i class="fa fa-check-circle" id="check_'+fila.id+'" style="'+opac+'position: relative;left: 5px;top: 2px;"></i></div><div>'+limitar_caracter(hex2a(fila.titulo),30) + '</div></div>';
				Dhtml += '<div class="panel-body">';
			  	Dhtml += '<div class="row" style="min-height: 100px; max-height: 150px;word-wrap: break-word;">';
			    
			    if(fila.ruta_img != ''){
				  	Dhtml+='		  	<div class="col-md-12" style="padding-right: 0;padding-left: 0;max-height: 200px;overflow-y: hidden;background-color: #f7f7f7;">';
					Dhtml+='	    		<img src="'+url_ar+'/imgs/'+fila.ruta_img+'" alt="Noticias" class="img-responsive" style="margin: 0 auto;width: 100%;"/>';
					Dhtml+='		  	<div class="col-md-12" style="position:absolute;bottom:0;padding: 8px;background: rgba(0, 0, 0, 0.50);color: #fff;font-weight: bold;">';
										if(fila.descripcion_texto!=undefined)
									  	{
								  			Dhtml +=  limitar_caracter(hex2a(fila.descripcion_texto),300);
									  	}
									  	else
									  	{
									  		Dhtml +=  "Sin descripcion...";
									  	}
					Dhtml+='	    	</div>';
					Dhtml+='	    	</div>';
			    }else{
			    	Dhtml+='		  	<div class="col-md-12" style="height: 200px;overflow-y: hidden;">';
										if(fila.descripcion_texto!=undefined)
									  	{
								  			Dhtml +=  limitar_caracter(hex2a(fila.descripcion_texto),300);
									  	}
									  	else
									  	{
									  		Dhtml +=  "Sin descripcion...";
									  	}
					Dhtml+='	    	</div>';
			    }

				/*
		    	Dhtml += '<div class="col-md-12">';
			  	
			  	if(fila.descripcion_texto!=undefined)
			  	{
		  			Dhtml +=  limitar_caracter(hex2a(fila.descripcion_texto),400);
			  	}
			  	else
			  	{
			  		Dhtml +=  limitar_caracter("Sin descripcion...",400);
			  	}
			  	Dhtml +='</div>';*/
			  	Dhtml +='</div>';	
				Dhtml +='<div class="row pie_noti">';

				Dhtml +='<div class="col-lg-12 text-left" style="margin: 3px;position: relative;top: 5px;">';
				Dhtml +='<div style="text-align: right;font-size: 11px;color:#6d6d6d;">' + fila.fecha + '</div>';
				Dhtml +='<div style="overflow-x:auto">';
				var palabra_clave = fila.palabras_claves.split(",");
		        for(i = 0; i < palabra_clave.length; i++){
		          Dhtml +='<span class="label azul" style="margin: 1px;">'+palabra_clave[i]+'</span>';
		        }
				
				Dhtml +='</div>';	
		  		Dhtml +='</div>';
		  		
		  		Dhtml +='<br><div class="col-lg-12">';
		  			/*
	  				Dhtml +='<span style="float: left;position: relative;top: 20px;">';
			  		Dhtml +='<i class="fa fa-check-circle" id="check_'+fila.id+'" style="'+opac+'"></i>';
			  		Dhtml +='</span>';*/
					Dhtml +='<span style="float: right;">';
						Dhtml +='<button class="btn btn-primary verMas" title = "Ver detalle" data-toggle="modal" data-id="'+fila.id+'" data-target="#MasNoticia"><i class="glyphicon glyphicon-fullscreen"></i></button> ';

						if(fila.ruta_file!="")
						{
							Dhtml +='<button class="btn btn-success verPdf" title = "Ver Archivo" data-toggle="modal" data-id="'+fila.id+'" data-target="#modalPdf"><i class="fa fa-file-pdf-o"></i></button> ';
						}

						if(fila.url!="")
						{
							Dhtml +='<a href="'+fila.url+'" target="_blank" title = "Ver '+ tipo +'" class="btn btn-warning"><i class="fa fa-external-link"></i></a>';

						}
					Dhtml +='</span>';

				Dhtml +='</div>';
				Dhtml +='</div>';
				Dhtml +='</div>';
				Dhtml +='</div>';
				Dhtml +='</div>';

			});

			$("#content-noticia").html(Dhtml + '<div style="clear:both;"></div>');

			// Paginacion de noticias

				pageSize = 4;

				var limite = $(".div-noticia").length;

				if(limite < 100)
			    	pagesCount = limite;
				else
					pagesCount = 100;

			    var currentPage = 1;
			    
			    /////////// PREPARE NAV ///////////////
			    var nav = '';
			    $(".numeros").remove();
			    var totalPages = Math.ceil(pagesCount / pageSize);
			    for (var s=0; s<totalPages; s++){
			        nav += '<li class="numeros"><a href="#">'+(s+1)+'</a></li>';
			    }
			    $(".pag_prev").after(nav);
			    $(".numeros").first().addClass("active");
			    //////////////////////////////////////

			    showPage = function() {
			        $(".div-noticia").hide().each(function(n) {
			            if (n >= pageSize * (currentPage - 1) && n < pageSize * currentPage)
			                $(this).show();
			        });
			    }
			    showPage();


			    $(".pagination li.numeros").click(function() {
			        $(".pagination li").removeClass("active");
			        $(this).addClass("active");
			        currentPage = parseInt($(this).text());
			        showPage();
			    });

			    $(".pagination li.pag_prev").click(function()
			    {
			        if($(this).next().is('.active')) return;
			        $('.numeros.active').removeClass('active').prev().addClass('active');
			        currentPage = currentPage > 1 ? (currentPage-1) : 1;
			        showPage();
			    });

			    $(".pagination li.pag_next").click(function() 
			    {
			        if($(this).prev().is('.active')) return;
			        $('.numeros.active').removeClass('active').next().addClass('active');
			        currentPage = currentPage < totalPages ? (currentPage+1) : totalPages;
			        showPage();
			    });

				// Fin de paginacion noticias

			$('.verPdf').unbind('click');
			$('.verPdf').on('click',function(){
				var Noti = $(this).attr('data-id');
				
				$.ajax({
					url: "modulos/noticias_promociones/controlador.php",
					type: "POST",
					dataType: "JSON",
					data: {
						accion: "detalles_noticias",
						id: Noti
					},
					success: function(data){
						
						var file = data[0]['ruta_file'];
						var titulo = hex2a(data[0]['titulo']);
						$('#miTitlePdf').html(titulo);
						$('#filePdf').attr("data",url_ar+"/files/"+file);

					}
				});
				
			});

			$('.verMas').unbind('click');
			$('.verMas').on('click',function(){
				var Noti = $(this).attr('data-id');
				
				$.ajax({
					url: "modulos/noticias_promociones/controlador.php",
					type: "POST",
					dataType: "JSON",
					data: {
						accion: "detalles_noticias",
						id: Noti
					},
					success: function(data){
						var tipo = data[0]['tipo'];
						var tipoN = "";

						if(tipo == 1)
						{
							tipoN = "Noticia";
						}
						else
						{
							tipoN = "Promoción";
						}

						var titulo = hex2a(data[0]['titulo']);
						var imagen = data[0]['ruta_img'];
						var descripcion = hex2a(data[0]['descripcion']);
						var url = data[0]['url'];
						var file = data[0]['ruta_file'];

						if(imagen != "")
						{
							$('#imgNoti').attr("src",url_ar+"/imgs/"+imagen);
							$('#imgNoti').show();
						}
						else
						{
							$('#imgNoti').hide();
						}

						$('h4#myTituloNoticia').html("<strong>" + tipoN + "</strong> - " + titulo);
						$('div#myDescripcionNoticia').html(descripcion);

						notificaciones();

					}
				});
				$('#check_'+Noti).attr('style','');
				$('#check_'+Noti).attr('style','color: lightgreen; opacity:1; float: right;position: relative;left: 5px;top: 2px;');
			});
		    
			$('#MasNoticia').on('hidden.bs.modal', function (e) {
			  mostrar_noticias();
			});
		
	});
	
}

function limitar_caracter(string, lengCaracter){
	var descripcion = string;
	if(string.length > lengCaracter){
		descripcion = string.substr(0,lengCaracter)+' ...';
	}
	return descripcion;
}