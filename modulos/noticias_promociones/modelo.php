<?php
require("../../config/mainModel.php");
class principal {
    //Espacio Para declara las funciones que retornan los datos de la DB.
    public function __construct()
  	{
  		$BD=new BD();
  		$this->DB = $BD;
  		$this->DB->conectar();
  	}
    public function __destruct()
    {
    	$this->DB->desconectar();
    }
public function Cargar_Banner_Mbox(){
    $oSession = unserialize($_SESSION[$GLOBALS["SESION_POS"]]);
      $idpos=$oSession->VSidpos;
      $pZdcs = 0;
      $pTdcs = 0;
      $pRdcs = 0;
      $IdDistri = 0;

      $sql = "SELECT zona,territorio,id_regional,id_distri FROM puntos WHERE idpos = $idpos";
      $resPos = $this->DB->consultar($sql);

      if(count($resPos)>0){
        $pZdcs = $resPos->fields["zona"];
        $pTdcs = $resPos->fields["territorio"];
        $pRdcs = $resPos->fields["id_regional"];
        $IdDistri = $resPos->fields["id_distri"];
      }

      if(strlen($pRdcs)>0)
        $pR = $pRdcs;
      else
        $pR = 0;

      if(strlen($pTdcs)>0)
        $pT = $pTdcs;
      else
        $pT = 0;

      if(strlen($pZdcs)>0)
        $pZ = $pZdcs;
      else
        $pZ = 0;


   $consulta = "SELECT
                    nt.id,
                    nt.tipo,
                    nt.titulo,
                    nt.descripcion,
                    nt.url,
                    nt.ruta_img,
                    nt.ruta_file,
                    IF(nt.palabras_claves IS NULL,'Ninguna',nt.palabras_claves) palabras_claves,
                    nt.estado_mo,
                    nt.descripcion_texto,CONCAT(nt.fecha_inicio,' hasta ', nt.fecha_fin) AS fecha
                  FROM
                    {$GLOBALS["BD_NAME"]}.noticias_promociones AS nt
                    INNER JOIN {$GLOBALS["BD_NAME"]}.niveles_dcs_noticias AS nd ON (nd.id_noticia = nt.id)

                  WHERE
                    nt.estado=1 AND nd.nivel=4 AND nd.web = 1 AND nt.vigente=1 AND nt.tipo = 3 AND ((nd.tipo=1 and nd.id_tipo in ($pR)) OR (nd.tipo=2 and nd.id_tipo=$IdDistri) OR (nd.tipo=3 and nd.id_tipo in ($pT)) OR (nd.tipo=4 and nd.id_tipo in ($pZ))) AND nt.fecha_inicio<=curdate() AND nt.imagen_web = 1 GROUP BY nt.id ORDER BY id DESC";
      mysql_set_charset("UTF8");
      $res = $this->DB->devolver_array($consulta);
      return $res;

  }
    public function noticias($param){

      $oSession = unserialize($_SESSION[$GLOBALS["SESION_POS"]]);
      $idpos=$oSession->VSidpos;

      $pZdcs = 0;
      $pTdcs = 0;
      $pRdcs = 0;
      $IdDistri = 0;

      $sql = "SELECT zona,territorio,id_regional,id_distri FROM puntos WHERE idpos = $idpos";
      $resPos = $this->DB->consultar($sql);

      if(count($resPos)>0){
        $pZdcs = $resPos->fields["zona"];
        $pTdcs = $resPos->fields["territorio"];
        $pRdcs = $resPos->fields["id_regional"];
        $IdDistri = $resPos->fields["id_distri"];
      }

      if(strlen($pRdcs)>0)
        $pR = $pRdcs;
      else
        $pR = 0;

      if(strlen($pTdcs)>0)
        $pT = $pTdcs;
      else
        $pT = 0;

      if(strlen($pZdcs)>0)
        $pZ = $pZdcs;
      else
        $pZ = 0;

      $condicion = '';

      if(trim($param) != "")
      {
        $condicion .= ' AND nt.palabras_claves like "%'.$param.'%" ';
      }

      $consulta = "SELECT
                    nt.id,
                    nt.tipo,
                    nt.titulo,
                    nt.descripcion,
                    nt.url,
                    nt.ruta_img,
                    nt.ruta_file,
                    IF(nt.palabras_claves IS NULL,'Ninguna',nt.palabras_claves) palabras_claves,
                    nt.estado_mo,nl.leido_mo, nl.leido,
                    nt.descripcion_texto,CONCAT(nt.fecha_inicio,' a ', nt.fecha_fin) AS fecha,
                    IF(nl.leido=0,0,IF(nl.leido=0 AND nl.leido_mo=0 AND nt.estado_mo=0,0,IF(nl.leido=1 AND nl.leido_mo=0 AND nt.estado_mo=0,1,IF(nl.leido=1 AND nl.leido_mo=0 AND nt.estado_mo=1,0,IF(nl.leido=1 AND nl.leido_mo=1 AND nt.estado_mo=1,1,0))))) AS orden
                  FROM
                    {$GLOBALS["BD_NAME"]}.noticias_promociones AS nt
                    INNER JOIN {$GLOBALS["BD_NAME"]}.niveles_dcs_noticias AS nd ON (nd.id_noticia = nt.id)
                    LEFT JOIN {$GLOBALS["BD_NAME"]}.noticias_leidas AS nl ON (nl.id_noticia = nt.id AND nl.id_pos=$idpos)
                  WHERE
                    nt.estado=1 AND nt.tipo in(1,2) AND nd.nivel=4 AND nd.web = 1 AND nt.vigente=1 AND ((nd.tipo=1 and nd.id_tipo in ($pR)) OR (nd.tipo=2 and nd.id_tipo=$IdDistri) OR (nd.tipo=3 and nd.id_tipo in ($pT)) OR (nd.tipo=4 and nd.id_tipo in ($pZ))) AND nt.fecha_inicio<=curdate() $condicion GROUP BY nt.id ORDER BY  orden,fecha ASC";
      mysql_set_charset("UTF8");
      $res = $this->DB->devolver_array($consulta);

      if(!empty($res))
      {
        foreach ($res as $val)
        {
          # code...
          $id_noticia = $val['id'];
          $estado_mo = $val['estado_mo'];

          $cons = "SELECT id,recibido_mo FROM {$GLOBALS["BD_NAME"]}.noticias_leidas WHERE id_noticia=$id_noticia AND id_pos=$idpos";
          $rescons = $this->DB->devolver_array($cons);

          if(empty($rescons))
          {
            $sql = "INSERT INTO {$GLOBALS["BD_NAME"]}.noticias_leidas(id_noticia,id_distri,id_pos,recibido,fecha_recibido,hora_recibido)VALUES($id_noticia,$IdDistri,$idpos,1,curdate(),curtime())";
            $execute = $this->DB->consultar($sql);

          }

          if($estado_mo == 1)
          {
            for($i=0;$i<count($rescons);$i++)
            {
              if($rescons[$i]['recibido_mo']==0)
              {
                $sqlUp3 = "UPDATE {$GLOBALS["BD_NAME"]}.noticias_leidas SET recibido_mo=1,fecha_recibido_mo=curdate(),hora_recibido_mo=curtime() WHERE id_noticia=$id_noticia AND id_pos=$idpos";
                $executeUp3 = $this->DB->consultar($sqlUp3);
              }
            }
          }

        }
      }

      return $res;

    }

    public function detalles_noticias($noticia)
    {

      $oSession = unserialize($_SESSION[$GLOBALS["SESION_POS"]]);
      $idpos=$oSession->VSidpos;

      $permZonas = 0;
      $permTerri = 0;
      $permReg = 0;
      $IdDistri = 0;

      $sql = "SELECT zona,territorio,id_regional,id_distri FROM puntos WHERE idpos = $idpos";
      $resPos = $this->DB->consultar($sql);

      if(count($resPos)>0){
        $pZdcs = $resPos->fields["zona"];
        $pTdcs = $resPos->fields["territorio"];
        $pRdcs = $resPos->fields["id_regional"];
        $IdDistri = $resPos->fields["id_distri"];
      }

      $consultar_modi = "SELECT
                          estado_mo
                        FROM
                          {$GLOBALS["BD_NAME"]}.noticias_promociones
                        WHERE
                          id=$noticia";
      $resm = $this->DB->devolver_array($consultar_modi);

      $consultar_leido = "SELECT leido,leido_mo FROM {$GLOBALS["BD_NAME"]}.noticias_leidas WHERE id_noticia = $noticia AND id_pos=$idpos";
      $resl = $this->DB->devolver_array($consultar_leido);

        if($resl[0]['leido']==0)
        {
          $sqlUp1 = "UPDATE {$GLOBALS["BD_NAME"]}.noticias_leidas SET leido=1,fecha_leido=curdate(),hora_leido=curtime(),id_distri = $IdDistri WHERE id_noticia=$noticia AND id_pos=$idpos";
          $executeUp1 = $this->DB->consultar($sqlUp1);
        }

        if($resm[0]['estado_mo']==1)
        {
          if($resl[0]['leido_mo']==0)
          {
            $sqlUp2 = "UPDATE {$GLOBALS["BD_NAME"]}.noticias_leidas SET leido_mo=1,fecha_mo=curdate(),hora_mo=curtime() WHERE id_noticia=$noticia AND id_pos=$idpos";
            $executeUp2 = $this->DB->consultar($sqlUp2);
          }

        }

      $consulta = "SELECT id,ruta_img,tipo,titulo,descripcion,url,ruta_file FROM {$GLOBALS["BD_NAME"]}.noticias_promociones WHERE id=$noticia";
      mysql_set_charset("UTF8");
      $res = $this->DB->devolver_array($consulta);


      return $res;
    }


} // Fin de la clase
?>