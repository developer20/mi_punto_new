<?php
require("../../config/mainModel.php");
class principal {
	/*Metodos de la DB.
  $this->consultar_datos($query); // Realiza la consulta y devuelve un Array Asociativo con los datos
  $this->ejecuta_query($query); // Ejecuta Insert, Update, Delete, Alter, Drop.. / devuelve el ultimo id afectado.
  $this->total_registros($columna, $tabla, $where = ""); // Devuelve el numero de registros. count();
  $this->trae_uno($query); // Realiza una consulta, devolviendo el primer valor que encuentre.
  $this->begin_work();
  $this->commit();
  $this->rollback();
  */

    /*
VARIABLES GLOBALES SISTEMA
$GLOBALS["BD_NAME"];
$GLOBALS["SERVER"];
$GLOBALS["SERVER_USER"];
$GLOBALS["SERVER_PASS"];
$GLOBALS["BD_DIS"];
$GLOBALS["BD_POS"];
$GLOBALS["SESION_OP"];
$GLOBALS["SESION_DIS"];

*/
	public function __construct()	{		
		$BD=new BD();
    $this->BD = $BD;
    $this->BD->conectar();
    $oSession = unserialize($_SESSION[$GLOBALS["SESION_POS"]]);
    $this->idpos=$oSession->VSidpos;
	}


  //Espacio Para declara las funciones que retornan los datos de la DB.
  
  public function info_dia(){
    $c_comp_simcar = "SELECT count(id) as sims 
                      FROM {$GLOBALS["BD_NAME"]}.simcards
                      WHERE fecha_venta = curdate() and estado = 1 and id_pos = {$this->idpos} and id_combo = 0";
    $r_comp_simcar = $this->BD->devolver_array($c_comp_simcar);

    $c_comp_prod = "SELECT count(id) as prod 
                    FROM {$GLOBALS["BD_NAME"]}.producto 
                    WHERE fecha_venta = curdate() and estado = 1 and id_pos = {$this->idpos} and id_combo > 0";

    $r_comp_prod = $this->BD->devolver_array($c_comp_prod);

    $c_comp_saldo = "SELECT if(sum(valor_acreditado) is null,0,sum(valor_acreditado)) as saldo 
                     FROM {$GLOBALS["BD_NAME"]}.recargas__venta_saldo 
                     WHERE id_pos = {$this->idpos} and fecha = curdate()";

    $r_comp_saldo = $this->BD->devolver_array($c_comp_saldo);

    $c_activaciones = "SELECT count(id) as acti 
                       FROM {$GLOBALS["BD_NAME"]}.simcards 
                       WHERE fecha_ac = curdate() and activo = 1 and id_pos = {$this->idpos}";

    $r_activaciones = $this->BD->devolver_array($c_activaciones);

    $c_recargas = "SELECT if(SUM(if(operador = 6,valor,0))is null,0,SUM(if(operador = 6,valor,0)))AS v_rec_mov,if(SUM(if(operador = 6,0,valor)) is null,0,SUM(if(operador = 6,0,valor)))AS v_rec_otro
                   FROM {$GLOBALS["BD_POS"]}.venta__servicios 
                   WHERE fecha = curdate() and id_punto = {$this->idpos} and estado = 1";

    $r_recargas = $this->BD->devolver_array($c_recargas);

    $fecha_vis = "N/A";
    $hora_vis = "N/A";
    $vendedor = "N/A";

    $c_visita_ult = "SELECT vis.id,vis.fecha,vis.hora,us.nombre_usuario
                     FROM {$GLOBALS["BD_POS"]}.visitas as vis
                     INNER JOIN {$GLOBALS["BD_NAME"]}.usuarios_distri as us on (us.id_usuario = vis.usuario and vis.id_distri=us.id_distri)
                     WHERE vis.punto = {$this->idpos}  
                     order by vis.id desc limit 1;";

    $r_visita_ult = $this->BD->devolver_array($c_visita_ult);
    if(count($r_visita_ult)>0){
      $fecha_vis = $r_visita_ult[0]["fecha"];
      $hora_vis = $r_visita_ult[0]["hora"];
      $vendedor = $r_visita_ult[0]["nombre_usuario"];
    }

    return array("compra_sim"=>$r_comp_simcar[0]["sims"],"compra_equi"=>$r_comp_prod[0]["prod"],"compra_saldo"=>$r_comp_saldo[0]["saldo"],"sim_act"=>$r_activaciones[0]["acti"],"rec_movistar"=>$r_recargas[0]["v_rec_mov"],"rec_otros"=>$r_recargas[0]["v_rec_otro"],"fecha_ult_vis"=>$fecha_vis,"hora_ult_vis"=>$hora_vis,"vendedor"=>$vendedor);

  }

  public function info_sem(){
    $diaactual = date("N");

    $array_dias_sim = array();
    $array_dias_prod = array();
    $array_dias_saldo = array();
    $array_dias_activaci = array();
    $array_dias_vent_recar = array();

    $c_comp_simcar = "SELECT count(id) as cant,if(DAYOFWEEK(fecha_venta-1)=0,7,DAYOFWEEK(fecha_venta-1)) as dia
                      from {$GLOBALS["BD_NAME"]}.simcards 
                      where fecha_venta >= (ADDDATE(curdate(),-($diaactual-1))) and fecha_venta <= curdate() and estado = 1 and id_combo = 0 and id_pos = {$this->idpos} group by dia order by fecha_venta;";
    $r_comp_simcar = $this->BD->consultar($c_comp_simcar);
    
    if($this->BD->numreg($r_comp_simcar) > 0){
      while(!$r_comp_simcar->EOF ){
        $array_dias_sim[$r_comp_simcar->fields['dia']]=$r_comp_simcar->fields['cant'];
        $r_comp_simcar->MoveNext();
      }
    }

    for ($i=1; $i <= $diaactual; $i++) { 
      if(!isset($array_dias_sim[$i])){
        $array_dias_sim[$i] = 0;
      }
    }

    $c_comp_pro = "SELECT count(id) as cant,if(DAYOFWEEK(fecha_venta)-1=0,7,DAYOFWEEK(fecha_venta)-1) as dia
                    from {$GLOBALS["BD_NAME"]}.producto 
                    where fecha_venta >= (ADDDATE(curdate(),-($diaactual-1))) and fecha_venta <= curdate() and estado = 1 and id_pos={$this->idpos} group by dia order by fecha_venta;";
    $r_comp_pro = $this->BD->consultar($c_comp_pro);
    
    if($this->BD->numreg($r_comp_pro) > 0){
      while(!$r_comp_pro->EOF ){
        $array_dias_prod[$r_comp_pro->fields['dia']]=$r_comp_pro->fields['cant'];
        $r_comp_pro->MoveNext();
      }
    }

    for ($i=1; $i <= $diaactual; $i++) {
      if(!isset($array_dias_prod[$i])){
        $array_dias_prod[$i] = 0;
      }
    }

    $c_comp_saldo = "SELECT if(sum(valor_acreditado) is null,0,sum(valor_acreditado)) as saldo,DAYOFWEEK(fecha)-1 as dia
                     FROM {$GLOBALS["BD_NAME"]}.recargas__venta_saldo 
                     WHERE id_pos = {$this->idpos} and fecha >= (ADDDATE(curdate(),-($diaactual-1))) and fecha <= curdate() group by dia";

    $r_comp_saldo = $this->BD->consultar($c_comp_saldo);

    if($this->BD->numreg($r_comp_saldo) > 0){
      while(!$r_comp_saldo->EOF ){
        $array_dias_saldo[$r_comp_saldo->fields['dia']]=$r_comp_saldo->fields['saldo'];
        $r_comp_saldo->MoveNext();
      }
    }

     for ($i=1; $i <= $diaactual; $i++) {
      if(!isset($array_dias_saldo[$i])){
        $array_dias_saldo[$i] = 0;
      }
    }


    $c_activaciones = "SELECT count(id) as acti,DAYOFWEEK(fecha_ac)-1 as dia
                       FROM {$GLOBALS["BD_NAME"]}.simcards 
                       WHERE fecha_ac >= (ADDDATE(curdate(),-($diaactual-1))) and fecha_ac <= curdate() and activo = 1 and id_pos = {$this->idpos} group by dia;";

    $r_activaciones = $this->BD->consultar($c_activaciones);

    if($this->BD->numreg($r_activaciones) > 0){
      while(!$r_activaciones->EOF ){
        $array_dias_activaci[$r_activaciones->fields['dia']]=$r_activaciones->fields['acti'];
        $r_activaciones->MoveNext();
      }
    }

     for ($i=1; $i <= $diaactual; $i++) {
      if(!isset($array_dias_activaci[$i])){
        $array_dias_activaci[$i] = 0;
      }
    }


    $c_recargas = "SELECT if(SUM(if(operador = 6,valor,0))is null,0,SUM(if(operador = 6,valor,0)))AS v_rec_mov,if(SUM(if(operador = 6,0,valor)) is null,0,SUM(if(operador = 6,0,valor)))AS v_rec_otro,DAYOFWEEK(fecha)-1 as dia
                   FROM {$GLOBALS["BD_POS"]}.venta__servicios 
                   WHERE fecha >= (ADDDATE(curdate(),-($diaactual-1))) and fecha <= curdate() and id_punto = {$this->idpos} and estado = 1 group by dia";

    $r_recargas = $this->BD->consultar($c_recargas);

    if($this->BD->numreg($r_recargas) > 0){
      while(!$r_recargas->EOF ){
        $array_dias_vent_recar[$r_recargas->fields['dia']]=array("recarga_mov" => $r_recargas->fields['v_rec_mov'],"recarga_otros" => $r_recargas->fields['v_rec_otro']);
        $r_recargas->MoveNext();
      }
    }

     for ($i=1; $i <= $diaactual; $i++) {
      if(!isset($array_dias_vent_recar[$i])){
        $array_dias_vent_recar[$i] = array("recarga_mov" => 0,"recarga_otros" => 0);;
      }
    }

    //FECHA ULTIMA VISITA
      $fecha_vis = "N/A";
      $hora_vis = "N/A";
      $vendedor = "N/A";

      $c_visita_ult = "SELECT vis.id,vis.fecha,vis.hora,us.nombre_usuario
                       FROM {$GLOBALS["BD_POS"]}.visitas as vis
                       INNER JOIN {$GLOBALS["BD_NAME"]}.usuarios_distri as us on (us.id_usuario = vis.usuario and vis.id_distri=us.id_distri)
                       WHERE vis.punto = {$this->idpos}  
                       order by vis.id desc limit 1;";

      $r_visita_ult = $this->BD->devolver_array($c_visita_ult);
      if(count($r_visita_ult)>0){
        $fecha_vis = $r_visita_ult[0]["fecha"];
        $hora_vis = $r_visita_ult[0]["hora"];
        $vendedor = $r_visita_ult[0]["nombre_usuario"];
      }
    //FECHA ULTIMA VISITA FIN

      return array("dias_comp_sim" => $array_dias_sim,"dias_comp_pro"=>$array_dias_prod,"comp_saldo_sem"=>$array_dias_saldo,"activaciones_pun"=>$array_dias_activaci,"recargas_pun_sem"=>$array_dias_vent_recar,"fecha_ult_vis"=>$fecha_vis,"hora_ult_vis"=>$hora_vis,"vendedor"=>$vendedor);
  }

  public function info_mes(){

    $diaactual = date("j");
    $mesactual = date("m");
    $anioactual = date("Y");

    $array_dias_sim = array();
    $array_dias_prod = array();
    $array_dias_saldo = array();
    $array_dias_activaci = array();
    $array_dias_vent_recar = array();

    $fecha_inicial = $anioactual."-".$mesactual."-01";
    
    //COMPRA SIMCARDS

    $c_comp_simcar = "SELECT count(id) as cant,DAY(fecha_venta) as dia
                      from {$GLOBALS["BD_NAME"]}.simcards 
                      where fecha_venta >= '$fecha_inicial' and estado = 1 and id_combo = 0 and id_pos = {$this->idpos} group by dia order by fecha_venta;";
    $r_comp_simcar = $this->BD->consultar($c_comp_simcar);
    
    if($this->BD->numreg($r_comp_simcar) > 0){
      while(!$r_comp_simcar->EOF ){
        $array_dias_sim[$r_comp_simcar->fields['dia']]=$r_comp_simcar->fields['cant'];
        $r_comp_simcar->MoveNext();
      }
    }

    for ($i=1; $i <= $diaactual; $i++) { 
      if(!isset($array_dias_sim[$i])){
        $array_dias_sim[$i] = 0;
      }
    }

    $c_comp_pro = "SELECT count(id) as cant,DAY(fecha_venta) as dia
                    from {$GLOBALS["BD_NAME"]}.producto 
                    where fecha_venta >= '$fecha_inicial' and estado = 1 and id_pos={$this->idpos} group by dia order by fecha_venta;";
    $r_comp_pro = $this->BD->consultar($c_comp_pro);
    
    if($this->BD->numreg($r_comp_pro) > 0){
      while(!$r_comp_pro->EOF ){
        $array_dias_prod[$r_comp_pro->fields['dia']]=$r_comp_pro->fields['cant'];
        $r_comp_pro->MoveNext();
      }
    }

    for ($i=1; $i <= $diaactual; $i++) {
      if(!isset($array_dias_prod[$i])){
        $array_dias_prod[$i] = 0;
      }
    }

    //COMPRA SIMCARDS FIN

    //COMPRA DE SALDO
      $c_comp_saldo = "SELECT if(sum(valor_acreditado) is null,0,sum(valor_acreditado)) as saldo,DAY(fecha) as dia
                     FROM {$GLOBALS["BD_NAME"]}.recargas__venta_saldo 
                     WHERE id_pos = {$this->idpos} and fecha >= '$fecha_inicial' group by dia";

      $r_comp_saldo = $this->BD->consultar($c_comp_saldo);

      if($this->BD->numreg($r_comp_saldo) > 0){
        while(!$r_comp_saldo->EOF ){
          $array_dias_saldo[$r_comp_saldo->fields['dia']]=$r_comp_saldo->fields['saldo'];
          $r_comp_saldo->MoveNext();
        }
      }

       for ($i=1; $i <= $diaactual; $i++) {
        if(!isset($array_dias_saldo[$i])){
          $array_dias_saldo[$i] = 0;
        }
      }
    //COMPRA DE SALDO FIN

    //ACTIVACIONES SIMCARD
      $c_activaciones = "SELECT count(id) as acti,DAY(fecha_ac) as dia
                       FROM {$GLOBALS["BD_NAME"]}.simcards 
                       WHERE fecha_ac >= '$fecha_inicial' and activo = 1 and id_pos = {$this->idpos} group by dia;";

      $r_activaciones = $this->BD->consultar($c_activaciones);

      if($this->BD->numreg($r_activaciones) > 0){
        while(!$r_activaciones->EOF ){
          $array_dias_activaci[$r_activaciones->fields['dia']]=$r_activaciones->fields['acti'];
          $r_activaciones->MoveNext();
        }
      }

       for ($i=1; $i <= $diaactual; $i++) {
        if(!isset($array_dias_activaci[$i])){
          $array_dias_activaci[$i] = 0;
        }
      }
    //ACTIVACIONES SIMCARD FIN
    
    //RECARGAS
       $c_recargas = "SELECT if(SUM(if(operador = 6,valor,0))is null,0,SUM(if(operador = 6,valor,0)))AS v_rec_mov,if(SUM(if(operador = 6,0,valor)) is null,0,SUM(if(operador = 6,0,valor)))AS v_rec_otro,DAY(fecha) as dia
                   FROM {$GLOBALS["BD_POS"]}.venta__servicios 
                   WHERE fecha >= '$fecha_inicial' and id_punto = {$this->idpos} and estado = 1 group by dia";

        $r_recargas = $this->BD->consultar($c_recargas);

        if($this->BD->numreg($r_recargas) > 0){
          while(!$r_recargas->EOF ){
            $array_dias_vent_recar[$r_recargas->fields['dia']]=array("recarga_mov" => $r_recargas->fields['v_rec_mov'],"recarga_otros" => $r_recargas->fields['v_rec_otro']);
            $r_recargas->MoveNext();
          }
        }

         for ($i=1; $i <= $diaactual; $i++) {
          if(!isset($array_dias_vent_recar[$i])){
            $array_dias_vent_recar[$i] =array("recarga_mov" => 0,"recarga_otros" => 0);
          }
        }
    //RECARGAS FIN

    //FECHA ULTIMA VISITA
      $fecha_vis = "N/A";
      $hora_vis = "N/A";
      $vendedor = "N/A";

      $c_visita_ult = "SELECT vis.id,vis.fecha,vis.hora,us.nombre_usuario
                       FROM {$GLOBALS["BD_POS"]}.visitas as vis
                       INNER JOIN {$GLOBALS["BD_NAME"]}.usuarios_distri as us on (us.id_usuario = vis.usuario and vis.id_distri=us.id_distri)
                       WHERE vis.punto = {$this->idpos}  
                       order by vis.id desc limit 1;";

      $r_visita_ult = $this->BD->devolver_array($c_visita_ult);
      if(count($r_visita_ult)>0){
        $fecha_vis = $r_visita_ult[0]["fecha"];
        $hora_vis = $r_visita_ult[0]["hora"];
        $vendedor = $r_visita_ult[0]["nombre_usuario"];
      }
    //FECHA ULTIMA VISITA FIN
      return array("dias_comp_sim" => $array_dias_sim,"dias_comp_pro"=>$array_dias_prod,"comp_saldo_sem"=>$array_dias_saldo,"activaciones_pun"=>$array_dias_activaci,"recargas_pun_sem"=>$array_dias_vent_recar,"fecha_ult_vis"=>$fecha_vis,"hora_ult_vis"=>$hora_vis,"vendedor"=>$vendedor);

  }
  
}// Fin clase
?>