var sim_col = "";
var equi_col = "";
var mov_col = "";
var otro_col = "";
var url_ar = "http://prueba.movilbox.net:88/movistar_colombia/files_noti";
//var url_ar = "../files_noti";
$(document).ready(function() {
	Cargar_Banner_Mbox();
	cargar_dia();
	
	$('#select_dia').on('shown.bs.tab', function (e) {
	  cargar_dia();
	})
	$('#select_sem').on('shown.bs.tab', function (e) {
      cargar_sem();
	})
	
	$('#select_mes').on('shown.bs.tab', function (e) {
	  cargar_mes();
	})
});
function Cargar_Banner_Mbox(){
	var i, concatenar = "", concatenar2 = "", count=0;
	$.ajax({
		url:"modulos/noticias_promociones/controlador.php",
	    type:"POST",
		data:{"accion":"Cargar_Banner_Mbox"},
		dataType:"json",
	    success:function(datos){
	    	if(datos == ""){
	    		$(".grupo2").html("");
	    	}
	    	console.log(datos);
	    	$.each(datos,function(i, val){

	    		if(i == 0){
	    			concatenar += "<li data-target='#banner-mbox' data-slide-to='"+count+"' class='active'></li>";
	    			concatenar2 += "<div class='item active'><img src='"+url_ar+"/imgs/"+val.ruta_img+"'> <div class='carousel-caption' style='background: rgba(0, 0, 0, 0.30);right:0 !important;left:0 !important;width:100%;bottom: 0px !important; padding-bottom:0; padding-top:0;'> <h4>Vigencia: Desde "+val.fecha+"</h4><br></div></div>";
	    		}else{
	    			concatenar += "<li data-target='#banner-mbox' data-slide-to='"+count+"'></li>";
	    			concatenar2 += "<div class='item'><img src='"+url_ar+"/imgs/"+val.ruta_img+"'><div class='carousel-caption' style='background: rgba(0, 0, 0, 0.30);right:0 !important;left:0 !important;width:100%;bottom: 0px !important;padding-bottom:0; padding-top:0;'> <h4>Vigencia: Desde "+val.fecha+"</h4><br></div></div>";
	    		}
	    		count++;
	    	})
	    	$("#indicadores").html(concatenar);
	    	$("#cont_img").html(concatenar2);
	    }

	})

}
function cargar_dia(){
	

	$.post('modulos/principal/controlador.php',
	{
	  	accion:'info_dia'		
	},
	function(data, textStatus) {
		 data = JSON.parse(data);
		 var datos = new Array();

		 var html = "";

		 html += "<tr>";
         html +=  "<td>"+data.compra_sim+"</td>";
         html +=  "<td>"+data.compra_equi+"</td>";
         html +=  "<td>"+data.compra_saldo+"</td>";
         html += "</tr>";

         $("#tb_compras_dia").html(html);

         var html2 = "<tr><td>"+data.sim_act+"</td></tr>";

         $("#tb_ventas_dia").html(html2);

         var html3 = "";

         html3+= "<tr>";
         html3+=  "<td>"+data.fecha_ult_vis+"</td>";
         html3+=  "<td>"+data.hora_ult_vis+"</td>";
         html3+=  "<td>"+data.vendedor+"</td>";
         html3+= "</tr>";
         $("#tb_vis_dia").html(html3);

		 datos.push({name:"Movistar",color:"#10628a",y:parseInt(data.rec_movistar)});
		 datos.push({name:"Otros",color:"#fffff",y:parseInt(data.rec_otros)});
		 
		 grafico_Recargas(datos);
		 
	});
}

function cargar_sem(){
	$.post('modulos/principal/controlador.php',
	{
	  	accion:'info_sem'		
	},
	function(data, textStatus) {
		 data = JSON.parse(data);
		 dias = ['Lunes','Martes','Miercoles','Jueves','Viernes','Sabado','Domingo'];
		 var dias_act = new Array();
		 var d_comp_sim = new Array(); 
		 var d_comp_pro = new Array();
		 var d_comp_sal = new Array();
		 var d_activaciones = new Array();
		 var d_rec_movistar = new Array();
		 var d_rec_otros = new Array();
		 var datos_compras_sem = new Array(); 
		 var datos_comp_saldo_sem = new Array(); 
		 var datos_act_sem = new Array(); 
		 var datos_rec_sem = new Array();
		 /*
			grafico_compras_sem();
		  graf_comp_sal_sem();
		  graf_act_sem();
		  graf_rec_sem();
		 */ 
		 var html="";
		 html+= "<tr>";
         html+=  "<td>"+data.fecha_ult_vis+"</td>";
         html+=  "<td>"+data.hora_ult_vis+"</td>";
         html+=  "<td>"+data.vendedor+"</td>";
         html+= "</tr>";
         $("#tb_vis_sem").html(html);
         //compras
		 $.each(data.dias_comp_sim,function(index,val){
		 	dias_act.push(dias[index-1]);
		 	d_comp_sim.push(parseInt(val));
		 });

		 $.each(data.dias_comp_pro,function(index,val){
		 	d_comp_pro.push(parseInt(val));
		 });

		 datos_compras_sem.push({name:'Simcard',color:"#82FA58",data:d_comp_sim,pointPadding:0.1});
		 datos_compras_sem.push({name:'Equipos',color:"#2E2EFE",data:d_comp_pro,pointPadding:0.1});

		 grafico_compras_sem(datos_compras_sem,dias_act);
		 //compras fin
		
		 //compra saldo
		 dias_act = new Array();
		 $.each(data.comp_saldo_sem,function(index,val){
		 	dias_act.push(dias[index-1]);
		 	d_comp_sal.push({name:dias[index-1],color:'#82FA58',y:parseInt(val)});
		 });
		 graf_comp_sal_sem(d_comp_sal,dias_act);
		 //compra saldo fin

		 //activaciones
		 dias_act = new Array();
		 $.each(data.activaciones_pun,function(index,val){
		 	dias_act.push(dias[index-1]);
		 	d_activaciones.push({name:dias[index-1],color:'#82FA58',y:parseInt(val)});
		 });
		 graf_act_sem(d_activaciones,dias_act);
		 //activaciones fin

		 //recargas
		 dias_act = new Array();
		  $.each(data.recargas_pun_sem,function(index,val){
		 	dias_act.push(dias[index-1]);
		 	d_rec_movistar.push(parseInt(val.recarga_mov));
		    d_rec_otros.push(parseInt(val.recarga_otros));
		 });

		   datos_rec_sem.push({name:'Movistar',color:"#82FA58",data:d_rec_movistar,pointPadding:0.1});
		   datos_rec_sem.push({name:'Otros',color:"#2E2EFE",data:d_rec_otros,pointPadding:0.1});
		 	graf_rec_sem(datos_rec_sem,dias_act);
		 //recargas fin
	});
}

function cargar_mes(){
	$.post('modulos/principal/controlador.php',
	{
	  	accion:'info_mes'		
	},
	function(data, textStatus) {
		 data = JSON.parse(data);
		 var dias_act = new Array();
		 var d_comp_sim = new Array(); 
		 var d_comp_pro = new Array();
		 var d_comp_sal = new Array();
		 var d_activaciones = new Array();
		 var d_rec_movistar = new Array();
		 var d_rec_otros = new Array();
		 var datos_compras_sem = new Array(); 
		 var datos_comp_saldo_sem = new Array(); 
		 var datos_act_sem = new Array(); 
		 var datos_rec_sem = new Array();
		 /*
			grafico_compras_mes();
		  graf_comp_sal_mes();
		  graf_act_mes();
		  graf_rec_mes();
		 */ 
		 var html="";
		 html+= "<tr>";
         html+=  "<td>"+data.fecha_ult_vis+"</td>";
         html+=  "<td>"+data.hora_ult_vis+"</td>";
         html+=  "<td>"+data.vendedor+"</td>";
         html+= "</tr>";
         $("#tb_vis_mes").html(html);
         //compras
		 $.each(data.dias_comp_sim,function(index,val){
		 	dias_act.push(index);
		 	d_comp_sim.push(parseInt(val));
		 });

		 $.each(data.dias_comp_pro,function(index,val){
		 	d_comp_pro.push(parseInt(val));
		 });

		 datos_compras_sem.push({name:'Simcard',color:"#82FA58",data:d_comp_sim,pointPadding:0.1});
		 datos_compras_sem.push({name:'Equipos',color:"#2E2EFE",data:d_comp_pro,pointPadding:0.1});

		 grafico_compras_mes(datos_compras_sem,dias_act);
		 //compras fin
		
		 //compra saldo
		 dias_act = new Array();
		 $.each(data.comp_saldo_sem,function(index,val){
		 	dias_act.push(index);
		 	d_comp_sal.push({name:index,color:'#82FA58',y:parseInt(val)});
		 });
		 graf_comp_sal_mes(d_comp_sal,dias_act);
		 //compra saldo fin

		 //activaciones
		 dias_act = new Array();
		 $.each(data.activaciones_pun,function(index,val){
		 	dias_act.push(index);
		 	d_activaciones.push({name:index,color:'#82FA58',y:parseInt(val)});
		 });
		 graf_act_mes(d_activaciones,dias_act);
		 //activaciones fin

		 //recargas
		 dias_act = new Array();
		  $.each(data.recargas_pun_sem,function(index,val){
		 	dias_act.push(index);
		 	d_rec_movistar.push(parseInt(val.recarga_mov));
		    d_rec_otros.push(parseInt(val.recarga_otros));
		 });
		   datos_rec_sem.push({name:'Movistar',color:"#82FA58",data:d_rec_movistar,pointPadding:0.1});
		   datos_rec_sem.push({name:'Otros',color:"#2E2EFE",data:d_rec_otros,pointPadding:0.1});
		 	graf_rec_mes(datos_rec_sem,dias_act);
		 //recargas fin
			 
	});
}

function grafico_Recargas(datos){

		   var chart1 = new Highcharts.Chart({
		        chart: {
		            renderTo: 'graf_recargas_dia',
		            defaultSeriesType: 'column'
		        },
		        title: {
				            text: 'Recargas'
				},legend: {
					enabled:false
				},
		        xAxis: {
		            categories: ['Movistar','Otros'],
				    min: 0
		        },
			    yAxis: {
			    	labels: {
				        formatter: function() {
				            return format(this.value,0);
				        }
				    },
			        min: 0,
			        title: {
			            text: 'Cantidad de recargas'
			        }
			    },tooltip: {
					            formatter: function () {
					                var texto=this.point.name;
		                            texto+="<br>Cant.Recargas: "+format(this.point.y,0);
					                return texto;
					            		}
					        	},

		        series: [{
		            data: datos 
		        }]
    		});
}

function grafico_compras_sem(datos,cate){

		   var chart1 = new Highcharts.Chart({
		        chart: {
		            renderTo: 'graf_comp_sem',
		            defaultSeriesType: 'column'
		        },
		        title: {
				            text: 'Simcard/Equipos'
				},legend: {
					enabled:true
				},
		        xAxis: {
		            categories: cate,
				    min: 0
		        },
			    yAxis: {
			    	labels: {
				        formatter: function() {
				            return format(this.value,0);
				        }
				    },
			        min: 0,
			        title: {
			            text: 'Cantidad Simcard/Equipos'
			        }
			    },plotOptions: {
					        column: {
					            grouping: true,
					            shadow: true,
					            borderWidth: 0
					        }
					    },tooltip: {
					            formatter: function () {
					                var texto=this.series.name;
		                            texto+="<br>Cant: "+format(this.point.y,0);
					                return texto;
					            		}
					        	},

		        series: datos

    		});
}

function graf_comp_sal_sem(datos,cate){

		   var chart1 = new Highcharts.Chart({
		        chart: {
		            renderTo: 'graf_comp_sal_sem',
		            defaultSeriesType: 'column'
		        },
		        title: {
				            text: 'Saldo'
				},legend: {
					enabled:false
				},
		        xAxis: {
		           categories: cate,
				    min: 0
		        },
			    yAxis: {
			    	labels: {
				        formatter: function() {
				            return format(this.value,0);
				        }
				    },
			        min: 0,
			        title: {
			            text: 'Cantidad de recargas'
			        }
			    },tooltip: {
					            formatter: function () {
					                var texto=this.point.name;
		                            texto+="<br>Valor: "+format(this.point.y,0);
					                return texto;
					            		}
					        	},

		        series: [{
		            data: datos 
		        }]
    		});
}

function graf_act_sem(datos,cate){

		   var chart1 = new Highcharts.Chart({
		        chart: {
		            renderTo: 'graf_act_sem',
		            defaultSeriesType: 'column'
		        },
		        title: {
				            text: 'Cantidad de ventas/activaciones'
				},legend: {
					enabled:false
				},
		        xAxis: {
		            categories: cate,
				    min: 0
		        },
			    yAxis: {
			    	labels: {
				        formatter: function() {
				            return format(this.value,0);
				        }
				    },
			        min: 0,
			        title: {
			            text: 'Cantidad de ventas/activaciones'
			        }
			    },tooltip: {
					            formatter: function () {
					                var texto=this.point.name;
		                            texto+="<br>Cant.ventas/activaciones: "+format(this.point.y,0);
					                return texto;
					            		}
					        	},

		        series: [{
		            data: datos 
		        }]
    		});
}

function graf_rec_sem(datos,cate){

		   var chart1 = new Highcharts.Chart({
		        chart: {
		            renderTo: 'graf_rec_sem',
		            defaultSeriesType: 'column'
		        },
		        title: {
				            text: 'Recargas'
				},legend: {
					enabled:true
				},
		        xAxis: {
		            categories: cate,
				    min: 0
		        },
			    yAxis: {
			    	labels: {
				        formatter: function() {
				            return format(this.value,0);
				        }
				    },
			        min: 0,
			        title: {
			            text: 'Valor Recargas'
			        }
			    },plotOptions: {
					        column: {
					            grouping: true,
					            shadow: true,
					            borderWidth: 0
					        }
					    },tooltip: {
					            formatter: function () {
					                var texto=this.series.name;
		                            texto+="<br>Val. recargas: "+format(this.point.y,0);
					                return texto;
					            		}
					        	},

		        series: datos

    		});
}



/////////////////-----------------------MES----------------

function grafico_compras_mes(datos,cate){

		   var chart1 = new Highcharts.Chart({
		        chart: {
		            renderTo: 'graf_comp_mes',
		            defaultSeriesType: 'column'
		        },
		        title: {
				            text: 'Simcard/Equipos'
				},legend: {
					enabled:true
				},
		        xAxis: {
		            categories: cate,
				    min: 0
		        },
			    yAxis: {
			    	labels: {
				        formatter: function() {
				            return format(this.value,0);
				        }
				    },
			        min: 0,
			        title: {
			            text: 'Cantidad Simcard/Equipos'
			        }
			    },plotOptions: {
					        column: {
					            grouping: true,
					            shadow: true,
					            borderWidth: 0
					        }
					    },tooltip: {
					            formatter: function () {
					                var texto=this.series.name;
		                            texto+="<br>Cant: "+format(this.point.y,0);
					                return texto;
					            		}
					        	},

		        series: datos

    		});
}

function graf_comp_sal_mes(datos,cate){

		   var chart1 = new Highcharts.Chart({
		        chart: {
		            renderTo: 'graf_comp_sal_mes',
		            defaultSeriesType: 'column'
		        },
		        title: {
				            text: 'Saldo'
				},legend: {
					enabled:false
				},
		        xAxis: {
		           categories: cate,
				    min: 0
		        },
			    yAxis: {
			    	labels: {
				        formatter: function() {
				            return format(this.value,0);
				        }
				    },
			        min: 0,
			        title: {
			            text: 'Cantidad de recargas'
			        }
			    },tooltip: {
					            formatter: function () {
					                var texto=this.point.name;
		                            texto+="<br>Valor: "+format(this.point.y,0);
					                return texto;
					            		}
					        	},

		        series: [{
		            data: datos 
		        }]
    		});
}

function graf_act_mes(datos,cate){

		   var chart1 = new Highcharts.Chart({
		        chart: {
		            renderTo: 'graf_act_mes',
		            defaultSeriesType: 'column'
		        },
		        title: {
				            text: 'Cantidad de ventas/activaciones'
				},legend: {
					enabled:false
				},
		        xAxis: {
		            categories: cate,
				    min: 0
		        },
			    yAxis: {
			    	labels: {
				        formatter: function() {
				            return format(this.value,0);
				        }
				    },
			        min: 0,
			        title: {
			            text: 'Cantidad de ventas/activaciones'
			        }
			    },tooltip: {
					            formatter: function () {
					                var texto=this.point.name;
		                            texto+="<br>Cant.ventas/activaciones: "+format(this.point.y,0);
					                return texto;
					            		}
					        	},

		        series: [{
		            data: datos 
		        }]
    		});
}

function graf_rec_mes(datos,cate){

		   var chart1 = new Highcharts.Chart({
		        chart: {
		            renderTo: 'graf_rec_mes',
		            defaultSeriesType: 'column'
		        },
		        title: {
				            text: 'Recargas'
				},legend: {
					enabled:true
				},
		        xAxis: {
		            categories: cate,
				    min: 0
		        },
			    yAxis: {
			    	labels: {
				        formatter: function() {
				            return format(this.value,0);
				        }
				    },
			        min: 0,
			        title: {
			            text: 'Valor Recargas'
			        }
			    },plotOptions: {
					        column: {
					            grouping: true,
					            shadow: true,
					            borderWidth: 0
					        }
					    },tooltip: {
					            formatter: function () {
					                var texto=this.series.name;
		                            texto+="<br>Val. recargas: "+format(this.point.y,0);
					                return texto;
					            		}
					        	},

		        series: datos

    		});
}


