
$(".box-header").remove();
$(".content-header").remove();


$(document).ready(function() {
  obtener_productos();
  cargar_tip_pro_marcas();
  cargar_paginador(1); 
  $("select#nombre").on("change", function () {
    obtener_sub_productos();
  });
});


function obtener_productos() {
  $.post("modulos/lista_productos_distri_v1.0.0/controlador.php", {
    accion: "obtener_productos",
  },
    function (data, textStatus) {
      data = JSON.parse(data);
      html = "<option value=\"\">SELECCIONAR</option>";

      for (loop = 0; loop < data.length; loop++) {
        html += "<option value=\"" + data[loop]["id_tabla"] + "\">" + data[loop]["nombre"] + "</option>";
      }

      $("select#nombre").html(html);
    });
}

function obtener_sub_productos() {
  var id_producto;
  id_producto = $("select#nombre").val();

  $.post(
    "modulos/lista_productos_distri_v1.0.0/controlador.php",
    {
      accion: "obtener_sub_productos",
      id_producto: id_producto
    },
    function (data, textStatus) {
      data = JSON.parse(data);
      html = "<option value=\"\">SELECCIONAR</option>";

      for (loop = 0; loop < data.length; loop++) {
        html += "<option value=\"" + data[loop]["id"] + "\">" + data[loop]["nombre"] + "</option>";
      }
      $("select#nom_subtipo").html(html);
    });
}


function cargar_tip_pro_marcas() {

	$.post('modulos/lista_productos_distri_v1.0.0/controlador.php',
		{
			accion: 'cargar_tip_pro_marcas'
		},
		function (data, textStatus) {
			data = JSON.parse(data);
			var datalist = "<option value=''>SELECCIONAR</option>";
			$.each(data, function (index, fila) {
				datalist += "<option value=" + fila.id + ">" + fila.nombre + "</option>";
			});
			$("#nombretip").html(datalist).change();
		});
}


function EjecutarBusqueda(e){
  if (e.keyCode == 13) {
    cargar_paginador(1);
  }
}


function filtros_header(){
   cargar_paginador(1);
}


function cargar_paginador(pagina){

  var items_por_pagina = 12;
  var numero_pagina = pagina;
  var buscar = $("#txt_buscar").val().trim();
  var tipo   = $("#nombre").val();
  var sub_tipo =  $("#nom_subtipo").val();
  var marca    =  $("#nombretip").val();

  $("#conte_pagina > div").remove();

  $.post('modulos/lista_productos_distri_v1.0.0/controlador.php',
  {
    accion: 'productos_distribuidor',items_por_pagina: items_por_pagina,numero_pagina: numero_pagina,buscar:buscar,tipo:tipo,sub_tipo:sub_tipo,marca:marca
  },
  function (data) {
    data = JSON.parse(data);
    var html_paginador = "";
    var html_contenido_pag = "";
    var active;
    
    if(!$.isEmptyObject(data)){
      // paginador
      for(var i=1; i<= parseInt(data[0].num_paginador); i ++){
        var active = (i == pagina) ? "active" : "";
        html_paginador+=" <li class='page-item "+active+"'><a class='page-link' onclick='cargar_paginador("+i+")' tabindex='-1'>"+i+"</a></li>";
      }
     
      $("#paginador li").remove();
      $("#paginador").html(html_paginador);
      
      // contenido pagina a la que voy a vizualizar, se vizualizan 4 items por rows
      for(var i=1; i<= data.length; i ++){
        /// cierra el row
        if(i == 5 || i == 9){
           html_contenido_pag+= "</div>";
        }
        // crear un row
        if(i == 1 || i == 5 || i == 9){
           html_contenido_pag+= "<div class='row animate__animated animate__zoomIn' style='padding: 10px;'>";
        }

        sku = data[i-1].producto.substring(0, 17);
        //<div style='width: 50%;'><button type='button' style='float: right;' class='btn btn-primary btn-xs'>Añadir al Carrito</button></div>
        var ruta_imagen = (data[i-1].imagen == "") ? "static/img/not_available.png" : "./../claro/distribuidor/static/img/referencias/productos/"+data[i-1].imagen+" ";
        html_contenido_pag +="<div class='col-md-3 contenedor_producto' ><a href='#' class='thumbnail' ><img src='"+ruta_imagen+"' style='max-width:80%;max-height: 180px;min-height: 180px'><i data-toggle='tooltip' onclick='agregar_carrito("+data[i-1].id+")' title='Añadir al Carrito' class='fas fa-cart-plus' style='color: black;font-size: 29px;position: absolute;margin-top: -30px;right: 30px;'></i><hr style='margin-top: 5px;margin-bottom: 5px;'><div class='flex-container'><div style='width: 100%;'><p style='color:black;font-size: 18px;margin-bottom: 2px;'>$ "+ formato_numero(data[i-1].valor_referencia,0,',','.') +"</p></div></div><div class='flex-container'><div style='width: 100%;'><p style='color: #777;font-size: 14px;margin: 0 0 5px;' >"+sku+"</p></div></div></a></div>";
      
      }
    
      // cierro el ultimo row
      html_contenido_pag+= "</div>";
      
      $("#conte_pagina").append(html_contenido_pag);
      $('[data-toggle="tooltip"]').tooltip(); 
    }else{
      $("#paginador li").remove();
      $("#conte_pagina").append("<div class='row' style='padding: 10px;text-align:center'><h1 style='color:red' class='animate__animated animate__zoomInDown'>No se encontrarón datos</h1></div>")
    }
  }
);


}


function formato_numero(numero, decimales, separador_decimal, separador_miles){ 
  
  numero = parseFloat(numero);
   if(isNaN(numero)){
    return '';
  }
  
  if(decimales !== undefined){
     numero = numero.toFixed(decimales); //Redondeamos
  }
  
  //Convertimos el punto en separador_decimal
  numero = numero.toString().replace('.', separador_decimal !== undefined ? separador_decimal : ',');
  if(separador_miles){
    // Añadimos los separadores de miles
    var miles = new RegExp('(-?[0-9]+)([0-9]{3})');
     while(miles.test(numero)){
      numero = numero.replace(miles, '$1' + separador_miles + '$2');
     }
  }
   return numero;
  
  }
