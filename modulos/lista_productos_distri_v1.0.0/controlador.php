<?php

include_once("../../config/mainController.php"); // Incluye el Controlador Principal
include_once("../../modulos/lista_productos_distri_v1.0.0/modelo.php");	// Incluye el Modelo.
$controller = new mainController; // Instancia a la clase MainController
$modelo = new lista_productos_distri(); // Instancia a la clase del modelo

try // Try, manejo de Errores
{
	$metodo = $_SERVER['REQUEST_METHOD'];
	$tipo_res = "";
	$response = null; 
	// Se manejaran dos tipos JSON y HTML
	// Dependiendo del método de la petición ejecutaremos la acción correspondiente.
	// Por ahora solo POST, todas las llamadas se haran por POST
	$variables = $_POST;
	if(!isset($_POST['accion'])){echo "0"; return;} // Evita que ocurra un error si no manda accion.
	$accion = $variables['accion'];
	// Dependiendo de la accion se ejecutaran las tareas y se definira el tipo de respuesta.
	switch($accion) {
		
		case "obtener_productos":
			$tipo_res = "JSON";
			$datos = $modelo->obtener_productos();
			$response = $datos;
		break;
		case "obtener_sub_productos":
			$tipo_res = "JSON";
			$id_producto = $variables["id_producto"];
			$datos = $modelo->obtener_sub_productos($id_producto);
			$response = $datos;
		break;

		case 'cargar_tip_pro_marcas':
			$tipo_res = 'JSON'; //Definir tipo de respuesta;
			$response = $modelo->cargar_tip_pro_marcas();
	   break;


		case 'productos_distribuidor':
			$tipo_res = 'JSON'; //Definir tipo de respuesta
			$items_por_pagina = $variables['items_por_pagina'];
			$numero_pagina = $variables['numero_pagina'];
			$buscar = $variables['buscar'];
			$tipo   = $variables['tipo'];
			$sub_tipo = $variables['sub_tipo'];
			$marca    = $variables['marca'];
			$response = $modelo->productos_distribuidor($items_por_pagina,$numero_pagina,$buscar,$tipo,$sub_tipo,$marca);

	    break;
	
	   


	 
	   
	}

	// Respuestas del Controlador
	if($tipo_res == "JSON")
	{
	  echo json_encode($response,true); // $response será un array con los datos de nuestra respuesta.
	}
	elseif ($tipo_res == "HTML") {
	  echo $response; // $response será un html con el string de nuestra respuesta.
	}
} // Fin Try
catch (Exception $e) {}


