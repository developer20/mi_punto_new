<?php
require("../../config/mainModel.php");
class lista_productos_distri {
    //Espacio Para declara las funciones que retornan los datos de la DB.
    public function __construct(){
  		$BD=new BD();
  		$this->BD = $BD;
		$this->BD->conectar();
	}
	  
    public function __destruct(){
    	$this->BD->desconectar();
	}


	public function obtener_productos() {

		$oSession = unserialize($_SESSION[$GLOBALS["SESION_POS"]]);
		$idDistri = $oSession->VSid_distri;

		$sql_dis = "SELECT nombre_corto FROM {$GLOBALS["BD_NAME"]}.distribuidores WHERE id = $idDistri";
		$query_dis = $this->BD->devolver_array($sql_dis);
		$nombre_corto = $query_dis[0]["nombre_corto"];
		$bd = "{$GLOBALS["BD_DIS"]}" . $nombre_corto . "_" . $idDistri;

		$consulta = "SELECT 
							nombre,
							id_tabla
					  FROM 
					        $bd.tipo_productos
					  ORDER BY nombre";
	      mysql_set_charset("utf8");
		$datos = $this->BD->devolver_array($consulta);
	
		return $datos;
	  }
	
	  public function obtener_sub_productos($id_producto) {  
		  
		$oSession = unserialize($_SESSION[$GLOBALS["SESION_POS"]]);
		$idDistri = $oSession->VSid_distri;

		$sql_dis = "SELECT nombre_corto FROM {$GLOBALS["BD_NAME"]}.distribuidores WHERE id = $idDistri";
		$query_dis = $this->BD->devolver_array($sql_dis);
		$nombre_corto = $query_dis[0]["nombre_corto"];
		$bd = "{$GLOBALS["BD_DIS"]}" . $nombre_corto . "_" . $idDistri;
	
		$consulta = "SELECT 
							nombre,
							id
					  FROM 
					       $bd.tipo_productos_sub
					  WHERE id_tipo = '$id_producto'
					  ORDER BY nombre";
	     mysql_set_charset("utf8");
		$datos = $this->BD->devolver_array($consulta);
	
		return $datos;
	  }

	  public function cargar_tip_pro_marcas(){

		$oSession = unserialize($_SESSION[$GLOBALS["SESION_POS"]]);
		$idDistri = $oSession->VSid_distri;

		$sql_dis = "SELECT nombre_corto FROM {$GLOBALS["BD_NAME"]}.distribuidores WHERE id = $idDistri";
		$query_dis = $this->BD->devolver_array($sql_dis);
		$nombre_corto = $query_dis[0]["nombre_corto"];
		$bd = "{$GLOBALS["BD_DIS"]}" . $nombre_corto . "_" . $idDistri;
      
		$query="SELECT  
		             mp.id,mp.nombre as nombre
		        FROM 
				     $bd.tipo_pro_marcas mp 
			    ORDER BY 
					mp.nombre";

		  mysql_set_charset("utf8");			
		 return $this->BD->devolver_array($query);
	 }

	public function productos_distribuidor($items_por_pagina,$numero_pagina,$buscador,$tipo,$subtipo,$marca){

		$oSession = unserialize($_SESSION[$GLOBALS["SESION_POS"]]);
		$idDistri = $oSession->VSid_distri;
		$punto  =  $oSession->VSidpos;
		
		if($numero_pagina !=  1){
		    $desde = ($items_por_pagina * ($numero_pagina - 1));
		}else{
			$desde = 0;
		}

		$where = "";
		if($tipo != ""){
			$where.=" AND lis.id_tipo_producto = $tipo";
		}
		if($subtipo != ""){
			$where.=" AND lis.id_subtipo_producto = $subtipo";
		}
		if($marca != ""){
			$where.=" AND lis.id_marca = $marca";
		}
		if($buscador != ""){
			$where.=" AND r.producto LIKE '%$buscador%' OR r.pn LIKE '%$buscador%' ";
		}


			  $sql_dis = "SELECT nombre_corto FROM {$GLOBALS["BD_NAME"]}.distribuidores WHERE id = $idDistri";
		      $query_dis = $this->BD->devolver_array($sql_dis);
		      $nombre_corto = $query_dis[0]["nombre_corto"];
			  $bd = "{$GLOBALS["BD_DIS"]}" . $nombre_corto . "_" . $idDistri;
			  
			  $sql_to = "SELECT
								COUNT(lis.id) AS total
						FROM  
							    $bd.referencias r
					    INNER JOIN			
								$bd.lista_market_place lis
								 on r.id = lis.id_referencia
						WHERE 
								lis.estado = 1 $where ";
                   
					$res_total = $this->BD->devolver_array($sql_to);
					$total_reg = $res_total[0]['total'];

			 // # par ael paginador 
			 $num_paginado = ceil($total_reg / $items_por_pagina);
	 
			 $sql = "SELECT
							 r.id AS id_referencia,r.pn,r.producto,COALESCE(img.nombre,'') AS imagen,
							 $total_reg AS total_registros,$num_paginado AS num_paginador,lis.id 
					 FROM 
							  $bd.referencias r
					 INNER JOIN 
							  $bd.lista_market_place lis
							   ON r.id = lis.id_referencia
					 LEFT JOIN 
							  $bd.tipo_imagenes_submarcas img
								ON r.id = img.id_tipo AND img.tipo_submarca = 3 AND 
								 img.id IN (SELECT MIN(id) FROM $bd.tipo_imagenes_submarcas WHERE tipo_submarca = 3 AND r.id = id_tipo GROUP BY id_tipo HAVING MIN(id)) 
					 WHERE 
							 lis.estado = 1  $where
					LIMIT   
							$items_por_pagina
					OFFSET
					        $desde";
			 
			  mysql_set_charset("utf8");
			 $res = $this->BD->devolver_array($sql);
			 
			 $datos = array();
			 foreach($res as $all_data => $row) {
				$array = array();
				$precio_dcs = $this->Valor_ListaP($row['id_referencia']);// se demora mucho 
				$array['valor_referencia']  = $precio_dcs[0]['valor_referencia'];
				$array['num_paginador']  = $row['num_paginador'];
				$array['producto']  = $row['producto'];
				$array['total_registros']  = $row['total_registros'];
				$array['imagen']        = $row['imagen'];
				$array['pn']        = $row['pn'];
				$array['id'] = $row['id'];
				$array['id_referencia'] = $row['id_referencia'];
				$datos[] = $array;
			  
			 }
	 
			 return $datos;
	 		    
	}




    public function Valor_ListaP($referencia){

		$oSession = unserialize($_SESSION[$GLOBALS["SESION_POS"]]);
		$id_distri = $oSession->VSid_distri;
		$punto  =  $oSession->VSidpos;
        $lista = true;
		$r_referencia = array();
		
		$sql_dis = "SELECT nombre_corto FROM {$GLOBALS["BD_NAME"]}.distribuidores WHERE id = $id_distri";
		$query_dis = $this->BD->devolver_array($sql_dis);
		$nombre_corto = $query_dis[0]["nombre_corto"];
		$bd = "{$GLOBALS["BD_DIS"]}" . $nombre_corto . "_" . $id_distri;

        $consulta = "SELECT 
		                   idpos, territorio, zona 
					 FROM 
					       {$GLOBALS["BD_POS"]}.puntos 
					 WHERE 
							idpos = '$punto'";
				 
       $datos_punto = $this->BD->devolver_array($consulta);

        if(count($datos_punto)>0){
			
			$datos_reg = null;
			$datos_distri = null;
			$r_consulta_lista = null;
			$tipo_nivel = 3;
			$regional =0;
			// La funcion contiene los niveles de los cuales puede salir el precio de la referencia
			// Consulta para devolver los datos del punto.
            while ($lista) {
              switch ($tipo_nivel) {
				case 3://RUTA
					 $consulta_lista ="SELECT
												d.id_lista
										FROM 
										       $bd.lista_precio_market_place l
										INNER JOIN 
										       $bd.items_lista_market_place d
												ON l.id = d.id_lista
										INNER JOIN 
										       $bd.niveles_listaprecios_market_place n
												ON l.id = n.id_lista
										WHERE
											   d.estado = 1 AND l.estado = 1 AND  l.estado_vigencia=1 AND 
											   n.tipo_nivel = $tipo_nivel AND 
											   d.id_referencia  = $referencia AND 
											   n.id_nivel = ".$datos_punto[0]["zona"]." ";

                     $r_consulta_lista = $this->BD->devolver_array($consulta_lista);

					if(count($r_consulta_lista) > 0){
					    $lista = false;
					}else{
					    $tipo_nivel--;
					}

          break;
          case 2://CIRCUITO
			$consulta_lista ="SELECT
									d.id_lista
							FROM 
								$bd.lista_precio_market_place l
							INNER JOIN 
								$bd.items_lista_market_place d
									ON l.id = d.id_lista
							INNER JOIN 
								$bd.niveles_listaprecios_market_place n
									ON l.id = n.id_lista
							WHERE
								d.estado = 1 AND l.estado = 1 AND  l.estado_vigencia=1 AND 
								n.tipo_nivel = $tipo_nivel AND 
								d.id_referencia  = $referencia AND 
								n.id_nivel = ".$datos_punto[0]["territorio"]." ";
						 		
             $r_consulta_lista = $this->BD->devolver_array($consulta_lista);

			 if(count($r_consulta_lista)> 0){
				 $lista = false;
			 }else{
				$tipo_nivel--;
			 }

          break;
          case 1://REGIONAL OPERADOR

             $consulta = "SELECT 
			                    id_regional 
						  FROM 
						        {$GLOBALS["BD_NAME"]}.territorios_distribuidor 
						  WHERE 
								id_territorio = ".$datos_punto[0]["territorio"]."  
						  GROUP BY 
						        id_regional;";
			$datos_reg = $this->BD->devolver_array($consulta);

			$consulta_lista ="SELECT
									d.id_lista
							FROM 
								$bd.lista_precio_market_place l
							INNER JOIN 
								$bd.items_lista_market_place d
									ON l.id = d.id_lista
							INNER JOIN 
								$bd.niveles_listaprecios_market_place n
									ON l.id = n.id_lista
							WHERE
								d.estado = 1 AND l.estado = 1 AND  l.estado_vigencia=1 AND 
								n.tipo_nivel = $tipo_nivel AND 
								d.id_referencia  = $referencia AND 
								n.id_nivel = ".$datos_reg[0]["id_regional"]." ";
			
            $r_consulta_lista = $this->BD->devolver_array($consulta_lista);

            if(count($r_consulta_lista)> 0){
			  $lista = false;
            }else{
              $tipo_nivel--;
            }

          break;

          default:

            $r_consulta_lista = 0;
            $lista = false;

          break;
        }



      }
      //echo "si";
      if($r_consulta_lista != 0){


		$consulta_ref = "SELECT 
								d.precio_venta AS valor_referencia
						FROM 
								$bd.lista_market_place l 
						INNER JOIN 
								$bd.items_lista_market_place d
								ON l.id_referencia = d.id_referencia
						WHERE 
		                          l.estado = 1 AND d.id_lista = 21 AND
								  d.id_lista=".$r_consulta_lista[0]["id_lista"]." AND
								  d.id_referencia = $referencia";
      }else{
		
        $consulta_ref = "SELECT id,pn,precio AS valor_referencia FROM $bd.lista_market_place lm WHERE lm.id_referencia = $referencia";
      }
	 
	  $r_referencia =  $this->BD->devolver_array($consulta_ref);
	
    }else{
   
      //$this->Valor_ListaP_opeador($referencia,$punto,$id_distri);
      $consulta_ref = "SELECT id,pn,precio AS valor_referencia,0 AS iva_referencia,0 AS valor_directo FROM $bd.lista_market_place lm WHERE lm.id_referencia = $referencia";
    
      $r_referencia =  $this->BD->devolver_array($consulta_ref);

    }

    return $r_referencia;

}





	

}
