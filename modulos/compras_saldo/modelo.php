<?php
require ("../../config/mainModel.php");

class compras_saldo {
  /*
    Metodos de la DB.
    $this->consultar_datos($query); // Realiza la consulta y devuelve un Array Asociativo con los datos
    $this->ejecuta_query($query); // Ejecuta Insert, Update, Delete, Alter, Drop.. / devuelve el ultimo id afectado.
    $this->total_registros($columna, $tabla, $where = ""); // Devuelve el numero de registros. count();
    $this->trae_uno($query); // Realiza una consulta, devolviendo el primer valor que encuentre.
    $this->begin_work();
    $this->commit();
    $this->rollback();

    VARIABLES GLOBALES SISTEMA
    $GLOBALS["BD_NAME"];
    $GLOBALS["SERVER"];
    $GLOBALS["SERVER_USER"];
    $GLOBALS["SERVER_PASS"];
    $GLOBALS["BD_DIS"];
    $GLOBALS["BD_POS"];
    $GLOBALS["SESION_OP"];
    $GLOBALS["SESION_DIS"];
    */
  
  public function __construct ()	{		
    $BD = new BD ();
    $this->BD = $BD;
    $this->BD->conectar ();
    $oSession = unserialize ($_SESSION[$GLOBALS["SESION_POS"]]);
    $this->idpos=$oSession->VSidpos;
  }
  
  public function consulta_saldo_tipo ($f_ini, $f_fin, $tipo) {
    if ($tipo != "") {
      $condicion = " AND tipo = '$tipo' ";
    } else {
      $condicion = "";
    }

    $sql = "SELECT 
              SUM(IF(tipo = 0, valor_venta, 0)) AS valor_compra, 
              SUM(valor_acreditado) AS valor_acreditado, 
              SUM(IF(tipo = 0, (valor_acreditado - valor_venta), valor_acreditado)) AS valor_comicion, 
              tipo 
            FROM {$GLOBALS["BD_NAME"]}.recargas__venta_saldo 
            WHERE 
              fecha >= '$f_ini' AND 
              fecha <= '$f_fin' AND 
              id_pos = '{$this->idpos}' 
              $condicion 
            GROUP BY tipo";
    
    return $this->BD->devolver_array ($sql);
  }
  
  public function consulta_det_tipo ($f_ini, $f_fin, $tipo) {
    if ($tipo != "") {
      $condicion = " AND vs.tipo = '$tipo' ";
    } else {
      $condicion = "";
    }

    $sql = "SELECT 
              vs.id, 
              up.descripcion AS bolsa, 
              vs.fecha, 
              vs.hora, 
              IF(vs.tipo = 0, vs.valor_venta, 0) AS valor_compra, 
              vs.valor_acreditado, 
              IF(vs.tipo = 0, (vs.valor_acreditado - vs.valor_venta), vs.valor_acreditado) AS valor_comision, 
              IF(vs.tipo = 0, vs.porcentaje, 'N/A') AS porcentaje, 
              IF(vs.usuario = 0, 'Sistema', ud.nombre_usuario) AS usuario 
            
            FROM {$GLOBALS["BD_NAME"]}.recargas__venta_saldo AS vs 
            INNER JOIN {$GLOBALS["BD_POS"]}.bolsas_puntos AS bp ON (bp.id_pos = vs.id_pos AND bp.id_proveedor = vs.id_proveedor) 
            INNER JOIN {$GLOBALS["BD_NAME"]}.recargas__usudistri_prov AS up ON (up.id = bp.id_bolsa) 
            LEFT JOIN {$GLOBALS["BD_NAME"]}.usuarios_distri AS ud ON (ud.id_usuario = vs.usuario AND ud.id_distri = vs.id_distri) 
            WHERE 
              vs.fecha >= '$f_ini' AND 
              vs.fecha <= '$f_fin' AND 
              vs.id_pos = '{$this->idpos}' 
              $condicion 
            GROUP BY vs.id";
    
    return $this->BD->devolver_array ($sql);
  }
}
?>