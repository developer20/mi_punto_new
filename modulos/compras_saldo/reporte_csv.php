<?php
ini_set ("memory_limit", "3000M");
set_time_limit (3000);

if (!isset ($_SESSION)) {
	session_start ();
}

require_once ("../../config/Session.php");
require ("../../config/mainModel.php");

$BD = new BD ();
$BD->conectar ();
$oSession = unserialize ($_SESSION[$GLOBALS["SESION_POS"]]);
$idpos=$oSession->VSidpos;

header ("Content-type: application/vnd.ms-excel");
header ("Content-Disposition: filename=\"REPORTE_COMPRA_SALDO.csv\";");

$variables = $_GET;
$consulta = "";
$condicion = "";
$f_ini = $variables["f_ini"];
$f_fin = $variables["f_fin"];
$tipo = $variables["tipo"];

if ($tipo != "") {
	$condicion = " AND vs.tipo = $tipo ";
}

$sql = "SELECT 
			r_up.descripcion AS bolsa, 
			vs.fecha, 
			vs.hora, 
			IF(tipo = 0, vs.valor_venta, 0) AS valor_compra, 
			vs.valor_acreditado, 
			IF(vs.tipo = 0, (vs.valor_acreditado - vs.valor_venta), (vs.valor_acreditado)) as 
			valor_comision, 
			IF(tipo = 0, vs.porcentaje, 'N/A') AS porcentaje, 
			IF(vs.usuario = 0, 'Sistema', u.nombre_usuario) AS usuario, 
			IF(tipo = 0, 'Compra', IF(tipo = 1, 'Ajuste', IF(tipo = 2, 'Comision', IF(tipo = 3, 'Ajuste paquete', 'Incentivo')))) AS tipo 
		FROM {$GLOBALS["BD_NAME"]}.recargas__venta_saldo AS vs 
		INNER JOIN {$GLOBALS["BD_POS"]}.bolsas_puntos AS bp ON (bp.id_pos = vs.id_pos AND bp.id_proveedor = vs.id_proveedor) 
		INNER JOIN {$GLOBALS["BD_NAME"]}.recargas__usudistri_prov AS r_up ON (r_up.id = bp.id_bolsa) 
		LEFT JOIN {$GLOBALS["BD_NAME"]}.usuarios_distri AS u ON (u.id_usuario = vs.usuario AND u.id_distri = vs.id_distri) 
		WHERE 
			vs.fecha >= '$f_ini' AND 
			vs.fecha <= '$f_fin' AND 
			vs.id_pos = $idpos 
			$condicion
		GROUP BY vs.id";

$resultado = $BD->consultar ($sql);

// headers
echo "FECHA;HORA;TIPO;BOLSA;VENDEDOR;VALOR COMPRA;VALOR TRASLADO;VALOR COMISION;PORCENTAJE";

if ($BD->numreg ($resultado) > 0) {
	while (!$resultado->EOF) {
		if (intval ($resultado->fields["tipo"]) == 0) {
			$porcent = round ($resultado->fields["porcentaje"], 2)."%";
		} else {
			$porcent = $resultado->fields["porcentaje"];
		}
		
		echo "\n".
			$resultado->fields["fecha"].";".
			$resultado->fields["hora"].";".
			$resultado->fields["tipo"].";".
			$resultado->fields["bolsa"].";".
			$resultado->fields["usuario"].";".
			$resultado->fields["valor_compra"].";".
			$resultado->fields["valor_acreditado"].";".
			$resultado->fields["valor_comision"].";".
			$porcent;
		
		$resultado->MoveNext ();
	}
}
?>