$(document).ready(function() {
	$('.fecha').datepicker({
		format: 'yyyy-mm-dd',
		todayHighlight: true,
		todayBtn: true
	});
	$("#btn_buscar").click(function(){
		consulta_compra_distri("tbl_compra_saldo");
	});
	$("#CSV").click(function(e){
		var f_ini = $("#f_ini").val();
		var f_fin = $("#f_fin").val();
		var tipo = $("#tipo").val();
                             
		window.open('modulos/compras_saldo/reporte_csv.php?f_ini='+f_ini+'&f_fin='+f_fin+'&tipo='+tipo);
	});
});

function consulta_compra_distri(id){
	var f_ini = $("#f_ini").val();
	var f_fin = $("#f_fin").val();
	var distri = $("#distri").val();
	var tipo = $("#tipo").val();
	if(f_ini != "" && f_fin != ""){
	if ($.fn.DataTable.isDataTable('#'+id)) 
	{
		dtable.destroy();
	}

	dtable = $('#'+id).DataTable({
		"ajax": {
			"url": "modulos/compras_saldo/controlador.php",
			"type": "POST",
			"deferRender": false,
			"data":{
				accion:'consulta_saldo_tipo',
				f_ini:f_ini,
				f_fin:f_fin,
				tipo:tipo
			}
		},
		"bFilter": false, 
		"responsive":false,
		"columns":[
		{ "data": "tipo"},
		{ "data": "valor_compra"},
		{ "data": "valor_acreditado"},
		{ "data": "valor_comicion"}
		],
	        "columnDefs":[
	        	{
					"targets": 0,
					"data": "tipo",
					 render: function ( data, type, row ) {
					 	   if(parseInt(data) == 0){
					 	   		mensaje = 'Compra';
					 	   }else if(parseInt(data) == 1){
					 	   		mensaje = 'Ajuste';
					 	   }else if(parseInt(data) == 2){
					 	   		mensaje = 'Comision';
					 	   }else if(parseInt(data) == 3){
					 	   		mensaje = 'Ajuste Paquete';
					 	   }else if(parseInt(data) == 4){
					 	   		mensaje = 'Incentivo';
					 	   }

					 	   return mensaje;
					 }
				},
				{
					"targets": 4,
					"data": "",
					 render: function ( data, type, row ) {
					 	return "<i style='cursor:pointer; font-size:19px;' class='glyphicon glyphicon-plus-sign detalle text-primary'></i>";        
					 }
				}
	        ],
	        fnDrawCallback: function () {
			    $(".detalle").unbind("click");
			    $(".detalle").click(function(){
			    	var data = dtable.row( $(this).parents('tr') ).data();
			    	var tr = $(this).parent().parent();
			    	var tipo_mov = data.tipo;
			    	var tab;

					tab = $("body").find("#sald_"+tipo_mov);
					 
					if(!$.contains(window.document, tab[0]))
			       	{ 
			          $( "<tr id='sald_"+tipo_mov+"'><td colspan='5'></td></tr>" ).insertAfter($(tr));
					  $(this).removeClass( 'glyphicon-plus-sign' );
	                  $(this).addClass( 'glyphicon-minus-sign' );
	                  consulta_det_tipo(f_ini,f_fin,tipo_mov);
					}
					else
					{
					  $("#sald_"+tipo_mov).remove();
					  $(this).addClass( 'glyphicon-plus-sign' );
	                  $(this).removeClass( 'glyphicon-minus-sign' );
					}
			    });
		    	
	    	}
	});

		$("#consulta").show();
	}else{
		$("#consulta").hide();
		Notificacion("La fecha inicial y final son obligatorias.","warning")
	}
}

function consulta_det_tipo(f_ini,f_fin,tipo_movimiento){
	$.post('modulos/compras_saldo/controlador.php',
	{
	  accion:'consulta_det_tipo',/*fecha, hora, valor_compra, valor_acreditado, valor_comision, porcentaje, usuario*/
	  f_ini:f_ini,
	  f_fin:f_fin,
	  tipo:tipo_movimiento
	},
	function(data, textStatus) {
		 var reporte = "";
		 data = JSON.parse(data);
		 if(!$.isEmptyObject(data))
		 {
		 	reporte += "<div class='col-md-12' style='border:2px #ccc solid; border-radius:2px;'><div class='text-center'>DETALLE</div>";
			reporte += "<table class='table table-hovered table-condensed dt-responsive nowrap'>";
			reporte += "<thead><tr><th>FECHA</th><th>HORA</th><th>BOLSA</th><th>VALOR COMPRA</th><th>VALOR TRASLADADO</th><th>VALOR COMISION</th><th>PORCENTAJE</th><th>VENDEDOR</th></tr></thead><tbody>";
		 	$.each(data,function(index,row) {
		 		reporte += "<tr>";
		 		reporte += "<td>"+row.fecha+"</td>";
				reporte += "<td>"+row.hora+"</td>";
				reporte += "<td>"+row.bolsa+"</td>";
		 		reporte += "<td>"+row.valor_compra+"</td>";
		 		reporte += "<td>"+row.valor_acreditado+"</td>";
		 		reporte += "<td>"+row.valor_comision+"</td>";
		 		if(tipo_movimiento==0){
		 			reporte += "<td>"+format(row.porcentaje,2)+"%</td>";
		 		}else{
		 			reporte += "<td>"+row.porcentaje+"</td>";
		 		}
		 		reporte += "<td>"+row.usuario+"</td>";
		 		reporte += "</tr>";
		 	})
		 	reporte +="</tbody></table></div>";
		 	$("#sald_"+tipo_movimiento).find("td").html(reporte);

		 }
		 else
		 {
		 	Notificacion("No se encontraron resultados.","warning");
		 }
	});
}