<?php
include_once("../../config/mainController.php"); // Incluye el Controlador Principal
include_once("../../modulos/compras_saldo/modelo.php");	// Incluye el Modelo.
$controller = new mainController; // Instancia a la clase MainController
$modelo = new compras_saldo(); // Instancia a la clase del modelo
try // Try, manejo de Errores
{
	$metodo = $_SERVER['REQUEST_METHOD'];
	$tipo_res = "";
	$response = null; 
	// Se manejaran dos tipos JSON y HTML
	// Dependiendo del método de la petición ejecutaremos la acción correspondiente.
	// Por ahora solo POST, todas las llamadas se haran por POST
    $variables = $_POST;
	if(!isset($_POST['accion'])){echo "0"; return;} // Evita que ocurra un error si no manda accion.
	$accion = $variables['accion'];
		// Dependiendo de la accion se ejecutaran las tareas y se definira el tipo de respuesta.
		switch($accion) {
			case 'consulta_saldo_tipo':
				 $tipo_res = 'JSON'; //Definir tipo de respuesta;
				 $f_ini = $variables["f_ini"];
				 $f_fin = $variables["f_fin"];
				 $tipo = $variables["tipo"];
				 $datos = $modelo->consulta_saldo_tipo($f_ini,$f_fin,$tipo);
				 $response = array("draw" => 1,
							   "recordsTotal" => 0,
							   "recordsFiltered" => 0,
							   "data" => $datos);
			break;	
			case 'consulta_det_tipo':
				 $tipo_res = 'JSON'; //Definir tipo de respuesta;
				 $f_ini = $variables["f_ini"];
				 $f_fin = $variables["f_fin"];
				 $tipo = $variables["tipo"];
				 $datos = $modelo->consulta_det_tipo($f_ini,$f_fin,$tipo);
				 $response = $datos;
			break;	
		}

	// Respuestas del Controlador
	if($tipo_res == "JSON")
	{
	  echo json_encode($response,true); // $response será un array con los datos de nuestra respuesta.
	}
	elseif ($tipo_res == "HTML") {
	  echo $response; // $response será un html con el string de nuestra respuesta.
	}
} // Fin Try
catch (Exception $e) {}
