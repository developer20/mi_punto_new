<?php
require("../../config/mainModel.php");
class Reportes{
	
	public function reporteVentas($fecha_ini,$fecha_fin,$num_m,$opers){ 
	
		if(isset($_SESSION[$GLOBALS["SESION_POS"]])){
			
			$oSession = unserialize($_SESSION[$GLOBALS["SESION_POS"]]);
			$idpos=$oSession->VSidpos;
			
			$array_response = array();
			
			$BD=new BD();
		    $BD->conectar();
			
			$sql = "select movil,valor,id_servicio,id_item_servicio,fecha,horaregistro,transacionidresp,horatransaccion from venta__servicios where id_punto=$idpos and estado = 1";
			
			if($fecha_ini != "" && $fecha_fin != "")
			{
				$sql .= " and fecha >= '$fecha_ini' and fecha <= '$fecha_fin'";
			}else if($fecha_ini != "" && $fecha_fin == "")
			{
				$sql .= " and fecha = '$fecha_ini'";
			}else if($fecha_ini == "" && $fecha_fin != "")
			{
				$sql .= " and fecha = '$fecha_fin'";
			}
			
			if($num_m != ""){
				$sql .= " and movil = '$num_m'";
			}
			
			if($opers != ""){
				$sql .= " and id_servicio = '$opers'";
			}
			
			$sql .= " ORDER BY id desc";
			
			$res = $BD->consultar($sql);
			
			if($BD->numreg($res)>0){
				while(!$res ->EOF){
					
					$des_serv = "Paquetigo";
					$paque = "N/A";
				
					
					if($res->fields["id_item_servicio"] == 0){
						$sql = "select descripcion from operadores where id = ".$res->fields["id_servicio"];
						$res_oper = $BD->consultar($sql);
						$des_serv = $res_oper->fields["descripcion"];
					}else{
						$sql = "select descripcion from paquetigos where codigo = ".$res->fields["id_item_servicio"];
						$res_oper = $BD->consultar($sql);
						$paque  = $res_oper->fields["descripcion"];
					}
			
					$array_response[] = array("num_m" => $res->fields["movil"],"val_servs" => $res->fields["valor"],"des" => $des_serv,"paque" => $paque,"dateTrans" => $res->fields["fecha"],"horaTrans" => $res->fields["horatransaccion"],"horaReg" => $res->fields["horaregistro"],"transac" => $res->fields["transacionidresp"]);
					
					$res ->MoveNext();
				}
			} 
			
			$BD->desconectar();
			
			return $array_response;
			
			
		}
	}
	  
}

?>