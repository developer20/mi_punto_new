<?php
require("../../config/mainModel.php");
//require("../../config/Session.php");
require_once("../../config/f_val.php");

class ValidarPass {
	
	public function EnviarPass($passw,$pass_ant)
	{
		
		if(isset($_SESSION[$GLOBALS["SESION_POS"]])){
			
			$BD=new BD();
    	    $BD->conectar();
			
			$oSession = unserialize($_SESSION[$GLOBALS["SESION_POS"]]);
			$idpunto = $oSession->VSidpos;
			$login = $oSession->VSlogin;
			
			$error = 0;
			
			$pass = str_replace(' ', '', $passw);
			$PasswordLimpio=$BD->InyeccionSql("$pass");
		
			if($PasswordLimpio != $passw){
				
				return 3;
				
			}else{
				
				$sql="SELECT password FROM puntos WHERE idpos=$idpunto";
				$res=$BD->consultar($sql);
				
				$passwordBD = $res->fields["password"];
				
				if(crypt($pass_ant, $passwordBD) != $passwordBD)
				{
					return 4;  // SI EL PASSWORD DIGITADO COMO ANTERIOR ES DIFERENTE AL PASWORD DE LA BD 
				}else{
			
					$pass = movpass($pass);
				
					$BD->consultar("BEGIN");
					
					$sql = "update puntos set password='$pass' where idpos=$idpunto";
					$res = $BD->consultar($sql);
					if(!$res) $error = 1;
					
					if($error==1){
						$BD->consultar("ROLLBACK");
						return 0;
					}elseif($error==0){
						$BD->consultar("COMMIT");
						return 1;
					}
				}
			
			}
			
		}else{
			
			return 2;
		
		}
	}

}
?>