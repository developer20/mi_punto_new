$(document).ready(function() {
	
	$("#form_pass").submit(function(event) {
	 	event.preventDefault();
        autenticarPass();
    });
	
	$( "#pass" ).keypress(function(event) {
		if(window.event){
			keynum = event.keyCode;
		}else{
			keynum = event.which;
		}
		if((keynum>32)){
			  return true;
		}else{
			return false;
		}
	  //validar_espacios(event);
	});
	
	$( "#pass2" ).keypress(function(event) {
		if(window.event){
			keynum = event.keyCode;
		}else{
			keynum = event.which;
		}
		if((keynum>32)){
			  return true;
		}else{
			return false;
		}
	  //validar_espacios(event);
	});
	
	$( "#pass_ant" ).keypress(function(event) {
		if(window.event){
			keynum = event.keyCode;
		}else{
			keynum = event.which;
		}
		if((keynum>32)){
			  return true;
		}else{
			return false;
		}
	  //validar_espacios(event);
	});
	
	$( "#pass" ).change(function() {
		  var cadena = this.value; 
		  validar_pass2(cadena);
	});
	
});

function autenticarPass()
{
	
	pass = document.getElementById("pass").value;
	pass_ant = document.getElementById("pass_ant").value;
	var val = validar_pass();
	
	if(val == 0){

		$.post('modulos/CambioPass/controlador.php',
		{
		  accion: 'EnviarPass',
		  pass : pass,
		  pass_ant : pass_ant,
		},
			function(data, textStatus) {
				if(data == 1){
					BootstrapDialog.confirm("Se ha cambiado tu contraseña exitosamente.", function(result){
			   		if(result) {
						location.href = "?mod=CambioPass";
					}else{
						location.href = "?mod=CambioPass";
					}
					});
					setTimeout(function(){ window.location = "?mod=CambioPass" }, 5000);
				}else if(data == 2){
					BootstrapDialog.confirm("Error, tu sesión ha cerrado", function(result){
			   		if(result) {
						location.href = "?cerrar=S";
					}else{
						location.href = "?cerrar=S";
					}
					});
					setTimeout(function(){ window.location = "?cerrar=S" }, 5000);
				}else if(data == 3){
			   		Notificacion("Error, la contraseña enviada no es una contraseña valida","error");
				}else if(data == 4){
			   		Notificacion("La contraseña anterior enviada no es la misma contraseña que se encuentra registrada","error");
				}else if(data == 0){
					Notificacion("Error al guardar la contraseña","error");
				}
			}
		);	
	
	}else if(val == 2){
		Notificacion("Error, las contraseñas no coinciden","error");	
		document.getElementById("pass2").value = "";
		document.getElementById("pass2").focus();
	}else if(val == 3){
		Notificacion("No se permite campos vacíos","error");
	
	}
}

function validar_pass(){

   pass = document.getElementById("pass").value;
   pass2 = document.getElementById("pass2").value;
   var espacio_blanco    = /\s/;
   var valida = 0;
	
	if(pass=="" || pass2==""){
		valida =  3;
	}else if(pass!="" && pass2!="" && pass!=pass2){
		valida =  2;
	}
	
	return valida;

}

//DESHABILITO LA BARRA ESPACIADORA PARA QUE EL USUARIO NO INGRESE ESPACIOS AL CREAR LA CLAVE NUEVA
function validar_espacios(evt){
	if(window.event){
		keynum = evt.keyCode;
	}else{
		keynum = evt.which;
	}
	if((keynum>32)){
		  return true;
	}else{
		return false;
	}
}

function validar_pass2(cadena) {
		
	var tamano = false
	var mayus = false  
	var minus = false   
	var num = false
	var inyeccion = false      
	var caracter = false
	var tilde = false;
	var espa = false;
	var inyectss = "";
	var conMin = false;
	var conMay = false;
	var conNum = false;
	var inyecciones = [";", "'", "ALTER", "DROP", "SELECT", "FROM", "WHERE", "INSERT", "DELETE", "*", " OR ", " AND ", "%27", "TABLE", "(", ")", "?", "=", "&", ",", ".", "ñ", "Ñ"];  
	var le_con_minu = ['abc', 'bcd', 'cde', 'def', 'efg', 'fgh', 'ghi', 'hij','ijk', 'jkl', 'klm', 'lmn', 'mno', 'nop', 'opq', 'pqr', 'qrs', 'rst', 'stu', 'tuv', 'uvw', 'vwx', 'xyz'];
	var le_con_mayu = ['ABC', 'BCD', 'CDE', 'DEF', 'EFG', 'FGH', 'GHI', 'HIJ','IJK', 'JKL', 'KLM', 'LMN', 'MNO', 'NOP', 'OPQ', 'PQR', 'QRS', 'RST', 'STU', 'TUV', 'UVW', 'VWX', 'XYZ'];
	var num_val = ['012','123','234','345','456','567','678','789'];	
	var numeros="0123456789";
	var patron_may=/[A-Z]/g; 
	var patron_min=/[a-z]/g;
	var patron_til = /[áéíóúàèìòù´`]/g;
	
	 for(i=0;i<cadena.length;i++){  
		
		if (cadena.match(patron_may) != null){
			
			mayus=true
		}

		if (cadena.match(patron_til) != null){
			tilde = true;
		}
		
		if (cadena.match(patron_min) != null){
			minus = true;
		}
		
		if(numeros.indexOf(cadena.charAt(i),0)!=-1) 
		{  
		   num=true  
		 
		}
		
		for(var n=0;n<le_con_minu.length;n++){
			if(cadena.indexOf(le_con_minu[n]) != -1) {
			   conMin = true;
			}
		}
		
		for(var y=0;y<le_con_mayu.length;y++){
			if(cadena.indexOf(le_con_mayu[y]) != -1) {
			   conMay = true;
			}
		}
		
		for(var z=0;z<num_val.length;z++){
			if(cadena.indexOf(num_val[z]) != -1) {
			   conNum = true;
			}
		}
		
		if ( cadena.charAt(i) == " " ) {
			espa = true;
		}
		
	}

	for(i=0;i<cadena.length;i++){  
		
		cadena = cadena.toUpperCase();
		for(var z=0;z<inyecciones.length;z++){
			if(cadena.indexOf(inyecciones[z]) != -1) {
			   inyeccion = true;
			   inyectss = inyecciones[z];
			}
		}
		
	} 
	
	if(cadena.length < 7 || cadena.length > 12){
		tamano = true;
	} 

	
	if(espa == true){
	   Notificacion("Error, no se permite que la contraseña contenga espacios","error");
	   document.getElementById("pass").value="";
	   document.getElementById("pass").focus()
	}else if(inyeccion == true){
	   Notificacion("No debe contener palabras o caracteres como "+inyectss+" entre la contraseña","error");
	   document.getElementById("pass").value="";
	   document.getElementById("pass").focus()
	   return false 
	}else if(!num)
	{  
		Notificacion("Error, la contraseña debe tener minimo un número","error");
		document.getElementById("pass").value="";
		document.getElementById("pass").focus()
		return false //cambiar false por true para hacer el submit
	}else if(!mayus){
		Notificacion("Error, la contraseña debe tener minimo una mayuscula","error");
		document.getElementById("pass").value="";
		document.getElementById("pass").focus()
		return false //cambiar false por true para hacer el submit 
	}else if(!minus){
		Notificacion("Error, la contraseña debe tener minimo una minuscula","error");
		document.getElementById("pass").value="";
		document.getElementById("pass").focus()
		return false //cambiar false por true para hacer el submit 
	}else if(tilde){
		Notificacion("Error, la contraseña debe tener no debe tener tildes","error");
		document.getElementById("pass").value="";
		document.getElementById("pass").focus()
		return false //cambiar false por true para hacer el submit 
	}else if(tamano==true)
	{  
		Notificacion("Error, la contraseña nueva debe ser mínimo de 7 caracteres y máximo de 12","error");
		document.getElementById("pass").value="";
		document.getElementById("pass").focus()
		return false //cambiar false por true para hacer el submit 
	}else if(conMin==true)
	{  
		Notificacion("Error, la contraseña no puede tener mas de 3 letras minusculas consecutivas","error");
		document.getElementById("pass").value="";
		document.getElementById("pass").focus()
		return false //cambiar false por true para hacer el submit 
	}else if(conMay==true)
	{  
		Notificacion("Error, la contraseña no puede tener mas de 3 letras mayusculas consecutivas","error");
		document.getElementById("pass").value="";
		document.getElementById("pass").focus()
		return false //cambiar false por true para hacer el submit 
	}else if(conNum==true)
	{  
		Notificacion("Error, la contraseña no puede tener mas de 3 numeros consecutivos","error");
		document.getElementById("pass").value="";
		document.getElementById("pass").focus()
		return false //cambiar false por true para hacer el submit 
	}  
}

// FUNCIONES PARA MOSTRAR Y OCULTAR LOS MENSAJES DE AYUDA EN LOS CAMPOS
onload=function() 
{
	cAyuda=document.getElementById("mensajesAyuda");
	cNombre=document.getElementById("ayudaTitulo");
	cTex=document.getElementById("ayudaTexto");
	divTransparente=document.getElementById("transparencia");
	divMensaje=document.getElementById("transparenciaMensaje");
	ayuda=new Array();
	ayuda["Contraseña Nueva"]="<strong><li type='disc'>La nueva clave debe ser minimo de 7 caracteres y máximo de 12</li><li type='disc'>Debe contener letras y números</li><li type='disc'>Debe contener como mínimo una letra mayúscula</li><li type='disc'>Debe contener letras minúsculas</li><li type='disc'>No debe contener espacios</li><li type='disc'>No debe contener comillas simple ni dobles</li><li type='disc'>No debe contener 3 letras consecutivas</li><li type='disc'>No debe contener 3 números consecutivos</li><li type='disc'>No debe contener 3 letras simultaneas</li><li type='disc'>No debe contener 3 números simultaneos</li><li type='disc'>No debe ingresar una contraseña anterior</li><li type='disc'>No debe contener palabras como alter, drop, select, from, where, insert, delete, *, or, and, table, () , ?, =, &</li><li type='disc'>Si el mensaje de validacion es : '<font color='#DF0101'>Contraseña de usuario no permitida</font>' significa que estas utilizando palabras que son reservadas del sistema ya mencionadas anteriormente y debes cambiarlo para poder continuar</li></strong>";
	
	ayuda["Usuario Login"]="<strong><li type='disc'>El nombre de usuario no debe de contener espacios.</li><li type='disc'>El nombre de usuario debe tener minimo 6 caracteres y maximo 15.</li><li type='disc'>No debe contener comillas simple ni dobles</li><li type='disc'>No debe contener 3 letras consecutivas</li><li type='disc'>No debe contener 3 números consecutivos</li><li type='disc'>No debe contener 3 letras simultaneas</li><li type='disc'>No debe contener 3 números simultaneos</li><li type='disc'>No debe contener palabras como alter, drop, select, from, where, insert, delete, *, or, and, table, () , ?, =, &</li><li type='disc'>Si el mensaje de validacion es : '<font color='#DF0101'>Nombre de usuario invalido</font>' significa que estas utilizando palabras que son reservadas del sistema y debes cambiarlo &nbsp;&nbsp;&nbsp;para poder continuar</li></strong>";

}
function ocultaMensaje()
{
	divTransparente.style.display="none";
}

function muestraMensaje(mensaje)
{
	divMensaje.innerHTML=mensaje;
	divTransparente.style.display="block";
}

if(navigator.userAgent.indexOf("MSIE")>=0)
	navegador=0;
	else
	navegador=1;

function colocaAyuda(event)
{
	if(navegador==0)
	{
		var corX=window.event.clientX+document.documentElement.scrollLeft;
		var corY=window.event.clientY+document.documentElement.scrollTop;
	}
	else
	{
		var corX=event.clientX+window.scrollX;
		var corY=event.clientY+window.scrollY;
	}
	cAyuda.style.top=corY-130+"px";
	cAyuda.style.left=corX-560+"px";
}

function ocultaAyuda()
{
	cAyuda.style.display="none";
	if(navegador==0) 
	{
		document.detachEvent("onmousemove", colocaAyuda);
		document.detachEvent("onmouseout", ocultaAyuda);
	}
	else 
	{
		document.removeEventListener("mousemove", colocaAyuda, true);
		document.removeEventListener("mouseout", ocultaAyuda, true);
	}
}

function muestraAyuda(event, campo)
{
 colocaAyuda(event);
	if(navegador==0) 
	{ 
		document.attachEvent("onmousemove", colocaAyuda); 
		document.attachEvent("onmouseout", ocultaAyuda); 
	}
	else 
	{
		document.addEventListener("mousemove", colocaAyuda, true);
		document.addEventListener("mouseout", ocultaAyuda, true);
	}
	
	cNombre.innerHTML=campo;
	cTex.innerHTML=ayuda[campo];
	cAyuda.style.display="block";
}
