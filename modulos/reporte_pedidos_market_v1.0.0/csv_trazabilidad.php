<?php

ini_set('memory_limit', '3000M');
set_time_limit(3000);

if (!isset($_SESSION)) {
  session_start();
}
require_once("../../config/Session.php");
require("../../config/mainModel.php");

$BD = new BD();
$BD->conectar();

$variables = $_GET;
$fecha_inicio = $variables['fecha_inicio'];
$fecha_fin = $variables['fecha_fin'];
$ids = $variables['ids'];
$bd_distri = $BD->bd_distri();
$condiccion = ""; 

  if($fecha_inicio != ""){
    $condiccion .= " AND m.fecha >= '$fecha_inicio' ";
  }
  if($fecha_fin != ""){
    $condiccion .= " AND m.fecha <= '$fecha_fin' ";
  }
  if($ids != ""){
    $condiccion .= " AND m.id_pedido IN ($ids)";
  }  

  $consulta = "SELECT 
                     m.id_pedido,
                     m.fecha, 
                     m.hora,
                     IF(u.nombre IS NULL,CONCAT('Punto: ',m.id_usuario_accion),CONCAT(u.nombre,' ',u.apellido)) AS usuario,                      
                     CASE                                
                          WHEN m.estado = 1 THEN 'Aprobado'
                          WHEN m.estado = 2 THEN 'Rechazado por Bodega'
                          WHEN m.estado = 3 THEN 'Picking'
                          WHEN m.estado = 4 THEN 'En Reparto'
                          WHEN m.estado = 5 THEN 'Entregado'
                          WHEN m.estado = 6 THEN 'Rechazado por Punto'
                          WHEN m.estado = 0 THEN 'Solicitado' 
                      END estado
                FROM $bd_distri.marketplace__pedido_auditoria AS m
          LEFT JOIN $bd_distri.usuarios AS u ON (u.id = m.id_usuario_accion)
             WHERE 1 $condiccion
            GROUP BY m.id
            ORDER BY m.id ASC";              

$datos = $BD->devolver_array($consulta);

if (count($datos) > 0) {
  $nombre_archivo = "Reporte_Pedidos_Trazabilidad.CSV";
  $encabezados = "Nro Pedido; Fecha; Hora; Estado; Usuario";
  $llaves = array(
    "id_pedido",
    "fecha",
    "hora",
    "estado",
    "usuario"
  );

  /*Se llama el metodo que comprime el archivo. Los argumentos son:
  1: nombre del archivo zip
  2: nombre del archivo que se va a comprimir
  3: Encabezado del archivo que se va a comprimir
  4: Datos que va conterner el archivo que se va a comprimir (es un arreglo)
  5: Llave de los datos
  */
  $BD->comprimir_archivo("Reporte_Pedidos_Trazabilidad", $nombre_archivo, $encabezados, $datos, $llaves);
}

?>
