<?php
require("../../config/mainModel.php");
class reporte_pedidos
{
    /*
 METODOS DE LA BD.
 $this->BD->consultar($query); // Ejecuta la consulta y devuelve string.!
 $this->BD->devolver_array_new($query); // Ejecuta la consulta y devuelve array asociativo.!
 $this->BD->consultar("BEGIN"); // Antes de transacciones.!
 $this->BD->consultar("COMMIT"); // Commit para guardar datos.!
 $this->BD->consultar("ROLLBACK"); // Devolver datos si hay error.!
 $this->BD->numreg($query); // Devuelve el numero de registros de la consulta.!
*/
    public function __construct()
    {
        $BD = new BD();
        $this->BD = $BD;
        $this->BD->conectar();        
    }
    public function __destruct()
    {
        $this->BD->desconectar();
    }
    //Espacio Para declara las funciones que retornan los datos de la DB.        

    public function cargar_estado(){
        
        $bd_distri = $this->BD->bd_distri();
        
        $consulta = "SELECT id, nombre FROM $bd_distri.marketplace__estados ORDER BY nombre";        
		$datos = $this->BD->devolver_array($consulta);
		
		return $datos;
    }

    public function Consultar($fecha_inicio,$fecha_fin,$estado){

        $bd_distri = $this->BD->bd_distri();
        $condiccion = "";        			

        if($fecha_inicio != ""){
            $condiccion .= " AND m.fecha >= '$fecha_inicio' ";
        }

        if($fecha_fin != ""){
            $condiccion .= " AND m.fecha <= '$fecha_fin' ";
        }

        if($estado != ""){
            $condiccion .= " AND m.estado = $estado";
        }

        $consulta = "SELECT 
                            m.id,
                            m.fecha, 
                            m.hora, 
                            m.cantidad, 
                            CONCAT('$',' ', CONVERT(FORMAT(m.total, 0) USING utf8)) AS total,
                            CASE                                
                                WHEN m.estado = 1 THEN 'Aprobado'
                                WHEN m.estado = 2 THEN 'Rechazado por Bodega'
                                WHEN m.estado = 3 THEN 'Picking'
                                WHEN m.estado = 4 THEN 'En Reparto'
                                WHEN m.estado = 5 THEN 'Entregado'
                                WHEN m.estado = 6 THEN 'Rechazado por Punto'
                                WHEN m.estado = 0 THEN 'Solicitado' 
                            END estado
                           FROM $bd_distri.marketplace__pedido AS m                           
                          WHERE 1 $condiccion"; 
                                                
        $datos = $this->BD->devolver_array($consulta);
		
        return $datos;
    }

    public function DetallePedido($id){

        $bd_distri = $this->BD->bd_distri();
        $condiccion = "";        			

        $consulta = "SELECT 
                            m.id,
                            r.pn, 
                            r.producto,
                            m.cantidad,
                            CONCAT('$',' ', CONVERT(FORMAT(m.total, 0) USING utf8)) AS total
                       FROM $bd_distri.marketplace__detalle_pedido AS m
                 INNER JOIN $bd_distri.referencias AS r ON (r.id = m.id_referencia)
                      WHERE m.id_pedido = $id
                   GROUP BY m.id_referencia";

        $datos = $this->BD->devolver_array($consulta);
		
        return $datos;             
    }

    public function TrazabilidadPedido($id){

        $bd_distri = $this->BD->bd_distri();
        $condiccion = "";        			

        $consulta = "SELECT 
                            m.fecha, 
                            m.hora,
                            IF(u.nombre IS NULL,CONCAT('Punto: ',m.id_usuario_accion),CONCAT(u.nombre,' ',u.apellido)) AS usuario,                            
                            CASE                                
                                WHEN m.estado = 1 THEN 'Aprobado'
                                WHEN m.estado = 2 THEN 'Rechazado por Bodega'
                                WHEN m.estado = 3 THEN 'Picking'
                                WHEN m.estado = 4 THEN 'En Reparto'
                                WHEN m.estado = 5 THEN 'Entregado'
                                WHEN m.estado = 6 THEN 'Rechazado por Punto'
                                WHEN m.estado = 0 THEN 'Solicitado' 
                            END estado
                       FROM $bd_distri.marketplace__pedido_auditoria AS m
                 LEFT JOIN $bd_distri.usuarios AS u ON (u.id = m.id_usuario_accion)
                      WHERE m.id_pedido = $id
                   GROUP BY m.id
                   ORDER BY m.id ASC";        
        $datos = $this->BD->devolver_array($consulta);
		
        return $datos;             
    }

  
}// Fin clase
