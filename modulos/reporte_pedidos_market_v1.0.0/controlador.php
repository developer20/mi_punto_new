<?php
include_once("../../config/mainController.php"); // Incluye el Controlador Principal
include_once("../../modulos/reporte_pedidos_market_v1.0.0/modelo.php");	// Incluye el Modelo.
$controller = new mainController; // Instancia a la clase MainController
$modelo = new reporte_pedidos(); // Instancia a la clase del modelo
try // Try, manejo de Errores
{
	$metodo = $_SERVER['REQUEST_METHOD'];
	$tipo_res = "";
	$response = null;
	// Se manejaran dos tipos JSON y HTML
	// Dependiendo del método de la petición ejecutaremos la acción correspondiente.
	// Por ahora solo POST, todas las llamadas se haran por POST
    $variables = $_POST;
	if(!isset($_POST['accion'])){echo "0"; return;} // Evita que ocurra un error si no manda accion.
	$accion = $variables['accion'];
		// Dependiendo de la accion se ejecutaran las tareas y se definira el tipo de respuesta.
		switch($accion) {
			case 'cargar_estado':
				$tipo_res = 'JSON'; //Definir tipo de respuesta;				
				$response = $modelo->cargar_estado();
			break;
			case 'Consultar':
				$tipo_res = 'JSON'; //Definir tipo de respuesta;
				$fecha_inicio = $variables['fecha_inicio'];
				$fecha_fin = $variables['fecha_fin'];
				$estado = $variables['estado'];
				$datos = $modelo->Consultar($fecha_inicio,$fecha_fin,$estado);
				$response = array("draw" => 1,"recordsTotal" => 0,"recordsFiltered" => 0,"data" => $datos);
			break;
			case 'DetallePedido':
				$tipo_res = 'JSON'; //Definir tipo de respuesta;
				$id = $variables['id'];
				$response = $modelo->DetallePedido($id);
			break;
			case 'TrazabilidadPedido':
				$tipo_res = 'JSON'; //Definir tipo de respuesta;
				$id = $variables['id'];
				$response = $modelo->TrazabilidadPedido($id);
			break;
 		}

	// Respuestas del Controlador
	if($tipo_res == "JSON")
	{
	  echo json_encode($response,true); // $response será un array con los datos de nuestra respuesta.
	}
	elseif ($tipo_res == "HTML") {
	  echo $response; // $response será un html con el string de nuestra respuesta.
	}
} // Fin Try
catch (Exception $e) {}
