
var dtable;

$(document).ready(function () {

    $(".content-header").find("h1").html("Reporte Estados de pedido");
    cargar_estado();
    $(".fecha").datepicker({
        format: "yyyy-mm-dd",
        todayHighlight: true,
        todayBtn: true
    });

    $("#frm_buscar_pedidos").submit(function (event) {
        event.preventDefault();
        Consultar();
    });

    $("#CSV").click(function () {
        CSV();
    });

    $("#CSVTRAZABILIDAD").click(function () {
        var datos = dtable.data();
        var ids = '';

        $.each(datos, function (index, fila) {
            ids += fila.id + ",";
        });

        ids = ids.substring(0, (ids.length) - 1);
        CSVTRAZABILIDAD(ids);
    });

})

function cargar_estado() {

    $.post("modulos/reporte_pedidos_market_v1.0.0/controlador.php",
        {
            accion: 'cargar_estado'
        },
        function (data) {
            data = JSON.parse(data);
            var html = '<option value = "">SELECCIONAR</option>';
            $.each(data, function (index, val) {
                html += '<option value = "' + val.id + '">' + val.nombre + '</option>';
            });

            $("#estado").html(html).change();
        });
}


function Consultar() {

    fecha_inicio = $("#fecha_inicio").val();
    fecha_fin = $("#fecha_fin").val();
    estado = $("#estado").val();
    f_ini = new Date(fecha_inicio);
    f_fin = new Date(fecha_fin);
    dias = f_fin.getTime() - f_ini.getTime();
    diasDif = Math.round(dias / (1000 * 60 * 60 * 24));

    if (fecha_inicio == "" || fecha_fin == "") {
        Notificacion("La fecha inicial y la fecha final son obligatorios", "warning");
        return false;
    }
    if (fecha_inicio > fecha_fin) {
        Notificacion("La fecha inicial no debe ser mayor a la fecha final", "warning");
        return false;
    }
    if (diasDif > 31) {
        Notificacion("El rango maximo entre las fechas es de 31 dias.", "warning");
        return false;
    }

    if (!$.fn.DataTable.isDataTable('#t_reporte')) {

        dtable = $("#t_reporte").DataTable({

            "ajax": {
                "url": "modulos/reporte_pedidos_market_v1.0.0/controlador.php",
                "type": "POST",
                "deferRender": false,
                "data": {
                    accion: 'Consultar',
                    fecha_inicio: fecha_inicio,
                    fecha_fin: fecha_fin,
                    estado: estado
                }
            },
            "bFilter": false,
            "responsive": true,
            "columns": [
                { "data": "id" },
                { "data": "estado" },
                { "data": "fecha" },
                { "data": "hora" },
                { "data": "cantidad" },
                { "data": "total" },
                { "data": "id" },
                { "data": "id" }
            ],
            "columnDefs": [
                {
                    "targets": 6,
                    "data": "",
                    render: function (data, type, row) {

                        return '<i class="glyphicon glyphicon-eye-open TrazPed" id ="posTraza_' + row.id + '"  data-idPedido="' + row.id + '" style="color: #10628A;font-size: 20px;cursor: pointer;position:relative;"></i>';

                    }
                },
                {
                    "targets": 7,
                    "data": "",
                    render: function (data, type, row) {

                        return '<i class="glyphicon glyphicon-plus-sign desDeta" id = "posDeta_' + row.id + '" data-idPedido="' + row.id + '" style="color: #10628A;font-size: 20px;cursor: pointer;position:relative;"></i>';

                    }
                },
            ],

            fnDrawCallback: function () {

                $(".desDeta").unbind("click");
                $(".desDeta").click(function () {

                    var data = dtable.row($(this).parents('tr')).data();
                    var id = data['id'];
                    var elemento = $("#posDeta_" + id);
                    var tr = $(this).closest('tr');
                    var row = dtable.row(tr);
                    var tabla = "";

                    if (elemento.hasClass("desDeta")) {

                        elemento.attr("class", "glyphicon glyphicon-minus-sign CloseDeta");

                        $.post('modulos/reporte_pedidos_market_v1.0.0/controlador.php',
                            {
                                accion: 'DetallePedido',
                                id: id
                            },
                            function (data, textStatus) {

                                if (data != "") {
                                    var datos = JSON.parse(data);

                                    if (datos != "") {

                                        tabla += "<div class='col-md-12' style='border:2px #ccc solid; border-radius:2px;'>";
                                        tabla += '<table class="table table-hovered table-condensed">';
                                        tabla += '<thead>';
                                        tabla += '<tr>';
                                        tabla += '<th align="center">SKU</th>';
                                        tabla += '<th align="center">Nombre Referencia</th>';
                                        tabla += '<th align="center">Cant Solicitada</th>';
                                        tabla += '<th align="center">Valor Uni.</th>';
                                        tabla += '</tr>';
                                        tabla += '</thead>';
                                        $.each(datos, function (index, fila) {
                                            tabla += '<tr>';
                                            tabla += '<td >' + fila.pn + '</td>';
                                            tabla += '<td>' + fila.producto + '</td>';
                                            tabla += '<td>' + fila.cantidad + '</td>';
                                            tabla += '<td>' + fila.total + '</td>';
                                            tabla += '</tr>';
                                        });
                                        tabla += '</table>';
                                        tabla += "</div>";
                                    }else{
                                        Notificacion("No se encontraron resultados", "warning");
                                        return false;
                                    }
                                    row.child(tabla).show();
                                    tr.addClass('shown');
                                }
                            }
                        );

                    } else {
                        elemento.attr("class", "glyphicon glyphicon-plus-sign desDeta");
                        row.child.hide();
                        tr.removeClass('shown');                        
                    }
                });

                $(".TrazPed").unbind("click");
                $(".TrazPed").click(function () {

                    var data = dtable.row($(this).parents('tr')).data();
                    var id = data['id'];
                    var elemento = $("#posTraza_" + id);
                    var tr = $(this).closest('tr');
                    var row = dtable.row(tr);
                    var tabla = "";

                    if (elemento.hasClass("TrazPed")) {

                        elemento.attr("class", "glyphicon glyphicon-eye-open CloseTrazPed");

                        $.post('modulos/reporte_pedidos_market_v1.0.0/controlador.php',
                            {
                                accion: 'TrazabilidadPedido',
                                id: id
                            },
                            function (data, textStatus) {

                                if (data != "") {
                                    var datos = JSON.parse(data);

                                    if (datos != "") {

                                        tabla += "<div class='col-md-12' style='border:2px #ccc solid; border-radius:2px;'>";
                                        tabla += '<table class="table table-hovered table-condensed">';
                                        tabla += '<thead>';
                                        tabla += '<tr>';
                                        tabla += '<th align="center">Fecha</th>';
                                        tabla += '<th align="center">Hora</th>';
                                        tabla += '<th align="center">Estado</th>';
                                        tabla += '<th align="center">Usuario Acción</th>';
                                        tabla += '</tr>';
                                        tabla += '</thead>';
                                        $.each(datos, function (index, fila) {
                                            tabla += '<tr>';
                                            tabla += '<td >' + fila.fecha + '</td>';
                                            tabla += '<td>' + fila.hora + '</td>';
                                            tabla += '<td>' + fila.estado + '</td>';
                                            tabla += '<td>' + fila.usuario + '</td>';
                                            tabla += '</tr>';
                                        });
                                        tabla += '</table>';
                                        tabla += "</div>";
                                    }else{
                                        Notificacion("No se encontraron resultados", "warning");
                                        return false;
                                    }
                                    row.child(tabla).show();
                                    tr.addClass('shown');
                                }
                            }
                        );
                    } else {
                        elemento.attr("class", "glyphicon glyphicon-eye-open TrazPed");
                        row.child.hide();
                        tr.removeClass('shown');
                    }
                });
            }
        });
    }
    else {
        dtable.destroy();
        Consultar();
    }
    $("#consulta").show();
}

function CSV() {

    fecha_inicio = $("#fecha_inicio").val();
    fecha_fin = $("#fecha_fin").val();
    estado = $("#estado").val();

    window.open("modulos/reporte_pedidos_market_v1.0.0/csv.php?fecha_inicio=" + fecha_inicio + "&fecha_fin=" + fecha_fin + "&estado=" + estado);
}

function CSVTRAZABILIDAD(ids) {

    fecha_inicio = $("#fecha_inicio").val();
    fecha_fin = $("#fecha_fin").val();

    window.open("modulos/reporte_pedidos_market_v1.0.0/csv_trazabilidad.php?fecha_inicio=" + fecha_inicio + "&fecha_fin=" + fecha_fin + "&ids=" + ids);
}

$(document).ajaxSend(function (event, request, settings) {
    loader = $('body').loadingIndicator({
        useImage: false,
    }).data("loadingIndicator");

    loader.show();
});