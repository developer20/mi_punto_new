<?php

ini_set('memory_limit', '3000M');
set_time_limit(3000);

if (!isset($_SESSION)) {
  session_start();
}
require_once("../../config/Session.php");
require("../../config/mainModel.php");

$BD = new BD();
$BD->conectar();

$variables = $_GET;
$fecha_inicio = $variables['fecha_inicio'];
$fecha_fin = $variables['fecha_fin'];
$estado = $variables['estado'];
$bd_distri = $BD->bd_distri();
$condiccion = "";

  if($fecha_inicio != ""){
    $condiccion .= " AND m.fecha >= '$fecha_inicio' ";
  }
  if($fecha_fin != ""){
    $condiccion .= " AND m.fecha <= '$fecha_fin' ";
  }
  if(isset($estado)){
    if($estado != ""){
      $condiccion .= " AND m.estado = $estado";
    }
  }

  $consulta = "SELECT 
                    m.id, 
                    m.fecha, 
                    m.hora,
                    CASE                                
                        WHEN m.estado = 1 THEN 'Aprobado'
                        WHEN m.estado = 2 THEN 'Rechazado por Bodega'
                        WHEN m.estado = 3 THEN 'Picking'
                        WHEN m.estado = 4 THEN 'En Reparto'
                        WHEN m.estado = 5 THEN 'Entregado'
                        WHEN m.estado = 6 THEN 'Rechazado por Punto'
                        WHEN m.estado = 0 THEN 'Solicitado' 
                    END estado
               FROM $bd_distri.marketplace__pedido AS m
            WHERE 1 $condiccion";

$datos = $BD->devolver_array($consulta);

if (count($datos) > 0) {
  $nombre_archivo = "Reporte_Pedidos.csv";
  $encabezados = "Nro Pedido; Fecha; Hora; Estado;";
  $llaves = array(
    "id",
    "fecha",
    "hora",
    "estado"
  );

  /*Se llama el metodo que comprime el archivo. Los argumentos son:
  1: nombre del archivo zip
  2: nombre del archivo que se va a comprimir
  3: Encabezado del archivo que se va a comprimir
  4: Datos que va conterner el archivo que se va a comprimir (es un arreglo)
  5: Llave de los datos
  */
  $BD->comprimir_archivo("Reporte_Pedidos", $nombre_archivo, $encabezados, $datos, $llaves);
}

?>
