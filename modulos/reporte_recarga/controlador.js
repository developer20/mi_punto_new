var dtable;

$ (document).ready (function () {
	cargar_operadores ();

	$ (".fecha").datepicker ({
		format: "yyyy-mm-dd",
		todayHighlight: true,
		todayBtn: true
	});

	$ ("#btn_buscar").click (function () {
		consulta_reporte ("tbl_recarga");
	});

	$ ("#CSV").click (function () {
		var f_ini = $ ("#f_ini").val (),
			f_fin = $ ("#f_fin").val (),
			operador = $ ("#operador").val (),
			movil = $ ("#movil").val ();

		window.open ("modulos/reporte_recarga/csv.php?f_ini=" + f_ini + "&f_fin=" + f_fin + "&operador=" + operador + "&movil=" + movil);
	});
});

function cargar_operadores () {
	$.post ("modulos/reporte_recarga/controlador.php", {
		accion: "cargar_operadores"
	}, function (data, textStatus) {
		var html = "<option value=''>Seleccionar...</option>";

		data = JSON.parse (data);

		$.each (data, function (index, row) {
			html += "<option value="+row.id+" >"+row.descripcion+"</option>";
		});

		$ ("#operador").html (html).change ();
	});
}

function consulta_reporte (id) {
	var f_ini = $ ("#f_ini").val (),
		f_fin = $ ("#f_fin").val (),
		operador = $ ("#operador").val (),
		movil = $ ("#movil").val ();

	if (f_ini != "" && f_fin != "") {
		if ($.fn.DataTable.isDataTable ("#" + id)) {
			dtable.destroy ();
		}

		dtable = $ ("#" + id).DataTable ({
			"ajax": {
				"url": "modulos/reporte_recarga/controlador.php",
				"type": "POST",
				"deferRender": false,
				"data":{
					accion: "consulta_reporte",
					f_ini: f_ini,
					f_fin: f_fin,
					operador: operador,
					movil: movil
				}
			}, "bFilter": false,
			"responsive":true,
			"columns":[
				{ "data": "fecha" },
				{ "data": "horaregistro" },
				{ "data": "bolsa" },
				{ "data": "movil" },
				{ "data": "valor" },
				{ "data": "saldo_anterior_incentivo"},
				{ "data": "desc_incentivo" },
				{ "data": "saldo_final_incent" },
				{ "data": "saldo_anterior" },
				{ "data": "desc_saldo" },
				{ "data": "saldo_final" },
				{ "data": "operador" },
				{ "data": "proveedor" },
				{ "data": "acceso" },
				{ "data": "tipo" },
				{ "data": "tipo_desc" }
			],"columnDefs":[{
				"targets": 3,
				"data": "valor",
				render: function (data, type, row) {
						 return data;
					}
				}, {
					"targets": 4,
					"data": "saldo_anterior_incentivo",
					render: function (data, type, row) {
						return format (data, 0);
					}
				}, {
					"targets": 5,
					"data": "desc_incentivo",
					render: function (data, type, row) {
						return format (data, 0);
					}
				}, {
					"targets": 6,
					"data": "saldo_final_incent",
					render: function (data, type, row) {
						return format (data, 0);
					}
				}, {
					"targets": 7,
					"data": "saldo_anterior",
					render: function (data, type, row) {
						return format (data, 0);
					}
				}, {
					"targets": 8,
					"data": "desc_saldo",
					render: function (data, type, row) {
						return format (data, 0);
					}
				}, {
					"targets": 9,
					"data": "saldo_final",
					render: function (data, type, row) {
						return format (data, 0);
					}
				}
			]
		});

		$ ("#consulta").show ();
	} else {
		$ ("#consulta").hide ();

		Notificacion ("La fecha inicial y final son obligatorias.", "warning");
	}
}