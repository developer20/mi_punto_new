<?php
require ("../../config/mainModel.php");

class reporte_recarga {
  /*
    Metodos de la DB.
    $this->consultar_datos($query); // Realiza la consulta y devuelve un Array Asociativo con los datos
    $this->ejecuta_query($query); // Ejecuta Insert, Update, Delete, Alter, Drop.. / devuelve el ultimo id afectado.
    $this->total_registros($columna, $tabla, $where = ""); // Devuelve el numero de registros. count();
    $this->trae_uno($query); // Realiza una consulta, devolviendo el primer valor que encuentre.
    $this->begin_work();
    $this->commit();
    $this->rollback();

    VARIABLES GLOBALES SISTEMA
    $GLOBALS["BD_NAME"];
    $GLOBALS["SERVER"];
    $GLOBALS["SERVER_USER"];
    $GLOBALS["SERVER_PASS"];
    $GLOBALS["BD_DIS"];
    $GLOBALS["BD_POS"];
    $GLOBALS["SESION_OP"];
    $GLOBALS["SESION_DIS"];
    */

  public function __construct ()	{
		$BD = new BD ();
    $this->BD = $BD;
    $this->BD->conectar ();
    $oSession = unserialize ($_SESSION[$GLOBALS["SESION_POS"]]);
    $this->idpos = $oSession->VSidpos;
  }

  public function cargar_operadores () {
    return $this->BD->devolver_array ("SELECT id, descripcion FROM operadores WHERE estado = 1;");
  }

  public function consulta_reporte ($f_ini, $f_fin, $operador, $movil) {
    $condicion = "";

    if ($operador != "") {
      $condicion .= " AND vs.operador = $operador ";
    }

    if ($movil != "") {
      $condicion .= " AND vs.movil = $movil ";
    }

    $sql = "SELECT
              r_up.descripcion AS bolsa,
              vs.fecha,
              vs.horaregistro,
              vs.movil,
              vs.valor,
              op.descripcion AS operador,
              IF(vs.id_item_servicio > 0, 'Paquete', 'Recarga') AS tipo,
              IF(id_item_servicio > 0, p.descripcion, 'Recarga') AS tipo_desc,
              vs.saldo_anterior_incentivo,
              IF(op.id = 6 AND vs.valor > vs.saldo_anterior_incentivo, vs.saldo_anterior_incentivo, IF(op.id = 6 AND vs.valor <= vs.saldo_anterior_incentivo, vs.valor, 0)) AS desc_incentivo,
              vs.saldo_actual_incentivo AS saldo_final_incent,
              vs.saldo_anterior,
              IF(op.id = 6 AND vs.valor > vs.saldo_anterior_incentivo, vs.valor - vs.saldo_anterior_incentivo, IF(op.id = 6 AND vs.valor <= vs.saldo_anterior_incentivo, 0, vs.valor)) AS desc_saldo,
              vs.saldo_actual AS saldo_final,pro.nombre AS proveedor,IF(vs.acceso = 0,'Web',IF(vs.acceso = 1,'App','Indefinido')) AS acceso
            FROM venta__servicios AS vs
            INNER JOIN operadores AS op ON (vs.operador = op.id)
            INNER JOIN proveedores AS pro ON (pro.id = vs.id_proveedor)
            LEFT JOIN paquetigos AS p ON (p.id = vs.id_item_servicio)
            LEFT JOIN {$GLOBALS["BD_NAME"]}.recargas__usudistri_prov AS r_up ON (r_up.id_distri = vs.id_distri AND r_up.id_proveedor = vs.id_proveedor)
            WHERE
              vs.estado = 1 AND
              vs.id_punto = $this->idpos AND
              vs.fecha >= '$f_ini' AND
              vs.fecha <= '$f_fin'
            $condicion
            GROUP BY vs.id;";

    return $this->BD->devolver_array ($sql);
  }
}
?>