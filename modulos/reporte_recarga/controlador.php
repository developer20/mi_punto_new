<?php
include_once ("../../config/mainController.php"); // Incluye el Controlador Principal
include_once ("../../modulos/reporte_recarga/modelo.php"); // Incluye el Modelo

$controller = new mainController; // Instancia a la clase MainController
$modelo = new reporte_recarga (); // Instancia a la clase del modelo

// Manejo de errores
try {
	$metodo = $_SERVER["REQUEST_METHOD"];
	$tipo_res = "";
	$response = null; 
	$variables = $_POST;
	
	// Evita que ocurra un error si no manda accion
	if (!isset ($_POST["accion"])) { echo "0"; return; }

	$accion = $variables["accion"];
	
	// Dependiendo de la accion se ejecutaran las tareas y se definira el tipo de respuesta
	switch ($accion) {
			case 'cargar_operadores':
				 $tipo_res = 'JSON';
				 $response = $modelo->cargar_operadores();
			break;	
			case 'consulta_reporte':
				 $tipo_res = 'JSON'; //Definir tipo de respuesta;
				 $f_ini = $variables["f_ini"];
				 $f_fin = $variables["f_fin"];
				 $operador = $variables["operador"];
				 $movil = $variables["movil"];
				 $datos = $modelo->consulta_reporte($f_ini,$f_fin,$operador,$movil);
				 $response = array("draw" => 1,
							   "recordsTotal" => 0,
							   "recordsFiltered" => 0,
							   "data" => $datos);
			break;	
		}

	// Respuestas del Controlador
	if($tipo_res == "JSON")
	{
	  echo json_encode($response,true); // $response será un array con los datos de nuestra respuesta.
	}
	elseif ($tipo_res == "HTML") {
	  echo $response; // $response será un html con el string de nuestra respuesta.
	}
} // Fin Try
catch (Exception $e) {}
