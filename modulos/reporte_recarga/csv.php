<?php
ini_set ("memory_limit", "3000M");
set_time_limit (3000);

if (!isset ($_SESSION)) {
	session_start ();
}

require_once ("../../config/Session.php");
require ("../../config/mainModel.php");

$BD = new BD ();
$BD->conectar ();
$oSession = unserialize ($_SESSION[$GLOBALS["SESION_POS"]]);
$idpos = $oSession->VSidpos;

header ("Content-type: application/vnd.ms-excel");
header ("Content-Disposition:  filename=\"REPORTE_RECARGA.csv\";");

$variables = $_GET;
$condicion = "";
$f_ini = $variables["f_ini"];
$f_fin = $variables["f_fin"];
$operador = $variables["operador"];
$movil = $variables["movil"];

if ($operador != "") {
    $condicion .= " AND vs.operador = $operador ";
}

if ($movil != "") {
    $condicion .= " AND vs.movil = $movil ";
}

$sql = "SELECT
          r_up.descripcion AS bolsa,
          vs.fecha,
          vs.horaregistro,
          vs.movil,
          vs.valor,
          op.descripcion AS operador,
          IF(vs.id_item_servicio > 0, 'Paquete', 'Recarga') AS tipo,
          IF(id_item_servicio > 0, p.descripcion, 'Recarga') AS tipo_desc,
          vs.saldo_anterior_incentivo,
          IF(op.id = 6 AND vs.valor > vs.saldo_anterior_incentivo, vs.saldo_anterior_incentivo, IF(op.id = 6 AND vs.valor <= vs.saldo_anterior_incentivo, vs.valor, 0)) AS desc_incentivo,
          vs.saldo_actual_incentivo AS saldo_final_incent,
          vs.saldo_anterior,
          IF(op.id = 6 AND vs.valor > vs.saldo_anterior_incentivo, vs.valor - vs.saldo_anterior_incentivo, IF(op.id = 6 AND vs.valor <= vs.saldo_anterior_incentivo, 0, vs.valor)) AS desc_saldo,
          vs.saldo_actual AS saldo_final,pro.nombre AS proveedor,IF(vs.acceso = 0,'Web',IF(vs.acceso = 1,'App','Indefinido')) AS acceso
        FROM venta__servicios AS vs
        INNER JOIN operadores AS op ON (vs.operador = op.id)
        INNER JOIN proveedores AS pro ON (pro.id = vs.id_proveedor)
        LEFT JOIN paquetigos AS p ON (p.id = vs.id_item_servicio)
        LEFT JOIN {$GLOBALS["BD_NAME"]}.recargas__usudistri_prov AS r_up ON (r_up.id_distri = vs.id_distri AND r_up.id_proveedor = vs.id_proveedor)
        WHERE
          vs.estado = 1 AND
          vs.id_punto = $idpos AND
          vs.fecha >= '$f_ini' AND
          vs.fecha <= '$f_fin'
        $condicion
        GROUP BY vs.id;";

$resultado = $BD->consultar ($sql);

// headers
echo "FECHA;HORA;BOLSA;NUMERO;VALOR;SALDO ANTERIOR INCENTIVO;DESCUENTO INCENTIVO;SALDO FINAL INCENTIVO;SALDO ANTERIOR;DECUENTO SALDO;SALDO FINAL;OPERADOR;PROVEEDOR;ACCESO;TIPO;PAQUETE";

if ($BD->numreg ($resultado) > 0) {
	while (!$resultado->EOF) {
		echo "\n".
			$resultado->fields["fecha"].";".
            $resultado->fields["horaregistro"].";".
            $resultado->fields["bolsa"].";".
			$resultado->fields["movil"].";".
			$resultado->fields["valor"].";".
			$resultado->fields["saldo_anterior_incentivo"].";".
			$resultado->fields["desc_incentivo"].";".
			$resultado->fields["saldo_final_incent"].";".
            $resultado->fields["saldo_anterior"].";".
            $resultado->fields["desc_saldo"].";".
            $resultado->fields["saldo_final"].";".
            $resultado->fields["operador"].";".
            $resultado->fields["proveedor"].";".
            $resultado->fields["acceso"].";".
            $resultado->fields["tipo"].";".
            $resultado->fields["tipo_desc"];

		$resultado->MoveNext ();
	}
}
?>