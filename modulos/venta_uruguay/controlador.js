
var key_pes = 0;
var id_oper = 0;
var id_pro = 0;
var moneda = "";
var pais = "";
var max_movil = "";
var min_movil = "";

$(document).ready(function() {

	 CargarBols();
	 CargarOpers();

	 $("#frmRe").submit(function() {
		 event.preventDefault();
		 validate();
     });

	 $("#valorotro").keypress(function(e) {
		var key;
		if(window.event){ // IE
			key = e.keyCode;
		}else if(e.which){ // Netscape/Firefox/Opera
			key = e.which;
		}
		if (key == 13){
		  validar();
		}
		else if ((pais == 2 && key < 46) || (pais != 2 && key < 48) || key > 57){
			return false;
		}
		return true;
		  //validar_espacios(event);
	});

	$("#num_m").keypress(function(e) {
		var key;
		if(window.event){ // IE
			key = e.keyCode;
		}else if(e.which){ // Netscape/Firefox/Opera
			key = e.which;
		}
		if (key == 13){
		  validar();
		}
		else if (key < 48 || key > 57){
			return false;
		}
		return true;
		  //validar_espacios(event);
	});

	$("#cancelar").click(function() {
		window.location = "?mod=venta_uruguay"
	});

	$("#ultimeTransac").click(function() {
		ultimas_transac();
	});

});


function CargarBols()
{
	$.post('modulos/venta_uruguay/controlador.php',
		{
			accion: 'CargarBols',

		},
		function(data, textStatus) {

			var datos = JSON.parse(data);

			if(datos["estado"] == 1){

				$("#bolsa").html("<b>"+datos["moneda"]+" "+addCommas(datos["saldo"])+"</b>");
				$("#bolsa_in").html("<b>"+datos["moneda"]+" "+addCommas(datos["saldo_in"])+"</b>");
				$("#saldoac").val(datos["saldo"]);
				$("#saldoacIn").val(datos["saldo_in"]);
				pais = datos["pais"];
				moneda = datos["moneda"];
				max_movil = datos["max_movil"];
				min_movil = datos["min_movil"];
				//$("#moneda").html(datos["moneda"]);

				key_pes = datos["val_pes"];

			}else if(datos["estado"] == 0){
				Notificacion(datos["msg"],"error");
				setTimeout("window.location='?cerrar=S'",3000);
			}else if(datos["estado"] == -1){
				$("#recargas").html("");
				Notificacion(datos["msg"],"error");
			}


		}
	);
}

function CargarOpers()
{

	$.post('modulos/venta_uruguay/controlador.php',
		{
			accion: 'CargarOpers',
			tipo: '1',

		},
		function(data, textStatus) {

			if(data != 0 || data != "")
			{

				var datos = JSON.parse(data);
				var operadores = "";
				var valores = "";
				//if(datos != ""){
					if(datos["operadores"].length > 0){

						var operador = datos["operadores"];
						var valor = datos["valores"];

						$.each(operador,function(index, fila) {

							if(fila.id == 6){
								$("#oper_movistar").show();
							}else{

								$("#otros_oper").show();

								operadores += '<span class="cssToolTip">';
								operadores += "<input type='radio' id='"+fila.id+"' name='servicio' value='"+fila.id+"-"+fila.titulo+"-"+fila.paquetes+"' onClick='loaddescrip(this.value)'>";
								operadores += '<label for="'+fila.id+'"><img src="template/img/operadores/'+fila.imagen+'" alt="'+fila.titulo+'"></label>';
								operadores += '<div> '+fila.titulo+' </div>';
								operadores += '</span>';

							}

						});

						$("#ver_operadores").html(operadores);

						$.each(valor,function(index, fila2) {

							valores += '<input type="radio" id="'+fila2.valor+'" name="'+fila2.valor+'" value="'+fila2.valor+'" onClick="clearvalue(this.value)">';
                            valores += '<label for="'+fila2.valor+'" style="font-size:25px;">'+addCommas(fila2.valor)+'</label>';

						});

						$("#valores").html(valores);

					}else{
						Notificacion("El proveedor no tiene operadores asignados","error");
						$("#operadores").html("");
					}

				//}

			}
		}
	);
}

function loaddescrip(valor)
{

	valor=valor.split("-");

	divdescrip=document.getElementById("resultser");

	divdescrip.innerHTML=valor[1]
	document.getElementById("num_m").focus();

	var id_servicio = valor[0];
	var val_paque = 0;

	if(valor[2]==1)
	{

	    $("#paquetes").html("");
		document.getElementById("valorotro").value="";
		document.getElementById("valuestatic").style.display="none"
		$("#des_saldo").fadeIn("fast");
		$("#mo2").fadeIn("fast");
		$('#valorotro').attr('readonly', true);
		$("#seleccione").fadeOut("fast");
		document.getElementById("num_m").type="text"
		document.getElementById("num_m").focus();
		document.getElementById("valorotro").value="";

		for(i=0;i<document.getElementsByName("valor").length;i++)
		{

			document.getElementsByName("valor")[i].disabled=true

		}

		$("#servs").fadeIn("fast");

		val_paque =  1;


	}else
	{

		$("#servs").fadeOut("fast");
		$("#idservicio").html("");
		$("#des_saldo").fadeOut("fast");

		$('#valorotro').attr('readonly', false);
		$("#seleccione").fadeIn("fast");
		document.getElementById("num_m").type="text";
		//document.getElementById("valorotro").value = val;

		document.getElementById("valuestatic").style.display="inline";
		document.getElementById("idservicio2").innerHTML=0;

		$("#mo2").fadeIn("fast");

		for(i=0;i<document.getElementsByName("valor").length;i++)
        {

			document.getElementsByName("valor")[i].disabled=false

        }

	}

	$.post('modulos/venta_uruguay/controlador.php',
		{
			accion: 'CargarCatePaquete',
			id_servicio: id_servicio,
			val_paque: val_paque

		},
		function(data, textStatus) {

			if(data != 0 || data != "")
			{
				var datos = JSON.parse(data);

				if(val_paque == 1){

					var servicios = '<select name="catePaquete" id="catePaquete" onChange="loadselect(this.value,1,'+id_servicio+');" class="form-control" style="font-size:17px;">';

					servicios += "<option value = ''>Selecciona una categoria del paquete</option>";

					if(datos["info_paquete"].length > 0){

						$.each(datos["info_paquete"],function(index, fila) {
							servicios += "<option value = '"+fila.categoria+"'>"+fila.des+"</option>";
						});

					}

					servicios += "</select>";
					$("#idservicio").html(servicios);

				}

				id_oper = datos.id_oper;
				id_pro = datos.id_pro;

			}
		}
	);
}

function clearvalue(valor)
{
	document.getElementById("valorotro").value=valor
	document.getElementById("valorotro2").value=valor
}

function loadselect(valors,op,operador)
{

	if(op == 1){

		$.post('modulos/venta_uruguay/controlador.php',
			{
				accion: 'CargarServs',
				id_servicio: operador,
				categoria: valors,
				id_pro: id_pro

			},
			function(data, textStatus) {

				if(data != 0 || data != "")
				{
					var datos = JSON.parse(data);

					var servicios = '<br><label style="font-size: 18px;">Item Paquete :</label>';

					servicios += '<select name="itemservi" id="itemservi" onChange="loadselect(this.value,2,'+operador+');" class="form-control" style="font-size:17px;">';

					servicios += "<option value = ''>Selecciona un paquete..</option>";

					if(datos.length > 0){

						$.each(datos,function(index, fila) {
							servicios += "<option value = '"+fila.code+"-"+fila.valVen+"-"+fila.obs+"-"+fila.des+"-"+fila.vals+"'>"+fila.des+"</option>";
						});

					}

					servicios += "</select>";
					$("#paquetes").html(servicios);

				}
			}
		);


	}else{

		divdescrip=document.getElementById("resultser")

		if(valors!="")
		{
			valors=valors.split("-");
			$("#des_saldo").fadeIn("fast");
			document.getElementById("valorotro").value=valors[1];
			document.getElementById("valorotro2").value=valors[4];
			document.getElementById("valor_des").innerHTML=addCommas(valors[4]);
			divdescrip.innerHTML=valors[2];
			ext = valors[1] - valors[4];
			document.getElementById("idservicio2").innerHTML=addCommas(ext);
		}else
		{
			document.getElementById("valorotro").value="";
			document.getElementById("valorotro2").value="";
			document.getElementById("valor_des").innerHTML=0;
			document.getElementById("idservicio2").innerHTML=0;
			divdescrip.innerHTML="N/A";
		}

	}

}

function guardar_venta_uruguay()
{

	for(i=0;i<document.getElementsByName("servicio").length;i++)
	{

		if(document.getElementsByName("servicio")[i].checked)
		{
			valor=document.getElementsByName("servicio")[i].value.split("-");
			var servicio = id_oper;
			var descripcion = "<b>"+valor[1]+"</b>";
			var itemservicio=0;

		}

	}

	var des_deta = "la siguiente ";

	if( $('#servs').css('display') == 'block' )
	{
		var itemser=document.getElementById("itemservi").value
		var itemser=itemser.split("-")
		var descripcion = "<b>"+itemser[3]+"</b>";
		var itemservicio= itemser[0] ;
	}

	var num_m = document.getElementById("num_m").value

	var val=document.getElementById("valorotro").value

	var valores = addCommas(val);

	$.post('modulos/venta_uruguay/controlador.php',
	{
		accion: 'consultar_porcent',
		servs: servicio,
		id_pro: id_pro,
		val: val,
		num: num_m,
		items: itemservicio

	},
	function(data, textStatus) {

		var msg = "";
		var datos = JSON.parse(data);
		var concat = "";

		if(servicio == 6){

			if(datos.ajuste > 0 || datos.comision > 0){
				msg = "<br><br><div style='color:#000'> Por esta venta_uruguay de <b>Recarga Movistar</b> te acreditamos a tu saldo: ";

				if(datos.ajuste > 0){
					msg += " <b>"+moneda+addCommas(datos.ajuste)+"</b> por ajuste";
					concat = " y";
				}

				if(datos.comision > 0){
					msg += concat+" <b>"+moneda+addCommas(datos.comision)+"</b> por comisión";
				}

				msg += "</div>";

			}

			if(parseInt($("#saldoacIn").val())>0){
				//msg = "<br><br><div style='color:#000'> Por esta venta_uruguay de <b>Recarga Movistar</b> el descuento será al saldo que tienes de incentivo ";
			}
		}

		if(itemservicio > 0){

			if(datos.ganancia > 0){

				if(datos.tarifa == 0){
					msg = "<center><br><br><div style='color:#000;font-size:16px'> Por la venta_uruguay de este paquete obtendrás una ganancia de: ";

						msg += " <b>"+moneda+addCommas(datos.ganancia)+"</b>";

					msg += "</div></center>";

					BootstrapDialog.configDefaultOptions({
					    cssClass: ''
					});

				}else{
					msg = "<center><br><br><div style='color:#000;font-size:16px'> ! Hoy es un día especial por la venta_uruguay de este paquete, del cual vas a obtener una ganancia de:";

						msg += " <b>"+moneda+addCommas(datos.ganancia)+"</b> ¡";

					msg += "</div></center>";

					BootstrapDialog.configDefaultOptions({
					    cssClass: 'tarifa_especial'
					});

				}

			}

			des_deta = "el siguiente paquete ";
		}

        BootstrapDialog.show({
            title: 'Confirmación de la venta_uruguay',
            closable: false,
            closeByBackdrop: false,
	        closeByKeyboard: false,
            message: '¿ Esta seguro de enviar '+des_deta+descripcion+' ?<p/> <span style="font-size:16px">Movil: <b>'+num_m+'</b><p/> Valor: <b>'+moneda+addCommas(valores)+"</b></span>"+msg,
            buttons: [{
                label: 'No',
            	cssClass: 'btn',
                action: function(dialog) {
                    dialog.close();

                }
            }, {
                label: 'Si',
            	cssClass: 'btn-primary',
                action: function(dialog) {

                $(".load").show();

				$.post('modulos/venta_uruguay/controlador.php',
				{
					accion: 'RecarServs',
					servs: servicio,
					id_pro: id_pro,
					val: val,
					num: num_m,
					items: itemservicio,
					key_pes: key_pes

				},
				function(data, textStatus)
				{


					if(data != "")
					{
						var datos = JSON.parse(data);

						if(datos["id"]==1)
						{
							$("#respuesta").fadeOut("fast");
							var msg2 = "";

							if(datos["ajuste"] > 0 || datos["comision"] > 0 || datos["ganancia"] > 0){
								msg2 = "<br><br><div style='color:#000'> A tu saldo se ha agregado ";
								concat = "";

								if(datos["ajuste"] > 0){
									msg2 += " un ajuste por un valor de <b>"+moneda+addCommas(datos["ajuste"])+"</b>";
									concat = " y";
								}

								if(datos["comision"] > 0){
									msg2 += concat+" una comisión por un valor de <b>"+moneda+addCommas(datos["comision"])+"</b>";
								}

								if(datos["ganancia"] > 0){
									msg2 += concat+" un ajuste por paquete por un valor de <b>"+moneda+addCommas(datos["ganancia"])+"</b>";
								}


								msg2 += "</div>";
							}

							BootstrapDialog.show({
					            title: 'venta_uruguay Exitosa',
					            type: BootstrapDialog.TYPE_SUCCESS,
					            closable: false,
					            closeByBackdrop: false,
						        closeByKeyboard: false,
					            message: 'venta_uruguay exitosa de '+descripcion+'<p/>Movil: <b>'+num_m+'</b> <p/>Valor: <b>'+moneda+addCommas(valores)+'</b>'+msg2+' <br> <center>¿ Desea Generar Recibo de venta_uruguay ?</center>',
					            buttons: [{
					                label: 'No',
				                	cssClass: 'btn',
					                action: function(dialog) {
					                    dialog.close();
				                    	window.location='?mod=venta_uruguay';
					                }
					            }, {
					                label: 'Si',
				                	cssClass: 'btn-success',
					                action: function(dialog) {

				                    	dialog.close();
					                    var url= "pdf/colilla.php?registro="+datos["regs"];
										Abrir_venta_uruguayna(url)
					                }
					            }],
					            onshonw: function(dialog) {
					            }
					        });


						}else if(datos["id"]==-1)
						{
							Notificacion(datos["msg"],"error");
							setTimeout("window.location='?mod=venta_uruguay'",7000);
						}else if(datos["id"]==-2)
						{
							Notificacion(datos["msg"],"error");
							setTimeout("window.location='?cerrar=S'",5000);
						}

						$(".load").hide();

					}
					}
					);
                	dialog.close();
                }
            }],
            onshonw: function(dialog) {
            }
        });
		/*
		BootstrapDialog.confirm('¿ Esta seguro de enviar '+des_deta+descripcion+' ?<p/> <span style="font-size:16px">Movil: <b>'+num_m+'</b><p/> Valor: <b>'+addCommas(valores)+"</b></span>"+msg , function(result){
			if(result){

				$(".load").show();

				$.post('modulos/venta_uruguay/controlador.php',
				{
					accion: 'RecarServs',
					servs: servicio,
					val: val,
					num: num_m,
					items: itemservicio,
					key_pes: key_pes

				},
				function(data, textStatus)
				{


						if(data != "")
						{
							var datos = JSON.parse(data);

							if(datos["id"]==1)
							{
								$("#respuesta").fadeOut("fast");
								var msg2 = "";

								if(datos["ajuste"] > 0 || datos["comision"] > 0 || datos["ganancia"] > 0){
									msg2 = "<br><br><div style='color:#000'> A tu saldo se ha agregado ";
									concat = "";

									if(datos["ajuste"] > 0){
										msg2 += " un ajuste por un valor de <b>$"+addCommas(datos["ajuste"])+"</b>";
										concat = " y";
									}

									if(datos["comision"] > 0){
										msg2 += concat+" una comisión por un valor de <b>$"+addCommas(datos["comision"])+"</b>";
									}

									if(datos["ganancia"] > 0){
										msg2 += concat+" un ajuste por paquete por un valor de <b>$"+addCommas(datos["ganancia"])+"</b>";
									}


									msg2 += "</div>";
								}

								BootstrapDialog.show({
						            title: 'venta_uruguay Exitosa',
						            type: BootstrapDialog.TYPE_SUCCESS,
						            closable: false,
						            closeByBackdrop: false,
							        closeByKeyboard: false,
						            message: 'venta_uruguay exitosa de '+descripcion+'<p/>Movil: <b>'+num_m+'</b> <p/>Valor: <b>$'+addCommas(valores)+'</b>'+msg2+' <br> <center>¿ Desea Generar Recibo de venta_uruguay ?</center>',
						            buttons: [{
						                label: 'No',
					                	cssClass: 'btn',
						                action: function(dialog) {
						                    dialog.close();
					                    	window.location='?mod=venta_uruguay';
						                }
						            }, {
						                label: 'Si',
					                	cssClass: 'btn-success',
						                action: function(dialog) {

					                    	dialog.close();
						                    var url= "pdf/colilla.php?registro="+datos["regs"];
											Abrir_venta_uruguayna(url)
						                }
						            }],
						            onshonw: function(dialog) {
						            }
						        });


							}else if(datos["id"]==-1)
							{
								Notificacion(datos["msg"],"error");
								setTimeout("window.location='?mod=venta_uruguay'",3000);
							}else if(datos["id"]==-2)
							{
								Notificacion(datos["msg"],"error");
								setTimeout("window.location='?cerrar=S'",3000);
							}

							$(".load").hide();

						}
					}
				);

			}

		});*/

		jQuery('.modal-header').bind('load', function(e) {

			if(itemservicio > 0){

				if(datos.ganancia > 0){

					if(datos.tarifa == 1){
						$(".modal-header").addClass("tarifa_especial");
					}

				}

			}

		})

	}
	);

}

function validate()
{

	var conts=0;

	var movilstring=document.getElementById("num_m").value


	for(i=0;i<document.getElementsByName("servicio").length;i++)
    {

		if(document.getElementsByName("servicio")[i].checked)
		{
		conts++;
		}

    }

	if(conts==0)
	{
		Notificacion('Por favor seleccione un operador',"error");
		return false;
	}else if($('#servs').css('display') == 'block' && !$("#itemservi") && document.getElementById("itemservi").value=="")
	{
		Notificacion('Por favor Seleccione un paquetigo',"error");
	    return false;
	}
	else if(movilstring.length<min_movil || movilstring.length>max_movil)
	{
		Notificacion('El número movil debe ser minimo de '+min_movil+' y maximo de '+max_movil+' digitos', "error")

	    return false;

	}
	else if(document.getElementById("num_m").value=="")
	{
		Notificacion('Por favor ingrese un numero movil valido', "error")

	    return false;

	}else if(document.getElementById("valorotro").value=="")
	{
		Notificacion('Por favor ingrese un valor', "error")
		return false;
	}else if(parseFloat(document.getElementById("saldoac").value) < parseFloat(document.getElementById("valorotro").value) && parseFloat(document.getElementById("saldoacIn").value) == 0)
	{
	    Notificacion('Error, no tiene saldo disponible<p/>saldo actual : '+document.getElementById("saldoac").value+"<p/>valor recarga : "+addCommas(document.getElementById("valorotro").value), "error")
		return false;
	}else if(!validateDecimal(document.getElementById("valorotro").value) && pais == 2){
		Notificacion('El valor de la recarga no es permitido, solo se puede enviar entre 1 a 2 decimales',"error");
		return false;
	}
	else
    {
		guardar_venta_uruguay();
		return true;
	}

}

function Abrir_venta_uruguayna (pagina)
{
	day = new Date();
	id = day.getTime();

	/*
	eval("page" + id + " = window.open(pagina, '" + id + "', 'toolbar=0,scrollbars=0,location=0,statusbar=0,menubar=0,resizable=0,width=300,height=300,left = 910,top = 440');");
	window.location="?mod=venta_uruguay";*/

	BootstrapDialog.show({
	   title: 'Recibo de venta_uruguay',
	   closable: true,
	   closeByBackdrop: false,
	   closeByKeyboard: false,
	   size: BootstrapDialog.SIZE_WIDE,
	   message: '<embed src="'+pagina+'" style="width:100%; height:500px;"  type="application/pdf">',
	   onhidden: function(dialogRef){
		  window.location='?mod=venta_uruguay';
	   }
	});
}

var dtable;
function ultimas_transac(){
	if ( ! $.fn.DataTable.isDataTable( '#tblTrans' ) ) {

		 dtable = $("#tblTrans").DataTable({

			"ajax": {
				"url": "modulos/venta_uruguay/controlador.php",
				"type": "POST",
				"deferRender": false,
				"data":{
					accion:'ultimasTransac'
				}
			},
			  "bFilter": false,
			  "paginate": false,
			  "order": [],
			  "responsive":false,
			  "scrollX": true,
			  "columns": [
			  	{ "data": "movi"},
			  	{ "data": "des_operador"},
			    { "data": "num_m"},
				{ "data": "des_paquete"},
				{ "data": "fecha_trans"},
				{ "data": "hora_trans"},
				{ "data": "porcen"},
				{ "data": "s_ant_inc"},
				{ "data": "s_ant_inc"},
				{ "data": "v_inc"},
				{ "data": "s_act_inc"},
				{ "data": "s_ant"},
				{ "data": "v_recarga_des"},
				{ "data": "s_act"}
				//{ "data": "esta"}
				],
				"columnDefs": [
				{
					"targets": 0,
					"data": "movi",
					 render: function ( data, type, row ) {

                          var color = "";

					 	  if(row.movi_int == 1){
					 	  	color = "#de1919";
					 	  }

					 	  if(row.movi_int == 2 || row.movi_int == 3 || row.movi_int == 4){
					 	  	color = "#056311";
					 	  }

						  return "<div style='font-weight: bold;color:"+color+"'>"+data+"</div>";
					 }
				},
				{
					"targets": 7,
					"data": "",
					 render: function ( data, type, row ) {
					 	  var total = parseFloat(row["v_inc"]) + parseFloat(row["v_recarga"]);
						  return moneda+" "+addCommas(total);
					 }
				},
				{
					"targets": 8,
					"data": "s_ant_inc",
					 render: function ( data, type, row ) {
						  return moneda+" "+addCommas(data);
					 }
				},
				{
					"targets": 9,
					"data": "v_inc",
					 render: function ( data, type, row ) {
						  return moneda+" "+addCommas(data);
					 }
				},
				{
					"targets": 10,
					"data": "s_act_inc",
					 render: function ( data, type, row ) {
						  return moneda+" "+addCommas(data);
					 }
				},
				{
					"targets": 11,
					"data": "s_ant",
					 render: function ( data, type, row ) {
						  return moneda+" "+addCommas(data);
					 }
				},
				{
					"targets": 12,
					"data": "v_recarga_des",
					 render: function ( data, type, row ) {
						  return moneda+" "+addCommas(data);
					 }
				},
				{
					"targets": 13,
					"data": "s_act",
					 render: function ( data, type, row ) {
						  return moneda+" "+addCommas(data);
					 }
				}
				],
		 });

	}
	else{
		dtable.destroy();
		ultimas_transac();
	}
	$("#resultado").show();
}

function addCommas(nStr)
{
	nStr += '';
	x = nStr.split('.');
	x1 = x[0];
	x2 = x.length > 1 ? '.' + x[1] : '';
	var rgx = /(\d+)(\d{3})/;
	while (rgx.test(x1)) {
	x1 = x1.replace(rgx, '$1' + ',' + '$2');
	}
	return x1 + x2;
}

function validateDecimal(valor) {

	var n = valor.indexOf(".");

	if(n > 0){

	    deci_array = valor.split(".");

	    console.log(deci_array[1]);

	    if(deci_array[1].length == 2 || deci_array[1].length == 1){
			return true;
		}else{
			return false;
		}

	}else{
		return true;
	}
}
