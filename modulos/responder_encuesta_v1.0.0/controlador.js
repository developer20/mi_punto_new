var envio = true;
var contador = 0;
var contador2 = 0;
var cantidad = 0;
var cantidad2 = 0;
var conteo = 0;
var conteo2 = 0;
var navegar = 0;
var myCont = 0;
var anterior = 0;
var anterior2 = 0;
var id_encuesta = 0;
var encuesta = 0;
var pregunta_ant = 0;
var respuesta = 0;
var obligatorio = 0;
var cont = 0;
var act = 0;
var cont2 = 0;
var act2 = 0;
var check_resp = new Array();
var id_preg = 0;
var id_preg2 = 0;
var text_resp = new Array();
var pregunta_anidada = 0;
var id_respuestaAnida = 0;
var id_respTemp = 0;
var anteAnida = 0;
var val_check = new Array();
var val_ult_cantResp = 0;
var sub_pregunta_txt = "";
var sub_respuesta_txt = "";
var array_depurado = new Array();
var id_preguntaPadre = 0;
var id_respuestaPadre = 0;
var array_pregunta_respuesta = new Array();
var text_active = 0;
var id_encuestaTxt = "";
var array_Can = new Array();
var sub_pregunta_id = 0;
var badAnidado = 0;

$(document).ready(function() {

    Cargar_Encuesta();


    $('.saltar_enc').on('click', function() {
        saltar_encuesta();
    });


});


function saltar_encuesta() {

    if (localStorage.getItem('id_encuesta') != "") {
        //localStorage.removeItem("id_encuestas");
        if (localStorage['id_encuesta'] !== undefined) {
            id_encuestaTxt = localStorage['id_encuesta'] + "," + id_encuesta;
        }
    } else {
        localStorage['id_encuesta'] = "";
        id_encuestaTxt = id_encuesta;
    }

    localStorage.setItem("id_encuesta", id_encuestaTxt);
    window.location = "?mod=responder_encuesta_v1.0.0"
}

function Cargar_Encuesta() {

    if (localStorage.getItem('id_encuesta') != '') {
        id_encuesta = localStorage['id_encuesta'];
    }


    if (id_encuesta == null) {
        id_encuesta = 0;
    }

    var id_respuesta = 0;
    var ult = 0;

    $.ajax({
        url: "modulos/responder_encuesta_v1.0.0/controlador.php",
        type: "POST",
        dataType: "JSON",
        data: {
            accion: "Cargar_Encuesta",
            id_encuesta: id_encuesta
        },
        success: function(data) {

            if (data.length > 0) {
                $("#id_enc").html(data.length);
            } else {
                $("#id_enc").hide()
            }

            if (data['error'] == 0) {
                $("#visor_panel").html("<h3 class='text-center' style='margin:20px;'>No hay encuestas pendientes por responder</h3>");
                $("#next").hide();

                if (localStorage.getItem('est_enc') > 0) {
                    localStorage['est_enc'] = 0;
                    setTimeout(function() { window.location = "?mod=principal" }, 2000);
                }

                localStorage['id_encuesta'] = "";

                return false;
            }


            for (var i = 0; i < data.length; i++) {

                if (data[0]['obligatorio'] == 0) {
                    $('.saltar_enc').show();
                }

                cantidad = data[0]['cant'];
                navegar = data[0]['navegar_atras'];

                $(".titulo_enc h1").html(data[0]['titulo']);
                $(".descrp_enc h3").html(data[0]['descripcion']);
                $("#total_preg").html(data[0]['cant']);
                id_encuesta = data[0]['id'];

            }

            $("#next").unbind('click')
            $('#next').on('click', function() {


                var band = 0;
                var bandTipo = 0;


                if ($("#resptxt_" + id_preg).length > 0) {
                    encuesta = $("#resptxt_" + id_preg).data("enc");
                    pregunta = $("#resptxt_" + id_preg).data("preg");
                    tipo = $("#resptxt_" + id_preg).data("tipo");
                    idRespuestaPadre = $("#resptxt_" + id_preg).data("idrespuestapadre");
                    valor = $("#resptxt_" + id_preg).val();

                    if (parseInt(id_encuesta) == parseInt(encuesta) && parseInt(id_preg) == parseInt(pregunta) && parseInt(tipo) == 3 && obligatorio == 1 && valor == "") {
                        bandTipo = 1;
                    } else {
                        if (parseInt(id_encuesta) == parseInt(encuesta) && parseInt(id_preg) == parseInt(pregunta) && parseInt(tipo) > 2) {

                            for (var i in text_resp) {
                                if (text_resp[i]["id_pre"] == pregunta) {
                                    delete text_resp[i];
                                }
                            }

                            if (tipo == 4 && parseInt(valor) >= 0) {
                                band = 1;
                                text_resp.push({ id_enc: encuesta, id_pre: pregunta, idRespuestaPadre: idRespuestaPadre, id_resp: 0, respuesta_texto: valor });

                            } else if (tipo == 4 && parseInt(valor) < 0) {
                                bandTipo = 1;
                                Notificacion("Por favor ingrese una respuesta válida", "warning");
                                return false;
                            } else if (tipo == 3 && valor != "") {
                                band = 1;
                                text_resp.push({ id_enc: encuesta, id_pre: pregunta, idRespuestaPadre: idRespuestaPadre, id_resp: 0, respuesta_texto: valor });

                            }
                        } else if (id_encuesta == encuesta && tipo == 3 && valor == "") {
                            bandTipo = 1;
                        }
                    }

                }




                for (var x in check_resp) {
                    info_enc = check_resp[x].split(",");

                    var cantidadPre = 0;

                    if (id_encuesta == info_enc[0] && info_enc[1] == id_preg && info_enc[2] > 0) {
                        band = 1;
                    }

                    if (id_encuesta == info_enc[0] && info_enc[1] == id_preg) {

                        cantidadPre = info_enc[3];
                        id_respuesta = info_enc[2];


                        if (cantidadPre > 0) {


                            id_respuestaAnida = id_respuesta;
                            pregunta_anidada = 1;

                            cont2 = 0;
                            conteo2 = 0;
                            contador2 = 0;

                            sub_pregunta_txt = $("#pregunta_" + id_preg).text();
                            sub_respuesta_txt = $("#respuesta_" + id_respuesta).text();

                        }

                    }

                }


                if (cont2 >= cantidad2 && cantidadPre == 0) {
                    ult = 1;
                } else {
                    ult = 0;
                }


                if (obligatorio == 1 && !band) {
                    if (bandTipo) {
                        Notificacion("La pregunta es obligatoria, por favor ingrese una respuesta válida", "warning");
                        return false;
                    }
                    Notificacion("La pregunta es obligatoria, por favor selecciona una opción", "warning");
                } else {

                    $('.saltar_enc').hide();

                    $("#titulo_enc").html(data[0]['titulo']);


                    if (cantidadPre == 0) {
                        if (array_Can.length > 0) {
                            ultimoValue = array_Can.pop()


                            if (ultimoValue !== id_preg) {
                                ult = 0;

                            } else {

                                ult = 1;

                            }
                        }
                    }



                    if (pregunta_anidada) {

                        if (array_pregunta_respuesta.length > 0 && ult) {
                            array_pregunta_respuesta.forEach(function(datosPreAnida, b) {

                                if (datosPreAnida["cont2"] < datosPreAnida["cantidad2"]) {
                                    id_preg = datosPreAnida["id_pregunta"];
                                    id_respuestaAnida = datosPreAnida["id_respuesta"];
                                    contador2 = datosPreAnida["contador2"];
                                    cont2 = datosPreAnida["cont2"];
                                    cantidad2 = datosPreAnida["cantidad2"];
                                    conteo2 = datosPreAnida["conteo2"];
                                    sub_pregunta_txt = datosPreAnida["sub_pregunta_txt"];
                                    sub_respuesta_txt = datosPreAnida["sub_respuesta_txt"];
                                    sub_pregunta_id = datosPreAnida["id_pregunta"];

                                    text_active = 1;

                                    ult = 0;
                                } else {
                                    text_active = 0;
                                }
                            });
                        }




                        if (ult) {


                            if (cont < cantidad) {

                                array_pregunta_respuesta = [];

                                Cargar_Preguntas(id_encuesta, cantidad, 1, ult);

                            } else {
                                llamar_guardar_encuesta();
                            }
                        } else {

                            Cargar_Preguntas_Anidas(id_encuesta, 1, id_respuestaAnida);
                        }


                    } else {

                        if (cont < cantidad) {

                            array_pregunta_respuesta = [];

                            Cargar_Preguntas(id_encuesta, cantidad, 1, ult);
                        } else {

                            llamar_guardar_encuesta();
                        }

                    }

                }
            });

            $("#previous").on('click', function() {



                var cantResp = 0;
                var bandPreAnida = 0;
                var contadorr = 0;

                var id_preguntaAnida = 0;
                var id_respuesta = id_respuestaAnida;

                for (var x in check_resp) {
                    info_enc = check_resp[x].split(",");

                    if (info_enc[4] > 0 && info_enc[4] === id_respuesta && info_enc[1] === id_preg) {

                        delete check_resp[x];

                    }
                }


                array_pregunta_respuesta.forEach(function(datosPreAnida, b) {
                    if (datosPreAnida["id_pregunta"] == id_preg && datosPreAnida["id_respuesta"] == id_respuesta) {
                        array_pregunta_respuesta.splice(b, 1);
                    }
                });

                for (var x in val_check) {
                    if (val_check.length > 1) {
                        if (x > 0) {


                            if (val_check[x]["id_pregunta"] == id_preg && val_check[x]["id_respuesta"] == id_respTemp) {
                                id_preg = val_check[x - 1]["id_pregunta"];
                                id_respuestaAnida = val_check[x - 1]["id_respuesta"];
                                bandPreAnida = val_check[x - 1]["val_anidado"];
                                sub_pregunta_txt = val_check[x - 1]["sub_pregunta_txt"];
                                sub_respuesta_txt = val_check[x - 1]["sub_respuesta_txt"];
                                contador2 = val_check[x - 1]["contador"];
                                cantidad2 = val_check[x - 1]["cantidad"];
                                conteo2 = val_check[x - 1]["conteo"];
                                cont2 = val_check[x - 1]["cont"];
                                val_check.splice(x, 1);
                            }
                        }
                    } else {

                        val_check.splice(x, 1);
                    }
                }



                if (bandPreAnida == 0) {
                    array_pregunta_respuesta = [];
                    $("#next").val("Siguiente");
                    contador = contador2;
                    conteo = conteo2;
                    cont = cont2;


                    if (anterior > 0) {

                        if (conteo == 0) {
                            $("#previous").hide();
                        }

                        Cargar_Preguntas(id_encuesta, cantidad, 0);
                    }
                } else {



                    if (contador > 1 && cont <= cantidad) {
                        contador--;
                        conteo--;
                        cont--;

                    }

                    if (anterior2 > 0) {

                        Cargar_Preguntas_Anidas_Devolver(id_encuesta, 0, id_preg, id_respuestaAnida);
                    }
                }

            });

        }

    });
}

function Cargar_Preguntas(value, cantidad, next) {


    var bandPreAnida = 0;
    pregunta_anidada = 0;

    var id_respuesta = 0;
    var cantidad_preguntas = 0;

    id_encuesta = value;

    $("input[data-nom='resp[]'").each(function() {
        activo = $(this).prop("checked");

        encuesta = $(this).attr("data-enc");
        pregunta = $(this).attr("data-preg");
        respuesta = $(this).attr("data-resp");
        var tipo = $(this).attr("data-tipo");

        if (activo == true) {
            act++;
        }
    });

    if (cont < cantidad) {
        $.ajax({
            url: "modulos/responder_encuesta_v1.0.0/controlador.php",
            type: "POST",
            dataType: "JSON",
            data: {
                accion: "Cargar_Preguntas",
                encuesta: id_encuesta,
                id_respuesta: 0
            },
            success: function(data) {

                var cant = data.length;
                id_preg = data[contador]['id'];
                var pregunta = data[contador]['pregunta'];
                var tipo = data[contador]['tipo'];
                obligatorio = data[contador]['obligatorio'];
                var bandExist = 0;

                id_respTemp = data[contador]['id_respuesta'];

                for (var i in val_check) {
                    if (parseInt(val_check[i]["id_pregunta"]) == parseInt(id_preg) && parseInt(val_check[i]["id_respuesta"]) == parseInt(id_respTemp) && parseInt(val_check[i]["val_anidado"]) == 0) {
                        bandExist = 1;
                    }
                }

                if (!bandExist) {
                    val_check.push({ id_encuesta: id_encuesta, id_pregunta: id_preg, cant: cant, id_respuesta: data[contador]['id_respuesta'], contador: contador, cont: cont, cantidad: cantidad, conteo: conteo, val_anidado: 0, sub_pregunta_txt: sub_pregunta_txt, sub_respuesta_txt });
                }

                if (navegar == 1 && cont > 0) {
                    $("#previous").show();
                    anterior = 1;
                    anterior2 = 1;
                }

                if (obligatorio == 1) {
                    $("#obliga").show();
                } else {
                    $("#obliga").hide();
                }

                sub_pregunta_txt = "";
                sub_respuesta_txt = "";

                $("#visor-sub-pregunta").hide();

                $(".pregunta-padre").css("padding", "0");
                $(".sub-pregunta").css("padding", "0");
                $(".sub-pregunta").css("border", "0");
                $(".sub-pregunta").css("margin", "0");

                $(".titulo_enc h1").html("<label class='text-info' id='pregunta_" + id_preg + "'>" + pregunta + "</label>");
                $(".cant_preg").hide();

                Cargar_Respuestas(id_preg, id_encuesta, tipo, obligatorio, 0);
                contador++;

                cont++;

                if (cont == cantidad) {
                    /*if(cantidad == 1){
			    		obligatorio = 1;
						$("#next").prop('disabled', true);
					}*/
                    $("#next").val("Enviar");
                }

            }
        });
    }

    if (cont == cantidad) {
        $("#next").val("Enviar");
    }

}

function Cargar_Preguntas_Anidas(id_encuesta, next, id_respuestaAnida) {

    pregunta_anidada = 1;

    $("input[data-nom='resp[]'").each(function() {
        activo = $(this).prop("checked");

        encuesta = $(this).attr("data-enc");
        pregunta = $(this).attr("data-preg");
        respuesta = $(this).attr("data-resp");
        var tipo = $(this).attr("data-tipo");

        if (activo == true) {
            act++;
        }
    });

    $.ajax({
        url: "modulos/responder_encuesta_v1.0.0/controlador.php",
        type: "POST",
        dataType: "JSON",
        data: {
            accion: "Cargar_Preguntas",
            encuesta: id_encuesta,
            id_respuesta: id_respuestaAnida
        },
        success: function(data) {

            var cant2 = data.length;
            cantidad2 = cant2;


            for (var i in data) {
                array_Can.push(data[i]['id'])
            }

            if (cont2 < cantidad2) {


                id_preg = data[contador2]['id'];
                var pregunta = data[contador2]['pregunta'];
                var tipo = data[contador2]['tipo'];
                obligatorio = data[contador2]['obligatorio'];
                var bandExist = 0;

                id_respTemp = data[contador2]['id_respuesta'];

                for (var i in val_check) {
                    if (val_check[i]["id_pregunta"] == id_preg && val_check[i]["id_respuesta"] == data[contador2]['id_respuesta'] && val_check[i]["val_anidado"] == 1) {
                        bandExist = 1;
                    }
                }

                if (!bandExist) {
                    val_check.push({ id_encuesta: id_encuesta, id_pregunta: id_preg, cant: cant2, id_respuesta: data[contador2]['id_respuesta'], contador: contador2, cont: cont2, cantidad: cantidad2, conteo: conteo2, val_anidado: 1, sub_pregunta_txt: sub_pregunta_txt, sub_respuesta_txt });
                }

                if (navegar == 1) {
                    $("#previous").show();
                    anterior2 = 1;
                    anterior = 1;
                }

                if (obligatorio == 1) {
                    $("#obliga").show();
                } else {
                    $("#obliga").hide();
                }

                $("#visor-sub-pregunta").show();

                //if(cont2 == 0 || text_active){

                $(".titulo_pregunta").html("<label>PREGUNTA: " + sub_pregunta_txt + "</label>");
                $(".descrp_respuesta").html("<label>RESPUESTA: " + sub_respuesta_txt + "</label>");
                //}

                $(".pregunta-padre").css("padding", "20px");
                $(".sub-pregunta").css("padding", "10px");
                $(".sub-pregunta").css("border-top", "1px solid #ccc");
                $(".sub-pregunta").css("margin", "0 auto");

                $(".titulo_enc h1").html("<label class='text-info' id = 'pregunta_" + id_preg + "'>" + pregunta + "</label>");
                $(".cant_preg").hide();

                Cargar_Respuestas(id_preg, id_encuesta, tipo, obligatorio, data[contador2]['id_respuesta']);

                contador2++;
                cont2++;
                var ult = 0;

                if (cont2 == cantidad2) {

                    /*if(cantidad2 == 1){
		    		obligatorio = 1;
					$("#next").prop('disabled', true);
				}*/

                    if (cont == cantidad) {
                        $("#next").val("Enviar");
                    }

                    ult = 1;

                    //cantidad2 = 0;
                } else {
                    $("#next").val("Siguiente");
                }

                //if(next == 1){
                var encontrar = 0;

                array_pregunta_respuesta.forEach(function(datosPreAnida, b) {
                    if (datosPreAnida["id_respuesta"] == id_respTemp) {
                        //array_pregunta_respuesta.splice(b, 1);
                        array_pregunta_respuesta[b]["contador2"] = contador2;
                        array_pregunta_respuesta[b]["cont2"] = cont2;
                        array_pregunta_respuesta[b]["cantidad2"] = cantidad2;
                        array_pregunta_respuesta[b]["conteo2"] = conteo2;
                        array_pregunta_respuesta[b]["id_pregunta"] = id_preg;
                        encontrar = 1;
                    }
                });

                if (!encontrar) {
                    array_pregunta_respuesta.push({ id_pregunta: id_preg, id_respuesta: id_respTemp, id_respuesta_padre: id_respuestaPadre, contador2: contador2, cont2: cont2, cantidad2: cantidad2, sub_pregunta_txt: sub_pregunta_txt, sub_respuesta_txt, conteo2: conteo2, ult: ult });
                }

                //}

            }

        }
    });

    if (cont == cantidad) {
        $("#next").val("Enviar");
    }
}

function Cargar_Respuestas(id_preg, id_encuesta, tipo, obligatorio, id_respuestaPadre) {

    var cantidadPre = cantidad;
    var conteoPre = 0;

    if (!pregunta_anidada) {
        conteo++;
        conteoPre = conteo;
    } else {
        conteo2++;
        cantidadPre = cantidad2;
        conteoPre = conteo2;
    }

    $.ajax({
        url: "modulos/responder_encuesta_v1.0.0/controlador.php",
        type: "POST",
        dataType: "JSON",
        data: {
            accion: "Cargar_Respuestas",
            encuesta: id_encuesta,
            id_preg: id_preg
        },
        success: function(data) {


            /*if(cantidadPre == 1){
            	$("#obliga").show();
            	$("#next").prop('disabled', true);
            }*/

            var cant = data.length;
            envio = false;
            var checked = "";
            var option = "";
            var tip_sel = "";
            var requer = "";

            if (tipo == 1) {
                tip_sel = "type='radio'";
            } else if (tipo == 2) {
                tip_sel = "type='checkbox'";
            }

            if (obligatorio == 1) {
                requer = "required='required'";
            }


            var num = 0;

            if (cant > 0) {

                for (var i = 0; i < cant; i++) {

                    var activar = data[i]["activar"];
                    var respuesta = data[i]['respuesta'];
                    var id_respuesta = data[i]['id'];
                    var cantidad_preguntas = data[i]['cant_pre'];
                    var checked = "";

                    for (var x in check_resp) {
                        info_enc = check_resp[x].split(",");

                        if (id_encuesta == info_enc[0] && id_preg == info_enc[1] && id_respuesta == info_enc[2]) {
                            checked = "checked='checked'";
                            if (info_enc[2] == 1) {
                                $("#next").prop('disabled', false);
                            }

                        }

                    }

                    /*
                    if(activar>0)
                    {
                    	checked = "checked='checked'";
                    }*/

                    option += "<div class='col-md-10 text-left' style='margin-bottom: 3px; position: relative;'>";
                    option += "<div style='display: inline-block; padding: 2px; margin-right: 5px; position: relative;'>";
                    option += "<input " + tip_sel + " " + checked + " name='respuestas' style='font-size: 21px;' data-nom='resp[]' data-tipo='" + tipo + "' onclick='regActivo(" + i + "," + tipo + ");' data-resp='" + id_respuesta + "' data-enc='" + id_encuesta + "' data-preg='" + id_preg + "' data-cantidadPreguntas='" + cantidad_preguntas + "' data-idrespuestapadre = " + id_respuestaPadre + " " + requer + " id='resp_" + i + "' value='" + i + "' />";
                    option += "</div>";
                    option += "<label class='text-info' for='resp_" + i + "' style='font-size: 18px;cursor: pointer; margin: 0px; padding: 0px;' id = 'respuesta_" + id_respuesta + "'>" + respuesta + "</label>";
                    option += "</div>";

                    num++;
                }
                option += "<div class='col-md-12 text-center' style='font-size: 14px;'>Pregunta " + conteoPre + " de " + cantidadPre + "</div>"

            } else {

                if (tipo == 3) {

                    var valor = "";

                    text_resp.forEach(function(value, i) {
                        if (value["id_pre"] == id_preg) {
                            valor = value["respuesta_texto"];
                        }
                    });

                    option += "<textarea name='respuesta_texto' style='font-size: 15px;width: 300px;' data-tipo='" + tipo + "' data-enc='" + id_encuesta + "' data-preg='" + id_preg + "' id='resptxt_" + id_preg + "' data-obligatorio = " + obligatorio + " data-idrespuestapadre = " + id_respuestaPadre + " data-nom='resptxt[]'>" + valor + "</textarea>";

                    num++;

                    option += "<div class='col-md-12 text-center' style='font-size: 14px;'>Pregunta " + conteoPre + " de " + cantidadPre + "</div>"

                } else if (tipo == 4) {

                    var valor = 0;



                    text_resp.forEach(function(value, i) {
                        if (value["id_pre"] == id_preg) {
                            valor = value["respuesta_texto"];
                        }
                    });


                    option += "<input type='number' name='respuesta_texto' style='font-size: 21px;' data-tipo='" + tipo + "' data-enc='" + id_encuesta + "' data-preg='" + id_preg + "' id='resptxt_" + id_preg + "' data-obligatorio = " + obligatorio + " data-idrespuestapadre = " + id_respuestaPadre + " min='0' step='1'  data-nom='resptxt[]' value = '" + valor + "' onkeypress=' return validar_numeros()'/>";

                    num++;

                    option += "<div class='col-md-12 text-center' style='font-size: 14px;'>Pregunta " + conteoPre + " de " + cantidadPre + "</div>"

                } else {
                    option += "<h3>No hay opciones de respuesta...</h3>";
                }

            }

            $(".descrp_enc").html(option);

        }
    });

}

function Cargar_Preguntas_Anidas_Devolver(id_encuesta, next, id_preg, id_respuestaAnida) {


    array_Can = [];
    pregunta_anidada = 1;

    $("input[data-nom='resp[]'").each(function() {
        activo = $(this).prop("checked");

        encuesta = $(this).attr("data-enc");
        pregunta = $(this).attr("data-preg");
        respuesta = $(this).attr("data-resp");
        var tipo = $(this).attr("data-tipo");

        if (activo == true) {
            act++;
        }
    });

    $.ajax({
        url: "modulos/responder_encuesta_v1.0.0/controlador.php",
        type: "POST",
        dataType: "JSON",
        data: {
            accion: "Cargar_Preguntas_Anidada",
            encuesta: id_encuesta,
            id_preg: id_preg,
            id_respuestaAnida: id_respuestaAnida
        },
        success: function(data) {


            datos = data["dataPadre"]
            for (var i in datos) {
                array_Can.push(datos[i]['id'])
            }

            data = data["data"];
            var cant2 = data.length;
            cantidad2 = cant2;


            if (cantidad2 > 0) {

                id_preg = data[0]['id'];
                var pregunta = data[0]['pregunta'];
                var tipo = data[0]['tipo'];
                obligatorio = data[0]['obligatorio'];
                var bandExist = 0;

                id_respTemp = data[0]['id_respuesta'];

                for (var i in val_check) {
                    if (val_check[i]["id_pregunta"] == id_preg && val_check[i]["id_respuesta"] == data[0]['id_respuesta'] && val_check[i]["val_anidado"] == 1) {
                        bandExist = 1;
                    }
                }

                if (!bandExist) {
                    val_check.push({ id_encuesta: id_encuesta, id_pregunta: id_preg, cant: cant2, id_respuesta: data[0]['id_respuesta'], contador: contador2, cont: cont2, cantidad: cantidad2, conteo: conteo2, val_anidado: 1, sub_pregunta_txt: sub_pregunta_txt, sub_respuesta_txt });
                }

                if (navegar == 1) {
                    $("#previous").show();
                    anterior2 = 1;
                    anterior = 1;
                }

                if (obligatorio == 1) {
                    $("#obliga").show();
                } else {
                    $("#obliga").hide();
                }

                $("#visor-sub-pregunta").show();

                //if(cont2 == 0 || text_active){
                $(".titulo_pregunta").html("<label>PREGUNTA: " + sub_pregunta_txt + "</label>");
                $(".descrp_respuesta").html("<label>RESPUESTA: " + sub_respuesta_txt + "</label>");
                //}

                $(".pregunta-padre").css("padding", "20px");
                $(".sub-pregunta").css("padding", "10px");
                $(".sub-pregunta").css("border-top", "1px solid #ccc");
                $(".sub-pregunta").css("margin", "0 auto");

                $(".titulo_enc h1").html("<label class='text-info' id = 'pregunta_" + id_preg + "'>" + pregunta + "</label>");
                $(".cant_preg").hide();


                Cargar_Respuestas(id_preg, id_encuesta, tipo, obligatorio, data[0]['id_respuesta']);

                contador2++;
                cont2++;
                var ult = 0;

                if (cont2 == cantidad2) {

                    /*if(cantidad2 == 1){
		    		obligatorio = 1;
					$("#next").prop('disabled', true);
				}*/

                    if (cont == cantidad) {
                        $("#next").val("Enviar");
                    }

                    ult = 1;

                    //cantidad2 = 0;
                } else {
                    $("#next").val("Siguiente");
                }

                //if(next == 1){
                var encontrar = 0;

                array_pregunta_respuesta.forEach(function(datosPreAnida, b) {
                    if (datosPreAnida["id_respuesta"] == id_respTemp) {

                        //array_pregunta_respuesta.splice(b, 1);
                        array_pregunta_respuesta[b]["contador2"] = contador2;
                        array_pregunta_respuesta[b]["cont2"] = cont2;
                        array_pregunta_respuesta[b]["cantidad2"] = cantidad2;
                        array_pregunta_respuesta[b]["conteo2"] = conteo2;
                        array_pregunta_respuesta[b]["id_pregunta"] = id_preg;
                        encontrar = 1;
                    }
                });

                if (!encontrar) {
                    array_pregunta_respuesta.push({ id_pregunta: id_preg, id_respuesta: id_respTemp, id_respuesta_padre: id_respuestaPadre, contador2: contador2, cont2: cont2, cantidad2: cantidad2, sub_pregunta_txt: sub_pregunta_txt, sub_respuesta_txt, conteo2: conteo2, ult: ult });
                }

                //}

            }

        }
    });

    if (cont == cantidad) {
        $("#next").val("Enviar");
    }
}

function llamar_guardar_encuesta() {

    var validar_test = new Array();

    text_resp.forEach(function(value, index) {

        validar_test.push(value["respuesta_texto"]);
    })


    if (check_resp.length > 0 || validar_test.length > 0) {
        BootstrapDialog.show({
            title: 'Mensaje del Sistema',
            message: "<h3 class='text-center'>Gracias por responder la encuesta!</h3>",
            buttons: [{
                id: 'btn-exit',
                label: 'Cancelar',
                cssClass: 'btn-default',
                autospin: false,
                action: function(result) {
                    if (result) {
                        result.close();
                    }
                }
            }, {
                id: 'btn-ok',
                label: 'Continuar',
                cssClass: 'btn-primary',
                autospin: false,
                action: function(result) {
                    if (result) {
                        //Cargar_Preguntas(id_encuesta,cantidad,1);
                        Guardar_Encuesta();
                        result.close();
                    }
                }
            }]
        });
    } else {
        BootstrapDialog.show({
            title: 'Mensaje del Sistema',
            message: "<h3 class='text-center'>Debe responder al menos una pregunta.</h3>",
            buttons: [{
                id: 'btn-exit',
                label: 'Continuar',
                cssClass: 'btn-default',
                autospin: false,
                action: function(result) {
                    if (result) {
                        result.close();
                    }
                }
            }]
        });
    }

}


function Guardar_Encuesta() {

    var check_resp_enc = new Array();


    for (var x in check_resp) {

        if (check_resp[x] != "") {
            info_enc = check_resp[x].split(",");

            check_resp_enc.push({ id_enc: parseInt(info_enc[0]), id_pre: parseInt(info_enc[1]), id_resp: parseInt(info_enc[2]), cantidadPregunta: parseInt(info_enc[3]), idRespuestaAnida: parseInt(info_enc[4]), respuesta_texto: "" });
        }

    }

    text_resp.forEach(function(value, i) {


        var encuesta = value["id_enc"];
        var pregunta = value["id_pre"];
        var respuesta_texto = value["respuesta_texto"];
        var idRespuestaPadre = value["idRespuestaPadre"];

        check_resp_enc.push({ id_enc: encuesta, id_pre: pregunta, id_resp: 0, cantidadPregunta: 0, idRespuestaAnida: +idRespuestaPadre, respuesta_texto: respuesta_texto });
    });


    depurar_arreglo(check_resp_enc);

    $.ajax({
        url: "modulos/responder_encuesta_v1.0.0/controlador.php",
        type: "POST",
        dataType: "JSON",
        data: {
            accion: "Guardar_Encuesta",
            check_resp_enc: array_depurado

        },
        success: function(data) {

            var error = data['error'];

            if (error == 0) {
                window.location = "?mod=responder_encuesta_v1.0.0";
            } else if (error == 1) {
                Notificacion("Error al tratar de guardar la encuesta, por favor comuniquese con soporte.", "danger");
            } else if (error == 2) {
                Notificacion("No hay encuesta para enviar.", "warning");
            }

        }
    });

}

function regActivo(value, tipo) {

    var estado = $("#resp_" + value).prop("checked");

    var id_encuesta = $("#resp_" + value).attr("data-enc");
    var id_pregunta = $("#resp_" + value).attr("data-preg");
    var id_respuesta = $("#resp_" + value).attr("data-resp");
    var cantidadPreguntas = $("#resp_" + value).attr("data-cantidadPreguntas");
    var idRespuestaPadre = $("#resp_" + value).attr("data-idrespuestapadre");
    val_ult_cantResp = 0;

    if (id_preg2 == id_pregunta) {
        val_ult_cantResp = 1;
    }

    if (estado == true) {
        act = 1;

        if (tipo == 1) {
            for (var x in check_resp) {
                info_enc = check_resp[x].split(",");
                if (id_encuesta == info_enc[0] && id_pregunta == info_enc[1]) {

                    delete check_resp[x];

                }
            }
        }

        if (check_resp.indexOf(id_encuesta + "," + id_pregunta + "," + id_respuesta + "," + cantidadPreguntas + "," + idRespuestaPadre + "," + val_ult_cantResp) == -1) {
            check_resp.push(id_encuesta + "," + id_pregunta + "," + id_respuesta + "," + cantidadPreguntas + "," + idRespuestaPadre + "," + val_ult_cantResp);
            $("#next").prop('disabled', false);

            id_respuestaPadre = idRespuestaPadre;
        }


    } else {

        for (var x in check_resp) {
            info_enc = check_resp[x].split(",");

            if (id_encuesta == info_enc[0] && id_pregunta == info_enc[1] && id_respuesta == info_enc[2]) {

                delete check_resp[x];
            }

        }

        act = 0;
    }

}

function depurar_arreglo(arre) {

    var array_padre = new Array();
    var array_hijo = new Array();
    var bandLlamar = 0;

    arre.forEach(function(value, x) {

        var id_respuesta = value["id_resp"];
        var idRespuestaAnida = value["idRespuestaAnida"];

        array_padre.push(id_respuesta);

        if (idRespuestaAnida > 0) {
            array_hijo.push(idRespuestaAnida);
        }

    });

    for (var i in array_hijo) {
        if (array_padre.indexOf(array_hijo[i]) === -1) {
            arre.forEach(function(datos, p) {
                if (datos["idRespuestaAnida"] == array_hijo[i]) {

                    delete arre[p];
                }
            });
            bandLlamar = 1;
        }
    }

    if (bandLlamar) {
        array_depurado = [];
        depurar_arreglo(arre);
    } else {
        array_depurado = [];
        arre.forEach(function(datoss, b) {

            var id_encuesta = datoss["id_enc"];
            var id_pregunta = datoss["id_pre"];
            var id_respuesta = datoss["id_resp"];
            var cantidadPregunta = datoss["cantidadPregunta"];
            var idRespuestaAnida = datoss["idRespuestaAnida"];
            var respuesta_texto = datoss["respuesta_texto"];

            array_depurado.push({ id_enc: id_encuesta, id_pre: id_pregunta, id_resp: id_respuesta, cantidadPregunta: cantidadPregunta, idRespuestaAnida: +idRespuestaAnida, respuesta_texto: respuesta_texto });
        });

    }

}


function validar_numeros() {


    if (window.event) {
        keynum = window.event.keyCode;
    } else {
        keynum = window.event.which;
    }
    if ((keynum > 47 && keynum < 58) || (keynum == 13)) {
        return true;
    } else {
        return false;
    }

}