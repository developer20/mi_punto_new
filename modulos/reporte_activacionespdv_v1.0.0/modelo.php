<?php
require("../../config/mainModel.php");
class reporte_altas_combos {
/*
  METODOS DE LA BD.
  $this->BD->consultar($query); // Ejecuta la consulta y devuelve string.!
  $this->BD->devolver_array($query); // Ejecuta la consulta y devuelve array asociativo.!
  $this->BD->consultar("BEGIN"); // Antes de transacciones.!
  $this->BD->consultar("COMMIT"); // Commit para guardar datos.!
  $this->BD->consultar("ROLLBACK"); // Devolver datos si hay error.!
  $this->BD->numreg($query); // Devuelve el numero de registros de la consulta.!
*/
	public function __construct() 
    {
      $BD=new BD();
      $this->BD = $BD;
      $this->BD->conectar();

      $oSession = unserialize($_SESSION[$GLOBALS["SESION_POS"]]);
      $this->idDistri = $oSession->VSid_distri;
      $this->punto  =  $oSession->VSidpos;

    $sql_dis = "SELECT nombre_corto FROM {$GLOBALS["BD_NAME"]}.distribuidores WHERE id = $this->idDistri";
      $query_dis = $this->BD->devolver_array($sql_dis);
      $nombre_corto = $query_dis[0]["nombre_corto"];
      $bd = "{$GLOBALS["BD_DIS"]}" . $nombre_corto . "_" . $this->idDistri;

      //$this->id_usu = $oSession->VSid;
      //$this->idDistri = $oSession->VSidDistri;
    }
    public function __destruct()
    {
      $this->BD->desconectar();
    }

    public function Retornar_Altas($fecha_ini,$fecha_fin,$op)
    {

      //VALIDACION DE QUE SOLO SEAN 31 DIAS 

      $tipo = 1;
              
              $dias = (strtotime($fecha_ini)-strtotime($fecha_fin))/86400;
              $dias = abs($dias); $dias = floor($dias);


              if(($dias+1) > 31){
                return array(
                          "estado" => -1,
                          "mns"=>"Solo se permite la consulta de un rango maximo de 31 días."
                      );
              }
        //VALIDACION DE QUE SOLO SEAN 31 DIAS FIN 
       //$permisos_regio = $this->BD->Permisos_Dcs_Regionales();
       //$permisos_terr = $this->BD->Permisos_Dcs_Territorios();
       //$permisos_zona = $this->BD->Permisos_Dcs_Zonas();

       $res_total = array();
       $res = array();
       $response = array();

       if($fecha_ini=="" || $fecha_fin==""){
        $response = array("error"=>1);
        return $response;
       }

       $sql = "";

  // ********** Opcion 1 para consultar los datos que se van a ver en la web **********

      if($op == 1){

          $perms = " AND id_pos = $this->punto";

          /*

          $perms = " AND distri = $this->idDistri";

          if($permisos_regio != ""){
            $perms .= " AND regional in ($permisos_regio,0)";
          }

          if($permisos_terr != ""){
            $perms .= " AND territorio in ($permisos_terr,0)";
          }

          if($permisos_zona != ""){
            $perms .= " AND zona in ($permisos_zona,0)";
          }
          */

         $sql1 = "SELECT count(id) cant_act,1 tipo,fecha_ac,regional,distri,territorio,zona,id_pos,depto,ciudad FROM {$GLOBALS["BD_NAME"]}.simcards WHERE activo = 1  AND fecha_ac >= '$fecha_ini' AND fecha_ac <= '$fecha_fin' $perms"; 
         //$sql1 = "SELECT count(id) cant_act,1 tipo,fecha_ac,regional,distri,territorio,zona,id_pos,depto,ciudad FROM {$GLOBALS["BD_NAME"]}.simcards WHERE activo = 1  AND fecha_ac >= '$fecha_ini' AND fecha_ac <= '$fecha_fin' $perms";
         //$sql2 = "SELECT count(id) cant_act,2 tipo,fecha_ac,regional,distri,territorio,zona,id_pos,depto,ciudad FROM {$GLOBALS["BD_NAME"]}.producto WHERE activo = 1 and combo = 1 AND id_combo > 0 AND fecha_ac >= '$fecha_ini' AND fecha_ac <= '$fecha_fin' $perms";
         //$sql3 = "SELECT count(id) cant_act,3 tipo,fecha_ac,regional,distri,territorio,zona,id_pos,depto,ciudad FROM {$GLOBALS["BD_NAME"]}.producto WHERE activo = 1 and combo = 0 AND id_combo > 0 AND fecha_ac >= '$fecha_ini' AND fecha_ac <= '$fecha_fin' $perms";

         $sql = "SELECT 
         cant_act,
         tipo,
         regional,
         distri,
         territorio,
         zona,
         id_pos,
         CASE tipo WHEN 1 THEN 'SIMCARD' WHEN 2 THEN 'COMBO' WHEN 3 THEN 'DESCOMBOTIZADO' END tipo_alta,
         fecha_ac,CASE WHEN sum(cant_act) IS NULL THEN 0 WHEN sum(cant_act) IS NOT NULL THEN sum(cant_act) END total_act,depto,ciudad
         FROM 
         (".$sql1.") pro 
         WHERE 
         tipo > 0";

         if($tipo != ""){
              $sql .= " AND tipo = $tipo";
         }
   
   // ********** Opcion 2 para consultar los datos que se van a ver en el CSV **********

      }elseif($op == 2){

  // ********** Consulta las altas de las simcards que se activaron sin combo **********

         $sql_ref = "(SELECT producto FROM {$GLOBALS["BD_NAME"]}.referencias WHERE id = pro.id_referencia)";
         $sql_ref_combo = "(SELECT referencias.producto FROM {$GLOBALS["BD_NAME"]}.referencias,{$GLOBALS["BD_NAME"]}.simcards WHERE referencias.id = simcards.id_referencia AND simcards.id_combo = pro.id_combo)";

         $sql1 = "SELECT 
         pro.fecha_ac,
         CASE WHEN pro.fecha_venta IS NULL THEN 'N/A' WHEN pro.fecha_venta IS NOT NULL THEN pro.fecha_venta END as fecha_venta,
         IF(pro.tipo_alta = 1,'Simcard',IF(pro.tipo_alta = 2,'Combo',IF(pro.tipo_alta = 3,'Descombotizado',''))) AS tipo_alta,

         IF(pro.tipo_alta = 1,(SELECT 'Con colocacion' colocacion FROM {$GLOBALS["BD_NAME"]}.simcards WHERE estado = 1 AND id_pos > 0 AND activo = 1 AND combo = 0 AND id = pro.id UNION ALL SELECT 'Sin colocacion' colocacion FROM {$GLOBALS["BD_NAME"]}.simcards WHERE estado = 0 AND id_pos = 0 AND activo = 1 AND combo = 0 and id_combo = 0 AND id = pro.id),

         IF(pro.tipo_alta = 2,(SELECT 'Con colocacion' colocacion FROM {$GLOBALS["BD_NAME"]}.producto WHERE estado = 1 AND id_pos > 0 AND activo = 1 AND combo = 1 and id_combo > 0 AND id = pro.id UNION ALL SELECT 'Sin colocacion' colocacion FROM {$GLOBALS["BD_NAME"]}.producto WHERE estado = 0 AND id_pos = 0 AND activo = 1 AND combo = 1 and id_combo > 0 AND id = pro.id),

         IF(pro.tipo_alta = 3,(SELECT 'Con colocacion' colocacion FROM {$GLOBALS["BD_NAME"]}.producto WHERE estado = 1 AND id_pos > 0 AND activo = 1 AND combo = 0 and id_combo > 0 AND id = pro.id UNION ALL SELECT 'Sin colocacion' colocacion FROM {$GLOBALS["BD_NAME"]}.producto WHERE estado = 0 AND id_pos = 0 AND activo = 1 AND combo = 0 and id_combo > 0 AND id = pro.id),''))) colocacion,
         IF(pro.tipo_alta = 2 || pro.tipo_alta = 3,$sql_ref,'N/A') AS nom_sku_pro,
         IF(pro.tipo_alta = 2 || pro.tipo_alta = 3,$sql_ref_combo,IF(pro.tipo_alta = 1,$sql_ref,'N/A')) AS nom_sku_sim,
         IF(pro.tipo_alta = 2 || pro.tipo_alta = 3,CONCAT(pro.imei,'.'),'N/A') AS imei,
         IF(pro.tipo_alta = 1 || pro.tipo_alta = 2 || pro.tipo_alta = 3,CONCAT(pro.iccid,'.'),'N/A') AS iccid,
         CASE WHEN concat(usu.nombre,' ',usu.apellido) IS NULL THEN 'N/A' WHEN concat(usu.nombre,' ',usu.apellido) IS NOT NULL THEN concat(usu.nombre,' ',usu.apellido) END as nombre_usuario,
         CASE WHEN pos.idpos IS NULL THEN 'N/A' WHEN pos.idpos IS NOT NULL THEN pos.idpos END as id_punto,
         CASE WHEN pos.razon IS NULL THEN 'N/A' WHEN pos.razon IS NOT NULL THEN pos.razon END as razon,
         CASE WHEN reg.nombre IS NULL THEN 'N/A' WHEN reg.nombre IS NOT NULL THEN reg.nombre END as nom_reg,
         CASE WHEN ter.nombre IS NULL THEN 'N/A' WHEN ter.nombre IS NOT NULL THEN ter.nombre END as nom_ter,
         CASE WHEN zonas.nombre IS NULL THEN 'N/A' WHEN zonas.nombre IS NOT NULL THEN zonas.nombre END as nom_zona,
         CASE WHEN de.nombre IS NULL THEN 'N/A' WHEN de.nombre IS NOT NULL THEN de.nombre END as nom_depa,
         CASE WHEN mun.nombre IS NULL THEN 'N/A' WHEN mun.nombre IS NOT NULL THEN mun.nombre END as nom_provi
         pro.tipo_alta tipo,
         pro.distri,
         pro.regional,
         pro.territorio,
         pro.zona,
         pro.id_pos,
         pro.depto,
         pro.ciudad
         FROM
         (SELECT fecha_ac,fecha_venta,id,iccid,distri,regional,territorio,zona,id_pos,depto,ciudad,id_referencia,id_vendedor,1 tipo_alta,'' imei,0 id_combo FROM {$GLOBALS["BD_NAME"]}.simcards WHERE activo = 1  AND fecha_ac >= '$fecha_ini' AND fecha_ac <= '$fecha_fin') pro LEFT JOIN
         $bd.usuarios usu ON usu.id = pro.id_vendedor LEFT JOIN
         {$GLOBALS["BD_POS"]}.puntos pos ON pos.idpos = pro.id_pos LEFT JOIN
         {$GLOBALS["BD_NAME"]}.regional reg ON reg.id = pro.regional LEFT JOIN
         {$GLOBALS["BD_NAME"]}.territorios ter ON ter.id = pro.territorio LEFT JOIN
         {$GLOBALS["BD_NAME"]}.zonas ON zonas.id = pro.zona AND zonas.territorio = ter.id LEFT JOIN
         {$GLOBALS["BD_NAME"]}.departamentos de ON pos.depto = de.id LEFT JOIN
         {$GLOBALS["BD_NAME"]}.municipios mun ON pro.ciudad = mun.id and mun.departamento = de.id";

  // ********** Une las consultas anteriores en una sola para organizar la información **********

         $sql = "SELECT 
         fecha_ac,fecha_venta,tipo_alta,colocacion,nom_sku_pro,nom_sku_sim,imei,iccid,nombre_usuario,id_punto,razon,nom_reg,nom_ter,nom_zona,nom_depa,nom_provi,tipo,regional,territorio,zona,id_pos,depto,ciudad
         FROM 
         (".$sql1.") pro 
         WHERE tipo > 0 AND pro.distri = $this->idDistri";

        /*
          if($permisos_regio != ""){
            $sql .= " AND pro.regional in ($permisos_regio,0)";
          }

          if($permisos_terr != ""){
            $sql .= " AND pro.territorio in ($permisos_terr,0)";
          }

          if($permisos_zona != ""){
            $sql .= " AND pro.zona in ($permisos_zona,0)";
          }
          */


      // ********** Filtros de las consultas anteriores para el CSV **********


          if($depa != ''){
              $city = "";

              $sqls = "SELECT id FROM {$GLOBALS["BD_NAME"]}.municipios WHERE departamento = $depa";
              $res_depa = $this->BD->devolver_array($sqls);

              foreach ($res_depa as $muni) {
                 $city.=$muni["id"].","; 
              }
              $city=substr($city,0,-1);

              $sql .= " AND pro.ciudad in ($city)";
          }

          if($pro != ''){
              $sql .= " AND pro.ciudad = $pro";
          }

          if($tipo != ""){
              $sql .= " AND pro.tipo = $tipo";
          }
         
      }
  
  // ********** La opcion 1 para devolver los datos y agrupar por el tipo de altas en el reporte de la web **********

      if($op == 1){

         $res_total = $this->BD->devolver_array($sql);
         
         $sql .= " GROUP BY tipo";

         $res = $this->BD->devolver_array($sql);

         return array("res_total" => $res_total,"datos" => $res);

  // ********** La opcion 2 para devolver los datos en el reporte del CSV **********

      }elseif($op == 2){

         $datos = $this->BD->devolver_array($sql);

         
         for ($i=0; $i < count($datos); $i++) { 
             $datos[$i]["razon"] = utf8_encode($datos[$i]["razon"]);
             $datos[$i]["10"] = utf8_encode($datos[$i]["10"]);
         }

         return array("datos"=>$datos,"estado"=>1);
      }

  }

  public function Retornar_Coloca($fecha_ini,$fecha_fin,$tipo){

    //$permisos_regio = $this->BD->Permisos_Dcs_Regionales();
    //$permisos_terr = $this->BD->Permisos_Dcs_Territorios();
    //$permisos_zona = $this->BD->Permisos_Dcs_Zonas();

    $perms = " AND id_pos = $this->punto";

    //$perms = " and distri = $this->idDistri";

    /*if($permisos_regio != ""){
      $perms .= " AND regional in ($permisos_regio,0)";
    }

    if($permisos_terr != ""){
      $perms .= " AND territorio in ($permisos_terr,0)";
    }

    if($permisos_zona != ""){
      $perms .= " AND zona in ($permisos_zona,0)";
    }
    */
       
     $sql_pro = "";
     
     if($tipo == 1){
         $sql = "SELECT count(id) cant_act,1 tipo,fecha_ac,IF(id_pos>0,1,0) coloca,regional,distri,territorio,zona,id_pos,depto,ciudad FROM {$GLOBALS["BD_NAME"]}.simcards WHERE activo = 1 AND fecha_ac >= '$fecha_ini' AND fecha_ac <= '$fecha_fin' $perms GROUP BY coloca";
         //$sql = "SELECT count(id) cant_act,1 tipo,fecha_ac,IF(id_pos>0,1,0) coloca,regional,distri,territorio,zona,id_pos,depto,ciudad FROM {$GLOBALS["BD_NAME"]}.simcards WHERE activo = 1 AND fecha_ac >= '$fecha_ini' AND fecha_ac <= '$fecha_fin' $perms GROUP BY coloca";

         $sql_pro = $sql;
     }

     if($tipo == 2){
        $sql = "SELECT count(id) cant_act,2 tipo,fecha_ac,1 coloca,regional,distri,territorio,zona,id_pos,depto,ciudad FROM {$GLOBALS["BD_NAME"]}.producto WHERE activo = 1 and combo = 1 AND id_pos > 0 AND estado = 1 AND id_combo > 0 AND fecha_ac >= '$fecha_ini' AND fecha_ac <= '$fecha_fin' $perms UNION SELECT count(id) cant_act,2 tipo,fecha_ac,0 coloca,regional,distri,territorio,zona,id_pos,depto,ciudad FROM {$GLOBALS["BD_NAME"]}.producto WHERE activo = 1 and combo = 1 AND id_pos = 0 AND estado = 0 AND id_combo > 0 AND fecha_ac >= '$fecha_ini' AND fecha_ac <= '$fecha_fin' $perms";
        //$sql = "SELECT count(id) cant_act,2 tipo,fecha_ac,1 coloca,regional,distri,territorio,zona,id_pos,depto,ciudad FROM {$GLOBALS["BD_NAME"]}.producto WHERE activo = 1 and combo = 1 AND id_pos > 0 AND estado = 1 AND id_combo > 0 AND fecha_ac >= '$fecha_ini' AND fecha_ac <= '$fecha_fin' $perms UNION SELECT count(id) cant_act,2 tipo,fecha_ac,0 coloca,regional,distri,territorio,zona,id_pos,depto,ciudad FROM {$GLOBALS["BD_NAME"]}.producto WHERE activo = 1 and combo = 1 AND id_pos = 0 AND estado = 0 AND id_combo > 0 AND fecha_ac >= '$fecha_ini' AND fecha_ac <= '$fecha_fin' $perms";

        $sql_pro = $sql;
     }
     
     if($tipo == 3){

        $sql = "SELECT count(id) cant_act,3 tipo,fecha_ac,1 coloca,regional,distri,territorio,zona,id_pos,depto,ciudad FROM {$GLOBALS["BD_NAME"]}.producto WHERE activo = 1 and combo = 0 AND id_pos > 0 AND estado = 1 AND id_combo > 0 AND fecha_ac >= '$fecha_ini' AND fecha_ac <= '$fecha_fin' $perms UNION SELECT count(id) cant_act,3 tipo,fecha_ac,0 coloca,regional,distri,territorio,zona,id_pos,depto,ciudad FROM {$GLOBALS["BD_NAME"]}.producto WHERE activo = 1 and combo = 0 AND id_pos = 0 AND estado = 0 AND id_combo > 0 AND fecha_ac >= '$fecha_ini' AND fecha_ac <= '$fecha_fin' $perms";
        //$sql = "SELECT count(id) cant_act,3 tipo,fecha_ac,1 coloca,regional,distri,territorio,zona,id_pos,depto,ciudad FROM {$GLOBALS["BD_NAME"]}.producto WHERE activo = 1 and combo = 0 AND id_pos > 0 AND estado = 1 AND id_combo > 0 AND fecha_ac >= '$fecha_ini' AND fecha_ac <= '$fecha_fin' $perms UNION SELECT count(id) cant_act,3 tipo,fecha_ac,0 coloca,regional,distri,territorio,zona,id_pos,depto,ciudad FROM {$GLOBALS["BD_NAME"]}.producto WHERE activo = 1 and combo = 0 AND id_pos = 0 AND estado = 0 AND id_combo > 0 AND fecha_ac >= '$fecha_ini' AND fecha_ac <= '$fecha_fin' $perms";

        $sql_pro = $sql;

      }

      $sql = "SELECT 
       sum(cant_act) cant_act,
       tipo,
       coloca,
       regional,
       distri,
       territorio,
       zona,
       id_pos,
       CASE coloca WHEN 1 THEN 'Con Colocacion' WHEN 0 THEN 'Sin Colocacion' END colocacion,
       fecha_ac,depto,ciudad
       FROM 
       (".$sql_pro.") pro WHERE tipo = $tipo";

      $sql .= " GROUP BY coloca";

      return $this->BD->devolver_array($sql);
  }





  public function Retornar_Reg($fecha_ini,$fecha_fin,$tipo,$coloca){
      
      //$permisos_regio = $this->BD->Permisos_Dcs_Regionales();
      //$permisos_terr = $this->BD->Permisos_Dcs_Territorios();
      //$permisos_zona = $this->BD->Permisos_Dcs_Zonas();

      $perms = " AND pro.id_pos = $this->punto";

      /*$perms = " and pro.distri = $this->idDistri";

      if($permisos_regio != ""){
        $perms .= " AND pro.regional in ($permisos_regio,0)";
      }

      if($permisos_terr != ""){
        $perms .= " AND pro.territorio in ($permisos_terr,0)";
      }

      if($permisos_zona != ""){
        $perms .= " AND pro.zona in ($permisos_zona,0)";
      }
      */

      if($fecha_ini != "" && $fecha_fin != ""){
           $perms .= " AND pro.fecha_ac >= '$fecha_ini' AND pro.fecha_ac <= '$fecha_fin'";
      }elseif($fecha_ini != "" && $fecha_fin == ""){
           $perms .= " AND pro.fecha_ac = '$fecha_ini'";
      }elseif($fecha_ini == "" && $fecha_fin != ""){
           $perms .= " AND pro.fecha_ac = '$fecha_fin'";
      }

      $sql_pro = ""; 

      if($tipo == 1){
          $sql = "SELECT count(pro.id) cant_act,1 tipo,pro.fecha_ac,reg.nombre nom_reg,reg.id id_reg,pro.distri,pro.territorio,pro.zona,pro.id_pos,pro.depto,pro.ciudad FROM {$GLOBALS["BD_NAME"]}.simcards pro,{$GLOBALS["BD_NAME"]}.regional reg WHERE pro.activo = 1 and combo = 0 and id_combo = 0 and reg.id = pro.regional and pro.estado = 1 and pro.id_pos > 0 $perms GROUP BY pro.regional";
          //$sql = "SELECT count(pro.id) cant_act,1 tipo,pro.fecha_ac,reg.nombre nom_reg,reg.id id_reg,pro.distri,pro.territorio,pro.zona,pro.id_pos,pro.depto,pro.ciudad FROM {$GLOBALS["BD_NAME"]}.simcards pro,{$GLOBALS["BD_NAME"]}.regional reg WHERE pro.activo = 1 and combo = 0 and id_combo = 0 and reg.id = pro.regional and pro.estado = 1 and pro.id_pos > 0 $perms GROUP BY pro.regional";

          $sql_pro = $sql;
       }

       if($tipo == 2){
          $sql = "SELECT count(pro.id) cant_act,2 tipo,pro.fecha_ac,reg.nombre nom_reg,reg.id id_reg,pro.distri,pro.territorio,pro.zona,pro.id_pos,pro.depto,pro.ciudad, FROM {$GLOBALS["BD_NAME"]}.producto pro,{$GLOBALS["BD_NAME"]}.regional reg WHERE pro.activo = 1 and combo = 1 and id_combo > 0 and reg.id = pro.regional and pro.estado = 1 and pro.id_pos > 0 $perms GROUP BY pro.regional";
          //$sql = "SELECT count(pro.id) cant_act,2 tipo,pro.fecha_ac,reg.nombre nom_reg,reg.id id_reg,pro.distri,pro.territorio,pro.zona,pro.id_pos,pro.depto,pro.ciudad, FROM {$GLOBALS["BD_NAME"]}.producto pro,{$GLOBALS["BD_NAME"]}.regional reg WHERE pro.activo = 1 and combo = 1 and id_combo > 0 and reg.id = pro.regional and pro.estado = 1 and pro.id_pos > 0 $perms GROUP BY pro.regional";

          $sql_pro = $sql;
       }

       if($tipo == 3){

          $sql = "SELECT count(pro.id) cant_act,3 tipo,pro.fecha_ac,reg.nombre nom_reg,reg.id id_reg,pro.distri,pro.territorio,pro.zona,pro.id_pos,pro.depto,pro.ciudad FROM {$GLOBALS["BD_NAME"]}.producto pro,{$GLOBALS["BD_NAME"]}.regional reg WHERE pro.activo = 1 and combo = 0 and id_combo > 0 and reg.id = pro.regional and pro.estado = 1 and pro.id_pos > 0 $perms GROUP BY pro.regional";
          //$sql = "SELECT count(pro.id) cant_act,3 tipo,pro.fecha_ac,reg.nombre nom_reg,reg.id id_reg,pro.distri,pro.territorio,pro.zona,pro.id_pos,pro.depto,pro.ciudad FROM {$GLOBALS["BD_NAME"]}.producto pro,{$GLOBALS["BD_NAME"]}.regional reg WHERE pro.activo = 1 and combo = 0 and id_combo > 0 and reg.id = pro.regional and pro.estado = 1 and pro.id_pos > 0 $perms GROUP BY pro.regional";

          $sql_pro = $sql;
       }

       $sql = "SELECT 
       cant_act,
       tipo,
       fecha_ac,
       nom_reg,
       id_reg,
       distri,depto,ciudad
       FROM 
       (".$sql_pro.") pro WHERE tipo = $tipo";

      return $this->BD->devolver_array($sql);
  }

  public function Retornar_Terri($fecha_ini,$fecha_fin,$tipo,$coloca,$id_reg){

      //$permisos_regio = $this->BD->Permisos_Dcs_Regionales();
      //$permisos_terr = $this->BD->Permisos_Dcs_Territorios();
      //$permisos_zona = $this->BD->Permisos_Dcs_Zonas();

      $perms = " AND pro.id_pos = $this->punto";

      //$perms = " and pro.distri = $this->idDistri";

    /*
      if($permisos_regio != ""){
        $perms .= " AND pro.regional in ($permisos_regio,0)";
      }

      if($permisos_terr != ""){
        $perms .= " AND pro.territorio in ($permisos_terr,0)";
      }

      if($permisos_zona != ""){
        $perms .= " AND pro.zona in ($permisos_zona,0)";
      }
      */

      $sql_pro = ""; 

      if($tipo == 1){
        $sql = "SELECT count(pro.id) cant_act,1 tipo,pro.fecha_ac,ter.nombre nom_terri,pro.regional,pro.distri,pro.territorio,pro.zona,pro.id_pos,pro.depto,pro.ciudad FROM {$GLOBALS["BD_NAME"]}.simcards pro,{$GLOBALS["BD_NAME"]}.territorios ter WHERE pro.activo = 1 and combo = 0 and id_combo = 0 and ter.id = pro.territorio and pro.estado = 1 and pro.id_pos > 0 and pro.regional = $id_reg AND pro.fecha_ac >= '$fecha_ini' AND pro.fecha_ac <= '$fecha_fin' $perms GROUP BY pro.territorio";
        //$sql = "SELECT count(pro.id) cant_act,1 tipo,pro.fecha_ac,ter.nombre nom_terri,pro.regional,pro.distri,pro.territorio,pro.zona,pro.id_pos,pro.depto,pro.ciudad FROM {$GLOBALS["BD_NAME"]}.simcards pro,{$GLOBALS["BD_NAME"]}.territorios ter WHERE pro.activo = 1 and combo = 0 and id_combo = 0 and ter.id = pro.territorio and pro.estado = 1 and pro.id_pos > 0 and pro.regional = $id_reg AND pro.fecha_ac >= '$fecha_ini' AND pro.fecha_ac <= '$fecha_fin' $perms GROUP BY pro.territorio";

        $sql_pro = $sql;
      }

      if($tipo == 2){
          $sql = "SELECT count(pro.id) cant_act,2 tipo,pro.fecha_ac,ter.nombre nom_terri,pro.regional,pro.distri,pro.territorio,pro.zona,pro.id_pos,pro.depto,pro.ciudad FROM {$GLOBALS["BD_NAME"]}.producto pro,{$GLOBALS["BD_NAME"]}.territorios ter WHERE pro.activo = 1 and combo = 1 and id_combo > 0 and ter.id = pro.territorio and pro.estado = 1 and pro.id_pos > 0 and pro.regional = $id_reg AND pro.fecha_ac >= '$fecha_ini' AND pro.fecha_ac <= '$fecha_fin' $perms GROUP BY pro.territorio";
          //$sql = "SELECT count(pro.id) cant_act,2 tipo,pro.fecha_ac,ter.nombre nom_terri,pro.regional,pro.distri,pro.territorio,pro.zona,pro.id_pos,pro.depto,pro.ciudad FROM {$GLOBALS["BD_NAME"]}.producto pro,{$GLOBALS["BD_NAME"]}.territorios ter WHERE pro.activo = 1 and combo = 1 and id_combo > 0 and ter.id = pro.territorio and pro.estado = 1 and pro.id_pos > 0 and pro.regional = $id_reg AND pro.fecha_ac >= '$fecha_ini' AND pro.fecha_ac <= '$fecha_fin' $perms GROUP BY pro.territorio";

          $sql_pro = $sql;
      }

      if($tipo == 3){
          $sql = "SELECT count(pro.id) cant_act,3 tipo,pro.fecha_ac,ter.nombre nom_terri,pro.regional,pro.distri,pro.territorio,pro.zona,pro.id_pos,pro.depto,pro.ciudad FROM {$GLOBALS["BD_NAME"]}.producto pro,{$GLOBALS["BD_NAME"]}.territorios ter WHERE pro.activo = 1 and combo = 0 and id_combo > 0 and ter.id = pro.territorio and pro.estado = 1 and pro.id_pos > 0 and pro.regional = $id_reg AND pro.fecha_ac >= '$fecha_ini' AND pro.fecha_ac <= '$fecha_fin' $perms GROUP BY pro.territorio";

          //$sql = "SELECT count(pro.id) cant_act,3 tipo,pro.fecha_ac,ter.nombre nom_terri,pro.regional,pro.distri,pro.territorio,pro.zona,pro.id_pos,pro.depto,pro.ciudad FROM {$GLOBALS["BD_NAME"]}.producto pro,{$GLOBALS["BD_NAME"]}.territorios ter WHERE pro.activo = 1 and combo = 0 and id_combo > 0 and ter.id = pro.territorio and pro.estado = 1 and pro.id_pos > 0 and pro.regional = $id_reg AND pro.fecha_ac >= '$fecha_ini' AND pro.fecha_ac <= '$fecha_fin' $perms GROUP BY pro.territorio";
          $sql_pro = $sql;
      }

       $sql = "SELECT 
       cant_act,
       tipo,
       nom_terri,
       regional,
       distri,
       territorio,
       zona,
       id_pos,
       fecha_ac,
       distri,depto,ciudad
       FROM 
       (".$sql_pro.") pro WHERE tipo = $tipo";

      return $this->BD->devolver_array($sql);
  }

  public function Retornar_Ruta($fecha_ini,$fecha_fin,$tipo,$coloca,$id_reg,$id_terri){
       
      //$permisos_regio = $this->BD->Permisos_Dcs_Regionales();
      //$permisos_terr = $this->BD->Permisos_Dcs_Territorios();
      //$permisos_zona = $this->BD->Permisos_Dcs_Zonas();

      $perms = " AND pro.id_pos = $this->punto";

      //$perms = " and pro.distri = $this->idDistri";

      /*if($permisos_regio != ""){
        $perms .= " AND pro.regional in ($permisos_regio,0)";
      }

      if($permisos_terr != ""){
        $perms .= " AND pro.territorio in ($permisos_terr,0)";
      }

      if($permisos_zona != ""){
        $perms .= " AND pro.zona in ($permisos_zona,0)";
      }
      */

      $sql_pro = ""; 

      if($tipo == 1){
          $sql = "SELECT count(pro.id) cant_act,1 tipo,pro.fecha_ac,zonas.nombre nom_ruta,pro.regional,pro.distri,pro.territorio,pro.zona,pro.id_pos,pro.depto,pro.ciudad FROM {$GLOBALS["BD_NAME"]}.simcards pro,{$GLOBALS["BD_NAME"]}.zonas WHERE pro.activo = 1 and combo = 0 and id_combo = 0 and zonas.id = pro.zona and pro.estado = 1 and pro.id_pos > 0 AND pro.fecha_ac >= '$fecha_ini' AND pro.fecha_ac <= '$fecha_fin' and pro.territorio = $id_terri $perms  GROUP BY pro.zona";
          //$sql = "SELECT count(pro.id) cant_act,1 tipo,pro.fecha_ac,zonas.nombre nom_ruta,pro.regional,pro.distri,pro.territorio,pro.zona,pro.id_pos,pro.depto,pro.ciudad FROM {$GLOBALS["BD_NAME"]}.simcards pro,{$GLOBALS["BD_NAME"]}.zonas WHERE pro.activo = 1 and combo = 0 and id_combo = 0 and zonas.id = pro.zona and pro.estado = 1 and pro.id_pos > 0 AND pro.fecha_ac >= '$fecha_ini' AND pro.fecha_ac <= '$fecha_fin' and pro.territorio = $id_terri $perms GROUP BY pro.zona";

          $sql_pro = $sql; 
      }

      if($tipo == 2){
          $sql = "SELECT count(pro.id) cant_act,2 tipo,pro.fecha_ac,zonas.nombre nom_ruta,pro.regional,pro.distri,pro.territorio,pro.zona,pro.id_pos,pro.depto,pro.ciudad FROM {$GLOBALS["BD_NAME"]}.producto pro,{$GLOBALS["BD_NAME"]}.zonas WHERE pro.activo = 1 and combo = 1 and id_combo > 0 and zonas.id = pro.zona and pro.estado = 1 and pro.id_pos > 0 AND pro.fecha_ac >= '$fecha_ini' AND pro.fecha_ac <= '$fecha_fin' and pro.territorio = $id_terri $perms  GROUP BY pro.zona";
          //$sql = "SELECT count(pro.id) cant_act,2 tipo,pro.fecha_ac,zonas.nombre nom_ruta,pro.regional,pro.distri,pro.territorio,pro.zona,pro.id_pos,pro.depto,pro.ciudad FROM {$GLOBALS["BD_NAME"]}.producto pro,{$GLOBALS["BD_NAME"]}.zonas WHERE pro.activo = 1 and combo = 1 and id_combo > 0 and zonas.id = pro.zona and pro.estado = 1 and pro.id_pos > 0 AND pro.fecha_ac >= '$fecha_ini' AND pro.fecha_ac <= '$fecha_fin' and pro.territorio = $id_terri $perms GROUP BY pro.zona";

          $sql_pro = $sql; 
      }
      if($tipo == 3){
          $sql = "SELECT count(pro.id) cant_act,3 tipo,pro.fecha_ac,zonas.nombre nom_ruta,pro.regional,pro.distri,pro.territorio,pro.zona,pro.id_pos,pro.depto,pro.ciudad FROM {$GLOBALS["BD_NAME"]}.producto pro,{$GLOBALS["BD_NAME"]}.zonas WHERE pro.activo = 1 and combo = 0 and id_combo > 0 and zonas.id = pro.zona and pro.estado = 1 and pro.id_pos > 0 AND pro.fecha_ac >= '$fecha_ini' AND pro.fecha_ac <= '$fecha_fin' and pro.territorio = $id_terri $perms GROUP BY pro.zona";
          //$sql = "SELECT count(pro.id) cant_act,3 tipo,pro.fecha_ac,zonas.nombre nom_ruta,pro.regional,pro.distri,pro.territorio,pro.zona,pro.id_pos,pro.depto,pro.ciudad FROM {$GLOBALS["BD_NAME"]}.producto pro,{$GLOBALS["BD_NAME"]}.zonas WHERE pro.activo = 1 and combo = 0 and id_combo > 0 and zonas.id = pro.zona and pro.estado = 1 and pro.id_pos > 0 AND pro.fecha_ac >= '$fecha_ini' AND pro.fecha_ac <= '$fecha_fin' and pro.territorio = $id_terri $perms GROUP BY pro.zona";
          $sql_pro = $sql; 
      }

      $sql = "SELECT 
       cant_act,
       tipo,
       nom_ruta,
       regional,
       distri,
       territorio,
       zona,
       id_pos,
       fecha_ac,
       distri,depto,ciudad
       FROM 
       (".$sql_pro.") pro WHERE tipo = $tipo";

      return $this->BD->devolver_array($sql);
  }

  //Metodo para el permiso sobre la regional
  public function Devolver_Permiso_Regio($idVende)
    {
        
    $regionales = array();
    $filtrar_regio = array();
    
    $oSession = unserialize($_SESSION[$GLOBALS["SESION_DIS"]]);
    $idDistri = $oSession->VSidDistri;
    
    $consulta = "SELECT r.id id_regio,r.nombre nom_regio,$idVende id_vende,CASE dcs.tipo WHEN 1 THEN 0 ELSE 1 END as tipo_n FROM {$GLOBALS["BD_NAME"]}.regional r, {$GLOBALS["BD_NAME"]}.regionales_distri rd,niveles_dcs dcs,((SELECT regional FROM {$GLOBALS["BD_NAME"]}.simcards WHERE distri = $idDistri) UNION (SELECT regional FROM {$GLOBALS["BD_NAME"]}.producto WHERE distri = $idDistri)) pro
         WHERE r.id = rd.id_reg AND r.id  = pro.regional AND rd.id_distri = $idDistri AND dcs.id_usus = $idVende AND ((dcs.tipo = 1 AND dcs.id_tipo = r.id) OR dcs.p4 = r.id) GROUP BY r.id";
    $regionales = $this->BD->devolver_array($consulta);

    $regionales[] = array("id_regio" => 0, "nom_regio" => "Venta Directa", 'id_vende' => $idVende, 'tipo_n' => 0);

    return $regionales;
    
    }

    //Metodo para el permiso sobre el territorio
  public function Devolver_Permiso_Terri($idVende,$regionalArray)
    {
      
    $oSession = unserialize($_SESSION[$GLOBALS["SESION_DIS"]]);
    $idDistri = $oSession->VSidDistri;
    $id_user = $oSession->VSid;
    
    $territorios = array();
    
    if(count($regionalArray)>0){
      
      
      foreach($regionalArray as $val_regio){
        
        $regional = $val_regio["id_regio"];
        $regional;
        $tipo_n = $val_regio["tipo_n"];
        
        if($tipo_n == 1){
      
          $consulta = "SELECT t.id id_terri,t.descripcion nom_terri,$regional id_regio,$idVende id_vende,CASE dcs.tipo WHEN 3 THEN 0 ELSE 1 END as tipo_n FROM {$GLOBALS["BD_NAME"]}.territorios t, {$GLOBALS["BD_NAME"]}.territorios_distribuidor td,niveles_dcs dcs
             WHERE t.id = td.id_territorio AND td.id_distri = '$idDistri' AND td.id_regional = '$regional' AND dcs.id_usus = '$idVende' AND ((dcs.tipo = 3 AND dcs.id_tipo = t.id AND dcs.p3 = 1 AND dcs.p4 = $regional) OR (dcs.p2 = t.id AND dcs.p3 = 1 AND dcs.p4 = $regional))";
          $territorios = $this->BD->devolver_array($consulta);
          
        }else{
          $consulta = "SELECT t.id id_terri,t.descripcion nom_terri,$regional id_regio,$idVende id_vende,0 tipo_n FROM {$GLOBALS["BD_NAME"]}.territorios t, {$GLOBALS["BD_NAME"]}.territorios_distribuidor td
             WHERE t.id = td.id_territorio AND td.id_distri = '$idDistri' AND td.id_regional = '$regional'";
          $territorios = $this->BD->devolver_array($consulta);
        }
        
      }
    }
    
    return $territorios;
    
    }

    //Metodo para el permiso sobre la zona
  public function Devolver_Permiso_Zonas($idVende,$territorioArray)
    {
    
    $oSession = unserialize($_SESSION[$GLOBALS["SESION_DIS"]]);
    $idDistri = $oSession->VSidDistri;
    $id_user = $oSession->VSid;
    
    $zonas = array();
  
    if(count($territorioArray)>0){
    
      
      foreach($territorioArray as $val_terri){
        
        $regional = $val_terri["id_regio"];
        $id_territorio = $val_terri['id_terri'];
        $tipo_n = $val_terri["tipo_n"];
        
        if($tipo_n == 1){
        
          $consulta = "SELECT zonas.id id_zona,zonas.nombre nom_zona,$regional id_regio,$id_territorio id_terri,$idVende id_vende,CASE dcs.tipo WHEN 4 THEN 0 ELSE 1 END as tipo_n FROM {$GLOBALS["BD_NAME"]}.zonas,niveles_dcs dcs WHERE zonas.territorio = '$id_territorio' AND id_usus = '$idVende' AND ((dcs.tipo = 4 AND dcs.id_tipo = zonas.id AND p2 = '$id_territorio' AND dcs.p3 = 1 AND dcs.p4 = '$regional') OR (dcs.p1 = zonas.id AND dcs.p2 = '$id_territorio' AND dcs.p3 = 1 AND dcs.p4 = '$regional'))";
          $zonas = $this->BD->devolver_array($consulta);
        
         }else{
          $consulta = "SELECT id id_zona,nombre nom_zona,$regional id_regio,$id_territorio id_terri,$idVende id_vende,0 tipo_n FROM {$GLOBALS["BD_NAME"]}.zonas WHERE territorio = '$id_territorio'";
          $zonas = $this->BD->devolver_array($consulta);
         }
      
      }
      
    }

    return $zonas;
    
    }
   
  
}// Fin clase
?>