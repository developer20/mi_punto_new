dtable = "";

$(document).ready(function() {
    $(".fecha").datepicker({
        format: "yyyy-mm-dd",
        todayHighlight: true,
        todayBtn: true
	});
	
	$(".content-header").find("h1").html("Mis Activaciones");

    $("#frm_datos").submit(function(event) {
        event.preventDefault();
        cargar_reporte();
    });

    $("#CSV").click(function() {
        var fecha_ini = $("#fecha_ini").val();
        var fecha_fin = $("#fecha_fin").val();

        window.open(
            "modulos/reporte_activacionespdv_v1.0.0/reporte_csv.php?fecha_ini=" +
            fecha_ini +
            "&fecha_fin=" +
            fecha_fin 
        );
    });
});


function cargar_reporte() {
    var fecha_ini = $("#fecha_ini").val();
    var fecha_fin = $("#fecha_fin").val();

    if (fecha_ini == "" || fecha_fin == "") {
        Notificacion("El rango de fechas es obligatorio", "warning");
    } else {
        $.post(
            "modulos/reporte_activacionespdv_v1.0.0/controlador.php", {
                accion: "datosAltas",
                fecha_ini: fecha_ini,
                fecha_fin: fecha_fin,
                op: 1
            },
            function(data, textStatus) {
                var reporte = "";
                var reporte_sin = "";
                data = JSON.parse(data);
                var total_html = "";
                if (!$.isEmptyObject(data) && data.estado != -1) {
                    var datos_t = data["res_total"];
                    var datos = data["datos"];
                    error = data["error"];
                    var total = 0;

                    if (error == 1) {
                        Notificacion("El rango de fechas es obligatorio", "warning");
                        return false;
                    }

                    //Recorrer los datos de la agrupacion por regional.!

                    $.each(datos_t, function(index, fila) {

                        total += parseInt(fila.total_act);

                        total_html += "<tr>";
                        total_html +=
                            "<td style='text-align: center;'>" +
                            addCommas(fila.total_act) +
                            "</td>";
                        total_html +=
                            "<td style='text-align: center;'><i style='cursor:pointer; font-size:19px;' data-toggle='collapse' class='glyphicon glyphicon-plus-sign text-primary det_altas'></i></td>";
                        total_html += "</tr>";
                        total_html +=
                            "<tr><td id='ver_altas' colspan='3' style='display:none'></td></tr>";
                    });

                    $("#t_reporte_tot").html(total_html);
                    $("#consulta").show();

                    if (total == 0) {
                        Notificacion("No se encontraron resultados con los parámetros de búsqueda", "warning");
                        $("#consulta").hide();
                        return;
                    }

                    $(".det_altas").unbind("click");
                    $(".det_altas").click(function() {
                        var elemento = $(this);
                        if (elemento.hasClass("det_altas")) {
                            elemento.attr(
                                "class",
                                "glyphicon glyphicon-minus-sign text-primary CloseDetaC"
                            );

                            tabla =
                                "<div class='col-md-12' style='border:2px #ccc solid; border-radius:2px;'>";
                            tabla +=
                                '<table class="table table-striped dt-responsive nowrap" cellspacing="0" width="100%">';
                            tabla += $("#t_altas").html();
                            $.each(datos, function(index, fila) {
                                tabla +=
                                    "<tr class='dad' data-tipo_alta = '" + fila.tipo + "'>";
                                tabla +=
                                    "<td style='text-align: center;'>" + fila.tipo_alta + "</td>";
                                tabla +=
                                    "<td style='text-align: center;'>" +
                                    addCommas(fila.cant_act, 0) +
                                    "</td>";
                                tabla +=
                                    "<td style='text-align: center;'><i style='cursor:pointer; font-size:19px;' data-toggle='collapse' class='glyphicon glyphicon-plus-sign text-primary det_coloca' id='det_coloca_" +
                                    fila.tipo +
                                    "'></i></td>";
                                tabla += "</tr>";
                                tabla +=
                                    "<tr><td id='ver_coloca_" +
                                    fila.tipo +
                                    "' colspan='3' style='display:none'></td></tr>";
                            });
                            tabla += "</table>";
                            tabla += "</div>";

                            $("#ver_altas").html(tabla);
                            $("#ver_altas").show();

                            $(".det_coloca").unbind("click");
                            $(".det_coloca").click(function() {
                                var tipo_alta = $(this)
                                    .parents("tr.dad")
                                    .attr("data-tipo_alta");

                                var elemento = $("#det_coloca_" + tipo_alta);

                                if (elemento.hasClass("det_coloca")) {
                                    elemento.attr(
                                        "class",
                                        "glyphicon glyphicon-minus-sign text-primary CloseDetaColo"
                                    );

                                    $.post(
                                        "modulos/reporte_activacionespdv_v1.0.0/controlador.php", {
                                            accion: "datosColocacion",
                                            fecha_ini: fecha_ini,
											fecha_fin: fecha_fin,
											tipo: tipo_alta

                                        },
                                        function(data, textStatus) {
                                            var reporte = "";
                                            var reporte_sin = "";
                                            data = JSON.parse(data);
                                            var total_html = "";
                                            if (!$.isEmptyObject(data)) {
                                                //Recorrer los datos de la agrupacion por regional.!

                                                tabla =
                                                    "<div class='col-md-12' style='border:2px #ccc solid; border-radius:2px;'>";
                                                tabla +=
                                                    '<table class="table table-striped dt-responsive nowrap" cellspacing="0" width="100%">';
                                                tabla += $("#t_altas_coloca").html();

                                                $.each(data, function(index, fila) {
                                                    tabla +=
                                                        "<tr class='dad2' data-tipo_alta = '" +
                                                        tipo_alta +
                                                        "' data-coloca = '" +
                                                        fila.coloca +
                                                        "'>";
                                                    tabla +=
                                                        "<td style='text-align: center;'>" +
                                                        fila.colocacion +
                                                        "</td>";
                                                    tabla +=
                                                        "<td style='text-align: center;'>" +
                                                        addCommas(fila.cant_act) +
                                                        "</td>";
                                                    if (fila.coloca == 1) {
                                                        tabla +=
                                                            "<td style='text-align: center;'><i style='cursor:pointer; font-size:19px;' class='glyphicon glyphicon-plus-sign text-primary det_reg' id='det_reg_" +
                                                            tipo_alta +
                                                            "_" +
                                                            fila.coloca +
                                                            "'></i></td>";
                                                    } else {
                                                        tabla += "<td style='text-align: center;'></td>";
                                                    }
                                                    tabla += "</tr>";
                                                    tabla +=
                                                        "<tr><td id='ver_reg_" +
                                                        tipo_alta +
                                                        "_" +
                                                        fila.coloca +
                                                        "' colspan='3' style='display:none'></td></tr>";
                                                });

                                                tabla += "</table>";
                                                tabla += "</div>";

                                                $("#ver_coloca_" + tipo_alta).html(tabla);
                                                $("#ver_coloca_" + tipo_alta).show();

                                                $(".det_reg").unbind("click");
                                                $(".det_reg").click(function() {
                                                    var tipo_alta = $(this)
                                                        .parents("tr.dad2")
                                                        .attr("data-tipo_alta");
                                                    var coloca = $(this)
                                                        .parents("tr.dad2")
                                                        .attr("data-coloca");

                                                    if (coloca == 1) {
                                                        var elemento = $(
                                                            "#det_reg_" + tipo_alta + "_" + coloca
                                                        );

                                                        if (elemento.hasClass("det_reg")) {
                                                            elemento.attr(
                                                                "class",
                                                                "glyphicon glyphicon-minus-sign text-primary CloseDetaR"
                                                            );

                                                            $.post(
                                                                "modulos/reporte_activacionespdv_v1.0.0/controlador.php", {
                                                                    accion: "datosReg",
                                                                    fecha_ini: fecha_ini,
																	fecha_fin: fecha_fin,
																	tipo: tipo_alta,
																	coloca: coloca

                                                                },
                                                                function(data, textStatus) {
                                                                    var reporte = "";
                                                                    var reporte_sin = "";
                                                                    data = JSON.parse(data);
                                                                    if (!$.isEmptyObject(data)) {
                                                                        tabla =
                                                                            "<div class='col-md-12' style='border:2px #ccc solid; border-radius:2px;'>";
                                                                        tabla +=
                                                                            '<table class="table table-striped dt-responsive nowrap" cellspacing="0" width="100%">';
                                                                        tabla += $("#t_altas_reg").html();
                                                                        $.each(data, function(index, fila) {
                                                                            tabla +=
                                                                                "<tr class='dad4' data-tipo_alta = '" +
                                                                                tipo_alta +
                                                                                "' data-coloca = '" +
                                                                                coloca +
                                                                                "' data-id_reg = '" +
                                                                                fila.id_reg +
                                                                                "'>";
                                                                            tabla +=
                                                                                "<td style='text-align: center;'>" +
                                                                                fila.nom_reg +
                                                                                "</td>";
                                                                            tabla +=
                                                                                "<td style='text-align: center;'>" +
                                                                                addCommas(fila.cant_act) +
                                                                                "</td>";
                                                                            tabla +=
                                                                                "<td style='text-align: center;'><i style='cursor:pointer; font-size:19px;' class='glyphicon glyphicon-plus-sign text-primary det_ter' id='det_ter_" +
                                                                                tipo_alta +
                                                                                "_" +
                                                                                coloca +
                                                                                "_" +
                                                                                fila.id_reg +
                                                                                "'></i></td>";
                                                                            tabla += "</tr>";
                                                                            tabla +=
                                                                                "<tr><td id='ver_ter_" +
                                                                                tipo_alta +
                                                                                "_" +
                                                                                coloca +
                                                                                "_" +
                                                                                fila.id_reg +
                                                                                "' colspan='3' style='display:none'></td></tr>";
                                                                        });
                                                                        tabla += "</table>";
                                                                        tabla += "</div>";

                                                                        $(
                                                                            "#ver_reg_" + tipo_alta + "_" + coloca
                                                                        ).html(tabla);
                                                                        $(
                                                                            "#ver_reg_" + tipo_alta + "_" + coloca
                                                                        ).show();

                                                                        $(".det_ter").unbind("click");
                                                                        $(".det_ter").click(function() {
                                                                            var tipo_alta = $(this)
                                                                                .parents("tr.dad4")
                                                                                .attr("data-tipo_alta");
                                                                            var coloca = $(this)
                                                                                .parents("tr.dad4")
                                                                                .attr("data-coloca");
                                                                            var id_reg = $(this)
                                                                                .parents("tr.dad4")
                                                                                .attr("data-id_reg");

                                                                            var elemento = $(
                                                                                "#det_ter_" +
                                                                                tipo_alta +
                                                                                "_" +
                                                                                coloca +
                                                                                "_" +
                                                                                id_reg
                                                                            );

                                                                            if (elemento.hasClass("det_ter")) {
                                                                                elemento.attr(
                                                                                    "class",
                                                                                    "glyphicon glyphicon-minus-sign text-primary CloseDetaTer"
                                                                                );

                                                                                $.post(
                                                                                    "modulos/reporte_activacionespdv_v1.0.0/controlador.php", {
                                                                                        accion: "datosTerri",
                                                                                        fecha_ini: fecha_ini,
																						fecha_fin: fecha_fin,
                                                                                        tipo: tipo_alta,
                                                                                        coloca: coloca,
                                                                                        id_reg: id_reg
                                                                                    },
                                                                                    function(data, textStatus) {
                                                                                        var reporte = "";
                                                                                        var reporte_sin = "";
                                                                                        data = JSON.parse(data);
                                                                                        if (!$.isEmptyObject(data)) {
                                                                                            tabla =
                                                                                                "<div class='col-md-12' style='border:2px #ccc solid; border-radius:2px;'>";
                                                                                            tabla +=
                                                                                                '<table class="table table-striped dt-responsive nowrap" cellspacing="0" width="100%">';
                                                                                            tabla += $("#t_altas_ter").html();
                                                                                            $.each(data, function(
                                                                                                index,
                                                                                                fila
                                                                                            ) {
                                                                                                tabla +=
                                                                                                    "<tr class='dad5' data-tipo_alta = '" +
                                                                                                    tipo_alta +
                                                                                                    "' data-coloca = '" +
                                                                                                    coloca +
                                                                                                    "' data-id_reg = '" +
                                                                                                    id_reg +
                                                                                                    "' data-id_terri = '" +
                                                                                                    fila.territorio +
                                                                                                    "'>";
                                                                                                tabla +=
                                                                                                    "<td style='text-align: center;'>" +
                                                                                                    fila.nom_terri +
                                                                                                    "</td>";
                                                                                                tabla +=
                                                                                                    "<td style='text-align: center;'>" +
                                                                                                    addCommas(fila.cant_act) +
                                                                                                    "</td>";
                                                                                                tabla +=
                                                                                                    "<td style='text-align: center;'><i style='cursor:pointer; font-size:19px;' class='glyphicon glyphicon-plus-sign text-primary det_ruta' id='det_ruta_" +
                                                                                                    tipo_alta +
                                                                                                    "_" +
                                                                                                    coloca +
                                                                                                    "_" +
                                                                                                    id_reg +
                                                                                                    "_" +
                                                                                                    fila.territorio +
                                                                                                    "'></i></td>";
                                                                                                tabla += "</tr>";
                                                                                                tabla +=
                                                                                                    "<tr><td id='ver_ruta_" +
                                                                                                    tipo_alta +
                                                                                                    "_" +
                                                                                                    coloca +
                                                                                                    "_" +
                                                                                                    id_reg +
                                                                                                    "_" +
                                                                                                    fila.territorio +
                                                                                                    "' colspan='3' style='display:none'></td></tr>";
                                                                                            });
                                                                                            tabla += "</table>";
                                                                                            tabla += "</div>";

                                                                                            $(
                                                                                                "#ver_ter_" +
                                                                                                tipo_alta +
                                                                                                "_" +
                                                                                                coloca +
                                                                                                "_" +
                                                                                                id_reg
                                                                                            ).html(tabla);
                                                                                            $(
                                                                                                "#ver_ter_" +
                                                                                                tipo_alta +
                                                                                                "_" +
                                                                                                coloca +
                                                                                                "_" +
                                                                                                id_reg
                                                                                            ).show();

                                                                                            $(".det_ruta").unbind("click");
                                                                                            $(".det_ruta").click(function() {
                                                                                                var tipo_alta = $(this)
                                                                                                    .parents("tr.dad5")
                                                                                                    .attr("data-tipo_alta");
                                                                                                var coloca = $(this)
                                                                                                    .parents("tr.dad5")
                                                                                                    .attr("data-coloca");
                                                                                                var id_reg = $(this)
                                                                                                    .parents("tr.dad5")
                                                                                                    .attr("data-id_reg");
                                                                                                var id_terri = $(this)
                                                                                                    .parents("tr.dad5")
                                                                                                    .attr("data-id_terri");

                                                                                                var elemento = $(
                                                                                                    "#det_ruta_" +
                                                                                                    tipo_alta +
                                                                                                    "_" +
                                                                                                    coloca +
                                                                                                    "_" +
                                                                                                    id_reg +
                                                                                                    "_" +
                                                                                                    id_terri
                                                                                                );

                                                                                                if (
                                                                                                    elemento.hasClass("det_ruta")
                                                                                                ) {
                                                                                                    elemento.attr(
                                                                                                        "class",
                                                                                                        "glyphicon glyphicon-minus-sign text-primary CloseDetaRu"
                                                                                                    );

                                                                                                    $.post(
                                                                                                        "modulos/reporte_activacionespdv_v1.0.0/controlador.php", {
                                                                                                            accion: "datosRuta",
                                                                                                            fecha_ini: fecha_ini,
																											fecha_fin: fecha_fin,
																											tipo: tipo_alta,
                                                                                                            coloca: coloca,
                                                                                                            id_reg: id_reg,
                                                                                                            id_terri: id_terri
                                                                                                        },
                                                                                                        function(data, textStatus) {
                                                                                                            var reporte = "";
                                                                                                            var reporte_sin = "";
                                                                                                            data = JSON.parse(data);
                                                                                                            if (!$.isEmptyObject(data)) {
                                                                                                                tabla =
                                                                                                                    "<div class='col-md-12' style='border:2px #ccc solid; border-radius:2px;'>";
                                                                                                                tabla +=
                                                                                                                    '<table class="table table-striped dt-responsive nowrap" cellspacing="0" width="100%">';
                                                                                                                tabla += $(
                                                                                                                    "#t_altas_ruta"
                                                                                                                ).html();
                                                                                                                $.each(data, function(
                                                                                                                    index,
                                                                                                                    fila
                                                                                                                ) {
                                                                                                                    tabla += "<tr>";
                                                                                                                    tabla +=
                                                                                                                        "<td style='text-align: center;'>" +
                                                                                                                        fila.nom_ruta +
                                                                                                                        "</td>";
                                                                                                                    tabla +=
                                                                                                                        "<td style='text-align: center;'>" +
                                                                                                                        addCommas(
                                                                                                                            fila.cant_act
                                                                                                                        ) +
                                                                                                                        "</td>";
                                                                                                                    tabla += "</tr>";
                                                                                                                });
                                                                                                                tabla += "</table>";
                                                                                                                tabla += "</div>";

                                                                                                                $(
                                                                                                                    "#ver_ruta_" +
                                                                                                                    tipo_alta +
                                                                                                                    "_" +
                                                                                                                    coloca +
                                                                                                                    "_" +
                                                                                                                    id_reg +
                                                                                                                    "_" +
                                                                                                                    id_terri
                                                                                                                ).html(tabla);
                                                                                                                $(
                                                                                                                    "#ver_ruta_" +
                                                                                                                    tipo_alta +
                                                                                                                    "_" +
                                                                                                                    coloca +
                                                                                                                    "_" +
                                                                                                                    id_reg +
                                                                                                                    "_" +
                                                                                                                    id_terri
                                                                                                                ).show();
                                                                                                            }
                                                                                                        }
                                                                                                    );
                                                                                                } else {
                                                                                                    elemento.attr(
                                                                                                        "class",
                                                                                                        "glyphicon glyphicon-plus-sign text-primary det_ruta"
                                                                                                    );
                                                                                                    $(
                                                                                                        "#ver_ruta_" +
                                                                                                        tipo_alta +
                                                                                                        "_" +
                                                                                                        coloca +
                                                                                                        "_" +
                                                                                                        id_reg +
                                                                                                        "_" +
                                                                                                        id_terri
                                                                                                    ).hide();
                                                                                                }
                                                                                            });
                                                                                        }
                                                                                    }
                                                                                );
                                                                            } else {
                                                                                elemento.attr(
                                                                                    "class",
                                                                                    "glyphicon glyphicon-plus-sign text-primary det_ter"
                                                                                );
                                                                                $(
                                                                                    "#ver_ter_" +
                                                                                    tipo_alta +
                                                                                    "_" +
                                                                                    coloca +
                                                                                    "_" +
                                                                                    id_reg
                                                                                ).hide();
                                                                            }
                                                                        });
                                                                    }
                                                                }
                                                            );
                                                        } else {
                                                            elemento.attr(
                                                                "class",
                                                                "glyphicon glyphicon-plus-sign text-primary det_reg"
                                                            );
                                                            $("#ver_reg_" + tipo_alta + "_" + coloca).hide();
                                                        }
                                                    }
                                                });
                                            }
                                        }
                                    );
                                } else {
                                    elemento.attr(
                                        "class",
                                        "glyphicon glyphicon-plus-sign text-primary det_coloca"
                                    );
                                    $("#ver_coloca_" + tipo_alta).hide();
                                }
                            });
                        } else {
                            elemento.attr(
                                "class",
                                "glyphicon glyphicon-plus-sign text-primary det_altas"
                            );
                            $("#ver_altas").hide();
                        }
                    });
                } else {
                    Notificacion(data.mns, "warning");
                    $("#consulta").hide();
                }
            }
        );
    }
}

function addCommas(nStr) {
    nStr += "";
    x = nStr.split(".");
    x1 = x[0];
    x2 = x.length > 1 ? "." + x[1] : "";
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, "$1" + "," + "$2");
    }
    return x1 + x2;
}