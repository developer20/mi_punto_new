<?php
include_once("../../config/mainController.php"); // Incluye el Controlador Principal
include_once("../../modulos/reporte_activacionespdv_v1.0.0/modelo.php");	// Incluye el Modelo.
$controller = new mainController; // Instancia a la clase MainController
$modelo = new reporte_altas_combos(); // Instancia a la clase del modelo
try // Try, manejo de Errores
{
	$metodo = $_SERVER['REQUEST_METHOD'];
	$tipo_res = "";
	$response = null; 
	// Se manejaran dos tipos JSON y HTML
	// Dependiendo del método de la petición ejecutaremos la acción correspondiente.
	// Por ahora solo POST, todas las llamadas se haran por POST
    $variables = $_POST;
	if(!isset($_POST['accion'])){echo "0"; return;} // Evita que ocurra un error si no manda accion.
	$accion = $variables['accion'];
		// Dependiendo de la accion se ejecutaran las tareas y se definira el tipo de respuesta.
		switch($accion) {

			case 'datosAltas':
				 $tipo_res = 'JSON'; //Definir tipo de respuesta;
				 $fecha_ini = $variables['fecha_ini'];
				 $fecha_fin = $variables['fecha_fin'];
				 $op = $variables['op'];

				 $response = $modelo->Retornar_Altas($fecha_ini,$fecha_fin,$op);
	
			break;
			case 'datosColocacion':
				 $tipo_res = 'JSON'; //Definir tipo de respuesta;
				 $fecha_ini = $variables['fecha_ini'];
				 $fecha_fin = $variables['fecha_fin'];
				 $tipo = $variables['tipo'];

				 $response = $modelo->Retornar_Coloca($fecha_ini,$fecha_fin,$tipo);
	
			break;
			case 'datosDistri':
				 $tipo_res = 'JSON'; //Definir tipo de respuesta;
				 $fecha_ini = $variables['fecha_ini'];
				 $fecha_fin = $variables['fecha_fin'];
				 $depa = $variables['depa'];
				 $pro = $variables['pro'];
				 $dis = $variables['dis'];
				 $regional = $variables['regional'];
				 $idCircuito = $variables['idCircuito'];
				 $idRuta = $variables['idRuta'];
				 $pos = $variables['pos'];
				 $tipo = $variables['tipo'];
				 $coloca = $variables['coloca'];

				 $response = $modelo->Retornar_Distri($fecha_ini,$fecha_fin,$depa,$pro,$dis,$regional,$idCircuito,$idRuta,$pos,$tipo,$coloca);
	
			break;
			case 'datosReg':
				 $tipo_res = 'JSON'; //Definir tipo de respuesta;
				 $fecha_ini = $variables['fecha_ini'];
				 $fecha_fin = $variables['fecha_fin'];
				 $tipo = $variables['tipo'];
				 $coloca = $variables['coloca'];

				 $response = $modelo->Retornar_Reg($fecha_ini,$fecha_fin,$tipo,$coloca);
	
			break;
			case 'datosTerri':
				 $tipo_res = 'JSON'; //Definir tipo de respuesta;
				 $fecha_ini = $variables['fecha_ini'];
				 $fecha_fin = $variables['fecha_fin'];
				 $tipo = $variables['tipo'];
				 $coloca = $variables['coloca'];
				 $id_reg = $variables['id_reg'];
				 $response = $modelo->Retornar_Terri($fecha_ini,$fecha_fin,$tipo,$coloca,$id_reg);
	
			break;
			case 'datosRuta':
				 $tipo_res = 'JSON'; //Definir tipo de respuesta;
				 $fecha_ini = $variables['fecha_ini'];
				 $fecha_fin = $variables['fecha_fin'];
				 $tipo = $variables['tipo'];
				 $coloca = $variables['coloca'];
				 $id_reg = $variables['id_reg'];
				 $id_terri = $variables['id_terri'];

				 $response = $modelo->Retornar_Ruta($fecha_ini,$fecha_fin,$tipo,$coloca,$id_reg,$id_terri);
	
			break;
		}

	// Respuestas del Controlador
	if($tipo_res == "JSON")
	{
	  echo json_encode($response,true); // $response será un array con los datos de nuestra respuesta.
	}
	elseif ($tipo_res == "HTML") {
	  echo $response; // $response será un html con el string de nuestra respuesta.
	}
} // Fin Try
catch (Exception $e) {}
