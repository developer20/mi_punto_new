<?php

ini_set('memory_limit', '3000M');
set_time_limit(3000);

if (!isset($_SESSION)) {
	session_start();
}
require_once "../../config/Session.php";
require_once "../../config/mainModel.php";


$BD = new BD();
$BD->conectar();
$oSession = unserialize($_SESSION[$GLOBALS["SESION_POS"]]);
$distri = $oSession->VSid_distri;
$punto  =  $oSession->VSidpos;

$sql_dis = "SELECT nombre_corto FROM {$GLOBALS["BD_NAME"]}.distribuidores WHERE id = $distri";
$query_dis = $BD->devolver_array($sql_dis);
$nombre_corto = $query_dis[0]["nombre_corto"];
$bd = "{$GLOBALS["BD_DIS"]}" . $nombre_corto . "_" . $distri;

/*$oSession = unserialize($_SESSION[$GLOBALS["SESION_DIS"]]);
$id_usu   = $oSession->VSid;
$distri   = $oSession->VSidDistri;
*/

//$permisos_zona = $BD->Permisos_Dcs_Zonas();

$variables = $_GET;

$fecha_ini = $variables['fecha_ini'];
$fecha_fin = $variables['fecha_fin'];
$cont1 = '="';
$cont2 = '"';

$sql_ref       = "(SELECT producto FROM {$GLOBALS["BD_NAME"]}.referencias WHERE id = pro.id_referencia)";
$sql_ref_combo = "(SELECT referencias.producto FROM {$GLOBALS["BD_NAME"]}.referencias,{$GLOBALS["BD_NAME"]}.simcards WHERE referencias.id = simcards.id_referencia AND simcards.id_combo = pro.id_combo)";

$sql = "SELECT
 pro.fecha_ac,
 CASE WHEN pro.fecha_venta IS NULL THEN 'N/A' WHEN pro.fecha_venta IS NOT NULL THEN pro.fecha_venta END as fecha_venta,
 IF(pro.tipo_alta = 1,'Simcard',IF(pro.tipo_alta = 2,'Combo',IF(pro.tipo_alta = 3,'Descombotizado',''))) AS tipo_alta,
 IF(pro.tipo_alta = 1 && pro.id_pos > 0,'Con colocacion',
 IF(pro.tipo_alta = 2 && pro.id_pos > 0,'Con colocacion',
 IF(pro.tipo_alta = 3 && pro.id_pos > 0,'Con colocacion','Sin colocacion'))) colocacion,
 IF(pro.tipo_alta = 2 || pro.tipo_alta = 3,$sql_ref,'N/A') AS nom_sku_pro,
 IF(pro.tipo_alta = 2 || pro.tipo_alta = 3,$sql_ref_combo,IF(pro.tipo_alta = 1,$sql_ref,'N/A')) AS nom_sku_sim,
 IF(pro.tipo_alta = 2 || pro.tipo_alta = 3,CONCAT('$cont1','',pro.imei,'$cont2'),'N/A') AS imei,
 IF(pro.tipo_alta = 1 || pro.tipo_alta = 2 || pro.tipo_alta = 3,CONCAT('$cont1','',pro.iccid,'$cont2'),'N/A') AS iccid,pro.movil AS movil,
 IF(usu.nombre IS NULL,'N/A',CONCAT(usu.nombre,' ',usu.apellido)) as nombre_usuario,
 CASE WHEN pos.idpos IS NULL THEN 'N/A' WHEN pos.idpos IS NOT NULL THEN pos.idpos END as id_punto,
 CASE WHEN pos.razon IS NULL THEN 'N/A' WHEN pos.razon IS NOT NULL THEN pos.razon END as razon,
 CASE WHEN pos.cod_cum IS NULL THEN 'N/A' WHEN pos.cod_cum IS NOT NULL THEN pos.cod_cum END as cod_cum,
 CASE WHEN reg.nombre IS NULL THEN 'N/A' WHEN reg.nombre IS NOT NULL THEN reg.nombre END as nom_reg,
 CASE WHEN ter.nombre IS NULL THEN 'N/A' WHEN ter.nombre IS NOT NULL THEN ter.nombre END as nom_ter,
 CASE WHEN zonas.nombre IS NULL THEN 'N/A' WHEN zonas.nombre IS NOT NULL THEN zonas.nombre END as nom_zona,
 CASE WHEN de.nombre IS NULL THEN 'N/A' WHEN de.nombre IS NOT NULL THEN de.nombre END as nom_depa,
 CASE WHEN mun.nombre IS NULL THEN 'N/A' WHEN mun.nombre IS NOT NULL THEN mun.nombre END as nom_provi,
 CASE WHEN d.nombre IS NULL THEN 'N/A' WHEN d.nombre IS NOT NULL THEN d.nombre END as distribuidor,
 pro.tipo_alta tipo,
 pro.distri,
 pro.regional,
 pro.territorio,
 pro.zona,
 pro.id_pos,
 pro.depto,
 pro.ciudad
 FROM
 (SELECT fecha_ac,fecha_venta,id,iccid,movil,distri,regional,territorio,zona,id_pos,depto,ciudad,id_referencia,id_vendedor,1 tipo_alta,'' imei,0 id_combo
 FROM {$GLOBALS["BD_NAME"]}.simcards
 WHERE activo = 1 AND fecha_ac >= '$fecha_ini' AND fecha_ac <= '$fecha_fin'
 ) pro
 LEFT JOIN $bd.usuarios usu ON usu.id = pro.id_vendedor
 LEFT JOIN {$GLOBALS["BD_POS"]}.puntos pos ON pos.idpos = pro.id_pos
 LEFT JOIN {$GLOBALS["BD_NAME"]}.distribuidores d ON pro.distri = d.id
 LEFT JOIN {$GLOBALS["BD_NAME"]}.regional reg ON reg.id = pro.regional
 LEFT JOIN {$GLOBALS["BD_NAME"]}.territorios ter ON ter.id = pro.territorio
 LEFT JOIN {$GLOBALS["BD_NAME"]}.zonas ON zonas.id = pro.zona
 LEFT JOIN {$GLOBALS["BD_NAME"]}.departamentos de ON pro.depto = de.id
 LEFT JOIN {$GLOBALS["BD_NAME"]}.municipios mun ON pro.ciudad = mun.id and mun.departamento = de.id
 WHERE pro.tipo_alta > 0 AND pro.id_pos = $punto ";

 //AND pro.distri = $distri

// ********** Une las consultas anteriores en una sola para organizar la información **********

/*
$sql = "SELECT
fecha_ac,fecha_venta,tipo_alta,colocacion,nom_sku_pro,nom_sku_sim,imei,iccid,nom_dis,nombre_usuario,id_punto,razon,nom_reg,nom_ter,nom_zona,nom_depa,nom_provi,nom_distrito,tipo,regional,territorio,zona,id_pos,depto,ciudad,distrito
FROM
(".$sql1.") pro
WHERE tipo > 0";*/

//if ($permisos_zona != "") {
//	$sql .= " AND pro.zona in ($permisos_zona,0)";
//}

// ********** Filtros de las consultas anteriores para el CSV **********

$sql .= " ORDER BY colocacion";

$resultado = $BD->devolver_array($sql);

if(count($resultado)>0){

  $nombre_archivo = "Reporte_Activaciones.csv";
  $encabezados = "FECHA ALTA;FECHA VENTA;COLOCACION;REFERENCIA EQUIPO;REFERENCIA SIMCARD;IMEI;ICCID;NOMBRE DISTRIBUIDOR;VENDEDOR;ID PUNTO;NOMBRE PUNTO;REGIONAL;CIRCUITO;RUTA;DEPARTAMENTO;CIUDAD;MOVIL";
  $llaves = array("fecha_ac","fecha_venta","colocacion","nom_sku_pro","nom_sku_sim","imei","iccid","distribuidor","nombre_usuario","id_punto","razon","nom_ter","nom_reg","nom_zona","nom_depa","nom_provi","movil");

  /*Se llama el metodo que comprime el archivo. Los argumentos son:
    1: nombre del archivo zip
    2: nombre del archivo que se va a comprimir
    3: Encabezado del archivo que se va a comprimir
    4: Datos que va conterner el archivo que se va a comprimir (es un arreglo)
    5: Llave de los datos
  */
  $BD->comprimir_archivo("Reporte_Activaciones",$nombre_archivo,$encabezados,$resultado,$llaves);
}

/*if ($BD->numreg($resultado) > 0) {

	header("Content-type:text/csv; charset=ISO-8859-1");
	header("Content-Disposition:  filename=\"Reporte_Activaciones.csv\";");

	echo $encabezados;

	while (!$resultado->EOF) {

		$razon = $resultado->fields["razon"];

		echo "\n".$resultado->fields['fecha_ac'].";".$resultado->fields['fecha_venta'].";".$resultado->fields['tipo_alta'].";".$resultado->fields['colocacion'].";".$resultado->fields['nom_sku_pro'].";".$resultado->fields['nom_sku_sim'].";\"".$resultado->fields['imei']."\";\"".$resultado->fields['iccid']."\";".$resultado->fields['movil'].";".$resultado->fields['nombre_usuario'].";".$resultado->fields['id_punto'].";".$razon.";".$resultado->fields['nom_reg'].";".$resultado->fields['nom_ter'].";".$resultado->fields['nom_zona'].";".$resultado->fields['nom_depa'].";".$resultado->fields['nom_provi'];

		$resultado->MoveNext();
	}
}*/

?>