var dtable = "";
var dtable2 = "";
$(document).ready(function() {
	$('.fecha').datepicker({
		format: 'yyyy-mm-dd',
		todayHighlight: true,
		todayBtn: true
	});

	$("#frmConsulta").submit(function(event) {
	 	event.preventDefault();
	 	Cargar_Reporte();
		Cargar_Reporte_meses();
		cargar_reporte_grafico();
    });
	
	$("#CSV").click(function() {
		Datos_CSV();
	});
});

function Cargar_Reporte(){
	
	
   if ( ! $.fn.DataTable.isDataTable( '#t_reporte' ) ) {
	  dtable = $("#t_reporte").DataTable({
			"ajax": {
			"url": "modulos/reporte_consumos/controlador.php",
			"type": "POST",
			"deferRender": false,
			"data":{accion:'Cargar_Informe_actual'}
		  },
		  "bFilter": false,
		  "dom": '<"top"fl>rt<"bottom"<"toolbar">p><"clear">',
		  "responsive":true,
		  "columns": [
			{ "data": "cant_acti"},
			{ "data": "cant_recar_act"},
			{ "data": "cant_recar"},
			{ "data": "total_recar"}
		],
		 "columnDefs": [
			{
				"targets": 0,
				"data": "cant_acti",
				 render: function ( data, type, row ) {
					
					if(row.ultima_fecha_act != "" || row.ultima_fecha_recar != ""){
					 
				    	$("#fecha_movi").html("<div style='color: #333333;font-size: 18px;fill: #333333;'>Ultimas fechas movimiento </div><div>Fecha activación: "+row.ultima_fecha_act+"<br> Fecha recarga: "+row.ultima_fecha_recar+"</div>");
					
					}
					 
					return row.cant_acti;
				 }
				 
			}
			
		 ],
	  });
	  $("#consulta").show();
	}else{
		dtable.destroy();
		Cargar_Reporte();
	}
		
}

function Cargar_Reporte_meses(){
	
	if ( ! $.fn.DataTable.isDataTable( '#t_reporte2' ) ) {
	  dtable2 = $("#t_reporte2").DataTable({
			"ajax": {
			"url": "modulos/reporte_consumos/controlador.php",
			"type": "POST",
			"deferRender": false,
			"data":{accion:'Cargar_Informe_meses'}
		  },
		  "bFilter": false,
		  "dom": '<"top"fl>rt<"bottom"<"toolbar">p><"clear">',
		  "responsive":true,
		  "columns": [
			{ "data": "cant_acti"},
			{ "data": "cant_recar_act"},
			{ "data": "cant_recar"},
			{ "data": "total_recar"}
		]
	  });
	  $("#consulta2").show();
	}else{
		dtable2.destroy();
		Cargar_Reporte_meses();
	}
		
}

function cargar_reporte_grafico() {
	
	var suma = 0;
	
	$.post('modulos/reporte_consumos/controlador.php',
	{
		accion: 'Recargas_Grafico_Mes'
	},
	function(datav, textStatus) {
		
		datav = JSON.parse(datav);
		var datos_acti = datav['array_acti'];
		var datos_recar = datav['array_recar'];
		var rango_fecha = datav['rango_fecha'];
		
		var Lineas = []; var Recargas = []; var valor = []; var valor_act = []; var valor_recar = []; var Meses = []; var Meses_rango = [];
		
		if(datos_acti.length > 0 || datos_recar.length > 0)
		{
			$.each(datos_acti,function(index,row){
			   Lineas.push({"y":parseInt(row.cant_acti)});
			   Recargas.push({"y":parseInt(row.cant_recar)});
			   Meses.push(row.mes_venta);
			});
			
			/*$.each(datos_recar,function(index,row){
			  
			   Meses.push(row.mes_venta);
			});*/
			
			
		  	///// graficar
		 
			$('#recargas').highcharts({
				chart: { zoomType: 'xy',height: 600 },
				title: { text: 'Recargas por Meses' },
				subtitle: { text: 'Rango de Fechas '+rango_fecha },
				xAxis: [{ categories: Meses, crosshair: false }],
				yAxis: [ {  title: { text: 'Cantidad' }, labels: { format: '{value}', style: { color: Highcharts.getOptions().colors[4] } }, opposite: true
				}, { title: { text: 'Cantidad' },labels: { format: '{value}k'} }],
				tooltip: { shared: true },
				scrollbar: { enabled: true },
				plotOptions: {},
				series: [
				{ name: 'Cantidad Lineas Activas', type: 'column', yAxis: 1, data: Lineas },
				{ name: 'Cantidad Lineas Recargadas', type: 'column', yAxis: 1, data: Recargas }
				],
				scrollbar: { enabled:true, barBackgroundColor: 'gray', barBorderRadius: 7, barBorderWidth: 0, buttonBackgroundColor: 'gray', buttonBorderWidth: 0, buttonArrowColor: 'yellow', buttonBorderRadius: 7, rifleColor: 'yellow', trackBackgroundColor: 'white', trackBorderWidth: 1, trackBorderColor: 'silver', trackBorderRadius: 7}
			});
	   //fin graficar
	   }
	   else
	   {
		   Notificacion("No se encontraron datos","warning");
	   }	
	}
	);
	
	$("#pie").show();
	$("#recargas").show();
}

function Datos_CSV(){
	
	var f_ini = $("#f_ini").val();
	var f_fin = $("#f_fin").val();
	
	$.post('modulos/reporte_consumos/controlador.php',
	{
		accion: 'Datos_CSV',
		f_ini: f_ini,
		f_fin: f_fin
	},
	function(data, textStatus) {
		if(data != ""){
			data = JSON.parse(data);
			
		    var header = "Serial Recargado,Movil Recargado,Fecha Recarga,Total,Distribuidor,Asesor,Fecha de Activacion,Hora de Activacion";
			var columnas = "serial,movil,FEC_RECARGA,total_recar,distribuidor,asesor,fecha_act,hora_act";
			ExportarCSV(data, header,"Reporte de Consumos",columnas);
			 
		}
	});
}