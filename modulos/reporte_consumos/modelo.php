<?php
require("../../config/mainModel.php");
class reporte_consumos{

   public function Cargar_Informe_actual()
   {
	    $oSession = unserialize($_SESSION[$GLOBALS["SESION_POS"]]);
	    $idpos=$oSession->VSidpos;
		
	    $array_response = array();
		
	    $BD=new BD();
	    $BD->conectar();
		
		$fecha_act = date("Y-m-d");
		
		$fecha_act = strtotime ( '-1 day' , strtotime ( $fecha_act ) ) ;
		$fecha_act = date ( 'Y-m-d' , $fecha_act );
		
		$month = date('m');
        $year = date('Y');
        $fecha_ini = date('Y-m-d', mktime(0,0,0, $month, 1, $year));
		
		//Cantidad Activaciones
		
		$sql = "SELECT count(acti.id) as cant_acti
		
		FROM {$GLOBALS["BD_NAME"]}.csv_activaciones acti, {$GLOBALS["BD_NAME"]}.simcards sim
		
		WHERE acti.NUMERO_ICC = sim.serie and acti.FECHA_VENTA >= '$fecha_ini' and acti.FECHA_VENTA <= '$fecha_act' and sim.id_pos = $idpos";
		
		$res_act = $BD->consultar($sql);
		
		//Cantidad kit activos
		
		$sql = "SELECT count(acti.id) as cant_acti
		
		FROM {$GLOBALS["BD_NAME"]}.csv_activaciones acti, {$GLOBALS["BD_NAME"]}.producto pro
		
		WHERE acti.NUMERO_ICC = pro.serie and pro.serie>0 and acti.FECHA_VENTA >= '$fecha_ini' and acti.FECHA_VENTA <= '$fecha_act' and pro.id_pos = $idpos";
		
		$res_kit_act = $BD->consultar($sql);
		
		//Total Cantidad recargas activas			
	    
		$sql = "SELECT count(recar.id) as cant_recar,sum(VAL_RECARGA) as total_recar
		
		FROM {$GLOBALS["BD_NAME"]}.csv_activaciones acti, {$GLOBALS["BD_NAME"]}.csv_recarga recar, {$GLOBALS["BD_NAME"]}.simcards sim
		
		WHERE acti.NUMERO_ICC = recar.NUM_ICC and acti.NUMERO_ICC = sim.serie and acti.FECHA_VENTA >= '$fecha_ini' and acti.FECHA_VENTA <= '$fecha_act' and recar.FEC_RECARGA >= '$fecha_ini' and recar.FEC_RECARGA <= '$fecha_act' and sim.id_pos = $idpos";
		
		$res_recar_act = $BD->consultar($sql);
		
		//Total Cantidad recargas kit activas			
	    
		$sql = "SELECT count(recar.id) as cant_recar,sum(VAL_RECARGA) as total_recar
		
		FROM {$GLOBALS["BD_NAME"]}.csv_activaciones acti, {$GLOBALS["BD_NAME"]}.csv_recarga recar, {$GLOBALS["BD_NAME"]}.producto pro
		
		WHERE acti.NUMERO_ICC = recar.NUM_ICC and acti.NUMERO_ICC = pro.serie and pro.serie>0 and acti.FECHA_VENTA >= '$fecha_ini' and acti.FECHA_VENTA <= '$fecha_act' and pro.id_pos = $idpos";
		
		$res_recar_kit_act = $BD->consultar($sql);
		
		//Cantidad recargas por iccid	
		
		$sql = "SELECT count(distinct recar.NUM_ICC) as cant_recar
		
		FROM {$GLOBALS["BD_NAME"]}.csv_activaciones acti, {$GLOBALS["BD_NAME"]}.csv_recarga recar, {$GLOBALS["BD_NAME"]}.simcards sim
		
		WHERE acti.NUMERO_ICC = recar.NUM_ICC and acti.NUMERO_ICC = sim.serie and acti.FECHA_VENTA >= '$fecha_ini' and acti.FECHA_VENTA <= '$fecha_act' and acti.FECHA_VENTA >= '$fecha_ini' and acti.FECHA_VENTA <= '$fecha_act' and sim.id_pos = $idpos";
		
		$res_recar = $BD->consultar($sql);
		
		//Cantidad recargas kit por iccid	
		
		$sql = "SELECT count(distinct recar.NUM_ICC) as cant_recar
		
		FROM {$GLOBALS["BD_NAME"]}.csv_activaciones acti, {$GLOBALS["BD_NAME"]}.csv_recarga recar, {$GLOBALS["BD_NAME"]}.producto pro
		
		WHERE acti.NUMERO_ICC = recar.NUM_ICC and acti.NUMERO_ICC = pro.serie and pro.serie>0 and acti.FECHA_VENTA >= '$fecha_ini' and acti.FECHA_VENTA <= '$fecha_act' and acti.FECHA_VENTA >= '$fecha_ini' and acti.FECHA_VENTA <= '$fecha_act' and pro.id_pos = $idpos";
		
		$res_recar_kit = $BD->consultar($sql);
	
		
		if($BD->numreg($res_act)>0 || $BD->numreg($res_kit_act)>0){
			
			$total_recar = 0;
			$cant_acti = 0;
			$cant_recar_act = 0;
			$cant_recar = 0;
			$ultima_fecha_acti = "";
			$ultima_fecha_recar = "";
			
			if($res_recar_act->fields['total_recar'] != "")
				$total_recar = number_format($res_recar_act->fields['total_recar']+$res_recar_kit_act->fields['total_recar'],0);	
			
			$cant_acti = $res_act->fields['cant_acti'] + $res_kit_act->fields['cant_acti'];
			$cant_recar_act = $res_recar->fields['cant_recar'] + $res_recar_kit->fields['cant_recar'];
			$cant_recar = $res_recar_act->fields['cant_recar'] + $res_recar_kit_act->fields['cant_recar'];
			
			$return_fechas = $this->ultima_fecha_actualizada($fecha_ini,$fecha_act);
			
			$return_fechas = explode("|",$return_fechas);
			
			$ultima_fecha_acti = $return_fechas[0];
			$ultima_fecha_recar = $return_fechas[1];
			
			$array_response[] = array("cant_acti" => number_format($cant_acti,0), "cant_recar_act" => number_format($cant_recar_act,0), "cant_recar" => number_format($cant_recar,0), "total_recar" => $total_recar, "comi" => 0, "ultima_fecha_act" => $ultima_fecha_acti, "ultima_fecha_recar" => $ultima_fecha_recar);
			
		}
			
		return $array_response;
		
   }
   
   public function ultima_fecha_actualizada($fecha_ini,$fecha_act)
   {
	   
	    $oSession = unserialize($_SESSION[$GLOBALS["SESION_POS"]]);
	    $idpos=$oSession->VSidpos;
		
		$BD=new BD();
	    $BD->conectar();
	   
		//Ultima activacion sim
		
		$sql = "SELECT acti.FECHA_VENTA
	
		FROM {$GLOBALS["BD_NAME"]}.csv_activaciones acti, {$GLOBALS["BD_NAME"]}.simcards sim
		
		WHERE acti.NUMERO_ICC = sim.serie and acti.FECHA_VENTA >= '$fecha_ini' and acti.FECHA_VENTA <= '$fecha_act' and sim.id_pos = $idpos order by acti.id desc limit 1";
		
		$res_act_sim = $BD->consultar($sql);
		
		
		//Ultima recarga sim
		
		$sql = "SELECT recar.FEC_RECARGA
	
		FROM {$GLOBALS["BD_NAME"]}.csv_activaciones acti, {$GLOBALS["BD_NAME"]}.csv_recarga recar, {$GLOBALS["BD_NAME"]}.simcards sim
		
		WHERE acti.NUMERO_ICC = recar.NUM_ICC and acti.NUMERO_ICC = sim.serie and recar.FEC_RECARGA >= '$fecha_ini' and recar.FEC_RECARGA <= '$fecha_act' and sim.id_pos = $idpos order by recar.id desc limit 1";
		
		$res_recar_act_sim = $BD->consultar($sql);
		
		
		//Ultima activacion kit
		
		$sql = "SELECT acti.FECHA_VENTA
	
		FROM {$GLOBALS["BD_NAME"]}.csv_activaciones acti, {$GLOBALS["BD_NAME"]}.producto pro
		
		WHERE acti.NUMERO_ICC = pro.serie and acti.FECHA_VENTA >= '$fecha_ini' and acti.FECHA_VENTA <= '$fecha_act' and pro.id_pos = $idpos order by acti.id desc limit 1";
		
		$res_act_kit = $BD->consultar($sql);
		
		
		//Ultima recarga kit
		
		$sql = "SELECT recar.FEC_RECARGA
	
		FROM {$GLOBALS["BD_NAME"]}.csv_activaciones acti, {$GLOBALS["BD_NAME"]}.csv_recarga recar, {$GLOBALS["BD_NAME"]}.producto pro
		
		WHERE acti.NUMERO_ICC = recar.NUM_ICC and acti.NUMERO_ICC = pro.serie and recar.FEC_RECARGA >= '$fecha_ini' and recar.FEC_RECARGA <= '$fecha_act' and pro.id_pos = $idpos order by recar.id desc limit 1";
		
		$res_recar_act_kit = $BD->consultar($sql);
		
		
		
		$ultima_fecha_act = "";
		$ultima_fecha_recar = "";
		
		
		if($BD->numreg($res_act_sim)>0 && $BD->numreg($res_act_kit)==0){
		
			$ultima_fecha_act = $res_act_sim->fields["FECHA_VENTA"];
		
		}else{
			
			$ultima_fecha_act = $res_act_kit->fields["FECHA_VENTA"];
				
		}
		
		
		if($BD->numreg($res_recar_act_sim)==0 && $BD->numreg($res_recar_act_kit)>0){
		
			$ultima_fecha_recar = $res_recar_act_sim->fields["FECHA_VENTA"];
		
		}else{
			
			$ultima_fecha_recar = $res_recar_act_kit->fields["FECHA_VENTA"];
				
		}
		
		if($res_act_sim->fields["FECHA_VENTA"] > $res_act_kit->fields["FECHA_VENTA"]){
			$ultima_fecha_act = $res_act_sim->fields["FECHA_VENTA"];
		}else{
			$ultima_fecha_act = $res_act_kit->fields["FECHA_VENTA"];
		}
		
		if($res_recar_act_sim->fields["FEC_RECARGA"] > $res_recar_act_kit->fields["FEC_RECARGA"]){
			$ultima_fecha_recar = $res_recar_act_sim->fields["FEC_RECARGA"];
		}else{
			$ultima_fecha_recar = $res_recar_act_kit->fields["FEC_RECARGA"];
		}
		
		return $ultima_fecha_act."|".$ultima_fecha_recar;
   }
   
   public function Cargar_Informe_meses()
   {
	    
		$oSession = unserialize($_SESSION[$GLOBALS["SESION_POS"]]);
	    $idpos=$oSession->VSidpos;
		
	    $array_response = array();
		
	    $BD=new BD();
	    $BD->conectar();
		
		$fecha_act = date("Y-m-d");
		
		$fecha_act = strtotime ( '-1 day' , strtotime ( $fecha_act ) ) ;
		$fecha_act = date ( 'Y-m-d' , $fecha_act );
		
		$fecha_ini = strtotime ( '-6 month' , strtotime ( $fecha_act ) ) ;
		$fecha_ini = date ( 'Y-m-d' , $fecha_ini );
		
		//Cantidad Activaciones
		
		$sql = "SELECT count(acti.id) as cant_acti
		
		FROM {$GLOBALS["BD_NAME"]}.csv_activaciones acti, {$GLOBALS["BD_NAME"]}.simcards sim
		
		WHERE acti.NUMERO_ICC = sim.serie and acti.FECHA_VENTA >= '$fecha_ini' and acti.FECHA_VENTA <= '$fecha_act' and sim.id_pos = $idpos";
		
		$res_act = $BD->consultar($sql);
		
		//Cantidad kit activos
		
		$sql = "SELECT count(acti.id) as cant_acti
		
		FROM {$GLOBALS["BD_NAME"]}.csv_activaciones acti, {$GLOBALS["BD_NAME"]}.producto pro
		
		WHERE acti.NUMERO_ICC = pro.serie and pro.serie>0 and acti.FECHA_VENTA >= '$fecha_ini' and acti.FECHA_VENTA <= '$fecha_act' and pro.id_pos = $idpos";
		
		$res_kit_act = $BD->consultar($sql);
		
		//Total Cantidad recargas activas			
	    
		$sql = "SELECT count(recar.id) as cant_recar,sum(VAL_RECARGA) as total_recar
		
		FROM {$GLOBALS["BD_NAME"]}.csv_activaciones acti, {$GLOBALS["BD_NAME"]}.csv_recarga recar, {$GLOBALS["BD_NAME"]}.simcards sim
		
		WHERE acti.NUMERO_ICC = recar.NUM_ICC and acti.NUMERO_ICC = sim.serie and acti.FECHA_VENTA >= '$fecha_ini' and acti.FECHA_VENTA <= '$fecha_act' and sim.id_pos = $idpos";
		
		$res_recar_act = $BD->consultar($sql);
		
		//Total Cantidad recargas kit activas			
	    
		$sql = "SELECT count(recar.id) as cant_recar,sum(VAL_RECARGA) as total_recar
		
		FROM {$GLOBALS["BD_NAME"]}.csv_activaciones acti, {$GLOBALS["BD_NAME"]}.csv_recarga recar, {$GLOBALS["BD_NAME"]}.producto pro
		
		WHERE acti.NUMERO_ICC = recar.NUM_ICC and acti.NUMERO_ICC = pro.serie and pro.serie>0 and acti.FECHA_VENTA >= '$fecha_ini' and acti.FECHA_VENTA <= '$fecha_act' and pro.id_pos = $idpos";
		
		$res_recar_kit_act = $BD->consultar($sql);
		
		//Cantidad recargas por iccid	
		
		$sql = "SELECT count(distinct recar.NUM_ICC) as cant_recar
		
		FROM {$GLOBALS["BD_NAME"]}.csv_activaciones acti, {$GLOBALS["BD_NAME"]}.csv_recarga recar, {$GLOBALS["BD_NAME"]}.simcards sim
		
		WHERE acti.NUMERO_ICC = recar.NUM_ICC and acti.NUMERO_ICC = sim.serie and acti.FECHA_VENTA >= '$fecha_ini' and acti.FECHA_VENTA <= '$fecha_act' and sim.id_pos = $idpos";
		
		$res_recar = $BD->consultar($sql);
		
		//Cantidad recargas kit por iccid	
		
		$sql = "SELECT count(distinct recar.NUM_ICC) as cant_recar
		
		FROM {$GLOBALS["BD_NAME"]}.csv_activaciones acti, {$GLOBALS["BD_NAME"]}.csv_recarga recar, {$GLOBALS["BD_NAME"]}.producto pro
		
		WHERE acti.NUMERO_ICC = recar.NUM_ICC and acti.NUMERO_ICC = pro.serie and pro.serie>0 and acti.FECHA_VENTA >= '$fecha_ini' and acti.FECHA_VENTA <= '$fecha_act' and pro.id_pos = $idpos";
		
		$res_recar_kit = $BD->consultar($sql);
		
		if($BD->numreg($res_act)>0 || $BD->numreg($res_kit_act)>0){
			
			$total_recar = 0;
			$cant_acti = 0;
			$cant_recar_act = 0;
			$cant_recar = 0;
			
			if($res_recar_act->fields['total_recar'] != "")
				$total_recar = number_format($res_recar_act->fields['total_recar'],0);	
			
			$cant_acti = $res_act->fields['cant_acti'] + $res_kit_act->fields['cant_acti'];
			$cant_recar_act = $res_recar->fields['cant_recar'] + $res_recar_kit->fields['cant_recar'];
			$cant_recar = $res_recar_act->fields['cant_recar'] + $res_recar_kit_act->fields['cant_recar'];
			
			$array_response[] = array("cant_acti" => number_format($cant_acti,0), "cant_recar_act" => number_format($cant_recar_act,0), "cant_recar" => number_format($cant_recar,0), "total_recar" => $total_recar, "comi" => 0);
			
		}
			
		return $array_response;
		
   }
   
   public function Recargas_Grafico_Mes()
   {
			   
		$oSession = unserialize($_SESSION[$GLOBALS["SESION_POS"]]);
		$idpos=$oSession->VSidpos;
		
		$BD=new BD();
	    $BD->conectar();
	    
		$array_response = array();
	    $array_acti= array();
		$cont_recar = array();
		
		$fecha_array = array();
		
		$fecha_act = date("Y-m-d");
		
		$fecha_act = strtotime ( '-1 day' , strtotime ( $fecha_act ) ) ;
		$fecha_act = date ( 'Y-m-d' , $fecha_act );
		
		$fecha_ini = strtotime ( '-6 month' , strtotime ( $fecha_act ) ) ;
		$fecha_ini = date ( 'Y-m-d' , $fecha_ini );
		$fecha_reco = $fecha_ini;
		
		$i = 0;
		$mes_primero = "";
		$anio_primero = "";
	    
		$sql_sim = "SELECT count(acti.id) as cant_acti,YEAR(acti.FECHA_VENTA) as anio,MONTH(acti.FECHA_VENTA) as mes
		
							FROM {$GLOBALS["BD_NAME"]}.csv_activaciones acti, {$GLOBALS["BD_NAME"]}.simcards sim
							
							WHERE acti.NUMERO_ICC = sim.serie and acti.FECHA_VENTA >= '$fecha_ini' and acti.FECHA_VENTA <= '$fecha_act' and sim.id_pos = $idpos
						
							GROUP BY YEAR(acti.FECHA_VENTA),MONTH(acti.FECHA_VENTA)";
		
		
	    $sql_kit = "SELECT count(acti.id) as cant_acti,YEAR(acti.FECHA_VENTA) as anio,MONTH(acti.FECHA_VENTA) as mes
		
							FROM {$GLOBALS["BD_NAME"]}.csv_activaciones acti, {$GLOBALS["BD_NAME"]}.producto pro
							
							WHERE acti.NUMERO_ICC = pro.serie and acti.FECHA_VENTA >= '$fecha_ini' and acti.FECHA_VENTA <= '$fecha_act' and pro.id_pos = $idpos
						
							GROUP BY YEAR(acti.FECHA_VENTA),MONTH(acti.FECHA_VENTA)";
		
		
		$consulta_act = $sql_sim." UNION ".$sql_kit;
		
		$res_act = $BD->consultar($consulta_act);
		
		while(!$res_act->EOF){
			
			$i++;
			
			$anio = $res_act->fields['anio'];
			$mes = $res_act->fields['mes'];
			
			
			if($i==1){
				
				$mes_primero .= $mes;
				$anio_primero .= $anio;
			}
			
			$cont_recar[$anio.$mes] = 0;
			
			if($anio_primero == $anio){
			
				$meses_concat = " (YEAR(acti.FECHA_VENTA) = '$anio' and (MONTH(acti.FECHA_VENTA) >= '$mes_primero' and MONTH(acti.FECHA_VENTA) <= '$mes'))";
			
			}else{
				
				$mes_des = 1; 
				$meses_concat = " (YEAR(acti.FECHA_VENTA) = '$anio' and (MONTH(acti.FECHA_VENTA) >= '$mes_des' and MONTH(acti.FECHA_VENTA) <= '$mes'))";	
			
			}	
			
			$sql_sim = "SELECT count(distinct recar.NUM_ICC) as cant_recar
		
			FROM {$GLOBALS["BD_NAME"]}.csv_activaciones acti, {$GLOBALS["BD_NAME"]}.csv_recarga recar, {$GLOBALS["BD_NAME"]}.simcards sim
			
			WHERE acti.NUMERO_ICC = recar.NUM_ICC and acti.NUMERO_ICC = sim.serie and ".$meses_concat." and YEAR(recar.FEC_RECARGA) = '$anio' and MONTH(recar.FEC_RECARGA) = '$mes' and sim.id_pos = $idpos";
			
			$sql_kit = "SELECT count(distinct recar.NUM_ICC) as cant_recar
		
			FROM {$GLOBALS["BD_NAME"]}.csv_activaciones acti, {$GLOBALS["BD_NAME"]}.csv_recarga recar, {$GLOBALS["BD_NAME"]}.producto pro
			
			WHERE acti.NUMERO_ICC = recar.NUM_ICC and acti.NUMERO_ICC = pro.serie and ".$meses_concat." and YEAR(recar.FEC_RECARGA) = '$anio' and MONTH(recar.FEC_RECARGA) = '$mes' and pro.id_pos = $idpos";
			
			$consulta_recar = $sql_sim." UNION ".$sql_kit;
			
			$res_recar = $BD->consultar($consulta_recar);
			
			while(!$res_recar->EOF){
				
				$cont_recar[$anio.$mes] += $res_recar->fields['cant_recar'];
				
				$res_recar->MoveNext();
			}
			
			if($anio_primero != $anio){
				
				$mes_ante = 12;
				
				$meses_concat = " (YEAR(acti.FECHA_VENTA) = '$anio_primero' and (MONTH(acti.FECHA_VENTA) >= '$mes_primero' and MONTH(acti.FECHA_VENTA) <= '$mes_ante'))";
				
				$sql_sim = "SELECT count(distinct recar.NUM_ICC) as cant_recar
		
				FROM {$GLOBALS["BD_NAME"]}.csv_activaciones acti, {$GLOBALS["BD_NAME"]}.csv_recarga recar, {$GLOBALS["BD_NAME"]}.simcards sim
				
				WHERE acti.NUMERO_ICC = recar.NUM_ICC and acti.NUMERO_ICC = sim.serie and ".$meses_concat." and YEAR(recar.FEC_RECARGA) = '$anio' and MONTH(recar.FEC_RECARGA) = '$mes' and sim.id_pos = $idpos";
				
				$sql_kit = "SELECT count(distinct recar.NUM_ICC) as cant_recar
			
				FROM {$GLOBALS["BD_NAME"]}.csv_activaciones acti, {$GLOBALS["BD_NAME"]}.csv_recarga recar, {$GLOBALS["BD_NAME"]}.producto pro
				
				WHERE acti.NUMERO_ICC = recar.NUM_ICC and acti.NUMERO_ICC = pro.serie and ".$meses_concat." and YEAR(recar.FEC_RECARGA) = '$anio' and MONTH(recar.FEC_RECARGA) = '$mes' and pro.id_pos = $idpos";
				
				$consulta_recar = $sql_sim." UNION ".$sql_kit;
				
				$res_recar = $BD->consultar($consulta_recar);
				
				while(!$res_recar->EOF){
				
					$cont_recar[$anio.$mes] += $res_recar->fields['cant_recar'];
					
					$res_recar->MoveNext();
				}	
				
			}
			
			$fecha_mes = $this->contver_fecha($mes,$anio);
			
			$array_acti[] = array("cant_acti" => $res_act->fields['cant_acti'], "cant_recar" => $cont_recar[$anio.$mes],"mes_venta" => $fecha_mes);
			
			$res_act->MoveNext();
			
		}
		
		$array_response = array("array_acti" => $array_acti, "rango_fecha" => $fecha_ini." hasta ".$fecha_act);
		
		return $array_response;
   }
   
   public function contver_fecha($mes,$anio)
   {
   		switch($mes){
			case 1:
				$mess= "Enero";
				break;
			case 2:
				$mess= "Febrero";
				break;
			case 3:
				$mess= "Marzo";
				break;
			case 4:
				$mess= "Abril";
				break;
			case 5:
				$mess= "Mayo";
				break;
			case 6:
				$mess= "Junio";
				break;
			case 7:
				$mess= "Julio";
				break;
			case 8:
				$mess= "Agosto";
				break;
			case 9:
				$mess= "Septiembre";
				break;
			case 10:
				$mess= "Octubre";
				break;
			case 11:
				$mess= "Noviembre";
				break;
			case 12:
				$mess= "Diciembre";
				break;
			default:
		}
		
		return $mess." ".$anio;
   }
   
   public function Datos_CSV()
   {
	    $oSession = unserialize($_SESSION[$GLOBALS["SESION_POS"]]);
	    $idpos=$oSession->VSidpos;
		
	    $array_response = array();
		
	    $BD=new BD();
	    $BD->conectar();
		
		$fecha_act = date("Y-m-d");
		
		$fecha_act = strtotime ( '-1 day' , strtotime ( $fecha_act ) ) ;
		$fecha_act = date ( 'Y-m-d' , $fecha_act );
		
		$fecha_ini = strtotime ( '-6 month' , strtotime ( $fecha_act ) ) ;
		$fecha_ini = date ( 'Y-m-d' , $fecha_ini );
		
		$sql_sim = "SELECT acti.NUMERO_ICC,acti.FECHA_VENTA,acti.HORA_VENTA,sim.distri,sim.id_vendedor,recar.FEC_RECARGA,recar.VAL_RECARGA,recar.NUM_ICC,recar.NUM_CELULAR
		
							FROM {$GLOBALS["BD_NAME"]}.csv_activaciones acti, {$GLOBALS["BD_NAME"]}.simcards sim, {$GLOBALS["BD_NAME"]}.csv_recarga recar
							
							WHERE acti.NUMERO_ICC = sim.serie and recar.NUM_ICC = acti.NUMERO_ICC and acti.FECHA_VENTA >= '$fecha_ini' and acti.FECHA_VENTA <= '$fecha_act' and sim.id_pos = $idpos";
		
	
	    $res_act = $BD->consultar($sql_sim);
	   
		if($BD->numreg($res_act)>0){
			
			while(!$res_act->EOF){
			
				$bd_distri = "";
				
				//Consulta distribuidor
				
				$sql = "SELECT nombre_corto
			
				FROM {$GLOBALS["BD_NAME"]}.distribuidores distri
				
				WHERE distri.id = '".$res_act->fields["distri"]."'";
				
				$res_distri = $BD->consultar($sql);
				
				$bd_distri = "distri_".$res_distri->fields["nombre_corto"]."_".$res_act->fields["distri"];
				
				//Consulta asesor
				
				$sql = "SELECT concat(nombre,' ',apellido) as nombres
			
				FROM ".$bd_distri.".usuarios usu
				
				WHERE usu.id = '".$res_act->fields["id_vendedor"]."'";
				
				$res_ase = $BD->consultar($sql);
		
				$array_response[] = array("serial" => $res_act->fields['NUM_ICC'].".", "movil" => $res_act->fields['NUM_CELULAR'], "fecha_act" => $res_act->fields['FECHA_VENTA'], "hora_act" => $res_act->fields['HORA_VENTA'], "FEC_RECARGA" => $res_act->fields["FEC_RECARGA"], "total_recar" => $res_act->fields["VAL_RECARGA"], "distribuidor" => $res_distri->fields["nombre_corto"], "asesor" => $res_ase->fields["nombres"]);
				
				$res_act->MoveNext();
			}
			
		}
		
	    $sql_kit  = "SELECT acti.NUMERO_ICC,acti.FECHA_VENTA,acti.HORA_VENTA,pro.distri,pro.id_vendedor,recar.FEC_RECARGA,recar.VAL_RECARGA,recar.NUM_ICC,recar.NUM_CELULAR
	
						FROM {$GLOBALS["BD_NAME"]}.csv_activaciones acti, {$GLOBALS["BD_NAME"]}.producto pro, {$GLOBALS["BD_NAME"]}.csv_recarga recar
						
						WHERE acti.NUMERO_ICC = pro.serie and recar.FEC_RECARGA >= '$fecha_ini' and recar.FEC_RECARGA <= '$fecha_act' and pro.id_pos = $idpos";
		
		$res_act2 = $BD->consultar($sql_kit);
		
		if($BD->numreg($res_act2)>0){
			
			while(!$res_act2->EOF){
			
				$bd_distri = "";
				
				//Consulta distribuidor
				
				$sql = "SELECT nombre_corto
			
				FROM {$GLOBALS["BD_NAME"]}.distribuidores distri
				
				WHERE distri.id = '".$res_act2->fields["distri"]."'";
				
				$res_distri = $BD->consultar($sql);
				
				$bd_distri = "distri_".$res_distri->fields["nombre_corto"]."_".$res_act2->fields["distri"];
				
				//Consulta asesor
				
				$sql = "SELECT concat(nombre,' ',apellido) as nombres
			
				FROM ".$bd_distri.".usuarios usu
				
				WHERE usu.id = '".$res_act2->fields["id_vendedor"]."'";
				
				$res_ase = $BD->consultar($sql);
		
				$array_response[] = array("serial" => $res_act2->fields['NUM_ICC'].".", "movil" => $res_act2->fields['NUM_CELULAR'], "fecha_act" => $res_act2->fields['FECHA_VENTA'], "hora_act" => $res_act2->fields['HORA_VENTA'], "FEC_RECARGA" => $res_act2->fields["FEC_RECARGA"], "total_recar" => $res_act2->fields["VAL_RECARGA"], "distribuidor" => $res_distri->fields["nombre_corto"], "asesor" => $res_ase->fields["nombres"]);
				
				$res_act2->MoveNext();
			}
			
		}
			
		return $array_response;
		
   }
   
   
}// Fin clase
?>