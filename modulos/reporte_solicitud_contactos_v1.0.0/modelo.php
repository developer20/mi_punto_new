<?php
require("../../config/mainModel.php");
class reporte_solicitud
{
    /*
 METODOS DE LA BD.
 $this->BD->consultar($query); // Ejecuta la consulta y devuelve string.!
 $this->BD->devolver_array_new($query); // Ejecuta la consulta y devuelve array asociativo.!
 $this->BD->consultar("BEGIN"); // Antes de transacciones.!
 $this->BD->consultar("COMMIT"); // Commit para guardar datos.!
 $this->BD->consultar("ROLLBACK"); // Devolver datos si hay error.!
 $this->BD->numreg($query); // Devuelve el numero de registros de la consulta.!
*/
    public function __construct()
    {
        $BD = new BD();
        $this->BD = $BD;
        $this->BD->conectar();  
        
        $oSession = unserialize($_SESSION[$GLOBALS["SESION_POS"]]);
        $this->idDistri = $oSession->VSid_distri;
        $this->punto  =  $oSession->VSidpos;
  
      $sql_dis = "SELECT nombre_corto FROM {$GLOBALS["BD_NAME"]}.distribuidores WHERE id = $this->idDistri";
        $query_dis = $this->BD->devolver_array($sql_dis);
        $nombre_corto = $query_dis[0]["nombre_corto"];
        $bd = "{$GLOBALS["BD_DIS"]}" . $nombre_corto . "_" . $this->idDistri;

    }
    public function __destruct()
    {
        $this->BD->desconectar();
    }

    public function Consultar($fecha_inicio,$fecha_fin,$tiposol,$estadosol){

        $bd_distri = $this->BD->bd_distri();
        $condiccion = "";        			

        if($fecha_inicio != ""){
            $condiccion .= " AND sp.fecha >= '$fecha_inicio' ";
        }

        if($fecha_fin != ""){
            $condiccion .= " AND sp.fecha <= '$fecha_fin' ";
        }

        if($tiposol != ""){
            if ($tiposol == "1") {
                $condiccion .= " AND sp.id_pos = $this->punto";
            }else{
                $condiccion .= " AND sp.id_pos <> $this->punto";
            }  
        }

        if($estadosol != ""){
            $condiccion .= " AND sp.estado = $estadosol";
        }

        $consulta = "SELECT 
                            sp.id,
                            sp.fecha, 
                            sp.hora, 
                            CASE  
                                WHEN sp.id_pos = $this->punto THEN 'Hechas por mi'                              
                                WHEN sp.id_pos <> $this->punto THEN 'Que me hicieron'
                            END id_pos,
                            CASE  
                                WHEN sp.estado = 0 THEN 'N/A'                              
                                WHEN sp.estado = 1 THEN 'Aprobado'
                                WHEN sp.estado = 2 THEN 'No Aprobado'
                            END estado
                           FROM {$GLOBALS["BD_POS"]}.solicitud_publicacion__mp AS sp                           
                          WHERE 1 $condiccion";  
                                                
        $datos = $this->BD->devolver_array($consulta);
		
        return $datos;
    }

    public function obtener_datos($id){

        $consulta = "SELECT 
            sp.id,
            sp.fecha, 
            sp.hora, 
            sp.id_pos,
            sp.nombre,
            sp.nom_encargado,
            sp.telefono, 
            sp.celular, 
            cp.titulo, 
        CASE  
            WHEN sp.id_pos = $this->punto THEN 'Hechas por mi'                              
            WHEN sp.id_pos <> $this->punto THEN 'Que me hicieron'
        END tiposoltud,
        CASE  
            WHEN sp.estado = 0 THEN 'N/A'                              
            WHEN sp.estado = 1 THEN 'Aprobado'
            WHEN sp.estado = 2 THEN 'No Aprobado'
        END estado
        FROM {$GLOBALS["BD_POS"]}.solicitud_publicacion__mp AS sp 
        LEFT JOIN {$GLOBALS["BD_POS"]}.puntos p ON p.idpos = sp.id_pos  
        LEFT JOIN {$GLOBALS["BD_POS"]}.crear_productos__mp cp ON cp.id = sp.id_producto                   
                WHERE sp.id = $id";                  
       
         $datos = $this->BD->devolver_array($consulta);

         return $datos;
      }

  
}// Fin clase
