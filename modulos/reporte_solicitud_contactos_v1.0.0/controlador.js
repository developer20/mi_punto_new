
var dtable;

$(document).ready(function () {

    $(".content-header").find("h1").html("Reporte  Solicitud de Contacto");
    $(".fecha").datepicker({
        format: "yyyy-mm-dd",
        todayHighlight: true,
        todayBtn: true
    });

    $("#frm_buscar_solicitud").submit(function (event) {
        event.preventDefault();
        Consultar();
    });

    $("#CSV").click(function () {
        CSV();
    });

})


function Consultar() {

    fecha_inicio = $("#fecha_inicio").val();
    fecha_fin = $("#fecha_fin").val();
    tiposol = $("#tiposol").val();
    estadosol = $("#estadosol").val();
    f_ini = new Date(fecha_inicio);
    f_fin = new Date(fecha_fin);
    dias = f_fin.getTime() - f_ini.getTime();
    diasDif = Math.round(dias / (1000 * 60 * 60 * 24));

    if (fecha_inicio == "" || fecha_fin == "") {
        Notificacion("La fecha inicial y la fecha final son obligatorios", "warning");
        return false;
    }
    if (fecha_inicio > fecha_fin) {
        Notificacion("La fecha inicial no debe ser mayor a la fecha final", "warning");
        return false;
    }
    if (diasDif > 31) {
        Notificacion("El rango maximo entre las fechas es de 31 dias.", "warning");
        return false;
    }

    if (!$.fn.DataTable.isDataTable('#t_reporte')) {

        dtable = $("#t_reporte").DataTable({

            "ajax": {
                "url": "modulos/reporte_solicitud_contactos_v1.0.0/controlador.php",
                "type": "POST",
                "deferRender": false,
                "data": {
                    accion: 'Consultar',
                    fecha_inicio: fecha_inicio,
                    fecha_fin: fecha_fin,
                    tiposol: tiposol,
                    estadosol: estadosol
                }
            },
            "bFilter": false,
            "responsive": true,
            "columns": [
                { "data": "fecha" },
                { "data": "hora" },
                { "data": "id_pos" },
                { "data": "estado" },
                { "data": "id" }
            ],
            "columnDefs": [
                {
                    "targets": 4,
                    "data": "",
                    render: function (data, type, row) {

                        return '<i class="glyphicon glyphicon-plus-sign desDeta" onclick="ver_detalle(' + row.id + ')" style="color: #10628A;font-size: 20px;cursor: pointer;position:relative;"></i>';

                    }
                },
            ],

        });
    }
    else {
        dtable.destroy();
        Consultar();
    }
    $("#consulta").show();
}


function ver_detalle(id) {

    $.post('modulos/reporte_solicitud_contactos_v1.0.0/controlador.php', { accion: 'obtener_datos', id: id },
      function (data) {
        data = JSON.parse(data);

        $('#modal_contacto').modal({ backdrop: 'static', keyboard: false });
        $("#modal_contacto").modal("show");
        $("#modal_contacto").show();

        $("#idpdvsol").html(data[0]["id_pos"]);
        $("#nombrepdvsol").html(data[0]["nombre"]);
        $("#nombreencargadosol").html(data[0]["nom_encargado"]);
        $("#telefonosol").html(data[0]["telefono"]);
        $("#celularsol").html(data[0]["celular"]);
        $("#tituloprod").html(data[0]["titulo"]);

      });
  
  }

  function cerrar_modal() {

    $('#modal_contacto').modal({ backdrop: 'static', keyboard: true });
    $("#modal_contacto").hide();
    return false;
  }

function CSV() {

    fecha_inicio = $("#fecha_inicio").val();
    fecha_fin = $("#fecha_fin").val();
    tiposol = $("#tiposol").val();
    estadosol = $("#estadosol").val();

    window.open("modulos/reporte_solicitud_contactos_v1.0.0/csv.php?fecha_inicio=" + fecha_inicio + "&fecha_fin=" + fecha_fin + "&tiposol=" + tiposol + "&estadosol=" + estadosol);
}

$(document).ajaxSend(function (event, request, settings) {
    loader = $('body').loadingIndicator({
        useImage: false,
    }).data("loadingIndicator");

    loader.show();
});