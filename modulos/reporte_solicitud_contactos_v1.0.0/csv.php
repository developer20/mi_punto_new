<?php

ini_set('memory_limit', '3000M');
set_time_limit(3000);

if (!isset($_SESSION)) {
  session_start();
}
require_once("../../config/Session.php");
require("../../config/mainModel.php");

$BD = new BD();
$BD->conectar();

$oSession = unserialize($_SESSION[$GLOBALS["SESION_POS"]]);
$idDistri = $oSession->VSid_distri;
$punto  =  $oSession->VSidpos;

$variables = $_GET;

$fecha_inicio = $variables['fecha_inicio'];
$fecha_fin = $variables['fecha_fin'];
$tiposol = $variables['tiposol'];
$estadosol = $variables['estadosol'];

$condiccion = "";

if($fecha_inicio != ""){
  $condiccion .= " AND sp.fecha >= '$fecha_inicio' ";
}

if($fecha_fin != ""){
  $condiccion .= " AND sp.fecha <= '$fecha_fin' ";
}

if($tiposol != ""){
  if ($tiposol == "1") {
      $condiccion .= " AND sp.id_pos = $punto";
  }else{
      $condiccion .= " AND sp.id_pos <> $punto";
  }  
}

if($estadosol != ""){
  $condiccion .= " AND sp.estado = $estadosol";
}

          $consulta = "SELECT 
          sp.id,
          sp.fecha, 
          sp.hora, 
          sp.id_pos,
          sp.nombre,
          sp.nom_encargado,
          sp.telefono, 
          sp.celular, 
          CASE  
              WHEN sp.id_pos = $punto THEN 'Hechas por mi'                              
              WHEN sp.id_pos <> $punto THEN 'Que me hicieron'
          END tiposoltud,
          CASE  
              WHEN sp.estado = 0 THEN 'N/A'                              
              WHEN sp.estado = 1 THEN 'Aprobado'
              WHEN sp.estado = 2 THEN 'No Aprobado'
          END estado
          FROM {$GLOBALS["BD_POS"]}.solicitud_publicacion__mp AS sp                           
          WHERE 1 $condiccion";  

$datos = $BD->devolver_array($consulta);

if (count($datos) > 0) {
  $nombre_archivo = "Reporte_Solicitud_Contactos.csv";
  $encabezados = "Fecha; Hora; Id Pdv; Tipo Solicitud; Nombre Pdv; Nombre Encargado; Telefono; Movil; Estado;";
  $llaves = array(
    "fecha",
    "hora",
    "id_pos",
    "tiposoltud",
    "nombre",
    "nom_encargado",
    "telefono",
    "celular",
    "estado"
  );

  /*Se llama el metodo que comprime el archivo. Los argumentos son:
  1: nombre del archivo zip
  2: nombre del archivo que se va a comprimir
  3: Encabezado del archivo que se va a comprimir
  4: Datos que va conterner el archivo que se va a comprimir (es un arreglo)
  5: Llave de los datos
  */
  $BD->comprimir_archivo("Reporte_Pedidos", $nombre_archivo, $encabezados, $datos, $llaves);
}

?>
