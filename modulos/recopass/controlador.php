<?php
//include("../../config/mainController.php");
//$controller = new mainController;
$metodo = $_SERVER['REQUEST_METHOD'];
$recurso = $_SERVER['REQUEST_URI'];
$tipo_res = "";
$response = ""; 

// Se manejaran dos tipos JSON y HTML
// Dependiendo del método de la petición ejecutaremos la acción correspondiente.
// Por ahora solo POST, todas las llamadas se haran por POST
	$variables = $_POST;

	if(!isset($_POST['accion'])) {
		print json_encode(0);
		return;
	}
    $accion = $variables['accion'];
	
	function __autoload($class){
		include_once("../../modulos/recopass/modelo.php");	
	}
	$val = new Validar(); // Instancia a la clase del modelo
	switch($accion) {
		case 'validarReco':
			 $tipo_res = 'HTML'; //Definir tipo de respuesta;
			 $response = $val->validarReco();
		break;
		case 'EnviarPass':
			 $tipo_res = 'HTML'; //Definir tipo de respuesta;
			 $pass = $variables['pass'];
			 $response = $val->EnviarPass($pass);
		break;
	}
if($tipo_res == "JSON")
{
  echo json_encode($response,true); // $response será un array con los datos de nuestra respuesta.
}
elseif ($tipo_res == "HTML") {
  echo $response; // $response será un html con el string de nuestra respuesta.
}
exit();
