<?php

session_start();
require("../../config/mainModel.php");
require("../../config/Session.php");
require("../../config/mail/mail.php");
require_once("../../config/f_val.php");

class Validar {
	
	public function validarReco()
	{
		if(isset($_SESSION["rpss"])){
			
			$BD=new BD();
			$BD->conectar();
			
			$rpss = $_SESSION["rpss"];
			
			$sql = "select idpos,hora_re,fecha_re from puntos where recover = '$rpss' limit 1";
			$res_cosult = $BD->consultar($sql);
			if($BD->numreg($res_cosult) > 0){
				
				$horaactual=date("H:i:s");
				$fechaactual=date("Y-m-d");
				
				if($horaactual > $res_cosult->fields["hora_re"]  or $res_cosult->fields["fecha_re"] < $fechaactual){
						
					return 2;
				
				}else{
					return 0;
				}
				
			}else{
				return 1;
			}
		}else{
			return 3;
		}
	}
	
	public function EnviarPass($passw)
	{
		if(isset($_SESSION["rpss"])){
		    
			$rpss = $_SESSION["rpss"];
			
			$BD=new BD();
    	    $BD->conectar();
		
			$error = 0;
			$num = 0;
			
			$pass = str_replace(' ', '', $passw);
			$PasswordLimpio=$BD->InyeccionSql("$pass");
			
			if($PasswordLimpio != $passw){
				
				return 2;
				
			}else{
				
				$sql = "select idpos,hora_re,fecha_re from puntos where recover = '$rpss' limit 1";
				$res_cosult = $BD->consultar($sql);
				
				if($BD->numreg($res_cosult) > 0){
					
					$horaactual=date("H:i:s");
					$fechaactual=date("Y-m-d");
					
					if($horaactual > $res_cosult->fields["hora_re"]  or $res_cosult->fields["fecha_re"] < $fechaactual){
						
						return 5;
						
					}else{
			
						$pass = movpass($pass);
						$idpunto = $res_cosult->fields["idpos"];
						
						if (@$_SERVER["HTTPS"] != "off"){
						  $url_cli = "https:/";
						}else{
						  $url_cli = "http:/";
						}
					
						$url_cli .= '192.168.2.24/web/{$GLOBALS["BD_NAME"]}2/puntorecarga';
						
						//$url_cli=dirname($url_cli);
					
						$recover=md5(rand());
					
						$BD->consultar("BEGIN");
						
						/*$sql = "update sesion_distri{$GLOBALS["BD_NAME"]}.session_puntos set correo='$correo',recover='$recover' where login='$login'";
						$res = $BD->consultar($sql);
						if(!$res) $error = 1;
						
						$sql = "update {$GLOBALS["BD_POS"]}.pos set correo='$correo' where idpos='$login'";
						$res2 = $BD->consultar($sql);
						if(!$res2) $error = 1;*/
						
						$sql = "update puntos set password='$pass', recover='$recover' where idpos=$idpunto and recover = '$rpss'";
						$res = $BD->consultar($sql);
						if(!$res) $error = 1;
						
						if($error==1){
							$BD->consultar("ROLLBACK");
							return 0;
						}elseif($error==0){
							$BD->consultar("COMMIT");
							return 1;
							unset($_SESSION["rpss"]);
						}
					}
				}else{
					return 3;
				}
			}
			
		}else{
			
			return 4;
		
		}
	}

}
?>