$(document).ready(function() {
	
	validarReco();
	
	$("#form_pass").submit(function(event) {
	 	event.preventDefault();
        autenticarPass();
    });
	
	$( "#pass" ).keypress(function(event) {
		if(window.event){
			keynum = event.keyCode;
		}else{
			keynum = event.which;
		}
		if((keynum>32)){
			  return true;
		}else{
			return false;
		}
	  //validar_espacios(event);
	});
	
	$( "#pass2" ).keypress(function(event) {
		if(window.event){
			keynum = event.keyCode;
		}else{
			keynum = event.which;
		}
		if((keynum>32)){
			  return true;
		}else{
			return false;
		}
	  //validar_espacios(event);
	});
	
	
	$( "#pass" ).change(function() {
		
		cadena = this.value;  
		
		var tamano = false
		var mayus = false  
		var minus = false   
		var num = false
		var inyeccion = false      
		var caracter = false
		var espa = false;
		var inyectss = "";
		var conMin = false;
		var conMay = false;
		var conNum = false;
		var inyecciones = [";", "'", "ALTER", "DROP", "SELECT", "FROM", "WHERE", "INSERT", "DELETE", "*", " OR ", " AND ", "%27", "TABLE", "(", ")", "?", "=", "&"];  
		var le_con_minu = ['abc', 'bcd', 'cde', 'def', 'efg', 'fgh', 'ghi', 'hij','ijk', 'jkl', 'klm', 'lmn', 'mno', 'nop', 'opq', 'pqr', 'qrs', 'rst', 'stu', 'tuv', 'uvw', 'vwx', 'xyz'];
		var le_con_mayu = ['ABC', 'BCD', 'CDE', 'DEF', 'EFG', 'FGH', 'GHI', 'HIJ','IJK', 'JKL', 'KLM', 'LMN', 'MNO', 'NOP', 'OPQ', 'PQR', 'QRS', 'RST', 'STU', 'TUV', 'UVW', 'VWX', 'XYZ'];
		var num_val = ['012','123','234','345','456','567','678','789'];	
		var numeros="0123456789";
		var patron_may=/[A-Z]/g; 
		var patron_min=/[a-z]/g;
		
		 for(i=0;i<cadena.length;i++){  
			
			if (cadena.match(patron_may) != null){
				
				mayus=true
			}
			
			if (cadena.match(patron_min) != null){
				minus = true;
			}
			
			if(numeros.indexOf(cadena.charAt(i),0)!=-1) 
			{  
			   num=true  
			 
			}
			
			for(var n=0;n<le_con_minu.length;n++){
				if(cadena.indexOf(le_con_minu[n]) != -1) {
				   conMin = true;
				}
			}
			
			for(var y=0;y<le_con_mayu.length;y++){
				if(cadena.indexOf(le_con_mayu[y]) != -1) {
				   conMay = true;
				}
			}
			
			for(var z=0;z<num_val.length;z++){
				if(cadena.indexOf(num_val[z]) != -1) {
				   conNum = true;
				}
			}
			
			if ( cadena.charAt(i) == " " ) {
				espa = true;
			}
			
		}
	
		for(i=0;i<cadena.length;i++){  
			
			cadena = cadena.toUpperCase();
			for(var z=0;z<inyecciones.length;z++){
				if(cadena.indexOf(inyecciones[z]) != -1) {
				   inyeccion = true;
				   inyectss = inyecciones[z];
				}
			}
			
		} 
		
		if(cadena.length < 7 || cadena.length > 12){
			tamano = true;
		} 
	
		
		if(espa == true){
		   Notificacion("Error, no se permite que la contraseña contenga espacios","error");
		   document.getElementById("pass").value="";
		   document.getElementById("pass").focus()
		}else if(inyeccion == true){
		   Notificacion("No debe contener palabras o caracteres como "+inyectss+" entre la contraseña","error");
		   document.getElementById("pass").value="";
		   document.getElementById("pass").focus()
		   return false 
		}else if(!num)
		{  
			Notificacion("Error, la contraseña debe tener minimo un número","error");
			document.getElementById("pass").value="";
			document.getElementById("pass").focus()
			return false //cambiar false por true para hacer el submit
		}else if(!mayus){
			Notificacion("Error, la contraseña debe tener minimo una mayuscula","error");
			document.getElementById("pass").value="";
			document.getElementById("pass").focus()
			return false //cambiar false por true para hacer el submit 
		}else if(!minus){
			Notificacion("Error, la contraseña debe tener minimo una minuscula","error");
			document.getElementById("pass").value="";
			document.getElementById("pass").focus()
			return false //cambiar false por true para hacer el submit 
		}else if(tamano==true)
		{  
			Notificacion("Error, la contraseña nueva debe ser mínimo de 7 caracteres y máximo de 12","error");
			document.getElementById("pass").value="";
			document.getElementById("pass").focus()
			return false //cambiar false por true para hacer el submit 
		}else if(conMin==true)
		{  
			Notificacion("Error, la contraseña no puede tener mas de 2 letras minusculas consecutivas","error");
			document.getElementById("pass").value="";
			document.getElementById("pass").focus()
			return false //cambiar false por true para hacer el submit 
		}else if(conMay==true)
		{  
			Notificacion("Error, la contraseña no puede tener mas de 2 letras mayusculas consecutivas","error");
			document.getElementById("pass").value="";
			document.getElementById("pass").focus()
			return false //cambiar false por true para hacer el submit 
		}else if(conNum==true)
		{  
			Notificacion("Error, la contraseña no puede tener mas de 2 numeros consecutivos","error");
			document.getElementById("pass").value="";
			document.getElementById("pass").focus()
			return false //cambiar false por true para hacer el submit 
		}  
	});

	
});

function validarReco(){
		
		$.post('modulos/recopass/controlador.php',
			{
			  accion: 'validarReco',
			},
			function(data, textStatus) {
				
				
				
				if(data == 1){
					BootstrapDialog.confirm("No se permite que restablezcas la contraseña, porque la contraseña ya fue restablecida.", function(result){
					if(result) {
						location.href = "?cerrar=S";
					}else{
						location.href = "?cerrar=S";
					}
					});
					setTimeout(function(){ window.location = "?cerrar=S" }, 5000);
					
				}else if(data == 2){
					BootstrapDialog.confirm("No se permite que restablezcas la contraseña, porque se agotó el limite de espera.", function(result){
					if(result) {
						location.href = "?cerrar=S";
					}else{
						location.href = "?cerrar=S";
					}
					});
					setTimeout(function(){ window.location = "?cerrar=S" }, 5000);
				}else if(data == 3){
					BootstrapDialog.confirm("Error, la url no es una url valida", function(result){
					if(result) {
						location.href = "?cerrar=S";
					}else{
						location.href = "?cerrar=S";
					}
					});
					setTimeout(function(){ window.location = "?cerrar=S" }, 5000);
				}
				
			}
		);
		
	}

function autenticarPass()
{
	
	pass = document.getElementById("pass").value;
	var val = validar_pass();
	
	if(val == 0){
		
		$(".panel-footer").html("Espera un momento... ");
		
		$.post('modulos/recopass/controlador.php',
		{
		  accion: 'EnviarPass',
		  pass : pass,
		},
			function(data, textStatus) {
				if(data == 1){
					BootstrapDialog.confirm("Se ha cambiado tu contraseña exitosamente.", function(result){
			   		if(result) {
						location.href = "?cerrar=S";
					}else{
						location.href = "?cerrar=S";
					}
					});
					setTimeout(function(){ window.location = "?cerrar=S" }, 3000);
				}else if(data == 2){
					Notificacion("Error, la contraseña enviada no es una contraseña valida","error");
					$(".panel-footer").html('<button type="submit" name="bt_usu_search" id="bt_usu_search" class="btn btn-primary">Guardar</button>');
				}else if(data == 3){
					BootstrapDialog.confirm("No se permite que restablezcas la contraseña, porque la contraseña ya fue restablecida.", function(result){
			   		if(result) {
						location.href = "?cerrar=S";
					}else{
						location.href = "?cerrar=S";
					}
					});
					setTimeout(function(){ window.location = "?cerrar=S" }, 5000);
				}else if(data == 4){
					BootstrapDialog.confirm("Error, la url no es una url valida", function(result){
			   		if(result) {
						location.href = "?cerrar=S";
					}else{
						location.href = "?cerrar=S";
					}
					});
					setTimeout(function(){ window.location = "?cerrar=S" }, 5000);
				}else if(data==5)
				{   
					BootstrapDialog.confirm("No se permite que restablezcas la contraseña, porque se agotó el limite de espera.", function(result){
			   		if(result) {
						location.href = "?cerrar=S";
					}else{
						location.href = "?cerrar=S";
					}
					});
					setTimeout(function(){ window.location = "?cerrar=S" }, 5000);
				}else if(data == 0){
					Notificacion("Error al guardar la contraseña","error");
					$(".panel-footer").html('<button type="submit" name="bt_usu_search" id="bt_usu_search" class="btn btn-primary">Guardar</button>');
				}
			}
		);	
	
	}else if(val == 2){
		Notificacion("Error, las contraseñas no coinciden","error");	
		document.getElementById("pass2").value = "";
		document.getElementById("pass2").focus();
	}else if(val == 3){
		Notificacion("No se permite campos vacíos","error");
	
	}
}

function validar_pass(){

   pass = document.getElementById("pass").value;
   pass2 = document.getElementById("pass2").value;
   var espacio_blanco    = /\s/;
   var valida = 0;
	
	if(pass=="" || pass2==""){
		valida =  3;
	}else if(pass!="" && pass2!="" && pass!=pass2){
		valida =  2;
	}
	
	return valida;

}

//DESHABILITO LA BARRA ESPACIADORA PARA QUE EL USUARIO NO INGRESE ESPACIOS AL CREAR LA CLAVE NUEVA
function validar_espacios(evt){
	if(window.event){
		keynum = evt.keyCode;
	}else{
		keynum = evt.which;
	}
	if((keynum>32)){
		  return true;
	}else{
		return false;
	}
}
