<?php
require("../../config/mainModel.php");
class reporte_compra{
	

   public function Cargar_Informe($f_ini,$f_fin,$tipo)
   {
	   $oSession = unserialize($_SESSION[$GLOBALS["SESION_POS"]]);
	   $idpos=$oSession->VSidpos;
		
	   $array_response = array();
		
	   $BD=new BD();
	   $BD->conectar();
		
		$sql1 = "SELECT s.id_referencia, r.producto, COUNT(DISTINCT s.iccid) cantidad,'Simcard' as tipo FROM {$GLOBALS["BD_NAME"]}.simcards s, {$GLOBALS["BD_NAME"]}.referencias r WHERE s.id_referencia = r.id AND s.estado = 1 AND s.id_pos = '$idpos' AND s.fecha_venta BETWEEN '$f_ini' AND '$f_fin' GROUP BY s.id_referencia";
		
		$sql2 = "SELECT p.id_referencia, r.producto, COUNT(DISTINCT p.imei) cantidad,'Equipo' as tipo FROM {$GLOBALS["BD_NAME"]}.producto p, {$GLOBALS["BD_NAME"]}.referencias r WHERE p.id_referencia = r.id AND p.estado = 1 AND p.id_pos = '$idpos' AND p.fecha_venta BETWEEN '$f_ini' AND '$f_fin' GROUP BY p.id_referencia";
		
		$sql = $sql1." UNION ".$sql2;
		
		$res = $BD->consultar($sql);
		
		if($BD->numreg($res)>0){
				while(!$res ->EOF){
					$array_response[] = array("id_ref" => $res->fields['id_referencia'], "producto" => $res->fields['producto'], "cantidad" => $res->fields['cantidad'], "tipo" => $res->fields['tipo']);
					$res ->MoveNext();	
				}
		}
			
		return $array_response;
		
   }
   
   public function Cargar_Informe_Ases($f_ini,$f_fin,$id_ref)
   {
	   $oSession = unserialize($_SESSION[$GLOBALS["SESION_POS"]]);
	   $idpos=$oSession->VSidpos;
		
	   $array_response = array();
		
	   $BD=new BD();
	   $BD->conectar();
		
		$sql1 = "SELECT s.id_referencia, r.producto,s.distri,s.id_vendedor,d.nombre n_distri,d.nombre_corto n_db, COUNT(DISTINCT s.iccid) cantidad FROM {$GLOBALS["BD_NAME"]}.simcards s, {$GLOBALS["BD_NAME"]}.referencias r, {$GLOBALS["BD_NAME"]}.distribuidores d WHERE s.id_referencia = r.id AND s.id_referencia = '$id_ref' AND s.estado = 1 AND d.id = s.distri AND s.id_pos = '$idpos' AND s.fecha_venta BETWEEN '$f_ini' AND '$f_fin' GROUP BY s.id_referencia,s.distri,s.id_vendedor";
		
		$sql2 = "SELECT p.id_referencia, r.producto,p.distri,p.id_vendedor,d.nombre n_distri,d.nombre_corto n_db, COUNT(DISTINCT p.imei) cantidad FROM {$GLOBALS["BD_NAME"]}.producto p, {$GLOBALS["BD_NAME"]}.referencias r, {$GLOBALS["BD_NAME"]}.distribuidores d WHERE p.id_referencia = r.id AND p.id_referencia = '$id_ref' AND p.estado = 1 AND d.id = p.distri AND p.id_pos = '$idpos' AND p.fecha_venta BETWEEN '$f_ini' AND '$f_fin' GROUP BY p.id_referencia,p.distri,p.id_vendedor";
		
		$sql = $sql1." UNION ".$sql2;
		
		$res = $BD->consultar($sql);
	
		if($BD->numreg($res)>0){
				while(!$res ->EOF){
					$id_asesor = $res->fields['id_vendedor'];
					$id_distri = $res->fields['distri'];
					$db_corto = $res->fields['n_db'];
					$nm_db = $GLOBALS["BD_DIS"].$db_corto.'_'.$id_distri;
					$db = $nm_db.'.usuarios';
					
					$sq2 = "SELECT CONCAT(nombre,' ',apellido) nom_as FROM $db WHERE id = '$id_asesor' ";
					$r2 = $BD->consultar($sq2);
					$nom_as = $r2->fields['nom_as'];
					
					$array_response[] = array('id_distri' => $id_distri, 'nm_distri' => $res->fields['n_distri'], 'id_ase' => $id_asesor, 'nm_ase' => $nom_as, 'id_ref' => $res->fields['id_referencia'], 'cantidad' => $res->fields['cantidad']);
					$res ->MoveNext();
				}
		}
			
		return $array_response;
		
   }
   
   public function Cargar_Informe_Serie($f_ini,$f_fin,$id_ref,$id_ase,$id_distri)
   {
	   $oSession = unserialize($_SESSION[$GLOBALS["SESION_POS"]]);
	   $idpos=$oSession->VSidpos;
		
	   $array_response = array();
		
	   $BD=new BD();
	   $BD->conectar();
		
		$sql1 = "SELECT s.fecha_venta, s.iccid,'',CASE s.id_pagocomi WHEN 0 THEN 'No' ELSE 'Si' END AS comisionado FROM {$GLOBALS["BD_NAME"]}.simcards s, {$GLOBALS["BD_NAME"]}.referencias r, {$GLOBALS["BD_NAME"]}.distribuidores d WHERE s.id_referencia = r.id AND s.id_referencia = '$id_ref' AND s.estado = 1 AND d.id = s.distri AND s.distri = '$id_distri' AND s.id_vendedor = '$id_ase' AND s.id_pos = '$idpos' AND s.fecha_venta BETWEEN '$f_ini' AND '$f_fin' ";
		
		$sql2 = "SELECT p.fecha_venta, p.imei serie,'',CASE p.id_pagocomi WHEN 0 THEN 'No' ELSE 'Si' END AS comisionado FROM {$GLOBALS["BD_NAME"]}.producto p, {$GLOBALS["BD_NAME"]}.referencias r, {$GLOBALS["BD_NAME"]}.distribuidores d WHERE p.id_referencia = r.id AND p.id_referencia = '$id_ref' AND p.estado = 1 AND d.id = p.distri AND p.distri = '$id_distri' AND p.id_vendedor = '$id_ase' AND p.id_pos = '$idpos' AND p.fecha_venta BETWEEN '$f_ini' AND '$f_fin' ";
		
		
		$sql = $sql1." UNION ".$sql2;
		
		$res = $BD->consultar($sql);
	
		if($BD->numreg($res)>0){
				while(!$res ->EOF){
					
					$array_response[] = array('f_venta' => $res->fields['fecha_venta'], 'serie' => $res->fields['iccid'], 'concepto' => "Compra",'comisionado' => $res->fields['comisionado']);
					$res ->MoveNext();
				}
		}
			
		return $array_response;
		
   }
   
    public function Datos_CSV($f_ini,$f_fin)
   {
	   $oSession = unserialize($_SESSION[$GLOBALS["SESION_POS"]]);
	   $idpos=$oSession->VSidpos;
		
	   $array_response = array();
		
	   $BD=new BD();
	   $BD->conectar();
		
		$sql1 = "SELECT s.iccid, '',s.fecha_venta, s.id_referencia, r.producto,s.distri,s.id_vendedor,d.nombre n_distri,d.nombre_corto n_db,CASE s.id_pagocomi WHEN 0 THEN 'No' ELSE 'Si' END AS comisionado,'Simcard' as tipo FROM {$GLOBALS["BD_NAME"]}.simcards s, {$GLOBALS["BD_NAME"]}.referencias r, {$GLOBALS["BD_NAME"]}.distribuidores d WHERE s.id_referencia = r.id AND s.estado = 1 AND d.id = s.distri AND s.id_pos = '$idpos' AND s.fecha_venta BETWEEN '$f_ini' AND '$f_fin' ";
		
		$sql2 = "SELECT p.imei serie, '',p.fecha_venta, p.id_referencia, r.producto,p.distri,p.id_vendedor,d.nombre n_distri,d.nombre_corto n_db,CASE p.id_pagocomi WHEN 0 THEN 'No' ELSE 'Si' END AS comisionado,'Equipo' as tipo FROM {$GLOBALS["BD_NAME"]}.producto p, {$GLOBALS["BD_NAME"]}.referencias r, {$GLOBALS["BD_NAME"]}.distribuidores d WHERE p.id_referencia = r.id AND p.estado = 1 AND d.id = p.distri AND p.id_pos = '$idpos' AND p.fecha_venta BETWEEN '$f_ini' AND '$f_fin' ";
		
		$sql = $sql1." UNION ".$sql2;
		
		$res = $BD->consultar($sql);
	    $con = 0;
		if($BD->numreg($res)>0){
				while(!$res ->EOF){
					$con++;
					$id_asesor = $res->fields['id_vendedor'];
					$id_distri = $res->fields['distri'];
					$db_corto = $res->fields['n_db'];
					$nm_db = $GLOBALS["BD_DIS"].$db_corto.'_'.$id_distri;
					$db = $nm_db.'.usuarios';
					
					$sq2 = "SELECT CONCAT(nombre,' ',apellido) nom_as FROM $db WHERE id = '$id_asesor' ";
					$r2 = $BD->consultar($sq2);
					$nom_as = $r2->fields['nom_as'];
					$array_response[] = array('referencia' => $res->fields['producto'], 'serie' => $res->fields['iccid'].".", 'movil' =>"N/A", 'f_compra' => $res->fields['fecha_venta'], 'distri' => $res->fields['n_distri'], 'asesor' => $nom_as, 'comisionado' => $res->fields['comisionado'], 'tipo' => $res->fields['tipo']);
					$res ->MoveNext();
				}
				return $array_response;
		}else{
			return "";	
		}
		
   }
   
}// Fin clase
?>