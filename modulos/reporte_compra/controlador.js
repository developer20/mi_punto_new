$(document).ready(function() {
	$('.fecha').datepicker({
		format: 'yyyy-mm-dd',
		todayHighlight: true,
		todayBtn: true
	});

	$("#frmConsulta").submit(function(event) {
	 	event.preventDefault();
	 	Cargar_Reporte();
    });

    
});

function Cargar_Reporte(){
	
	 tipo = 1;
	 f_ini = $("#f_ini").val();
	 f_fin = $("#f_fin").val();
   if ( ! $.fn.DataTable.isDataTable( '#t_reporte' ) ) {
	  dtable = $("#t_reporte").DataTable({
			"ajax": {
		    "url": "modulos/reporte_compra/controlador.php",
		    "type": "POST",
		    "deferRender": false,
		    "data":{accion:'Cargar_Informe',f_ini:f_ini, f_fin:f_fin,tipo:tipo}
		  },
		  "bFilter": false,
		  "dom": '<"top"fl>rt<"bottom"<"toolbar">p><"clear">',
		  "responsive":true,
		  "columns": [
		  	{ "data": "tipo"},
            { "data": "producto"},
			{ "data": "cantidad"},
			{ "data": "id_ref"}
        ],
         "columnDefs": [
			{
				"targets": 3,
				"data": "id_ref",
				 render: function ( data, type, row ) {
					if (typeof(row[0]) !== "undefined") {
						cant_total = row[0];
						$("div.toolbar").html('Total Articulos: '+cant_total).addClass("text-left");
					}
					return '<i class="glyphicon glyphicon-plus-sign DetaRef" id = "id_ref_'+row.id_ref+'" style="color: #10628A;font-size: 20px;cursor: pointer;position:relative;"></i>';
				 }
				 
			}
		 ],
	    fnDrawCallback: function () {
			
	    	$(".DetaRef").unbind("click");
	    	$(".DetaRef").click(function() {
				
	    		var data = dtable.row( $(this).parents('tr') ).data();
				var id_ref = data['id_ref'];
				var elemento = $(this);
				
				var tr = $(this).closest('tr');
				var row = dtable.row( tr );
				var tabla = "";
				
				if(elemento.hasClass( "DetaRef" )){
					
					elemento.attr("class","glyphicon glyphicon-minus-sign CloseDeta");

					$.post('modulos/reporte_compra/controlador.php',
						{
							accion: 'Consultar_Ases',
							id_ref:id_ref,
							f_ini : f_ini,
							f_fin : f_fin
							
						},
						function(data, textStatus) {
							if(data != 0 || data != "")
							{	
							
								var datos = JSON.parse(data);
								if(datos != ""){
									var cont = 0;
									tabla += '<table id="tblPed2" align="center" class="table dt-responsive nowrap" cellspacing="0" width="100%" style="border: 1px solid #DADADA;">';
									tabla += '<thead>';
									tabla += '<tr>';
									tabla += '<th align="center">Nº</th>';
									tabla += '<th align="center">Distribuidor</th>';
									tabla += '<th align="center">Asesor</th>';
									tabla += '<th align="center">Cantidad</th>';
									tabla += '<th align="center">Detalle</th>';
									tabla += '</tr>';
									tabla += '</thead>';
									$.each(datos,function(index, fila) {
										cont++;
										tabla += '<tr>';
										tabla += '<td >'+cont+'</td>';
										tabla += '<td >'+fila.nm_distri+'</td>';
										tabla += '<td>'+fila.nm_ase+'</td>';
										
										tabla += '<td>'+fila.cantidad+'</td>';
										tabla += '<td><i class="glyphicon glyphicon-plus-sign" style="color: #10628A;font-size: 20px;cursor: pointer;" id = "boton_de'+id_ref+'_'+fila.id_ase+'_'+fila.id_distri+'" class="viewDetaSerie" onclick = "viewDetaSerie('+id_ref+','+fila.id_ase+','+fila.id_distri+')"></i></td>';
										
										tabla += '</tr>';
										tabla += '<tr><td colspan = "7" id = "posDeta_'+id_ref+'_'+fila.id_ase+'_'+fila.id_distri+'" class = "desDetaSerie" ></td></tr>';
									});
									
									tabla += '</table>';
									
								}
								
								row.child(tabla).show();
								tr.addClass('shown');
							}
						}
					);
				
				}else{
				
					elemento.attr("class","glyphicon glyphicon-plus-sign DetaRef");
					row.child.hide();
					tr.removeClass('shown');
				}
				
	    	});
			
	    }

	  });
	}else{
		dtable.destroy();
		Cargar_Reporte();
	}
	
	$("#CSV").unbind("click");
	$("#CSV").click(function() {
		Datos_CSV();
	});
	$("#consulta").show();
	
}

function viewDetaSerie(id_ref,id_ase,id_distri){
	
	var cont = 0;
	var tabla = "";
	var elemento = $('#posDeta_'+id_ref+'_'+id_ase+'_'+id_distri);
	var icono = $('#boton_de'+id_ref+'_'+id_ase+'_'+id_distri);
	f_ini = $("#f_ini").val();
	 f_fin = $("#f_fin").val();
	
	if(elemento.hasClass( "desDetaSerie" )){
	
		icono.attr("class","glyphicon glyphicon-minus-sign");
		elemento.attr("class","CloseDetaSerie");
		elemento.show();
		$.post('modulos/reporte_compra/controlador.php',
			{
				accion: 'Consultar_Serie',
				id_ref:id_ref,
				id_ase:id_ase,
				id_distri: id_distri,
				f_ini: f_ini,
				f_fin: f_fin
				
			},
			function(data, textStatus) {
				if(data != 0 || data != "")
				{	
					var datos = JSON.parse(data);
					
					tabla += '<table id="tblPed2" align="center" class="table dt-responsive nowrap" cellspacing="0" width="100%" style="border: 1px solid #DADADA;">';
					tabla += '<thead>';
					tabla += '<tr>';
					tabla += '<th style="text-align: center;" width="25%">Nº</th>';
					tabla += '<th style="text-align: center;" width="25%">Fecha </th>';
					tabla += '<th style="text-align: center;" width="25%">Serial/Imei</th>';
					tabla += '<th style="text-align: center;" width="25%">Numero Linea</th>';
					tabla += '<th style="text-align: center;" width="25%">Comisión</th>';
					tabla += '</tr>';
					tabla += '</thead>';
					$.each(datos,function(index, fila) {
						cont++;
						tabla += '<tr>';
						tabla += '<td style="text-align: center;">'+cont+'</td>';
						tabla += '<td style="text-align: center;">'+fila.f_venta+'</td>';
						tabla += '<td style="text-align: center;">'+fila.serie+'</td>';
						
						if(fila.nro_movil == 0 || fila.nro_movil == undefined ){
							tabla += '<td style="text-align: center;">N/A</td>';
						}else{
							tabla += '<td style="text-align: center;">'+fila.movil+'</td>';
						}
						tabla += '<td style="text-align: center;">'+fila.comisionado+'</td>';
						tabla += '</tr>';
					});
					tabla += '</table>';
					elemento.html(tabla);
					
				}
			}
		);
		
	}else{
		
		icono.attr("class","glyphicon glyphicon-plus-sign");
		elemento.attr("class","desDetaSerie");
		elemento.hide();
		elemento.html("");
		
	}
	
}

function Datos_CSV() {
	
	f_ini = $("#f_ini").val();
	 f_fin = $("#f_fin").val();
	
	$("#CSV").unbind("click");
	$.post('modulos/reporte_compra/controlador.php',
	{
		accion: 'Datos_CSV',
		f_ini: f_ini,
		f_fin: f_fin
	},
	function(data, textStatus) {
		if(data != ""){
			data = JSON.parse(data);
			var header = "TIPO,REFERENCIA,SERIAL/IMEI,COMISION,NUMERO LINEA,FECHA_COMPRA,DISTRIBUIDOR,ASESOR";
			var columnas = "tipo,referencia,serie,comisionado,movil,f_compra,distri,asesor";
			ExportarCSV(data, header,"Reporte de Compras",columnas);
			
			$("#CSV").click(function() {
				Datos_CSV();
			});
		}
	});
	
}

	
	

