<?php

include_once("../../config/mainController.php"); // Incluye el Controlador Principal
include_once("../../modulos/comisiones/modelo.php");	// Incluye el Modelo.
$controller = new mainController; // Instancia a la clase MainController
$modelo = new comisiones(); // Instancia a la clase del modelo

try // Try, manejo de Errores
{
	$metodo = $_SERVER['REQUEST_METHOD'];
	$tipo_res = "";
	$response = null; 
	// Se manejaran dos tipos JSON y HTML
	// Dependiendo del método de la petición ejecutaremos la acción correspondiente.
	// Por ahora solo POST, todas las llamadas se haran por POST
	$variables = $_POST;
	if(!isset($_POST['accion'])){echo "0"; return;} // Evita que ocurra un error si no manda accion.
	$accion = $variables['accion'];
	// Dependiendo de la accion se ejecutaran las tareas y se definira el tipo de respuesta.
	switch($accion) {
		case 'comi_pen':
			 $tipo_res = 'JSON'; //Definir tipo de respuesta;
			 $array = $modelo->comisiones_pendientes();
			 $response = array("draw" => 1,
								   "recordsTotal" => 0,
								   "recordsFiltered" => 0,
								   "data" => $array);
		break;
		case 'generar_code':
			 $tipo_res = 'JSON'; //Definir tipo de respuesta;
			 $id_comi = $variables['id_comi'];
			 $response = $modelo->generar_codigo($id_comi);
		break;
	}

	// Respuestas del Controlador
	if($tipo_res == "JSON")
	{
	  echo json_encode($response,true); // $response será un array con los datos de nuestra respuesta.
	}
	elseif ($tipo_res == "HTML") {
	  echo $response; // $response será un html con el string de nuestra respuesta.
	}
} // Fin Try
catch (Exception $e) {}



