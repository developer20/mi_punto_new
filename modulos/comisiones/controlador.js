var dtable = "";
var dtable2 = "";
$(document).ready(function() {

	$("#frmPendientes").submit(function(event) {
	 	event.preventDefault();
	 	Cargar_comisiones_pen();
    });
	
});

function Cargar_comisiones_pen(){
	
   var cont = 0;	
	
   if ( ! $.fn.DataTable.isDataTable( '#t_reporte' ) ) {
	  dtable = $("#t_reporte").DataTable({
			"ajax": {
			"url": "modulos/comisiones/controlador.php",
			"type": "POST",
			"deferRender": false,
			"data":{accion:'comi_pen'}
		  },
		  "bFilter": false,
		  "responsive":false,
		  "columns": [
			{ "data": "cant_acti"},
			{ "data": "per_comi"},
			{ "data": "t_comi"},
			{ "data": "sal_pen"},
			{ "data": "code_comi"},
			{ "data": "fecha_vigencia"}
		],
		 "columnDefs": [
			{
				"targets": 0,
				"data": "",
				 render: function ( data, type, row ) {
					
					cont++;
					
					return cont;
				 }
				 
			},
			{
				"targets": 4,
				"data": "",
				 render: function ( data, type, row ) {
					
					var boton = ""
					
					if(row.code_comi == ""){
					
						boton = '<button type="buttom" name="generar" id="generar" class="btn btn-primary generar_code">Generar</button>';
					
					}else{
						boton = "<div id='codigo_"+row.id+"'>"+data+"</div>";	
					}
					
					return boton;
				 }
				 
			}
			
		 ],
		 fnDrawCallback: function () { 
			
			   $('.generar_code').unbind("click");
	           $('.generar_code').click(function () {
					var data = dtable.row( $(this).parents('tr') ).data();	
					var id_comi = data['id'];
					
					BootstrapDialog.confirm("¿Está seguro de generar el código?", function(result){
					if(result) {
							$.post('modulos/comisiones/controlador.php',
							{
								accion: 'generar_code',
								id_comi: id_comi
							},
							function(data, textStatus) {
							
								if(data == 1){
									
									Notificacion("Se ha generado el código con éxito","success");
									Cargar_comisiones_pen();
									
								}
								else if(data == 0)
								{
									Notificacion("Error al generar el código","error");	
								
								}
							}
							);	
						}
					})
				});
	        }
	  });
	  $("#consulta").show();
	}else{
		dtable.destroy();
		Cargar_comisiones_pen();
	}
		
}