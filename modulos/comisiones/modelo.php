<?php
require("../../config/mainModel.php");
class comisiones{
   
   public function comisiones_pendientes()
   {
		$oSession = unserialize($_SESSION[$GLOBALS["SESION_POS"]]);
		$idpos=$oSession->VSidpos;
		
		$BD=new BD();
		$BD->conectar();
		
		$array_response = array();
		
		$sqlconf = "select estado_codigo_comi from {$GLOBALS["BD_NAME"]}.configuracion_general where id = 1";
		$redcon = $BD->consultar($sqlconf);
		$estado_codigo = intval($redcon->fields["estado_codigo_comi"]); 
			
		$sql = "SELECT id,cod_distri distri_code,periodo per_comi,total_comision t_comi,saldo_pendiente sal_pen FROM {$GLOBALS["BD_NAME"]}.comisiones_puntos WHERE idpos = $idpos and saldo_pendiente > 0";
		$res = $BD->consultar($sql);
		
		while(!$res->EOF){
			$fecha_actual=date("Y-m-d H:i:s");
			$fecha_vigencia = "";
			$codigo = "";
			
			$sql = "SELECT codigo,fecha_limite,hora_limite FROM {$GLOBALS["BD_NAME"]}.codigos_pago_comi WHERE id_comi_punto = ".$res->fields["id"]." and idpos = $idpos and estado = 0 and concat(fecha_limite,' ',hora_limite) >= '$fecha_actual'";
			$res_code = $BD->consultar($sql);
			
			if($BD->numreg($res_code)>0){		
				$fecha_limite = $res_code->fields['fecha_limite'];
				$hora_limite = $res_code->fields['hora_limite'];
				$fecha_vigencia = $fecha_limite." - ".$hora_limite;
				$codigo = $res_code->fields['codigo'];
				
			}
			
			if($estado_codigo == 0){
			   $fecha_vigencia = "N/A";
			   $codigo = "N/A";
			}
			
			$array_response[] = array("id" => $res->fields["id"],"distri_code" => $res->fields["distri_code"],"per_comi" => $res->fields["per_comi"],"t_comi" => number_format($res->fields["t_comi"],0),"sal_pen" => number_format($res->fields["sal_pen"],0),"code_comi" => $codigo,"fecha_vigencia" => $fecha_vigencia);
		
			$res->MoveNext();	
		}
		
		return $array_response;
   }
   
   public function generar_codigo($id_comi)
   {
	    $oSession = unserialize($_SESSION[$GLOBALS["SESION_POS"]]);
	    $idpos=$oSession->VSidpos;
		
		$BD=new BD();
	    $BD->conectar();
		
		$error = 0;
		
		$cadena = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
		$longitudCadena=strlen($cadena);
		$code = "";
		for($i=1 ; $i<=6 ; $i++){
			//Definimos numero aleatorio entre 0 y la longitud de la cadena de caracteres-1
			$pos=rand(0,$longitudCadena-1);
			//Vamos formando la contrase–a en cada iteraccion del bucle, a–adiendo a la cadena $pass la letra correspondiente a la posicion $pos en la cadena de caracteres definida.
			$code.= substr($cadena,$pos,1);
		}
		
		$sql = "SELECT horas FROM {$GLOBALS["BD_NAME"]}.vigencia_pago_comi";
		$res_vige = $BD->consultar($sql);
		
		$hr = $res_vige->fields['horas'];
		$fecha = date("Y-m-d");
		$hora = date("H:i:s");
		
		$tiempo_limite = date("Y-m-d H:i:s", strtotime($fecha.$hora." + $hr hour"));
		$tiempo_limite = explode(" ",$tiempo_limite);
		
		$fecha_limite = $tiempo_limite[0];
		$hora_limite = $tiempo_limite[1];
		
		$sql = "INSERT INTO {$GLOBALS["BD_NAME"]}.codigos_pago_comi (id_comi_punto,codigo,fecha,hora,fecha_limite,hora_limite,idpos) VALUES ($id_comi,'$code',curdate(),curtime(),'$fecha_limite','$hora_limite',$idpos)";
		$res = $BD->consultar($sql);
		
		if(!$res){
			$error = 1;
		}	
		
		$fecha_actual=date("Y-m-d H:i:s");
		
		$sql = "UPDATE {$GLOBALS["BD_NAME"]}.codigos_pago_comi SET estado = 1 WHERE id_comi_punto = $id_comi and concat(fecha_limite,' ',hora_limite) < '$fecha_actual'";
		$res = $BD->consultar($sql);
		
		if(!$res){
			$error = 1;
		}
		
		if($error==1){
			$BD->consultar("ROLLBACK");
			return 0;
		}elseif($error==0){
			$BD->consultar("COMMIT");
			return 1;
		}
		
   }
   
   
   
}// Fin clase
?>