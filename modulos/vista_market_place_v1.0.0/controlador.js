var imgCont_edit = 0;
var imageContainer = [];
$(".box-header").remove();
$(".content-header").remove();

$(document).ready(function () {
  cargar_paginador(1);
  cargar_slider();
});

function EjecutarBusqueda(e) {
  if (e.keyCode == 13) {
    cargar_paginador(1);
  }
}


function cargar_slider() {

  $.post('modulos/vista_market_place_v1.0.0/controlador.php',
    {
      accion: 'productos_distribuidor'
    },
    function (data) {
      data = JSON.parse(data);
      var html_contenido_pag = "";
      var active;
      $("#total_pro").html("");

      if (!$.isEmptyObject(data)) {
        //console.log(data[0].total_registros);
        $("#total_pro").html("Cantidad: " + data[0].total_registros);

        // contenido pagina a la que voy a vizualizar, se vizualizan 4 items por rows
        var salto = 5; // porque cada 4 items creo otro row
        for (var i = 1; i <= data.length; i++) {
          active = (i == 1) ? "active" : "";
          /// cierra el row
          if (salto == 0) {
            html_contenido_pag += "</div></div>";
          }
          // crear un row
          if (i == 1 || salto == 0) {
            html_contenido_pag += "<div class='item " + active + " '><div class='row'>";
            salto = 4;
          }
          // contenido del items 
          sku = data[i - 1].producto.substring(0, 17);
          //src='static/img/publicaciones/"+data[i-1].imagen+"' 
          var ruta_imagen = (data[i - 1].imagen == "") ? "static/img/not_available.png" : "./../claro/distribuidor/static/img/referencias/productos/" + data[i - 1].imagen + " ";
          html_contenido_pag += "<div class='col-md-3 contenedor_producto' ><a href='#' class='thumbnail' style='overflow: hidden;' ><img src='" + ruta_imagen + "' style='max-width:80%;max-height: 180px;min-height: 180px'><i data-toggle='tooltip' onclick='agregar_carrito(" + data[i - 1].id + ")' title='Añadir al Carrito' class='fas fa-cart-plus' style='color: black;font-size: 29px;position: absolute;margin-top: -30px;right: 30px;'></i><hr style='margin-top: 5px;margin-bottom: 5px;'><div class='flex-container'><div style='width: 100%;'><p style='color:black;font-size: 18px;margin-bottom: 2px;'>$ " + formato_numero(data[i - 1].valor_referencia, 0, ',', '.') + "</p></div></div><div class='flex-container'><div style='width: 100%;'><p style='color: #777;font-size: 14px;margin: 0 0 5px;' >" + sku + "</p></div></div></a></div>";
          salto = salto - 1;
        }

        // cierro el ultimo row

        $(".carousel-inner").html(html_contenido_pag);
        $('[data-toggle="tooltip"]').tooltip();
      } else {
      }
    }
  );
}

function cargar_paginador(pagina) {

  var items_por_pagina = 8;
  var numero_pagina = pagina;
  var buscar = $("#txt_buscar").val().trim();
  $("#conte_pagina > div").remove();

  $.post('modulos/vista_market_place_v1.0.0/controlador.php',
    {
      accion: 'cargar_contenido_paginador', items_por_pagina: items_por_pagina, numero_pagina: numero_pagina, buscar: buscar
    },
    function (data) {
      data = JSON.parse(data);
      var html_paginador = "";
      var html_contenido_pag = "";
      var active, titulo_publi, fecha_publi;

      if (!$.isEmptyObject(data)) {
        // paginador
        for (var i = 1; i <= parseInt(data[0].num_paginador); i++) {
          var active = (i == pagina) ? "active" : "";
          html_paginador += " <li class='page-item " + active + "'><a class='page-link' onclick='cargar_paginador(" + i + ")' tabindex='-1'>" + i + "</a></li>";
        }

        $("#paginador").html(html_paginador);

        // contenido pagina a la que voy a vizualizar, se vizualizan 4 items por rows
        for (var i = 1; i <= data.length; i++) {
          /// cierra el row
          if (i == 5) {
            html_contenido_pag += "</div>";
          }
          // crear un row
          if (i == 1 || i == 5) {
            html_contenido_pag += "<div class='row animate__animated animate__zoomIn' style='padding: 10px;'>";
          }
          // contenido del items 
          titulo_publi = data[i - 1].titulo.substring(0, 20);
          fecha_publi = data[i - 1].fecha_pub.substring(0, 6);

          //src='static/img/publicaciones/"+data[i-1].imagen+"' 
          html_contenido_pag += "<div class='col-md-3 contenedor_producto'><a href='javascript:void(0);' style='overflow: hidden;' onclick='ver_detalle(" + data[i - 1].id_publicacion + ")' class='thumbnail' ><img src='static/img/referencias/publicaciones/" + data[i - 1].imagen + "' style='max-width: 175px;min-height: 180px;'><hr style='margin-top: 5px;margin-bottom: 5px;'><div class='flex-container'><div style='width: 100%;'><p style='color:black;font-size: 20px;margin-bottom: 0px;'>$ " + formato_numero(data[i - 1].precio, 0, ',', '.') + "</p></div></div><div class='flex-container'><div style='width: 100%;'><p style='color: #777;font-size: 16px;margin: 0 0 5px;'>" + titulo_publi + "</p></div></div><div class='flex-container'><div style='width: 80%;'><p style='color: #777;font-size: 12px;margin-bottom: 2px;'>" + data[i - 1].ciudad + "</p></div><div style='width: 20%;'><p style='color: #777;font-size: 12px;margin-bottom: 2px;float: right'>" + fecha_publi + "</p></div></div></a></div>";

        }

        // cierro el ultimo row
        html_contenido_pag += "</div>";
        $("#conte_pagina").append(html_contenido_pag);
      } else {
        $("#paginador li").remove();
        $("#conte_pagina").append("<div class='row' style='padding: 10px;text-align:center'><h1 style='color:red' class='animate__animated animate__zoomInDown'>No se encontrarón datos</h1></div>")
      }
    }
  );

}


function ver_detalle(id) {


  $.post('modulos/vista_market_place_v1.0.0/controlador.php', { accion: 'obtener_datos_edit', id: id },
    function (data) {
      data = JSON.parse(data);

      $("#modal-edit").modal("show");
      $("#imgLarge-edit img").remove();

      $("#id_update").val(data[0]["id"]);
      $(".product-title-edit").val(data[0]["titulo"]);
      $(".product-title-edit").html(data[0]["titulo"]);
      $(".product-price-edit").html("$ " + formato_numero(data[0]["precio"], 0, ',', '.'));
      $("#marca-prod-edit").html(data[0]["marca"]);
      $(".description-content-edit").html(data[0]["descripcion"]);
      $("#estado-prod-edit").html(data[0]["estado_prod"]);
      $(".product-time").html("Publicado hace " + data[0]["fecha_pub"] + " días");
      cargar_vendedor_edit(data[0]["id"]);
      setLocation_edit(data[0]["latitud"], data[0]["longitud"]);
      cargar_imagenes(data[0]["id"]);
      // $(".leaflet-map-pane").css("right",0);

    });

}

function cargar_imagenes(id) {

  $(".gallery-body-edit").remove();
  $(".gallerySlide-edit").remove();
  imgCont_edit = 0;

  $.post('modulos/vista_market_place_v1.0.0/controlador.php',
    {
      accion: 'cargar_imagenes',
      id: id
    },
    function (data) {
      data = JSON.parse(data);

      $.each(data, function (index, row) {
        imgCont_edit++;
        var imgSmall = $('<div class="gallery-body-edit">  <img src="static/img/referencias/publicaciones/' + row.nombre + '" alt="Foto del usuario" class="selectImg-edit selectImg-opacityx selectImg-hover-opacity-off" onclick="currentDiv_edit(' + imgCont_edit + ')" style="width:100%;height:100%;border: 2px solid #000;cursor:pointer;border-radius: 10px;">  </div>');
        if (row.principal == 1) {
          var imgLarge = $('<img src="static/img/referencias/publicaciones/' + row.nombre + '" alt="Foto del usuario" class="image-preview-edit gallerySlide-edit" style="width:100%; display:block">');
          $("#not_img_edit").hide();
        } else {
          var imgLarge = $('<img src="static/img/referencias/publicaciones/' + row.nombre + '" alt="Foto del usuario" class="image-preview-edit gallerySlide-edit" style="width:100%; display:none">');
          $("#not_img_edit").hide();
        }

        $(imgSmall).insertBefore("#add-photo-container-edit");
        $(imgLarge).insertBefore("#imgLarge-edit");
        $("#not_img_edit").hide();
        $("#cargadas-edit").html(imgCont_edit + " de 4");
      });
    });
}

function currentDiv_edit(n) {
  showDivs_edit(slideIndex = n);
}

function showDivs_edit(n) {
  var i;
  var x = document.getElementsByClassName("gallerySlide-edit");
  var dots = document.getElementsByClassName("selectImg-edit");
  if (n > x.length) { slideIndex = 1 }
  if (n < 1) { slideIndex = x.length }
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
    dots[i].className = dots[i].className.replace(" selectImg-opacity-off", "");
  }
  x[slideIndex - 1].style.display = "block";
  dots[slideIndex - 1].className += " selectImg-opacity-off";
}

function cargar_vendedor_edit(id) {

  $.post('modulos/vista_market_place_v1.0.0/controlador.php',
    {
      accion: 'cargar_vendedor_edit',
      id: id
    },
    function (data) {
      datos = JSON.parse(data);
      var data = datos['datos'];
      var usuario = datos['usuario'];
      var id_pos_contacto = data[0]["idpos"];

      if (usuario == id_pos_contacto) {
        $("#btn_modal_contacto").prop("disabled", true);
      } else {
        $("#btn_modal_contacto").prop("disabled", false);
      }

      $("#nombre-pdv-edit").html(" " + data[0]["nombre_pdv"]);
      $("#nombre-vendedor-edit").html(" " + data[0]["nombre"]);
    });
}


function formato_numero(numero, decimales, separador_decimal, separador_miles) {

  numero = parseFloat(numero);
  if (isNaN(numero)) {
    return '';
  }

  if (decimales !== undefined) {
    numero = numero.toFixed(decimales); //Redondeamos
  }

  //Convertimos el punto en separador_decimal
  numero = numero.toString().replace('.', separador_decimal !== undefined ? separador_decimal : ',');
  if (separador_miles) {
    // Añadimos los separadores de miles
    var miles = new RegExp('(-?[0-9]+)([0-9]{3})');
    while (miles.test(numero)) {
      numero = numero.replace(miles, '$1' + separador_miles + '$2');
    }
  }
  return numero;

}

// VALIDACIONES MAPS EDITAR//

const DEFAULT_COORD_EDIT = [6.2443382, -75.573553]
const resultsWrapperHTML_edit = document.getElementById("search-result-edit");
const Map_edit = L.map("render-map-edit");

const osmTileUrl_edit = "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png";

const osmTile_edit = new L.TileLayer(osmTileUrl_edit, { minZoom: 2, maxZoom: 20 });

Map_edit.setView(new L.LatLng(DEFAULT_COORD_EDIT[0], DEFAULT_COORD_EDIT[1]), 12);
Map_edit.addLayer(osmTile_edit);

const Marker_edit = L.marker(DEFAULT_COORD_EDIT).addTo(Map_edit)

function setLocation_edit(lat, lon) {
  $("#lat_edit").val(lat);
  $("#lon_edit").val(lon);

  Map_edit.setView(new L.LatLng(lat, lon), 12);
  Marker_edit.setLatLng([lat, lon]);
}


function modal_contacto() {

  $('#modal_contacto').modal({ backdrop: 'static', keyboard: false });
  $("#modal_contacto").modal("show");
  $("#modal_contacto").show();

  var titulo = $(".product-title-edit").val();

  $.post('modulos/vista_market_place_v1.0.0/controlador.php',
    {
      accion: 'cargar_vendedor'
    },
    function (data) {
      data = JSON.parse(data);

      $("#id-pdv-contacto").html(" " + data[0]["idpos"]);
      $("#id-pdv-contacto").val(data[0]["idpos"]);
      $("#nombre-pdv-contacto").html(" " + data[0]["nombre_pdv"]);
      $("#nombre-pdv-contacto").val(data[0]["nombre_pdv"]);
      $("#nombre-encargado-contacto").html(" " + data[0]["nombre"]);
      $("#nombre-encargado-contacto").val(data[0]["nombre"]);
      $("#telefono-contacto").html(" " + data[0]["telefono"]);
      $("#telefono-contacto").val(data[0]["telefono"]);
      $("#celular-contacto").html(" " + data[0]["celular"]);
      $("#celular-contacto").val(data[0]["celular"]);
      $("#descripcion-pub").html("Estoy interesado en tu publicación de <strong>" + titulo + "</strong>, mis datos de contacto son los siguientes:");
      var lat_contacto = data[0]["latitud"];
      var lon_contacto = data[0]["longitud"];

      // VALIDACIONES MAPS CONTACTO//

      const DEFAULT_COORD = [lat_contacto, lon_contacto]
      const Map = L.map("render-map-contacto");
      const osmTileUrl = "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png";
      const osmTile = new L.TileLayer(osmTileUrl, { minZoom: 2, maxZoom: 20 });
      Map.setView(new L.LatLng(DEFAULT_COORD[0], DEFAULT_COORD[1]), 12);
      Map.addLayer(osmTile);
      L.marker(DEFAULT_COORD).addTo(Map)
    });
}

function cerrar_modal_contacto() {

  $('#modal_contacto').modal({ backdrop: 'static', keyboard: true });
  $("#modal_contacto").hide();
  return false;
}

function guardar_contacto() {

  var id_pdv = $("#id-pdv-contacto").val();
  var nombre = $("#nombre-pdv-contacto").val();
  var nom_encargado = $("#nombre-encargado-contacto").val();
  var telefono = $("#telefono-contacto").val();
  var celular = $("#celular-contacto").val();
  var ter_cond = $("#ter_cond").val();  
  var id_update = $("#id_update").val();
  var accion = "guardar_contacto";

  if (!$('#ter_cond').prop('checked')) {
    Notificacion("Los terminos y condiciones son obligatorios", "warning");
    return false;
  }

  $.ajax({
    type: "POST",
    url: 'modulos/vista_market_place_v1.0.0/controlador.php',
    data: ('id_pdv=' + id_pdv +
      '&nombre=' + nombre +
      '&nom_encargado=' + nom_encargado +
      '&telefono=' + telefono +
      '&celular=' + celular +
      '&ter_cond=' + ter_cond +
      '&id_update=' + id_update +
      '&accion=' + accion + ' '),
    cache: false,
    beforeSend: function () {
      $("#btn_guardar_cont").prop("disabled", true);
    },
    success: function (respuesta) {
      if (respuesta == "0") {
        $("#btn_guardar_cont").prop("disabled", false);
        Notificacion("Se enviado la solicitud correctamente", "success");
        cerrar_modal_contacto();
        return false;
      } else if (respuesta == "2") {
        $("#btn_guardar_cont").prop("disabled", false);
        Notificacion("La solicitud ya se realizo anteriormente", "danger");
        return false;
      } else {
        Notificacion("Error al enviar la solicitud", "danger");
        return false;
      }
    }
  });
}