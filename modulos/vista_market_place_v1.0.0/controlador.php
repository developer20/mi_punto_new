<?php

include_once("../../config/mainController.php"); // Incluye el Controlador Principal
include_once("../../modulos/vista_market_place_v1.0.0/modelo.php");	// Incluye el Modelo.
$controller = new mainController; // Instancia a la clase MainController
$modelo = new vista_market_place(); // Instancia a la clase del modelo

try // Try, manejo de Errores
{
	$metodo = $_SERVER['REQUEST_METHOD'];
	$tipo_res = "";
	$response = null; 
	// Se manejaran dos tipos JSON y HTML
	// Dependiendo del método de la petición ejecutaremos la acción correspondiente.
	// Por ahora solo POST, todas las llamadas se haran por POST
	$variables = $_POST;
	if(!isset($_POST['accion'])){echo "0"; return;} // Evita que ocurra un error si no manda accion.
	$accion = trim($variables['accion']);
	// Dependiendo de la accion se ejecutaran las tareas y se definira el tipo de respuesta.
	switch($accion) {		

		case 'cargar_contenido_paginador':
			$tipo_res = 'JSON'; //Definir tipo de respuesta
			$items_por_pagina = $variables['items_por_pagina'];
			$numero_pagina = $variables['numero_pagina'];
			$buscar = $variables['buscar'];
			$response = $modelo->cargar_contenido_paginador($items_por_pagina,$numero_pagina,$buscar);
		break;
		
		case 'productos_distribuidor':
            $tipo_res = 'JSON'; //Definir tipo de respuesta
			$response = $modelo->productos_distribuidor();

		break;

		case "obtener_datos_edit":
			$tipo_res = "JSON";
			$id = $variables["id"];
			$response = $modelo->obtener_datos_edit($id);
		break;

		case 'cargar_vendedor_edit': 
				$tipo_res = 'JSON'; //Definir tipo de respuesta;
				$id = $variables["id"];
				$response = $modelo->cargar_vendedor_edit($id);
		break;
	   
		case 'cargar_imagenes': 
				$tipo_res = 'JSON'; //Definir tipo de respuesta;
				$id = $variables["id"];
				$response = $modelo->cargar_imagenes($id);
		break;

		case 'guardar_contacto':
				$tipo_res = 'JSON';
				$id_pdv = $variables['id_pdv'];
				$nombre = $variables['nombre'];
				$nom_encargado = $variables['nom_encargado'];
				$telefono = $variables['telefono'];
				$celular = $variables['celular'];
				$ter_cond = $variables['ter_cond'];
				$id_update = $variables['id_update'];
				$response = $modelo->guardar_contacto($id_pdv,$nombre,$nom_encargado,$telefono,$celular,$ter_cond,$id_update);
          break;

		case 'consultar_notificacion':
				$tipo_res = 'JSON'; //Definir tipo de respuesta;
				$response = $modelo->consultar_notificacion();
		break;

		case 'leer_notificacion':
				$tipo_res = 'HTML'; //Definir tipo de respuesta;
				$id_notif = $variables['id_notif'];
				$response = $modelo->leer_notificacion($id_notif);
		break;

		case 'eliminar_solicitud':
				$tipo_res = 'JSON';
				$id = $variables['id'];
				$response = $modelo->eliminar_solicitud($id);
		break;

		case 'validar_usuario': 
				$tipo_res = 'JSON'; //Definir tipo de respuesta;
				$response = $modelo->validar_usuario();
		break;

		case 'cargar_vendedor': 
				$tipo_res = 'JSON'; //Definir tipo de respuesta;
				$response = $modelo->cargar_vendedor();
		break; 	   
	}	

	// Respuestas del Controlador
	if($tipo_res == "JSON")
	{
	  echo json_encode($response,true); // $response será un array con los datos de nuestra respuesta.
	}
	elseif ($tipo_res == "HTML") {
	  echo $response; // $response será un html con el string de nuestra respuesta.
	}
} // Fin Try
catch (Exception $e) {}