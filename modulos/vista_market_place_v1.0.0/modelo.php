<?php
require("../../config/mainModel.php");
class vista_market_place {
    //Espacio Para declara las funciones que retornan los datos de la DB.
    public function __construct(){
  		$BD=new BD();
  		$this->BD = $BD;
		$this->BD->conectar();
	}
	  
    public function __destruct(){
    	$this->BD->desconectar();
	}

	public function cargar_contenido_paginador($items_por_pagina,$numero_pagina,$buscador){

		$oSession = unserialize($_SESSION[$GLOBALS["SESION_POS"]]);
		$idDistri = $oSession->VSid_distri;
		$punto  =  $oSession->VSidpos;
		
		if($numero_pagina !=  1){
		    $desde = ($items_por_pagina * ($numero_pagina - 1));
		}else{
			$desde = 0;
		}

		$where = "";
		if($buscador != ""){
			$where.=" AND pu.titulo LIKE '%$buscador%' OR pu.palabras LIKE '%$buscador%' ";
		}


			  $sql_dis = "SELECT nombre_corto FROM {$GLOBALS["BD_NAME"]}.distribuidores WHERE id = $idDistri";
		      $query_dis = $this->BD->devolver_array($sql_dis);
		      $nombre_corto = $query_dis[0]["nombre_corto"];
			  $bd = "{$GLOBALS["BD_DIS"]}" . $nombre_corto . "_" . $idDistri;
			  
			// total registro
			 $sql_total = "SELECT COUNT(*) AS cantidad FROM {$GLOBALS["BD_POS"]}.crear_productos__mp AS pu WHERE pu.estado = 1 $where";
			 $res_tot = $this->BD->devolver_array($sql_total);
			 $total_reg = $res_tot[0]['cantidad'];

			 // # par ael paginador 
			 $num_paginado = ceil($total_reg / $items_por_pagina);
	
			   $sql = "SELECT 
							pu.id AS id_publicacion,pu.titulo,'falta' AS distribuidor,img.nombre AS imagen,date_format(pu.fecha_inicio,'%d %M') AS fecha_pub,
							pu.precio,$total_reg AS total_registros,pu.ubicacion AS ciudad,$num_paginado AS num_paginador
					FROM
					          {$GLOBALS["BD_POS"]}.crear_productos__mp pu
					LEFT JOIN 
					          {$GLOBALS["BD_POS"]}.imagenes_productos__mp img
							    ON pu.id = img.id_producto AND img.principal = 1
					WHERE 
							pu.estado = 1 $where
					LIMIT   
							$items_por_pagina
					OFFSET
					        $desde";
					
				
				$res = $this->BD->devolver_array($sql);
                return $res;
	 		    
	}


	public function productos_distribuidor(){

		$oSession = unserialize($_SESSION[$GLOBALS["SESION_POS"]]);
		$idDistri = $oSession->VSid_distri;
		$punto  =  $oSession->VSidpos;

		$sql_dis = "SELECT nombre_corto FROM {$GLOBALS["BD_NAME"]}.distribuidores WHERE id = $idDistri";
		$query_dis = $this->BD->devolver_array($sql_dis);
		$nombre_corto = $query_dis[0]["nombre_corto"];
		$bd = "{$GLOBALS["BD_DIS"]}" . $nombre_corto . "_" . $idDistri;

		$sql_to = "SELECT
						COUNT(lis.id) AS total
				  FROM
						$bd.referencias r
				   INNER JOIN			
						$bd.lista_market_place lis
						  on r.id = lis.id_referencia 
				   WHERE 
						lis.estado = 1 ";
     
		$res_total = $this->BD->devolver_array($sql_to);
		$total_reg = $res_total[0]['total'];

		$sql = "SELECT
						r.id AS id_referencia,r.pn,r.producto,COALESCE(img.nombre,'') AS imagen,
						$total_reg AS total_registros,lis.id AS id
				FROM 
				         $bd.referencias r
				INNER JOIN 
				         $bd.lista_market_place lis
						  ON r.id = lis.id_referencia
				LEFT JOIN 
				         $bd.tipo_imagenes_submarcas img
						   ON r.id = img.id_tipo AND img.tipo_submarca = 3 AND 
							img.id IN (SELECT MIN(id) FROM $bd.tipo_imagenes_submarcas WHERE tipo_submarca = 3 AND r.id = id_tipo GROUP BY id_tipo HAVING MIN(id)) 
				WHERE 
						lis.estado = 1 ";
		 
		
		$res = $this->BD->devolver_array($sql);
		
		$datos = array();
		foreach($res as $all_data => $row) {
		   $array = array();
		   $precio_dcs = $this->Valor_ListaP($row['id_referencia']);
		   $array['valor_referencia']  = $precio_dcs[0]['valor_referencia'];
		   $array['id']  = $row['id'];
		   $array['producto']  = $row['producto'];
		   $array['total_registros']  = $row['total_registros'];
		   $array['imagen']        = $row['imagen'];
		   $array['pn']        = $row['pn'];
		   $array['id_referencia'] = $row['id_referencia'];
		   $datos[] = $array;
         
		}

		return $datos;

	}

    public function Valor_ListaP($referencia){

		$oSession = unserialize($_SESSION[$GLOBALS["SESION_POS"]]);
		$id_distri = $oSession->VSid_distri;
		$punto  =  $oSession->VSidpos;
        $lista = true;
		$r_referencia = array();
		
		$sql_dis = "SELECT nombre_corto FROM {$GLOBALS["BD_NAME"]}.distribuidores WHERE id = $id_distri";
		$query_dis = $this->BD->devolver_array($sql_dis);
		$nombre_corto = $query_dis[0]["nombre_corto"];
		$bd = "{$GLOBALS["BD_DIS"]}" . $nombre_corto . "_" . $id_distri;

        $consulta = "SELECT 
		                   idpos, territorio, zona 
					 FROM 
					       {$GLOBALS["BD_POS"]}.puntos 
					 WHERE 
							idpos = '$punto'";
				 
       $datos_punto = $this->BD->devolver_array($consulta);

        if(count($datos_punto)>0){
			
			$datos_reg = null;
			$datos_distri = null;
			$r_consulta_lista = null;
			$tipo_nivel = 3;
			$regional =0;
			// La funcion contiene los niveles de los cuales puede salir el precio de la referencia
			// Consulta para devolver los datos del punto.
            while ($lista) {
              switch ($tipo_nivel) {
				case 3://RUTA
					 $consulta_lista ="SELECT
												d.id_lista
										FROM 
										       $bd.lista_precio_market_place l
										INNER JOIN 
										       $bd.items_lista_market_place d
												ON l.id = d.id_lista
										INNER JOIN 
										       $bd.niveles_listaprecios_market_place n
												ON l.id = n.id_lista
										WHERE
											   d.estado = 1 AND l.estado = 1 AND  l.estado_vigencia=1 AND 
											   n.tipo_nivel = $tipo_nivel AND 
											   d.id_referencia  = $referencia AND 
											   n.id_nivel = ".$datos_punto[0]["zona"]." ";

                     $r_consulta_lista = $this->BD->devolver_array($consulta_lista);

					if(count($r_consulta_lista) > 0){
					    $lista = false;
					}else{
					    $tipo_nivel--;
					}

          break;
          case 2://CIRCUITO
			$consulta_lista ="SELECT
									d.id_lista
							FROM 
								$bd.lista_precio_market_place l
							INNER JOIN 
								$bd.items_lista_market_place d
									ON l.id = d.id_lista
							INNER JOIN 
								$bd.niveles_listaprecios_market_place n
									ON l.id = n.id_lista
							WHERE
								d.estado = 1 AND l.estado = 1 AND  l.estado_vigencia=1 AND 
								n.tipo_nivel = $tipo_nivel AND 
								d.id_referencia  = $referencia AND 
								n.id_nivel = ".$datos_punto[0]["territorio"]." ";
						 		
             $r_consulta_lista = $this->BD->devolver_array($consulta_lista);

			 if(count($r_consulta_lista)> 0){
				 $lista = false;
			 }else{
				$tipo_nivel--;
			 }

          break;
          case 1://REGIONAL OPERADOR

             $consulta = "SELECT 
			                    id_regional 
						  FROM 
						        {$GLOBALS["BD_NAME"]}.territorios_distribuidor 
						  WHERE 
								id_territorio = ".$datos_punto[0]["territorio"]."  
						  GROUP BY 
						        id_regional;";
			$datos_reg = $this->BD->devolver_array($consulta);

			$consulta_lista ="SELECT
									d.id_lista
							FROM 
								$bd.lista_precio_market_place l
							INNER JOIN 
								$bd.items_lista_market_place d
									ON l.id = d.id_lista
							INNER JOIN 
								$bd.niveles_listaprecios_market_place n
									ON l.id = n.id_lista
							WHERE
								d.estado = 1 AND l.estado = 1 AND  l.estado_vigencia=1 AND 
								n.tipo_nivel = $tipo_nivel AND 
								d.id_referencia  = $referencia AND 
								n.id_nivel = ".$datos_reg[0]["id_regional"]." ";
			
            $r_consulta_lista = $this->BD->devolver_array($consulta_lista);

            if(count($r_consulta_lista)> 0){
			  $lista = false;
            }else{
              $tipo_nivel--;
            }

          break;

          default:

            $r_consulta_lista = 0;
            $lista = false;

          break;
        }



      }
      //echo "si";
      if($r_consulta_lista != 0){


		$consulta_ref = "SELECT 
								d.precio_venta AS valor_referencia
						FROM 
								$bd.lista_market_place l 
						INNER JOIN 
								$bd.items_lista_market_place d
								ON l.id_referencia = d.id_referencia
						WHERE 
		                          l.estado = 1 AND d.id_lista = 21 AND
								  d.id_lista=".$r_consulta_lista[0]["id_lista"]." AND
								  d.id_referencia = $referencia";
      }else{
		
        $consulta_ref = "SELECT id,pn,precio AS valor_referencia FROM $bd.lista_market_place lm WHERE lm.id_referencia = $referencia";
      }
	 
	  $r_referencia =  $this->BD->devolver_array($consulta_ref);
	
    }else{
   
      //$this->Valor_ListaP_opeador($referencia,$punto,$id_distri);
      $consulta_ref = "SELECT id,pn,precio AS valor_referencia,0 AS iva_referencia,0 AS valor_directo FROM $bd.lista_market_place lm WHERE lm.id_referencia = $referencia";
    
      $r_referencia =  $this->BD->devolver_array($consulta_ref);

    }

    return $r_referencia;

}


public function obtener_datos_edit($id){

	$consulta = "SELECT 
						cp.id,cp.titulo,cp.precio,cp.id_categoria,cp.id_sub_categoria,cp.marca,cp.estado_producto,
						IF(cp.estado_producto = 1,'Nuevo',IF(cp.estado_producto = 2, 'Usado - Como nuevo',
						IF(cp.estado_producto = 3, 'Usado - Buen estado',IF(cp.estado_producto = 4,'Usado - Aceptable','')))) AS estado_prod,
						cp.fecha_inicio,DATEDIFF(CURDATE(),cp.fecha_inicio) AS fecha_pub,
						IF(cp.fecha_fin = '0000-00-00',DATE_ADD(cp.fecha_inicio, INTERVAL 6 MONTH),cp.fecha_fin) AS fecha_final,
						cp.descripcion,cp.ubicacion,cp.disponibilidad,
						IF(cp.disponibilidad = 1,'Unico',IF(cp.disponibilidad = 2, 'Disponible','')) AS disponibilidad_prod,
						cp.palabras,cp.latitud,cp.longitud
				FROM 
				        {$GLOBALS["BD_POS"]}.crear_productos__mp cp
				WHERE 
				          cp.id = $id";                    
   
	 $datos = $this->BD->devolver_array($consulta);

	 return $datos;
  }


  

  public function cargar_vendedor_edit($id){
	$oSession = unserialize($_SESSION[$GLOBALS["SESION_POS"]]);
	$id_user = $oSession->VSidpos;
	
	$consulta ="SELECT 
						cp.id,
						p.idpos,
						p.razon AS nombre_pdv,
						p.nombre_cli AS nombre,
						p.tel AS telefono,
						p.celular AS celular
					FROM {$GLOBALS["BD_POS"]}.crear_productos__mp cp
			   LEFT JOIN {$GLOBALS["BD_POS"]}.puntos p ON p.idpos = cp.id_pos
			   WHERE cp.id = $id";
	$datos = $this->BD->devolver_array($consulta);

	return array("datos" => $datos,"usuario" => $id_user);
}

public function cargar_imagenes($id){
	$consulta ="SELECT 
						ip.id,ip.nombre,ip.principal
				FROM 
				      {$GLOBALS["BD_POS"]}.imagenes_productos__mp ip
				WHERE 
					   ip.id_producto = $id";
					   
	$datos = $this->BD->devolver_array($consulta);

	return $datos;
}


public function guardar_contacto($id_pdv,$nombre,$nom_encargado,$telefono,$celular,$ter_cond,$id_update){

	$oSession = unserialize($_SESSION[$GLOBALS["SESION_POS"]]);
	$id_user = $oSession->VSidpos;
	$id_distri  = $oSession->VSid_distri;
	$error = 0;

	$this->BD->consultar("BEGIN");

	$sql ="SELECT COUNT(*) AS registro FROM {$GLOBALS["BD_POS"]}.solicitud_publicacion__mp WHERE id_pos = '$id_pdv' AND id_producto = $id_update";$query =  $this->BD->devolver_array($sql);

	if($query[0]["registro"] == 0){
	$consulta = "INSERT INTO {$GLOBALS["BD_POS"]}.solicitud_publicacion__mp(id_pos,nombre,nom_encargado,telefono,celular,fecha,hora,ter_cond,id_producto) VALUES('$id_pdv','$nombre','$nom_encargado','$telefono','$celular',CURDATE(),CURTIME(),'$ter_cond',$id_update)";    
	$datos = $this->BD->consultar($consulta);
	//$id_registro = $this->BD->last_insert_id();
	if(!$datos){
		$error = 1;
	 }/*else{             
		 $consulta_audi = "INSERT INTO {$GLOBALS["BD_POS"]}.audi_crear_productos__mp(id_producto,id_pos,id_distri,titulo,precio,id_categoria,id_sub_categoria,marca,estado_producto,fecha_inicio,fecha_fin,descripcion,ubicacion,latitud,longitud,disponibilidad,palabras,estado,usuario,fecha_update,hora_update,accion) (SELECT id,id_pos,id_distri,titulo,precio,id_categoria,id_sub_categoria,marca,estado_producto,fecha_inicio,fecha_fin,descripcion,ubicacion,latitud,longitud,disponibilidad,palabras,estado, '".$id_user."',CURDATE(),CURTIME(), 0 FROM {$GLOBALS["BD_POS"]}.crear_productos__mp WHERE id = $id_registro);";             
		 $datos_audi = $this->BD->consultar($consulta_audi);
		 if(!$datos_audi){
			$error = 1;
		 }
		}*/
	}else{
		$error = 2;
	}
		if($error == 0){
			$this->BD->consultar("COMMIT");
			return 0;
		}else if($error == 2){
			$this->BD->consultar("COMMIT");
			return 2;
		}else{
			$this->BD->consultar("ROLLBACK");
			return 1;
		}
	// }

}

public function consultar_notificacion(){          

	$oSession = unserialize($_SESSION[$GLOBALS["SESION_POS"]]);
	$id_user = $oSession->VSidpos;
	$id_distri  = $oSession->VSid_distri;

	  $sql = "SELECT 						
					COUNT(cp.id) AS cantidad
					FROM {$GLOBALS["BD_POS"]}.solicitud_publicacion__mp cp
					INNER JOIN {$GLOBALS["BD_POS"]}.crear_productos__mp cpp ON cpp.id = cp.id_producto
					WHERE cpp.id_pos = $id_user AND cp.revisado = 0";                        
	  
	  $res_noti = $this->BD->devolver_array($sql);

	  $consulta = "SELECT 						
					cp.id,
					cp.id_pos,
					cp.nombre,
					cp.nom_encargado,
					cp.telefono,
					cp.celular,
					cp.ter_cond,
					cp.estado,
					IF(LENGTH(cpp.titulo) > 30,CONCAT(SUBSTRING(cpp.titulo,1,25),'...'),cpp.titulo) AS titulo,
					cp.fecha,
					cp.hora,
					cp.revisado,
					ip.nombre AS nombre_imagen,
					cpp.ubicacion
				   FROM {$GLOBALS["BD_POS"]}.solicitud_publicacion__mp cp
				   INNER JOIN {$GLOBALS["BD_POS"]}.crear_productos__mp cpp ON cpp.id = cp.id_producto
				   INNER JOIN {$GLOBALS["BD_POS"]}.imagenes_productos__mp ip ON ip.id_producto = cpp.id AND ip.principal = 1
				   WHERE cpp.id_pos = $id_user AND cp.estado = 0
				   ORDER BY cp.id DESC";	      
	  $carga_noti = $this->BD->devolver_array($consulta); 

	  return array("cant_noti" => $res_noti[0]["cantidad"],"carga_noti" => $carga_noti);

}

public function leer_notificacion($id){

	  $oSession = unserialize($_SESSION[$GLOBALS["SESION_POS"]]);
	  $id_user = $oSession->VSidpos;
	  $id_distri  = $oSession->VSid_distri;
	  $sql = "";
	  
	  $consulta = "SELECT fecha_leido FROM {$GLOBALS["BD_POS"]}.solicitud_publicacion__mp WHERE id = $id";
	  $datos = $this->BD->devolver_array($consulta);
	  
	  if($datos[0]["fecha_leido"] == ""){
		  $sql .= ",fecha_leido = CURDATE(),hora_leido = CURTIME()";
	  }

	  $sql = "UPDATE {$GLOBALS["BD_POS"]}.solicitud_publicacion__mp SET revisado = 1 $sql WHERE id = $id";                    
	  $res_noti = $this->BD->consultar($sql);

	  if($this->BD->numupdate() == 0){
		   return 0;
	  }
	  else if($id > 0){
		   return $id;
	  }else{
		   return 1;
	  }
}

public function eliminar_solicitud($id){

	$oSession = unserialize($_SESSION[$GLOBALS["SESION_POS"]]);
	$id_user=$oSession->VSid;
	$id_distri=$oSession->VSidDistri;

	$consulta = "UPDATE {$GLOBALS["BD_POS"]}.solicitud_publicacion__mp SET estado = 2 WHERE id = $id";          
	$datos = $this->BD->consultar($consulta);
	if(!$datos){
		return 1;
	}else{
		return 0;
	}
}

public function cargar_vendedor(){

	$oSession = unserialize($_SESSION[$GLOBALS["SESION_POS"]]);
	$id_user = $oSession->VSidpos;

	$consulta = "SELECT 
						p.idpos,
						p.razon AS nombre_pdv,
						p.nombre_cli AS nombre,
						p.tel AS telefono,
						p.celular AS celular,
						p.latitud,
						p.longitud
				   FROM {$GLOBALS["BD_POS"]}.puntos p
				  WHERE p.idpos = $id_user";                                          

	 $datos = $this->BD->devolver_array($consulta);

	 return $datos;
	}

}
