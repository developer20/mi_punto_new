<?php
include_once("../../config/mainController.php"); // Incluye el Controlador Principal
include_once("../../modulos/responder_encuesta/modelo.php");	// Incluye el Modelo.
$controller = new mainController; // Instancia a la clase MainController
$modelo = new responder_encuesta(); // Instancia a la clase del modelo
try // Try, manejo de Errores
{
	$metodo = $_SERVER['REQUEST_METHOD'];
	$tipo_res = "";
	$response = null; 
	// Se manejaran dos tipos JSON y HTML
	// Dependiendo del método de la petición ejecutaremos la acción correspondiente.
	// Por ahora solo POST, todas las llamadas se haran por POST
    $variables = $_POST;
	if(!isset($_POST['accion'])){echo "0"; return;} // Evita que ocurra un error si no manda accion.
	$accion = $variables['accion'];
		// Dependiendo de la accion se ejecutaran las tareas y se definira el tipo de respuesta.
		switch($accion) {
			case 'Cargar_Encuesta':
				 $tipo_res = 'JSON'; //Definir tipo de respuesta;
				 $response = $modelo->Cargar_Encuesta();
			break;
			case 'Cargar_Preguntas':
				 $tipo_res = 'JSON'; //Definir tipo de respuesta;
				 $encuesta = $variables['encuesta'];
				 $response = $modelo->Cargar_Preguntas($encuesta);
			break;
			case 'Cargar_Respuestas':
				 $tipo_res = 'JSON'; //Definir tipo de respuesta;
				 $encuesta = $variables['encuesta'];
				 $id_preg = $variables['id_preg'];
				 $response = $modelo->Cargar_Respuestas($encuesta,$id_preg);
			break;
			case 'Agregar_Respuesta':
				 $tipo_res = 'JSON'; //Definir tipo de respuesta;
				 $id_respuesta = $variables['id_respuesta'];
				 $encuesta = $variables['encuesta'];
				 $id_pregunta = $variables['id_pregunta'];
				 $response = $modelo->Agregar_Respuesta($id_respuesta,$encuesta,$id_pregunta);
			break;
			case 'Guardar_Encuesta':
				 $tipo_res = 'JSON'; //Definir tipo de respuesta;

				 $check_resp_enc = $variables['check_resp_enc'];

				 $response = $modelo->Guardar_Encuesta($check_resp_enc);
			break;	
		}

	// Respuestas del Controlador
	if($tipo_res == "JSON")
	{
	  echo json_encode($response,true); // $response será un array con los datos de nuestra respuesta.
	}
	elseif ($tipo_res == "HTML") {
	  echo $response; // $response será un html con el string de nuestra respuesta.
	}
} // Fin Try
catch (Exception $e) {}
