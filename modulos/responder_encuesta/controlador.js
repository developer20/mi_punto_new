var envio = true;
var contador = 0;
var cantidad = 0;
var conteo = 0;
var navegar = 0;
var myCont=0;
var anterior = 0;
var id_encuesta = 0;
var encuesta = 0;
var pregunta_ant = 0;
var respuesta = 0;
var obligatorio = 0;
var cont = 0;
var act = 0;
var check_resp = new Array();
var id_preg = 0;

$(document).ready(function() {

	Cargar_Encuesta();


	$('.saltar_enc').on('click',function(){
		localStorage['est_enc_dis'] = 0;
		window.location="?mod=principal";
	});


});


function Cargar_Encuesta(){

	$.ajax({
		url:"modulos/responder_encuesta/controlador.php",
		type: "POST",
		dataType: "JSON",
		data: {
			accion:"Cargar_Encuesta"
		},
		success: function(data){
			
			if(data['error']==0){
				$("#visor_panel").html("<h3 class='text-center' style='margin:20px;'>No hay encuestas pendientes por responder</h3>");
				$("#next").hide();

				if(localStorage.getItem('est_enc_dis') > 0){
					localStorage['est_enc_dis'] = 0;
					setTimeout(function(){ window.location = "?mod=principal" }, 2000);
				}

				return false;
			}
			
			for(var i=0;i<data.length;i++)
			{

				if(data[0]['obligatorio']==0){
					$('.saltar_enc').show();
				}
			
				cantidad = data[0]['cant'];
				navegar = data[0]['navegar_atras'];

				$(".titulo_enc h1").html(data[0]['titulo']);
				$(".descrp_enc h3").html(data[0]['descripcion']);
				$("#total_preg").html(data[0]['cant']);
				id_encuesta = data[0]['id'];
				
			}

			$("#next").unbind('click')
			$('#next').on('click',function(){


				    var band = 0;

					for(var x in check_resp){
						info_enc = check_resp[x].split(",");

						if(id_encuesta == info_enc[0] && info_enc[1] == id_preg && info_enc[2] > 0){
							band = 1;
						}

					}

					if(obligatorio == 1 && !band){
						Notificacion("La pregunta es obligatoria, por favor selecciona una opción","warning");
					}else{

						$('.saltar_enc').hide();

						$("#titulo_enc").html(data[0]['titulo']);

						/*
					    if(conteo == cantidad){
					    	$("#next").html("Enviar");
					    }else{*/
					    	
							if(cont<cantidad){
								Cargar_Preguntas(id_encuesta,cantidad,1);
							}else{

								BootstrapDialog.show({
							    	title:'Mensaje del Sistema',
							    	message:"<h3 class='text-center'>Gracias por responder la encuesta!</h3>",
							    	buttons: [{
								        id: 'btn-exit',      
								        label: 'Cancelar',
								        cssClass: 'btn-default', 
								        autospin: false,
								        action: function(result){ 
								        	  if(result) {
								            	result.close();
									    	}
								        }
								    },{
								        id: 'btn-ok',      
								        label: 'Continuar',
								        cssClass: 'btn-primary', 
								        autospin: false,
								        action: function(result){ 
								        	if(result) {	
								        	  	//Cargar_Preguntas(id_encuesta,cantidad,1);
									    		Guardar_Encuesta();
								            	result.close();
									    	}
								        }
								    }]
							    });
								
							}
						/*
						}*/

					}
			});

			$("#previous").on('click',function(){
				$("#next").val("Siguiente");
				contador = contador-2;
				conteo = conteo-2;
				cont = cont-2;
				if(anterior>0)
				{

					if(conteo==0)
					{
						$("#previous").hide();
					}
					/*
					if(id_encuesta>0)
					{ */   
						Cargar_Preguntas(id_encuesta,cantidad,0);
					//}
				}
			});

		}
	});
}

function Cargar_Preguntas(value,cantidad,next){

	id_encuesta = value;
	
	$("input[data-nom='resp[]'").each(function(){
		activo = $(this).prop("checked");

		encuesta = $(this).attr("data-enc");
		pregunta = $(this).attr("data-preg");
		respuesta = $(this).attr("data-resp");
		var tipo = $(this).attr("data-tipo");
		
		if(activo==true)
		{
			//Agregar_Respuesta(encuesta,pregunta,respuesta,tipo)
			act++;
		}
	});

	/*
	if(cont==0)
	{*/
	if(cont < cantidad){
		$.ajax({
		url:"modulos/responder_encuesta/controlador.php",
		type: "POST",
		dataType: "JSON",
		data: {
			accion:"Cargar_Preguntas",
			encuesta: id_encuesta
		},
		success: function(data){

				var cant = data.length;
				id_preg = data[contador]['id'];
				var pregunta = data[contador]['pregunta'];
				var tipo = data[contador]['tipo'];
				obligatorio = data[contador]['obligatorio'];

				if(navegar==1 && cont > 0)
				{
	    	  		$("#previous").show();
	    	  		anterior = 1;
	    	  	}	

	    	  	/*
	    	  	if(conteo==0)
				{
	    	  		$("#previous").hide();
	    	  	}*/

				if(obligatorio == 1){
					$("#obliga").show();
				}else{
					$("#obliga").hide();
				}

				$(".titulo_enc h1").html("<label class='text-info'>"+pregunta+"</label>");
				$(".cant_preg").hide();

				Cargar_Respuestas(id_preg,id_encuesta,tipo,obligatorio);
				contador++;

				cont++;

			    if(cont == cantidad){

			    	$("#next").val("Enviar");
			    }

			}
		});
	}
		
	/*
	}else{

		if(obligatorio==1)
		{
			if(act==0)
			{
				Notificacion("Seleccione una Opcion por favor!.","warning");
				return false;
			}
		}
		else
		{
			act++;

			Cargar_Preguntas(value,cantidad,next);
		}

		/*
		if(act>0) // establecer mensaje de obligatorio.
		{
			if(navegar==1)
			{
    	  		$("#previous").show();
    	  	}

    	  	if(conteo==0)
			{
    	  		$("#previous").hide();
    	  	}
			anterior++;
    	  	
			$.ajax({
			url:"modulos/responder_encuesta/controlador.php",
			type: "POST",
			dataType: "JSON",
			data: {
				accion:"Cargar_Preguntas",
				encuesta: id_encuesta
			},
			success: function(data){

				var cant = data.length;
				var id_preg = data[contador]['id'];
				var pregunta = data[contador]['pregunta'];
				var tipo = data[contador]['tipo'];
				obligatorio = data[contador]['obligatorio'];

				$(".titulo_enc h1").html("<label class='text-info'>"+pregunta+"</label>");
				$(".cant_preg").hide();

				Cargar_Respuestas(id_preg,id_encuesta,tipo,obligatorio);
				contador++;

				}
			});
			cont++;
		}else{
			Notificacion("Seleccione una Opcion por favor!.","warning");
		}
	}*/
	

	if(cont==cantidad){
		$("#next").val("Enviar");
	}

}

function Cargar_Respuestas(id_preg,id_encuesta,tipo,obligatorio){
conteo++;

	$.ajax({
		url:"modulos/responder_encuesta/controlador.php",
		type: "POST",
		dataType: "JSON",
		data: {
			accion:"Cargar_Respuestas",
			encuesta: id_encuesta,
			id_preg: id_preg
		},
		success: function(data){

			var cant = data.length;
			envio = false;
			var checked = "";
			var option = "";
			var tip_sel = "";
			var requer = "";

			if(tipo==1){
				tip_sel = "radio";
			}else{
				tip_sel = "checkbox";
			}

			if(obligatorio==1){
				requer = "required='required'";
			}

			if(cant>0){
				var num=0;
				for(var i=0;i<cant;i++)
				{

					var activar = data[i]['activar'];
					var respuesta = data[i]['respuesta'];
					var id_respuesta = data[i]['id'];
					var checked = "";

					for(var x in check_resp){
						info_enc = check_resp[x].split(",");

						if(id_encuesta == info_enc[0] && id_preg == info_enc[1] && id_respuesta == info_enc[2]){
							checked = "checked='checked'";
						}

					}

					/*
					if(activar>0)
					{
						checked = "checked='checked'";
					}*/

					option += "<div class='col-md-10 text-left' style='margin-bottom: 3px; position: relative;'>";
					option += "<div style='display: inline-block; padding: 2px; margin-right: 5px; position: relative;'>";
					option+="<input type='"+tip_sel+"' "+checked+" name='respuestas' style='font-size: 21px;' data-nom='resp[]' data-tipo='"+tipo+"' onclick='regActivo("+i+","+tipo+");' data-resp='"+id_respuesta+"' data-enc='"+id_encuesta+"' data-preg='"+id_preg+"'  "+requer+" id='resp_"+i+"' value='"+i+"' />";
					option += "</div>";
					option += "<label class='text-info' for='resp_"+i+"' style='font-size: 18px;cursor: pointer; margin: 0px; padding: 0px;'>"+respuesta+"</label>";
					option += "</div>";

					num++;
				}
				option += "<div class='col-md-12 text-center' style='font-size: 14px;'>Pregunta "+conteo+" de "+cantidad+"</div>"

			}
			else{
				option += "<h3>No hay opciones de respuesta...</h3>";
			}
			
			$(".descrp_enc").html(option);

		}
	});

}


function Guardar_Encuesta(){

	var check_resp_enc = new Array();

	for(var x in check_resp){

		if(check_resp[x] != ""){
			info_enc = check_resp[x].split(",");

			check_resp_enc.push({id_enc:info_enc[0],id_pre:info_enc[1],id_resp:info_enc[2]});
		}

	}

	$.ajax({
		url:"modulos/responder_encuesta/controlador.php",
		type: "POST",
		dataType: "JSON",
		data: {
			accion:"Guardar_Encuesta",
			check_resp_enc:check_resp_enc

		},
		success: function(data){

			var error = data['error'];

			if(error==0){
				window.location="?mod=responder_encuesta";
			}else if(error==1){
				Notificacion("Error al tratar de guardar la encuesta, por favor comuniquese con soporte.","danger");
			}else if(error==2){
				Notificacion("No hay encuesta para enviar.","warning");
			}

		}
	});

}

function regActivo(value,tipo){
	var estado = $("#resp_"+value).prop("checked");

	if(estado==true){
		act=1;
		var id_encuesta = $("#resp_"+value).attr("data-enc");
		var id_pregunta = $("#resp_"+value).attr("data-preg");
		var id_respuesta = $("#resp_"+value).attr("data-resp");

		if(tipo == 1){
			for(var x in check_resp){
				info_enc = check_resp[x].split(",");
				if(id_encuesta == info_enc[0] && id_pregunta == info_enc[1]){

					delete check_resp[x];
				}
			}
		}

		if(check_resp.indexOf(id_encuesta+","+id_pregunta+","+id_respuesta) == -1){
			check_resp.push(id_encuesta+","+id_pregunta+","+id_respuesta);
		}

	}else{
		var id_encuesta = $("#resp_"+value).attr("data-enc");
		var id_pregunta = $("#resp_"+value).attr("data-preg");
		var id_respuesta = $("#resp_"+value).attr("data-resp");
		for(var x in check_resp){
			info_enc = check_resp[x].split(",");

			if(id_encuesta == info_enc[0] && id_pregunta == info_enc[1] && id_respuesta == info_enc[2]){

				delete check_resp[x];
			}

		}

		act=0;
	}

}



