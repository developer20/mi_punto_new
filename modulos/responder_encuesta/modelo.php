<?php
require("../../config/mainModel.php");
class responder_encuesta {
/*
  METODOS DE LA BD.
  $this->BD->consultar($query); // Ejecuta la consulta y devuelve string.!
  $this->BD->devolver_array($query); // Ejecuta la consulta y devuelve array asociativo.!
  $this->BD->consultar("BEGIN"); // Antes de transacciones.!
  $this->BD->consultar("COMMIT"); // Commit para guardar datos.!
  $this->BD->consultar("ROLLBACK"); // Devolver datos si hay error.!
  $this->BD->numreg($query); // Devuelve el numero de registros de la consulta.!
*/
	public function __construct()
    {
      $BD=new BD();
      $this->BD = $BD;
      $this->BD->conectar();
    }
    public function __destruct()
    {
      $this->BD->desconectar();
    }
   //Espacio Para declara las funciones que retornan los datos de la DB.


    public function Cargar_Encuesta(){

      $oSession = unserialize($_SESSION[$GLOBALS["SESION_POS"]]);
      $idpos=$oSession->VSidpos;

      $permZonas = 0;
      $permTerri = 0;
      $permReg = 0;
      $IdDistri = 0;

      $sql = "SELECT zona,territorio,id_regional,id_distri FROM puntos WHERE idpos = $idpos";
      $resPos = $this->BD->consultar($sql);

      if(count($resPos)>0){
        $permZonas = $resPos->fields["zona"];
        $permTerri = $resPos->fields["territorio"];
        $permReg = $resPos->fields["id_regional"];
        $IdDistri = $resPos->fields["id_distri"];
      }

      $sql = "SELECT 
      (SELECT COUNT(id) FROM {$GLOBALS["BD_NAME"]}.enc__preguntas WHERE id_encuesta = enc.id AND estado = 1) as cant,
      enc.id,
      enc.titulo,
      enc.descripcion,
      enc.fecha_inicio,
      enc.fecha_fin,
      enc.obligatorio,
      enc.navegar_atras 
              FROM {$GLOBALS["BD_NAME"]}.enc__encuesta as enc
              INNER JOIN {$GLOBALS["BD_NAME"]}.enc__preguntas as preg ON (preg.id_encuesta=enc.id AND preg.estado=1)
              INNER JOIN {$GLOBALS["BD_NAME"]}.enc__niveles_dcs AS dcs ON (dcs.id_encuesta=enc.id)
              WHERE enc.estado=1 AND enc.vigente=1 AND dcs.nivel=5 AND dcs.web = 1
              AND ((dcs.tipo=1 and dcs.id_tipo IN ($permReg))
                  OR(dcs.tipo=2 and dcs.id_tipo = $IdDistri)
                  OR(dcs.tipo=3 and dcs.id_tipo IN ($permTerri))
                  OR(dcs.tipo=4 and dcs.id_tipo IN ($permZonas))) 
                  AND enc.id NOT IN (SELECT id_encuesta FROM {$GLOBALS["BD_NAME"]}.enc__respuestas_encuesta WHERE id_pos=$idpos AND id_encuesta = enc.id GROUP BY id_encuesta)
              GROUP BY dcs.id_encuesta ORDER BY enc.obligatorio DESC";
      $response = $this->BD->devolver_array($sql);

      if(count($response) > 0){
        return $response;
      }
      else{
        return array("error"=>0);
      }
    }

    public function Cargar_Preguntas($encuesta){

      $sql = "SELECT id,pregunta,id_encuesta,tipo,obligatorio,orden
              FROM {$GLOBALS["BD_NAME"]}.enc__preguntas
              WHERE id_encuesta=$encuesta AND estado=1 ORDER BY orden ASC";
      $response = $this->BD->devolver_array($sql);

      return $response;
    }

    public function Cargar_Respuestas($encuesta,$id_pregunta){

      $existe = false;
      $tar_preg = false;
      $pregunta = 0;
      $cantidad = 0;

      $sql = "SELECT id,respuesta,id_pregunta,id_encuesta,orden, 0 activar
            FROM {$GLOBALS["BD_NAME"]}.enc__opciones_resp
            WHERE id_pregunta=$id_pregunta AND id_encuesta=$encuesta AND estado=1 ORDER BY orden ASC";
      $response = $this->BD->devolver_array($sql);

      return $response;

    }

    /*
    public function Agregar_Respuesta($id_respuesta,$encuesta,$id_pregunta){

      if(!isset($_SESSION['encuesta_distri']))
      {
        $_SESSION['encuesta_distri'] = array();
      }

      $_SESSION['encuesta_distri'][] = array("encuesta"=>$encuesta,"pregunta"=>$id_pregunta,"respuesta"=>$id_respuesta);
      
    }*/

    public function Guardar_Encuesta($check_resp_enc){

      $oSession = unserialize($_SESSION[$GLOBALS["SESION_POS"]]);
      $idpos=$oSession->VSidpos;

      $pZdcs = 0;
      $pTdcs = 0;
      $pRdcs = 0;
      $IdDistri = 0;

      $sql = "SELECT zona,territorio,id_regional,id_distri FROM puntos WHERE idpos = $idpos";
      $resPos = $this->BD->consultar($sql);

      if(count($resPos)>0){
        $pZdcs = $resPos->fields["zona"];
        $pTdcs = $resPos->fields["territorio"];
        $pRdcs = $resPos->fields["id_regional"];
        $IdDistri = $resPos->fields["id_distri"];
      }

      $total = count($check_resp_enc);

      $this->BD->consultar("BEGIN");
      $error = 0;
      $msg = 0;

      if($total>0)
      {

        foreach ($check_resp_enc as $resp)
        {
          # code...
          $encuesta = $resp['id_enc'];
          $pregunta = $resp['id_pre'];
          $respuesta = $resp['id_resp'];

          $sql = "INSERT INTO {$GLOBALS["BD_NAME"]}.enc__respuestas_encuesta(tipo_encuestado,id_encuesta,id_pregunta,id_opcion,fecha,hora,origen,regional,territorio,zona,distri,id_pos) VALUES (1,$encuesta,$pregunta,$respuesta,curdate(),curtime(),2,$pRdcs,$pTdcs,$pZdcs,$IdDistri,$idpos)";
          $response = $this->BD->consultar($sql);

          if(!$response)
            $error=1;
        }

      }
      else
      {
        $error = -1;
      }

      if($error == 1)
      {
        $this->BD->consultar("ROLLBACK");
        $msg = 1;
      }
      else if($error == -1)
      {
        $msg = 2;
      }
      else
      {
        $this->BD->consultar("COMMIT");
      }

      return array("error"=>$msg);

    }
   
  
}// Fin clase
?>