var dtable;
$(document).ready(function() {
	
	CargarOpers();
	
	$("#frmRe").submit(function(event) {
		event.preventDefault();
		reporteTransac();
	});
	
	$("#num_m").keypress(function(e) {
		var key;
		if(window.event){ // IE
			key = e.keyCode;
		}else if(e.which){ // Netscape/Firefox/Opera
			key = e.which;
		}
		if (key == 13){
		  validar();
		}
		else if (key < 48 || key > 57){
			return false;
		}
		return true;
		  //validar_espacios(event);
	});
	
	$("#num_m").click(function(e) {
		$( "#num_m" ).attr("onpaste","return false");
	});
	$("#CSV").click(function() {
		ExportarCSV(dtable.data(), true,"Reporte de Ventas");
	});
	
	$('#datetimepicker1').datepicker({
		format: 'yyyy-mm-dd'
	})
	
	$('#datetimepicker2').datepicker({
		format: 'yyyy-mm-dd'
	})

});

function CargarOpers()
{
	
	$.post('modulos/venta/controlador.php',
		{
			accion: 'CargarOpers',
			tipo: '2',
			
		},
		function(data, textStatus) {
			
			if(data != 0 || data != "")
			{	
				var datos = JSON.parse(data);
				var operadores = "";
				if(datos != ""){
					operadores = '<option value="" >Elige un operador...</option>';
					$.each(datos,function(index, fila) {
						
						operadores += '<option value="'+fila.id+'">'+fila.des+'</option>';
					});
					
					$("#opers").html(operadores);
					
				}
				
			}
		}
	);
}


function reporteTransac(){
	
	var fecha_ini = $("#fecha_ini").val();
	var fecha_fin = $("#fecha_fin").val();
	var num_m = $("#num_m").val();
	var opers = $("#opers").val();
	var cont = 0;
	if ( ! $.fn.DataTable.isDataTable( '#tblTrans' ) ) {
		
		 dtable = $("#tblTrans").DataTable({
			 
			"ajax": {
				"url": "modulos/reporte_ventas/controlador.php",
				"type": "POST",
				"deferRender": false,
				"data":{
					accion:'reporteVentas',
					fecha_ini:fecha_ini,
					fecha_fin:fecha_fin,
					num_m:num_m,
					opers:opers,
				}
			},
			  "bFilter": false, 
			  "responsive":true,
			  "columns": [
			  	{ "data": "dateTrans"},
			  	{ "data": "dateTrans"},
				{ "data": "horaTrans"},
				{ "data": "horaReg"},
				{ "data": "transac"},
				{ "data": "num_m"},
				{ "data": "val_servs"},
				{ "data": "des"},
				{ "data": "paque"}
				],
				"columnDefs": [
				{
					"targets": 0,
					"data": "",
					 render: function ( data, type, row ) {
						cont += 1;
						return cont ;       
					 }
				}
				],
		 });
	
	}
	else{
		dtable.destroy();
		reporteTransac();
	}
	$("#resultado").show();
}

function addCommas(nStr)
{
	nStr += '';
	x = nStr.split('.');
	x1 = x[0];
	x2 = x.length > 1 ? '.' + x[1] : '';
	var rgx = /(\d+)(\d{3})/;
	while (rgx.test(x1)) {
	x1 = x1.replace(rgx, '$1' + ',' + '$2');
	}
	return x1 + x2;
}