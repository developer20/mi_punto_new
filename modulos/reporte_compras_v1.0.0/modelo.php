<?php
require("../../config/mainModel.php");
class reporte_compras {
    //Espacio Para declara las funciones que retornan los datos de la DB.
    public function __construct(){
  		$BD=new BD();
  		$this->BD = $BD;
		$this->BD->conectar();
	}
	  
    public function __destruct(){
    	$this->BD->desconectar();
	}

	public function ConsultarVentas($fin,$inicio){

		$oSession = unserialize($_SESSION[$GLOBALS["SESION_POS"]]);
		$idDistri = $oSession->VSid_distri;
		$punto  =  $oSession->VSidpos;

			
			  $sql_dis = "SELECT nombre_corto FROM {$GLOBALS["BD_NAME"]}.distribuidores WHERE id = $idDistri";
		      $query_dis = $this->BD->devolver_array($sql_dis);
		      $nombre_corto = $query_dis[0]["nombre_corto"];
		      $bd = "{$GLOBALS["BD_DIS"]}" . $nombre_corto . "_" . $idDistri;
	
			 $sql = "SELECT 
							u.id idUser,CONCAT(u.nombre,' ',u.apellido) nom_user,ven.id_venta,ven.id_vendedor,
							ven.regional,count(ven.id) as cant_ventas,ven.valor_venta AS valor_unitario, 
							sum(ven.valor_venta) as total_venta,ven.tipo,ven.fecha_venta
					  FROM 
					       {$GLOBALS["BD_NAME"]}.inventario__punto ven
					  LEFT JOIN 
					        $bd.usuarios u
								ON ven.id_vendedor = u.id  AND u.vendedor = 1
					  WHERE
							ven.distri = $idDistri AND ven.id_combo = 0 AND ven.tipo = 1 
							AND ven.fecha_venta BETWEEN '$inicio' AND '$fin'  AND ven.id_pos  = $punto
					  GROUP BY 
							ven.id_venta
					  ORDER BY 
							ven.fecha_venta,ven.id_vendedor;";
						
                 mysql_set_charset("utf8");
				$res_ven = $this->BD->devolver_array($sql);
				return array("data" => $res_ven);
	 		    
	}


	public function detalle_venta($id){

		$oSession = unserialize($_SESSION[$GLOBALS["SESION_POS"]]);
		$idDistri = $oSession->VSid_distri;
		$punto  =  $oSession->VSidpos;

		
	
			 $sql = "SELECT 
							ref.pn AS sku,ref.producto as referencia,count(ven.id) as cant_ventas,
							ven.valor_venta AS valor_unitario,sum(ven.valor_venta) as total_venta
					  FROM 
					       {$GLOBALS["BD_NAME"]}.inventario__punto ven
					 LEFT JOIN 
				          {$GLOBALS["BD_NAME"]}.referencias ref
					        ON ven.id_referencia = ref.id
					  WHERE
							ven.distri = $idDistri AND ven.id_combo = 0 AND ven.tipo = 1 
							AND ven.id_venta = $id  
					  GROUP BY 
							ven.id_referencia";
			   
			   mysql_set_charset("utf8");
				$res_ven = $this->BD->devolver_array($sql);
				return $res_ven ;
	 		    
	}


	public function cargar_tabla_seriales($id){

		$oSession = unserialize($_SESSION[$GLOBALS["SESION_POS"]]);
		$idDistri = $oSession->VSid_distri;
		$punto  =  $oSession->VSidpos;

		
	
			 $sql = "SELECT 
							ref.pn AS sku,ref.producto as referencia,ven.serial
					  FROM 
					       {$GLOBALS["BD_NAME"]}.inventario__punto ven
					 LEFT JOIN 
				          {$GLOBALS["BD_NAME"]}.referencias ref
					        ON ven.id_referencia = ref.id
					  WHERE
							ven.distri = $idDistri AND ven.id_combo = 0 AND ven.tipo = 1 
							AND ven.id_venta = $id  
					  ORDER  BY 
							ven.id_referencia";
			   
			   mysql_set_charset("utf8");
				$res_ven = $this->BD->devolver_array($sql);
				return array("data" => $res_ven) ;
	 		    
	}



	

}
