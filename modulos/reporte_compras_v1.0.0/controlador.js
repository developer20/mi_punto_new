var dtable;


$(document).ready(function() {
  $("#datetimepicker1").datepicker({
    format: "yyyy-mm-dd"
  });

  $("#datetimepicker2").datepicker({
    format: "yyyy-mm-dd"
  });

  $(".content-header").find("h1").html("Mis Compras");

 
  //$("#frmVentas").submit(function(event) {
  $("#btn_buscar").click(function(event) {
    event.preventDefault();
    Cargar_Reporte();
  });
});




Array.prototype.in_array = function() {
  for (var j in this) {
    if (this[j] == arguments[0]) {
      return true;
    }
  }
  return false;
};


function calculoFecha(fecha1,fecha2) {               
  var tiempo = fecha2.getTime() - fecha1.getTime(); 
  var dias = Math.floor(tiempo / (1000 * 60 * 60 * 24));            
  return dias;          

}

var table;

function Cargar_Reporte() {
  var inicio = $("#fecha_ini").val();
  var final = $("#fecha_fin").val();

  if (inicio != "" && fin != "") {
    
          var ini=new Date($('#fecha_ini').val());
          var fin=new Date($('#fecha_fin').val());
          var dper = 31;
          
          var fechaHoraInicial = new Date(ini).getTime();
          var fechaHoraFinal = new Date(fin).getTime();
        
            if (fechaHoraInicial > fechaHoraFinal) {
                Notificacion("La fecha inicial no debe ser mayor a la fecha final", "warning");
                return;
            }
        
          
          if (calculoFecha(ini,fin) > dper) {
            //alert (calculoFecha(ini,fin));
            Notificacion("El rango maximo entre las fechas es de 31 dias.","warning");
          }else {
            if($.fn.DataTable.isDataTable("#consulta")) {
              table.clear();
              table.destroy();
           }
       
            $("#reporte_ventas").show();
            $("#consulta").show();
          
            
             table = $("#consulta").DataTable({
            "responsive": true,
             "ajax" :{
              "url": "modulos/reporte_compras_v1.0.0/controlador.php",
               "data": {accion: "ConsultarVentas", inicio: inicio, final: final},
              "type": "POST"
             },
             "paging" : true,
             "info" : true,
             "searching": false,
             "filter" : true,
             "columns": [
               {data: 'nom_user'},
               {data: 'fecha_venta'},
              ],
              "columnDefs": [
                {
                  "targets": [2],
                  "data": 'cant_ventas',
                  "render": function (data, type, row) {
                     return "<p style='cursor: cell;color:red' onclick='ver_seriales("+row.id_venta+")'>"+ formato_numero(data,0,',','.') + "</p>";
                  }
                },
                {
                    "targets": [3],
                    "data": 'total_venta',
                    "render": function (data, type, row) {
                       return formato_numero(data,0,',','.');
                    }
                },
                {
                  "targets": [4],
                  "data": 'total_venta' ,
                  "render": function (data, type, row) {
                     return "<i style='cursor:pointer; font-size:19px;' class='glyphicon glyphicon-plus-sign det_reg text-primary'></i>";
                  }
              }
    
            ],
            "createdRow": function(row,data,dataIndex) {
               $(row).attr("data-id_venta",data.id_venta);
             },
             fnDrawCallback: function () {
            
               $(".det_reg").unbind("click");	
               $(".det_reg").click(function(event){
              event.preventDefault();
              var id_venta = $(this).closest("tr").data("id_venta");
              var tr = $(this).closest("tr"); 
              var icono = $(this);
               $.post('modulos/reporte_compras_v1.0.0/controlador.php',{accion: "detalle_venta",id:id_venta},function(data) {
                 if(data != 0 || data != ""){
                  var html = "";
                  data = JSON.parse(data);

                  if(icono.hasClass("glyphicon-plus-sign")){// simbolo +
                    icono.addClass("glyphicon-minus-sign");
                    icono.removeClass("glyphicon-plus-sign");
                    
                    html+='<td class="tbl_'+id_venta+'"  colspan="5"><div class="col-md-12" style="border:2px #ccc solid; border-radius:2px;"><div class="text-center">Detalles de la Compra</div>'
                    html +="<table class='table table-hovered table-condensed'><thead><tr ><th>SKU</th><th>Referencia</th><th style='text-align: center;'>Cantidad</th><th>Valor Unitario</th><th>Valor Total</th></tr></thead><tbody>";
                    
                    $.each(data,function(i) {
                        html += "<tr><td>"+data[i].sku+"</td><td>"+data[i].referencia+"</td><td align='center'>"+formato_numero(data[i].cant_ventas,0,',','.')+"</td><td>"+formato_numero(data[i].valor_unitario,0,',','.')+"</td><td>"+formato_numero(data[i].total_venta,0,',','.')+"</td>"; 
                    });	
                    html +="</tbody></table></div></tr>";
                    tr.after(html);
                    
                       
                    
                 }else{
                  icono.removeClass("glyphicon-minus-sign");
                  icono.addClass("glyphicon-plus-sign");
                  $("#consulta tbody").find("td.tbl_"+id_venta+"").remove();
                }

                  
                  }
               });
       
              });
             }
             	
          });
        }
      }        
}




function descargar_csv(){ 

  var inicio = $("#fecha_ini").val();
  var final = $("#fecha_fin").val();
	var filas_detalle = $("#consulta tbody tr:first").find("td").html();

	if (filas_detalle == "No hay datos disponibles") {
		Notificacion("No hay datos para mostrar", "warning");
		return false;
	}

	window.open("modulos/reporte_compras_v1.0.0/csv.php?inicio=" + inicio + "&final=" + final);

}

var table_2;
function ver_seriales(id_venta){
        
  $("#modal_seriales").modal("show");

  if ($.fn.DataTable.isDataTable("#consulta_modal")) {
    table_2.clear();
    table_2.destroy();
  }

  //desc
  table_2 = $("#consulta_modal").DataTable({
    "order": [[ 0, "asc" ]],
    "responsive": true,
     "ajax" :{
      "url": "modulos/reporte_compras_v1.0.0/controlador.php",
       "data": {id_venta:id_venta , accion: "cargar_tabla_seriales"},
      "type": "POST"
     },
     "paging" : true,
     "info" : true,
     "searching": true,
     "filter" : true,
     "columns": [
         {data: 'sku'},
         {data: 'referencia'},
         {data: 'serial'}
      ]
     
     });

    
  }



function formato_numero(numero, decimales, separador_decimal, separador_miles){ 
  
  numero = parseFloat(numero);
   if(isNaN(numero)){
    return '';
  }
  
  if(decimales !== undefined){
     numero = numero.toFixed(decimales); //Redondeamos
  }
  
  //Convertimos el punto en separador_decimal
  numero = numero.toString().replace('.', separador_decimal !== undefined ? separador_decimal : ',');
  if(separador_miles){
    // Añadimos los separadores de miles
    var miles = new RegExp('(-?[0-9]+)([0-9]{3})');
     while(miles.test(numero)){
      numero = numero.replace(miles, '$1' + separador_miles + '$2');
     }
  }
   return numero;
  
  }
