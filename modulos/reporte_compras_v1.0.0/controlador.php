<?php

include_once("../../config/mainController.php"); // Incluye el Controlador Principal
include_once("../../modulos/reporte_compras_v1.0.0/modelo.php");	// Incluye el Modelo.
$controller = new mainController; // Instancia a la clase MainController
$modelo = new reporte_compras(); // Instancia a la clase del modelo

try // Try, manejo de Errores
{
	$metodo = $_SERVER['REQUEST_METHOD'];
	$tipo_res = "";
	$response = null; 
	// Se manejaran dos tipos JSON y HTML
	// Dependiendo del método de la petición ejecutaremos la acción correspondiente.
	// Por ahora solo POST, todas las llamadas se haran por POST
	$variables = $_POST;
	if(!isset($_POST['accion'])){echo "0"; return;} // Evita que ocurra un error si no manda accion.
	$accion = $variables['accion'];
	// Dependiendo de la accion se ejecutaran las tareas y se definira el tipo de respuesta.
	switch($accion) {
		
		case 'ConsultarVentas':
			 $tipo_res = 'JSON'; //Definir tipo de respuesta;
			 $fin = $variables['final'];
			 $inicio = $variables['inicio'];
			 $response = $modelo->ConsultarVentas($fin,$inicio);
			 
		break;

		case 'cargar_tabla_seriales':
			$tipo_res = 'JSON'; //Definir tipo de respuesta;
			$id = $variables['id_venta'];
			$response = $modelo->cargar_tabla_seriales($id);
			
	   break;
		
		case 'Datos_CSV':
			 $tipo_res = 'JSON'; //Definir tipo de respuesta;
			 $fin = $variables['fin'];
			 $inicio = $variables['inicio'];
			 $idVende = $variables['idVende'];
			 $idRegio = $variables['idRegio'];
			 $idCircu = $variables['idCircuito'];
			 $idRuta = $variables['idRuta'];
			 $response = $modelo->Datos_CSV($fin,$inicio,$idVende,$idRegio,$idCircu,$idRuta);
			 
		break;

		case 'detalle_venta':
			$tipo_res = 'JSON'; //Definir tipo de respuesta
			$id = $variables['id'];
			$response = $modelo->detalle_venta($id);
	
	   break;
	}

	// Respuestas del Controlador
	if($tipo_res == "JSON")
	{
	  echo json_encode($response,true); // $response será un array con los datos de nuestra respuesta.
	}
	elseif ($tipo_res == "HTML") {
	  echo $response; // $response será un html con el string de nuestra respuesta.
	}
} // Fin Try
catch (Exception $e) {}


