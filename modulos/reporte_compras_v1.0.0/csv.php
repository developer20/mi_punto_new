<?php

ini_set("memory_limit", "3000M");
set_time_limit(3000);

if(!isset($_SESSION)) session_start();

require_once "../../config/Session.php";
require_once "../../config/mainModel.php";

$BD = new BD();
$BD->conectar();

$variables = $_GET;
$condicion = '';
$c1 = '="';
$c2 = '"';

$fin = $variables["final"];
$inicio = $variables["inicio"];
$oSession = unserialize($_SESSION[$GLOBALS["SESION_POS"]]);
$id_distri = $oSession->VSid_distri;
$punto  =  $oSession->VSidpos;

	$sql_dis = "SELECT nombre_corto FROM {$GLOBALS["BD_NAME"]}.distribuidores WHERE id = $id_distri";
	$query_dis = $BD->devolver_array($sql_dis);
	$nombre_corto = $query_dis[0]["nombre_corto"];
	$bd = "{$GLOBALS["BD_DIS"]}" . $nombre_corto . "_" . $id_distri;
          
	 $sql = "SELECT 
                  ven.fecha_venta,ven.hora_venta,r.nombre AS regional,d.nombre AS distribuidor,t.nombre AS circuito,
                  z.nombre AS ruta,pu.idpos AS IdPos,pu.razon nombre_pos,IF(visi.extraruta = 1,'SI','NO') AS Extra_ruta,
                  ref.producto AS referencia,ref.pn AS sku,CONCAT('$c1','',ven.serial,'$c2') AS ICCID,ven.valor_venta,
                  CONCAT(u.nombre,' ',u.apellido) vendedor,IF(ven.tipo = 1,'Simcard','Producto') AS tipo_producto,
                  ven.nro_pedido
		     FROM 
                  {$GLOBALS["BD_NAME"]}.inventario__punto ven
			 LEFT JOIN 
			      $bd.usuarios u
					  ON ven.id_vendedor = u.id  AND u.vendedor = 1
			 LEFT JOIN 
                  {$GLOBALS["BD_NAME"]}.regional r
					 ON ven.regional = r.id
			 LEFT JOIN 
                  {$GLOBALS["BD_NAME"]}.distribuidores d
				    ON ven.distri = d.id
		     LEFT JOIN 
                {$GLOBALS["BD_NAME"]}.territorios t
			        ON ven.territorio = t.id
			 LEFT JOIN 
                {$GLOBALS["BD_NAME"]}.zonas z
			      ON ven.zona = z.id
			 LEFT JOIN 
				{$GLOBALS["BD_POS"]}.puntos pu
				  ON ven.id_pos = pu.idpos
			 LEFT JOIN 
				 {$GLOBALS["BD_NAME"]}.referencias ref
					ON ven.id_referencia = ref.id
			 LEFT JOIN 
				  {$GLOBALS["BD_POS"]}.visitas__check visi
					ON ven.visita = visi.id
			  WHERE
				  ven.distri = $id_distri AND ven.id_combo = 0 AND ven.tipo = 1 
				  AND ven.fecha_venta BETWEEN '$inicio' AND '$fin'  AND ven.id_pos = $punto
			   ORDER BY 
				   ven.fecha_venta,ven.id_vendedor;";

				$res_ven = $BD->devolver_array($sql);
  
if(count($res_ven) > 0){

  $nombre_archivo = "Reporte_de_Compras.csv";
  $encabezados = "Fecha;Hora;Regional;Distribuidor;Circuito;Ruta;IDPOS;Nombre POS;Extra-Ruta;SKU;Nombre Referencia;ICCID;Valor;Vendedor;Tipo producto;Nro Pedido";
  $llaves = array("fecha_venta","hora_venta","regional","distribuidor","circuito","ruta","IdPos","nombre_pos","Extra_ruta","sku","referencia","ICCID","valor_venta","vendedor","tipo_producto","nro_pedido");

  
  $BD->comprimir_archivo("Reporte_de_Ventas_del_Distribuidor",$nombre_archivo,$encabezados,$res_ven,$llaves);

  
}
?>