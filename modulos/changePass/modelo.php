<?php

session_start();
require("../../config/mainModel.php");
require("../../config/Session.php");
require("../../config/mail/mail.php");
require_once("../../config/f_val.php");
require("../../config/log_inc.php");

class Validar {

  public function __construct ()	{		
    $BD = new BD ();
    $this->BD = $BD;
    $this->BD->conectar ();
    $oSession = unserialize ($_SESSION[$GLOBALS["SESION_POS"]]);
    $this->idpos=$oSession->VSidpos;

    //GUARDADO DEL LOG DE USUARIO
			if((isset($_SESSION[$GLOBALS["SESION_POS"]]) && $_SESSION[$GLOBALS["SESION_POS"]])){
				if(isset($GLOBALS["ESTADO_LOG_USUARIO"]) && intval($GLOBALS["ESTADO_LOG_USUARIO"])==1){
					$idUsuario = $this->idpos;
					$usuario = $oSession->VSlogin;
					$cedula = $oSession->VScedula;
					$id_distribuidor = $oSession->VSid_distri;
					$navegador = $oSession->VSnavegador;
					$log_usu = new Log();
					$log_usu -> audi($idUsuario,$usuario,$cedula,$id_distribuidor,$navegador,$_SESSION["mod"],json_encode($_POST));
				}else{
					unset($_SESSION["mod"]);
				}
			}else{
				if(!isset($_SESSION["rpss"])){
					die(json_encode(array("estadoSesion"=>201),true));
				}
			}
		//GUARDADO DEL LOG DE USUARIO FIN
  }

  public function validarReco()
	{ 
		if(isset($_SESSION["rpss"]) && isset($_SESSION["cambio_password"]) && $_SESSION["cambio_password"] == 0){
			
			$rpss = $_SESSION["rpss"];
			
			$sql = "select idpos from puntos where recover = '$rpss' limit 1";
			$res_cosult = $this->BD->consultar($sql);
			if($this->BD->numreg($res_cosult) > 0){

				return 0;
				
			}else{
				return 1;
			}
		}else{
			return 2;
		}
  }
  
  public function datosPunto()
  {
    $sql = "SELECT `razon`, `tipo_doc`, `cedula`, `nombre_cli`, `email`, `depto`, `ciudad`, `barrio`, `tel`, `celular`, `detalle_direccion` FROM datos_punto WHERE idpos = $this->idpos";
    $resDatosPunto = $this->BD->devolver_array($sql);
    
    if(count($resDatosPunto) == 0){
      $sql = "SELECT `razon`, `tipo_doc`, `cedula`, `nombre_cli`, `email`, `depto`, `ciudad`, `barrio`, `tel`, `celular`, `detalle_direccion` FROM puntos WHERE idpos = $this->idpos";
      $resDatosPunto = $this->BD->devolver_array($sql);
    }

    return $resDatosPunto;
  }

  public function cargar_tipo_doc(){
		$c_tipo_documento = "SELECT id, nombre_doc, min_digitos, max_digitos FROM {$GLOBALS["BD_NAME"]}.tipo_documento;";
		return $this->BD->devolver_array($c_tipo_documento);
	}

  public function EnviarPass ($passw, $pass_ant, $user, $datos) {
    if (isset($_SESSION[$GLOBALS["SESION_POS"]])) {
      
      $oSession = unserialize($_SESSION[$GLOBALS["SESION_POS"]]);
      $idpunto = $oSession->VSidpos;
      $login = $oSession->VSlogin;
      
      $cambio_password = intval($_SESSION["cambio_password"]);
      $error = 0;
      $num = 0;
      
      $pass = str_replace(' ', '', $passw);
      $PasswordLimpio = $this->BD->InyeccionSql("$pass");
      $UserLimpio = $this->BD->InyeccionSql("$user");
      
      if ($PasswordLimpio != $passw) {
        return 3;
      } else if ($UserLimpio != $user) {
        return 5;
      } else {
        $sql = "SELECT password,username FROM puntos WHERE idpos=$idpunto";
        $res = $this->BD->consultar($sql);
        
        $passwordBD = $res->fields["password"];
        
        $sql = "SELECT username FROM puntos WHERE idpos != {$idpunto} AND username LIKE '{$user}'";
        $res_val = $this->BD->consultar($sql);
        
        if ($this->BD->numreg($res_val) == 0) {
          $sql = "SELECT idpos FROM audi_puntos WHERE password = '".crypt($passw, $passwordBD)."' and idpos=$idpunto";
          $res_val_pass = $this->BD->consultar($sql);
          
          if ($this->BD->numreg($res_val_pass) == 0) {
            if (crypt($pass_ant, $passwordBD) != $passwordBD) {
              return 4;  // SI EL PASSWORD DIGITADO COMO ANTERIOR ES DIFERENTE AL PASWORD DE LA BD
            } else {
              $pass = movpass($pass);
              
              if (@$_SERVER["HTTPS"] != "off") {
                $url_cli = "https:/";
              } else {
                $url_cli = "http:/";
              }
              
              $url_cli .= '192.168.2.24/web/{$GLOBALS["BD_NAME"]}2/puntorecarga';
              
              //$url_cli=dirname($url_cli);
              
              $recover = md5(rand());
              
              $this->BD->consultar("BEGIN");
              
              $sql = "update puntos set password='$pass',username='$user', recover='$recover', cambio_password = 1 where idpos=$idpunto";
              $res = $this->BD->consultar($sql);
              if (!$res) {
                $error = 1;
              }
              
              $sql2 = "insert into audi_puntos (idpos,username,password,usuario_crea,fecha_crea,hora_crea,movimiento) values( $idpunto,'$user','$pass',$idpunto,curdate(),curtime(),2)";
              $result = $this->BD->consultar($sql2);
              
              if (!$result) {
                $error = 1;
              }

              $error = $this->guardarDatosPunto($datos);
              
              if ($error == 1) {
                $this->BD->consultar("ROLLBACK");
                return 0;
              } else if ($error == 0) {
                $this->BD->consultar("COMMIT");
                $_SESSION["cambio_password"] = 1;
                $this->cambiarSession();
                return 1;
                
              }
            }
          } else {
            return 7;
          }
        } else {
          return 6;
        }
      }
    } else {
      return 2;
    }
  }

  private function guardarDatosPunto($datos)
  {

    $error = false;
    $movimiento = 1;

    $sql = "SELECT id FROM datos_punto WHERE idpos = $this->idpos";
    if(count($this->BD->devolver_array($sql)) > 0){
      $sql = "UPDATE datos_punto SET 
                                  razon = '{$datos["pt_nombre"]}',
                                  tipo_doc = {$datos["tipod"]},
                                  cedula = '{$datos["pt_dni"]}',
                                  nombre_cli = '{$datos["pt_name_cli"]}',
                                  email = '{$datos["pt_email"]}',
                                  depto = {$datos["pt_dpto"]},
                                  ciudad = {$datos["pt_ciudad"]},
                                  barrio = '{$datos["pt_barrio"]}',
                                  tel = '{$datos["pt_tel"]}',
                                  celular = {$datos["pt_cel"]},
                                  detalle_direccion = '{$datos["txtdireccion"]}'
                                WHERE idpos = $this->idpos";
      $result = $this->BD->consultar($sql);
              
      if (!$result) {
        $error = true;
      }

      $movimiento = 2;
    }else{
      $sql = "INSERT INTO `datos_punto`(`idpos`, `razon`, `tipo_doc`, `cedula`, `nombre_cli`, `email`, `depto`, `ciudad`, `barrio`, `tel`, `celular`, `detalle_direccion`) VALUES ($this->idpos, '{$datos["pt_nombre"]}', {$datos["tipod"]}, '{$datos["pt_dni"]}', '{$datos["pt_name_cli"]}', '{$datos["pt_email"]}', {$datos["pt_dpto"]}, {$datos["pt_ciudad"]}, '{$datos["pt_barrio"]}', '{$datos["pt_tel"]}', {$datos["pt_cel"]}, '{$datos["txtdireccion"]}')";

      $result = $this->BD->consultar($sql);
              
      if (!$result) {
        $error = true;
      }
    }

    $sql = "INSERT INTO `datos_punto_audi`(`idpos`, `razon`, `tipo_doc`, `cedula`, `nombre_cli`, `email`, `depto`, `ciudad`, `barrio`, `tel`, `celular`, `detalle_direccion`, `fecha`, `hora`, `movimiento`) SELECT `idpos`, `razon`, `tipo_doc`, `cedula`, `nombre_cli`, `email`, `depto`, `ciudad`, `barrio`, `tel`, `celular`, `detalle_direccion`, CURDATE(), CURTIME(), $movimiento FROM datos_punto WHERE idpos = $this->idpos";

    $resultAudi = $this->BD->consultar($sql);
              
    if (!$resultAudi) {
      $error = true;
    }

    return $error;

  }

  private function cambiarSession()
  {
    $sql = "SELECT idpos,razon,username,cambio_password,id_distri,id_regional,territorio,zona,cedula FROM puntos WHERE idpos = $this->idpos";
    $res = $this->BD->consultar($sql);

		if($this->BD->numreg($res)>0){
      $nombre = $res->fields["razon"];
      $cedula = $res->fields["cedula"];
      
      $sql = "SELECT razon, cedula FROM datos_punto WHERE idpos = $this->idpos";
			$resDatos = $this->BD->consultar($sql);

			if($this->BD->numreg($resDatos)>0){
				$nombre = $resDatos->fields["razon"];
				$cedula = $resDatos->fields["cedula"];
      }
      
      $log = $res->fields["username"];
			$id_distri = $res->fields["id_distri"];
			$regional = $res->fields["id_regional"];
			$territorio = $res->fields["territorio"];
      $zona = $res->fields["zona"];
      $token = $this->generar_token();
      
      $ip = "";

      if (getenv("HTTP_CLIENT_IP") && strcasecmp(getenv("HTTP_CLIENT_IP"), "unknown"))
      $ip = getenv("HTTP_CLIENT_IP");

      else if (getenv("HTTP_X_FORWARDED_FOR") && strcasecmp(getenv("HTTP_X_FORWARDED_FOR"), "unknown"))
      $ip = getenv("HTTP_X_FORWARDED_FOR");

      else if (getenv("REMOTE_ADDR") && strcasecmp(getenv("REMOTE_ADDR"), "unknown"))
      $ip = getenv("REMOTE_ADDR");

      else if (isset($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'] && strcasecmp($_SERVER['REMOTE_ADDR'], "unknown"))
      $ip = $_SERVER['REMOTE_ADDR'];
      else
      $ip = "unknown";

      $navega = "";
      if(isset($navegador["browser"])){
        $navega = $navegador["browser"];
      }

      if($navega == ""){
        $navega = $_SERVER['HTTP_USER_AGENT'];
      }  

      $ipAddress=$_SERVER['REMOTE_ADDR'];
			$macAddr=false;

			#run the external command, break output into lines
			$arp=`arp -a $ipAddress`;
			$lines=explode("\n", $arp);

			#look for the output line describing our IP address
			foreach($lines as $line)
			{
			  $cols=preg_split('/\s+/', trim($line));
			  if ($cols[0]==$ipAddress)
			  {
			      $macAddr=$cols[1];
			  }
      }
      
      $oSession=new session($this->idpos,$nombre,$log,$ip,$id_distri,$token,$navega,$macAddr,$cedula);
			$_SESSION[$GLOBALS["SESION_POS"]] = serialize($oSession);

			$sql = "SELECT id FROM token_puntos WHERE id_pos = $this->idpos";
			$res_exits_to = $this->BD->consultar($sql);

			if($this->BD->numreg($res_exits_to) > 0){
				$sql = "UPDATE token_puntos SET token = '$token',fecha_token = CURDATE(),hora_token = CURTIME() WHERE id_pos = $this->idpos";
				$res_pos = $this->BD->consultar($sql);
			}else{
				$sql = "INSERT INTO token_puntos (id_pos,token,fecha_token,hora_token) VALUES ($this->idpos,'$token',CURDATE(),CURTIME())";
				$res_pos = $this->BD->consultar($sql);
			}

    }
  }

  private function generar_token(){

    $cadena = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
    $longitudCadena=strlen($cadena);
    $code = "";

    for($i=1 ; $i<=15 ; $i++){
      //Definimos numero aleatorio entre 0 y la longitud de la cadena de caracteres-1
      $pos=rand(0,$longitudCadena-1);
      //Vamos formando la contrase–a en cada iteraccion del bucle, a–adiendo a la cadena $pass la letra correspondiente a la posicion $pos en la cadena de caracteres definida.
      $code.= substr($cadena,$pos,1);
    }

    return $code;

 }
}

?>