$(document).ready(function() {

    validarReco();
    cargar_tipo_doc();
    Cargar_Departamento_Ciudad("pt_dpto", "pt_ciudad");

    $("#form_pass").submit(function(event) {
        event.preventDefault();
        autenticarPass();
    });

    $("#pass").keypress(function(event) {
        if (window.event) {
            keynum = event.keyCode;
        } else {
            keynum = event.which;
        }
        if ((keynum > 32)) {
            return true;
        } else {
            return false;
        }
        //validar_espacios(event);
    });

    $("#pass2").keypress(function(event) {
        if (window.event) {
            keynum = event.keyCode;
        } else {
            keynum = event.which;
        }
        if ((keynum > 32)) {
            return true;
        } else {
            return false;
        }
        //validar_espacios(event);
    });

    $("#pass_ant").keypress(function(event) {
        if (window.event) {
            keynum = event.keyCode;
        } else {
            keynum = event.which;
        }
        if ((keynum > 32)) {
            return true;
        } else {
            return false;
        }
        //validar_espacios(event);
    });

    $("#cancelar").click(function() {
        window.location = "?cerrar=S"
    });

    $("#pass").keyup(function() {

        cadena = this.value;
        validarPass(cadena);

    });

    $("#user").keyup(function() {

        cadena = this.value;
        validarUser(cadena);

    });

    $("#siguiente").click(function() {
        let pass = $("#pass").val();
        let user = $("#user").val();
        let val = validar_pass();
        let valPass = validarPass(pass);
        let valUser = validarUser(user);
        var validar = false;

        if (val == 2) {
            Notificacion("Las contraseñas no coinciden", "warning");
            document.getElementById("pass2").focus();
            validar = true;
        } else if (val == 3 || val == 4) {
            Notificacion("No se permite campos vacíos", "warning");
            validar = true;
        } else if (val == 5) {
            Notificacion("Los nombres de usuario no coinciden", "warning");
            document.getElementById("user2").focus();
            validar = true;
        }

        if (valPass) {
            Notificacion("La contraseña no cumple con los requisitos", "warning");
            document.getElementById("pass").focus();
            validar = true;
        }

        if (valUser) {
            Notificacion("El nombre de usuario no cumple con los requisitos", "warning");
            document.getElementById("user").focus();
            validar = true;
        }

        if (!validar) {
            cargarDatosPuntos();
        } else {
            return;
        }

    });

    $("#atras").click(function() {
        $("#datos-acceso").show();
        $("#datos-punto").hide();
        $("#atras").hide();
        $("#siguiente").show();
        $("#botonGuardar").hide();
    })


});

function validarReco() {

    $.post('modulos/changePass/controlador.php', {
            accion: 'validarReco',
        },
        function(data, textStatus) {

            if (data == 1) {
                BootstrapDialog.confirm("No es posible cambiar la contraseña.", function(result) {
                    if (result) {
                        location.href = "?cerrar=S";
                    } else {
                        location.href = "?cerrar=S";
                    }
                });
                setTimeout(function() { window.location = "?cerrar=S" }, 5000);

            } else if (data == 2) {
                BootstrapDialog.confirm("Error, la url no es una url valida", function(result) {
                    if (result) {
                        location.href = "?cerrar=S";
                    } else {
                        location.href = "?cerrar=S";
                    }
                });
                //setTimeout(function() { window.location = "?cerrar=S" }, 5000);
            }

        }
    );

}

function validarPass(cadena) {
    var tamano = false
    var mayus = false
    var minus = false
    var num = false
    var inyeccion = false
    var caracter = false
    var espa = false;
    var tilde = false;
    var inyectss = "";
    var conMin = false;
    var conMay = false;
    var conNum = false;
    var valPass = false;
    var inyecciones = [";", "'", "ALTER", "DROP", "SELECT", "FROM", "WHERE", "INSERT", "DELETE", "*", " OR ", " AND ", "%27", "TABLE", "(", ")", "?", "=", "&", "ñ", "Ñ", ".", ",", "á", "é", "í", "ó", "ú", "à", "è", "ì", "ò", "ù"];
    var le_con_minu = ['abc', 'bcd', 'cde', 'def', 'efg', 'fgh', 'ghi', 'hij', 'ijk', 'jkl', 'klm', 'lmn', 'mno', 'nop', 'opq', 'pqr', 'qrs', 'rst', 'stu', 'tuv', 'uvw', 'vwx', 'xyz'];
    var le_con_mayu = ['ABC', 'BCD', 'CDE', 'DEF', 'EFG', 'FGH', 'GHI', 'HIJ', 'IJK', 'JKL', 'KLM', 'LMN', 'MNO', 'NOP', 'OPQ', 'PQR', 'QRS', 'RST', 'STU', 'TUV', 'UVW', 'VWX', 'XYZ'];
    var num_val = ['012', '123', '234', '345', '456', '567', '678', '789'];
    var numeros = "0123456789";
    var patron_may = /[A-Z]/g;
    var patron_min = /[a-z]/g;
    var patron_til = /[áéíóúàèìòù´`]/g;

    for (i = 0; i < cadena.length; i++) {

        if (cadena.match(patron_may) != null) {
            mayus = true
        }

        if (cadena.match(patron_til) != null) {
            tilde = true;
        }

        if (cadena.match(patron_min) != null) {
            minus = true;
        }

        if (numeros.indexOf(cadena.charAt(i), 0) != -1) {
            num = true

        }

        for (var n = 0; n < le_con_minu.length; n++) {
            if (cadena.indexOf(le_con_minu[n]) != -1) {
                conMin = true;
            }
        }

        for (var y = 0; y < le_con_mayu.length; y++) {
            if (cadena.indexOf(le_con_mayu[y]) != -1) {
                conMay = true;
            }
        }

        for (var z = 0; z < num_val.length; z++) {
            if (cadena.indexOf(num_val[z]) != -1) {
                conNum = true;
            }
        }

        if (cadena.charAt(i) == " ") {
            espa = true;
        }

    }

    for (i = 0; i < cadena.length; i++) {

        cadena = cadena.toUpperCase();
        for (var z = 0; z < inyecciones.length; z++) {
            if (cadena.indexOf(inyecciones[z]) != -1) {
                inyeccion = true;
                inyectss = inyecciones[z];
            }
        }

    }

    if (cadena.length < 7 || cadena.length > 20) {
        tamano = true;
    }

    var mensaje = "";

    if (!tamano) {
        $("#all_1").css("color", "#048004");
        $("#pass_1").removeClass("glyphicon-unchecked");
        $("#pass_1").addClass("glyphicon-check");
    } else {
        $("#all_1").css("color", "#333");
        $("#pass_1").removeClass("glyphicon-check");
        $("#pass_1").addClass("glyphicon-unchecked");
        mensaje = "<div>La contraseña nueva debe ser mínimo de 7 caracteres y máximo de 20</div>";
        document.getElementById("pass").focus()
        valPass = true;
    }

    if (num) {
        $("#all_2").css("color", "#048004");
        $("#pass_2").removeClass("glyphicon-unchecked");
        $("#pass_2").addClass("glyphicon-check");
    } else {
        $("#all_2").css("color", "#333");
        $("#pass_2").removeClass("glyphicon-check");
        $("#pass_2").addClass("glyphicon-unchecked");
        mensaje = "<div>La contraseña debe tener minimo un número</div>";
        document.getElementById("pass").focus()
        valPass = true;
    }

    if (mayus) {
        $("#all_3").css("color", "#048004");
        $("#pass_3").removeClass("glyphicon-unchecked");
        $("#pass_3").addClass("glyphicon-check");
    } else {
        $("#all_3").css("color", "#333");
        $("#pass_3").removeClass("glyphicon-check");
        $("#pass_3").addClass("glyphicon-unchecked");
        mensaje = "<div>La contraseña debe tener minimo una mayuscula</div>";
        document.getElementById("pass").focus()
        valPass = true;
    }

    if (minus) {
        $("#all_4").css("color", "#048004");
        $("#pass_4").removeClass("glyphicon-unchecked");
        $("#pass_4").addClass("glyphicon-check");
    } else {
        $("#all_4").css("color", "#333");
        $("#pass_4").removeClass("glyphicon-check");
        $("#pass_4").addClass("glyphicon-unchecked");
        mensaje = "<div>La contraseña debe tener minimo una mayuscula</div>";
        document.getElementById("pass").focus()
        valPass = true;
    }

    if (!espa) {
        $("#all_5").css("color", "#048004");
        $("#pass_5").removeClass("glyphicon-unchecked");
        $("#pass_5").addClass("glyphicon-check");
    } else {
        $("#all_5").css("color", "#333");
        $("#pass_5").removeClass("glyphicon-check");
        $("#pass_5").addClass("glyphicon-unchecked");
        mensaje = "<div>No debe contener espacio</div>";
        valPass = true;
    }

    if (!conMin && !conMay) {
        $("#all_7").css("color", "#048004");
        $("#pass_7").removeClass("glyphicon-unchecked");
        $("#pass_7").addClass("glyphicon-check");
    } else {
        $("#all_7").css("color", "#333");
        $("#pass_7").removeClass("glyphicon-check");
        $("#pass_7").addClass("glyphicon-unchecked");
        mensaje = "<div>La contraseña no puede tener mas de 3 letras consecutivas</div>";
        document.getElementById("pass").focus()
        valPass = true;
    }

    if (!conNum) {
        $("#all_8").css("color", "#048004");
        $("#pass_8").removeClass("glyphicon-unchecked");
        $("#pass_8").addClass("glyphicon-check");
    } else {
        $("#all_8").css("color", "#333");
        $("#pass_8").removeClass("glyphicon-check");
        $("#pass_8").addClass("glyphicon-unchecked");
        mensaje = "<div>La contraseña no puede tener mas de 3 numeros consecutivos</div>";
        document.getElementById("pass").focus()
        valPass = true;
    }

    if (!inyeccion) {
        $("#all_9").css("color", "#048004");
        $("#pass_9").removeClass("glyphicon-unchecked");
        $("#pass_9").addClass("glyphicon-check");
    } else {
        $("#all_9").css("color", "#333");
        $("#pass_9").removeClass("glyphicon-check");
        $("#pass_9").addClass("glyphicon-unchecked");
        mensaje = "<div>No debe contener palabra reservada: '" + inyectss + "'</div>";
        document.getElementById("pass").focus()
        valPass = true;
    }

    if (tilde) {
        mensaje = "<div>La contraseña no debe tener tildes</div>";
        document.getElementById("pass").focus()
        valPass = true;
    }

    if (cadena.length == 0) {
        $("#all_1").css("color", "#333");
        $("#pass_1").removeClass("glyphicon-check");
        $("#pass_1").addClass("glyphicon-unchecked");

        $("#all_2").css("color", "#333");
        $("#pass_2").removeClass("glyphicon-check");
        $("#pass_2").addClass("glyphicon-unchecked");

        $("#all_3").css("color", "#333");
        $("#pass_3").removeClass("glyphicon-check");
        $("#pass_3").addClass("glyphicon-unchecked");

        $("#all_4").css("color", "#333");
        $("#pass_4").removeClass("glyphicon-check");
        $("#pass_4").addClass("glyphicon-unchecked");

        $("#all_5").css("color", "#333");
        $("#pass_5").removeClass("glyphicon-check");
        $("#pass_5").addClass("glyphicon-unchecked");

        $("#all_7").css("color", "#333");
        $("#pass_7").removeClass("glyphicon-check");
        $("#pass_7").addClass("glyphicon-unchecked");

        $("#all_8").css("color", "#333");
        $("#pass_8").removeClass("glyphicon-check");
        $("#pass_8").addClass("glyphicon-unchecked");

        $("#all_9").css("color", "#333");
        $("#pass_9").removeClass("glyphicon-check");
        $("#pass_9").addClass("glyphicon-unchecked");
    }


    $("#mensaje_pass").html(mensaje);

    return valPass;
}

function validarUser(cadena) {
    var tamano = false
    var mayus = false
    var minus = false
    var num = false
    var inyeccion = false
    var caracter = false
    var tilde = false
    var espa = false;
    var inyectss = "";
    var conMin = false;
    var conMay = false;
    var conNum = false;
    var valUser = false;
    var inyecciones = [";", "'", "ALTER", "DROP", "SELECT", "FROM", "WHERE", "INSERT", "DELETE", "*", " OR ", " AND ", "%27", "TABLE", "(", ")", "?", "=", "&", "__", '"', "%", "ñ", "Ñ", ",", "á", "é", "í", "ó", "ú", "à", "è", "ì", "ò", "ù"];
    var le_con_minu = ['abc', 'bcd', 'cde', 'def', 'efg', 'fgh', 'ghi', 'hij', 'ijk', 'jkl', 'klm', 'lmn', 'mno', 'nop', 'opq', 'pqr', 'qrs', 'rst', 'stu', 'tuv', 'uvw', 'vwx', 'xyz'];
    var le_con_mayu = ['ABC', 'BCD', 'CDE', 'DEF', 'EFG', 'FGH', 'GHI', 'HIJ', 'IJK', 'JKL', 'KLM', 'LMN', 'MNO', 'NOP', 'OPQ', 'PQR', 'QRS', 'RST', 'STU', 'TUV', 'UVW', 'VWX', 'XYZ'];
    var num_val = ['012', '123', '234', '345', '456', '567', '678', '789'];
    var numeros = "0123456789";
    var patron_may = /[A-Z]/g;
    var patron_min = /[a-z]/g;
    var patron_til = /[áéíóúàèìòù´`ÁÉÍÓÚÀÈÌÒÙ]/g;

    for (i = 0; i < cadena.length; i++) {

        if (cadena.match(patron_may) != null) {

            mayus = true
        }

        if (cadena.match(patron_til) != null) {

            tilde = true
        }

        if (cadena.match(patron_min) != null) {
            minus = true;
        }

        if (numeros.indexOf(cadena.charAt(i), 0) != -1) {
            num = true
        }

        for (var n = 0; n < le_con_minu.length; n++) {
            if (cadena.indexOf(le_con_minu[n]) != -1) {
                conMin = true;
            }
        }

        for (var y = 0; y < le_con_mayu.length; y++) {
            if (cadena.indexOf(le_con_mayu[y]) != -1) {
                conMay = true;
            }
        }

        for (var z = 0; z < num_val.length; z++) {
            if (cadena.indexOf(num_val[z]) != -1) {
                conNum = true;
            }
        }

        if (cadena.charAt(i) == " ") {
            espa = true;
        }

    }

    for (i = 0; i < cadena.length; i++) {

        cadena = cadena.toUpperCase();
        for (var z = 0; z < inyecciones.length; z++) {
            if (cadena.indexOf(inyecciones[z]) != -1) {
                inyeccion = true;
                inyectss = inyecciones[z];
            }
        }

    }

    if (cadena.length < 6 || cadena.length > 15) {
        tamano = true;
    }

    var mensaje = "";

    if (!espa) {
        $("#allu_1").css("color", "#048004");
        $("#user_1").removeClass("glyphicon-unchecked");
        $("#user_1").addClass("glyphicon-check");
    } else {
        $("#allu_1").css("color", "#333");
        $("#user_1").removeClass("glyphicon-check");
        $("#user_1").addClass("glyphicon-unchecked");
        mensaje = "<div>No se permite que el nombre de usuario contenga espacios</div>";
        document.getElementById("user").focus()
        valUser = true;
    }

    if (num) {
        $("#allu_2").css("color", "#048004");
        $("#user_2").removeClass("glyphicon-unchecked");
        $("#user_2").addClass("glyphicon-check");
    } else {
        $("#allu_2").css("color", "#333");
        $("#user_2").removeClass("glyphicon-check");
        $("#user_2").addClass("glyphicon-unchecked");
        mensaje = "<div>El nombre de usuario debe contener por lo menos un numero</div>";
        document.getElementById("user").focus()
        valUser = true;
    }

    if (!tamano) {
        $("#allu_3").css("color", "#048004");
        $("#user_3").removeClass("glyphicon-unchecked");
        $("#user_3").addClass("glyphicon-check");
    } else {
        $("#allu_3").css("color", "#333");
        $("#user_3").removeClass("glyphicon-check");
        $("#user_3").addClass("glyphicon-unchecked");
        mensaje = "<div>El nombre de usuario debe ser mínimo de 6 caracteres y máximo de 15</div>";
        document.getElementById("user").focus()
        valUser = true;
    }

    if (!conMin && !conMay) {
        $("#allu_4").css("color", "#048004");
        $("#user_4").removeClass("glyphicon-unchecked");
        $("#user_4").addClass("glyphicon-check");
    } else {
        $("#allu_4").css("color", "#333");
        $("#user_4").removeClass("glyphicon-check");
        $("#user_4").addClass("glyphicon-unchecked");
        mensaje = "<div>El nombre de usuario no puede tener mas de 3 letras consecutivas</div>";
        document.getElementById("user").focus()
        valUser = true;
    }

    if (!conNum) {
        $("#allu_5").css("color", "#048004");
        $("#user_5").removeClass("glyphicon-unchecked");
        $("#user_5").addClass("glyphicon-check");
    } else {
        $("#allu_5").css("color", "#333");
        $("#user_5").removeClass("glyphicon-check");
        $("#user_5").addClass("glyphicon-unchecked");
        mensaje = "<div>El nombre de usuario no puede tener mas de 3 numeros consecutivos</div>";
        document.getElementById("user").focus()
        valUser = true;
    }

    if (!inyeccion) {
        $("#allu_6").css("color", "#048004");
        $("#user_6").removeClass("glyphicon-unchecked");
        $("#user_6").addClass("glyphicon-check");
    } else {
        $("#allu_6").css("color", "#333");
        $("#user_6").removeClass("glyphicon-check");
        $("#user_6").addClass("glyphicon-unchecked");
        mensaje = "<div>No debe contener palabras o caracteres como " + inyectss + " entre el nombre de usuario</div>";
        document.getElementById("user").focus()
        valUser = true;
    }

    if (tilde) {
        mensaje = "<div>El nombre de usuario no puede tener tildes</div>";
        document.getElementById("user").focus()
        valUser = true;
    }

    if (cadena.length == 0) {
        $("#allu_1").css("color", "#333");
        $("#user_1").removeClass("glyphicon-check");
        $("#user_1").addClass("glyphicon-unchecked");

        $("#allu_2").css("color", "#333");
        $("#user_2").removeClass("glyphicon-check");
        $("#user_2").addClass("glyphicon-unchecked");

        $("#all2_3").css("color", "#333");
        $("#user_3").removeClass("glyphicon-check");
        $("#user_3").addClass("glyphicon-unchecked");

        $("#allu_4").css("color", "#333");
        $("#user_4").removeClass("glyphicon-check");
        $("#user_4").addClass("glyphicon-unchecked");

        $("#allu_5").css("color", "#333");
        $("#user_5").removeClass("glyphicon-check");
        $("#user_5").addClass("glyphicon-unchecked");

        $("#allu_6").css("color", "#333");
        $("#user_6").removeClass("glyphicon-check");
        $("#user_6").addClass("glyphicon-unchecked");
    }

    $("#mensaje_user").html(mensaje);

    return valUser;
}

function autenticarPass() {

    let pass = document.getElementById("pass").value;
    let pass_ant = document.getElementById("pass_ant").value;
    let user = document.getElementById("user").value;

    let pt_nombre = document.getElementById("pt_nombre").value;
    let pt_name_cli = document.getElementById("pt_name_cli").value;
    let tipod = document.getElementById("tipod").value;
    let pt_dni = document.getElementById("pt_dni").value;
    let pt_dpto = document.getElementById("pt_dpto").value;
    let pt_ciudad = document.getElementById("pt_ciudad").value;
    let pt_tel = document.getElementById("pt_tel").value;
    let pt_cel = document.getElementById("pt_cel").value;
    let pt_email = document.getElementById("pt_email").value;
    let pt_barrio = document.getElementById("pt_barrio").value;
    let txtdireccion = document.getElementById("txtdireccion").value;


    BootstrapDialog.confirm('¿Esta seguro que la información suministrada es correcta?', function(result) {
        if (result) {

            //$(".panel-footer").html("Espera un momento... ");

            $.post('modulos/changePass/controlador.php', {
                    accion: 'EnviarPass',
                    pass: pass,
                    pass_ant: pass_ant,
                    user: user,
                    pt_nombre: pt_nombre,
                    pt_name_cli: pt_name_cli,
                    tipod: tipod,
                    pt_dni: pt_dni,
                    pt_dpto: pt_dpto,
                    pt_ciudad: pt_ciudad,
                    pt_tel: pt_tel,
                    pt_cel: pt_cel,
                    pt_email: pt_email,
                    pt_barrio: pt_barrio,
                    txtdireccion: txtdireccion
                },
                function(data, textStatus) {
                    if (data == 1) {
                        localStorage.setItem('welcome', 1);
                        localStorage.setItem('est_pss_pos', 1)
                        BootstrapDialog.confirm("Se ha realizado los cambios con éxito", function(result) {
                            if (result) {
                                location.href = "?cerrar=S";
                            } else {
                                location.href = "?cerrar=S";
                            }
                        });
                        setTimeout(function() { window.location = "?mod=principal" }, 5000);
                    } else if (data == 2) {
                        BootstrapDialog.confirm("Error, tu sesión ha cerrado", function(result) {
                            if (result) {
                                location.href = "?cerrar=S";
                            } else {
                                location.href = "?cerrar=S";
                            }
                        });
                        setTimeout(function() { window.location = "?cerrar=S" }, 5000);
                    } else if (data == 3) {
                        Notificacion("La contraseña enviada no es una contraseña valida", "error");
                    } else if (data == 4) {
                        Notificacion("La contraseña anterior enviada no es la misma contraseña que se encuentra registrada", "error");
                    } else if (data == 5) {
                        Notificacion("El nombre de usuario enviada no es valido", "error");
                    } else if (data == 6) {
                        Notificacion("El nombre de usuario que ingresastes ya se encuentra registrado", "error");
                    } else if (data == 7) {
                        Notificacion("Ya habías utilizado esta contraseña anteriormente, por favor ingresa una nueva", "error");
                    } else if (data == 0) {
                        Notificacion("Error al guardar los datos", "error");
                    }
                }
            );

        }
    });

}

function validar_pass() {

    pass = document.getElementById("pass").value;
    pass2 = document.getElementById("pass2").value;
    user = document.getElementById("user").value;
    user2 = document.getElementById("user2").value;
    var valida = 0;

    if (pass == "" || pass2 == "") {
        valida = 3;
    } else if (pass != "" && pass2 != "" && pass != pass2) {
        valida = 2;
    } else if (user == "" || user2 == "") {
        valida = 4;
    } else if (user != "" && user2 != "" && user != user2) {
        valida = 5;
    }

    return valida;

}

function cargarDatosPuntos() {
    $("#datos-acceso").hide();
    $("#datos-punto").show();
    $("#atras").show();
    $("#siguiente").hide();
    $("#botonGuardar").show();
    cargarDatosPuntoBd();
}

function cargarDatosPuntoBd() {
    $.post('modulos/changePass/controlador.php', {
            accion: 'datosPunto',
        },
        function(data, textStatus) {

            data = JSON.parse(data);

            if (data.length > 0) {
                $.each(data, function(index, row) {
                    $("#pt_nombre").val(row.razon);
                    $("#tipod").val(row.tipo_doc).change();
                    $("#pt_dni").val(row.cedula);
                    $("#pt_name_cli").val(row.nombre_cli);
                    $("#pt_nombre").val(row.razon);
                    $("#pt_email").val(row.email);
                    $("#pt_dpto").val(row.depto).change();
                    $("#pt_ciudad").val(row.ciudad).change();
                    $("#pt_tel").val(row.tel);
                    $("#pt_cel").val(row.celular);
                    $("#pt_barrio").val(row.barrio);
                    $("#txtdireccion").val(row.detalle_direccion);
                })
            }

        }
    );
}

function cargar_tipo_doc() {
    $.post("modulos/changePass/controlador.php", {
            accion: "cargar_tipo_doc"
        },
        function(data, textStatus) { //tipod
            data = JSON.parse(data);
            var select_hmtl = "<option value=\"\">SELECCIONAR</option>";
            $.each(data, function(index, file) {
                select_hmtl += "<option value = \"" + file.id + "\" data-mindig = \"" + file.min_digitos + "\" data-maxdig = \"" + file.max_digitos + "\" >" + file.nombre_doc + "</option>";
            });
            $("#tipod").html(select_hmtl).change();

        }
    );
}


//DESHABILITO LA BARRA ESPACIADORA PARA QUE EL USUARIO NO INGRESE ESPACIOS AL CREAR LA CLAVE NUEVA
function validar_espacios(evt) {
    if (window.event) {
        keynum = evt.keyCode;
    } else {
        keynum = evt.which;
    }
    if ((keynum > 32)) {
        return true;
    } else {
        return false;
    }
}