var dtable, dtable_distri;

$(document).ready(function() {
  $(".fecha").datepicker({
    format: "yyyy-mm-dd",
    todayHighlight: true,
    todayBtn: true
  });

  $("#plataforma").change(function(e) {
    $("#distri").val("");
    $("#consulta, #consulta_distri").hide();

    if ($(this).val() == 1) {
      // OPERADOR
      $("#d_plataforma").addClass("col-md-offset-3");
      $("#d_distri").hide();
    } else if ($(this).val() == 2) {
      // DISTRIBUIDOR
      $("#d_plataforma").removeClass("col-md-offset-3");
      $("#d_distri").show();
      cargar_distri();
    } else {
      $("#d_plataforma").addClass("col-md-offset-3");
      $("#d_distri").hide();
    }
  });

  $("#btn_buscar").click(function(e) {
    cargar_log_op();
  });

  $("#CSV, #CSV_distri").click(function(e) {
    cargar_csv();
  });
});

function cargar_distri() {
  $.post(
    "modulos/reporte_log_pdvs_v1.0.0/controlador.php",
    {
      accion: "cargar_distri"
    },
    function(data, textStatus) {
      data = JSON.parse(data);
      var html_Select = "<option value=''>Seleccionar...</option>";
      if (!$.isEmptyObject(data)) {
        $.each(data, function(index, row) {
          html_Select +=
            "<option value='" + row.id + "'>" + row.nombre + "</option>";
        });
      }
      $("#distri")
        .html(html_Select)
        .change();
    }
  );
}

function cargar_log_op() {
  $("#consulta, #consulta_distri").hide();

  var f_ini = $("#f_ini").val();
  var f_fin = $("#f_fin").val();
  var plataforma = $("#plataforma").val();
  var distri = $("#distri").val();

  if (f_ini == "" || f_fin == "" || plataforma == "") {
    Notificacion(
      "El campo de fecha inicial, fecha final y plataforma son obligatorios",
      "warning"
    );

    return false;
  }
  $.post(
    "modulos/reporte_log_pdvs_v1.0.0/controlador.php",
    {
      accion: "cargar_log_op",
      f_ini: f_ini,
      f_fin: f_fin,
      plataforma: plataforma,
      distri: distri
    },
    function(data, textStatus) {
      data = JSON.parse(data);
      var colums = {};
      var function_c = function() {};

      if (plataforma == 1) {
        if ($.fn.DataTable.isDataTable("#t_general")) {
          dtable.destroy();
        }

        dtable = $("#t_general").DataTable({
          bFilter: false,
          responsive: true,
          data: data,
          columns: [{ data: "nombre" }, { data: "usuarios" }]
        });

        $("#consulta").show();

        dtable.columns.adjust().draw();
      } else if (plataforma == 2) {
        if ($.fn.DataTable.isDataTable("#t_general_distri")) {
          dtable_distri.destroy();
        }

        colums = {
          targets: 2,
          data: "nombre",
          render: function(data, type, row) {
            return (
              "<i style='cursor:pointer; font-size:19px;' data-distri='" +
              data +
              "' data-iddistri='" +
              row["id"] +
              "' class='glyphicon glyphicon-plus-sign det_dist text-primary'></i>"
            );
          }
        };

        function_c = function() {
          $(".det_dist").unbind("click");
          $(".det_dist").click(function() {
            var tr = $(this)
              .parent()
              .parent();
            var distri = $(this).attr("data-distri");
            var iddistri = $(this).attr("data-iddistri");
            var tab;
            tab = $("body").find("#distri_" + iddistri);

            if (!$.contains(window.document, tab[0])) {
              $(
                "<tr id='distri_" + iddistri + "'><td colspan='3'></td></tr>"
              ).insertAfter($(tr));
              $(this).removeClass("glyphicon-plus-sign");
              $(this).addClass("glyphicon-minus-sign");
              cargar_det_distri("#distri_" + iddistri, distri, iddistri);
            } else {
              $("#distri_" + iddistri).remove();
              $(this).addClass("glyphicon-plus-sign");
              $(this).removeClass("glyphicon-minus-sign");
            }
          });
        };

        dtable_distri = $("#t_general_distri").DataTable({
          bFilter: false,
          responsive: true,
          data: data,
          columns: [
            { data: "nombre" },
            { data: "usuarios" },
            { data: "usuarios" }
          ],
          columnDefs: [colums],
          fnDrawCallback: function_c
        });

        $("#consulta_distri").show();

        dtable_distri.columns.adjust().draw();
      }
    }
  );
}

function cargar_det_distri(target, distri, id_distri) {
  var f_ini = $("#f_ini").val();
  var f_fin = $("#f_fin").val();

  $.post(
    "modulos/reporte_log_pdvs_v1.0.0/controlador.php",
    {
      accion: "cargar_det_distri",
      f_ini: f_ini,
      f_fin: f_fin,
      id_distri: id_distri,
      distri: distri
    },
    function(data, textStatus) {
      var reporte = "";
      data = JSON.parse(data);
      var html = "";
      html +=
        "<div class='col-md-12' style='border:2px #ccc solid; border-radius:2px;'><table class='table table-hovered table-condensed'>";
      html += "<tr><th>Perfil</th><th>Cantidad usuarios</th></thead><tbody>";
      $.each(data, function(index, val) {
        html += "<tr>";
        html += "<td>" + val.nombre + "</td>";
        html += "<td>" + val.usuarios + "</td>";
        html += "</tr>";
      });
      html += "</tbody></table></div></td>";
      $(target)
        .find("td")
        .html(html);
    }
  );
}

function cargar_csv() {
  var f_ini = $("#f_ini").val();
  var f_fin = $("#f_fin").val();
  var plataforma = $("#plataforma").val();
  var distri = $("#distri").val();
  window.open(
    "modulos/reporte_log_pdvs_v1.0.0/reporte_csv.php?f_ini=" +
      f_ini +
      "&f_fin=" +
      f_fin +
      "&plataforma=" +
      plataforma +
      "&distri=" +
      distri
  );
}
