<?php
require_once "../../config/mainModel.php";

class reporte_log {
  public function __construct() {
    $BD = new BD();
    $this->BD = $BD;
    $this->BD->conectar();
  }

  public function __destruct() {
    $this->BD->desconectar();
  }

  public function cargar_distri() {
    $c_distri = "SELECT id,nombre FROM distribuidores WHERE estado=1;";
    return $this->BD->devolver_array($c_distri);
  }

  public function cargar_log_op($f_ini, $f_fin, $plataforma, $distri) {
    $num_f_ini = intval(date("Ymd", strtotime($f_ini)));
    $num_f_fin = intval(date("Ymd", strtotime($f_fin)));
    $array_perfiles = array();
    $array_distrs = array();
    if ($plataforma == 1) { //OPERADOR
      /*
      Operador:
      Fecha,Hora,Id_Usuario,Cedula_Usuario,Nombre_Usuario,Id_Perfil,Nombre_Perfil,Ip,Modulo,Datos
      Distribuidor
      Fecha,Hora,Id_Distri,Nombre_Distri,Id_Usuario,Cedula_Usuario,Nombre_Usuario,id_Perfil_app,Id_Perfil,Nombre_Perfil,Ip,Modulo,Datos
       */
      $array_usu_ac = array();
      for ($m = $num_f_ini; $m <= $num_f_fin; $m++) {
        $ruta_log = $s_logFile = $GLOBALS["LOG_POS"] . "pos_" . $m . ".log";

        
        //echo $ruta_log;exit;
        if (file_exists($ruta_log)) {
          
          $array_datos = explode("\n", file_get_contents($ruta_log));

          //CABECERAS:
          //Fecha,Hora,Id_Usuario,Cedula_Usuario,Nombre_Usuario,Id_Perfil,Nombre_Perfil,Ip,Modulo,Datos

          for ($i = 0; $i < count($array_datos) - 1; $i++) {
            $array_campos = explode("|", $array_datos[$i]);
            if(isset($array_campos[2])){
              $array_usu_ac[$array_campos[2]] = array("id" => $array_campos[2], "id_perfil" => $array_campos[5], "perfil" => $array_campos[6]);
            }
          }
        }
      }
      foreach ($array_usu_ac as $usu_ac) {
        $array_perfiles[$usu_ac["perfil"]]["nombre"] = $usu_ac["perfil"];
        if (isset($array_perfiles[$usu_ac["perfil"]]["usuarios"])) {
          $array_perfiles[$usu_ac["perfil"]]["usuarios"] += 1;
        } else {
          $array_perfiles[$usu_ac["perfil"]]["usuarios"] = 1;
        }
      }
      sort($array_perfiles);
      return $array_perfiles;
    } else if ($plataforma == 2) { //distribuidor

      $c_distri = "SELECT id FROM distribuidores order by id";
      $r_distri = $this->BD->devolver_array($c_distri);
      foreach ($r_distri AS $dist) {
        $array_usu_ac = array();
        for ($m = $num_f_ini; $m <= $num_f_fin; $m++) {
          $ruta_log = $s_logFile = $GLOBALS["LOG_DIS"] . "dis_" . $dist["id"] . "_" . $m . ".log";
          if (file_exists($ruta_log)) {

            $array_datos = explode("\n", file_get_contents($ruta_log));
            //CABECERAS:
            //Fecha,Hora,Id_Distri,Nombre_Distri,Id_Usuario,Cedula_Usuario,Nombre_Usuario,id_Perfil_app,Id_Perfil,Nombre_Perfil,Ip,Modulo,Datos

            for ($i = 0; $i < count($array_datos) - 1; $i++) {
              $array_campos = explode("|", $array_datos[$i]);
              if ($distri != "") {
                if (isset($array_campos[2]) && intval($array_campos[2]) == intval($distri)) {
                  $array_usu_ac[$array_campos[4]] = array("id" => $array_campos[4], "id_Distri" => $array_campos[2], "distri" => $array_campos[3]);
                }
              } else {
                $array_usu_ac[$array_campos[4]] = array("id" => $array_campos[4], "id_Distri" => $array_campos[2], "distri" => $array_campos[3]);
              }

            }
          }

        }
        foreach ($array_usu_ac as $usu_ac) {
          $array_distrs[$usu_ac["distri"]]["nombre"] = $usu_ac["distri"];
          $array_distrs[$usu_ac["distri"]]["id"] = $usu_ac["id_Distri"];
          if (isset($array_distrs[$usu_ac["distri"]]["usuarios"])) {
            $array_distrs[$usu_ac["distri"]]["usuarios"] += 1;
          } else {
            $array_distrs[$usu_ac["distri"]]["usuarios"] = 1;
          }
        }
      }
      //print_r($array_usu_ac);
      sort($array_distrs);
      return $array_distrs;
    }

  }

  function cargar_det_distri($f_ini, $f_fin, $id_distri, $distri) {
    $num_f_ini = intval(date("Ymd", strtotime($f_ini)));
    $num_f_fin = intval(date("Ymd", strtotime($f_fin)));
    $array_distrs = array();
    $array_usu_ac = array();
    for ($m = $num_f_ini; $m <= $num_f_fin; $m++) {
      $ruta_log = $s_logFile = $GLOBALS["LOG_DIS"] . "dis_" . $id_distri . "_" . $m . ".log";
      if (file_exists($ruta_log)) {

        $array_datos = explode("\n", file_get_contents($ruta_log));

        for ($i = 0; $i < count($array_datos) - 1; $i++) {
          $array_campos = explode("|", $array_datos[$i]);

          //if(strcmp($distri,$array_campos[3])!=0){
          $array_usu_ac[$array_campos[4]] = array("id" => $array_campos[4], "perfil" => $array_campos[9], "distri" => $array_campos[3]);
          //}
        }
      }
    }

    foreach ($array_usu_ac as $usu_ac) {
      $array_distrs[$usu_ac["perfil"]]["nombre"] = $usu_ac["perfil"];
      if (isset($array_distrs[$usu_ac["perfil"]]["usuarios"])) {
        $array_distrs[$usu_ac["perfil"]]["usuarios"] += 1;
      } else {
        $array_distrs[$usu_ac["perfil"]]["usuarios"] = 1;
      }
    }

    sort($array_distrs);
    return $array_distrs;
  }
}