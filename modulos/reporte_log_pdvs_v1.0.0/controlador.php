<?php
require_once "../../config/mainController.php";
require_once "../../modulos/reporte_log_pdvs_v1.0.0/modelo.php";

$controller = new mainController;
$modelo = new reporte_log();

try {
  $metodo = $_SERVER['REQUEST_METHOD'];
  $tipo_res = "";
	$response = null;
	$variables = $_POST;

	// Evita que ocurra un error si no manda accion
	if (!isset($_POST['accion'])) { echo "0";return; }
	
	$accion = $variables['accion'];
	
  // Dependiendo de la accion se ejecutaran las tareas y se definira el tipo de respuesta.
  switch ($accion) {
		case 'cargar_distri':
			$tipo_res = 'JSON'; //Definir tipo de respuesta;
			$response = $modelo->cargar_distri();
			break;
		case 'cargar_log_op':
			$tipo_res = 'JSON';
			$f_ini = $variables["f_ini"];
			$f_fin = $variables["f_fin"];
			$plataforma = $variables["plataforma"];
			$distri = $variables["distri"];
			$response = $modelo->cargar_log_op($f_ini, $f_fin, $plataforma, $distri);
			break;
		case 'cargar_det_distri':
			$tipo_res = 'JSON';
			$f_ini = $variables["f_ini"];
			$f_fin = $variables["f_fin"];
			$id_distri = $variables["id_distri"];
			$distri = $variables["distri"];
			$response = $modelo->cargar_det_distri($f_ini, $f_fin, $id_distri, $distri);
			break;
  }

  // Respuestas del Controlador
  if ($tipo_res == "JSON") {
    echo json_encode($response, true); // $response será un array con los datos de nuestra respuesta.
  } elseif ($tipo_res == "HTML") {
    echo $response; // $response será un html con el string de nuestra respuesta.
  }
}catch (Exception $e) {}