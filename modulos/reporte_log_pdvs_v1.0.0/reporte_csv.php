<?php
ini_set('memory_limit', '3000M');
set_time_limit(3000);

if (!isset($_SESSION)) {
  session_start();
}

require_once "../../config/Session.php";
require_once "../../config/mainModel.php";

$BD = new BD();
$BD->conectar();

/*// CABECERAS
header("Content-Encoding: UTF-8");
header("Content-type: text/csv; charset=UTF-8");
header("Content-Disposition: attachment; filename=\"REPORTE_LOG_USUARIOS.csv\";");

// UTF-8 BOM
echo "\xEF\xBB\xBF";*/

$resultado = array();
$variables = $_GET;

$f_ini = $variables['f_ini'];
$f_fin = $variables['f_fin'];
$plataforma = $variables['plataforma'];
$distri = $variables['distri'];

$num_f_ini = intval(date("Ymd", strtotime($f_ini)));
$num_f_fin = intval(date("Ymd", strtotime($f_fin)));

if ($plataforma == 1) { //OPERADOR
  //echo "Fecha;Hora;Usuario;Perfil;Modulo";
  for ($m = $num_f_ini; $m <= $num_f_fin; $m++) {
    $ruta_log = $s_logFile = $GLOBALS["LOG_OP"] . "op_" . $m . ".log";

    if (file_exists($ruta_log)) {

      $array_datos = explode("\n", file_get_contents($ruta_log));
      //CABECERAS:
      //Fecha,Hora,Id_Usuario,Cedula_Usuario,Nombre_Usuario,Id_Perfil,Nombre_Perfil,Ip,Modulo,Datos
      $hora_l_tem = "";
      for ($i = 0; $i < count($array_datos) - 1; $i++) {
        $array_campos = explode("|", $array_datos[$i]);
        //Fecha,Hora,Usuario,Perfil,Modulo
        $hora_l = $array_campos[1];
        if ($hora_l != $hora_l_tem) {
          $resultado[] = array("fecha"=>$array_campos[0],"hora"=>$array_campos[1], "usuario" => $array_campos[4], "perfil" => $array_campos[6], "modulo" => $array_campos[8]);
          //echo "\n" . $array_campos[0] . ";" . $array_campos[1] . ";" . $array_campos[4] . ";" . $array_campos[6] . ";" . $array_campos[8];
        }
        $hora_l_tem = $array_campos[1];
      }
    }
  }

  $nombre_archivo = "REPORTE_LOG_USUARIOS.csv";
  $encabezados = "Fecha;Hora;Usuario;Perfil;Modulo";
  $columnas = array("fecha","hora","usuario","perfil","modulo");

  /*Se llama el metodo que comprime el archivo csv. Los argumentos son:
      1: nombre del archivo zip.
      2: nombre del archivo que se va a comprimir
      3: Encabezado del archivo que se va a comprimir
      4: Datos que va conterner el archivo que se va a comprimir (es un arreglo)
      5: Columnas de los datos
  */
  $BD->comprimir_archivo("REPORTE_LOG_USUARIOS",$nombre_archivo,$encabezados,$resultado,$columnas);

} else if ($plataforma == 2) { //DISTRIBUIDOR
  //echo "Fecha;Hora;Distribuidor;Usuario;Perfil;Modulo";
  for ($m = $num_f_ini; $m <= $num_f_fin; $m++) {
    $c_distri = "SELECT id FROM distribuidores order by id";
    $r_distri = $BD->devolver_array($c_distri);
    foreach ($r_distri AS $dist) {
      $ruta_log = $s_logFile = $GLOBALS["LOG_DIS"] . "dis_" . $dist["id"] . "_" . $m . ".log";
      if (file_exists($ruta_log)) {
        $hora_l_tem = "";

        $array_datos = explode("\n", file_get_contents($ruta_log));
        //CABECERAS:
        //Fecha,Hora,Id_Distri,Nombre_Distri,Id_Usuario,Cedula_Usuario,Nombre_Usuario,id_Perfil_app,Id_Perfil,Nombre_Perfil,Ip,Modulo,Datos
        for ($i = 0; $i < count($array_datos) - 1; $i++) {
          $array_campos = explode("|", $array_datos[$i]);

          $hora_l = $array_campos[1];
          if ($hora_l != $hora_l_tem) {
            if ($distri != "") {
              if (intval($array_campos[2]) == intval($distri)) {
                //Fecha,Hora,Distribuidor,Usuario,Perfil,Modulo
                $resultado[] = array("fecha"=>$array_campos[0],"hora"=>$array_campos[1],"distribuidor"=>$array_campos[3],"usuario"=>$array_campos[6],"perfil"=>$array_campos[9],"modulo"=>$array_campos[11]);

                //echo "\n" . $array_campos[0] . ";" . $array_campos[1] . ";" . $array_campos[3] . ";" . $array_campos[6] . ";" . $array_campos[9] . ";" . $array_campos[11];
              }
            } else {
              $resultado[] = array("fecha"=>$array_campos[0],"hora"=>$array_campos[1],"distribuidor"=>$array_campos[3],"usuario"=>$array_campos[6],"perfil"=>$array_campos[9],"modulo"=>$array_campos[11]);

              //echo "\n" . $array_campos[0] . ";" . $array_campos[1] . ";" . $array_campos[3] . ";" . $array_campos[6] . ";" . $array_campos[9] . ";" . $array_campos[11];
            }
          }

          $hora_l_tem = $array_campos[1];
        }
      }
    }
  }

  $nombre_archivo = "REPORTE_LOG_USUARIOS.csv";
  $encabezados = "Fecha;Hora;Distribuidor;Usuario;Perfil;Modulo";
  $columnas = array("fecha","hora","distribuidor","usuario","perfil","modulo");

  /*Se llama el metodo que comprime el archivo csv. Los argumentos son:
      1: nombre del archivo zip.
      2: nombre del archivo que se va a comprimir
      3: Encabezado del archivo que se va a comprimir
      4: Datos que va conterner el archivo que se va a comprimir (es un arreglo)
      5: Columnas de los datos
  */
  $BD->comprimir_archivo("REPORTE_LOG_USUARIOS",$nombre_archivo,$encabezados,$resultado,$columnas);

}