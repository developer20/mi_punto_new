<?php
require("../../config/mainModel.php");
class reporte_visitas{
/*
  METODOS DE LA BD.
  $this->BD->consultar($query); // Ejecuta la consulta y devuelve string.!
  $this->BD->devolver_array($query); // Ejecuta la consulta y devuelve array asociativo.!
  $this->BD->consultar("BEGIN"); // Antes de transacciones.!
  $this->BD->consultar("COMMIT"); // Commit para guardar datos.!
  $this->BD->consultar("ROLLBACK"); // Devolver datos si hay error.!
  $this->BD->numreg($query); // Devuelve el numero de registros de la consulta.!
*/
	public function __construct()
    {
      $BD=new BD();
      $this->BD = $BD;
      $this->BD->conectar();
     
    }
    public function __destruct()
    {
      $this->BD->desconectar();
    }
   //Espacio Para declara las funciones que retornan los datos de la DB.

  public function cargar_visitas_check($f_ini,$f_fin,$nivel_consulta){
    
    $oSession = unserialize($_SESSION[$GLOBALS["SESION_POS"]]);
    $distri =  $oSession->VSid_distri;
    $punto  =  $oSession->VSidpos;

    $sql_dis = "SELECT nombre_corto FROM {$GLOBALS["BD_NAME"]}.distribuidores WHERE id = $distri";
    $query_dis = $this->BD->devolver_array($sql_dis);
    $nombre_corto = $query_dis[0]["nombre_corto"];
    $bd = "{$GLOBALS["BD_DIS"]}" . $nombre_corto . "_" . $distri;

    $c_fecha_rango = "SELECT DATEDIFF('$f_fin', '$f_ini') as dias;";
    $r_fecha_rango = $this->BD->devolver_array($c_fecha_rango);
    if(intval($r_fecha_rango[0]["dias"]) > 31){
      return array("estado"=>-1,"mns"=> "El rango maximo entre las fechas es de 31 dias.");
     }

    $c_vis_act = "SELECT 
                        c.fecha,if(c.hora is null,'N/A',c.hora) as hora_ini,if(c.hora_cierre is null,'N/A',c.hora_cierre) as hora_fin,if(c.venta = 1,'Si','No')as vis_efect,
                        if(extraruta = 1,'Si','No')as extraruta,p.razon,c.punto,if(ud.id is null,'N/A',CONCAT(ud.nombre,' ',ud.apellido)) AS nombre_usuario ,
                        c.id as id,c.venta,concat(c.id_distri,c.usuario) as usu_vis,IF(c.visitaEnPunto = 1,'SI','NO') AS en_punto
                    FROM 
                        {$GLOBALS["BD_POS"]}.visitas__check c
                    INNER JOIN 
                         {$GLOBALS["BD_POS"]}.puntos p ON (p.idpos = c.punto)
                    LEFT JOIN 
                          {$GLOBALS["BD_POS"]}.visitas_detalle vd ON (vd.proceso = 0 AND vd.id_check = c.id)
						        LEFT JOIN 
                          {$GLOBALS["BD_POS"]}.motivo_visita mv ON (vd.tipo_visita = mv.id)
                    LEFT JOIN 
					                 $bd.usuarios ud ON (ud.id = c.usuario  AND ud.vendedor = 1)
                    WHERE 
                          c.fecha >= '$f_ini' AND c.fecha <= '$f_fin'  AND c.reparto=0 AND 
                          c.id_reg >0 AND c.id_distri = $distri AND c.punto = $punto
                    GROUP BY c.id";

                    //echo $c_vis_act;
                    mysql_set_charset("utf8");
       $array_consol = $this->BD->devolver_array($c_vis_act);
        return array("data" => $array_consol);
  }


  public function cargar_visitas_check_tarjetas($f_ini,$f_fin,$nivel_consulta){

    
    $oSession = unserialize($_SESSION[$GLOBALS["SESION_POS"]]);
    $distri =  $oSession->VSid_distri;
    $punto  =  $oSession->VSidpos;

    $c_vis_act = "SELECT 
                        if(c.venta = 1,'Si','No')as vis_efect,c.punto,
                         c.id as id,c.venta,concat(c.id_distri,c.usuario) as usu_vis
                    FROM 
                        {$GLOBALS["BD_POS"]}.visitas__check c
                    WHERE 
                          c.fecha >= '$f_ini' AND c.fecha <= '$f_fin'  AND c.reparto=0 AND 
                          c.id_reg >0 AND c.id_distri = $distri AND c.punto = $punto";

      $t_visitas = 0;
      $visitas_efec = 0;
      $pdvs_visit = 0;
      $pdvs_compra = 0;
      $vendedores = 0;
      $array_posvis = array();
      $array_posven = array();
      $array_vend = array();

    
      $array_consol = $this->BD->devolver_array($c_vis_act);
      
          foreach ($array_consol as $key => $consol) {
                $t_visitas++;
                if(intval($consol["venta"]==1)){
                  $visitas_efec++;
                  if($consol["punto"] != ""){
                    $array_posven[] = $consol["punto"];
                  }
                }

                if($consol["punto"] != ""){
                  $array_posvis[] = $consol["punto"];
                }

                $array_vend[] = $consol["usu_vis"];
          }
      
      $pdvs_visit = count(array_unique($array_posvis));
      $pdvs_compra = count(array_unique($array_posven));
      $vendedores = count(array_unique($array_vend));
       
      $array_total_c = array(
        "t_visitas"    => $t_visitas,
        "visitas_efec" => $visitas_efec,
        "pdvs_visit"   => $pdvs_visit,
        "pdvs_compra"  => $pdvs_compra,
        "vendedores"   => $vendedores,
      );
    
      
      return array('total' => $array_total_c);
  }

  function calcular_marcacion($lat_pos,$long_pos,$lat_vis,$long_vis,$radio){
       $degtorad = 0.01745329;
       $radtodeg = 57.29577951;
       $km = 0;

       $lat1 = floatval($lat_pos);
       $long1 = floatval($long_pos);
       //FORMULARA PARA CALCULAR EL RADIO ENTRE DOS PUNTOS CON RESPECTO AL ULTIMO
       $lat2 = floatval($lat_vis);
       $long2 = floatval($long_vis);

       $dlong = ($long1 - $long2);
       $dvalue = (sin($lat1 * $degtorad) * sin($lat2 * $degtorad)) + (cos($lat1 * $degtorad) * cos($lat2 * $degtorad) * cos($dlong * $degtorad));
       $dd = acos($dvalue) * $radtodeg;
       $km = ($dd * 111.302);
       $km = ($km * 100)/100;
       $mt = $km * 1000;

       if($mt <= $radio){
          return 'Si';
       }else{
          return 'No';
       }
  }

}// Fin clase
?>