var id_checks = new Array();
var dtable2;
var dtable3;

$(document).ajaxSend(function(event, request, settings) {
  loader = $('body').loadingIndicator({
        useImage: false,
      }).data("loadingIndicator");

  loader.show();

});

$(document).ready(function() {

  $(".content-header").find("h1").html("Mis Visitas");

  $("#CSV").click(function() {
    var f_ini_c = $("#f_ini").val();
    var f_fin_c = $("#f_fin").val();
    
     window.location = "modulos/reporte_visitas_v1.0.0/reporte_csv.php?f_ini="+f_ini_c+"&f_fin="+f_fin_c;
   
  

  });


  $('.fecha').datepicker({
    format: 'yyyy-mm-dd',
    todayHighlight: true,
    todayBtn: true
  });

  $("#btn_buscar").click(function(){
    $("#CSV").show();
    
      cargar_reporte_detallado();
   

  });

  /*VALIDACION GENERAL DE LOS FORMULARIOS*/
 
  


});




function grafico_circulo_min(id_div_contenedor,porcent,color){
   $('#'+id_div_contenedor).circleProgress({
                value: (porcent/100),
                size: 50,
                fill:{
                  color:color
                }
              }).on('circle-animation-progress', function(event, progress) {
                $(this).find('strong').html('<font color="'+color+'">'+porcent + '%</font>');

              });
}



var f_ini;
var f_fin;
var dtable;


function cargar_reporte_detallado(){

  f_ini = $("#f_ini").val();
  f_fin = $("#f_fin").val();
 
  if(f_ini == "" || f_fin == ""){
    Notificacion("La fecha inicial y la fecha final son obligatorios","warning");
    return false;
  }

  var fechaHoraInicial = new Date(f_ini).getTime();
  var fechaHoraFinal = new Date(f_fin).getTime();

    if (fechaHoraInicial > fechaHoraFinal) {
        Notificacion("La fecha inicial no debe ser mayor a la fecha final", "warning");
        return;
    }

  if ($.fn.DataTable.isDataTable('#t_reporte')) {
    dtable.clear();
    dtable.destroy();
  }

 
  var cabecera = "<thead><tr><th>Fecha-Hora</th><th>Vendedor</th><th>Visita Efectiva</th><th>Visita Punto</th><th>Extra Ruta</th><th>Detalle</th></tr></thead>";
  $("#t_reporte").html(cabecera);
  dtable = $('#t_reporte').DataTable({
    "bFilter": false,
    "ajax": {
      "url": "modulos/reporte_visitas_v1.0.0/controlador.php",
      "type": "POST",
      "data": {
        accion: 'cargar_reporte_detalle',
        f_ini: f_ini,
        f_fin: f_fin,
        nivel_consulta: 8
      }
    },
    "columns": [
      { "data": "fecha" },
      { "data": "nombre_usuario" },
      { "data": "vis_efect" },
      { "data": "en_punto" },
      { "data": "extraruta" },
      { "data": "id" }
    ],
    "columnDefs": [
      {
        "targets": 0,
        "data": "vis_realizadas",
        render: function (data, type, row) {
          return row.fecha + " - " + row.hora_ini + " - " + row.hora_fin;
        }
      }, {
        "targets": 5,
        "data": "id",
        render: function (data, type, row) {
          return "<i style='cursor:pointer; font-size:19px;' class='glyphicon glyphicon-zoom-in det_vis text-primary'></i>";
        }
      }],
    fnDrawCallback: function () {
      $(".det_vis").unbind("click");
      $(".det_vis").click(function () {
        var tr = $(this).parent().parent();
        var data = dtable.row($(this).parents("tr")).data();
        var id_check = data.id;
        geo_detalle_visita(id_check);
      });
    }
  });
  crear_detalle_visita_detalle();
  
  $("#consulta").show();
 
}



function crear_detalle_visita_detalle() {

  f_ini = $("#f_ini").val();
  f_fin = $("#f_fin").val();
 

  $.post('modulos/reporte_visitas_v1.0.0/controlador.php',
    {
      accion: 'cargar_reporte_detalle_tarjetas',
      f_ini: f_ini,
      f_fin: f_fin,
      nivel_consulta: 8
    },
    function (data, textStatus) {
      data = JSON.parse(data);

      
      totales = data.total;
      $("#div_totales").show();
      $("#total_vis").html(totales.t_visitas);
      $("#vis_efect").html(totales.visitas_efec);
      $("#pdvs_vis").html(totales.pdvs_visit);
      $("#pdvs_compra").html(totales.pdvs_compra);
      $("#cant_vende").html(totales.vendedores);
      grafico_circulo_min(
        "circle_vis_efec",
        format((totales.visitas_efec * 100) / totales.t_visitas, 0),
        "#333"
      );
      grafico_circulo_min(
        "circle_pdv_com",
        format((totales.pdvs_compra * 100) / totales.pdvs_visit, 0),
        "#fff"
      );
      $("#div_totales").show();
    }
  );
}





/////////////////////////////////////////////////////














