<?php

if (!isset($_SESSION)) {
  session_start();
}
require_once("../../config/Session.php");
require("../../config/mainModel.php");

$BD = new BD();
$BD->conectar();
$oSession = unserialize($_SESSION[$GLOBALS["SESION_POS"]]);
$distri =  $oSession->VSid_distri;
$punto  =  $oSession->VSidpos;


$variables = $_GET;
$f_ini = $variables['f_ini'];
$f_fin = $variables['f_fin'];
$condicion = "";


if ($distri != "" && $distri != "undefined") {
  $condicion .= " AND c.id_distri = {$distri} ";
}


$sql_dis = "SELECT nombre_corto FROM {$GLOBALS["BD_NAME"]}.distribuidores WHERE id = $distri";
$query_dis = $BD->devolver_array($sql_dis);
$nombre_corto = $query_dis[0]["nombre_corto"];
$bd = "{$GLOBALS["BD_DIS"]}" . $nombre_corto . "_" . $distri;

$c_visitas_check = "SELECT distinct c.id, CONCAT(IF(c.fecha IS NULL,'N/A',c.fecha),'-',IF(c.hora IS NULL,'N/A',c.hora),'-',
                          IF(c.hora_cierre IS NULL,'N/A',c.hora_cierre)) AS fecha_hora,c.punto,p.razon,d.nombre AS distri,
                          r.nombre AS reg,t.nombre AS ter,z.nombre AS zon,CONCAT(ud.nombre,' ',ud.apellido) AS ven,ud.cedula,
                          IF(c.venta = 1,'Si','No') AS vis_efectiva,c.lat,c.lng,c.lat_pdv,c.long_pdv,c.precisiongps,
                          IF(c.extraruta = 1,'Si','No') AS extra_rut, IF(c.evidencia = 1,'Si','No') AS evidencia,
                          IF(vd.id IS NULL,'N/A',vd.observa) AS obeservacion, IF(mv.id IS NULL,'N/A',mv.motivo) AS motivo_visita, 
                          IF(c.qr = 1,'SI',IF(c.qr=0 AND c.observacion_qr IS NOT NULL AND c.observacion_qr != '','NO','N/A')) AS qr, 
                          IF(c.qr = 1,'',IF(c.qr = 0 AND c.observacion_qr IS NOT NULL AND c.observacion_qr != '',
                          c.observacion_qr,'N/A')) AS observacion_qr, IF(c.visitaEnPunto = 1, 'Si','No') AS marcacion, 
                          IF(c.posicion_simulada = '1', 'SI', 'NO') AS posicion_simulada
						FROM {$GLOBALS["BD_POS"]}.visitas__check c
						INNER JOIN 
                  {$GLOBALS["BD_POS"]}.puntos p 
                    ON (p.idpos = c.punto)
						LEFT JOIN 
                  {$GLOBALS["BD_POS"]}.visitas_detalle vd 
                    ON (vd.proceso = 0 AND vd.id_check = c.id)
						LEFT JOIN 
                  {$GLOBALS["BD_POS"]}.motivo_visita mv 
                    ON (vd.tipo_visita = mv.id)
            LEFT JOIN 
                  $bd.usuarios ud
                    ON c.usuario = ud.id AND ud.vendedor = 1
            LEFT JOIN 
                   {$GLOBALS["BD_NAME"]}.regional r
                    ON c.id_reg = r.id
            LEFT JOIN 
                  {$GLOBALS["BD_NAME"]}.distribuidores d
                    ON c.id_distri = d.id
            LEFT JOIN 
                  {$GLOBALS["BD_NAME"]}.territorios t
                    ON c.territorio = t.id
            LEFT JOIN 
                  {$GLOBALS["BD_NAME"]}.zonas z
                   ON c.zona = z.id
						WHERE c.fecha >= '$f_ini' AND c.fecha <= '$f_fin' and c.reparto = 0 AND c.id_reg > 0 $condicion AND c.punto = $punto ;";

          
$resultado = $BD->devolver_array($c_visitas_check);


  $nombre_archivo = "Reporte_visitas.csv";
  $encabezados = "FECHA-HORA;IDPDV;NOMBRE PDV;DISTRIBUIDOR;REGIONAL;RUTA;CIRCUITO;VENDEDOR;CEDULA;MOTIVO;OBSERVACION;VISITA EFECTIVA;EXTRA RUTA;LATITUD;LONGITUD";

  $llaves = array(
    "fecha_hora",
    "punto",
    "razon",
    "distri",
    "reg",
    "ter",
    "zon",
    "ven",
    "cedula",
    "motivo_visita",
    "obeservacion",
    "vis_efectiva",
    "extra_rut",
    "lat",
    "lng"
  );

  /*Se llama el metodo que comprime el archivo csv. Los argumentos son:
  1: nombre del archivo zip.
  2: nombre del archivo que se va a comprimir
  3: Encabezado del archivo que se va a comprimir
  4: Datos que va conterner el archivo que se va a comprimir (es un arreglo)
  5: Llave de los datos
  */
  $BD->comprimir_archivo("Reporte_visitas_detallado", $nombre_archivo, $encabezados, $resultado, $llaves);
