<?php 
//error_reporting(E_ALL);
//ini_set('display_errors', '1');

function udate($format = 'u', $utimestamp = null) {
	if (is_null($utimestamp))
		$utimestamp = microtime(true);

	$timestamp = floor($utimestamp);
	$milliseconds = round(($utimestamp - $timestamp) * 1000000);

	return date(preg_replace('`(?<!\\\\)u`', $milliseconds, $format), $timestamp);
}

function Recarga($valor,$movil,$operador,$transactionId,$timeout,$ultimoid,$bolsaanterior,$key_pes,$servicio,$bolsaanterior_inc,$valor_recar,$valor_inc,$id_pro,$ean,$token,$cod_movilway,$id_distri)
{

	$idpos=0;
	//$id_distri=0;
	$folder = "";

	if(isset($_SESSION[$GLOBALS["SESION_POS"]])){

		$oSession = unserialize($_SESSION[$GLOBALS["SESION_POS"]]);

		$idpos=$oSession->VSidpos;
		//$id_distri=$oSession->VSid_distri;

	}else{
		die();
	}

	$BD=new BD();
	$BD->conectar();
	
	// INICIA SIMULA RECARGAS PRUEBAS

	$sql="UPDATE venta__servicios SET estado = 1, horatransaccion = CURTIME(), saldo_actual = ROUND(($bolsaanterior-$valor_recar),2),saldo_actual_incentivo = ROUND(($bolsaanterior_inc-$valor_inc),2) WHERE id=$ultimoid";

	$resultven_up=$BD->consultar($sql);

	if($BD->numupdate() == 0){
		$resultven_up2=$BD->consultar($sql);
		if($BD->numupdate() == 0){
			$resultven_up3=$BD->consultar($sql);
		}
	}

	return array("estado" => 1, "msg" => '');

	// FIN SIMULA RECARGAS PRUEBAS

	if(!isset($_SESSION["key_trans"])){
		$sql = "INSERT INTO venta__errores (id_venta, fecha, hora, id_punto, id_distri, operador, id_item_servicio, valor, movil, msg) VALUES ($ultimoid,CURDATE(),CURTIME(),$idpos,$id_distri,$operador,0,$valor,'$movil', 'Error al intentar realizar la recarga')";
		$res_error=$BD->consultar($sql);

		return array("estado" => 4, "msg" => 'Error al intentar realizar la recarga');
	}

	if(strcmp($key_pes,$_SESSION["key_trans"]) != 0){

		$sql = "INSERT INTO venta__errores (id_venta, fecha, hora, id_punto, id_distri, operador, id_item_servicio, valor, movil, msg) VALUES ($ultimoid,CURDATE(),CURTIME(),$idpos,$id_distri,$operador,0,$valor,'$movil', 'Error al intentar realizar la recarga')";
		$res_error=$BD->consultar($sql);

		return array("estado" => 4, "msg" => 'Error al intentar realizar la recarga');
	} 	

	$sql = "SELECT id FROM token_ventas WHERE id_pos = $idpos AND token = '$key_pes' AND id_venta = $ultimoid";
	$res_val_venta=$BD->consultar($sql);

	if($BD->numreg($res_val_venta) == 0){

		$sql = "INSERT INTO venta__errores (id_venta, fecha, hora, id_punto, id_distri, operador, id_item_servicio, valor, movil, msg) VALUES ($ultimoid,CURDATE(),CURTIME(),$idpos,$id_distri,$operador,0,$valor,'$movil', 'Error al intentar realizar la recarga, la venta no existe')";
		$res_error=$BD->consultar($sql);

		return array("estado" => 4, "msg" => 'Error al intentar realizar la recarga');
	} 

	$sql = "SELECT id_distri FROM puntos WHERE idpos=$idpos";
	$res_pos=$BD->consultar($sql);

	if($BD->numreg($res_pos) == 0){

		$sql = "UPDATE venta__servicios SET estado = 2, horatransaccion = CURTIME(), code_error = 9991, mens_error='Error al realizar la recarga' WHERE id=$ultimoid";
		$resultven_up=$BD->consultar($sql);

		if($BD->numupdate() == 0){
			$resultven_up2=$BD->consultar($sql);
			if($BD->numupdate() == 0){
				$resultven_up3=$BD->consultar($sql);
			}
		}

		return array("estado" => 4, "msg" => 'Error al realizar la recarga');
	}

	/*$idDistri = $res_pos->fields["id_distri"];

	if($id_distri != $idDistri){

		$sql = "UPDATE venta__servicios SET estado = 2, horatransaccion = CURTIME(), code_error = 9992, mens_error='Error al realizar la recarga' WHERE id=$ultimoid";
		$resultven_up=$BD->consultar($sql);

		if($BD->numupdate() == 0){
			$resultven_up2=$BD->consultar($sql);
			if($BD->numupdate() == 0){
				$resultven_up3=$BD->consultar($sql);
			}
		}

		return array("estado" => 4, "msg" => 'Error al realizar la recarga');
	}*/

	$sql = "SELECT folder FROM proveedores WHERE id = $id_pro AND (folder IS NULL OR folder != '')";
	$res_fol_pro=$BD->consultar($sql);
	
	if($BD->numreg($res_fol_pro)>0){
	
		$folder = $res_fol_pro->fields["folder"];
	
	}

	$creden = $BD->credenciales($id_distri,$id_pro,$cod_movilway,$folder);

	if($creden["user"] == "" || empty($creden["user"])){

		$sql = "UPDATE venta__servicios SET estado = 2, horatransaccion = CURTIME(), code_error = 9993, mens_error='Las credenciales del distribuidor no existen' WHERE id=$ultimoid";
		$resultven_up=$BD->consultar($sql);

		if($BD->numupdate() == 0){
			$resultven_up2=$BD->consultar($sql);
			if($BD->numupdate() == 0){
				$resultven_up3=$BD->consultar($sql);
			}
		}

		return array("estado" => 4, "msg" => 'Error al realizar la recarga');
	}
	
	// ********************************** Descomentar codigo para producción **********************************
	
	/*$service="processTopUpTransaction";

	//meta(simpre es fijo)
	$requestDate=udate("YmdHis.u");	
	$customerId="173052";
	$userName=$creden["user"];
	$passwordHash=$creden["pass"];
	$deviceCode="666666";
	$channel="1";
	$systemId="9000";
	$originAddress="190.248.152.140";
	$requestReference=udate("Hisu");//codigo unico
	$requestSource="01";

	//$transactionId = substr($transactionId, 1);

//data
	$productId=$operador;
	$EANCode=$ean;
	$amount=$valor;
	$destinationNumber=$movil;
	$customerDate=date("Ymd");
	$customerTxReference=str_pad($transactionId,12,"0",STR_PAD_LEFT);//codigo unico

	$data=array('productId'=>$productId,'EANCode'=>$EANCode,'amount'=>$amount,'destinationNumber'=>$destinationNumber,'customerDate'=>$customerDate,'customerTxReference'=>$customerTxReference);

	$param_meta= array('requestDate'=>$requestDate,'customerId'=>$customerId,'userName'=>$userName,'passwordHash'=>$passwordHash,'deviceCode'=>$deviceCode,'channel'=>$channel,'systemId'=>$systemId,'originAddress'=>$originAddress,'requestReference'=>$requestReference,'requestSource'=>$requestSource);

	$param_data= array('productId'=>$productId,'EANCode'=>$EANCode,'amount'=>$amount,'destinationNumber'=>$destinationNumber,'customerDate'=>$customerDate,'customerTxReference'=>$customerTxReference);
	
	//requestSignature
	$systemSignature=sha1(json_encode($param_meta).json_encode($param_data)."87f7a1051bda94b3e466d829e5570ed9");

	//$systemSignature="25d55ad283aa400af464c76d713c07ad";

	$param= array('meta' => array('requestDate'=>$requestDate,'customerId'=>$customerId,'userName'=>$userName,'passwordHash'=>$passwordHash,'deviceCode'=>$deviceCode,'channel'=>$channel,'systemId'=>$systemId,'originAddress'=>$originAddress,'requestReference'=>$requestReference,'requestSource'=>$requestSource), 'data' =>$data,'requestSignature' => array('systemSignature'=>$systemSignature));

	$service_url = 'http://192.168.156.114:28090/GetraxPublicServices/rest/sales/'.$service;*/

	try{

		// ********************************** Descomentar codigo para producción **********************************
	
		/*$curl = curl_init($service_url);
		$curl_post_data = $param;
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_TIMEOUT, $timeout);
		curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 120);
		curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($curl_post_data));
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json','Content-Length: ' . strlen(json_encode($curl_post_data)))); 
		$curl_response = curl_exec($curl);
		if($curl_response === false)
		{
			$mensaje=curl_error($curl);
			curl_close($curl);
			
			if(strpos($mensaje, "Operation timed") !== false)
			{
				sleep(2);
				return Consulta($transactionId,3,0,$ultimoid,$bolsaanterior,$operador,$movil,$valor,$id_distri,$servicio,$bolsaanterior_inc,$valor_recar,$valor_inc,$id_pro,$cod_movilway,$folder);
			}else{

				$sql = "UPDATE venta__servicios SET estado = 2, horatransaccion = CURTIME(), code_error = 9994, mens_error='$mensaje' WHERE id=$ultimoid";
				$resultven_up=$BD->consultar($sql);

				if($BD->numupdate() == 0){
					$resultven_up2=$BD->consultar($sql);
					if($BD->numupdate() == 0){
						$resultven_up3=$BD->consultar($sql);
					}
				}


				return array("estado" => 4, "msg" => "Error al realizar la recarga");

			}
		}
		else
		{
			$decoded = json_decode($curl_response);  
			curl_close($curl);
			
			if(empty($decoded))
			{
				$mensaje = "Sin respuesta";

				$sql = "UPDATE venta__servicios SET estado = 2, horatransaccion = CURTIME(), mens_error='$mensaje' WHERE id=$ultimoid";
				$resultven_up=$BD->consultar($sql);

				if($BD->numupdate() == 0){
					$resultven_up2=$BD->consultar($sql);
					if($BD->numupdate() == 0){
						$resultven_up3=$BD->consultar($sql);
					}
				}

				return array("estado" => 4, "msg" => "Error al realizar la recarga");
			}
			else
			{*/
				
				// ********************************** Descomentar codigo para producción **********************************
				
				/*$statusCode=$decoded->outcome->statusCode;
				$errorType=$decoded->outcome->error->errorType;
				$errorCode=$decoded->outcome->error->errorCode;
				$errorMessage=$decoded->outcome->error->errorMessage;
				$mensaje = $errorMessage." - ".$errorType;*/
				
				$statusCode=200;
				$errorType=0;
				$errorCode=0;
				$errorMessage="";
				$mensaje = $errorMessage." - ".$errorType;

				if($statusCode=="200" && is_int($statusCode))
				{
					if($errorType=="0" && $errorCode=="0")
					{
						
						// ********************************** Descomentar codigo para producción **********************************
						
						/*$transactionCode=$decoded->data->transactionCode;
						$customerBalance=$decoded->data->customerBalance;*/
						
						$transactionCode=0;
						$customerBalance=0;

						$sql="UPDATE venta__servicios SET estado = 1, horatransaccion = CURTIME(), transacionidresp='$transactionCode', saldo_actual = ROUND(($bolsaanterior-$valor_recar),2),saldo_actual_incentivo = ROUND(($bolsaanterior_inc-$valor_inc),2),realBalance = '$customerBalance' WHERE id=$ultimoid";	
			
						$resultven_up=$BD->consultar($sql);

						if($BD->numupdate() == 0){
							$resultven_up2=$BD->consultar($sql);
							if($BD->numupdate() == 0){
								$resultven_up3=$BD->consultar($sql);
							}
						}

						return array("estado" => 1, "msg" => '');
					}
					else
					{

						$sql = "UPDATE venta__servicios SET estado = 2, horatransaccion = CURTIME(), code_error = '$errorCode', mens_error='$mensaje' WHERE id=$ultimoid";
						$resultven_up=$BD->consultar($sql);

						if($BD->numupdate() == 0){
							$resultven_up2=$BD->consultar($sql);
							if($BD->numupdate() == 0){
								$resultven_up3=$BD->consultar($sql);
							}
						}


						$msg = "Error al realizar la recarga";
						$codigo = $errorCode;
						$sub_codigo = 0;

						//if($codigo == 69 && $operador == 6){

							$findme   = '[';
							$buscar = strpos($mensaje, $findme);
							
							if ($buscar !== false) {
								$code_msg = explode("[",$mensaje);
								if(count($code_msg) > 0){
									$sub_codigo = intval($code_msg[1]);
								}
							}

						//}

						$sql = "SELECT msg FROM venta__msg_errores WHERE codigo = '$codigo' AND sub_codigo = '$sub_codigo' AND id_operador = '$servicio' AND id_proveedor = $id_pro";
						$res_mensaje=$BD->consultar($sql);

						if($BD->numreg($res_mensaje)>0){
							$msg = $res_mensaje->fields["msg"];
						}

						return array("estado" => 4, "msg" => $msg); 

					}

				}
				else
				{

					$sql = "UPDATE venta__servicios SET estado = 2, horatransaccion = CURTIME(), code_error = '$errorCode', mens_error='$mensaje' WHERE id=$ultimoid";
					$resultven_up=$BD->consultar($sql);

					if($BD->numupdate() == 0){
						$resultven_up2=$BD->consultar($sql);
						if($BD->numupdate() == 0){
							$resultven_up3=$BD->consultar($sql);
						}
					}

					$msg = "Error al realizar la recarga";
					$codigo = $errorCode;
					$sub_codigo = 0;

					//if($codigo == 69 && $operador == 6){

						$findme   = '[';
						$buscar = strpos($mensaje, $findme);
						
						if ($buscar !== false) {
							$code_msg = explode("[",$mensaje);
							if(count($code_msg) > 0){
								$sub_codigo = intval($code_msg[1]);
							}
						}

					//}

					$sql = "SELECT msg FROM venta__msg_errores WHERE codigo = '$codigo' AND sub_codigo = '$sub_codigo' AND id_operador = '$servicio' AND id_proveedor = $id_pro";
					$res_mensaje=$BD->consultar($sql);

					if($BD->numreg($res_mensaje)>0){
						$msg = $res_mensaje->fields["msg"];
					}

					return array("estado" => 4, "msg" => $msg); 

				}
				
			//}
		//}


	}catch(SoapFault $e)
	{

		$mensaje = $e->faultstring;
		$codeerror=99;
		
		$sql = "UPDATE venta__servicios SET estado = 2, horatransaccion = CURTIME(), code_error = $codeerror, mens_error='$mensaje' WHERE id=$ultimoid";
		$resultven_up=$BD->consultar($sql);

		if($BD->numupdate() == 0){
			$resultven_up2=$BD->consultar($sql);
			if($BD->numupdate() == 0){
				$resultven_up3=$BD->consultar($sql);
			}
		}

		return array("estado" => 4, "msg" => "Error al realizar la recarga");

	}	 	 

}
///fin recarga

function Consulta($transactionId,$t,$i,$ultimoid,$bolsaanterior,$operador,$movil,$valor,$id_distri,$servicio,$bolsaanterior_inc,$valor_recar,$valor_inc,$id_pro,$cod_movilway,$folder)//t=cantidad de validaciones i=intentos
{

	$timeout=30;
	$service="getTransactionState";

	$BD=new BD();
	$BD->conectar();

	$sql = "SELECT folder FROM proveedores WHERE id = $id_pro AND (folder IS NULL OR folder != '')";
	$res_fol_pro=$BD->consultar($sql);
	
	if($BD->numreg($res_fol_pro)>0){
	
		$folder = $res_fol_pro->fields["folder"];
	
	}

	$creden = $BD->credenciales($id_distri,$id_pro,$cod_movilway,$folder);

	//meta(simpre es fijo)
	$requestDate=udate("YmdHis.u");	
	$customerId="173052";
	$userName=$creden["user"];
	$passwordHash=$creden["pass"];
	$deviceCode="666666";
	$channel="1";
	$systemId="9000";
	$originAddress="190.248.152.140";
	$requestReference=udate("Hisu");//codigo unico
	$requestSource="01";
 
    //data
	$thirdPartyReference=str_pad($transactionId,12,"0",STR_PAD_LEFT);//codigo unico
	$transactionType="0";

	$data=array('thirdPartyReference'=>$thirdPartyReference,'transactionType'=>$transactionType);

    //requestSignature
	$systemSignature="25d55ad283aa400af464c76d713c07ad";

	$param= array('meta' => array('requestDate'=>$requestDate,'customerId'=>$customerId,'userName'=>$userName,'passwordHash'=>$passwordHash,'deviceCode'=>$deviceCode,'channel'=>$channel,'systemId'=>$systemId,'originAddress'=>$originAddress,'requestReference'=>$requestReference,'requestSource'=>$requestSource), 'data' =>$data,'requestSignature' => array('systemSignature'=>$systemSignature));

	$service_url = 'http://192.168.156.114:28090/GetraxPublicServices/rest/sales/'.$service;

	try{	 
		$curl = curl_init($service_url);
		$curl_post_data = $param;
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_TIMEOUT, $timeout);
		curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 120);
		curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($curl_post_data));
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json','Content-Length: ' . strlen(json_encode($curl_post_data)))); 
		$curl_response = curl_exec($curl);
		if($curl_response === false)
		{
			$mensaje=curl_error($curl);
			curl_close($curl);
			if(strpos($mensaje, "Operation timed") !== false)
			{
				$i++;
				if($i<$t)
				{

					$sql = "SELECT id_punto,id_distri,operador,id_item_servicio,valor,movil FROM venta__servicios WHERE id=$ultimoid LIMIT 1";
					$res_v=$BD->consultar($sql);

					$id_pos = $res_v->fields["id_punto"];
					$id_distri = $res_v->fields["id_distri"];
					$operador = $res_v->fields["operador"];
					$id_item_servicio = $res_v->fields["id_item_servicio"];
					$valor = $res_v->fields["valor"];
					$movil = $res_v->fields["movil"];

					$sql = "INSERT INTO venta__errores (id_venta, fecha, hora, id_punto, id_distri, operador, id_item_servicio, valor, movil) VALUES ($ultimoid,CURDATE(),CURTIME(),$id_pos,$id_distri,$operador,$id_item_servicio,$valor,'$movil')";
					$res_error=$BD->consultar($sql);

					sleep(5);
					Consulta($transactionId,$t,$i,$ultimoid,$bolsaanterior,$operador,$movil,$valor,$id_distri,$servicio,$bolsaanterior_inc,$valor_recar,$valor_inc,$id_pro);

				}else{

					$sql = "UPDATE venta__servicios SET estado = 2, horatransaccion = CURTIME(), mens_error='$mensaje' WHERE id=$ultimoid";
					$resultven_up=$BD->consultar($sql);

					if($BD->numupdate() == 0){
						$resultven_up2=$BD->consultar($sql);
						if($BD->numupdate() == 0){
							$resultven_up3=$BD->consultar($sql);
						}
					}

					return array("estado" => 4, "msg" => "Error al realizar la recarga");	  

				}

			}else{

				$sql = "UPDATE venta__servicios SET estado = 2, horatransaccion = CURTIME(), mens_error='$mensaje' WHERE id=$ultimoid";
				$resultven_up=$BD->consultar($sql);

				if($BD->numupdate() == 0){
					$resultven_up2=$BD->consultar($sql);
					if($BD->numupdate() == 0){
						$resultven_up3=$BD->consultar($sql);
					}
				}

				return array("estado" => 4, "msg" => "Error al realizar la recarga");	

			}  

		}
		else
		{
			$decoded = json_decode($curl_response);
			curl_close($curl);
			if(empty($decoded))
			{
				$mensaje = "Sin respuesta";

				$sql = "UPDATE venta__servicios SET estado = 2, horatransaccion = CURTIME(), mens_error='$mensaje' WHERE id=$ultimoid";
				$resultven_up=$BD->consultar($sql);

				if($BD->numupdate() == 0){
					$resultven_up2=$BD->consultar($sql);
					if($BD->numupdate() == 0){
						$resultven_up3=$BD->consultar($sql);
					}
				}

				return array("estado" => 4, "msg" => "Error al realizar la recarga");
			}
			else
			{
				$statusCode=$decoded->outcome->statusCode;
				$errorType=$decoded->outcome->error->errorType;
				$errorCode=$decoded->outcome->error->errorCode;
				$errorMessage=$decoded->outcome->error->errorMessage;
				$mensaje = $errorMessage." - ".$errorType;

				if($statusCode=="200")
				{
					if($errorType=="0" and $errorCode=="0")
					{
						$cantidad=count($decoded->data->getTransactionStateRecharges);
						$transactionId=$decoded->data->getTransactionStateRecharges[$cantidad-1]->transactionId;
						$transactionState=$decoded->data->getTransactionStateRecharges[$cantidad-1]->transactionState;
						$transactionDate=$decoded->data->getTransactionStateRecharges[$cantidad-1]->transactionDate;
						if($transactionDate==date("Ymd"))
						{
							if($transactionState=="1")
							{
								$transactionCode=$decoded->data->transactionCode;
								$customerBalance=$decoded->data->customerBalance;

								$sql="UPDATE venta__servicios SET estado = 1, horatransaccion = CURTIME(), transacionidresp='$transactionCode', saldo_actual = ROUND(($bolsaanterior-$valor_recar),2),saldo_actual_incentivo = ROUND(($bolsaanterior_inc-$valor_inc),2),realBalance = '$customerBalance' WHERE id=$ultimoid";	
			
								$resultven_up=$BD->consultar($sql);

								if($BD->numupdate() == 0){
									$resultven_up2=$BD->consultar($sql);
									if($BD->numupdate() == 0){
										$resultven_up3=$BD->consultar($sql);
									}
								}

								return array("estado" => 1, "msg" => '');
							}
							else
							{
								if($transactionState=="3")
								{
									
									$sql = "UPDATE venta__servicios SET estado = 2, horatransaccion = CURTIME(), code_error = '$errorCode', mens_error='$mensaje' WHERE id=$ultimoid";
									$resultven_up=$BD->consultar($sql);

									if($BD->numupdate() == 0){
										$resultven_up2=$BD->consultar($sql);
										if($BD->numupdate() == 0){
											$resultven_up3=$BD->consultar($sql);
										}
									}


									$msg = "Error al realizar la recarga";
									$codigo = $errorCode;
									$sub_codigo = 0;

									//if($codigo == 69 && $operador == 6){

										$findme   = '[';
										$buscar = strpos($mensaje, $findme);
										
										if ($buscar !== false) {
											$code_msg = explode("[",$mensaje);
											if(count($code_msg) > 0){
												$sub_codigo = intval($code_msg[1]);
											}
										}

									//}

									$sql = "SELECT msg FROM venta__msg_errores WHERE codigo = '$codigo' AND sub_codigo = '$sub_codigo' AND id_operador = '$servicio' AND id_proveedor = $id_pro";
									$res_mensaje=$BD->consultar($sql);

									if($BD->numreg($res_mensaje)>0){
										$msg = $res_mensaje->fields["msg"];
									}

									return array("estado" => 4, "msg" => $msg); 
								}
								else
								{
									$i++;
									if($i<$t)
									{

										$sql = "SELECT id_punto,id_distri,operador,id_item_servicio,valor,movil FROM venta__servicios WHERE id=$ultimoid LIMIT 1";
										$res_v=$BD->consultar($sql);

										$id_pos = $res_v->fields["id_punto"];
										$id_distri = $res_v->fields["id_distri"];
										$operador = $res_v->fields["operador"];
										$id_item_servicio = $res_v->fields["id_item_servicio"];
										$valor = $res_v->fields["valor"];
										$movil = $res_v->fields["movil"];

										$sql = "INSERT INTO venta__errores (id_venta, fecha, hora, id_punto, id_distri, operador, id_item_servicio, valor, movil) VALUES ($ultimoid,CURDATE(),CURTIME(),$id_pos,$id_distri,$operador,$id_item_servicio,$valor,'$movil')";
										$res_error=$BD->consultar($sql);

										sleep(5);
										Consulta($transactionId,$t,$i,$ultimoid,$bolsaanterior,$operador,$movil,$valor,$id_distri,$servicio,$bolsaanterior_inc,$valor_recar,$valor_inc,$id_pro);

									}else{

										$sql = "UPDATE venta__servicios SET estado = 2, horatransaccion = CURTIME(), code_error = '$errorCode', mens_error='$mensaje' WHERE id=$ultimoid";
										$resultven_up=$BD->consultar($sql);

										if($BD->numupdate() == 0){
											$resultven_up2=$BD->consultar($sql);
											if($BD->numupdate() == 0){
												$resultven_up3=$BD->consultar($sql);
											}
										}


										$msg = "Error al realizar la recarga";
										$codigo = $errorCode;
										$sub_codigo = 0;

										//if($codigo == 69 && $operador == 6){

											$findme   = '[';
											$buscar = strpos($mensaje, $findme);
											
											if ($buscar !== false) {
												$code_msg = explode("[",$mensaje);
												if(count($code_msg) > 0){
													$sub_codigo = intval($code_msg[1]);
												}
											}

										//}

										$sql = "SELECT msg FROM venta__msg_errores WHERE codigo = '$codigo' AND sub_codigo = '$sub_codigo' AND id_operador = '$servicio' AND id_proveedor = $id_pro";
										$res_mensaje=$BD->consultar($sql);

										if($BD->numreg($res_mensaje)>0){
											$msg = $res_mensaje->fields["msg"];
										}

										return array("estado" => 4, "msg" => $msg);	  

									}
								}
							}
						}
						else
						{

							$sql = "UPDATE venta__servicios SET estado = 2, horatransaccion = CURTIME(), code_error = '99', mens_error='Transaccion no existe' WHERE id=$ultimoid";
							$resultven_up=$BD->consultar($sql);

							if($BD->numupdate() == 0){
								$resultven_up2=$BD->consultar($sql);
								if($BD->numupdate() == 0){
									$resultven_up3=$BD->consultar($sql);
								}
							}
							
							return array("estado" => 4, "msg" => "Transaccion no existe");		
						}

						$sql = "UPDATE venta__servicios SET estado = 2, horatransaccion = CURTIME(), code_error = '$errorCode', mens_error='$mensaje' WHERE id=$ultimoid";
						$resultven_up=$BD->consultar($sql);

						if($BD->numupdate() == 0){
							$resultven_up2=$BD->consultar($sql);
							if($BD->numupdate() == 0){
								$resultven_up3=$BD->consultar($sql);
							}
						}


						$msg = "Error al realizar la recarga";
						$codigo = $errorCode;
						$sub_codigo = 0;

						//if($codigo == 69 && $operador == 6){

							$findme   = '[';
							$buscar = strpos($mensaje, $findme);
							
							if ($buscar !== false) {
								$code_msg = explode("[",$mensaje);
								if(count($code_msg) > 0){
									$sub_codigo = intval($code_msg[1]);
								}
							}

						//}

						$sql = "SELECT msg FROM venta__msg_errores WHERE codigo = '$codigo' AND sub_codigo = '$sub_codigo' AND id_operador = '$servicio' AND id_proveedor = $id_pro";
						$res_mensaje=$BD->consultar($sql);

						if($BD->numreg($res_mensaje)>0){
							$msg = $res_mensaje->fields["msg"];
						}

						return array("estado" => 4, "msg" => $msg);	 
					}
					else
					{
						$sql = "UPDATE venta__servicios SET estado = 2, horatransaccion = CURTIME(), code_error = '$errorCode', mens_error='$mensaje' WHERE id=$ultimoid";
						$resultven_up=$BD->consultar($sql);

						if($BD->numupdate() == 0){
							$resultven_up2=$BD->consultar($sql);
							if($BD->numupdate() == 0){
								$resultven_up3=$BD->consultar($sql);
							}
						}


						$msg = "Error al realizar la recarga";
						$codigo = $errorCode;
						$sub_codigo = 0;

						//if($codigo == 69 && $operador == 6){

							$findme   = '[';
							$buscar = strpos($mensaje, $findme);
							
							if ($buscar !== false) {
								$code_msg = explode("[",$mensaje);
								if(count($code_msg) > 0){
									$sub_codigo = intval($code_msg[1]);
								}
							}

						//}

						$sql = "SELECT msg FROM venta__msg_errores WHERE codigo = '$codigo' AND sub_codigo = '$sub_codigo' AND id_operador = '$servicio' AND id_proveedor = $id_pro";
						$res_mensaje=$BD->consultar($sql);

						if($BD->numreg($res_mensaje)>0){
							$msg = $res_mensaje->fields["msg"];
						}

						return array("estado" => 4, "msg" => $msg);	
					}

				}
				else
				{
					$sql = "UPDATE venta__servicios SET estado = 2, horatransaccion = CURTIME(), code_error = '$errorCode', mens_error='$mensaje' WHERE id=$ultimoid";
					$resultven_up=$BD->consultar($sql);

					if($BD->numupdate() == 0){
						$resultven_up2=$BD->consultar($sql);
						if($BD->numupdate() == 0){
							$resultven_up3=$BD->consultar($sql);
						}
					}

					$msg = "Error al realizar la recarga";
					$codigo = $errorCode;
					$sub_codigo = 0;

					//if($codigo == 69 && $operador == 6){

						$findme   = '[';
						$buscar = strpos($mensaje, $findme);
						
						if ($buscar !== false) {
							$code_msg = explode("[",$mensaje);
							if(count($code_msg) > 0){
								$sub_codigo = intval($code_msg[1]);
							}
						}

					//}

					$sql = "SELECT msg FROM venta__msg_errores WHERE codigo = '$codigo' AND sub_codigo = '$sub_codigo' AND id_operador = '$servicio' AND id_proveedor = $id_pro";
					$res_mensaje=$BD->consultar($sql);

					if($BD->numreg($res_mensaje)>0){
						$msg = $res_mensaje->fields["msg"];
					}

					return array("estado" => 4, "msg" => $msg);	
				}
					
			}
		}




	}catch(SoapFault $e)
	{

		$mensaje = $e->faultstring;
		$codeerror=99;
		
		$sql = "UPDATE venta__servicios SET estado = 2, horatransaccion = CURTIME(), code_error = $codeerror, mens_error='$mensaje' WHERE id=$ultimoid";
		$resultven_up=$BD->consultar($sql);

		if($BD->numupdate() == 0){
			$resultven_up2=$BD->consultar($sql);
			if($BD->numupdate() == 0){
				$resultven_up3=$BD->consultar($sql);
			}
		}

		return array("estado" => 4, "msg" => "Error al realizar la recarga");

	}


}

?>
