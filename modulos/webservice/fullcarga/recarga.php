<?php

function cifrar($key,$input)
{
	$td = mcrypt_module_open('tripledes', '', 'ecb', '');
	$iv = mcrypt_create_iv(mcrypt_enc_get_iv_size($td), MCRYPT_RAND);
	mcrypt_generic_init($td, $key, $iv);
	$encrypted_data = mcrypt_generic($td, $input);
	mcrypt_generic_deinit($td);
	mcrypt_module_close($td);
	return $encrypted_data;
}

function descifrar($key,$input)
{

	$key=foo($key);
	$input=foo($input);
	$td = mcrypt_module_open('tripledes', '', 'ecb', '');
	$iv = mcrypt_create_iv (mcrypt_enc_get_iv_size($td), MCRYPT_RAND);
	$key = substr($key, 0, mcrypt_enc_get_key_size($td));
	mcrypt_generic_init($td, $key, $iv);
	$decrypted_data = mdecrypt_generic($td, $input);
	mcrypt_generic_deinit($td);
	mcrypt_module_close($td);
    return strtoupper(bin2hex($decrypted_data));
}

function foo($hex) {
	$rv = '';
	foreach(str_split($hex, 2) as $b) {
		$rv .= chr(hexdec($b));
	}
	return $rv;
}

function hextobin($hexstr)
{
	$n = strlen($hexstr);
	$sbin="";
	$i=0;
	while($i<$n)
	{
		$a =substr($hexstr,$i,2);
		$c = pack("H*",$a);
		if ($i==0){$sbin=$c;}
		else {$sbin.=$c;}
		$i+=2;
	}
	return $sbin;
}

function strtohex($string)
{
	$string = str_split($string);
	foreach($string as &$char)
		$char = dechex(ord($char));
	return implode('',$string);
}

function strposa($haystack, $needle, $offset=0) //busca en un string un array de palabras
{
	if(!is_array($needle)) $needle = array($needle);
	foreach($needle as $query) {
        if(strpos($haystack, $query, $offset) !== false) return true; // stop on first true result
    }
    return false;
}


function iso_60($procod, $refh2h, $usuario, $telefono, $importe, $key) //busca en un string un array de palabras
{
	$importe=$importe."00";
	$iso_60=strtohex($procod).strtohex($usuario).str_pad(strlen($refh2h),4,"0",STR_PAD_LEFT).strtohex($refh2h)."02".str_pad(strlen($telefono),4,"0",STR_PAD_LEFT).strtohex($telefono).str_pad(strlen($importe),4,"0",STR_PAD_LEFT).strtohex($importe);
	$iso_60=strtoupper($iso_60);
//se rellena para que sea multiplo de 8
	$multiplo_8=16-(strlen($iso_60)%16);
	for($i=0;$i<$multiplo_8;$i++)
		$iso_60=$iso_60."0";

//$key = "100285E637D6F1263334776534336637100285E637D6F126"; //llave cifrado
$key = foo($key);

$input = $iso_60;
$input = foo($input);

$result=cifrar($key,$input);
$iso_60_cifrado=strtoupper(bin2hex($result));

return $iso_60_cifrado;
}

function iso_60_consulta($refh2h, $usuario, $key) //busca en un string un array de palabras
{
	$procod="CONSUH2H";
	$iso_60=strtohex($procod).strtohex($usuario)."01".str_pad(strlen($refh2h),4,"0",STR_PAD_LEFT).strtohex($refh2h);
	$iso_60=strtoupper($iso_60);
//se rellena para que sea multiplo de 8
	$multiplo_8=16-(strlen($iso_60)%16);
	for($i=0;$i<$multiplo_8;$i++)
		$iso_60=$iso_60."0";

//$key = "100285E637D6F1263334776534336637100285E637D6F126"; //llave cifrado
$key = foo($key);

$input = $iso_60;
$input = foo($input);

$result=cifrar($key,$input);
$iso_60_cifrado=strtoupper(bin2hex($result));

return $iso_60_cifrado;
}


function Recarga($valor,$movil,$operador,$transactionId,$timeout,$ultimoid,$bolsaanterior,$key_pes,$servicio,$bolsaanterior_inc,$valor_recar,$valor_inc,$id_pro,$ean,$token,$cod_movilway,$id_distri)
{

	$idpos=0;
	//$id_distri=0;
	$folder = "";

	if(isset($_SESSION[$GLOBALS["SESION_POS"]])){

		$oSession = unserialize($_SESSION[$GLOBALS["SESION_POS"]]);

		$idpos=$oSession->VSidpos;
		//$id_distri=$oSession->VSid_distri;

	}else{
		die();
	}

	$BD=new BD();
	$BD->conectar();

	// INICIA SIMULA RECARGAS PRUEBAS

	/*$sql="UPDATE venta__servicios SET estado = 1, horatransaccion = CURTIME(), saldo_actual = ROUND(($bolsaanterior-$valor_recar),2),saldo_actual_incentivo = ROUND(($bolsaanterior_inc-$valor_inc),2) WHERE id=$ultimoid";

	$resultven_up=$BD->consultar($sql);

	if($BD->numupdate() == 0){
		$resultven_up2=$BD->consultar($sql);
		if($BD->numupdate() == 0){
			$resultven_up3=$BD->consultar($sql);
		}
	}

	return array("estado" => 1, "msg" => '');*/

	// FIN SIMULA RECARGAS PRUEBAS

	if(strcmp($key_pes,$_SESSION["key_trans"]) != 0){

		return retornar_error(1,'Error al intentar realizar la recarga',0,$ultimoid,$idpos,$id_distri,$operador,$valor,$movil,$id_pro,$servicio);
	}

	$sql = "SELECT id FROM token_ventas WHERE id_pos = $idpos AND token = '$key_pes' AND id_venta = $ultimoid";
	$res_val_venta=$BD->consultar($sql);

	if($BD->numreg($res_val_venta) == 0){

		return retornar_error(1,'Error al intentar realizar la recarga, la venta no existe',0,$ultimoid,$idpos,$id_distri,$operador,$valor,$movil,$id_pro,$servicio);

	}

	$sql = "SELECT id_distri FROM puntos WHERE idpos=$idpos";
	$res_pos=$BD->consultar($sql);

	if($BD->numreg($res_pos) == 0){

		return retornar_error(2,'Error al realizar la recarga',9991,$ultimoid,$idpos,$id_distri,$operador,$valor,$movil,$id_pro,$servicio);

	}

	/*$idDistri = $res_pos->fields["id_distri"];

	if($id_distri != $idDistri){

		return retornar_error(2,'Error al realizar la recarga, ocurrio un error con el distribuidor',9992,$ultimoid,$idpos,$id_distri,$operador,$valor,$movil,$id_pro,$servicio);

	}*/

	$sql = "SELECT folder FROM proveedores WHERE id = $id_pro AND (folder IS NULL OR folder != '')";
	$res_fol_pro=$BD->consultar($sql);

	if($BD->numreg($res_fol_pro)>0){

		$folder = $res_fol_pro->fields["folder"];

	}

	$creden = $BD->credenciales($id_distri,$id_pro,$cod_movilway,$folder);

	if($creden["user"] == "" || empty($creden["user"])){

		return retornar_error(2,'Las credenciales del distribuidor no existen',9993,$ultimoid,$idpos,$id_distri,$operador,$valor,$movil,$id_pro,$servicio);
	}

	$procod=$ean; //longitud 8 string -> hex
	$refh2h=$transactionId; //tipo de dato LLV tamano variable long string -> hex
	$usuario=$creden["user"];//longitud 10  string -> hex
	$input=$creden["pass"]; //16  -> hex
	$terminal=$creden["t_id"];
	$key=$creden["com"];
	$key_hex = foo($key);
	$telefono=$movil;
	$importe=$valor; //campo + .00
	$codeerror = 0;
	$res_error = array();

	//$key = "100285E637D6F1263334776534336637100285E637D6F126"; //llave cifrado

	$password = strtoupper(bin2hex(cifrar($key_hex,$input)));

	try{
	    $p_iso03=6;//fix
	    $p_iso04=12;//fix
	    $p_iso11=6;//fix
		$p_iso12=6;//fix
		$p_iso13=4;//fix
		$p_iso37=24;//fix alpha
		$p_iso39=4;//fix alpha
		$p_iso41=16;//fix alpha
		$p_iso44=999;//fix alpha
		$p_iso48=999;//llvar
		$p_iso60=999;//llvar
		$p_iso61=999;//llvar
		$p_iso63=999;//llvar

		$port = 10301;//puerto pruebas
		$host = "190.145.60.203";//host pruebas

		$fp = fsockopen($host, $port);
	    if (empty($fp)) {

	    	return retornar_error(2,"host no responde",9994,$ultimoid,$idpos,$id_distri,$operador,$valor,$movil,$id_pro,$servicio);

	        //return "3 - host no responde";
	    }

		/*$socket = socket_create(AF_INET, SOCK_STREAM, 0);

		if ($socket === false)
		{

			return retornar_error(2,'host no responde',9994,$ultimoid,$idpos,$id_distri,$operador,$valor,$movil,$id_pro,$servicio);

		}

		$result = socket_connect($socket, $host, $port);

		if ($result === false)
		{

			return retornar_error(2,'error conexion',9995,$ultimoid,$idpos,$id_distri,$operador,$valor,$movil,$id_pro,$servicio);

		}*/

	    //$terminal="00000427";//ASCII
	    $send_tpdu="0000000000"; //10
	    $send_type="0200"; //4
	    $send_bitmap="2038000000800014"; //16
	    $send_iso03="000100"; //6
	    $send_iso11=date("His"); //6 identificador mando la hora
	    $send_iso12=date("His"); //6
	    $send_iso13=date("md"); //4
	    $send_iso41=strtoupper(strtohex($terminal)); //4

	    $send_iso60=iso_60($procod, $refh2h, $usuario, $telefono, $importe, $key);//4
	    $send_iso60="00".(strlen($send_iso60)/2).$send_iso60;
	    $version="02";
	    $send_iso62=strtoupper(strtohex($version)).$password;
	    $send_iso62="00".(strlen($send_iso62)/2).$send_iso62;

	    $message=$send_tpdu.$send_type.$send_bitmap.$send_iso03.$send_iso11.$send_iso12.$send_iso13.$send_iso41.$send_iso60.$send_iso62;
	    $send_leng="00".strtoupper(dechex(strlen($message)/2)); //4
	    $message=$send_leng.$message;
	    $message=hextobin($message);

	    $fw=fwrite($fp, $message);
	    stream_set_timeout($fp, $timeout);
	    $result = fread($fp, 2000);

	    $info = stream_get_meta_data($fp);
	    fclose($fp);
		// close socket
		if ($info['timed_out']) {

			sleep(2);
			return Consulta($transactionId,3,0,$ultimoid,$bolsaanterior,$operador,$movil,$valor,$id_distri,$servicio,$bolsaanterior_inc,$valor_recar,$valor_inc,$id_pro,$timeout,$cod_movilway,$folder);

	        //return "3 - Time out";
	    }

	    // ORIGINAL
	    /*socket_write($socket, $message, strlen($message)) or die("Could not send data to server\n");
	    // get server response
	    $result = socket_read ($socket, 1024) or die("Could not read server response\n");
		// close socket
	    socket_close($socket);*/

	    /*socket_write($socket, $message, strlen($message)) or $res_error = retornar_error(2,"Could not send data to server",9996,$ultimoid,$idpos,$id_distri,$operador,$valor,$movil,$id_pro,$servicio);
	    // get server response

	    if(count($res_error)==0){

		    $result = socket_read ($socket, 1024) or $res_error = retornar_error(2,"Could not read server response",9997,$ultimoid,$idpos,$id_distri,$operador,$valor,$movil,$id_pro,$servicio);
			// close socket
			if(count($res_error)==0){
		    	socket_close($socket);
			}else{
		    	return $res_error;
		    }

	    }else{
	    	return $res_error;
	    }*/

	    $respuesta=strtoupper(bin2hex($result));

	    $resp_leng=substr($respuesta, 0, 4);
	    $tpdu=hexdec(substr($respuesta, 4, 10));
	    $type=substr($respuesta, 14, 4);
	    $bitmap=substr($respuesta, 18, 16);

	    if($bitmap==="203800000A81001A") //si no sale la recarga
        {
			$ini=34;
			$iso03=substr($respuesta, $ini, $p_iso03);
			$iso11=substr($respuesta, $ini+=$p_iso03, $p_iso11);
			$iso12=substr($respuesta, $ini+=$p_iso11, $p_iso12);
			$iso13=substr($respuesta, $ini+=$p_iso12, $p_iso13);
			$iso37=hextobin(substr($respuesta, $ini+=$p_iso13, $p_iso37)); //referencia
			$iso39=hextobin(substr($respuesta, $ini+=$p_iso37, $p_iso39));
			$iso41=hextobin(substr($respuesta, $ini+=$p_iso39, $p_iso41));


			$p_iso48=substr($respuesta, $ini+=$p_iso41, 4);
			$p_iso48=$p_iso48*2;
			if($p_iso48==0)
				$p_iso48=6;
			$iso48=hextobin(substr($respuesta, $ini+=4, $p_iso48));
			$p_iso60=substr($respuesta, $ini+=$p_iso48, 4);
			$p_iso60=$p_iso60*2;
			$iso60=hextobin(substr($respuesta, $ini+=4, $p_iso60));
			$p_iso61=hexdec(substr($respuesta, $ini+=$p_iso60, 4));
			$p_iso61=$p_iso61*2;
			$iso61=hextobin(substr($respuesta, $ini+=4, $p_iso61));

            $p_iso63=hexdec(substr($respuesta, $ini+=$p_iso61, 4));
			$p_iso63=$p_iso63*2;
			$iso63=hextobin(substr($respuesta, $ini+=4, $p_iso63));

			$split_msg = explode(' ',$iso60);

			if(count($split_msg)>0){
				$codeerror = $split_msg[0];
			}

			//return "recarga error: referencia:".$iso37." mensaje error:".$iso60;

			return retornar_error(2,$iso60,$codeerror,$ultimoid,$idpos,$id_distri,$operador,$valor,$movil,$id_pro,$servicio);
		}
		else
		{
			if($bitmap==="303800000A910018") //recarga bien
			{
				$ini=34;
				$iso03=substr($respuesta, $ini, $p_iso03);
				$iso04=substr($respuesta, $ini+=$p_iso03, $p_iso04);
				$iso11=substr($respuesta, $ini+=$p_iso04, $p_iso11);
				$iso12=substr($respuesta, $ini+=$p_iso11, $p_iso12);
				$iso13=substr($respuesta, $ini+=$p_iso12, $p_iso13);
				$iso37=hextobin(substr($respuesta, $ini+=$p_iso13, $p_iso37)); //referencia
				$iso39=hextobin(substr($respuesta, $ini+=$p_iso37, $p_iso39));
				$iso41=hextobin(substr($respuesta, $ini+=$p_iso39, $p_iso41));

				$p_iso44=substr($respuesta, $ini+=$p_iso41, 2);
				$p_iso44=$p_iso44*2;
				$iso44=hextobin(substr($respuesta, $ini+=2, $p_iso44));
				$p_iso48=substr($respuesta, $ini+=$p_iso44, 4);
				$p_iso48=$p_iso48*2;
				$iso48=hextobin(substr($respuesta, $ini+=4, $p_iso48));
				$p_iso60=substr($respuesta, $ini+=$p_iso48, 4);
				$p_iso60=$p_iso60*2;
				$iso60=hextobin(substr($respuesta, $ini+=4, $p_iso60));
				$p_iso61=hexdec(substr($respuesta, $ini+=$p_iso60, 4));
				$p_iso61=$p_iso61*2;
				$iso61=hextobin(substr($respuesta, $ini+=4, $p_iso61));//saldo

				$saldoRemp = str_replace("Saldo: ","",$iso61);
				$saldoRemp = str_replace(" COP","",$saldoRemp);

				$saldoSplit = explode(",",$saldoRemp);
				$entero = $saldoSplit[0];
				$decimal = $saldoSplit[1];
				$entero = str_replace(".","", $entero);
				$realBalance = $entero.".".$decimal;

				$transacionidresp = str_replace("    ","",$iso37);

				$sql="UPDATE venta__servicios SET estado = 1, horatransaccion = CURTIME(), transacionidresp='$transacionidresp', saldo_actual = ROUND(($bolsaanterior-$valor_recar),2),saldo_actual_incentivo = ROUND(($bolsaanterior_inc-$valor_inc),2),realBalance = '$realBalance' WHERE id=$ultimoid";

				$resultven_up=$BD->consultar($sql);

				if($BD->numupdate() == 0){
					$resultven_up2=$BD->consultar($sql);
					if($BD->numupdate() == 0){
						$resultven_up3=$BD->consultar($sql);
					}
				}

				//return "recarga ok: referencia:".$iso37." saldo:".$iso61;

				return array("estado" => 1, "msg" => '');

			}
			else
			{
				//return "5 - error bitmap desconocido bitmap:".$bitmap;

				sleep(2);
				return Consulta($transactionId,3,0,$ultimoid,$bolsaanterior,$operador,$movil,$valor,$id_distri,$servicio,$bolsaanterior_inc,$valor_recar,$valor_inc,$id_pro,$timeout,$cod_movilway,$folder);

				//return retornar_error(2,"error bitmap desconocido",$bitmap,$ultimoid,$idpos,$id_distri,$operador,$valor,$movil,$id_pro,$servicio);
            }
       }
	}
    catch( Exception $e)
    {
 		$mensaje = $e->faultstring;
 		$codeerror=$e->faultcode;

 		if($mensaje=="timeout")
		{
			sleep(2);
			return Consulta($transactionId,3,0,$ultimoid,$bolsaanterior,$operador,$movil,$valor,$id_distri,$servicio,$bolsaanterior_inc,$valor_recar,$valor_inc,$id_pro,$timeout,$cod_movilway,$folder);
		}

 		//return "4- Exception:".$mensaje." ".$codeerror;

 		return retornar_error(2,$mensaje,$codeerror,$ultimoid,$idpos,$id_distri,$operador,$valor,$movil,$id_pro,$servicio);
    }
}
///fin recarga

function Consulta($transactionId,$t,$i,$ultimoid,$bolsaanterior,$operador,$movil,$valor,$id_distri,$servicio,$bolsaanterior_inc,$valor_recar,$valor_inc,$id_pro,$timeout,$cod_movilway,$folder)
{//t=cantidad de validaciones i=intentos

	$BD=new BD();
	$BD->conectar();

	$refh2h=$transactionId;
	//$usuario="          ";//longitud 10  string -> hex

	$sql = "SELECT folder FROM proveedores WHERE id = $id_pro AND (folder IS NULL OR folder != '')";
	$res_fol_pro=$BD->consultar($sql);

	if($BD->numreg($res_fol_pro)>0){

		$folder = $res_fol_pro->fields["folder"];

	}

	$creden = $BD->credenciales($id_distri,$id_pro,$cod_movilway,$folder);

	if($creden["user"] == "" || empty($creden["user"])){

		return retornar_error(2,'Las credenciales del distribuidor no existen',9993,$ultimoid,$idpos,$id_distri,$operador,$valor,$movil,$id_pro,$servicio);
	}

	$usuario=$creden["user"];//longitud 10  string -> hex
	$input=$creden["pass"]; //16  -> hex
	$terminal=$creden["t_id"];
	$key=$creden["com"];
	$key_hex = foo($key);

	$password = strtoupper(bin2hex(cifrar($key_hex,$input)));

	try{
	    $p_iso03=6;//fix
	    $p_iso04=12;//fix
	    $p_iso11=6;//fix
		$p_iso12=6;//fix
		$p_iso13=4;//fix
		$p_iso37=24;//fix alpha
		$p_iso39=4;//fix alpha
		$p_iso41=16;//fix alpha
		$p_iso44=999;//fix alpha
		$p_iso48=999;//llvar
		$p_iso60=999;//llvar
		$p_iso61=999;//llvar
		$p_iso63=999;//llvar

		$port = 10301;//puerto pruebas
		$host = "190.145.60.203";//host pruebas

		$fp = fsockopen($host, $port);
	    if (empty($fp)) {
	        return retornar_error(2,'host no responde',9994,$ultimoid,0,$id_distri,$operador,$valor,$movil,$id_pro,$servicio);
	    }
	    //

	    //$terminal="00000427";//ASCII
	    $send_tpdu="0000000000"; //10
	    $send_type="0800"; //4
	    $send_bitmap="2038000000800014"; //16
	    $send_iso03="400200"; //6
	    $send_iso11=date("His"); //6 identificador mando la hora
	    $send_iso12=date("His"); //6
	    $send_iso13=date("md"); //4
	    $send_iso41=strtoupper(strtohex($terminal)); //4

	    $send_iso60=iso_60_consulta($refh2h, $usuario, $key);//4
	    $send_iso60="00".(strlen($send_iso60)/2).$send_iso60;
	    $version="02";
	    $password="DD640AEB1DDB3394"; //16  -> hex
	    $send_iso62=strtoupper(strtohex($version)).$password;
	    $send_iso62="00".(strlen($send_iso62)/2).$send_iso62;

	    $message=$send_tpdu.$send_type.$send_bitmap.$send_iso03.$send_iso11.$send_iso12.$send_iso13.$send_iso41.$send_iso60.$send_iso62;
	    $send_leng="00".strtoupper(dechex(strlen($message)/2)); //4
	    $message=$send_leng.$message;
	    $message=hextobin($message);
	/*
	    socket_write($socket, $message, strlen($message)) or die("Could not send data to server\n");
	    // get server response
	    $result = socket_read ($socket, 1024) or die("Could not read server response\n");
		// close socket
	    socket_close($socket);
	    $respuesta=strtoupper(bin2hex($result));
	*/
	    $fw=fwrite($fp, $message);
	    stream_set_timeout($fp, $timeout);
	    $result = fread($fp, 2000);

	    $info = stream_get_meta_data($fp);
	    fclose($fp);
		// close socket
		if ($info['timed_out']) {

			return retornar_error(2,'Time out',9994,$ultimoid,0,$id_distri,$operador,$valor,$movil,$id_pro,$servicio);

	        //return "3 - Time out";
	    }

	    $respuesta=strtoupper(bin2hex($result));

	    $resp_leng=substr($respuesta, 0, 4);
	    $tpdu=hexdec(substr($respuesta, 4, 10));
	    $type=substr($respuesta, 14, 4);
	    $bitmap=substr($respuesta, 18, 16);

	    if($bitmap==="303800000A810018") //si estado es ok=recarga ok y ko=no salio
        {
			$ini=34;
			$iso03=substr($respuesta, $ini, $p_iso03);
		    $iso04=substr($respuesta, $ini+=$p_iso03, $p_iso04);
			$iso11=substr($respuesta, $ini+=$p_iso04, $p_iso11);
			$iso12=substr($respuesta, $ini+=$p_iso11, $p_iso12);
			$iso13=substr($respuesta, $ini+=$p_iso12, $p_iso13);
		    $iso37=hextobin(substr($respuesta, $ini+=$p_iso13, $p_iso37)); //referencia
		    $iso39=hextobin(substr($respuesta, $ini+=$p_iso37, $p_iso39));
		    $iso41=hextobin(substr($respuesta, $ini+=$p_iso39, $p_iso41));

            $p_iso48=substr($respuesta, $ini+=$p_iso41, 4);
			$p_iso48=$p_iso48*2;
			if($p_iso48==0)
				$p_iso48=0;
		    $iso48=substr($respuesta, $ini+=4, $p_iso48);

		    $p_iso60=substr($respuesta, $ini+=$p_iso48, 4);
			$p_iso60=$p_iso60*2;
			if($p_iso60==0)
				$p_iso60=6;
		    $iso60=substr($respuesta, $ini+=4, $p_iso60);
            //$key = "100285E637D6F1263334776534336637100285E637D6F126"; //llave cifrado
            $iso60=descifrar($key,$iso60);
            $iso60_1=hextobin(substr($iso60, 6, 16)); // Codigo error
            $iso60_2=hextobin(substr($iso60, 26, 4)); // Estado
            $iso60_3=hextobin(substr($iso60, 34, 44)); // Mensaje



			$p_iso61=hexdec(substr($respuesta, $ini+=$p_iso60, 4));
			$p_iso61=$p_iso61*2;
			if($p_iso61==0)
				$p_iso61=6;
		    $iso61=hextobin(substr($respuesta, $ini+=4, $p_iso61));

		    if(strtolower($iso60_2) == "ok"){

		    	/*$saldoRemp = str_replace("Saldo: ","",$iso61);
				$saldoRemp = str_replace(" COP","",$saldoRemp);

				$saldoSplit = explode(",",$saldoRemp);
				$entero = $saldoSplit[0];
				$decimal = $saldoSplit[1];
				$entero = str_replace(".","", $entero);
				$realBalance = $entero.".".$decimal;*/
				$realBalance = 0;

				$transacionidresp = str_replace("    ","",$iso37);

				$sql="UPDATE venta__servicios SET estado = 1, horatransaccion = CURTIME(), transacionidresp='$transacionidresp', saldo_actual = ROUND(($bolsaanterior-$valor_recar),2),saldo_actual_incentivo = ROUND(($bolsaanterior_inc-$valor_inc),2),realBalance = '$realBalance' WHERE id=$ultimoid";

				$resultven_up=$BD->consultar($sql);

				if($BD->numupdate() == 0){
					$resultven_up2=$BD->consultar($sql);
					if($BD->numupdate() == 0){
						$resultven_up3=$BD->consultar($sql);
					}
				}

				return array("estado" => 1, "msg" => '');

		    }else{
		    	return retornar_error(2,$iso60_3,$iso60_1,$ultimoid,0,$id_distri,$operador,$valor,$movil,$id_pro,$servicio);
		    }

			//return "recarga error: referencia:".$iso37." procod:".$iso60_1." estado:".$iso60_2." mensaje:".$iso60_3;
		}
		else
		{
			if($bitmap==="203800000A81001A") //no existe
			{
				$ini=34;
				$iso03=substr($respuesta, $ini, $p_iso03);
				$iso11=substr($respuesta, $ini+=$p_iso03, $p_iso11);
				$iso12=substr($respuesta, $ini+=$p_iso11, $p_iso12);
				$iso13=substr($respuesta, $ini+=$p_iso12, $p_iso13);
			    $iso37=hextobin(substr($respuesta, $ini+=$p_iso13, $p_iso37)); //referencia
			    $iso39=hextobin(substr($respuesta, $ini+=$p_iso37, $p_iso39));
			    $iso41=hextobin(substr($respuesta, $ini+=$p_iso39, $p_iso41));

	            $p_iso48=substr($respuesta, $ini+=$p_iso41, 4);
				$p_iso48=$p_iso48*2;
				if($p_iso48==0)
					$p_iso48=6;
			    $iso48=substr($respuesta, $ini+=4, $p_iso48);

			    $p_iso60=substr($respuesta, $ini+=$p_iso48, 4);
				$p_iso60=$p_iso60*2;
				if($p_iso60==0)
					$p_iso60=0;
			    $iso60=hextobin(substr($respuesta, $ini+=4, $p_iso60));
				$p_iso61=hexdec(substr($respuesta, $ini+=$p_iso60, 4));
				$p_iso61=$p_iso61*2;
			    $iso61=hextobin(substr($respuesta, $ini+=4, $p_iso61));

			    $p_iso63=hexdec(substr($respuesta, $ini+=$p_iso61, 4));
				$p_iso63=$p_iso63*2;
			    $iso63=hextobin(substr($respuesta, $ini+=4, $p_iso63));

			    $i++;
				if($i<$t)
				{

					$sql = "SELECT id_punto,id_item_servicio FROM venta__servicios WHERE id=$ultimoid";
					$res_v=$BD->consultar($sql);

					$id_pos = $res_v->fields["id_punto"];
					$id_item_servicio = $res_v->fields["id_item_servicio"];

					$sql = "INSERT INTO venta__errores (id_venta, fecha, hora, id_punto, id_distri, operador, id_item_servicio, valor, movil) VALUES ($ultimoid,CURDATE(),CURTIME(),$id_pos,$id_distri,$operador,$id_item_servicio,$valor,'$movil')";
					$res_error=$BD->consultar($sql);

					sleep(5);
					Consulta($transactionId,$t,$i,$ultimoid,$bolsaanterior,$operador,$movil,$valor,$id_distri,$servicio,$bolsaanterior_inc,$valor_recar,$valor_inc,$id_pro,$timeout);

				}else{

					return retornar_error(2,$iso60,$iso37,$ultimoid,0,$id_distri,$operador,$valor,$movil,$id_pro,$servicio);

				}

				//return "recarga ok: referencia:".$iso37." mensaje:".$iso60;
			}
			else
			{

				return retornar_error(2,"error bitmap desconocido bitmap",$bitmap,$ultimoid,0,$id_distri,$operador,$valor,$movil,$id_pro,$servicio);
				//return "5 - error bitmap desconocido bitmap:".$bitmap;
            }
        }
	}
	catch( Exception $e)
    {
 		$mensaje = $e->faultstring;
 		$codeerror=$e->faultcode;

 		return retornar_error(2,$mensaje,$codeerror,$ultimoid,0,$id_distri,$operador,$valor,$movil,$id_pro,$servicio);

 		//return "4- Exception:".$mensaje." ".$codeerror;
    }
}

//fin consulta

function retornar_error($op,$msg,$code,$ultimoid,$idpos,$id_distri,$operador,$valor,$movil,$id_pro,$servicio){

	$BD=new BD();
	$BD->conectar();

	if($op == 1){

		$sql = "INSERT INTO venta__errores (id_venta, fecha, hora, id_punto, id_distri, operador, id_item_servicio, valor, movil, msg) VALUES ($ultimoid,CURDATE(),CURTIME(),$idpos,$id_distri,$operador,0,$valor,'$movil', '$msg')";
		$res_error=$BD->consultar($sql);

		if(!$res_error){
			$res_error2=$this->BD->consultar($sql);
			if(!$res_error2){
				$res_error3=$this->BD->consultar($sql);
			}
		}

		return array("estado" => 4, "msg" => 'Error al intentar realizar la recarga');

	}else{

		$sql = "UPDATE venta__servicios SET estado = 2, horatransaccion = CURTIME(), code_error_txt = '$code', mens_error='$msg' WHERE id=$ultimoid";
		$resultven_up=$BD->consultar($sql);

		if($BD->numupdate() == 0){
			$resultven_up2=$BD->consultar($sql);
			if($BD->numupdate() == 0){
				$resultven_up3=$BD->consultar($sql);
			}
		}

		$mensaje = $msg;
		$msg = "Error al realizar la recarga";
		$codigo = $code;
		$sub_codigo = 0;

		//if($codigo == 69 && $operador == 6){

			$findme   = '[';
			$buscar = strpos($mensaje, $findme);

			if ($buscar !== false) {
				$code_msg = explode("[",$mensaje);
				if(count($code_msg) > 0){
					$sub_codigo = intval($code_msg[1]);
				}
			}

		//}

		$sql = "SELECT msg FROM venta__msg_errores WHERE codigo = '$codigo' AND sub_codigo = '$sub_codigo' AND id_operador = '$servicio' AND id_proveedor = $id_pro";
		$res_mensaje=$BD->consultar($sql);

		if($BD->numreg($res_mensaje)>0){
			$msg = $res_mensaje->fields["msg"];
		}

		return array("estado" => 4, "msg" => $msg);

	}
}

// Fin recarga

?>
