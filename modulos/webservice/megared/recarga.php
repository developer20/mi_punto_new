<?php
//error_reporting(E_ALL);
//ini_set('display_errors', '1');


class SoapClientTimeout extends SoapClient
{
	private $timeout = 0;
	private $connecttimeout = 0;
	private $sslverifypeer = true;

	public function __construct($wsdl, $options) {
		//"POP" our own defined options from the $options array before we call our parent constructor
		//to ensure we don't pass unknown/invalid options to our parent
		if (isset($options['timeout'])) {
			$this->__setTimeout($options['timeout']);
			unset($options['timeout']);
		}
		if (isset($options['connecttimeout'])) {
			$this->__setConnectTimeout($options['connecttimeout']);
			unset($options['connecttimeout']);
		}
		if (isset($options['sslverifypeer'])) {
			$this->__setSSLVerifyPeer($options['sslverifypeer']);
			unset($options['sslverifypeer']);
		}
		//Now call parent constructor
		parent::__construct($wsdl, $options);
	}

	public function __setTimeout($timeoutms)
	{
		if (!is_int($timeoutms) && !is_null($timeoutms) || $timeoutms<0)
			throw new Exception("Invalid timeout value");

		$this->timeout = $timeoutms;
	}

	public function __getTimeout()
	{
		return $this->timeout;
	}

	public function __setConnectTimeout($connecttimeoutms)
	{
		if (!is_int($connecttimeoutms) && !is_null($connecttimeoutms) || $connecttimeoutms<0)
			throw new Exception("Invalid connecttimeout value");

		$this->connecttimeout = $connecttimeoutms;
	}

	public function __getConnectTimeout()
	{
		return $this->connecttimeout;
	}

	public function __setSSLVerifyPeer($sslverifypeer)
	{
		if (!is_bool($sslverifypeer))
			throw new Exception("Invalid sslverifypeer value");

		$this->sslverifypeer = $sslverifypeer;
	}

	public function __getSSLVerifyPeer()
	{
		return $this->sslverifypeer;
	}

	public function __doRequest($request, $location, $action, $version, $one_way = FALSE)
	{
		if (($this->timeout===0) && ($this->connecttimeout===0))
		{
			// Call via parent because we require no timeout
			$response = parent::__doRequest($request, $location, $action, $version, $one_way);
		}
		else
		{
			// Call via Curl and use the timeout
			$curl = curl_init($location);
			if ($curl === false)
				throw new Exception('Curl initialisation failed');

			$options = array(
				CURLOPT_VERBOSE => false,
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_POST => true,
				CURLOPT_POSTFIELDS => $request,
				CURLOPT_HEADER => false,
				CURLOPT_NOSIGNAL => true,	//http://www.php.net/manual/en/function.curl-setopt.php#104597
				CURLOPT_HTTPHEADER => array(sprintf('Content-Type: %s', $version == 2 ? 'application/soap+xml' : 'text/xml'), sprintf('SOAPAction: %s', $action)),
				CURLOPT_SSL_VERIFYPEER => $this->sslverifypeer
				);

			if ($this->timeout>0) {
				if (defined('CURLOPT_TIMEOUT_MS')) {	//Timeout in MS supported?
					$options[CURLOPT_TIMEOUT_MS] = $this->timeout;
				} else	{ //Round(up) to second precision
					$options[CURLOPT_TIMEOUT] = ceil($this->timeout/1000);
				}
			}
			if ($this->connecttimeout>0) {
				if (defined('CURLOPT_CONNECTTIMEOUT_MS')) {	//ConnectTimeout in MS supported?
					$options[CURLOPT_CONNECTTIMEOUT_MS] = $this->connecttimeout;
				} else { //Round(up) to second precision
					$options[CURLOPT_CONNECTTIMEOUT] = ceil($this->connecttimeout/1000);
				}
			}

			if (curl_setopt_array($curl, $options) === false)
				throw new Exception('Failed setting CURL options');

			$response = curl_exec($curl);

			if (curl_errno($curl))
			{
				//throw new Exception(curl_error($curl));

				$string = "<SOAP-ENV:Envelope xmlns:SOAP-ENV='http://schemas.xmlsoap.org/soap/envelope/'>
				<SOAP-ENV:Body>
				<SOAP-ENV:Fault>
				<faultcode>SOAP-ENV:Server</faultcode>
				<faultstring>timeout</faultstring>
			</SOAP-ENV:Fault>
		</SOAP-ENV:Body>
	</SOAP-ENV:Envelope>";


	return $string;
}
curl_close($curl);
}

		// Return?
if (!$one_way)
	return ($response);
}
}


function Recarga($valor,$movil,$operador,$transactionId,$timeout,$ultimoid,$bolsaanterior,$key_pes,$servicio,$bolsaanterior_inc,$valor_recar,$valor_inc,$id_pro,$ean,$token,$cod_movilway,$id_distri)
{

	$idpos=0;
	//$id_distri=0;
	$folder = "";

	if(isset($_SESSION[$GLOBALS["SESION_POS"]])){

		$oSession = unserialize($_SESSION[$GLOBALS["SESION_POS"]]);

		$idpos=$oSession->VSidpos;
		//$id_distri=$oSession->VSid_distri;

	}else{
		die();
	}

	$BD=new BD();
	$BD->conectar();

	// INICIA SIMULA RECARGAS PRUEBAS

	$sql="UPDATE venta__servicios SET estado = 1, horatransaccion = CURTIME(), saldo_actual = ROUND(($bolsaanterior-$valor_recar),2),saldo_actual_incentivo = ROUND(($bolsaanterior_inc-$valor_inc),2) WHERE id=$ultimoid";

	$resultven_up=$BD->consultar($sql);

	if($BD->numupdate() == 0){
		$resultven_up2=$BD->consultar($sql);
		if($BD->numupdate() == 0){
			$resultven_up3=$BD->consultar($sql);
		}
	}

	return array("estado" => 1, "msg" => '');

	// FIN SIMULA RECARGAS PRUEBAS

	if(!isset($_SESSION["key_trans"])){
		$sql = "INSERT INTO venta__errores (id_venta, fecha, hora, id_punto, id_distri, operador, id_item_servicio, valor, movil, msg) VALUES ($ultimoid,CURDATE(),CURTIME(),$idpos,$id_distri,$operador,0,$valor,'$movil', 'Error al intentar realizar la recarga')";
		$res_error=$BD->consultar($sql);

		return array("estado" => 4, "msg" => 'Error al intentar realizar la recarga');
	}

	if(strcmp($key_pes,$_SESSION["key_trans"]) != 0){

		$sql = "INSERT INTO venta__errores (id_venta, fecha, hora, id_punto, id_distri, operador, id_item_servicio, valor, movil, msg) VALUES ($ultimoid,CURDATE(),CURTIME(),$idpos,$id_distri,$operador,0,$valor,'$movil', 'Error al intentar realizar la recarga')";
		$res_error=$BD->consultar($sql);

		return array("estado" => 4, "msg" => 'Error al intentar realizar la recarga');
	}

	$sql = "SELECT id FROM token_ventas WHERE id_pos = $idpos AND token = '$key_pes' AND id_venta = $ultimoid";
	$res_val_venta=$BD->consultar($sql);

	if($BD->numreg($res_val_venta) == 0){

		$sql = "INSERT INTO venta__errores (id_venta, fecha, hora, id_punto, id_distri, operador, id_item_servicio, valor, movil, msg) VALUES ($ultimoid,CURDATE(),CURTIME(),$idpos,$id_distri,$operador,0,$valor,'$movil', 'Error al intentar realizar la recarga, la venta no existe')";
		$res_error=$BD->consultar($sql);

		return array("estado" => 4, "msg" => 'Error al intentar realizar la recarga');
	}

	$sql = "SELECT id_distri FROM puntos WHERE idpos=$idpos";
	$res_pos=$BD->consultar($sql);

	if($BD->numreg($res_pos) == 0){

		$sql = "UPDATE venta__servicios SET estado = 2, horatransaccion = CURTIME(), code_error = 9991, mens_error='Error al realizar la recarga' WHERE id=$ultimoid";
		$resultven_up=$BD->consultar($sql);

		if($BD->numupdate() == 0){
			$resultven_up2=$BD->consultar($sql);
			if($BD->numupdate() == 0){
				$resultven_up3=$BD->consultar($sql);
			}
		}

		return array("estado" => 4, "msg" => 'Error al realizar la recarga');
	}

	/*$idDistri = $res_pos->fields["id_distri"];

	if($id_distri != $idDistri){

		$sql = "UPDATE venta__servicios SET estado = 2, horatransaccion = CURTIME(), code_error = 9992, mens_error='Error al realizar la recarga' WHERE id=$ultimoid";
		$resultven_up=$BD->consultar($sql);

		if($BD->numupdate() == 0){
			$resultven_up2=$BD->consultar($sql);
			if($BD->numupdate() == 0){
				$resultven_up3=$BD->consultar($sql);
			}
		}

		return array("estado" => 4, "msg" => 'Error al realizar la recarga');
	}*/

	/*$sql = "SELECT usuario AS user,pass,comercio FROM {$GLOBALS["BD_NAME"]}.recargas__usudistri_prov WHERE id_distri = $id_distri AND id_proveedor = $id_pro";
	$res_dis = $BD->consultar($sql);*/

	$sql = "SELECT folder FROM proveedores WHERE id = $id_pro AND (folder IS NULL OR folder != '')";
	$res_fol_pro=$BD->consultar($sql);

	if($BD->numreg($res_fol_pro)>0){

		$folder = $res_fol_pro->fields["folder"];

	}

	$creden = $BD->credenciales($id_distri,$id_pro,$cod_movilway,$folder);

	if($creden["user"] == "" || empty($creden["user"])){

		$sql = "UPDATE venta__servicios SET estado = 2, horatransaccion = CURTIME(), code_error = 9993, mens_error='Las credenciales del distribuidor no existen' WHERE id=$ultimoid";
		$resultven_up=$BD->consultar($sql);

		if($BD->numupdate() == 0){
			$resultven_up2=$BD->consultar($sql);
			if($BD->numupdate() == 0){
				$resultven_up3=$BD->consultar($sql);
			}
		}

		return array("estado" => 4, "msg" => 'Error al realizar la recarga');
	}

	//$wsdl='https://api-co.movilway.net:5803/Service/ExtendedApi/Public/ExtendedApi.svc?singleWsdl';
	$wsdl='https://proyecto01megared.megared.net.co/wsmegared_integrated/index_ws.php?wsdl';
	try{

		$horatr = date("H:i:s");
		$usuario=$creden["user"];
		$password=$creden["pass"];
		$comercio=$creden["com"];
		$canal="5";
		$celular=$movil;
		$realBalance=0;

		/*$client=new SoapClientTimeout($wsdl,array('timeout'   => $timeout,'sslverifypeer'   => false, 'connecttimeout'=> 120000));

		$param=array('celular' =>$celular, 'operador' =>$operador,'valor' =>$valor,'password' =>$password,'usuario' =>$usuario,'comercio' =>$comercio,'canal' =>$canal);

		$resultado=$client->__soapCall('RecargarWS',$param);

		$respuesta=explode("|",$resultado);
		$responseCode=$respuesta[0];*/

		$resultado="444|sdfsdfs|1234|0";
		$respuesta=explode("|",$resultado);
		$responseCode=$respuesta[0];

		if($responseCode=='444')
		{
			$Fee = "";

			$identificadorex=$respuesta[2];
			$realBalance=$respuesta[3];

			$sql="UPDATE venta__servicios SET estado = 1, horatransaccion = CURTIME(), transacionidresp='$identificadorex', saldo_actual = ROUND(($bolsaanterior-$valor_recar),2),saldo_actual_incentivo = ROUND(($bolsaanterior_inc-$valor_inc),2),realBalance = '$realBalance',fee = '$Fee' WHERE id=$ultimoid";

			$resultven_up=$BD->consultar($sql);

			if($BD->numupdate() == 0){
				$resultven_up2=$BD->consultar($sql);
				if($BD->numupdate() == 0){
					$resultven_up3=$BD->consultar($sql);
				}
			}

			return array("estado" => 1, "msg" => '');
		}
		else
		{
			$mensaje=$respuesta[1];

			$sql = "UPDATE venta__servicios SET estado = 2, horatransaccion = CURTIME(), code_error = $responseCode, mens_error='$mensaje',realBalance = '$realBalance' WHERE id=$ultimoid";
			$resultven_up=$BD->consultar($sql);

			if($BD->numupdate() == 0){
				$resultven_up2=$BD->consultar($sql);
				if($BD->numupdate() == 0){
					$resultven_up3=$BD->consultar($sql);
				}
			}

			$msg = "Error al realizar la recarga";
			$codigo = $responseCode;
			$sub_codigo = 0;

			//if($codigo == 69 && $operador == 6){

				$findme   = '[';
				$buscar = strpos($mensaje, $findme);

				if ($buscar !== false) {
					$code_msg = explode("[",$mensaje);
					if(count($code_msg) > 0){
						$sub_codigo = intval($code_msg[1]);
					}
				}

			//}

			$sql = "SELECT msg FROM venta__msg_errores WHERE codigo = '$codigo' AND sub_codigo = '$sub_codigo' AND id_operador = '$servicio' AND id_proveedor = $id_pro";
			$res_mensaje=$BD->consultar($sql);

			if($BD->numreg($res_mensaje)>0){
				$msg = $res_mensaje->fields["msg"];
			}

			return array("estado" => 4, "msg" => $msg);
		}


	}catch(SoapFault $e)
	{

		$mensaje = $e->faultstring;
		$codeerror=99;

		if($mensaje=="timeout")
		{
			sleep(2);
			return Consulta($transactionId,3,0,$ultimoid,$bolsaanterior,$operador,$movil,$valor,$id_distri,$servicio,$bolsaanterior_inc,$valor_recar,$valor_inc,$id_pro,$cod_movilway,$folder);
		}

		$sql = "UPDATE venta__servicios SET estado = 2, horatransaccion = CURTIME(), code_error = $codeerror, mens_error='$mensaje' WHERE id=$ultimoid";
		$resultven_up=$BD->consultar($sql);

		if($BD->numupdate() == 0){
			$resultven_up2=$BD->consultar($sql);
			if($BD->numupdate() == 0){
				$resultven_up3=$BD->consultar($sql);
			}
		}

		return array("estado" => 4, "msg" => "Error al realizar la recarga");

	}

}
///fin recarga

function Consulta($transactionId,$t,$i,$ultimoid,$bolsaanterior,$operador,$movil,$valor,$id_distri,$servicio,$bolsaanterior_inc,$valor_recar,$valor_inc,$id_pro,$cod_movilway,$folder)//t=cantidad de validaciones i=intentos
{

	$BD=new BD();
	$BD->conectar();

	//$wsdl='https://api-co.movilway.net:5803/Service/ExtendedApi/Public/ExtendedApi.svc?singleWsdl';
	$wsdl='https://proyecto01megared.megared.net.co/wsmegared_integrated/index_ws.php?wsdl';
	try{

		/*$sql = "SELECT usuario AS user,pass,comercio FROM {$GLOBALS["BD_NAME"]}.recargas__usudistri_prov WHERE id_distri = $id_distri AND id_proveedor = $id_pro";
		$res_dis=$BD->consultar($sql);*/

		$sql = "SELECT folder FROM proveedores WHERE id = $id_pro AND (folder IS NULL OR folder != '')";
		$res_fol_pro=$BD->consultar($sql);

		if($BD->numreg($res_fol_pro)>0){

			$folder = $res_fol_pro->fields["folder"];

		}

		$creden = $BD->credenciales($id_distri,$id_pro,$cod_movilway,$folder);

		$horatr = date("H:i:s");
		$usuario=$creden["user"];
		$password=$creden["pass"];
		$comercio=$creden["com"];

		$canal="5";
		$celular=$movil;

		$client=new SoapClientTimeout($wsdl,array('timeout'   => 0,'sslverifypeer'   => false, 'connecttimeout'=> 0));

		$param=array('celular' =>$celular, 'operador' =>$operador,'valor' =>$valor,'password' =>$password,'usuario' =>$usuario,'comercio' =>$comercio,'canal' =>$canal);

		$resultado=$client->__soapCall('ConsultaRecarga',$param);
		/*
		if($resultado == ""){
			$sql = "UPDATE venta__servicios SET estado = 2, horatransaccion = CURTIME(), code_error = 0, mens_error='Sin respuesta del proveedor',realBalance = '0' WHERE id=$ultimoid";
			$resultven_up=$BD->consultar($sql);

			return array("estado" => 4, "msg" => "Error al realizar la recarga");
		}*/

		$respuesta=explode("|",$resultado);
		$responseCode=$respuesta[0];
		$realBalance=0;

		if($responseCode==444 && is_int($responseCode))
		{

			$Fee = "";
			$identificadorex=$respuesta[2];
			$realBalance=$respuesta[3];

			$sql="UPDATE venta__servicios SET estado = 1, horatransaccion = CURTIME(), transacionidresp='$identificadorex', saldo_actual = ROUND(($bolsaanterior-$valor_recar),2),saldo_actual_incentivo = ROUND(($bolsaanterior_inc-$valor_inc),2),realBalance = '$realBalance',fee = '$Fee' WHERE id=$ultimoid";

			$resultven_up=$BD->consultar($sql);

			if($BD->numupdate() == 0){
				$resultven_up2=$BD->consultar($sql);
				if($BD->numupdate() == 0){
					$resultven_up3=$BD->consultar($sql);
				}
			}

			return array("estado" => 1, "msg" => '');
		}
		else
		{

			$i++;
			if($i<$t)
			{

				$sql = "SELECT id_punto,id_distri,operador,id_item_servicio,valor,movil FROM venta__servicios WHERE id=$ultimoid";
				$res_v=$BD->consultar($sql);

				$id_pos = $res_v->fields["id_punto"];
				$id_distri = $res_v->fields["id_distri"];
				$operador = $res_v->fields["operador"];
				$id_item_servicio = $res_v->fields["id_item_servicio"];
				$valor = $res_v->fields["valor"];
				$movil = $res_v->fields["movil"];

				$sql = "INSERT INTO venta__errores (id_venta, fecha, hora, id_punto, id_distri, operador, id_item_servicio, valor, movil) VALUES ($ultimoid,CURDATE(),CURTIME(),$id_pos,$id_distri,$operador,$id_item_servicio,$valor,'$movil')";
				$res_error=$BD->consultar($sql);

				sleep(5);

				Consulta($transactionId,$t,$i,$ultimoid,$bolsaanterior,$operador,$movil,$valor,$id_distri,$servicio,$bolsaanterior_inc,$valor_recar,$valor_inc,$id_pro);

			}else{

				$mensaje=$respuesta[1];

				$sql = "UPDATE venta__servicios SET estado = 2, horatransaccion = CURTIME(), code_error = $responseCode, mens_error='$mensaje',realBalance = '$realBalance' WHERE id=$ultimoid";
				$resultven_up=$BD->consultar($sql);

				if($BD->numupdate() == 0){
					$resultven_up2=$BD->consultar($sql);
					if($BD->numupdate() == 0){
						$resultven_up3=$BD->consultar($sql);
					}
				}

				$msg = "Error al realizar la recarga";
				$codigo = $responseCode;
				$sub_codigo = 0;

				//if($codigo == 69 && $operador == 6){

					$findme   = '[';
					$buscar = strpos($mensaje, $findme);

					if ($buscar !== false) {
						$code_msg = explode("[",$mensaje);
						if(count($code_msg) > 0){
							$sub_codigo = intval($code_msg[1]);
						}
					}
				//}

				$sql = "SELECT msg FROM venta__msg_errores WHERE codigo = '$codigo' AND sub_codigo = '$sub_codigo' AND id_operador = '$servicio' AND id_proveedor = $id_pro";
				$res_mensaje=$BD->consultar($sql);

				if($BD->numreg($res_mensaje)>0){
					$msg = $res_mensaje->fields["msg"];
				}

				return array("estado" => 4, "msg" => $msg);

			}
		}


	}catch(SoapFault $e)
	{

		$mensaje = $e->faultstring;
		$codeerror=99;


		$sql = "UPDATE venta__servicios SET estado = 2, horatransaccion = CURTIME(), code_error = $codeerror, mens_error='$mensaje' WHERE id=$ultimoid";
		$resultven_up=$BD->consultar($sql);

		if($BD->numupdate() == 0){
			$resultven_up2=$BD->consultar($sql);
			if($BD->numupdate() == 0){
				$resultven_up3=$BD->consultar($sql);
			}
		}

		return array("estado" => 4, "msg" => "Error al realizar la recarga");

	}


}

///fin recarga


?>
