<?php

function cifrar($key,$input)
{
	$td = mcrypt_module_open('tripledes', '', 'ecb', '');
	$iv = mcrypt_create_iv(mcrypt_enc_get_iv_size($td), MCRYPT_RAND);
	mcrypt_generic_init($td, $key, $iv);
	$encrypted_data = mcrypt_generic($td, $input);
	mcrypt_generic_deinit($td);
	mcrypt_module_close($td);
	return $encrypted_data;
}

function foo($hex) {
	$rv = '';
	foreach(str_split($hex, 2) as $b) {
		$rv .= chr(hexdec($b));
	}
	return $rv;
}

function hextobin($hexstr)
{
	$n = strlen($hexstr);
	$sbin="";
	$i=0;
	while($i<$n)
	{
		$a =substr($hexstr,$i,2);
		$c = pack("H*",$a);
		if ($i==0){$sbin=$c;}
		else {$sbin.=$c;}
		$i+=2;
	}
	return $sbin;
}

function strtohex($string)
{
	$string = str_split($string);
	foreach($string as &$char)
		$char = dechex(ord($char));
	return implode('',$string);
}

function strposa($haystack, $needle, $offset=0) //busca en un string un array de palabras
{
	if(!is_array($needle)) $needle = array($needle);
	foreach($needle as $query) {
        if(strpos($haystack, $query, $offset) !== false) return true; // stop on first true result
    }
    return false;
}

function zerofill ($var, $zerofill){
    return str_pad($var, $zerofill, '0', STR_PAD_LEFT);
}



function Recarga($valor,$movil,$operador,$transactionId,$timeout,$ultimoid,$bolsaanterior,$key_pes,$servicio,$bolsaanterior_inc,$valor_recar,$valor_inc,$id_pro,$ean,$token_val,$ticket,$lote,$terminal,$id_ti)
{

	/*$idpos=0;
	$id_distri=0;

	if(isset($_SESSION[$GLOBALS["SESION_POS"]])){

		$oSession = unserialize($_SESSION[$GLOBALS["SESION_POS"]]);

		$idpos=$oSession->VSidpos;
		$id_distri=$oSession->VSid_distri;

	}else{
		die();
	}*/

	$BD=new BD();
	$BD->conectar();

	$sql = "SELECT token_puntos.id_pos,puntos.id_distri FROM token_puntos,puntos WHERE token_puntos.token = '$token_val' AND token_puntos.id_pos = puntos.idpos";
	$res_exits_to = $BD->consultar($sql);

	if($BD->numreg($res_exits_to) == 0){
		die();
	}

	// INICIA SIMULA RECARGAS PRUEBAS

	$sql="UPDATE venta__servicios SET estado = 1, horatransaccion = CURTIME(), saldo_actual = ROUND(($bolsaanterior-$valor_recar),2),saldo_actual_incentivo = ROUND(($bolsaanterior_inc-$valor_inc),2) WHERE id=$ultimoid";

	$resultven_up=$BD->consultar($sql);

	if($BD->numupdate() == 0){
		$resultven_up2=$BD->consultar($sql);
		if($BD->numupdate() == 0){
			$resultven_up3=$BD->consultar($sql);
		}
	}

	//return "recarga ok: referencia:".$iso37." saldo:".$iso61;

	return array("estado" => 1, "msg" => '');


	// FIN SIMULA RECARGAS PRUEBAS

	$idpos=$res_exits_to->fields["id_pos"];
	$id_distri=$res_exits_to->fields["id_distri"];

	$sql = "UPDATE venta__ticket SET estado = 0,ticket = $ticket WHERE id = $id_ti AND estado = 1";
	$res_ti_up=$BD->consultar($sql);

	if($BD->numupdate() == 0){
		$res_ti_up2=$BD->consultar($sql);
		if($BD->numupdate() == 0){
			$res_ti_up3=$BD->consultar($sql);
		}
	}

	$sql="UPDATE venta__servicios SET estado = 1, horatransaccion = CURTIME(), saldo_actual = ROUND(($bolsaanterior-$valor_recar),2),saldo_actual_incentivo = ROUND(($bolsaanterior_inc-$valor_inc),2) WHERE id=$ultimoid";

	$resultven_up=$BD->consultar($sql);

	if($BD->numupdate() == 0){
		$resultven_up2=$BD->consultar($sql);
		if($BD->numupdate() == 0){
			$resultven_up3=$BD->consultar($sql);
		}
	}

	return array("estado" => 1, "msg" => '');

	if(!isset($_SESSION["key_trans"])){

		return retornar_error(1,'Error al intentar realizar la recarga',0,$ultimoid,$idpos,$id_distri,$operador,$valor,$movil,$id_pro,$servicio,$id_ti,0,0,0,0,0);

	}

	if(strcmp($key_pes,$_SESSION["key_trans"]) != 0){

		return retornar_error(1,'Error al intentar realizar la recarga',0,$ultimoid,$idpos,$id_distri,$operador,$valor,$movil,$id_pro,$servicio,$id_ti,0,0,0,0,0);
	}

	$sql = "SELECT id FROM token_ventas WHERE id_pos = $idpos AND token = '$key_pes' AND id_venta = $ultimoid";
	$res_val_venta=$BD->consultar($sql);

	if($BD->numreg($res_val_venta) == 0){

		return retornar_error(1,'Error al intentar realizar la recarga, la venta no existe',0,$ultimoid,$idpos,$id_distri,$operador,$valor,$movil,$id_pro,$servicio,$id_ti,0,0,0,0,0);

	}

	$sql = "SELECT id_distri FROM puntos WHERE idpos=$idpos";
	$res_pos=$BD->consultar($sql);

	if($BD->numreg($res_pos) == 0){

		return retornar_error(2,'Error al realizar la recarga',9991,$ultimoid,$idpos,$id_distri,$operador,$valor,$movil,$id_pro,$servicio,$id_ti,0,0,0,0,0);

	}

	$idDistri = $res_pos->fields["id_distri"];

	if($id_distri != $idDistri){

		return retornar_error(2,'Error al realizar la recarga, ocurrio un error con el distribuidor',9992,$ultimoid,$idpos,$id_distri,$operador,$valor,$movil,$id_pro,$servicio,$id_ti,0,0,0,0,0);

	}

	$creden = $BD->credenciales($id_distri,$id_pro);

	if($creden["user"] == "" || empty($creden["user"])){

		return retornar_error(2,'Las credenciales del distribuidor no existen',9993,$ultimoid,$idpos,$id_distri,$operador,$valor,$movil,$id_pro,$servicio,$id_ti,0,0,0,0,0);
	}

	$usuario=$creden["user"];//longitud 10  string -> hex
	$password=$creden["pass"]; //16  -> hex

}
///fin recarga

//fin reverso

function retornar_error($op,$msg,$code,$ultimoid,$idpos,$id_distri,$operador,$valor,$movil,$id_pro,$servicio,$id_ti,$hora_txt,$fecha_txt,$RRN,$code_resp,$code_auto){

	$BD=new BD();
	$BD->conectar();

	$sql = "UPDATE venta__ticket SET estado = 0 WHERE id = $id_ti AND estado = 1";
	$res_ti_up=$BD->consultar($sql);

	if($BD->numupdate() == 0){
		$res_ti_up2=$BD->consultar($sql);
		if($BD->numupdate() == 0){
			$res_ti_up3=$BD->consultar($sql);
		}
	}

	if($op == 1){

		$sql = "INSERT INTO venta__errores (id_venta, fecha, hora, id_punto, id_distri, operador, id_item_servicio, valor, movil, msg) VALUES ($ultimoid,CURDATE(),CURTIME(),$idpos,$id_distri,$operador,0,$valor,'$movil', '$msg')";
		$res_error=$BD->consultar($sql);

		if(!$res_error){
			$res_error2=$BD->consultar($sql);
			if(!$res_error2){
				$res_error3=$BD->consultar($sql);
			}
		}

		return array("estado" => 4, "msg" => 'Error al intentar realizar la recarga');

	}else{

		$sql = "UPDATE venta__servicios SET estado = 2, horatransaccion = CURTIME(), code_error = '$code', mens_error='$msg', hora_tx = '$hora_txt',fecha_tx = '$fecha_txt',rev_RRN = '$RRN',rev_codigo_respuesta = '$code_resp',rev_codigo_autorizacion = '$code_auto'  WHERE id=$ultimoid";
		$resultven_up=$BD->consultar($sql);

		if($BD->numupdate() == 0){
			$resultven_up2=$BD->consultar($sql);
			if($BD->numupdate() == 0){
				$resultven_up3=$BD->consultar($sql);
			}
		}

		$mensaje = $msg;
		$msg = "Error al realizar la recarga";
		$codigo = $code;
		$sub_codigo = 0;

		//if($codigo == 69 && $operador == 6){

			$findme   = '[';
			$buscar = strpos($mensaje, $findme);

			if ($buscar !== false) {
				$code_msg = explode("[",$mensaje);
				if(count($code_msg) > 0){
					$sub_codigo = intval($code_msg[1]);
				}
			}

		//}

		$sql = "SELECT msg FROM venta__msg_errores WHERE codigo = '$codigo' AND sub_codigo = '$sub_codigo' AND id_operador = '$servicio' AND id_proveedor = $id_pro";
		$res_mensaje=$BD->consultar($sql);

		if($BD->numreg($res_mensaje)>0){
			$msg = $res_mensaje->fields["msg"];
		}

		return array("estado" => 4, "msg" => $msg);

	}
}

?>
