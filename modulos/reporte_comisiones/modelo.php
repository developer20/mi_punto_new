<?php
require("../../config/mainModel.php");
class reporte_comisiones{

   public function charge_Period()
   {
	   $oSession = unserialize($_SESSION[$GLOBALS["SESION_POS"]]);
	   $idpos=$oSession->VSidpos;
		
	   $array_response = array();
		
	   $BD=new BD();
	   $BD->conectar();
		
		$sql = "SELECT DISTINCT periodo FROM {$GLOBALS["BD_NAME"]}.comisiones_puntos";
		
		$res = $BD->consultar($sql);
		
		if($BD->numreg($res)>0){
			while(!$res ->EOF){
				$array_response[] = array("id_per" => $res->fields['periodo'], "period" => $res->fields['periodo']);
				$res ->MoveNext();	
			}
		}
			
		return $array_response;
		
   }

   public function Cargar_Informe($periodo,$est_com){

   		$oSession = unserialize($_SESSION[$GLOBALS["SESION_POS"]]);
	   $idpos=$oSession->VSidpos;
		
	   $array_response = array();
		
	   $BD=new BD();
	   $BD->conectar();

	   $cad_period = "";
	   $cad_est = "";

	   if($periodo != ""){
	   		$cad_period = " AND periodo = '$periodo' ";
	   }
	   if($est_com != 0){
	   		if($est_com == 1){
	   			$cad_est = " AND saldo_pendiente = 0";
	   		}else{
	   			$cad_est = " AND saldo_pendiente != 0";
	   		}
	   }
		
		$sql = "SELECT id, periodo, total_comision, saldo_pendiente FROM {$GLOBALS["BD_NAME"]}.comisiones_puntos WHERE idpos = $idpos $cad_period $cad_est";
		
		$res = $BD->consultar($sql);
		
		if($BD->numreg($res)>0){
			$con = 1;
			while(!$res ->EOF){
				$array_response[] = array("con" => $con, "id" => $res->fields['id'], "periodo" => $res->fields['periodo'], "total" => '$ '.number_format($res->fields['total_comision'],0,'.',','), "pendiente" => '$ '.number_format($res->fields['saldo_pendiente'],0,'.',','));
				$res ->MoveNext();
				$con++;	
			}
		}
			
		return $array_response;

   }


    public function Datos_CSV($periodo,$est_com)
   {
	   $oSession = unserialize($_SESSION[$GLOBALS["SESION_POS"]]);
	   $idpos=$oSession->VSidpos;
		
	   $array_response = array();
		
	   $BD=new BD();
	   $BD->conectar();

	   $cad_period = "";
	   $cad_est = "";

	   if($periodo != ""){
	   		$cad_period = " AND periodo = '$periodo' ";
	   }
	   if($est_com != 0){
	   		if($est_com == 1){
	   			$cad_est = " AND saldo_pendiente = 0";
	   		}else{
	   			$cad_est = " AND saldo_pendiente != 0";
	   		}
	   }

	   $sql = "SELECT pac.id idpago, cop.periodo periodo, cop.idpos idpos, pun.razon nom_punto, cop.total_comision, pac.valor pag_tot, pac.saldo_pendiente, pac.fecha, pac.hora, pac.usuario, pac.distri, d.nombre_corto,
	   			(SELECT sum(valor_venta) FROM {$GLOBALS["BD_NAME"]}.simcards WHERE id_pagocomi = pac.id) venta_sim, 
	   			(SELECT sum(valor_venta) FROM {$GLOBALS["BD_NAME"]}.producto WHERE id_pagocomi = pac.id) venta_prod, 
	   			(SELECT sum(valor) FROM {$GLOBALS["BD_NAME"]}.efectivo_comi_puntos WHERE id_pagocomi = pac.id) valor_efectivo 
	   			FROM {$GLOBALS["BD_NAME"]}.comisiones_puntos cop, {$GLOBALS["BD_NAME"]}.pago_comisiones pac, {$GLOBALS["BD_POS"]}.puntos pun, {$GLOBALS["BD_NAME"]}.distribuidores d 
	   			WHERE cop.idpos = $idpos AND cop.id = pac.id_comi_punto AND cop.idpos = pun.idpos AND pac.distri = d.id $cad_period $cad_est";
		
		$res = $BD->consultar($sql);
		
		if($BD->numreg($res)>0){
			while(!$res ->EOF){
				$id_asesor = $res->fields['usuario'];
				$id_distri = $res->fields['distri'];
				$db_corto = $res->fields['nombre_corto'];
				$nm_db = $GLOBALS["BD_DIS"].$db_corto.'_'.$id_distri;
				$db = $nm_db.'.usuarios';
				$sq2 = "SELECT CONCAT(nombre,' ',apellido) nom_as FROM $db WHERE id = '$id_asesor' ";
				$r2 = $BD->consultar($sq2);
				$nom_as = $r2->fields['nom_as'];
				if($res->fields['venta_sim'] == null){
					$venta_sim = 0;
				}else{
					$venta_sim = $res->fields['venta_sim'];
				}
				if($res->fields['venta_prod'] == null){
					$venta_prod = 0;
				}else{
					$venta_prod = $res->fields['venta_prod'];
				}
				if($res->fields['valor_efectivo'] == null){
					$valor_efectivo = 0;
				}else{
					$valor_efectivo = $res->fields['valor_efectivo'];
				}

				$array_response[] = array("idpago" => $res->fields['idpago'], "periodo" => $res->fields['periodo'], "idpos" => $res->fields['idpos'], "nom_punto" => $res->fields['nom_punto'], "total_comision" => $res->fields['total_comision'], "pag_tot" => $res->fields['pag_tot'], "venta_sim" => $venta_sim, "venta_prod" => $venta_prod, "valor_efectivo" => $valor_efectivo, "fecha" => $res->fields['fecha'], "hora" => $res->fields['hora'], "asesor" => $nom_as, "saldo_pendiente" => $res->fields['saldo_pendiente']);
				$res ->MoveNext();
			}
			return $array_response;
		}else{
			return "";	
		}
		
   }
   
   
}// Fin clase
?>