$(document).ready(function() {

	charge_Period();

	$("#frmConsulta").submit(function(event) {
	 	event.preventDefault();
	 	Cargar_Reporte();
    });
});

function charge_Period(){
	$.post('modulos/reporte_comisiones/controlador.php',
		{
			accion: 'charge_Period'
		},					
		function(data, textStatus) {
		  if(data != "")
		  {
            data = JSON.parse(data);
            var select= "<option value=''>Seleccionar..</option>";
            $.each(data,function(index, fila) {
              select += "<option value='"+fila.id_per+"'>"+fila.period+"</option>" 
            });
            $("#periodo").html(select).change();
		  }
  
	   }
	);
}

function Cargar_Reporte(){
	 periodo = $("#periodo").val();
	 est_com = $("#est_com").val();
	
   if ( ! $.fn.DataTable.isDataTable( '#t_reporte' ) ) {
	  dtable = $("#t_reporte").DataTable({
			"ajax": {
		    "url": "modulos/reporte_comisiones/controlador.php",
		    "type": "POST",
		    "deferRender": false,
		    "data":{accion:'Cargar_Informe',periodo:periodo, est_com:est_com}
		  },
		  "bFilter": false,
		  "dom": '<"top"fl>rt<"bottom"<"toolbar">p><"clear">',
		  "responsive":true,
		  "columns": [
		  	{ "data": "con"},
            { "data": "periodo"},
			{ "data": "total"},
			{ "data": "pendiente"}
        ],
	    fnDrawCallback: function () {
			
	    }

	  });
	}else{
		dtable.destroy();
		Cargar_Reporte();
	}
	
	$("#CSV").unbind("click");
	$("#CSV").click(function() {
		Datos_CSV();
	});
	$("#consulta").show();
}

function Datos_CSV() {
	
	periodo = $("#periodo").val();
	est_com = $("#est_com").val();
	
	$("#CSV").unbind("click");
	$.post('modulos/reporte_comisiones/controlador.php',
	{
		accion: 'Datos_CSV',
		periodo: periodo,
		est_com: est_com
	},
	function(data, textStatus) {
		if(data != ""){
			data = JSON.parse(data);
			var header = "ID PAGO,PERIODO,IDPOS,PUNTO VENTA,TOTAL COMISION,PAGO TOTAL,SIMCARD,EFECTIVO,PRODUCTO,FECHA,HORA,ASESOR,SALDO PENDIENTE";
			var columnas = "idpago,periodo,idpos,nom_punto,total_comision,pag_tot,venta_sim,valor_efectivo,venta_prod,fecha,hora,asesor,saldo_pendiente";
			ExportarCSV(data, header,"Reporte Comisiones",columnas);
			
			$("#CSV").click(function() {
				Datos_CSV();
			});
		}
	});
	
}



