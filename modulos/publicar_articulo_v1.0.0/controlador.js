var palabras = new Array();
var espacio = /\s/;
var cont = 0;
var imgCont = 0;
var imgCont_edit = 0;
var date = new Date();
var fecha_actual = date.getFullYear() + "-" + ((date.getMonth() + 1) < 10 ? '0' + (date.getMonth() + 1) : (date.getMonth() + 1)) + "-" + date.getDate();
$(document).ready(function () {

    cargar_titulo();
    cargar_categorias();
    cargar_sub_categorias();

    $(".content-header").find("h1").html("Mis productos");
    $("#fecha_ini").datepicker({
        format: "yyyy-mm-dd",
        todayHighlight: true,
        todayBtn: true
    });
    $("#fecha_fin").datepicker({
        format: "yyyy-mm-dd",
        todayHighlight: true,
        todayBtn: true
    });

    $("#btn_buscar").click(function (e) {
        e.preventDefault();
        cargar_tabla();
    });

    $("#btn_crear").click(function (e) {
        $("#form_guardar")[0].reset();
        $("#palabra div").remove();
        palabras = new Array();
        $("#modal").modal("show");
        $("#f_ini_prod").datepicker({
            format: "yyyy-mm-dd",
            todayHighlight: true,
            todayBtn: true
        });
        $("#f_fin_prod").datepicker({
            format: "yyyy-mm-dd",
            todayHighlight: true,
            todayBtn: true
        });
        cargar_categorias();
        cargar_vendedor();
        getLocation();

        $("#categoria").on("change", function () {
            cargar_sub_categorias();
        });

        $("#estado_prod").change(function () {
            var select_estado = $(this).children("option:selected").val();
            previsualizacion(select_estado);
        });
        $(".product-title").html("");
        $(".product-price").html("");
        $("#marca-prod").html("");
        $(".description-content").html("");
        $("#estado-prod").html("");
    });

    $(".cerrar_guardar").click(function () {
        $(".modal").modal("hide");
    });
});


function cargar_titulo() {

    $.post('modulos/publicar_articulo_v1.0.0/controlador.php',
        {
            accion: 'cargar_titulo'
        },
        function (data) {
            data = JSON.parse(data);
            var html = '<option value = "">SELECCIONAR</option>';
            $.each(data, function (index, val) {
                html += '<option value = "' + val.id + '">' + val.titulo + '</option>';
            });
            $("#nombre").html(html).change();
        });
}

function cargar_categorias() {

    $.post('modulos/publicar_articulo_v1.0.0/controlador.php',
        {
            accion: 'cargar_categorias'
        },
        function (data) {
            data = JSON.parse(data);
            var html = '<option value = "">SELECCIONAR</option>';
            $.each(data, function (index, val) {
                html += '<option value = "' + val.id + '">' + val.nombre + '</option>';
            });
            $("#categoria").html(html).change();
            $("#categoria_edit").html(html).change();
        });
}

function cargar_sub_categorias() {

    var id_categoria = $("#categoria").val();

    $.post('modulos/publicar_articulo_v1.0.0/controlador.php',
        {
            accion: 'cargar_sub_categorias',
            id_categoria: id_categoria
        },
        function (data) {
            data = JSON.parse(data);
            var html = '<option value = "">SELECCIONAR</option>';
            $.each(data, function (index, val) {
                html += '<option value = "' + val.id + '">' + val.nombre + '</option>';
            });
            $("#sub_categoria").html(html).change();
        });
}

function cargar_sub_categorias_edit(tipo, selec) {

    $.post('modulos/publicar_articulo_v1.0.0/controlador.php',
        {
            accion: 'cargar_sub_categorias',
            id_categoria: tipo
        },
        function (data) {
            data = JSON.parse(data);
            var html = '<option value = "">SELECCIONAR</option>';
            $.each(data, function (index, val) {
                html += '<option value = "' + val.id + '">' + val.nombre + '</option>';
            });

            $("#sub_categoria_edit").html(html);
            $("#sub_categoria_edit").val(selec).change();
        });
}

function cargar_vendedor() {

    $.post('modulos/publicar_articulo_v1.0.0/controlador.php',
        {
            accion: 'cargar_vendedor'
        },
        function (data) {
            data = JSON.parse(data);

            $("#nombre-pdv").html(" " + data[0]["nombre_pdv"]);
            $("#nombre-vendedor").html(" " + data[0]["nombre"]);
            $("#celular-vendedor").html(" " + data[0]["celular"]);
        });
}
function cargar_vendedor_edit(id) {

    $.post('modulos/publicar_articulo_v1.0.0/controlador.php',
        {
            accion: 'cargar_vendedor_edit',
            id: id
        },
        function (data) {
            datos = JSON.parse(data);
            var data = datos['datos'];
            $("#nombre-pdv-edit").html(" " + data[0]["nombre_pdv"]);
            $("#nombre-vendedor-edit").html(" " + data[0]["nombre"]);
        });
}

function cargar_tabla() {

    var fecha_ini = $("#fecha_ini").val();
    var fecha_fin = $("#fecha_fin").val();
    var nombre = $("#nombre").val();
    var estado = $("#estado").val();      

    if ($.fn.DataTable.isDataTable("#tbl_producto")) {
        table.clear();
        table.destroy();
    }
    table = $("#tbl_producto").DataTable({
        "responsive": true,
        "paging": true,
        "info": true,
        "searching": false,
        "filter": true,
        "ajax": {
            "type": "POST",
            "url": "modulos/publicar_articulo_v1.0.0/controlador.php",
            "data": {
                accion: 'cargar_tabla',
                fecha_ini: fecha_ini,
                fecha_fin: fecha_fin,
                nombre: nombre,
                estado: estado
            },
        },
        "columns": [
            { data: 'titulo' },
            { data: 'fecha_inicio' },
            { data: 'fecha_final' }
        ],
        "columnDefs": [
            {
                "targets": [3],
                "data": { estado: 'estado', id_producto: 'id_producto' },
                "render": function (data, type, row) {         
                    if (data.fecha_inicio == fecha_actual && data.estado == "0") {
                        validar_fecha_estados(data.id_producto, 1);
                    }
                    if (data.fecha_final <= fecha_actual && data.estado == "1") {
                        validar_fecha_estados(data.id_producto, 0);
                    }
                    if (data.estado == "0") {
                        return "<div style='background: #FF7285;border-radius: 13px; padding: 3px;text-align: center;width: 90px;cursor:pointer;' onclick='cambiar_estado(" + data.id_producto + ", " + data.estado + ", this);'>Inactivado</div>";
                    } else {
                        return "<div style='background: #69E4A6;border-radius: 13px; padding: 3px;text-align: center;width: 90px;cursor:pointer;' onclick='cambiar_estado(" + data.id_producto + ", " + data.estado + ", this);'>Activado</div>";
                    }
                }
            },
            {
                "targets": [4],
                "data": "nombre_imagen",
                "render": function (data, type, row) {
                    if (data != "0") {
                        return '<img src="static/img/referencias/publicaciones/' + data + '"  id="tipo_acc" class="img-thumbnail" width="107" height="89"  style="width:107px; height:89px;"  style="cursor: pointer;"/>';
                    } else {
                        return '<img src="static/img/not_img.png"  id="tipo_acc_not" class="img-thumbnail" width="107" height="89"  style="width:107px; height:89px;"  style="cursor: pointer;"/>';
                    }
                }
            },
            {
                "targets": [5],
                "data": "id_producto",
                "className": "td_defecto",
                "render": function (data, type, row) {
                    return "<button onclick='obtener_datos_edit(" + row.id_producto + ")'  class='btn btn-sm btn-primary edit' title='Editar'><i class='fa fa-edit'></i></button>";
                }
            }]
    });
    $("#consulta").show();
}

function validar_fecha_estados(id, estado) {

    $.post("modulos/publicar_articulo_v1.0.0/controlador.php", {
        accion: "cambiar_estado_f_ini",
        id: id,
        estado: estado
    }, function (data, textStatus) {
        if (data == 1) {
            Notificacion("Error en la actualización de estados", "error");
        } else {
            //Notificacion("Esta estado se ha modificado", "success");                    
            //cargar_tabla();                  
        }
    });
}

function previsualizacion(estado) {

    var titulo = $("#titulo").val();
    var precio = $("#precio").val();
    var marca = $("#marca").val();
    var descripcion = $("#descripcion").val();

    if (estado == 1) {
        $("#estado-prod").html("Nuevo");
    } else if (estado == 2) {
        $("#estado-prod").html("Usado - Como nuevo");
    } else if (estado == 3) {
        $("#estado-prod").html("Usado - Buen estado");
    } else if (estado == 4) {
        $("#estado-prod").html("Usado - Aceptable");
    } else {
        $("#estado-prod").html("");
    }

    $(".product-title").html(titulo);
    $(".product-price").html("$ " + precio);
    $("#marca-prod").html(marca);
    $(".description-content").html(descripcion);
}

function obtener_datos_edit(id) {

    $.post('modulos/publicar_articulo_v1.0.0/controlador.php',
        {
            accion: 'obtener_datos_edit',
            id: id
        },
        function (data) {
            data = JSON.parse(data);

            palabras = [];
            var add = "";
            if (data[0]["palabras"] != "") {
                var pal = data[0]["palabras"].split(",");
                if (pal.length > 0) {
                    for (var i in pal) {
                        if (pal[i] != "") {
                            palabras.push(pal[i]);
                            add += "<div id = 'palabra_" + i + "'><div class='form-group col-md-2 palabras_conte_edit' style = 'padding: 5px;width: auto;'>" + pal[i] + " </div> <div class='form-group col-md-1 glyphicon glyphicon-remove-sign' Onclick = 'eliPalabra(" + i + ")' style='color:#dd4b39;cursor:pointer;position: relative;top: 5px;width: auto;'></div></div>";
                        }
                    }
                }
            }

            $("#modal-edit").modal("show");
            $("#f_ini_prod_edit").datepicker({
                format: "yyyy-mm-dd",
                todayHighlight: true,
                todayBtn: true
            });
            $("#f_fin_prod_edit").datepicker({
                format: "yyyy-mm-dd",
                todayHighlight: true,
                todayBtn: true
            });
            $("#palabra_edit").html(add);
            $("#ver_palabras_edit").show();
            $("#id_update").val(data[0]["id"]);
            $("#titulo_edit").val(data[0]["titulo"]);
            $("#precio_edit").val(formato_numero(data[0]["precio"], 0, ',', '.'));
            $("#categoria_edit").val(data[0]["id_categoria"]).change();
            cargar_sub_categorias_edit(data[0]["id_categoria"], data[0]["id_sub_categoria"]);
            $(".product-time").html("Publicado hace " + data[0]["fecha_pub"] + " días");
            $("#marca_edit").val(data[0]["marca"]);
            $("#estado_prod_edit").val(data[0]["estado_producto"]).change();
            $("#f_ini_prod_edit").val(data[0]["fecha_inicio"]);
            //$("#f_fin_prod_edit").val(data[0]["fecha_final"]);
            $("#descripcion_edit").val(data[0]["descripcion"]);
            $("#ubicacion_edit").val(data[0]["ubicacion"]);
            $("#disponibilidad_edit").val(data[0]["disponibilidad"]).change();
            previsualizacion_edit(0, data[0]["estado_prod"]);
            cargar_vendedor_edit(data[0]["id"]);
            setLocation_edit(data[0]["latitud"], data[0]["longitud"]);
            cargar_imagenes(data[0]["id"]);
            $("#estado_prod_edit").change(function () {
                var select_estado_edit = $(this).children("option:selected").val();
                previsualizacion_edit(select_estado_edit, 0);
            });
        });
}

$("#addPalabraEdit").click(function () {
    event.preventDefault();
    add_palabra_edit();
});


function add_palabra_edit() {

    var palabra_clave_edit = $("#palabra_clave_edit").val();
    var inyecciones = [";", "'", "ALTER", "DROP", "SELECT", "FROM", "WHERE", "INSERT", "DELETE", "*", " OR ", " AND ", "%27", "TABLE", "(", ")", "?", "=", "&", ".", ",", "|"];
    var band_clave = false;

    cadena = palabra_clave_edit.toUpperCase();
    for (var z = 0; z < inyecciones.length; z++) {
        if (cadena.indexOf(inyecciones[z]) != -1) {
            band_clave = true;
        }
    }
    var cont_w = 0;
    if (espacio.test(palabra_clave_edit)) {
        band_clave = true;
        Notificacion("Las palabras claves no permiten espacios", "warning");
    }

    for (i in palabras) {
        cont_w++;
    }
    if (cont_w < 5) {
        if (!band_clave) {
            if (palabra_clave_edit != "") {
                if (palabras.indexOf(palabra_clave_edit) == -1) {
                    palabras.push(palabra_clave_edit);

                    var add = "";
                    for (var i in palabras) {
                        if (palabras[i] != "") {
                            add += "<div id = 'palabra_edit_" + i + "'><div class='form-group col-md-2 palabras_conte_edit' style = 'padding: 5px;width: auto;'>" + palabras[i] + " </div> <div class='form-group col-md-1 glyphicon glyphicon-remove-sign' Onclick = 'eliPalabra_edit(" + i + ")' style='color:#dd4b39;cursor:pointer;position: relative;top: 5px;width: auto;'></div></div>";
                        }
                    }
                    $("#palabra_edit").html(add);
                    $("#ver_palabras_edit").show();
                }
            }
        }
    } else {
        Notificacion("Solo se permiten maximo 5 palabras clave", "warning");
    }

    $("#palabra_clave_edit").val("");
    $("#palabra_clave_edit").focus();
}
function eliPalabra_edit(i) {
    $("#palabra_edit_" + i).hide();
    $("#palabra_edit_" + i).remove();

    delete palabras[i];
}

function previsualizacion_edit(estado_edit, select) {

    var titulo_edit = $("#titulo_edit").val();
    var precio_edit = $("#precio_edit").val();
    var marca_edit = $("#marca_edit").val();
    var descripcion_edit = $("#descripcion_edit").val();
    if (select == 0) {
        if (estado_edit == 1) {
            $("#estado-prod-edit").html("Nuevo");
        } else if (estado_edit == 2) {
            $("#estado-prod-edit").html("Usado - Como nuevo");
        } else if (estado_edit == 3) {
            $("#estado-prod-edit").html("Usado - Buen estado");
        } else if (estado_edit == 4) {
            $("#estado-prod-edit").html("Usado - Aceptable");
        } else {
            $("#estado-prod-edit").html("");
        }
    } else {
        $("#estado-prod-edit").html(select);
    }

    $(".product-title-edit").html(titulo_edit);
    $(".product-price-edit").html("$ " + precio_edit);
    $("#marca-prod-edit").html(marca_edit);
    $(".description-content-edit").html(descripcion_edit);
}

function cargar_imagenes(id) {

    $(".gallery-body-edit").remove();
    $(".gallerySlide-edit").remove();
    imgCont_edit = 0;
    $.post('modulos/publicar_articulo_v1.0.0/controlador.php',
        {
            accion: 'cargar_imagenes',
            id: id
        },
        function (data) {
            data = JSON.parse(data);
            $.each(data, function (index, row) {
                imgCont_edit++;
                var imgSmall_edit = $('<div class="gallery-body-edit">  <img src="static/img/referencias/publicaciones/' + row.nombre + '" alt="Foto del usuario" class="selectImg-edit selectImg-opacityx selectImg-hover-opacity-off" onclick="currentDiv_edit(' + imgCont_edit + ')" style="width:100%;height:100%;border: 2px solid #000;cursor:pointer;border-radius: 10px;"> <figcaption style="text-align:center;" onclick="eliminar_img($(this),' + row.id + ')"> <i class="fas fa-trash-alt" style="cursor:pointer;" title="Eliminar imagen"></i> </figcaption>  </div>');
                if (row.principal == 1) {
                    var imgLarge_edit = $('<img src="static/img/referencias/publicaciones/' + row.nombre + '" alt="Foto del usuario" class="image-preview-edit gallerySlide-edit" style="width:100%; display:block">');
                } else {
                    var imgLarge_edit = $('<img src="static/img/referencias/publicaciones/' + row.nombre + '" alt="Foto del usuario" class="image-preview-edit gallerySlide-edit" style="width:100%; display:none">');
                }

                $(imgSmall_edit).insertBefore("#add-photo-container-edit");
                $(imgLarge_edit).insertBefore("#imgLarge-edit");
                $("#not_img_edit").hide();
                $("#cargadas-edit").html(imgCont_edit + " de 4");
            });
        });
}


$(document).on("click", "#add-photo-edit", function () {
    $("#add-new-photo-edit").click();
});

$(document).on("change", "#add-new-photo-edit", function () {

    var files = this.files;
    var element;
    var supportedImages = ["image/jpeg", "image/png", "image/gif"];
    var seEncontraronElementoNoValidos = false;

    for (var i = 0; i < files.length; i++) {
        element = files[i];
        if (supportedImages.indexOf(element.type) != -1) {
            createPreview_edit(element);
        }
        else {
            seEncontraronElementoNoValidos = true;
        }
    }
    if (seEncontraronElementoNoValidos) {
        Notificacion("Se encontraron archivos no validos.", "warning");
    }
});

function createPreview_edit(file) {
    imgCont_edit++;

    if (imgCont_edit > 4) {
        imgCont_edit = 4;
        Notificacion("Solo se permite que ingreses 4 Imagenes", "warning");
        return false;
    }
    var imgCodified = URL.createObjectURL(file);
    var imgSmall = $('<div class="gallery-body-edit">  <img src="' + imgCodified + '" alt="Foto del usuario" class="selectImg-edit selectImg-opacity selectImg-hover-opacity-off" onclick="currentDiv_edit(' + imgCont_edit + ')" style="width:100%;height:100%;border: 2px solid #000;cursor:pointer;border-radius: 10px;"> <figcaption style="text-align:center;" id="eliminar"> <i class="fas fa-trash-alt" style="cursor:pointer;" title="Eliminar imagen"></i> </figcaption>  </div>');

    var imgLarge = $('<img src="' + imgCodified + '" alt="Foto del usuario" class="image-preview gallerySlide-edit" style="width:100%; display:none">');

    $(imgSmall).insertBefore("#add-photo-container-edit");
    $(imgLarge).insertBefore("#imgLarge-edit");
    $("#cargadas-edit").html(imgCont_edit + " de 4");
    Notificacion("Todos los archivos se subieron correctamente.", "success");
}

function currentDiv_edit(n) {
    showDivs_edit(slideIndex = n);
}

function showDivs_edit(n) {
    var i;
    var x = document.getElementsByClassName("gallerySlide-edit");
    var dots = document.getElementsByClassName("selectImg-edit");
    if (n > x.length) { slideIndex = 1 }
    if (n < 1) { slideIndex = x.length }
    for (i = 0; i < x.length; i++) {
        x[i].style.display = "none";
    }
    for (i = 0; i < dots.length; i++) {
        dots[i].className = dots[i].className.replace(" selectImg-opacity-off", "");
    }
    x[slideIndex - 1].style.display = "block";
    dots[slideIndex - 1].className += " selectImg-opacity-off";
}

function eliminar_img(elemento, id) {

    BootstrapDialog.confirm("¿Esta seguro que desea eliminar la imagen?", function (result) {
        if (result) {

            $.post('modulos/publicar_articulo_v1.0.0/controlador.php',
                {
                    accion: 'eliminar_img',
                    id: id
                }, function (data, textStatus) {
                    if (data == 1) {
                        Notificacion("La imagen no se ha podido eliminar", "error");
                    } else {
                        Notificacion("La imagen se elimino correctamente", "success");
                        elemento.parent().remove();
                        imgCont_edit = imgCont_edit - 1;
                        $("#cargadas-edit").html(imgCont_edit + " de 4");
                    }
                });
        }
    });
}

function cambiar_estado(id, estado, elemento) {

    BootstrapDialog.confirm("¿Esta seguro de cambiar el estado?", function (result) {
        if (result) {
            estado = (estado == "1") ? "0" : "1";

            $.post("modulos/publicar_articulo_v1.0.0/controlador.php", {
                accion: "cambiar_estado",
                id: id,
                estado: estado
            }, function (data, textStatus) {
                if (data == 1) {
                    Notificacion("El estado no se pudo modificar", "error");
                } else {
                    Notificacion("Esta estado se ha modificado", "success");

                    $(elemento).attr("onclick", "cambiar_estado(" + id + ", " + estado + ", this);");
                    cargar_tabla();
                    if (estado == "1") {
                        $(elemento).attr("css", "style='background: #69E4A6;border-radius: 13px; padding: 3px;text-align: center;width: 100px;cursor:pointer;")
                    } else {
                        $(elemento).attr("css", "style='background: #FF7285;border-radius: 13px; padding: 3px;text-align: center;width: 100px;cursor:pointer;")
                    }
                }
            });
        }
    });
}

$("#addPalabra").click(function () {
    event.preventDefault();
    add_palabra();
});


function add_palabra() {

    var palabra_clave = $("#palabra_clave").val();
    var inyecciones = [";", "'", "ALTER", "DROP", "SELECT", "FROM", "WHERE", "INSERT", "DELETE", "*", " OR ", " AND ", "%27", "TABLE", "(", ")", "?", "=", "&", ".", ",", "|"];
    var band_clave = false;

    cadena = palabra_clave.toUpperCase();
    for (var z = 0; z < inyecciones.length; z++) {
        if (cadena.indexOf(inyecciones[z]) != -1) {
            band_clave = true;
        }
    }
    var cont_w = 0;
    if (espacio.test(palabra_clave)) {
        band_clave = true;
        Notificacion("Las palabras claves no permiten espacios", "warning");
    }

    for (i in palabras) {
        cont_w++;
    }
    if (cont_w < 5) {
        if (!band_clave) {
            if (palabra_clave != "") {
                if (palabras.indexOf(palabra_clave) == -1) {
                    palabras.push(palabra_clave);

                    var add = "";
                    for (var i in palabras) {
                        if (palabras[i] != "") {
                            add += "<div id = 'palabra_" + i + "'><div class='form-group col-md-2 palabras_conte' style = 'padding: 5px;width: auto;'>" + palabras[i] + " </div> <div class='form-group col-md-1 glyphicon glyphicon-remove-sign' Onclick = 'eliPalabra(" + i + ")' style='color:#dd4b39;cursor:pointer;position: relative;top: 5px;width: auto;'></div></div>";
                        }
                    }
                    $("#palabra").html(add);
                    $("#ver_palabras").show();
                }
            }
        }
    } else {
        Notificacion("Solo se permiten maximo 5 palabras clave", "warning");
    }

    $("#palabra_clave").val("");
    $("#palabra_clave").focus();
}
function eliPalabra(i) {
    $("#palabra_" + i).hide();
    $("#palabra_" + i).remove();

    delete palabras[i];
}

function guardar_datos() {

    var archivo = $('#add-new-photo')[0].files;
    var imagen = $('#add-new-photo').val();
    var titulo = $("#titulo").val();
    var precio = $("#precio").val().replace(/\D/g, '');
    var categoria = $("#categoria").val();
    var sub_categoria = $("#sub_categoria").val();
    var marca = $("#marca").val();
    var estado_prod = $("#estado_prod").val();
    var f_ini_prod = $("#f_ini_prod").val();
    var f_fin_prod = $("#f_fin_prod").val();
    var descripcion = $("#descripcion").val();
    var ubicacion = $("#ubicacion").val();
    var disponibilidad = $("#disponibilidad").val();
    var latitud = $("#lat").val();
    var longitud = $("#lon").val();
    var palabra_clave = new Array();
    var accion = "guardar_registros";
    var data = new FormData();

    $(".palabras_conte").each(function (i) {
        palabra_clave.push($(this).html());
    });

    if(imagen == ""){Notificacion("Debes agregar minimo 1 imagen", "warning");return false;}
    if(titulo == ""){Notificacion("El titulo es obligatorio", "warning");$("#titulo").focus();return false;}
    if(precio == ""){Notificacion("El precio es obligatorio", "warning");$("#precio").focus();return false;}
    if(categoria == ""){Notificacion("La categoria es obligatoria", "warning");return false;}
    if(sub_categoria == ""){Notificacion("La subcategoria es obligatoria", "warning");return false;}
    if(marca == ""){Notificacion("La marca es obligatoria", "warning");$("#marca").focus();return false;}
    if(f_ini_prod == ""){Notificacion("La fecha de inicio es obligatoria", "warning");$("#f_ini_prod").focus();return false;}
    if(ubicacion == ""){Notificacion("La ubicación es obligatoria", "warning");$("#ubicacion").focus();return false;}
    if(estado_prod == ""){Notificacion("El estado es obligatoria", "warning");$("#estado_prod").focus();return false;}
    if(disponibilidad == ""){Notificacion("La disponibilidad es obligatoria", "warning");$("#disponibilidad").focus();return false;}
    if(palabra_clave == ""){Notificacion("Debes agregar minimo 1 palabra clave", "warning");$("#palabra_clave").focus();return false;}
            
    if(f_ini_prod < fecha_actual){
        Notificacion("La fecha de inicio no puede ser menor a la fecha actual", "warning");
        $("#f_ini_prod").focus();      
        return false;
    }if(f_ini_prod < f_fin_prod){
        Notificacion("La fecha final no puede ser menor a la fecha inicial", "warning");
        $("#f_ini_prod").focus();      
        return false;
    }if(f_ini_prod > fecha_actual){
        Notificacion("La publicación se pondra en estado ACTIVO en la fecha registrada", "info");                
    }

    for (i = 0; i < archivo.length; i++) {
        data.append('archivo' + i, archivo[i]);
    }
    data.append('titulo', titulo);
    data.append('precio', precio);
    data.append('categoria', categoria);
    data.append('sub_categoria', sub_categoria);
    data.append('marca', marca);
    data.append('estado_prod', estado_prod);
    data.append('f_ini_prod', f_ini_prod);
    data.append('f_fin_prod', f_fin_prod);
    data.append('descripcion', descripcion);
    data.append('ubicacion', ubicacion);
    data.append('disponibilidad', disponibilidad);
    data.append('latitud', latitud);
    data.append('longitud', longitud);
    data.append('palabra_clave', palabra_clave);
    data.append('accion', accion);

    $.ajax({
        url: "modulos/publicar_articulo_v1.0.0/controlador.php",
        type: 'POST',
        data: data,
        processData: false,
        contentType: false,
        cache: false,
        beforeSend: function () {
            $("#btn_guardar").prop("disabled", true);
        },
        statusCode: {
            200: function (resp) {
                if (resp == "0") {
                    $("#btn_guardar").prop("disabled", false);
                    Notificacion("Se ha creado el producto correctamente", "success");
                    $("#add-new-photo").val("");
                    cargar_titulo();
                    cargar_tabla();
                    $("#modal").modal("hide");
                } else {
                    $("#btn_guardar").prop("disabled", false);
                    Notificacion("Error al guardar el registro", "danger");
                    $("#add-new-photo").val("");
                    return false;
                }
            },
            500: function (resp) {
                $("#btn_guardar").prop("disabled", false);
                Notificacion("Error al guardar el registro", "danger");
                $("#add-new-photo").val("");
                return false;
            }
        }
    });
}

function editar_datos() {

    var archivo_edit = $('#add-new-photo-edit')[0].files;
    var id_update = $("#id_update").val();
    var titulo = $("#titulo_edit").val();
    var precio = $("#precio_edit").val().replace(/\D/g, '');
    var categoria = $("#categoria_edit").val();
    var sub_categoria = $("#sub_categoria_edit").val();
    var marca = $("#marca_edit").val();
    var estado_prod = $("#estado_prod_edit").val();
    var f_ini_prod = $("#f_ini_prod_edit").val();
    var f_fin_prod = $("#f_fin_prod_edit").val();
    var descripcion = $("#descripcion_edit").val();
    var ubicacion = $("#ubicacion_edit").val();
    var disponibilidad = $("#disponibilidad_edit").val();
    var latitud = $("#lat_edit").val();
    var longitud = $("#lon_edit").val();
    var palabra_clave_edit = new Array();
    var accion = "editar_registros";
    var data = new FormData();

    $(".palabras_conte_edit").each(function (i) {
        palabra_clave_edit.push($(this).html());
    });

    if(titulo == ""){Notificacion("El titulo es obligatorio", "warning");$("#titulo_edit").focus();return false;}
    if(precio == ""){Notificacion("El precio es obligatorio", "warning");$("#precio_edit").focus();return false;}
    if(categoria == ""){Notificacion("La categoria es obligatoria", "warning");return false;}
    if(sub_categoria == ""){Notificacion("La subcategoria es obligatoria", "warning");return false;}
    if(marca == ""){Notificacion("La marca es obligatoria", "warning");$("#marca_edit").focus();return false;}
    if(f_ini_prod == ""){Notificacion("La fecha de inicio es obligatoria", "warning");$("#f_ini_prod_edit").focus();return false;}
    if(ubicacion == ""){Notificacion("La ubicación es obligatoria", "warning");$("#ubicacion_edit").focus();return false;}
    if(estado_prod == ""){Notificacion("El estado es obligatoria", "warning");$("#estado_prod_edit").focus();return false;}
    if(disponibilidad == ""){Notificacion("La disponibilidad es obligatoria", "warning");$("#disponibilidad_edit").focus();return false;}
    if(palabra_clave_edit == ""){Notificacion("Debes agregar minimo 1 palabra clave", "warning");$("#palabra_clave_edit").focus();return false;}
            
    if(f_ini_prod < fecha_actual){
        Notificacion("La fecha de inicio no puede ser menor a la fecha actual", "warning");
        $("#f_ini_prod_edit").focus();      
        return false;
    }if(f_ini_prod < f_fin_prod){
        Notificacion("La fecha final no puede ser menor a la fecha inicial", "warning");
        $("#f_ini_prod").focus();      
        return false;
    }if(f_ini_prod > fecha_actual){
        Notificacion("La publicación se pondra en estado ACTIVO en la fecha registrada", "info");                
    }    

    for (i = 0; i < archivo_edit.length; i++) {
        data.append('archivo_edit' + i, archivo_edit[i]);
    }
    data.append('id_update', id_update);
    data.append('titulo', titulo);
    data.append('precio', precio);
    data.append('categoria', categoria);
    data.append('sub_categoria', sub_categoria);
    data.append('marca', marca);
    data.append('estado_prod', estado_prod);
    data.append('f_ini_prod', f_ini_prod);
    data.append('f_fin_prod', f_fin_prod);
    data.append('descripcion', descripcion);
    data.append('ubicacion', ubicacion);
    data.append('disponibilidad', disponibilidad);
    data.append('latitud', latitud);
    data.append('longitud', longitud);
    data.append('palabra_clave_edit', palabra_clave_edit);
    data.append('accion', accion);

    $.ajax({
        url: "modulos/publicar_articulo_v1.0.0/controlador.php",
        type: 'POST',
        data: data,
        processData: false,
        contentType: false,
        cache: false,
        beforeSend: function () {
            $("#btn_guardar").prop("disabled", true);
        },
        statusCode: {
            200: function (resp) {
                if (resp == "0") {
                    $("#btn_editar").prop("disabled", false);
                    Notificacion("Se ha editado el producto correctamente", "success");
                    $("#add-new-photo-edit").val("");
                    cargar_titulo();
                    cargar_tabla();
                    $("#modal-edit").modal("hide");
                } else {
                    $("#btn_editar").prop("disabled", false);
                    Notificacion("Error al editar el registro", "danger");
                    $("#add-new-photo-edit").val("");
                    return false;
                }
            },
            500: function (resp) {
                $("#btn_editar").prop("disabled", false);
                Notificacion("Error al editar el registro", "danger");
                $("#add-new-photo-edit").val("");
                return false;
            }
        }
    });
}

//IMAGENES

$(document).on("click", "#add-photo", function () {
    $("#add-new-photo").click();
});

$(document).on("change", "#add-new-photo", function () {

    var files = this.files;
    var element;
    var supportedImages = ["image/jpeg", "image/png", "image/gif"];
    var seEncontraronElementoNoValidos = false;

    for (var i = 0; i < files.length; i++) {
        element = files[i];
        cont++;
        if (cont > 4) {
            cont = 4;
            Notificacion("Solo se permite que ingreses 4 Imagenes", "warning");
            return false;
        }
        $("#cargadas").html(cont + " de 4");
        if (supportedImages.indexOf(element.type) != -1) {
            createPreview(element);
        }
        else {
            seEncontraronElementoNoValidos = true;
        }
    }
    if (seEncontraronElementoNoValidos) {
        Notificacion("Se encontraron archivos no validos.", "warning");
    }
    else {
        Notificacion("Todos los archivos se subieron correctamente.", "success");
    }

});

$(document).on("click", "#eliminar ", function (e) {
    $(this).parent().remove();
    cont = cont - 1;
    $("#cargadas").html(cont + " de 4");
    if (cont == 0) {
        $(".content-gallery").html("<img src='static/img/not_available.png' class='image-preview gallerySlide' style='width:100%; display:block'>");
    }
});


function createPreview(file) {
    imgCont++;
    var imgCodified = URL.createObjectURL(file);
    var imgSmall = $('<div class="gallery-body">  <img src="' + imgCodified + '" alt="Foto del usuario" class="selectImg selectImg-opacity selectImg-hover-opacity-off" name="prueba" onclick="currentDiv(' + imgCont + ')" style="width:100%;height:100%;border: 2px solid #000;cursor:pointer;border-radius: 10px;"> <figcaption style="text-align:center;" id="eliminar"> <i class="fas fa-trash-alt" style="cursor:pointer;" title="Eliminar imagen"></i> </figcaption>  </div>');
    if (imgCont === 1) {
        var imgLarge = $('<img src="' + imgCodified + '" alt="Foto del usuario" class="image-preview gallerySlide" style="width:100%; display:block">');
        $("#not_img").hide();
    } else {
        var imgLarge = $('<img src="' + imgCodified + '" alt="Foto del usuario" class="image-preview gallerySlide" style="width:100%; display:none">');
        $("#not_img").hide();
    }

    $(imgSmall).insertBefore("#add-photo-container2");
    $(imgLarge).insertBefore("#imgLarge");
    $("#not_img").hide();
}

function currentDiv(n) {
    showDivs(slideIndex = n);
}

function showDivs(n) {
    var i;
    var x = document.getElementsByClassName("gallerySlide");
    var dots = document.getElementsByClassName("selectImg");
    if (n > x.length) { slideIndex = 1 }
    if (n < 1) { slideIndex = x.length }
    for (i = 0; i < x.length; i++) {
        x[i].style.display = "none";
    }
    for (i = 0; i < dots.length; i++) {
        dots[i].className = dots[i].className.replace(" selectImg-opacity-off", "");
    }
    x[slideIndex - 1].style.display = "block";
    dots[slideIndex - 1].className += " selectImg-opacity-off";
}


function formato(input) {

    var num = input.value.replace(/\./g, '');
    if (!isNaN(num)) {
        num = num.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g, '$1.');
        num = num.split('').reverse().join('').replace(/^[\.]/, '');
        input.value = num;
    } else {
        input.value = input.value.replace(/[^\d\.]*/g, '');
    }
}

function formato_numero(numero, decimales, separador_decimal, separador_miles) {

    numero = parseFloat(numero);
    if (isNaN(numero)) {
        return '';
    }
    if (decimales !== undefined) {
        numero = numero.toFixed(decimales);
    }
    numero = numero.toString().replace('.', separador_decimal !== undefined ? separador_decimal : ',');
    if (separador_miles) {
        var miles = new RegExp('(-?[0-9]+)([0-9]{3})');
        while (miles.test(numero)) {
            numero = numero.replace(miles, '$1' + separador_miles + '$2');
        }
    }
    return numero;
}

// VALIDACIONES MAPS AGREGAR//

$("#addUbicacion").click(function () {
    event.preventDefault();
    getLocation();
    $("#addUbicacion").prop("disabled", true);
});

function btn_ubicacion() {
    $("#addUbicacion").prop("disabled", false);
}

function getLocation() {
    if ("geolocation" in navigator) {
        navigator.geolocation.getCurrentPosition(showPosition);
    } else {
        Notificacion("La geolocalización no es compatible con este navegador", "warning");
    }
}

const resultsWrapperHTML = document.getElementById("search-result");
const Map = L.map("render-map");
var Marker = "";
let typingInterval;
function showPosition(position) {
    var latitud_ini = position.coords.latitude;
    var longitud_ini = position.coords.longitude;

    var latitude = "latitude=" + position.coords.latitude;
    var longitude = "&longitude=" + position.coords.longitude;
    var query = latitude + longitude + "&localityLanguage=en";
    const Http = new XMLHttpRequest();
    var bigdatacloud_api = "https://api.bigdatacloud.net/data/reverse-geocode-client?";

    bigdatacloud_api += query;

    Http.open("GET", bigdatacloud_api);
    Http.send();

    Http.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            var myObj = JSON.parse(this.responseText);
            var ciudad = myObj.locality.toUpperCase();
            $("#ubicacion").val(ciudad);
        }
    }
    if (latitud_ini == "0" || longitud_ini == "0") {
        latitud_ini = 6.2443382;
        longitud_ini = -75.573553;
    }
    const DEFAULT_COORD = [latitud_ini, longitud_ini];
    const osmTileUrl = "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png";

    //const attrib = 'Leaflet with <a href="https://academy.byidmore.com">Id More Academy<a>'

    //const osmTile = new L.TileLayer(osmTileUrl, { minZoom: 2, maxZoom: 20, attribution: attrib })
    const osmTile = new L.TileLayer(osmTileUrl, { minZoom: 2, maxZoom: 20 });

    Map.setView(new L.LatLng(DEFAULT_COORD[0], DEFAULT_COORD[1]), 12);
    Map.addLayer(osmTile);

    Marker = L.marker(DEFAULT_COORD).addTo(Map);
    //.bindPopup('Tu ubicación.')
    //.openPopup();
    setLocation(latitud_ini, longitud_ini);

    /*Map.on("click", function (e) {
        const { lat, lng } = e.latlng
        Marker.setLatLng([lat, lng])
    })*/
}
function onTyping(e) {
    clearInterval(typingInterval)
    const { value } = e

    typingInterval = setInterval(() => {
        clearInterval(typingInterval);
        searchLocation(value);
    }, 500)
}

function searchLocation(keyword) {
    if (keyword) {
        // request to nominatim api
        fetch(`https://nominatim.openstreetmap.org/search?q=${keyword}&format=json`)
            .then((response) => {
                return response.json()
            }).then(json => {
                if (json.length > 0) return renderResults(json)
                else Notificacion("No se encontro ningun resultado", "danger");
            })
    }
}

function renderResults(result) {
    let resultsHTML = "";
    result.map((n) => {
        resultsHTML += "<li><a href='#' onclick='setLocation(" + n.lat + "," + n.lon + ");'>" + n.display_name + "</a></li>";
    })
    resultsWrapperHTML.innerHTML = resultsHTML;
}

function setLocation(lat, lon) {
    $("#lat").val(lat);
    $("#lon").val(lon);

    Map.setView(new L.LatLng(lat, lon), 12);

    Marker.setLatLng([lat, lon]);

    clearResults();
}

function clearResults() {
    resultsWrapperHTML.innerHTML = "";
}

// VALIDACIONES MAPS EDITAR//

$("#addUbicacion_edit").click(function () {
    event.preventDefault();
    getLocation_edit();
    $("#addUbicacion_edit").prop("disabled", true);
});

function btn_ubicacion_edit() {
    $("#addUbicacion_edit").prop("disabled", false);
}

function getLocation_edit() {
    if ("geolocation" in navigator) {
        navigator.geolocation.getCurrentPosition(showPosition_edit);
    } else {
        Notificacion("La geolocalización no es compatible con este navegador", "warning");
    }
}

function showPosition_edit(position) {

    var latitud_ini_edit = position.coords.latitude;
    var longitud_ini_edit = position.coords.longitude;

    var latitude = "latitude=" + position.coords.latitude;
    var longitude = "&longitude=" + position.coords.longitude;
    var query = latitude + longitude + "&localityLanguage=en";
    const Http = new XMLHttpRequest();
    var bigdatacloud_api = "https://api.bigdatacloud.net/data/reverse-geocode-client?";

    bigdatacloud_api += query;

    Http.open("GET", bigdatacloud_api);
    Http.send();

    Http.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            var myObj = JSON.parse(this.responseText);
            var ciudad = myObj.locality.toUpperCase();
            $("#ubicacion_edit").val(ciudad);
        }
    }
    setLocation_edit(latitud_ini_edit, longitud_ini_edit);
}

const DEFAULT_COORD_EDIT = [6.2443382, -75.573553]
const resultsWrapperHTML_edit = document.getElementById("search-result-edit");
const Map_edit = L.map("render-map-edit");

const osmTileUrl_edit = "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png";

const osmTile_edit = new L.TileLayer(osmTileUrl_edit, { minZoom: 2, maxZoom: 20 });

Map_edit.setView(new L.LatLng(DEFAULT_COORD_EDIT[0], DEFAULT_COORD_EDIT[1]), 12);
Map_edit.addLayer(osmTile_edit);

const Marker_edit = L.marker(DEFAULT_COORD_EDIT).addTo(Map_edit)

let typingInterval_edit

function onTyping_edit(e) {
    clearInterval(typingInterval_edit)
    const { value } = e

    typingInterval_edit = setInterval(() => {
        clearInterval(typingInterval_edit)
        searchLocation_edit(value)
    }, 500)
}

function searchLocation_edit(keyword) {
    if (keyword) {
        // request to nominatim api
        fetch(`https://nominatim.openstreetmap.org/search?q=${keyword}&format=json`)
            .then((response) => {
                return response.json()
            }).then(json => {
                if (json.length > 0) return renderResults_edit(json)
                else Notificacion("No se encontro ningun resultado", "danger");
            })
    }
}

function renderResults_edit(result) {
    let resultsHTML_edit = "";
    result.map((n) => {
        resultsHTML_edit += "<li><a href='#' onclick='setLocation_edit(" + n.lat + "," + n.lon + ")'>" + n.display_name + "</a></li>";
    })
    resultsWrapperHTML_edit.innerHTML = resultsHTML_edit;
}

function setLocation_edit(lat, lon) {
    $("#lat_edit").val(lat);
    $("#lon_edit").val(lon);

    Map_edit.setView(new L.LatLng(lat, lon), 12);

    Marker_edit.setLatLng([lat, lon]);

    clearResults_edit();
}
function clearResults_edit() {
    resultsWrapperHTML_edit.innerHTML = "";
}
