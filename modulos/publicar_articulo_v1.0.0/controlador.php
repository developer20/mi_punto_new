<?php
include_once("../../config/mainController.php"); // Incluye el Controlador Principal
include_once("../../modulos/publicar_articulo_v1.0.0/modelo.php");	// Incluye el Modelo.
$controller = new mainController; // Instancia a la clase MainController
$modelo = new publicar_articulo(); // Instancia a la clase del modelo
try // Try, manejo de Errores
{
	$metodo = $_SERVER['REQUEST_METHOD'];
	$tipo_res = "";
	$response = null; 
	// Se manejaran dos tipos JSON y HTML
	// Dependiendo del método de la petición ejecutaremos la acción correspondiente.
	// Por ahora solo POST, todas las llamadas se haran por POST
    $variables = $_POST;
	if(!isset($_POST['accion'])){echo "0"; return;} // Evita que ocurra un error si no manda accion.
	$accion = trim($variables['accion']);
		// Dependiendo de la accion se ejecutaran las tareas y se definira el tipo de respuesta.
		switch($accion) {
			case 'cargar_titulo': 
				$tipo_res = 'JSON'; //Definir tipo de respuesta;
				$response = $modelo->cargar_titulo();
      break;

      case 'cargar_tabla':
        $tipo_res = 'JSON'; //Definir tipo de respuesta;
        $fecha_ini = $variables["fecha_ini"];
        $fecha_fin = $variables["fecha_fin"];
        $nombre = $variables["nombre"];
        $estado = $variables["estado"];
        $response = $modelo->cargar_tabla($fecha_ini, $fecha_fin, $nombre, $estado);
      break;

      case 'cargar_categorias': 
				$tipo_res = 'JSON'; //Definir tipo de respuesta;
				$response = $modelo->cargar_categorias();
      break;

      case 'cargar_sub_categorias': 
        $tipo_res = 'JSON'; //Definir tipo de respuesta;
        $id_categoria = $variables["id_categoria"];
        $response = $modelo->cargar_sub_categorias($id_categoria);
      break;

      case "cambiar_estado_f_ini":
        $tipo_res = "JSON";
        $id = $variables["id"];
        $estado = $variables["estado"];
        $datos = $modelo->cambiar_estado_f_ini($id, $estado);
        $response = $datos;
      break;

      case "cambiar_estado":
        $tipo_res = "JSON";
        $id = $variables["id"];
        $estado = $variables["estado"];
        $datos = $modelo->cambiar_estado($id, $estado);
        $response = $datos;
      break;

      case "guardar_registros":
        $tipo_res = "JSON";            
        $titulo = $variables['titulo'];
        $precio = $variables['precio'];
        $categoria = $variables['categoria'];
        $sub_categoria = $variables['sub_categoria'];
        $marca = $variables['marca'];
        $estado_prod = $variables['estado_prod'];
        $f_ini_prod = $variables['f_ini_prod'];
        $f_fin_prod = $variables['f_fin_prod'];
        $descripcion = $variables['descripcion'];
        $ubicacion = $variables['ubicacion'];
        $disponibilidad = $variables['disponibilidad'];
        $palabra_clave = $variables['palabra_clave'];
        $latitud = $variables['latitud'];
        $longitud = $variables['longitud'];              
        $response = $modelo->guardar_registros($titulo,$precio,$categoria,$sub_categoria,$marca,$estado_prod,$f_ini_prod,$f_fin_prod,$descripcion,$ubicacion,$disponibilidad,$palabra_clave,$latitud,$longitud);
      break;

      case "obtener_datos_edit":
        $tipo_res = "JSON";
        $id = $variables["id"];
        $response = $modelo->obtener_datos_edit($id);
      break;

      case 'cargar_vendedor': 
        $tipo_res = 'JSON'; //Definir tipo de respuesta;
        $response = $modelo->cargar_vendedor();
      break;

      case 'cargar_vendedor_edit': 
        $tipo_res = 'JSON'; //Definir tipo de respuesta;
        $id = $variables["id"];
        $response = $modelo->cargar_vendedor_edit($id);
      break;

      case 'cargar_imagenes': 
        $tipo_res = 'JSON'; //Definir tipo de respuesta;
        $id = $variables["id"];
        $response = $modelo->cargar_imagenes($id);
      break;

      case "editar_registros":
        $tipo_res = 'JSON';              
        $titulo = $variables['titulo'];
        $precio = $variables['precio'];
        $categoria = $variables['categoria'];
        $sub_categoria = $variables['sub_categoria'];
        $marca = $variables['marca'];
        $estado_prod = $variables['estado_prod'];
        $f_ini_prod = $variables['f_ini_prod'];
        $f_fin_prod = $variables['f_fin_prod'];
        $descripcion = $variables['descripcion'];
        $ubicacion = $variables['ubicacion'];
        $disponibilidad = $variables['disponibilidad'];
        $palabra_clave = $variables['palabra_clave_edit'];
        $latitud = $variables['latitud'];
        $longitud = $variables['longitud'];
        $id_update = $variables['id_update'];            
        $response = $modelo->editar_registros($titulo,$precio,$categoria,$sub_categoria,$marca,$estado_prod,$f_ini_prod,$f_fin_prod,$descripcion,$ubicacion,$disponibilidad,$palabra_clave,$latitud,$longitud,$id_update);
      break;

      case 'eliminar_img':
        $tipo_res = 'JSON';
        $id = $variables['id'];
        $response = $modelo->eliminar_img($id);
      break;

      case 'guardar_contacto':
        $tipo_res = 'JSON';
        $id_pdv = $variables['id_pdv'];
        $nombre = $variables['nombre'];
        $nom_encargado = $variables['nom_encargado'];
        $telefono = $variables['telefono'];
        $celular = $variables['celular'];
        $ter_cond = $variables['ter_cond'];
        $id_update = $variables['id_update'];
        $response = $modelo->guardar_contacto($id_pdv,$nombre,$nom_encargado,$telefono,$celular,$ter_cond,$id_update);
      break;

      case 'consultar_notificacion':
        $tipo_res = 'JSON'; //Definir tipo de respuesta;
        $response = $modelo->consultar_notificacion();
      break;

      case 'leer_notificacion':
        $tipo_res = 'HTML'; //Definir tipo de respuesta;
        $id_notif = $variables['id_notif'];
        $response = $modelo->leer_notificacion($id_notif);
      break;

      case 'eliminar_solicitud':
        $tipo_res = 'JSON';
        $id = $variables['id'];
        $response = $modelo->eliminar_solicitud($id);
      break;

      case 'validar_usuario': 
        $tipo_res = 'JSON'; //Definir tipo de respuesta;
        $response = $modelo->validar_usuario();
      break;
		}

	// Respuestas del Controlador
	if($tipo_res == "JSON")
	{
	  echo json_encode($response,true); // $response será un array con los datos de nuestra respuesta.
	}
	elseif ($tipo_res == "HTML") {
	  echo $response; // $response será un html con el string de nuestra respuesta.
	}
} // Fin Try
catch (Exception $e) {}
