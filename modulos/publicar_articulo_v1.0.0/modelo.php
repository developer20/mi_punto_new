<?php
require("../../config/mainModel.php");
class publicar_articulo{
	/*Metodos de la DB.
  $this->consultar_datos($query); // Realiza la consulta y devuelve un Array Asociativo con los datos
  $this->ejecuta_query($query); // Ejecuta Insert, Update, Delete, Alter, Drop.. / devuelve el ultimo id afectado.
  $this->total_registros($columna, $tabla, $where = ""); // Devuelve el numero de registros. count();
  $this->trae_uno($query); // Realiza una consulta, devolviendo el primer valor que encuentre.
  $this->begin_work();
  $this->commit();
  $this->rollback();
  */
    private $ruta ='../../static/img/referencias/publicaciones/';
  	public function __construct()
  	{
  		$BD=new BD();
  		$this->BD = $BD;
  		$this->BD->conectar();
  	}
    public function __destruct()
    {
    	$this->BD->desconectar();
    }

	//Espacio Para declara las funciones que retornan los datos de la BD.
	
	public function cargar_titulo(){
        
        $consulta = "SELECT id, titulo FROM {$GLOBALS["BD_POS"]}.crear_productos__mp ORDER BY titulo";        
		$datos = $this->BD->devolver_array($consulta);
		
		return $datos;
    }

    public function cargar_categorias(){
        
        $consulta = "SELECT id, nombre FROM {$GLOBALS["BD_POS"]}.categoria__mp ORDER BY nombre";        
		$datos = $this->BD->devolver_array($consulta);
		
		return $datos;
    }

    public function cargar_sub_categorias($id_categoria){
        
        $consulta = "SELECT id, nombre FROM {$GLOBALS["BD_POS"]}.sub_categoria__mp WHERE id_categoria = $id_categoria ORDER BY nombre";        
		$datos = $this->BD->devolver_array($consulta);
		
		return $datos;
    }

    public function cargar_tabla($fecha_ini, $fecha_fin, $nombre, $estado){

        $condicion = "";

        if($fecha_ini != "" && $fecha_fin !=""){
            $condicion .= "AND cp.fecha_inicio >= '$fecha_ini' AND IF(cp.fecha_fin = '0000-00-00',DATE_ADD(cp.fecha_inicio, INTERVAL 6 MONTH),cp.fecha_fin) <= '$fecha_fin'";
        }else if($fecha_ini != ""){
            $condicion .= "AND cp.fecha_inicio = '$fecha_ini'";
        }else if($fecha_fin != ""){
            $condicion .= "AND IF(cp.fecha_fin = '0000-00-00',DATE_ADD(cp.fecha_inicio, INTERVAL 6 MONTH),cp.fecha_fin) = '$fecha_fin'";
        }

        if($nombre != ""){
            $condicion .= "AND cp.id = '$nombre'";
        }
        if($estado != ""){
            $condicion .= "AND cp.estado = $estado";
        }

          $consulta = "SELECT 
                            cp.id AS id_producto,
                            cp.fecha_inicio,
                            IF(cp.fecha_fin = '0000-00-00',DATE_ADD(cp.fecha_inicio, INTERVAL 6 MONTH),cp.fecha_fin) AS fecha_final,
                            cp.titulo,
                            cp.estado,
                            ip.nombre AS nombre_imagen
                       FROM {$GLOBALS["BD_POS"]}.crear_productos__mp cp
                       INNER JOIN {$GLOBALS["BD_POS"]}.imagenes_productos__mp ip ON ip.id_producto = cp.id AND ip.principal = 1
                       WHERE 1 $condicion";                                               
          $datos = $this->BD->devolver_array($consulta);
          
          return array("data" => $datos);
      }
      
      public function cambiar_estado ($id, $estado) {

        $oSession = unserialize($_SESSION[$GLOBALS["SESION_POS"]]);
        $id_user = $oSession->VSidpos;
        $id_distri = $oSession->VSid_distri;        
        $response = 0;
       $condicion = "";
       if($estado == "1"){
            $condicion .= ",fecha_inicio = CURDATE(),fecha_fin = '0000-00-00'";
       }
        $consulta = "UPDATE {$GLOBALS["BD_POS"]}.crear_productos__mp SET estado = $estado $condicion WHERE id = $id";     
        $datos = $this->BD->consultar($consulta);        

        if (count ($datos) > 0) {
                $consulta_audi = "INSERT INTO {$GLOBALS["BD_POS"]}.audi_crear_productos__mp(id_producto,id_pos,id_distri,titulo,precio,id_categoria,id_sub_categoria,marca,estado_producto,fecha_inicio,fecha_fin,descripcion,ubicacion,disponibilidad,palabras,estado,usuario,fecha_update,hora_update,accion) (SELECT id,id_pos,id_distri,titulo,precio,id_categoria,id_sub_categoria,marca,estado_producto,fecha_inicio,fecha_fin,descripcion,ubicacion,disponibilidad,palabras,estado, ".$id_user.",CURDATE(),CURTIME(), 1 FROM {$GLOBALS["BD_POS"]}.crear_productos__mp WHERE id = $id);";
                $datos_audi = $this->BD->consultar ($consulta_audi);            
        } else {
            $response = 1;
        }
        return $response;
    }

    public function obtener_datos_edit($id){

        $consulta = "SELECT 
                            cp.id,
                            cp.titulo,
                            cp.precio,
                            cp.id_categoria,
                            cp.id_sub_categoria,
                            cp.marca,
                            cp.estado_producto,
                            IF(cp.estado_producto = 1,'Nuevo',IF(cp.estado_producto = 2, 'Usado - Como nuevo',
                            IF(cp.estado_producto = 3, 'Usado - Buen estado',IF(cp.estado_producto = 4,'Usado - Aceptable','')))) AS estado_prod,
                            cp.fecha_inicio,
                            DATEDIFF(CURDATE(),cp.fecha_inicio) AS fecha_pub,
                            IF(cp.fecha_fin = '0000-00-00',DATE_ADD(cp.fecha_inicio, INTERVAL 6 MONTH),cp.fecha_fin) AS fecha_final,
                            cp.descripcion,
                            cp.ubicacion,
                            cp.disponibilidad,
                            IF(cp.disponibilidad = 1,'Unico',IF(cp.disponibilidad = 2, 'Disponible','')) AS disponibilidad_prod,
                            cp.palabras,
                            cp.latitud,
                            cp.longitud
                    FROM {$GLOBALS["BD_POS"]}.crear_productos__mp cp
                    WHERE cp.id = $id";                    

         $datos = $this->BD->devolver_array($consulta);

         return $datos;
    }
    

    public function cargar_vendedor(){

        $oSession = unserialize($_SESSION[$GLOBALS["SESION_POS"]]);
		$id_user = $oSession->VSidpos;

        $consulta = "SELECT 
                            p.idpos,
                            p.razon AS nombre_pdv,
                            p.nombre_cli AS nombre,
                            p.tel AS telefono,
                            p.celular AS celular,
                            p.latitud,
                            p.longitud
                       FROM {$GLOBALS["BD_POS"]}.puntos p
                      WHERE p.idpos = $id_user";                                          

         $datos = $this->BD->devolver_array($consulta);

         return $datos;
    }

    public function cargar_vendedor_edit($id){
        $oSession = unserialize($_SESSION[$GLOBALS["SESION_POS"]]);
        $id_user = $oSession->VSidpos;
        
        $consulta ="SELECT 
                            cp.id,
                            p.idpos,
                            p.razon AS nombre_pdv,
                            p.nombre_cli AS nombre,
                            p.tel AS telefono,
                            p.celular AS celular
                        FROM {$GLOBALS["BD_POS"]}.crear_productos__mp cp
                   LEFT JOIN {$GLOBALS["BD_POS"]}.puntos p ON p.idpos = cp.id_pos
                   WHERE cp.id = $id";
        $datos = $this->BD->devolver_array($consulta);

        return array("datos" => $datos,"usuario" => $id_user);
    }

    public function cargar_imagenes($id){
        $consulta ="SELECT 
                            ip.id,
                            ip.nombre,
                            ip.principal
                    FROM {$GLOBALS["BD_POS"]}.imagenes_productos__mp ip
                    WHERE ip.id_producto = $id";
        $datos = $this->BD->devolver_array($consulta);

        return $datos;
    }

    public function eliminar_img($id){
        
        $response = 0;        	
        $consulta = "DELETE FROM {$GLOBALS["BD_POS"]}.imagenes_productos__mp WHERE id = '$id'";            		
        $datos = $this->BD->consultar($consulta);        
        if(!$datos){
            $response = 1;
        }

        return $response;
	}


    public function guardar_registros($titulo,$precio,$categoria,$sub_categoria,$marca,$estado_prod,$f_ini_prod,$f_fin_prod,$descripcion,$ubicacion,$disponibilidad,$palabra_clave,$latitud,$longitud){

        $oSession = unserialize($_SESSION[$GLOBALS["SESION_POS"]]);
        $id_user  = $oSession->VSidpos;
        $id_distri  = $oSession->VSid_distri;        
        $fecha = date("Y-m-d");
        $error = 0;
        $estado = "";

        if($f_ini_prod != $fecha){
            $estado .= 0;
        }else{
            $estado .= 1;
        }      

        $this->BD->consultar("BEGIN");

        $consulta = "INSERT INTO {$GLOBALS["BD_POS"]}.crear_productos__mp(id_pos,id_distri,titulo,precio,id_categoria,id_sub_categoria,marca,estado_producto,fecha_inicio,fecha_fin,descripcion,ubicacion,latitud,longitud,disponibilidad,palabras,estado) VALUES('$id_user','$id_distri','$titulo','$precio','$categoria','$sub_categoria','$marca','$estado_prod','$f_ini_prod','$f_fin_prod','$descripcion','$ubicacion','$latitud','$longitud','$disponibilidad','$palabra_clave', $estado)";            
        $datos = $this->BD->consultar($consulta);
        $id_registro = $this->BD->last_insert_id();
        if(!$datos){
            $error = 1;
         }else{             
             $consulta_audi = "INSERT INTO {$GLOBALS["BD_POS"]}.audi_crear_productos__mp(id_producto,id_pos,id_distri,titulo,precio,id_categoria,id_sub_categoria,marca,estado_producto,fecha_inicio,fecha_fin,descripcion,ubicacion,latitud,longitud,disponibilidad,palabras,estado,usuario,fecha_update,hora_update,accion) (SELECT id,id_pos,id_distri,titulo,precio,id_categoria,id_sub_categoria,marca,estado_producto,fecha_inicio,fecha_fin,descripcion,ubicacion,latitud,longitud,disponibilidad,palabras,estado, '".$id_user."',CURDATE(),CURTIME(), 0 FROM {$GLOBALS["BD_POS"]}.crear_productos__mp WHERE id = $id_registro);";             
             $datos_audi = $this->BD->consultar($consulta_audi);
             if(!$datos_audi){
                $error = 1;
             }else{       
            
            $principal = 1;
             foreach ($_FILES as $key){            
                $nombre_img = $key['name'];
                $temporal = $key['tmp_name'];                        
                $consulta_img = "INSERT INTO {$GLOBALS["BD_POS"]}.imagenes_productos__mp(nombre,principal,id_producto)VALUES('$nombre_img',$principal,$id_registro)";
                $datos_img = $this->BD->consultar($consulta_img);
                move_uploaded_file($temporal, $this->ruta.$nombre_img);                         
                $principal = 0;
              }
              if(!$datos_img){
                $error = 1;
             }
            }
            if($error == 0){
                $this->BD->consultar("COMMIT");
                return 0;
            }else{
                $this->BD->consultar("ROLLBACK");
                return 1;
            }
         }
    }

    public function editar_registros($titulo,$precio,$categoria,$sub_categoria,$marca,$estado_prod,$f_ini_prod,$f_fin_prod,$descripcion,$ubicacion,$disponibilidad,$palabra_clave,$latitud,$longitud,$id){
        
        $oSession = unserialize($_SESSION[$GLOBALS["SESION_POS"]]);
        $id_user = $oSession->VSidpos;
        $id_distri  = $oSession->VSid_distri;        
        $error = 0;
        
        $this->BD->consultar("BEGIN");

        $consulta = "UPDATE {$GLOBALS["BD_POS"]}.crear_productos__mp SET id_distri = '$id_distri', titulo = '$titulo',precio = '$precio',id_categoria = '$categoria',id_sub_categoria = '$sub_categoria',marca = '$marca',estado_producto = '$estado_prod',fecha_inicio = '$f_ini_prod',fecha_fin = '$f_fin_prod',descripcion = '$descripcion',ubicacion = '$ubicacion',disponibilidad = '$disponibilidad',palabras = '$palabra_clave',latitud = '$latitud', longitud = '$longitud' WHERE id = $id";                 
        $datos = $this->BD->consultar($consulta);
        if(!$datos){
            $error = 1;
         }else{
             $consulta_audi = "INSERT INTO {$GLOBALS["BD_POS"]}.audi_crear_productos__mp(id_producto,id_pos,id_distri,titulo,precio,id_categoria,id_sub_categoria,marca,estado_producto,fecha_inicio,fecha_fin,descripcion,ubicacion,latitud,longitud,disponibilidad,palabras,estado,usuario,fecha_update,hora_update,accion) (SELECT id,id_pos,id_distri,titulo,precio,id_categoria,id_sub_categoria,marca,estado_producto,fecha_inicio,fecha_fin,descripcion,ubicacion,latitud,longitud,disponibilidad,palabras,estado, '".$id_user."',CURDATE(),CURTIME(), 1 FROM {$GLOBALS["BD_POS"]}.crear_productos__mp WHERE id = $id);";            
             $datos_audi = $this->BD->consultar($consulta_audi);
             if(!$datos_audi){
                $error = 1;
             }else{
                 if(!empty($_FILES)){         
                foreach ($_FILES as $key){         
                    $nombre_img = $key['name'];
                    $temporal = $key['tmp_name'];                        
                    $consulta_img = "INSERT INTO {$GLOBALS["BD_POS"]}.imagenes_productos__mp(nombre,principal,id_producto)VALUES('$nombre_img',0,$id)";
                    $datos_img = $this->BD->consultar($consulta_img);
                    move_uploaded_file($temporal, $this->ruta.$nombre_img);                                                 
                  }                
                  if(!$datos_img){
                    $error = 1;
                 }
                }
             }
            if($error == 0){
                $this->BD->consultar("COMMIT");
                return 0;
            }
            else{
                $this->BD->consultar("ROLLBACK");
                return 1;
            }
         }
    }
   
	public function cambiar_estado_f_ini($id,$estado){
        $oSession = unserialize($_SESSION[$GLOBALS["SESION_POS"]]);
	    $id_user = $oSession->VSidpos;
        $id_distri  = $oSession->VSid_distri;

        $consulta = "UPDATE {$GLOBALS["BD_POS"]}.crear_productos__mp SET estado = $estado WHERE id = $id";                    
        $datos = $this->BD->consultar($consulta);
        
        if(!$datos){
            return 1;
        }else{
            return 0;
        }
    }
  
}// Fin clase
?>