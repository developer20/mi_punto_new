$(document).ready(function() {
	$('.fecha').datepicker({
		format: 'yyyy-mm-dd',
		todayHighlight: true,
		todayBtn: true
	});

	$("#frmConsulta").submit(function(event) {
	 	event.preventDefault();
	 	Cargar_Reporte();
    });
});

function Cargar_Reporte(){
	
	f_ini = $("#f_ini").val();
	f_fin = $("#f_fin").val();
	
	if(f_ini != "" && f_fin != ""){
	
	   var cont = 0;
		
	   if ( ! $.fn.DataTable.isDataTable( '#t_reporte' ) ) {
		  dtable = $("#t_reporte").DataTable({
				"ajax": {
				"url": "modulos/reporte_activaciones/controlador.php",
				"type": "POST",
				"deferRender": false,
				"data":{accion:'Cargar_Informe',f_ini:f_ini, f_fin:f_fin}
			  },
			  "bFilter": false,
			  "dom": '<"top"fl>rt<"bottom"<"toolbar">p><"clear">',
			  "responsive":false,
			  "columns": [
				{ "data": "refe"},
				{ "data": "refe"},
				{ "data": "cantidad"},
				{ "data": "id_ref"}
			],
			 "columnDefs": [
				{
					"targets": 0,
					"data": "",
					 render: function ( data, type, row ) {
						 
						cont++ 
						 
						return cont;
					 }
					 
				},
				{
					"targets": 3,
					"data": "id_ref",
					 render: function ( data, type, row ) {
						return '<i class="glyphicon glyphicon-plus-sign DetaRef" id = "id_ref_'+row.id_ref+'" style="color: #10628A;font-size: 20px;cursor: pointer;position:relative;"></i>';
					 }
					 
				},
				
			 ],
			fnDrawCallback: function () {
				
				$(".DetaRef").unbind("click");
				$(".DetaRef").click(function() {
					
					var data = dtable.row( $(this).parents('tr') ).data();
					var id_ref = data['id_ref'];
					var elemento = $(this);
					
					var tr = $(this).closest('tr');
					var row = dtable.row( tr );
					var tabla = "";
					 var cont2 = 0;
					
					if(elemento.hasClass( "DetaRef" )){
						
						elemento.attr("class","glyphicon glyphicon-minus-sign CloseDeta");
	
						$.post('modulos/reporte_activaciones/controlador.php',
							{
								accion: 'Consultar_Ases',
								id_ref:id_ref,
								f_ini:f_ini, 
								f_fin:f_fin
								
							},
							function(data, textStatus) {
								
								if(data != 0 || data != "")
								{	
									var datos = JSON.parse(data);
									
									if(datos != ""){
										
										tabla += '<table id="tblPed2" align="center" class="table dt-responsive nowrap" cellspacing="0" width="100%" style="border: 1px solid #DADADA;">';
										tabla += '<thead>';
										tabla += '<tr>';
										tabla += '<th align="center">Nº</th>';
										tabla += '<th align="center">Fecha Activación</th>';
										tabla += '<th align="center">Hora</th>';
										tabla += '<th align="center">Serial</th>';
										tabla += '<th align="center">Movil</th>';
										tabla += '<th align="center">Distribuidor</th>';
										tabla += '<th align="center">Vendedor</th>';
										tabla += '</tr>';
										tabla += '</thead>';
										$.each(datos,function(index, fila) {
											cont2++;
											tabla += '<tr>';
											tabla += '<td align="center">'+cont2+'</td>';
											tabla += '<td align="center">'+fila.fecha_act+'</td>';
											tabla += '<td align="center">'+fila.hora_act+'</td>';
											tabla += '<td align="center">'+fila.serial+'</td>';
											tabla += '<td align="center">'+fila.movil+'</td>';
											tabla += '<td align="center">'+fila.distribuidor+'</td>';
											tabla += '<td align="center">'+fila.vendedor+'</td>';
											tabla += '</tr>';
										});
										
										tabla += '</table>';
										
									}
									
									row.child(tabla).show();
									tr.addClass('shown');
								}
							}
						);
					
					}else{
					
						elemento.attr("class","glyphicon glyphicon-plus-sign DetaRef");
						row.child.hide();
						tr.removeClass('shown');
					}
					
				});
				
			}
	
		  });
		}else{
			dtable.destroy();
			Cargar_Reporte();
		}
		
		$("#CSV").unbind("click");
		$("#CSV").click(function() {
			Datos_CSV();
		});
		
		
		$("#consulta").show();
	
	}else{
		Notificacion('El rango de fechas es obligatorio',"error");
	}
	
}

function Datos_CSV(){
	
	var f_ini = $("#f_ini").val();
	var f_fin = $("#f_fin").val();
	
	$.post('modulos/reporte_activaciones/controlador.php',
	{
		accion: 'Datos_CSV',
		f_ini: f_ini,
		f_fin: f_fin
	},
	function(data, textStatus) {
		if(data != ""){
			data = JSON.parse(data);
			
		    var header = "Producto,Fecha de Activacion,Hora de Activacion,Serial, Movil, Distribuidor, Vendedor";
			var columnas = "refe,fecha_act,hora_act,serial,movil,distribuidor,vendedor";
			ExportarCSV(data, header,"Reporte de Activaciones",columnas);
			 
		}
	});
}