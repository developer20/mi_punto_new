<?php
require("../../config/mainModel.php");
class reporte_activaciones{

   public function Cargar_Informe($f_ini,$f_fin)
   {
	   $oSession = unserialize($_SESSION[$GLOBALS["SESION_POS"]]);
	   $idpos=$oSession->VSidpos;
		
	   $array_response = array();
		
	   $BD=new BD();
	   $BD->conectar();
		
		$sql = "SELECT sim.id_referencia id_ref, refe.producto refe, COUNT(DISTINCT sim.id) cantidad 
		
		FROM {$GLOBALS["BD_NAME"]}.simcards sim,{$GLOBALS["BD_NAME"]}.referencias refe 
		
		WHERE sim.activo = 1 and sim.id_referencia = refe.id and sim.estado = 1 and sim.id_pos = $idpos and sim.fecha_ac>='$f_ini' and sim.fecha_ac<= '$f_fin' GROUP BY sim.id_referencia";
		
		$res = $BD->devolver_array($sql);
		
		/*
		if($BD->numreg($res)>0){
			while(!$res ->EOF){
				$array_response[] = array("id_ref" => $res->fields['id_referencia'], "refe" => $res->fields['producto'], "cantidad" => $res->fields['cantidad']);
				$res ->MoveNext();	
			}
		}*/
			
		return $res;
		
   }
   
   public function Cargar_Informe_Ases($id_ref,$f_ini,$f_fin)
   {
	   $oSession = unserialize($_SESSION[$GLOBALS["SESION_POS"]]);
	   $idpos=$oSession->VSidpos;
		
	   $array_response = array();
		
	   $BD=new BD();
	   $BD->conectar();
		
		$sql = "SELECT sim.fecha_ac fecha_act,sim.hora_ac hora_act,CONCAT('89',sim.iccid) serial,sim.movil, dis.nombre as distribuidor,usu.nombre_usuario vendedor
		
		FROM {$GLOBALS["BD_NAME"]}.simcards sim,{$GLOBALS["BD_NAME"]}.distribuidores dis, {$GLOBALS["BD_NAME"]}.usuarios_distri usu
		
		WHERE sim.activo = 1 AND sim.id_referencia = $id_ref AND sim.id_pos = $idpos AND sim.distri = dis.id AND sim.fecha_ac>='$f_ini' AND sim.fecha_ac<= '$f_fin' AND usu.id_usuario = sim.id_vendedor AND usu.id_distri = sim.distri ORDER BY sim.fecha_ac,sim.hora_ac";
		
		$res = $BD->devolver_array($sql);
		
		/*
		if($BD->numreg($res)>0){
				while(!$res ->EOF){
					
					$distri_bd = "distri_".$res->fields['nombre_corto']."_".$res->fields['distri'];
					
					$sql = "SELECT concat(nombre,' ',apellido) as nombres
					
					FROM ".$distri_bd.".usuarios usu
					
					WHERE usu.id = ".$res->fields['id_vendedor'];
					
					$res_vende = $BD->consultar($sql);
					
					$array_response[] = array("fecha_act" => $res->fields['FECHA_VENTA'], "hora_act" => $res->fields['HORA_VENTA'], "serial" => $res->fields['NUMERO_ICC'], "movil" => $res->fields['CELULAR'], "distribuidor" => $res->fields['nombre_distri'], "vendedor" => $res_vende->fields['nombres']);
					$res ->MoveNext();	
				}
		}*/
			
		return $res;
		
   }
   
   public function Datos_CSV($f_ini,$f_fin)
   {
	   $oSession = unserialize($_SESSION[$GLOBALS["SESION_POS"]]);
	   $idpos=$oSession->VSidpos;
		
	   $array_response = array();

	   $cont1 = '="';
       $cont2 = '"';
		
	   $BD=new BD();
	   $BD->conectar();
		
		$sql = "SELECT refe.producto refe,sim.fecha_ac fecha_act,sim.hora_ac hora_act,CONCAT('$cont1','89',sim.iccid,'$cont2') serial,sim.movil, dis.nombre_corto, dis.nombre as distribuidor,usu.nombre_usuario vendedor
		
		FROM {$GLOBALS["BD_NAME"]}.simcards sim,{$GLOBALS["BD_NAME"]}.referencias refe,{$GLOBALS["BD_NAME"]}.distribuidores dis, {$GLOBALS["BD_NAME"]}.usuarios_distri usu
		
		WHERE sim.id_referencia = refe.id and sim.distri = dis.id and sim.estado = 1 and sim.id_pos = $idpos and sim.fecha_ac>='$f_ini' and sim.fecha_ac<= '$f_fin' AND usu.id_usuario = sim.id_vendedor AND usu.id_distri = sim.distri ORDER BY sim.fecha_ac,sim.hora_ac";
		
		$res = $BD->devolver_array($sql);
		
		/*
		if($BD->numreg($res)>0){
				while(!$res ->EOF){
					
					$distri_bd = "distri_".$res->fields['nombre_corto']."_".$res->fields['distri'];
					
					$sql = "SELECT concat(nombre,' ',apellido) as nombres
					
					FROM ".$distri_bd.".usuarios usu
					
					WHERE usu.id = ".$res->fields['id_vendedor'];
					
					$res_vende = $BD->consultar($sql);
					
					$array_response[] = array("refe" => $res->fields['producto'],"fecha_act" => $res->fields['FECHA_VENTA'], "hora_act" => $res->fields['HORA_VENTA'], "serial" => $res->fields['NUMERO_ICC'].".", "movil" => $res->fields['CELULAR'], "distribuidor" => $res->fields['nombre_distri'], "vendedor" => $res_vende->fields['nombres']);
					$res ->MoveNext();	
				}
		}*/
			
		return $res;
		
   }
   
}// Fin clase
?>