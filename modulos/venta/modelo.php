<?php
require("../../config/mainModel.php");
class ventas {

	public function __construct()
    {
      $BD=new BD();
      $this->BD = $BD;
      $this->BD->conectar();
    }
    public function __destruct()
    {
      $this->BD->desconectar();
    }

	//// Funciones para realizar la recarga

	public function CargarBols($id_bolsa)
    {
		if(isset($_SESSION[$GLOBALS["SESION_POS"]])){

			$oSession = unserialize($_SESSION[$GLOBALS["SESION_POS"]]);
			$idpos=$oSession->VSidpos;
			$token=$oSession->VStoken;
			$BD=new BD();
		    $BD->conectar();

		    $saldos = array();

		    $sql = "SELECT bp.id AS id_bolsa,rud.descripcion AS nom_bolsa,bp.id_proveedor,bp.id_distri,bp.saldo,bp.saldo_incentivo,bp.principal,rud.id AS id_bolsa_prin FROM bolsas_puntos AS bp INNER JOIN {$GLOBALS["BD_NAME"]}.recargas__usudistri_prov AS rud ON (rud.id = bp.id_bolsa AND rud.id_distri = bp.id_distri AND rud.id_proveedor = bp.id_proveedor AND rud.estado = 1)  WHERE bp.id_pos = $idpos";

		    if($id_bolsa > 0){
		    	$sql .= " AND bp.id = $id_bolsa";
		    }

			$res = $BD->devolver_array($sql);

			if(count($res)>0){

				$sql = "SELECT token FROM token_puntos WHERE id_pos = $idpos";
				$res_exits_to = $BD->consultar($sql);

				if($BD->numreg($res_exits_to) > 0){

					if(strcmp($token,$res_exits_to->fields["token"]) == 0){

						$sql = "SELECT idpos FROM puntos WHERE idpos=$idpos AND estado_recarga = 1";
						$res_pos = $BD->consultar($sql);

						if($BD->numreg($res_pos) > 0){

							$_SESSION["key_trans"] = $this->generar_token();
							$_SESSION["venta"]=0;

							if($id_bolsa > 0){

								$sql = "UPDATE bolsas_puntos AS bp SET bp.principal = 0 WHERE bp.id_pos = $idpos";
								$res_up_pos1=$this->BD->consultar($sql);

								if($BD->numupdate() == 0){
									$res_up_pos12=$BD->consultar($sql);
									if($BD->numupdate() == 0){
										$res_up_pos13=$BD->consultar($sql);
									}
								}

								$sql = "UPDATE bolsas_puntos AS bp SET bp.principal = 1 WHERE bp.id = $id_bolsa AND bp.id_pos = $idpos";
								$res_up_pos2=$this->BD->consultar($sql);

								if($BD->numupdate() == 0){
									$res_up_pos21=$BD->consultar($sql);
									if($BD->numupdate() == 0){
										$res_up_pos22=$BD->consultar($sql);
									}
								}

								$sql = "INSERT INTO audi_bolsas_puntos (id_bolsa_punto, id_pos, id_bolsa, id_proveedor, id_distri, saldo, saldo_incentivo, principal, movimiento, fecha, hora) VALUES ({$res[0]["id_bolsa_prin"]},$idpos,$id_bolsa,{$res[0]["id_proveedor"]},{$res[0]["id_distri"]},{$res[0]["saldo"]},{$res[0]["saldo_incentivo"]},1,2,CURDATE(),CURTIME())";
								$res_audi_bolsa=$BD->consultar($sql);
							}

							return array("saldo" => $res, "val_pes" => $_SESSION["key_trans"],"estado" => 1, "msg" => "");

						}else{
							return array("saldo" => 0,"saldo_in" => 0, "val_pes" => 0,"estado" => -1, "msg" => "No es posible que puedas acceder a este módulo porque no tienes hablitada la opción de venta de recargas");
						}

					}else{
						return array("saldo" => 0,"saldo_in" => 0, "val_pes" => 0,"estado" => 0, "msg" => "No es posible que puedas realizar recargas por que has iniciado sesión en otro computador");
					}

				}else{
					return array("saldo" => 0,"saldo_in" => 0, "val_pes" => 0,"estado" => 0, "msg" => "No es posible que puedas realizar recargas porque es necesario que vuelvas a iniciar sesión");
				}

			}

		}
	}

	public function CargarOpers($tipo,$id_bolsa)
    {

		if(isset($_SESSION[$GLOBALS["SESION_POS"]])){

			$oSession = unserialize($_SESSION[$GLOBALS["SESION_POS"]]);

			$idpos=$oSession->VSidpos;
			$idDistri=$oSession->VSid_distri;
			$res = array();

			$sql = "SELECT bp.id_proveedor FROM bolsas_puntos AS bp INNER JOIN {$GLOBALS["BD_NAME"]}.recargas__usudistri_prov AS rud ON (rud.id = bp.id_bolsa AND rud.id_distri = bp.id_distri AND rud.id_proveedor = bp.id_proveedor AND rud.estado = 1)  WHERE bp.id_pos = $idpos";

			if($id_bolsa > 0){
				$sql .= " AND bp.id = $id_bolsa";
			}else{
				$sql .= " AND bp.principal = 1";
			}

			$res_pro = $this->BD->consultar($sql);

			if(count($res_pro)>0){

				$sql = "SELECT id,titulo,imagen,descripcion AS des,paquetes FROM operadores WHERE estado = 1 AND id = (SELECT id_operador FROM operadores_proveedor AS op WHERE op.id_proveedor = {$res_pro->fields["id_proveedor"]} AND op.id_operador = operadores.id AND op.estado = 1)";

				/*if($tipo == 1){
					$sql .= " and id!=6";
				}*/

				$sql .= " order by orden";

				$res = $this->BD->devolver_array($sql);

			}

			$this->BD->desconectar();


			return $res;

		}

	}

	public function CargarCatePaquete($id_servicio,$val_paque,$id_bolsa)
    {

		if(isset($_SESSION[$GLOBALS["SESION_POS"]])){

			$oSession = unserialize($_SESSION[$GLOBALS["SESION_POS"]]);
			$idpos=$oSession->VSidpos;
			$idDistri=$oSession->VSid_distri;

			$array_response = array("id_oper"=>0,"id_pro"=>0,"info_paquete"=>array());
			$res = array();

			$sql = "SELECT bp.id_proveedor FROM bolsas_puntos AS bp INNER JOIN {$GLOBALS["BD_NAME"]}.recargas__usudistri_prov AS rud ON (rud.id = bp.id_bolsa AND rud.id_distri = bp.id_distri AND rud.id_proveedor = bp.id_proveedor AND rud.estado = 1)  WHERE bp.id_pos = $idpos AND bp.id = $id_bolsa";

			$res_pro = $this->BD->devolver_array($sql);

			/*$sql = "SELECT id_proveedor FROM {$GLOBALS["BD_NAME"]}.recargas__usudistri_prov WHERE id_distri = $idDistri AND estado = 1";
			$res_pro = $this->BD->devolver_array($sql);*/

			if(count($res_pro)>0)
			{

				$id_pro = $res_pro[0]["id_proveedor"];

				$sql = "SELECT o.id,op.id_proveedor FROM operadores AS o INNER JOIN operadores_proveedor AS op ON (op.id_operador = o.id AND op.id_proveedor = $id_pro) WHERE o.estado = 1 AND o.id = $id_servicio";
				$res_oper = $this->BD->devolver_array($sql);

				if(count($res_oper)>0){

					if($val_paque == 1){

						$sql="SELECT id categoria,nombre AS des FROM catagorias_paquetes WHERE id_servicio = $id_servicio";
						$res=$this->BD->devolver_array($sql);

					}

					$array_response = array("id_oper"=>$res_oper[0]["id"],"id_pro"=>$res_oper[0]["id_proveedor"],"info_paquete"=>$res);

				}

			}

			$this->BD->desconectar();

			return $array_response;

		}

	}

	public function CargarServs($id_servicio,$categoria,$id_pro)
    {

		if(isset($_SESSION[$GLOBALS["SESION_POS"]])){

			$oSession = unserialize($_SESSION[$GLOBALS["SESION_POS"]]);

			$array_response = array();

			$sql="SELECT p.id AS code,pp.valor_venta AS valVen, observacion AS obs,descripcion AS  des,0 AS vals FROM paquetigos AS p, paquetes_proveedores AS pp WHERE p.id = pp.id_paquete AND pp.id_prov = $id_pro AND p.id_servicio = $id_servicio AND p.estado = 1 AND p.categoria = $categoria";
			$res=$this->BD->devolver_array($sql);

			/*
			while(!$res ->EOF){

				$array_response[] = array("code" => $res->fields["id"],"valVen" => $res->fields["valor_venta"],"obs" => $res->fields["observacion"],"des" => $res->fields["descripcion"],"vals" => $res->fields["valor"]);

				$res ->MoveNext();
			}*/

			$this->BD->desconectar();

			return $res;

		}

	}


	public function consultar_porcentajes($servicio,$valor,$itemservicio,$id_pro,$id_bolsa)
    {

    	//$sql = "SELECT o.id,o.titulo,o.imagen,o.descripcion AS des,o.paquetes,op.id_proveedor AS id_pro FROM operadores AS o INNER JOIN operadores_proveedor AS op ON (op.id_operador = o.id AND op.estado = 1) WHERE o.estado = 1";

    	$oSession = unserialize($_SESSION[$GLOBALS["SESION_POS"]]);

		$idDistri=$oSession->VSid_distri;
		$idpos=$oSession->VSidpos;

		$val_por_mo = 0;
		$val_por_co = 0;
		$porcentaje_mo = 0;
		$porcentaje_co = 0;
		$por_pos = 0;
		$por_ajuste = 0;
		$ganancia = 0;
		$tarifa = 0;
		$tipo_dia = 0;


		if($servicio == 6){

			$sql = "SELECT bp.id_pos FROM bolsas_puntos AS bp WHERE bp.id_pos = $idpos AND bp.id = $id_bolsa AND bp.id_proveedor = $id_pro AND saldo_incentivo > 0";
			$res_pos = $this->BD->consultar($sql);

			if($this->BD->numreg($res_pos) == 0){

				$sql = "SELECT porcen_movistar,porcen_comision,porcen_pos,distri_config FROM {$GLOBALS["BD_NAME"]}.recargas__config WHERE id_proveedor = $id_pro";
				$res_config=$this->BD->consultar($sql);

				if($this->BD->numreg($res_config) > 0){

					$sql = "SELECT {$GLOBALS["BD_NAME"]}.tarifa_promocion_punto($idpos,1) AS porcen_pos";
	        		$res_conf = $this->BD->consultar($sql);

	        		$por_pos=$res_conf->fields["porcen_pos"];

					/*if($res_config->fields["distri_config"] == 0){
		        		$por_pos = $res_config->fields["porcen_pos"];
		        	}else{

		        		// Se consulta por el porcentaje configurado por el distribuidor

			        	$sql = "SELECT porcen_pos FROM {$GLOBALS["BD_NAME"]}.recargas__config_distri WHERE id_distri = $idDistri AND id_proveedor = $id_pro";
			        	$res_conf2 = $this->BD->consultar($sql);

			        	if($this->BD->numreg($res_conf2) > 0){
			        		$por_pos = $res_conf2->fields["porcen_pos"];
			        	}

		        	}*/

					if($res_config->fields["porcen_movistar"] > 0){
						$porcentaje_mo = $res_config->fields["porcen_movistar"];
					}

					if($res_config->fields["porcen_comision"] > 0){
						$porcentaje_co = $res_config->fields["porcen_comision"];
					}
				}

				if($porcentaje_mo > $por_pos){

					$por_ajuste = $porcentaje_mo - $por_pos;

					$val_por_mo = ($valor * $por_ajuste) / 100;
				}

				$val_por_co = ($valor * $porcentaje_co) / 100;

			}

		}

		$sql = "SELECT paquetes FROM operadores WHERE id = $servicio AND estado = 1";
		$res_oper = $this->BD->consultar($sql);

		if($this->BD->numreg($res_oper)>0){

			if($res_oper->fields["paquetes"] == 1){

				$sql = "SELECT ganancia,tipo FROM tarifas_especiales WHERE id_paquete = $itemservicio AND ((tipo = 1 AND dia_semana = (ELT(WEEKDAY(CURDATE()) + 1, '1', '2', '3', '4', '5', '6', '7'))) OR (tipo = 0 AND fecha_promo = CURDATE())) AND fecha_ini <= CURDATE() AND fecha_fin >= CURDATE() AND id_proveedor = $id_pro";
				$res_paque=$this->BD->consultar($sql);

				if($this->BD->numreg($res_paque) == 0){

					$sql="SELECT pp.ganancia FROM paquetes_proveedores AS pp WHERE pp.id_paquete = $itemservicio AND pp.id_prov = $id_pro AND pp.id_operador = $servicio";
					$res_paque=$this->BD->consultar($sql);

					if($this->BD->numreg($res_paque) > 0){
						$ganancia = $res_paque->fields["ganancia"];
					}
				}else{
					$tarifa = 1;
					$tipo_dia = $res_paque->fields["tipo"];
					$ganancia = $res_paque->fields["ganancia"];
				}
			}

		}


		return array("ajuste" => $val_por_mo,"comision" => $val_por_co,"ganancia" => $ganancia,"tarifa" => $tarifa,"tipo_dia" => $tipo_dia);

	}

	public function RecarServs($servicio,$id_pro,$valor,$movil,$itemservicio,$key_pes,$id_bolsa)
    {

    	$idpos=0;
		$id_distri=0;
		$token=0;

		if(isset($_SESSION[$GLOBALS["SESION_POS"]])){

			$oSession = unserialize($_SESSION[$GLOBALS["SESION_POS"]]);
			$idpos=$oSession->VSidpos;
			$id_distri=$oSession->VSid_distri;
			$token=$oSession->VStoken;
		}else{
			return array("id" => -1, "msg" => "Tu sesión ha cerrado, por favor inica sesión nuevamente y vuelve a intentarlo", "regs" => 0);
		}

		$id_paquete = 0;
		$valor_recar = 0;
		$valor_inc = 0;
		$ganancia = 0;
		$operador = 0;
		$val_por_mo = 0;
		$val_por_co = 0;
		$band_incent = false;
		$band_saldo = false;
		$band_recar_mo = false;
		$por_pro_mo = 0;
		$por_pro_otro = 0;
		$cod_movilway = 0;
		$recarga = array();
		$ruta = "";
		$ean = 0;

	    if(!$this->is_data($servicio,1) || !$this->is_data($valor,1) || !$this->is_data($movil,1) || !$this->is_data($itemservicio,1) || !$this->is_data($id_pro,1)){
	    	return array("id" => -1, "msg" => "Error al realizar la recarga, valores no permitidos", "regs" => 0);
	    }

	    if(!isset($_SESSION["key_trans"])){
	    	return array("id" => -1, "msg" => "No es posible que puedas utilizar este servicio", "regs" => 0);
	    }

	    if(!strcmp($key_pes,$_SESSION["key_trans"]) == 0){
	    	return array("id" => -1, "msg" => "No es posible que puedas utilizar este servicio", "regs" => 0);
	    }

	    $sql = "SELECT rud.id,rud.porcentaje_mo,rud.porcentaje_otros FROM bolsas_puntos AS bp INNER JOIN {$GLOBALS["BD_NAME"]}.recargas__usudistri_prov AS rud ON (rud.id = bp.id_bolsa AND rud.id_distri = bp.id_distri AND rud.id_proveedor = bp.id_proveedor AND rud.estado = 1)  WHERE bp.id = $id_bolsa AND bp.id_pos = $idpos";
		$res_pro = $this->BD->devolver_array($sql);

		if(count($res_pro)==0){
			return array("id" => -1, "msg" => "No es posible que puedas utilizar este servicio, error del proveedor", "regs" => 0);
		}

		$id_bolsa_prin = $res_pro[0]["id"];
		$por_pro_mo = $res_pro[0]["porcentaje_mo"];
		$por_pro_otro = $res_pro[0]["porcentaje_otros"];

    	$sql = "SELECT timeout,codigo,valor_min,valor_max,paquetes FROM operadores WHERE id = $servicio AND estado = 1";
		$res_oper = $this->BD->consultar($sql);

		if($this->BD->numreg($res_oper) > 0){

			$sql = "SELECT codigo,valor_min,valor_max,timeout,ean FROM operadores_proveedor WHERE id_operador = $servicio AND id_proveedor = $id_pro";
			$res_code=$this->BD->consultar($sql);
			if($this->BD->numreg($res_code)>0){
				$timeout = $res_code->fields["timeout"];
				$operador = $res_code->fields["codigo"];
				$ean = $res_code->fields["ean"];
				if($valor < $res_code->fields["valor_min"] && $res_oper->fields["paquetes"] == 0){
					return array("id" => -1, "msg" => "El valor minimo de la recarga es de ".number_format($res_code->fields["valor_min"]), "regs" => 0);
				}

				if($valor > $res_code->fields["valor_max"] && $res_oper->fields["paquetes"] == 0){
					return array("id" => -1, "msg" => "El valor maximo de la recarga es de ".number_format($res_code->fields["valor_max"]), "regs" => 0);
				}
			}

			$band_paquetes = $res_oper->fields["paquetes"];

			if($band_paquetes == 1){
				$objeto_paquete = $this->consultar_paquete($itemservicio,$servicio,$valor,$id_pro);
				$operador = $objeto_paquete["codigo"];
				$id_paquete = $objeto_paquete["id_paquete"];
				$ganancia = $objeto_paquete["ganancia"];
				$valor = $objeto_paquete["valor"];
				$ean = $objeto_paquete["ean"];
			}

			if($_SESSION["venta"]==0){
				$_SESSION["venta"]=1;

				if($servicio == "" && $movil == "" && ($valor==0 || $valor =="")){
					return array("id" => -1, "msg" => "Error, hay campos que se encuentran vacíos", "regs" => 0);
				}

				$sql = "SELECT bp.saldo,bp.saldo_incentivo,bp.id_distri,rud.cod_movilway FROM bolsas_puntos AS bp INNER JOIN {$GLOBALS["BD_NAME"]}.recargas__usudistri_prov AS rud ON (rud.id = bp.id_bolsa AND rud.id_distri = bp.id_distri AND rud.id_proveedor = bp.id_proveedor AND rud.estado = 1)  WHERE bp.id = $id_bolsa AND bp.id_pos = $idpos";
				$saldo_punto=$this->BD->consultar($sql);

				if($this->BD->numreg($saldo_punto) == 0){
					return array("id" => -1, "msg" => "No es posible que puedas realizar la recarga porque ha ocurrido un error en el proceso", "regs" => 0);
				}

				$idDistri = $saldo_punto->fields["id_distri"];
				$bolsaanterior = $saldo_punto->fields["saldo"];
				$bolsaanterior_inc = $saldo_punto->fields["saldo_incentivo"];
				$cod_movilway = $saldo_punto->fields["cod_movilway"];

				if($bolsaanterior_inc > 0){
					$band_incent = true;
				}

				$sql = "SELECT zona,territorio,id_distri,id_regional FROM puntos WHERE idpos=$idpos";
				$cupo_punto=$this->BD->consultar($sql);

				$sql = "SELECT token FROM token_puntos WHERE id_pos = $idpos";
				$res_exits_to = $this->BD->consultar($sql);

				if($this->BD->numreg($res_exits_to) == 0){
					return array("id" => -2, "msg" => "No es posible que puedas realizar recargas porque es necesario que vuelvas a iniciar sesión", "regs" => 0);
				}

				if(strcmp($token,$res_exits_to->fields["token"]) != 0){
					return array("id" => -2, "msg" => "No es posible que puedas utilizar este servicio. Has iniciado sesión en otro computador", "regs" => 0);
				}

				if($this->BD->numreg($cupo_punto) > 0){

	        		$terri_pos = $cupo_punto->fields["territorio"];
	        		$zona_pos = $cupo_punto->fields["zona"];
	        		$reg_pos = $cupo_punto->fields["id_regional"];

            		/*if($id_distri != $idDistri){
						return array("id" => -1, "msg" => "Ocurrió un inconveniente con los permisos asignados al distribuidor", "regs" => 0);
					}*/

					$horacontul = 0;
					$hora = date("H:i:s");

					$sql="SELECT horaregistro FROM venta__servicios WHERE id=(SELECT MAX(id) FROM venta__servicios WHERE movil = $movil and fecha = CURDATE() and id_punto = $idpos)";
					$resultdatm=$this->BD->consultar($sql);

					$horaactual=$this->segundos($hora);

					if($this->BD->numreg($resultdatm)!=0){
						$horacontul = $this->segundos($resultdatm->fields["horaregistro"]);
					}

					if(intval($horaactual-$horacontul)<30){

						return array("id" => -1, "msg" => "Error, Solo se permite vender a una linea cada 30 segundos", "regs" => 0);
					}

					$sql="SELECT fnidentificador() as id";
					$res2=$this->BD->consultar($sql);
					$transactionId=$res2->fields["id"];

					//$transactionId=$this->fnidentificador();

					if($transactionId == ""){
						return array("id" => -1, "msg" => "Error al generar el identificador", "regs" => 0);
					}

					$sql = "SELECT folder FROM proveedores WHERE id = $id_pro AND (folder IS NULL OR folder != '')";
					$res_fol_pro=$this->BD->consultar($sql);

					if($this->BD->numreg($res_fol_pro)>0){
						$ruta = "../../modulos/webservice/".$res_fol_pro->fields["folder"]."/recarga.php";
					}else{
						return array("id" => -1, "msg" => "Error al realizar la recarga, el proveedor no existe", "regs" => 0);
					}

					if(!file_exists($ruta)){
						return array("id" => -1, "msg" => "Error al realizar la recarga, el fichero no existe", "regs" => 0);
					}

    				$sql="INSERT INTO venta__servicios (fecha,horaregistro,movil,id_punto,operador,id_item_servicio,valor,saldo_anterior,saldo_anterior_incentivo,transactionid,id_distri,zona,territorio,regional,venta_val,id_proveedor,id_bolsa_punto) VALUES (CURDATE(),CURTIME(),$movil,$idpos,$servicio,$id_paquete,$valor,$bolsaanterior,$bolsaanterior_inc,'$transactionId',$idDistri,$zona_pos,$terri_pos,$reg_pos,'$key_pes',$id_pro,$id_bolsa)";
					$resultven=$this->BD->consultar($sql);

					if($resultven){

						$ultimoid=mysql_insert_id();

						//// Metodo para descontar el saldo del punto
						$descontar_saldo_pos = $this->descontar_saldo_pos($idpos,$valor,$band_incent,$servicio,$bolsaanterior_inc,$id_bolsa);

						$band_saldo = $descontar_saldo_pos["band_saldo"];
						$valor_recar = $descontar_saldo_pos["valor_recar"];
						$valor_inc = $descontar_saldo_pos["valor_inc"];
						$band_recar_mo = $descontar_saldo_pos["band_recar_mo"];

						//// Valida que el saldo del punto es suficiente para realizar la recarga
						if($band_saldo){

							$sql = "INSERT INTO token_ventas (`id_pos`, `token`, `id_venta`) VALUES ($idpos,'$key_pes',$ultimoid)";

							$res_val_venta=$this->BD->consultar($sql);

							if(!$res_val_venta){

								$sql = "UPDATE venta__servicios SET estado = 2, horatransaccion = CURTIME(), mens_error='No se inserto la validacion' WHERE id=$ultimoid";
								$resultven_up=$this->BD->consultar($sql);

								if($this->BD->numupdate() == 0){
									$resultven_up2=$this->BD->consultar($sql);
									if($this->BD->numupdate() == 0){
										$resultven_up3=$this->BD->consultar($sql);
									}
								}

								return array("id" => -1, "msg" => "Error al realizar la recarga, no se insertó la validación", "regs" => 0);

							}

							$timeout = $timeout * 1000;

							require($ruta);

							$recarga = Recarga($valor,$movil,$operador,$transactionId,$timeout,$ultimoid,$bolsaanterior,$key_pes,$servicio,$bolsaanterior_inc,$valor_recar,$valor_inc,$id_pro,$ean,$token,$cod_movilway,$idDistri);

							//// Valida que la funcion de recarga devuelva un entero o que exista la llave estado
							if(isset($recarga["estado"]) && intval($recarga["estado"]) > 0){

								$msg = $recarga["msg"];

								//// Valida que la recarga se ha enviado correctamente
								if($recarga["estado"]==1){

									//// Valida que la recarga sea de movistar
									if($band_recar_mo){

								       	$return_ajueste = $this->ajustes($idpos,$idDistri,$terri_pos,$zona_pos,$reg_pos,$valor_recar,$ultimoid,$id_pro,$por_pro_mo,$id_bolsa,$id_bolsa_prin);

								       	$val_por_mo = $return_ajueste["val_por_mo"];
										$val_por_co = $return_ajueste["val_por_co"];
									}else{
										$this->acredita_distri($idpos,$idDistri,$valor_recar,$id_pro,$por_pro_otro,$id_bolsa_prin);
									}

									//// Valida que la recarga sea un paquete
									if($band_paquetes == 1){
										if($ganancia > 0){

											$this->ganancia_paquete($idpos,$idDistri,$terri_pos,$zona_pos,$reg_pos,$ultimoid,$ganancia,$id_pro,$id_paquete,$id_bolsa,$id_bolsa_prin);

										}
									}

									$sql = "DELETE FROM token_ventas WHERE id_pos = $idpos AND token = '$key_pes' AND id_venta = $ultimoid";
									$res_val_venta=$this->BD->consultar($sql);

									if(!$res_val_venta){
										$res_val_venta2=$this->BD->consultar($sql);
										if(!$res_val_venta2){
											$res_val_venta3=$this->BD->consultar($sql);
										}
									}

									return array("id" => 1, "msg" => '', "regs" => $ultimoid,"ajuste"=>$val_por_mo,"comision"=>$val_por_co,"ganancia"=>$ganancia);

								//// Valida si la funcion recarga devuelve un error
								}elseif($recarga["estado"]==4)
								{

									$this->reversar_saldo_pos($idpos,$valor_inc,$valor_recar,$band_incent,$servicio,$id_bolsa);

									$sql = "DELETE FROM token_ventas WHERE id_pos = $idpos AND token = '$key_pes' AND id_venta = $ultimoid";
									$res_val_venta=$this->BD->consultar($sql);

									if(!$res_val_venta){
										$res_val_venta2=$this->BD->consultar($sql);
										if(!$res_val_venta2){
											$res_val_venta3=$this->BD->consultar($sql);
										}
									}

									return array("id" => -1, "msg" => $msg, "regs" => 0);
								}

							}else
							{

								$this->reversar_saldo_pos($idpos,$valor_inc,$valor_recar,$band_incent,$servicio,$id_bolsa);

								$sql = "DELETE FROM token_ventas WHERE id_pos = $idpos AND token = '$key_pes' AND id_venta = $ultimoid";
								$res_val_venta=$this->BD->consultar($sql);

								if(!$res_val_venta){
									$res_val_venta2=$this->BD->consultar($sql);
									if(!$res_val_venta2){
										$res_val_venta3=$this->BD->consultar($sql);
									}
								}

								return array("id" => -1, "msg" => "Error, no se pudo realizar la recarga. Intentalo de nuevo mas tarde", "regs" => 0);
							}

						}else
						{

							$sql = "UPDATE venta__servicios SET estado = 2, horatransaccion = CURTIME(), mens_error='El punto se quedo sin saldo' WHERE id=$ultimoid";
							$resultven_up=$this->BD->consultar($sql);

							if($this->BD->numupdate() == 0){
								$resultven_up2=$this->BD->consultar($sql);
								if($this->BD->numupdate() == 0){
									$resultven_up3=$this->BD->consultar($sql);
								}
							}

							return array("id" => -1, "msg" => "En este momento no cuentas con saldo disponible para realizar la recarga", "regs" => 0);
						}

					}else
					{
						return array("id" => -1, "msg" => "Error al generar la inserción de la venta", "regs" => 0);
					}

				}else{
					return array("id" => -1, "msg" => "No es posible que puedas utilizar este servicio", "regs" => 0);
				}


			}else{
				return array("id" => -1, "msg" => "Lo sentimos, No puedes enviar dos recargas a la vez", "regs" => 0);
			}
		}else{
			return array("id" => -1, "msg" => "No es posible que puedas utilizar este servicio", "regs" => 0);
		}

	}

	public function consultar_paquete($itemservicio,$servicio,$valor,$id_pro){

		$operador = 0;
		$id_paquete = 0;
		$ganancia = 0;
		$ean = 0;

		$sql="SELECT pp.valor_venta, pp.codigo, pp.ganancia, pp.ean FROM paquetes_proveedores AS pp WHERE pp.id_paquete = $itemservicio AND pp.id_operador = $servicio AND pp.id_prov = $id_pro";
		$res_paque=$this->BD->consultar($sql);

		if($this->BD->numreg($res_paque) > 0){

			if($res_paque->fields["valor_venta"] != $valor){
				$valor = $res_paque->fields["valor_venta"];
			}

			$operador = $res_paque->fields["codigo"];
			$id_paquete = $itemservicio;
			$ganancia = $res_paque->fields["ganancia"];
			$ean = $res_paque->fields["ean"];

			$sql = "SELECT ganancia,tipo FROM tarifas_especiales WHERE id_paquete = $itemservicio AND id_proveedor = $id_pro AND ((tipo = 1 AND dia_semana = (ELT(WEEKDAY(CURDATE()) + 1, '1', '2', '3', '4', '5', '6', '7'))) OR (tipo = 0 AND fecha_promo = CURDATE())) AND fecha_ini <= CURDATE() AND fecha_fin >= CURDATE()";

			$res_paque2=$this->BD->consultar($sql);

			if($this->BD->numreg($res_paque2) > 0){

				$ganancia = $res_paque2->fields["ganancia"];

			}
		}

		return array("codigo"=>$operador,"id_paquete"=>$id_paquete,"ganancia"=>$ganancia,"valor"=>$valor,"ean"=>$ean);

	}

	public function descontar_saldo_pos($idpos,$valor,$band_incent,$servicio,$bolsaanterior_inc,$id_bolsa){

		$valor_inc = 0;
		$valor_recar = $valor;
		$band_saldo = false;
		$band_recar_mo = false;

		//// Se valida que se tenga saldo incentivo ye que la recarga es de movistar
		if($band_incent && $servicio == 6){

			$valor_inc = $valor;
			$valor_recar = 0;

			// Se realiza el descuento del saldo incentivo
			$sql_up1 = "UPDATE bolsas_puntos AS bp SET bp.saldo_incentivo = ROUND((bp.saldo_incentivo-$valor),2) WHERE bp.id = $id_bolsa AND bp.id_pos = $idpos AND bp.saldo_incentivo >= $valor";
			//$sql_up1="UPDATE puntos SET saldo_incentivo = ROUND((saldo_incentivo-$valor),2) WHERE idpos=$idpos AND saldo_incentivo >= $valor";
			$res_up_pos=$this->BD->consultar($sql_up1);

			//Si la bolsa incentivo no es mayor al valor de la venta:
			if($this->BD->numupdate() == 0){

				// El valor incentivo queda como el total de la bolsa incentivo
				$valor_inc = $bolsaanterior_inc;
				// El valor de la recarga normal queda como el total del valor de la recarga menos la bolsa incentivo
				$valor_recar = $valor - $bolsaanterior_inc;

				// Se realiza el descuento del saldo normal
				$sql_up2 = "UPDATE bolsas_puntos AS bp SET bp.saldo = ROUND((bp.saldo-$valor_recar),2) WHERE bp.id = $id_bolsa AND bp.id_pos = $idpos AND bp.saldo >= $valor_recar";
				//$sql_up2="UPDATE puntos SET saldo = ROUND((saldo-$valor_recar),2) WHERE idpos=$idpos AND saldo >= $valor_recar";
				$res_up_pos_sal=$this->BD->consultar($sql_up2);

				if($this->BD->numupdate() > 0){

					// Se realiza el descuento del saldo incentivo
					$sql_up1 = "UPDATE bolsas_puntos AS bp SET bp.saldo_incentivo = ROUND((bp.saldo_incentivo-$valor_inc),2) WHERE bp.id = $id_bolsa AND bp.id_pos = $idpos AND bp.saldo_incentivo >= $valor_inc";
					//$sql_up1="UPDATE puntos SET saldo_incentivo = ROUND((saldo_incentivo-$valor_inc),2) WHERE idpos=$idpos AND saldo_incentivo >= $valor_inc";
					$res_up_pos=$this->BD->consultar($sql_up1);

					if($this->BD->numupdate() == 0){

						// Si no se realiza el descuento del saldo incentivo se reversa el saldo normal
						$sql_up2 = "UPDATE bolsas_puntos AS bp SET bp.saldo = ROUND((bp.saldo+$valor_recar),2) WHERE bp.id = $id_bolsa AND bp.id_pos = $idpos";
						//$sql_up2="UPDATE puntos SET saldo = ROUND((saldo+$valor_recar),2) WHERE idpos=$idpos";
						$res_up_pos_sal=$this->BD->consultar($sql_up2);

						if($this->BD->numupdate() == 0){
							$res_up_pos_sal2=$this->BD->consultar($sql_up2);
							if($this->BD->numupdate() == 0){
								$res_up_pos_sal3=$this->BD->consultar($sql_up2);
							}
						}

					}else{
						$band_saldo = true;
					}

					$band_recar_mo = true;
				}

			}else{
				$band_saldo = true;
			}

		}else{

			//// Se realiza la resta con el valor de la venta de forma normal

			$sql_up1 = "UPDATE bolsas_puntos AS bp SET bp.saldo = ROUND((bp.saldo-$valor),2) WHERE bp.id = $id_bolsa AND bp.id_pos = $idpos AND bp.saldo >= $valor";
			//$sql_up1="UPDATE puntos SET saldo = ROUND((saldo-$valor),2) WHERE idpos=$idpos AND saldo >= $valor";
			$res_up_pos=$this->BD->consultar($sql_up1);

			if($this->BD->numupdate() > 0){
				$band_saldo = true;

				if($servicio == 6){
					$band_recar_mo = true;
				}
			}

		}

		return array("valor_inc"=>$valor_inc,"valor_recar"=>$valor_recar,"band_saldo"=>$band_saldo,"band_recar_mo"=>$band_recar_mo);

	}

	private function reversar_saldo_pos($idpos,$valor_inc,$valor_recar,$band_incent,$servicio,$id_bolsa){

		if($band_incent && $servicio == 6){

			$reversar_saldo = "";

			if($valor_recar == 0){
				$reversar_saldo = "bp.saldo_incentivo = ROUND((bp.saldo_incentivo+$valor_inc),2)";
			}else{
				$reversar_saldo = "bp.saldo_incentivo = ROUND((bp.saldo_incentivo+$valor_inc),2),bp.saldo = ROUND((bp.saldo+$valor_recar),2)";
			}

			$sql_up1 = "UPDATE bolsas_puntos AS bp SET $reversar_saldo  WHERE bp.id = $id_bolsa AND bp.id_pos = $idpos";
			//$sql_up1="UPDATE puntos SET $reversar_saldo WHERE idpos=$idpos";
			$res_up_pos=$this->BD->consultar($sql_up1);

			if($this->BD->numupdate() == 0){
				$res_up_pos2=$this->BD->consultar($sql);
				if($this->BD->numupdate() == 0){
					$res_up_pos3=$this->BD->consultar($sql);
				}
			}

		}else{

			//// Se realiza la resta con el valor de la venta de forma normal
			$sql = "UPDATE bolsas_puntos AS bp SET bp.saldo = ROUND((bp.saldo+$valor_recar),2) WHERE bp.id = $id_bolsa AND bp.id_pos = $idpos";
			//$sql="UPDATE puntos SET saldo = ROUND((saldo+$valor_recar),2) WHERE idpos=$idpos";
			$res_up_pos=$this->BD->consultar($sql);

			if($this->BD->numupdate() == 0){
				$res_up_pos2=$this->BD->consultar($sql);
				if($this->BD->numupdate() == 0){
					$res_up_pos3=$this->BD->consultar($sql);
				}
			}

		}

	}

	public function ajustes($idpos,$idDistri,$terri_pos,$zona_pos,$reg_pos,$valor,$ultimoid,$id_pro,$por_pro,$id_bolsa,$id_bolsa_prin){

		$val_por_mo = 0;
		$val_por_co = 0;
		$porcentaje_mo = 0;
		$porcentaje_co = 0;
		$por_pos = 0;
		$por_ajuste = 0;

		$sql = "SELECT porcen_movistar,porcen_comision,porcen_pos,distri_config FROM {$GLOBALS["BD_NAME"]}.recargas__config WHERE id_proveedor = $id_pro";
		$res_config=$this->BD->consultar($sql);

		if($this->BD->numreg($res_config) > 0){

			$sql = "SELECT {$GLOBALS["BD_NAME"]}.tarifa_promocion_punto($idpos,1) AS porcen_pos";
    		$res_conf = $this->BD->consultar($sql);

    		$por_pos=$res_conf->fields["porcen_pos"];

			/*if($res_config->fields["distri_config"] == 0){
        		$por_pos = $res_config->fields["porcen_pos"];
        	}else{

        		// Se consulta por el porcentaje configurado por el distribuidor

	        	$sql = "SELECT porcen_pos FROM {$GLOBALS["BD_NAME"]}.recargas__config_distri WHERE id_distri = $idDistri AND id_proveedor = $id_pro";
	        	$res_conf2 = $this->BD->consultar($sql);

	        	if($this->BD->numreg($res_conf2) > 0){
	        		$por_pos = $res_conf2->fields["porcen_pos"];
	        	}

        	}*/

			if($res_config->fields["porcen_movistar"] > 0){
				$porcentaje_mo = $res_config->fields["porcen_movistar"];
			}

			if($res_config->fields["porcen_comision"] > 0){
				$porcentaje_co = $res_config->fields["porcen_comision"];
			}
		}

		if($porcentaje_mo > $por_pos){

			$por_ajuste = $porcentaje_mo - $por_pos;

			$val_por_mo = ($valor * $por_ajuste) / 100;
		}

		$val_por_co = ($valor * $porcentaje_co) / 100;

		$second = 1;

		if($val_por_mo > 0){

			$sql = "SELECT bp.saldo AS cupo FROM bolsas_puntos AS bp WHERE bp.id = $id_bolsa AND bp.id_pos = $idpos";
			//$sql = "SELECT saldo as cupo FROM puntos WHERE idpos=$idpos";
			$cupo_punto2=$this->BD->consultar($sql);

			$bolsaanterior2 = $cupo_punto2->fields["cupo"];

			$sql = "UPDATE bolsas_puntos AS bp SET bp.saldo = ROUND((bp.saldo+$val_por_mo),2) WHERE bp.id = $id_bolsa AND bp.id_pos = $idpos";
			//$sql="UPDATE puntos SET saldo = ROUND((saldo+($val_por_mo)),2) WHERE idpos=$idpos";
			$resultven_up2=$this->BD->consultar($sql);

			if($this->BD->numupdate() == 0){
				$resultven_up3=$this->BD->consultar($sql);
				if($this->BD->numupdate() == 0){
					$resultven_up4=$this->BD->consultar($sql);
				}
			}

			$sql = "SELECT saldo FROM {$GLOBALS["BD_NAME"]}.recargas__usudistri_prov WHERE id = $id_bolsa_prin AND id_proveedor = $id_pro";
			$res_dis = $this->BD->consultar($sql);

			$salto_ant_dis = $res_dis->fields["saldo"];

			$sql = "UPDATE {$GLOBALS["BD_NAME"]}.recargas__usudistri_prov SET saldo = ROUND((saldo - $val_por_mo),2) WHERE id = $id_bolsa_prin AND id_proveedor = $id_pro";

			$resultven_dis=$this->BD->consultar($sql);

			if($this->BD->numupdate() == 0){
				$resultven_dis2=$this->BD->consultar($sql);
				if($this->BD->numupdate() == 0){
					$resultven_dis3=$this->BD->consultar($sql);
				}
			}

			$sql = "INSERT INTO {$GLOBALS["BD_NAME"]}.recargas__venta_saldo (id_pos, valor_venta, valor_acreditado, porcentaje, saldo_anterior, saldo_final, fecha, hora, id_distri, zona, territorio, regional,tipo,saldo_ante_distri,saldo_final_distri,id_recarga_pos,id_proveedor,id_bolsa_pos,id_bolsa_distri) VALUES ($idpos,ROUND($val_por_mo,2),ROUND($val_por_mo,2),$por_ajuste,$bolsaanterior2,ROUND(($bolsaanterior2+$val_por_mo),2),CURDATE(),ADDTIME(CURTIME(),'00:00:01'),$idDistri,$zona_pos,$terri_pos,$reg_pos,1,$salto_ant_dis,ROUND(($salto_ant_dis-$val_por_mo),2),$ultimoid,$id_pro,$id_bolsa,$id_bolsa_prin)";

	       	$res_venta_s1 = $this->BD->consultar($sql);

	       	if(!$res_venta_s1){
				$res_venta_s2=$this->BD->consultar($sql);
				if(!$res_venta_s2){
					$res_venta_s3=$this->BD->consultar($sql);
				}
			}

	       	$second = 2;

       	}

		if($val_por_co > 0){

			$sql = "SELECT bp.saldo AS cupo FROM bolsas_puntos AS bp WHERE bp.id = $id_bolsa AND bp.id_pos = $idpos AND bp.id_proveedor = $id_pro";
			//$sql = "SELECT saldo as cupo FROM puntos WHERE idpos=$idpos";
			$cupo_punto2=$this->BD->consultar($sql);

			$bolsaanterior2 = $cupo_punto2->fields["cupo"];

			$sql = "UPDATE bolsas_puntos AS bp SET bp.saldo = ROUND((bp.saldo+$val_por_co),2) WHERE bp.id = $id_bolsa AND bp.id_pos = $idpos AND bp.id_proveedor = $id_pro";
			//$sql="UPDATE puntos SET saldo = ROUND((saldo+($val_por_co)),2) WHERE idpos=$idpos";
			$resultven_up2=$this->BD->consultar($sql);

			if($this->BD->numupdate() == 0){
				$resultven_up3=$this->BD->consultar($sql);
				if($this->BD->numupdate() == 0){
					$resultven_up4=$this->BD->consultar($sql);
				}
			}

			$sql = "SELECT saldo FROM {$GLOBALS["BD_NAME"]}.recargas__usudistri_prov WHERE id = $id_bolsa_prin AND id_proveedor = $id_pro";
			$res_dis = $this->BD->consultar($sql);

			$salto_ant_dis = $res_dis->fields["saldo"];

			$sql = "UPDATE {$GLOBALS["BD_NAME"]}.recargas__usudistri_prov SET saldo = ROUND((saldo - $val_por_co),2) WHERE id = $id_bolsa_prin AND id_proveedor = $id_pro";

			$resultven_dis=$this->BD->consultar($sql);

			if($this->BD->numupdate() == 0){
				$resultven_dis2=$this->BD->consultar($sql);
				if($this->BD->numupdate() == 0){
					$resultven_dis3=$this->BD->consultar($sql);
				}
			}

	       	$sql = "INSERT INTO {$GLOBALS["BD_NAME"]}.recargas__venta_saldo (id_pos, valor_venta, valor_acreditado, porcentaje, saldo_anterior, saldo_final, fecha, hora, id_distri, zona, territorio, regional,tipo,saldo_ante_distri,saldo_final_distri,id_recarga_pos,id_proveedor,id_bolsa_pos,id_bolsa_distri) VALUES ($idpos,ROUND($val_por_co,2),ROUND($val_por_co,2),$porcentaje_co,$bolsaanterior2,ROUND(($bolsaanterior2+$val_por_co),2),CURDATE(),ADDTIME(CURTIME(),'00:00:0".$second."'),$idDistri,$zona_pos,$terri_pos,$reg_pos,2,$salto_ant_dis,ROUND(($salto_ant_dis-$val_por_co),2),$ultimoid,$id_pro,$id_bolsa,$id_bolsa_prin)";

	       	$res_venta_s2 = $this->BD->consultar($sql);

	       	if(!$res_venta_s2){
				$res_venta_s3=$this->BD->consultar($sql);
				if(!$res_venta_s3){
					$res_venta_s4=$this->BD->consultar($sql);
				}
			}

		}

		$this->acredita_distri($idpos,$idDistri,$valor,$id_pro,$por_pro,$id_bolsa_prin);

		return array("val_por_mo"=>$val_por_mo,"val_por_co"=>$val_por_co);

	}

	public function acredita_distri($idpos,$idDistri,$valor,$id_pro,$por_pro,$id_bolsa_prin){

		if($por_pro>0){

			$val_por_acre = ($valor * $por_pro) / 100;

			$sql = "SELECT saldo FROM {$GLOBALS["BD_NAME"]}.recargas__usudistri_prov WHERE id = $id_bolsa_prin AND id_proveedor = $id_pro";
			$res_dis = $this->BD->consultar($sql);

			$salto_ant_dis = $res_dis->fields["saldo"];

			$sql = "UPDATE {$GLOBALS["BD_NAME"]}.recargas__usudistri_prov SET saldo = ROUND((saldo + $val_por_acre),2) WHERE id = $id_bolsa_prin AND id_proveedor = $id_pro";

			$resultven_dis=$this->BD->consultar($sql);

			if($this->BD->numupdate() == 0){
				$resultven_dis2=$this->BD->consultar($sql);
				if($this->BD->numupdate() == 0){
					$resultven_dis3=$this->BD->consultar($sql);
				}
			}

			$sql = "INSERT INTO {$GLOBALS["BD_NAME"]}.recargas__compras_distri (`id_distri`, `valor`, `valor_acreditado`, `saldo_anterior`, `saldo_final`, `fecha`, `hora`, `tipo_movimiento`, `fecha_ws`, `hora_ws`, `id_proveedor`) VALUES ($idDistri,ROUND(($val_por_acre),2),ROUND(($val_por_acre),2),ROUND(($salto_ant_dis),2),ROUND(($salto_ant_dis+$val_por_acre),2),CURDATE(),CURTIME(),5,CURDATE(),CURTIME(),$id_pro)";

			$result_acre_dis=$this->BD->consultar($sql);

			if(!$result_acre_dis){
				$result_acre_dis2=$this->BD->consultar($sql);
				if(!$result_acre_dis2){
					$result_acre_dis3=$this->BD->consultar($sql);
				}
			}
		}

	}

	public function ganancia_paquete($idpos,$idDistri,$terri_pos,$zona_pos,$reg_pos,$ultimoid,$ganancia,$id_pro,$id_paquete,$id_bolsa,$id_bolsa_prin){

		$sql = "SELECT bp.saldo AS cupo FROM bolsas_puntos AS bp  WHERE bp.id = $id_bolsa AND bp.id_pos = $idpos AND bp.id_proveedor = $id_pro";

    	//$sql = "SELECT saldo as cupo FROM puntos WHERE idpos=$idpos";
		$cupo_punto2=$this->BD->consultar($sql);

		$bolsaanterior2 = $cupo_punto2->fields["cupo"];

		$sql = "UPDATE bolsas_puntos AS bp SET bp.saldo = ROUND((bp.saldo+$ganancia),2) WHERE bp.id = $id_bolsa AND bp.id_pos = $idpos AND bp.id_proveedor = $id_pro";
		//$sql="UPDATE puntos SET saldo = ROUND((saldo+($ganancia)),2) WHERE idpos=$idpos";
		$resultven_up2=$this->BD->consultar($sql);

		if($this->BD->numupdate() == 0){
			$resultven_up3=$this->BD->consultar($sql);
			if($this->BD->numupdate() == 0){
				$resultven_up4=$this->BD->consultar($sql);
			}
		}

		$sql = "SELECT saldo FROM {$GLOBALS["BD_NAME"]}.recargas__usudistri_prov WHERE id = $id_bolsa_prin AND id_proveedor = $id_pro";
		$res_dis = $this->BD->consultar($sql);

		$salto_ant_dis = $res_dis->fields["saldo"];

		$sql = "UPDATE {$GLOBALS["BD_NAME"]}.recargas__usudistri_prov SET saldo = ROUND((saldo - $ganancia),2) WHERE id = $id_bolsa_prin AND id_proveedor = $id_pro";

		$resultven_dis=$this->BD->consultar($sql);

		if($this->BD->numupdate() == 0){
			$resultven_dis2=$this->BD->consultar($sql);
			if($this->BD->numupdate() == 0){
				$resultven_dis3=$this->BD->consultar($sql);
			}
		}

       	$sql = "INSERT INTO {$GLOBALS["BD_NAME"]}.recargas__venta_saldo (id_pos, valor_venta, valor_acreditado, porcentaje, saldo_anterior, saldo_final, fecha, hora, id_distri, zona, territorio, regional,tipo,saldo_ante_distri,saldo_final_distri,id_recarga_pos,id_paquete,id_proveedor,id_bolsa_pos,id_bolsa_distri) VALUES ($idpos,ROUND($ganancia,2),ROUND($ganancia,2),0,$bolsaanterior2,ROUND(($bolsaanterior2+$ganancia),2),CURDATE(),ADDTIME(CURTIME(),'00:00:01'),$idDistri,$zona_pos,$terri_pos,$reg_pos,3,$salto_ant_dis,ROUND(($salto_ant_dis-$ganancia),2),$ultimoid,$id_paquete,$id_pro,$id_bolsa,$id_bolsa_prin)";

       	$res_venta_s3 = $this->BD->consultar($sql);

       	if(!$res_venta_s3){
			$res_venta_s4=$this->BD->consultar($sql);
			if(!$res_venta_s4){
				$res_venta_s5=$this->BD->consultar($sql);
			}
		}

	}

	public function fnidentificador(){

		$sql = "SELECT identificador FROM tblidentificador";
		$res = $this->BD->consultar($sql);

		if($this->BD->numreg($res)==0){
			$identi = 1;
			$sql = "INSERT INTO tblidentificador (identificador) VALUES ($identi)";
			$res_in = $this->BD->consultar($sql);
		}else{
			$identi = $res->fiels["identificador"] + 1;
			$sql = "UPDATE tblidentificador SET identificador = $identi";
			$res_up = $this->BD->consultar($sql);
		}

		return $identi;

	}

	public function segundos($hora){
		list($h, $m, $s) = explode(':', $hora);
		return ($h * 3600) + ($m * 60) + $s;
	}

	public function ultimasTransac(){

		if(isset($_SESSION[$GLOBALS["SESION_POS"]])){

			$oSession = unserialize($_SESSION[$GLOBALS["SESION_POS"]]);
			$idpos=$oSession->VSidpos;
			$id_distri=$oSession->VSid_distri;

			$array_response = array();

			$sql = "SELECT vs.movil AS num_m,IF(op.id = 6 AND vs.valor > vs.saldo_anterior_incentivo,vs.valor-vs.saldo_anterior_incentivo,IF(op.id = 6 AND vs.valor <= vs.saldo_anterior_incentivo,0,vs.valor)) AS v_recarga,vs.fecha AS fecha_trans,vs.horatransaccion AS hora_trans,op.descripcion AS des_operador,IF(pt.descripcion IS NULL,'N/A',pt.descripcion) AS des_paquete,saldo_anterior AS s_ant,saldo_actual AS s_act,'Recarga' AS movi,0 AS porcen,1 AS movi_int,vs.saldo_anterior_incentivo AS s_ant_inc,vs.saldo_actual_incentivo AS s_act_inc,IF(op.id = 6 AND vs.valor > vs.saldo_anterior_incentivo,vs.saldo_anterior_incentivo,IF(op.id = 6 AND vs.valor <= vs.saldo_anterior_incentivo,vs.valor,0)) AS v_inc,IF(op.id = 6 AND vs.valor > vs.saldo_anterior_incentivo,vs.valor-vs.saldo_anterior_incentivo,IF(op.id = 6 AND vs.valor <= vs.saldo_anterior_incentivo,0,vs.valor)) AS v_recarga_des,rud.descripcion AS nom_bolsa

				FROM venta__servicios vs INNER JOIN operadores op ON (op.id = vs.operador) LEFT JOIN paquetigos pt ON (pt.id = vs.id_item_servicio AND pt.id_servicio = op.id)
					LEFT JOIN bolsas_puntos AS bp ON (vs.id_bolsa_punto = bp.id)
					LEFT JOIN {$GLOBALS["BD_NAME"]}.recargas__usudistri_prov AS rud ON (bp.id_bolsa = rud.id)
				 WHERE vs.id_punto=$idpos AND vs.estado = 1

				UNION ALL

				SELECT 0 AS num_m, valor_acreditado AS v_recarga, fecha AS fecha_trans, hora hora_trans,'Compra de saldo' AS des_operador, 'N/A' AS des_paquete,saldo_anterior AS s_ant, saldo_final AS s_act,IF(tipo = 0,'Compra',IF(tipo = 1,'Ajuste',IF(tipo = 2,'Comision',IF(tipo = 4,'Incentivo','Ajuste Paquete')))) AS movi,porcentaje AS porcen,IF(tipo = 0,2,IF(tipo = 1,3,4)) AS movi_int,0 AS s_ant_inc,0 AS s_act_inc,0 AS v_inc,0 AS v_recarga_des,rud.descripcion AS nom_bolsa
				FROM {$GLOBALS["BD_NAME"]}.recargas__venta_saldo
					LEFT JOIN bolsas_puntos AS bp ON (recargas__venta_saldo.id_bolsa_pos = bp.id)
					LEFT JOIN {$GLOBALS["BD_NAME"]}.recargas__usudistri_prov AS rud ON (bp.id_bolsa = rud.id)
				WHERE recargas__venta_saldo.id_pos = $idpos ORDER BY fecha_trans DESC,hora_trans DESC LIMIT 0,10";

			$res = $this->BD->devolver_array($sql);

			$this->BD->desconectar();

			return $res;

		}
	}


   function total_pendiente($idpos)
   {

		$fecha_hora = date("Y-m-d\TH:i:s");
		$fecha_hora_act = explode("T",$fecha_hora);
		$hora_act = $fecha_hora_act[1];
		$fecha_act = $fecha_hora_act[0];
		$nuevaHora = strtotime ( '-1 minute' , strtotime ( $hora_act ) ) ;
		$nuevaHora = date ( 'H:i:s' , $nuevaHora );

		$hora1 = strtotime($nuevaHora);
		$hora2 = strtotime($hora_act);

		if($hora1>$hora2){
			$fecha_act = strtotime ( '-1 day' , strtotime ( $fecha_act ) ) ;
			$fecha_act = date ( 'Y-m-d' , $fecha_act);
		}

		/*
		$sql = "SELECT id FROM venta__servicios WHERE id_punto = $idpos and estado = 0 and fecha = '$fecha_act' and (horaregistro >= '$nuevaHora' and horaregistro<='$hora_act')";
		$res_ven=$BD->consultar($sql);

		if($BD->numreg($res_ven)>1){
			$sql="UPDATE venta__servicios SET estado=3,horatransaccion=curtime(),mens_error='Error, transacciones pendientes' WHERE id=$ultimoid";
		    $resultupventa=$BD->consultar($sql);

			return 0;
		}else{
			return 1;
		}*/

		$sql = "SELECT sum(valor) total FROM venta__servicios WHERE id_punto = $idpos and estado = 0 and fecha = '$fecha_act' and (horaregistro >= '$nuevaHora' and horaregistro<='$hora_act')";
		$res_ven=$this->BD->consultar($sql);

		if($this->BD->numreg($res_ven)>0){
			 return doubleval($res_ven->fields["total"]);
		}else{
			 return 0;
		}

	}


   public function generar_token(){

      $cadena = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
      $longitudCadena=strlen($cadena);
      $code = "";

      for($i=1 ; $i<=15 ; $i++){
        //Definimos numero aleatorio entre 0 y la longitud de la cadena de caracteres-1
        $pos=rand(0,$longitudCadena-1);
        //Vamos formando la contrase–a en cada iteraccion del bucle, a–adiendo a la cadena $pass la letra correspondiente a la posicion $pos en la cadena de caracteres definida.
        $code.= substr($cadena,$pos,1);
      }

      return $code;

   }

   public function is_data($cadena,$op,$minimo = 0,$maximo = 0){
   		$cadena = rtrim($cadena);
   		if($op == 1){

   			$permitidos = "1234567890";
   			for ($i=0; $i<strlen($cadena); $i++){
				if (strpos($permitidos, substr($cadena,$i,1))===false){
					//no es válido;
					return false;
				}
			}

   		}
   		//$minimo = 0,$maximo = 0
   		if($minimo != 0 or $maximo != 0){
   			if(strlen($cadena) < $minimo || strlen($cadena) > $maximo){
   				return false;
	   		}
   		}

		return true;
   }

}

?>