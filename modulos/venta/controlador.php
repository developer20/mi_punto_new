<?php

include_once("../../config/mainController.php"); // Incluye el Controlador Principal
include_once("../../modulos/venta/modelo.php");	// Incluye el Modelo.
$controller = new mainController; // Instancia a la clase MainController
$modelo = new ventas(); // Instancia a la clase del modelo

try // Try, manejo de Errores
{
	$metodo = $_SERVER['REQUEST_METHOD'];
	$tipo_res = "";
	$response = null;
	// Se manejaran dos tipos JSON y HTML
	// Dependiendo del método de la petición ejecutaremos la acción correspondiente.
	// Por ahora solo POST, todas las llamadas se haran por POST
	$variables = $_POST;
	if(!isset($_POST['accion'])){echo "0"; return;} // Evita que ocurra un error si no manda accion.
	$accion = $variables['accion'];
		// Dependiendo de la accion se ejecutaran las tareas y se definira el tipo de respuesta.
		switch($accion) {
			case 'CargarBols':

				 $tipo_res = 'JSON'; //Definir tipo de respuesta;
				 $id_bolsa = $variables["id_bolsa"];
				 $response = $modelo->CargarBols($id_bolsa);
			break;
			case 'CargarOpers':

				 $tipo_res = 'JSON'; //Definir tipo de respuesta;
				 $tipo = $variables["tipo"];
				 $id_bolsa = $variables["id_bolsa"];
				 $response = $modelo->CargarOpers($tipo,$id_bolsa);
			break;
			case 'CargarCatePaquete':

				 $tipo_res = 'JSON'; //Definir tipo de respuesta;
				 $id_servicio = $variables["id_servicio"];
				 $val_paque = $variables["val_paque"];
				 $id_bolsa = $variables["id_bolsa"];
				 $response = $modelo->CargarCatePaquete($id_servicio,$val_paque,$id_bolsa);
			break;
			case 'CargarServs':

				 $tipo_res = 'JSON'; //Definir tipo de respuesta;
				 $id_servicio = $variables["id_servicio"];
				 $categoria = $variables["categoria"];
				 $id_pro = $variables["id_pro"];
				 $response = $modelo->CargarServs($id_servicio,$categoria,$id_pro);
			break;
			case 'consultar_porcent':

				 $tipo_res = 'JSON'; //Definir tipo de respuesta;

				 $servs = $variables["servs"];
				 $id_pro = $variables["id_pro"];
				 $val = $variables["val"];
				 $num = $variables["num"];
				 $items = $variables["items"];
				 $id_bolsa = $variables["id_bolsa_pdv"];

				 $response = $modelo->consultar_porcentajes($servs,$val,$items,$id_pro,$id_bolsa);
			break;
			case 'RecarServs':

				 $tipo_res = 'JSON'; //Definir tipo de respuesta;

				 $servs = $variables["servs"];
				 $id_pro = $variables["id_pro"];
				 $val = $variables["val"];
				 $num = $variables["num"];
				 $items = $variables["items"];
				 $key_pes = $variables["key_pes"];
				 $id_bolsa = $variables["id_bolsa"];
				 $response = $modelo->RecarServs($servs,$id_pro,$val,$num,$items,$key_pes,$id_bolsa);
			break;
			case 'ultimasTransac':

				 $tipo_res = 'JSON'; //Definir tipo de respuesta;
				 $datos = $modelo->ultimasTransac();
				 $response = array("draw" => 1,
								   "recordsTotal" => 0,
								   "recordsFiltered" => 0,
								   "data" => $datos);
			break;

		}

	// Respuestas del Controlador
	if($tipo_res == "JSON")
	{
	  echo json_encode($response,true); // $response será un array con los datos de nuestra respuesta.
	}
	elseif ($tipo_res == "HTML") {
	  echo $response; // $response será un html con el string de nuestra respuesta.
	}
} // Fin Try
catch (Exception $e) {}


