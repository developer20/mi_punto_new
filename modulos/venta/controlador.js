
var key_pes = 0;
var id_oper = 0;
var id_pro = 0;
var id_bolsa_pdv = 0;
var url_ar = "http://prueba.movilbox.net:88/movistar_colombia/files_noti";
//var url_ar = "../files_noti";
$(document).ready(function() {

	 CargarBols();
	 CargarOpers(0);
	 Cargar_Banner_Mbox();

	 $("#frmRe").submit(function() {
		 event.preventDefault();
		 validate();
     });

	 $("#valorotro").keypress(function(e) {
		var key;
		if(window.event){ // IE
			key = e.keyCode;
		}else if(e.which){ // Netscape/Firefox/Opera
			key = e.which;
		}
		if (key == 13){
		  validar();
		}
		else if (key < 48 || key > 57){
			return false;
		}
		return true;
		  //validar_espacios(event);
	});

	$("#num_m").keypress(function(e) {
		var key;
		if(window.event){ // IE
			key = e.keyCode;
		}else if(e.which){ // Netscape/Firefox/Opera
			key = e.which;
		}
		if (key == 13){
		  validar();
		}
		else if (key < 48 || key > 57){
			return false;
		}
		return true;
		  //validar_espacios(event);
	});

	$("#cancelar").click(function() {
		window.location = "?mod=venta"
	});

	$("#ultimeTransac").click(function() {
		ultimas_transac();
	});

});

function Cargar_Banner_Mbox(){
	var i, concatenar = "", concatenar2 = "", count=0;
	$.ajax({
		url:"modulos/noticias_promociones/controlador.php",
	    type:"POST",
		data:{"accion":"Cargar_Banner_Mbox"},
		dataType:"json",
	    success:function(datos){
	    	if(datos == ""){
	    		$(".grupo2").html("");
	    	}
	    	$.each(datos,function(i, val){

	    		if(i == 0){
	    			concatenar += "<li data-target='#banner-mbox' data-slide-to='"+count+"' class='active'></li>";
	    			concatenar2 += "<div class='item active'><img src='"+url_ar+"/imgs/"+val.ruta_img+"'> <div class='carousel-caption' style='background: rgba(0, 0, 0, 0.30);right:0 !important;left:0 !important;width:100%;bottom: 0px !important; padding-bottom:0; padding-top:0;'> <h4>Vigencia: Desde "+val.fecha+"</h4><br></div></div>";
	    		}else{
	    			concatenar += "<li data-target='#banner-mbox' data-slide-to='"+count+"'></li>";
	    			concatenar2 += "<div class='item'><img src='"+url_ar+"/imgs/"+val.ruta_img+"'><div class='carousel-caption' style='background: rgba(0, 0, 0, 0.30);right:0 !important;left:0 !important;width:100%;bottom: 0px !important;padding-bottom:0; padding-top:0;'> <h4>Vigencia: Desde "+val.fecha+"</h4><br></div></div>";
	    		}
	    		count++;
	    	})
	    	$("#indicadores").html(concatenar);
	    	$("#cont_img").html(concatenar2);
	    }

	})

}
function CargarBols()
{

	$.post('modulos/venta/controlador.php',
		{
			accion: 'CargarBols',
			id_bolsa: 0,

		},
		function(data, textStatus) {

			var data = JSON.parse(data);

			if(data["estado"] == 1){

				if(data["saldo"].length > 1){
					$("#cargar_bolsa").show();
				}

				$.each(data["saldo"],function(index, datos) {

					if(datos["principal"] == 1){
						$("#nom_bolsa").html(datos["nom_bolsa"]+"</b>");
						$("#bolsa").html("<br><b>$ "+addCommas(datos["saldo"])+"</b>");
						$("#bolsa_in").html("<br><b>$ "+addCommas(datos["saldo_incentivo"])+"</b>");
						$("#saldoac").val(datos["saldo"]);
						$("#saldoacIn").val(datos["saldo_in"]);

						id_bolsa_pdv = datos["id_bolsa"];
					}
			   });

			   $("#cargar_bolsa").unbind("click");
			   $("#cargar_bolsa").click(function() {

			   	  var bolsa_html = "";

			   	  $.each(data["saldo"],function(index, datos2) {

			   	  	bolsa_html += '<div class="s_b" onclick="CargarOpers('+datos2["id_bolsa"]+')">'+datos2["nom_bolsa"]+' <br>Saldo: <b>$ '+addCommas(datos2["saldo"])+'</b><br>Saldo Incentivo: <b>$ '+addCommas(datos2["saldo_incentivo"])+'</b></div>';

				  	//CargarOpers(datos2["id_bolsa"]);
				  });

				  if($( ".w_b_v" ).hasClass( "activo" )){
				  	$(".arrow").removeClass( "glyphicon glyphicon-chevron-up");
				  	$(".arrow").addClass( "glyphicon glyphicon-chevron-down");
				  	$(".w_b_v").removeClass("activo");
				  }else{
				  	$(".arrow").removeClass( "glyphicon glyphicon-chevron-down");
				  	$(".arrow").addClass( "glyphicon glyphicon-chevron-up");
				  	$(".w_b_v").addClass("activo");
				  }

			   	  $( ".w_b_v" ).toggle();
				  $("#carga_bolsas").html(bolsa_html);

		       });

			   key_pes = data["val_pes"];

			}else if(data["estado"] == 0){
				Notificacion(data["msg"],"error");
				setTimeout("window.location='?cerrar=S'",3000);
			}else if(data["estado"] == -1){
				$("#recargas").html("");
				Notificacion(data["msg"],"error");
			}


		}
	);
}

function CargarBols2(id_bolsa)
{

	$.post('modulos/venta/controlador.php',
		{
			accion: 'CargarBols',
			id_bolsa: id_bolsa,

		},
		function(data, textStatus) {

			var data = JSON.parse(data);

			if(data["estado"] == 1){

				$.each(data["saldo"],function(index, datos) {
					$("#nom_bolsa").html(datos["nom_bolsa"]+"</b>");
					$("#bolsa").html("<br><b>$ "+addCommas(datos["saldo"])+"</b>");
					$("#bolsa_in").html("<br><b>$ "+addCommas(datos["saldo_incentivo"])+"</b>");
					$("#saldoac").val(datos["saldo"]);
					$("#saldoacIn").val(datos["saldo_in"]);

					id_bolsa_pdv = datos["id_bolsa"];
			   });

			   key_pes = data["val_pes"];

			   $(".arrow").removeClass( "glyphicon glyphicon-chevron-up");
			   $(".arrow").addClass( "glyphicon glyphicon-chevron-down");
			   $(".w_b_v").removeClass("activo");
			   $( ".w_b_v" ).toggle();

			}else if(data["estado"] == 0){
				Notificacion(data["msg"],"error");
				setTimeout("window.location='?cerrar=S'",3000);
			}else if(data["estado"] == -1){
				$("#recargas").html("");
				Notificacion(data["msg"],"error");
			}


		}
	);

}

function CargarOpers(id_bolsa)
{

	$.post('modulos/venta/controlador.php',
		{
			accion: 'CargarOpers',
			tipo: '1',
			id_bolsa: id_bolsa

		},
		function(data, textStatus) {

			if(data != 0 || data != "")
			{

				var datos = JSON.parse(data);
				var operadores = "";
				//if(datos != ""){
					if(datos.length > 0){

						if(id_bolsa > 0){
							CargarBols2(id_bolsa);
						}

						$("#resultser").html("N/A");

						$.each(datos,function(index, fila) {

							if(fila.id == 6){
								$("#oper_movistar").show();

								$("#oper_movistar").html('<label style="font-size: 18px;">Operador Principal:</label><br /><span class="cssToolTip2"><input type="radio" id="6" name="servicio" value="6-Recarga Movistar" onClick="loaddescrip(this.value)"><label for="6"><img src="template/img/operadores/Movistar.png" alt="Recarga Movistar"></label><div> Recarga Movistar </div></span><br /><br />')

							}else{

								$("#otros_oper").show();

								operadores += '<span class="cssToolTip">';
								operadores += "<input type='radio' id='"+fila.id+"' name='servicio' value='"+fila.id+"-"+fila.titulo+"-"+fila.paquetes+"' onClick='loaddescrip(this.value)'>";
								operadores += '<label for="'+fila.id+'"><img src="template/img/operadores/'+fila.imagen+'" alt="'+fila.titulo+'"></label>';
								operadores += '<div> '+fila.titulo+' </div>';
								operadores += '</span>';
							}

							/*operadores += '<span class="cssToolTip">';
							operadores += "<input type='radio' id='"+fila.id+"' name='servicio' value='"+fila.id+"-"+fila.titulo+"-"+fila.paquetes+"' onClick='loaddescrip(this.value)'>";
							operadores += '<label for="'+fila.id+'"><img src="template/img/operadores/'+fila.imagen+'" alt="'+fila.titulo+'"></label>';
							operadores += '<div> '+fila.titulo+' </div>';
							operadores += '</span>';*/
						});

						$("#ver_operadores").html(operadores);

						id_oper = 0;
						id_pro = 0;

					}else{
						Notificacion("El proveedor no tiene operadores asignados","error");
						if(id_bolsa == 0){
							("#operadores").html("");
						}
					}

				//}

			}
		}
	);
}

function loaddescrip(valor)
{

	valor=valor.split("-");

	divdescrip=document.getElementById("resultser");

	divdescrip.innerHTML=valor[1]
	document.getElementById("num_m").focus();

	var id_servicio = valor[0];
	var val_paque = 0;

	if(valor[2]==1)
	{

	    $("#paquetes").html("");
		document.getElementById("valorotro").value="";
		document.getElementById("valuestatic").style.display="none"
		$("#des_saldo").fadeIn("fast");
		$("#mo2").fadeIn("fast");
		$('#valorotro').attr('readonly', true);
		$("#seleccione").fadeOut("fast");
		document.getElementById("num_m").type="text"
		document.getElementById("num_m").focus();
		document.getElementById("valorotro").value="";

		for(i=0;i<document.getElementsByName("valor").length;i++)
		{

			document.getElementsByName("valor")[i].disabled=true

		}

		$("#servs").fadeIn("fast");

		val_paque =  1;


	}else
	{

		$("#servs").fadeOut("fast");
		$("#idservicio").html("");
		$("#des_saldo").fadeOut("fast");

		$('#valorotro').attr('readonly', false);
		$("#seleccione").fadeIn("fast");
		document.getElementById("num_m").type="text";
		//document.getElementById("valorotro").value = val;

		document.getElementById("valuestatic").style.display="inline";
		document.getElementById("idservicio2").innerHTML=0;

		$("#mo2").fadeIn("fast");

		for(i=0;i<document.getElementsByName("valor").length;i++)
        {

			document.getElementsByName("valor")[i].disabled=false

        }

	}

	$.post('modulos/venta/controlador.php',
		{
			accion: 'CargarCatePaquete',
			id_servicio: id_servicio,
			val_paque: val_paque,
			id_bolsa: id_bolsa_pdv

		},
		function(data, textStatus) {

			if(data != 0 || data != "")
			{
				var datos = JSON.parse(data);

				if(val_paque == 1){

					var servicios = '<select name="catePaquete" id="catePaquete" onChange="loadselect(this.value,1,'+id_servicio+');" class="form-control" style="font-size:17px;">';

					servicios += "<option value = ''>Selecciona una categoria del paquete</option>";

					if(datos["info_paquete"].length > 0){

						$.each(datos["info_paquete"],function(index, fila) {
							servicios += "<option value = '"+fila.categoria+"'>"+fila.des+"</option>";
						});

					}

					servicios += "</select>";
					$("#idservicio").html(servicios);

				}

				id_oper = datos.id_oper;
				id_pro = datos.id_pro;

			}
		}
	);
}

function clearvalue(valor)
{
	document.getElementById("valorotro").value=valor
	document.getElementById("valorotro2").value=valor
}

function loadselect(valors,op,operador)
{

	if(op == 1){

		$.post('modulos/venta/controlador.php',
			{
				accion: 'CargarServs',
				id_servicio: operador,
				categoria: valors,
				id_pro: id_pro

			},
			function(data, textStatus) {

				if(data != 0 || data != "")
				{
					var datos = JSON.parse(data);

					var servicios = '<br><label style="font-size: 18px;">Item Paquete :</label>';

					servicios += '<select name="itemservi" id="itemservi" onChange="loadselect(this.value,2,'+operador+');" class="form-control" style="font-size:17px;">';

					servicios += "<option value = ''>Selecciona un paquete..</option>";

					if(datos.length > 0){

						$.each(datos,function(index, fila) {
							servicios += "<option value = '"+fila.code+"-"+fila.valVen+"-"+fila.obs+"-"+fila.des+"-"+fila.vals+"'>"+fila.des+"</option>";
						});

					}

					servicios += "</select>";
					$("#paquetes").html(servicios);

				}
			}
		);


	}else{

		divdescrip=document.getElementById("resultser")

		if(valors!="")
		{
			valors=valors.split("-");
			$("#des_saldo").fadeIn("fast");
			document.getElementById("valorotro").value=valors[1];
			document.getElementById("valorotro2").value=valors[4];
			document.getElementById("valor_des").innerHTML=addCommas(valors[4]);
			divdescrip.innerHTML=valors[2];
			ext = valors[1] - valors[4];
			document.getElementById("idservicio2").innerHTML=addCommas(ext);
		}else
		{
			document.getElementById("valorotro").value="";
			document.getElementById("valorotro2").value="";
			document.getElementById("valor_des").innerHTML=0;
			document.getElementById("idservicio2").innerHTML=0;
			divdescrip.innerHTML="N/A";
		}

	}

}

function guardar_venta()
{

	if(id_oper == 0 || id_pro == 0){

		Notificacion("No se puede realizar la recarga porque se ha detectado un error en el proceso","error");

		return 0;
	}

	for(i=0;i<document.getElementsByName("servicio").length;i++)
	{

		if(document.getElementsByName("servicio")[i].checked)
		{
			valor=document.getElementsByName("servicio")[i].value.split("-");
			var servicio = id_oper;
			var descripcion = "<b>"+valor[1]+"</b>";
			var itemservicio=0;

		}

	}

	var des_deta = "la siguiente ";

	if( $('#servs').css('display') == 'block' )
	{
		var itemser=document.getElementById("itemservi").value
		var itemser=itemser.split("-")
		var descripcion = "<b>"+itemser[3]+"</b>";
		var itemservicio= itemser[0] ;
	}

	var num_m = document.getElementById("num_m").value

	var val=document.getElementById("valorotro").value

	var valores = addCommas(val);

	$.post('modulos/venta/controlador.php',
	{
		accion: 'consultar_porcent',
		servs: servicio,
		id_pro: id_pro,
		val: val,
		num: num_m,
		items: itemservicio,
		id_bolsa_pdv: id_bolsa_pdv

	},
	function(data, textStatus) {

		var msg = "";
		var datos = JSON.parse(data);
		var concat = "";

		if(servicio == 6){

			if(datos.ajuste > 0 || datos.comision > 0){
				msg = "<br><br><div style='color:#000'> Por esta venta de <b>Recarga Movistar</b> te acreditamos a tu saldo: ";

				if(datos.ajuste > 0){
					msg += " <b>$"+addCommas(datos.ajuste)+"</b> por ajuste";
					concat = " y";
				}

				if(datos.comision > 0){
					msg += concat+" <b>$"+addCommas(datos.comision)+"</b> por comisión";
				}

				msg += "</div>";

			}

			if(parseInt($("#saldoacIn").val())>0){
				//msg = "<br><br><div style='color:#000'> Por esta venta de <b>Recarga Movistar</b> el descuento será al saldo que tienes de incentivo ";
			}
		}

		if(itemservicio > 0){

			if(datos.ganancia > 0){

				if(datos.tarifa == 0){
					msg = "<center><br><br><div style='color:#000;font-size:16px'> Por la venta de este paquete obtendrás una ganancia de: ";

						msg += " <b>$"+addCommas(datos.ganancia)+"</b>";

					msg += "</div></center>";

					BootstrapDialog.configDefaultOptions({
					    cssClass: ''
					});

				}else{
					msg = "<center><br><br><div style='color:#000;font-size:16px'> ! Hoy es un día especial por la venta de este paquete, del cual vas a obtener una ganancia de:";

						msg += " <b>$"+addCommas(datos.ganancia)+"</b> ¡";

					msg += "</div></center>";

					BootstrapDialog.configDefaultOptions({
					    cssClass: 'tarifa_especial'
					});

				}

			}

			des_deta = "el siguiente paquete ";
		}

        BootstrapDialog.show({
            title: 'Confirmación de la venta',
            closable: false,
            closeByBackdrop: false,
	        closeByKeyboard: false,
            message: '¿ Esta seguro de enviar '+des_deta+descripcion+' ?<p/> <span style="font-size:16px">Movil: <b>'+num_m+'</b><p/> Valor: <b>'+addCommas(valores)+"</b></span>"+msg,
            buttons: [{
                label: 'No',
            	cssClass: 'btn',
                action: function(dialog) {
                    dialog.close();

                }
            }, {
                label: 'Si',
            	cssClass: 'btn-primary',
                action: function(dialog) {

                $(".load").show();

				$.post('modulos/venta/controlador.php',
				{
					accion: 'RecarServs',
					servs: servicio,
					id_pro: id_pro,
					val: val,
					num: num_m,
					items: itemservicio,
					key_pes: key_pes,
					id_bolsa: id_bolsa_pdv

				},
				function(data, textStatus)
				{


					if(data != "")
					{
						var datos = JSON.parse(data);

						if(datos["id"]==1)
						{
							$("#respuesta").fadeOut("fast");
							var msg2 = "";

							if(datos["ajuste"] > 0 || datos["comision"] > 0 || datos["ganancia"] > 0){
								msg2 = "<br><br><div style='color:#000'> A tu saldo se ha agregado ";
								concat = "";

								if(datos["ajuste"] > 0){
									msg2 += " un ajuste por un valor de <b>$"+addCommas(datos["ajuste"])+"</b>";
									concat = " y";
								}

								if(datos["comision"] > 0){
									msg2 += concat+" una comisión por un valor de <b>$"+addCommas(datos["comision"])+"</b>";
								}

								if(datos["ganancia"] > 0){
									msg2 += concat+" un ajuste por paquete por un valor de <b>$"+addCommas(datos["ganancia"])+"</b>";
								}


								msg2 += "</div>";
							}

							BootstrapDialog.show({
					            title: 'Venta Exitosa',
					            type: BootstrapDialog.TYPE_SUCCESS,
					            closable: false,
					            closeByBackdrop: false,
						        closeByKeyboard: false,
					            message: 'Venta exitosa de '+descripcion+'<p/>Movil: <b>'+num_m+'</b> <p/>Valor: <b>$'+addCommas(valores)+'</b>'+msg2+' <br> <center>¿ Desea Generar Recibo de venta ?</center>',
					            buttons: [{
					                label: 'No',
				                	cssClass: 'btn',
					                action: function(dialog) {
					                    dialog.close();
				                    	window.location='?mod=venta';
					                }
					            }, {
					                label: 'Si',
				                	cssClass: 'btn-success',
					                action: function(dialog) {

				                    	dialog.close();
					                    var url= "pdf/colilla.php?registro="+datos["regs"];
										Abrir_ventana(url)
					                }
					            }],
					            onshonw: function(dialog) {
					            }
					        });


						}else if(datos["id"]==-1)
						{
							Notificacion(datos["msg"],"error");
							setTimeout("window.location='?mod=venta'",7000);
						}else if(datos["id"]==-2)
						{
							Notificacion(datos["msg"],"error");
							setTimeout("window.location='?cerrar=S'",5000);
						}

						$(".load").hide();

					}
					}
					);
                	dialog.close();
                }
            }],
            onshonw: function(dialog) {
            }
        });
		/*
		BootstrapDialog.confirm('¿ Esta seguro de enviar '+des_deta+descripcion+' ?<p/> <span style="font-size:16px">Movil: <b>'+num_m+'</b><p/> Valor: <b>'+addCommas(valores)+"</b></span>"+msg , function(result){
			if(result){

				$(".load").show();

				$.post('modulos/venta/controlador.php',
				{
					accion: 'RecarServs',
					servs: servicio,
					val: val,
					num: num_m,
					items: itemservicio,
					key_pes: key_pes

				},
				function(data, textStatus)
				{


						if(data != "")
						{
							var datos = JSON.parse(data);

							if(datos["id"]==1)
							{
								$("#respuesta").fadeOut("fast");
								var msg2 = "";

								if(datos["ajuste"] > 0 || datos["comision"] > 0 || datos["ganancia"] > 0){
									msg2 = "<br><br><div style='color:#000'> A tu saldo se ha agregado ";
									concat = "";

									if(datos["ajuste"] > 0){
										msg2 += " un ajuste por un valor de <b>$"+addCommas(datos["ajuste"])+"</b>";
										concat = " y";
									}

									if(datos["comision"] > 0){
										msg2 += concat+" una comisión por un valor de <b>$"+addCommas(datos["comision"])+"</b>";
									}

									if(datos["ganancia"] > 0){
										msg2 += concat+" un ajuste por paquete por un valor de <b>$"+addCommas(datos["ganancia"])+"</b>";
									}


									msg2 += "</div>";
								}

								BootstrapDialog.show({
						            title: 'Venta Exitosa',
						            type: BootstrapDialog.TYPE_SUCCESS,
						            closable: false,
						            closeByBackdrop: false,
							        closeByKeyboard: false,
						            message: 'Venta exitosa de '+descripcion+'<p/>Movil: <b>'+num_m+'</b> <p/>Valor: <b>$'+addCommas(valores)+'</b>'+msg2+' <br> <center>¿ Desea Generar Recibo de venta ?</center>',
						            buttons: [{
						                label: 'No',
					                	cssClass: 'btn',
						                action: function(dialog) {
						                    dialog.close();
					                    	window.location='?mod=venta';
						                }
						            }, {
						                label: 'Si',
					                	cssClass: 'btn-success',
						                action: function(dialog) {

					                    	dialog.close();
						                    var url= "pdf/colilla.php?registro="+datos["regs"];
											Abrir_ventana(url)
						                }
						            }],
						            onshonw: function(dialog) {
						            }
						        });


							}else if(datos["id"]==-1)
							{
								Notificacion(datos["msg"],"error");
								setTimeout("window.location='?mod=venta'",3000);
							}else if(datos["id"]==-2)
							{
								Notificacion(datos["msg"],"error");
								setTimeout("window.location='?cerrar=S'",3000);
							}

							$(".load").hide();

						}
					}
				);

			}

		});*/

		jQuery('.modal-header').bind('load', function(e) {

			if(itemservicio > 0){

				if(datos.ganancia > 0){

					if(datos.tarifa == 1){
						$(".modal-header").addClass("tarifa_especial");
					}

				}

			}

		})

	}
	);

}

function validate()
{

	var conts=0;

	var movilstring=document.getElementById("num_m").value


	for(i=0;i<document.getElementsByName("servicio").length;i++)
    {

		if(document.getElementsByName("servicio")[i].checked)
		{
		conts++;
		}

    }


	if(conts==0)
	{
		Notificacion('Por favor seleccione un operador',"error");
		return false;
	}else if($('#servs').css('display') == 'block' && !$("#itemservi") && document.getElementById("itemservi").value=="")
	{
		Notificacion('Por favor Seleccione un paquetigo',"error");
	    return false;
	}
	else if(document.getElementById("num_m").value=="" || movilstring.length<10)
	{
		Notificacion('Por favor ingrese un numero movil valido', "error")

	    return false;

	}else if(document.getElementById("valorotro").value=="")
	{
		Notificacion('Por favor ingrese un valor', "error")
		return false;
	}else if(parseInt(document.getElementById("saldoac").value) < parseInt(document.getElementById("valorotro").value) && parseInt(document.getElementById("saldoacIn").value) == 0)
	{
	    Notificacion('Error, no tiene saldo disponible<p/>saldo actual : '+document.getElementById("saldoac").value+"<p/>valor recarga : "+addCommas(document.getElementById("valorotro").value), "error")
		return false;
	}
	else
    {
		guardar_venta();
		return true;
	}

}

function Abrir_ventana (pagina)
{
	day = new Date();
	id = day.getTime();

	/*
	eval("page" + id + " = window.open(pagina, '" + id + "', 'toolbar=0,scrollbars=0,location=0,statusbar=0,menubar=0,resizable=0,width=300,height=300,left = 910,top = 440');");
	window.location="?mod=venta";*/

	BootstrapDialog.show({
	   title: 'Recibo de venta',
	   closable: true,
	   closeByBackdrop: false,
	   closeByKeyboard: false,
	   size: BootstrapDialog.SIZE_WIDE,
	   message: '<embed src="'+pagina+'" style="width:100%; height:500px;"  type="application/pdf">',
	   onhidden: function(dialogRef){
		  window.location='?mod=venta';
	   }
	});
}

var dtable;
function ultimas_transac(){
	if ( ! $.fn.DataTable.isDataTable( '#tblTrans' ) ) {

		 dtable = $("#tblTrans").DataTable({

			"ajax": {
				"url": "modulos/venta/controlador.php",
				"type": "POST",
				"deferRender": false,
				"data":{
					accion:'ultimasTransac'
				}
			},
			  "bFilter": false,
			  "paginate": false,
			  "order": [],
			  "responsive":false,
			  "scrollX": true,
			  "columns": [
			  	{ "data": "movi"},
			  	{ "data": "des_operador"},
			    { "data": "num_m"},
				{ "data": "des_paquete"},
				{ "data": "fecha_trans"},
				{ "data": "hora_trans"},
				{ "data": "porcen"},
				{ "data": "s_ant_inc"},
				{ "data": "s_ant_inc"},
				{ "data": "v_inc"},
				{ "data": "s_act_inc"},
				{ "data": "s_ant"},
				{ "data": "v_recarga_des"},
				{ "data": "s_act"},
				{ "data": "nom_bolsa"}
				//{ "data": "esta"}
				],
				"columnDefs": [
				{
					"targets": 0,
					"data": "movi",
					 render: function ( data, type, row ) {

                          var color = "";

					 	  if(row.movi_int == 1){
					 	  	color = "#de1919";
					 	  }

					 	  if(row.movi_int == 2 || row.movi_int == 3 || row.movi_int == 4){
					 	  	color = "#056311";
					 	  }

						  return "<div style='font-weight: bold;color:"+color+"'>"+data+"</div>";
					 }
				},
				{
					"targets": 7,
					"data": "",
					 render: function ( data, type, row ) {
					 	  var total = parseInt(row["v_inc"]) + parseInt(row["v_recarga"]);
						  return "$ "+addCommas(total);
					 }
				},
				{
					"targets": 8,
					"data": "s_ant_inc",
					 render: function ( data, type, row ) {
						  return "$ "+addCommas(data);
					 }
				},
				{
					"targets": 9,
					"data": "v_inc",
					 render: function ( data, type, row ) {
						  return "$ "+addCommas(data);
					 }
				},
				{
					"targets": 10,
					"data": "s_act_inc",
					 render: function ( data, type, row ) {
						  return "$ "+addCommas(data);
					 }
				},
				{
					"targets": 11,
					"data": "s_ant",
					 render: function ( data, type, row ) {
						  return "$ "+addCommas(data);
					 }
				},
				{
					"targets": 12,
					"data": "v_recarga_des",
					 render: function ( data, type, row ) {
						  return "$ "+addCommas(data);
					 }
				},
				{
					"targets": 13,
					"data": "s_act",
					 render: function ( data, type, row ) {
						  return "$ "+addCommas(data);
					 }
				}
				],
		 });

	}
	else{
		dtable.destroy();
		ultimas_transac();
	}
	$("#resultado").show();
}

function addCommas(nStr)
{
	nStr += '';
	x = nStr.split('.');
	x1 = x[0];
	x2 = x.length > 1 ? '.' + x[1] : '';
	var rgx = /(\d+)(\d{3})/;
	while (rgx.test(x1)) {
	x1 = x1.replace(rgx, '$1' + ',' + '$2');
	}
	return x1 + x2;
}