$(document).ready(function() {
	
	Bienvenida(); // funcion de ejemplo.

	//Funcion del boton Agregar..!
	$("#add").click(function() {
		$('#modal').modal('show'); // Abre el modal donde se crea el formulario.
	})

	//Funcion para cuando se haga el submit del form.
	$("#frm_ejemplo").submit(function(event) {
	 	event.preventDefault();
      	CRUD_ejemplo();
    });
});

function Bienvenida () {
	Notificacion("Empieaza a Programar el modulo. [[modulo]]","success");
};

//Funcion que contendra los metodos ajax con las respuestas de nuestro form.
function CRUD_ejemplo () {
	Notificacion("El Crud funciona, programa las opciones que contendra y los metodos ajax necesarios","success");
}