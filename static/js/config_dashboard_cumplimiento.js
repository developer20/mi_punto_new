var columnaGeneral = 0;
var configWidgets = [];
var identificadorPopover = "";
var configPopovers = [];
var columnaGrupoWidget = 0;
var get;
var hoy = new Date();
var anioActual = parseInt(hoy.getFullYear());
var mesActual = parseInt(hoy.getMonth()) + 1;

$(document).ready(() => {

    get = get_vars();
    element_ladda = document.querySelector("button[type=submit]") || document.querySelector("button[type=button]");

    consultarConfiguracion();

    $("#widgets").sortable({
        start: function(event, ui) {
            $(this).css("border-style", "dashed");
            $(this).css("border-color", "#b5b5b5");
            $(this).css("border-radius", "7px");
        },
        stop: function(event, ui) {
            $(this).removeAttr('style');
            $(this).css("padding-top", "20px");
            $(this).css("margin-right", "0");
            $(this).css("margin-left", "0");
        },
        update: function(event, ui) {
            almacenarConfiguracion();
        }
    }).disableSelection();

});

function ordernarWidgets() {

    if (configWidgets.length > 0) {
        var contenidoWidgets = "";
        $.each(configWidgets, function(index, fila) {
            let id = fila.id;
            let clase = fila.clase;
            let style = fila.style;
            let data = fila.data;
            let contenido = $("#" + id).html();

            contenidoWidgets += `<div class="${clase}" id="${id}" style="${style}" data-tamanio = "${data}">${contenido}</div>`;
        });
        $('#widgets').html(contenidoWidgets);
    }

}

function seleccionarVista(identificador, columnaG, identificadorW) {

    $(".seleccionar-vista-" + identificadorW).css("background-color", "#fff");
    $(".seleccionar-vista-" + identificadorW).css("color", "#333");
    $("#" + identificador).css("background-color", "#2097ae");
    $("#" + identificador).css("color", "#fff");

    columnaGeneral = columnaG;
    identificadorPopover = identificador;

}

function aplicarSeleccion(idSeleccion, identificadorColumna, columnaGrupo = 0) {

    $("#widget-" + identificadorColumna).removeAttr('class');
    $("#widget-" + identificadorColumna).attr('class', 'col-md-' + columnaGeneral);
    $("#widget-" + identificadorColumna).data("tamanio", identificadorPopover);

    $('#' + idSeleccion).popover('hide');
    $('#' + idSeleccion).popover('toggle');

    $("#" + identificadorPopover).css("background-color", "#2097ae");
    $("#" + identificadorPopover).css("color", "#fff");

    if (columnaGrupo > 0) {
        columnaGrupoWidget = columnaGrupo;
    }

    almacenarConfiguracion();

}

function pintarTamanioPopover() {

    $('#widgets').children().each(function() {
        let data = $(this).data("tamanio");
        $("#" + data).css("background-color", "#2097ae");
        $("#" + data).css("color", "#fff");
    });

}

function almacenarConfiguracion() {

    if (typeof(Storage) !== undefined) {

        configWidgets = [];

        $('#widgets').children().each(function() {
            let id = $(this).attr("id");
            let clase = $(this).attr("class")
            let style = $(this).attr("style")
            let data = $(this).data("tamanio");
            configWidgets.push({ 'id': id, 'clase': clase, 'style': style, 'data': data });
        });

        let infoDashboard = {};

        if (localStorage.getItem('infoDashboard') !== null) {
            infoDashboard = JSON.parse(localStorage.getItem('infoDashboard'));
            if (infoDashboard['configWidgets'] !== undefined) {
                localStorage.removeItem('configWidgets');
                infoDashboard['configWidgets'] = configWidgets;
            }

        } else {
            infoDashboard['configWidgets'] = configWidgets;
        }

        if (columnaGrupoWidget > 0) {
            infoDashboard['columnaGrupoWidget'] = columnaGrupoWidget;
        }

        let jsonInfo = JSON.stringify(infoDashboard);

        localStorage.setItem('infoDashboard', jsonInfo);
    }
}

function consultarConfiguracion() {
    if (typeof(Storage) !== undefined) {

        if (localStorage.getItem('infoDashboard') !== null) {

            let infoDashboard = JSON.parse(localStorage.getItem('infoDashboard'));

            if (infoDashboard['configWidgets'] !== undefined) {
                let infoWidgets = infoDashboard['configWidgets'];
                configWidgets = infoWidgets;
            }
        }
    }
}

function agregarAnios(idAnio, idAnioSelect) {
    var anioCargado2 = anioActual - 2;
    var html = '<option value="0">Año</option>';
    while (anioCargado2 <= anioActual) {
        html += `<option value="${anioCargado2}">${anioCargado2}</option>`;
        anioCargado2++;
    }

    $("#" + idAnio).html(html);
    $("#" + idAnio).val(anioActual).prop("selected", true);

    if (idAnioSelect > 0) {
        $("#" + idAnio).val(idAnioSelect).prop("selected", true);
    }

}

function cargarFiltros(idDistribuidorCampo, idVendedorCampo, idRegionalCampo, idbodegaCampo, idDistribuidorSelect, idVendedorSelect, idRegionalSelect, idBodegaSelect) {
    $.post(`modulos/dashboard_cumplimiento_v1.0.0/controlador.php`, {
        accion: "cargarFiltros"
    }, (data) => {
        data = JSON.parse(data);
        let distribuidores = data.distribuidores;
        let regionales = data.regionales;
        let vendedores = data.vendedores;
        let bodegas = data.bodegas;

        $("#" + idDistribuidorCampo).html('<option value="0">Distribuidor</option>');
        $.each(distribuidores, (id, distribuidor) => {
            $("#" + idDistribuidorCampo).append(`<option value = "${distribuidor.id}">${distribuidor.nombre}</option>`);
        })

        $("#" + idDistribuidorCampo).change(() => {

            var idDistribuidor = $("#" + idDistribuidorCampo).val();

            cargarFiltrosVendedorRegionalBodega(idVendedorCampo, idRegionalCampo, idbodegaCampo, idDistribuidor, regionales, vendedores, bodegas, idVendedorSelect, idRegionalSelect, idBodegaSelect);


        }).val(idDistribuidorSelect).prop("selected", true);

        if (idDistribuidorSelect > 0) {
            cargarFiltrosVendedorRegionalBodega(idVendedorCampo, idRegionalCampo, idbodegaCampo, idDistribuidorSelect, regionales, vendedores, bodegas, idVendedorSelect, idRegionalSelect, idBodegaSelect);
        }

    });
}

function cargarFiltrosVendedorRegionalBodega(idVendedorCampo, idRegionalCampo, idbodegaCampo, idDistribuidor, regionales, vendedores, bodegas, idVendedorSelect, idRegionalSelect, idBodegaSelect) {
    if (idVendedorCampo != "") {
        $("#" + idVendedorCampo).html('<option value="0">Vendedor</option>');
    }
    if (idRegionalCampo != "") {
        $("#" + idRegionalCampo).html('<option value="0">Regional</option>');
        $("#" + idbodegaCampo).html('<option value="0">Bodega</option>');
    }

    if (idDistribuidor != "0") {

        if (idVendedorCampo != "") {
            $.each(vendedores, (iv, vendedor) => {
                if (vendedor.id_distri == idDistribuidor) {
                    $("#" + idVendedorCampo).append(`<option value = "${vendedor.id}">${vendedor.nombre}</option>`);
                }
            })
            $("#" + idVendedorCampo).change(() => {
                if (idRegionalCampo != "") {
                    $("#" + idRegionalCampo).html('<option value="0">Regional</option>');
                    $("#" + idbodegaCampo).html('<option value="0">Bodega</option>');

                    var disabled = false;
                    if ($("#" + idVendedorCampo).val() != "0") {
                        disabled = true;
                    }

                    $("#" + idRegionalCampo).prop('disabled', disabled);
                    $("#" + idbodegaCampo).prop('disabled', disabled);



                    if (!disabled) {
                        cargarfiltrosBodegaRegional(idRegionalCampo, idbodegaCampo, regionales, bodegas, idDistribuidor, idRegionalSelect, idBodegaSelect);
                    }
                }

            }).val(idVendedorSelect).prop("selected", true);
        }

        if (idVendedorSelect > 0) {

            if (idRegionalCampo != "") {

                if ($("#" + idVendedorCampo).val() != "0") {
                    disabled = true;
                }

                $("#" + idRegionalCampo).prop('disabled', disabled);
                $("#" + idbodegaCampo).prop('disabled', disabled);
            }

        } else {
            cargarfiltrosBodegaRegional(idRegionalCampo, idbodegaCampo, regionales, bodegas, idDistribuidor, idRegionalSelect, idBodegaSelect);
        }

    }
}

function cargarfiltrosBodegaRegional(idRegionalCampo, idbodegaCampo, regionales, bodegas, idDistribuidor, idRegionalSelect, idBodegaSelect) {

    if (idRegionalCampo != "") {

        $.each(regionales, (ir, regional) => {
            if (regional.id_distri == idDistribuidor) {
                $("#" + idRegionalCampo).append(`<option value = "${regional.id}">${regional.nombre}</option>`);
            }
        })

        $("#" + idRegionalCampo).change(() => {
            $("#" + idbodegaCampo).html('<option value="0">Bodega</option>');
            var idRegional = $("#" + idRegionalCampo).val();
            if (idRegional != "0") {
                $.each(bodegas, (ib, bodega) => {
                    if (bodega.id_distri == idDistribuidor && bodega.id_regional == idRegional) {
                        $("#" + idbodegaCampo).append(`<option value = "${bodega.id}">${bodega.nombre}</option>`);
                    }
                    $("#" + idbodegaCampo).change().val(idBodegaSelect).prop("selected", true);;
                })
            }
        }).val(idRegionalSelect).prop("selected", true);

        if (idRegionalSelect > 0) {
            let idRegional = idRegionalSelect;
            $.each(bodegas, (ib, bodega) => {
                if (bodega.id_distri == idDistribuidor && bodega.id_regional == idRegional) {
                    $("#" + idbodegaCampo).append(`<option value = "${bodega.id}">${bodega.nombre}</option>`);
                }
                $("#" + idbodegaCampo).change().val(idBodegaSelect).prop("selected", true);;
            })
        }

    }

}

function recargarWidget(idWidget, anio, mes, idDistribuidor, idVendedor, idRegional, idBodega) {

    $("#etiquetas-" + idWidget).children().each((i, fila) => {
        $(fila).hide()
    })

    if (idWidget == 1) {
        cumplimiento_grupo(anio, mes, idDistribuidor, idVendedor, idBodega);
    }

    if (idWidget == 2) {
        devolverResultadosGrupoDistri(anio, mes, idDistribuidor, idVendedor, idRegional, idBodega);
    }

    if (idWidget == 3) {
        devolverResultadosHistoricoDistri(anio, mes, idDistribuidor, idVendedor, idRegional, idBodega);
    }

    if (idWidget == 4) {
        devolverResultadosColocacionBodega(anio, mes, idDistribuidor, idVendedor, idRegional, idBodega);
    }

    if (idWidget == 5) {
        devolverResultadosColocacionGrupoDistri(anio, mes, idDistribuidor, idVendedor, idRegional, idBodega);
    }

    if (idWidget == 6) {
        devolverResultadosColocacionVendedor(anio, mes, idDistribuidor, idVendedor, idRegional, idBodega);
    }

    if (idWidget == 7) {
        devolverResultadosColocacionBodegaDistri(anio, mes, idDistribuidor, idVendedor, idRegional, idBodega);
    }

}