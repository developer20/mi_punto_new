var Menu = "";
var loader_dw;
$(document).ready(function() {

    var get = get_vars();
    
    // mostrar la cantidad del carrito
    function cant_productos(){
  
        $.post('funciones/controlador.php',
        {
            accion: 'cantidad_carrito',
        },
        function (data) {
            $("#num_pro_car").html(data);
        });
    
    
    }
    cant_productos();

    if (localStorage.getItem('est_pss_pos') == 0 && get['mod'] != "changePass" && get['mod'] != "login") {
        location.href = "?mod=changePass";
    }

    if (localStorage.getItem("est_enc") > 0 && get["mod"] != "responder_encuesta_v1.0.0" && get["mod"] != "login") {
        window.location = "?mod=responder_encuesta_v1.0.0";
    }

    $(".tex_acceso").html(localStorage.getItem('psid'));

    if (get['mod'] != "login" && get['mod'] != "recopass") {
        //notificaciones();
    }
    //notificaciones2();

    count_noti_contacto();

    $("#showAllNoty").click(function () {
        leer_notificacion(0);
        notificaciones_contacto();
    });

    if (get.mod != "login") {
        $("select").css("width", "100%").addClass("select2").select2({});
        $("select").css({ "width": "10%", "position": "absolute", "display": "" });
    }

    var loader = "";
var eso;
$(document).ajaxSend(function(event, request, settings) {
    loader = $('body').loadingIndicator({
        useImage: false,
    }).data("loadingIndicator");

});

$(document).ajaxComplete(function(event, request, settings) {
    loader.hide();
});

$(document).ajaxError(function(event, xhr, ajaxOptions, thrownError) {
    Error_Sistema(xhr.responseText);
});

})





function notificaciones() {

    $.post('modulos/val_permiso/controlador.php', {
            accion: 'consultar_noti'
        },
        function(data, textStatus) {

            var datos = JSON.parse(data);

            if (datos['estado'] == 1) {

                var encNum = parseInt(datos['cant_enc']);
                var notiNum = parseInt(datos['cant_noti']);

                if (notiNum !== null && notiNum != "" && notiNum > 0) {
                    if ($("#id_noti").length > 0) {

                        $("#id_noti").html(notiNum);

                    } else {
                        $("#notiNews").append('<span class="badge red" id = "id_noti">' + notiNum + '</span>');
                    }
                } else {
                    $("#id_noti").remove();
                }

                if (encNum !== null && encNum != "" && encNum > 0) {
                    if ($("#id_enc").length > 0) {

                        var valorE = parseInt($("#id_enc").html());

                        var total_e = valorE - encNum;

                        if (total_e > 0) {
                            $("#id_noti").html(total_e);
                        }

                    } else {
                        $("#notiEnc").append('<span class="badge red" id = "id_enc">' + encNum + '</span>');
                    }
                } else {
                    $("#id_enc").remove();
                }

            } else {
                Notificacion(datos["msg"], "error");
                setTimeout("window.location='?cerrar=S'", 3000);
            }

        }
    );
}

function notificaciones2() {

    $.post('modulos/val_permiso/controlador.php', {
            accion: 'consultar_noti2'
        },
        function(data, textStatus) {

            var datos = JSON.parse(data);
            var notiNum = parseInt(datos['cant_noti']);

            if (notiNum !== null && notiNum != "" && notiNum > 0) {
                if ($("#id_notif").length > 0) {

                    $("#id_notif").html(notiNum);

                } else {
                    $("#notiNews2").append('<span class="badge red" id = "id_notif" style="margin: 7px 25px 0;">' + notiNum + '</span>');
                }

            } else {
                $("#id_notif").remove();
            }

            $("#notiNews2").click(function() {
                cargar_notificaciones();
            });

        }
    );
}

function cargar_notificaciones() {
    $("#showNoti").slideToggle();

    var html = "";

    $.post('modulos/val_permiso/controlador.php', {
            accion: 'consultar_noti2'
        },
        function(data, textStatus) {

            var datos = JSON.parse(data);
            var info = datos['carga_noti'];

            $.each(info, function(index, fila) {

                var back = "#fff";
                var text = "Leida";

                if (fila.estado == 0) {
                    back = "#A8DDF4";
                    text = "";
                }

                html += '<li class="view_deta" style="background: ' + back + '" data-id_notif = "' + fila.id + '" id = "notf_' + fila.id + '"><a href="#" style="color: #555;font-size: 13px;" id = "detaNoti_' + fila.id + '"><div style="float:left;width: 205px;">' + fila.titulo + '</div><div style="position: relative;font-size: 10px;top: 7px;float:left;">' + text + '</div><i class="fa fa-angle-left pull-right" id="icon-arrow-noti_' + fila.id + '"></i></a>';
                html += '<ul class="menu" id="ul_noty_' + fila.id + '" style="width: 100%;height: 160px;padding: 10px;display:none;background: #fff;">hola</ul>';
                html += '</li>';
            });

            $("#ul_noty").html(html);

            $(".view_deta").click(function() {
                var id_notif = $(this).data("id_notif");
                view_deta(info, id_notif);
            });

            if ($("#showNoti").is(":visible")) {
                $(".main-sidebar, .main-footer, .content-wrapper").click(function() {
                    $("#showNoti").slideUp();
                });
            }

        }
    );

}

function view_deta(info, id_notif) {

    var html = "";

    if ($("#ul_noty_" + id_notif).is(":visible")) {

        $("#ul_noty_" + id_notif).slideUp();
        $("#icon-arrow-noti_" + id_notif).removeAttr("style");
        $("#detaNoti_" + id_notif).css("border-bottom", "0");

        var options = {};

        /*
        $("#notf_"+id_notif).fadeOut("500");
        $("#notf_"+id_notif).remove();*/

    } else {
        $("#ul_noty_" + id_notif).slideDown();
        $("#icon-arrow-noti_" + id_notif).removeAttr("style");
        $("#icon-arrow-noti_" + id_notif).css("transform", "rotate(-90deg)");
        $("#detaNoti_" + id_notif).css("border-bottom", "1px solid rgb(195, 195, 195)");
        $("#notf_" + id_notif).css("background", "#fff");

        $.each(info, function(index, fila) {
            if (fila.id == id_notif) {

                var style_content = "border: 1px solid #e4e4e4;padding: 5px;-webkit-box-shadow: 0 1px 2px rgba(0,0,0,.2);box-shadow: 0 1px 2px rgba(0,0,0,.2);-webkit-border-radius: 3px;border-radius: 3px;";
                var style_t = "font-size: 14px;padding: 5px;padding: 5px;font-weight: bold;";
                var style_m = "font-size: 13px;padding: 5px;padding: 5px;";
                var style_ti = "font-size: 11px;padding: 5px;padding: 5px;text-align:right;";

                html += '<div style="' + style_content + '">';
                html += '<div style="' + style_t + '">' + fila.titulo_d + '</div>';
                html += '<div style="' + style_m + '">' + fila.msg + '</div>';
                html += '<div style="' + style_ti + '">' + fila.fecha_envio + ' - ' + fila.hora_envio + '</div>';
                html += '</div>';
            }

        });

        $("#ul_noty_" + id_notif).html(html);

        leer_notif(id_notif);

    }

}

function leer_notif(id_notif) {

    $.post('modulos/val_permiso/controlador.php', {
            accion: 'leer_noti',
            id_notif: id_notif
        },
        function(data, textStatus) {

            if (data > 0) {
                if ($("#id_notif").length > 0) {

                    if (id_notif > 0) {

                        var t_noti = parseInt($("#id_notif").html());
                        var t_noti = t_noti - 1;

                        if (t_noti == 0) {
                            $("#id_notif").remove();
                        } else {

                            $("#id_notif").html(t_noti);

                        }

                    } else {
                        $("#id_notif").remove();
                        $("#ul_noty").html("");
                    }

                }
            }

        }
    );
}

function get_vars() {
    var url = location.search.replace("?", "");
    var arrUrl = url.split("&");
    var urlObj = {};
    for (var i = 0; i < arrUrl.length; i++) {
        var x = arrUrl[i].split("=");
        urlObj[x[0]] = x[1];
    }
    return urlObj;
}

function format(n, c, d, t) {
    c = isNaN(c = Math.abs(c)) ? 2 : c,
        d = d == undefined ? "." : d,
        t = t == undefined ? "," : t,
        s = n < 0 ? "-" : "",
        i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
        j = (j = i.length) > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};

$("#cargar_visita_detalle").load("template/vista_detalle_visita.html");

// agregar al carrito
$("#cargar_carrito_compras").load("template/vista_carrito_compras.html");

// ver carrito 
$("#ver_carrito_compras").load("template/carrito_productos.html");

/*function CargarMenu(id_padre){
	
	var device = navigator.userAgent
	if ((window.innerWidth<=720) || (device.match(/Iphone/i)|| device.match(/Ipod/i)|| device.match(/Android/i)|| device.match(/J2ME/i)|| device.match(/BlackBerry/i)|| device.match(/iPhone|iPad|iPod/i)|| device.match(/Opera Mini/i)|| device.match(/IEMobile/i)|| device.match(/Mobile/i)|| device.match(/Windows Phone/i)|| device.match(/windows mobile/i)|| device.match(/windows ce/i)|| device.match(/webOS/i)|| device.match(/palm/i)|| device.match(/bada/i)|| device.match(/series60/i)|| device.match(/nokia/i)|| device.match(/symbian/i)|| device.match(/HTC/i))) {
		
		
		Menu+='<li><a href="../inicio/inicio.php" target="_parent" class="">Inicio</a></li>';
		Menu+='<li class="has-sub"><a href="#" target="_parent" class=""><span>Ventas</span></a>';
		Menu+='<ul>';
		Menu+='<li><a href="?mod=venta" target="_parent" class="">Recargas</a></li>';
		Menu+='<li><a href="../venta/frmreporte_venta.php" target="_parent" class="">Reporte</a></li>';
		Menu+='</ul>';
		Menu+='</li>';
		Menu+='<li class="has-sub"><a href="#" target="_parent" class=""><span>Activaciones</span></a>';
		Menu+='<ul>';
		Menu+='<li><a href="../activaciones/frmactivaciones.php" target="_parent" class="">Activar Simcard</a></li>';
		Menu+='<li><a href="../activaciones/frmreporte_activacion.php" target="_parent" class="">Reporte</a></li>';
		Menu+='</ul>';
		Menu+='</li>';
		Menu+='<li class="has-sub"><a href="#" target="_parent" class=""><span>Registros</span></a>';
		Menu+='<ul>';
		Menu+='<li><a href="../registro_equipos/frm_registroequipos.php" target="_parent" class="">Registro de Equipos</a></li>';
		Menu+='<li><a href="../registro_equipos/frmreporte_registros.php" target="_parent" class="">Reporte</a></li>';
		Menu+='</ul>';
		Menu+='</li>';
		Menu+='<li class="has-sub"><a href="#" target="_parent" class=""><span>Reposicion</span></a>';
		Menu+='<ul>';
		Menu+='<li><a href="../reposicion_simcard/frm_reponersimcard.php" target="_parent" class="">Reposicion Simcard</a></li>';
		Menu+='<li><a href="../reposicion_simcard/frmreporte_simcard.php" target="_parent" class="">Reporte</a></li>';
		Menu+='</ul>';
		Menu+='</li>';
		Menu+='<li class="has-sub"><a href="#" target="_parent" class=""><span>Reportes</span></a>';
		Menu+='<ul>';
		Menu+='<li><a href="../compras/frmcompras.php" target="_parent" class="">Compras</a></li>';
		Menu+='<li><a href="../reporte_inventario/frm_reporte_inventario.php" target="_parent" class="">Inventario</a></li>';
		Menu+='</ul>';
		Menu+='</li>';
		Menu +='<li><a href="?mod=cambio_pass" rel="external">Cambiar contraseña</a></li>';
		Menu +='<li><a href="?cerrar=S" rel="external">Cerrar sesión</a></li>';
		
	}else{
		 
		Menu+='<li class="topmenu"><a href="../inicio/inicio.php" target="_parent" class="">Inicio</a></li>';
		Menu+='<li class="topmenu"><a href="#" target="_parent" class=""><span>Ventas</span></a>';
		Menu+='<ul>';
		Menu+='<li><a href="?mod=venta" target="_parent" class="">Recargas</a></li>';
		Menu+='<li><a href="../venta/frmreporte_venta.php" target="_parent" class="">Reporte</a></li>';
		Menu+='</ul>';
		Menu+='</li>';
		Menu+='<li class="topmenu"><a href="#" target="_parent" class=""><span>Activaciones</span></a>';
		Menu+='<ul>';
		Menu+='<li><a href="../activaciones/frmactivaciones.php" target="_parent" class="">Activar Simcard</a></li>';
		Menu+='<li><a href="../activaciones/frmreporte_activacion.php" target="_parent" class="">Reporte</a></li>';
		Menu+='</ul>';
		Menu+='</li>';
		Menu+='<li class="topmenu"><a href="#" target="_parent" class=""><span>Registros</span></a>';
		Menu+='<ul>';
		Menu+='<li><a href="../registro_equipos/frm_registroequipos.php" target="_parent" class="">Registro de Equipos</a></li>';
		Menu+='<li><a href="../registro_equipos/frmreporte_registros.php" target="_parent" class="">Reporte</a></li>';
		Menu+='</ul>';
		Menu+='</li>';
		Menu+='<li class="topmenu"><a href="#" target="_parent" class=""><span>Reposicion</span></a>';
		Menu+='<ul>';
		Menu+='<li><a href="../reposicion_simcard/frm_reponersimcard.php" target="_parent" class="">Reposicion Simcard</a></li>';
		Menu+='<li><a href="../reposicion_simcard/frmreporte_simcard.php" target="_parent" class="">Reporte</a></li>';
		Menu+='</ul>';
		Menu+='</li>';
		Menu+='<li class="topmenu"><a href="#" target="_parent" class=""><span>Reportes</span></a>';
		Menu+='<ul>';
		Menu+='<li><a href="../compras/frmcompras.php" target="_parent" class="">Compras</a></li>';
		Menu+='<li><a href="../reporte_inventario/frm_reporte_inventario.php" target="_parent" class="">Inventario</a></li>';
		Menu+='</ul>';
		Menu+='</li>';
   
  }
}

function Menu_Tiene_Hijos (id_padre) {
  var n = 0;
  var Datos_Menu = JSON.parse(localStorage.getItem('d_d_t'));
  var MENU = Datos_Menu['MENU'];

  $.each(MENU, function (indice, fila) {
		
        var padre = parseInt(fila.idpadre);
        if(padre == id_padre)
        {
           n += 1;
        }
    });
  return n;
}*/
//Form to Json
function formToJSON(selector) {
    var form = {};
    $(selector).find(':input[name]:checked').each(function() {
        var self = $(this);
        if (self.attr('name') != undefined) {
            var name = self.attr('name');
            if (form[name]) {
                form[name] = form[name] + ',' + self.val();
            } else {
                form[name] = self.val();
            }
        }

    });

    var TXTinputs = $(selector).find('input[type=text],input[type=email],input[type=password],input[type=radio],select,textarea').filter(function() {
        return this.value != '-1';
    });

    TXTinputs.each(function() {
        var self = $(this);
        if (self.attr('name') != undefined) {
            var name = self.attr('name');
            if (form[name]) {
                form[name] = form[name] + ',' + self.val();
            } else {
                form[name] = self.val();
            }
        }
    });

    return form;
}

function Error_Sistema(texto) {
    new PNotify({
        title: "Error de Sistema",
        text: texto,
        type: "error",
        addclass: "stack-bar-top",
        width: "50%",
        mouse_reset: false
    });
}

function Notificacion(texto, tipo) {
    PNotify.removeAll();
    new PNotify({
        title: "Mensaje del Sistema",
        text: texto,
        type: tipo,
        width: "50%",
        mouse_reset: false
    });
}
//Fin Form to Json
//Validaciones de Envios Ajax.


function confirma_rest(data, accion) {
    if (data.indexOf("<b>Parse error</b>:") > -1 || data.indexOf("<b>Mensaje: </b>") > -1) {
        Error_Sistema("<pre>" + data + "<br><b>Accion Rest: </b>" + accion + "</pre>");
        loader.hide();
        return false;
    } else if (data == "null") {
        Notificacion("La consulta no devolvio ningun dato.<br><b>Accion Rest: </b>" + accion, 'warning');
        loader.hide();
        return false;
    } else {
        return data;
    }
}

function Validar_Permiso(modulo) {
    var n = 0;
    var Datos_Menu = JSON.parse(localStorage.getItem('d_d_t'));
    var MENU = Datos_Menu['MENU'];
    modulo = "?mod=" + modulo;
    $.each(MENU, function(indice, fila) {
        var menu = fila.url;
        if (menu == modulo) {
            $("titulomod").html(fila.nombre);
            $("descripcionmod").html("<small>" + fila.nombre + "</small>");
            n += 1;
        }
    });
    if (n == 0) {
        location.href = "?mod=principal&per=N";

    }
}

function ExportarCSV(JSONData, ShowLabel, fileName, columnas) {
    var arrData = typeof JSONData != 'object' ? JSON.parse(JSONData) : JSONData;
    var CSV = '';
    if (columnas == undefined) { columnas = "" }
    if (ShowLabel != "" && ShowLabel != true) {
        var row = "";
        var datos = ShowLabel.split(",");
        for (var i = 0; i < datos.length; i++) {
            row += datos[i] + ';';
        };
        row = row.slice(0, -1);
        CSV += row + '\r\n';
    } else {
        var row = "";
        for (var index in arrData[0]) {
            row += index + ';';
        }
        row = row.slice(0, -1);
        CSV += row + '\r\n';
    }
    if (columnas != "" && columnas != true) {
        var col = columnas.split(",");
        for (var i = 0; i < arrData.length; i++) {
            var row = "";
            for (var a = 0; a < col.length; a++) {
                var index = col[a];
                row += '' + arrData[i][index] + ';';
            }
            row.slice(0, row.length - 1);
            CSV += row + '\r\n';
        }
    } else {
        for (var i = 0; i < arrData.length; i++) {
            var row = "";
            for (var index in arrData[i]) {
                row += '' + arrData[i][index] + ';';
            }
            row.slice(0, row.length - 1);
            CSV += row + '\r\n';
        }
    }

    if (CSV == '') {
        Notificacion("Informacion del Reporte Invalida, Verificar con soporte tecnico", "error");
        return;
    }
    /*var uri = 'data:text/csv;charset=utf-8,' + encodeURIComponent(CSV);
    var link = document.createElement("a");    
    link.href = uri;
    link.style = "visibility:hidden";
    link.download = fileName + ".csv";
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);*/
    var link = document.createElement("a");
    link.id = "lnkDwnldLnk"
    document.body.appendChild(link);
    var csv = CSV;
    blob = new Blob([csv], { type: 'text/csv' });
    var csvUrl = window.webkitURL.createObjectURL(blob);
    var filename = fileName + ".csv";

    $("#lnkDwnldLnk").attr({
        'download': filename,
        'href': csvUrl
    });

    $('#lnkDwnldLnk')[0].click();
    document.body.removeChild(link);
}

function hex2a(hex) {
    var str = '';
    for (var i = 0; i < hex.length; i += 2) str += String.fromCharCode(parseInt(hex.substr(i, 2), 16));
    return str;
}

function Cargar_Departamento_Ciudad(select_depto, select_ciudad) {
    $.post("funciones/controlador.php", { accion: "Retornar_Departamento_Ciudad" }, function(data, textStatus) {
        if (confirma_rest(data, "Retornar_Departamento_Ciudad")) {
            if (data != 0 || data != "") {
                data = JSON.parse(data);
                let html_selectDepartamento = "<option selected='selected' value=''>SELECCIONAR</option>";
                $.each(data.departamento, function(index, fila) {
                    html_selectDepartamento += "<option value='" + fila.id + "'>" + fila.nombre + "</option>";
                });
                $("#" + select_depto).change(function() {
                    let html_selectCiudad = "<option selected='selected' value=''>SELECCIONAR</option>";
                    let id_depto = $(this).val();
                    $.each(data.ciudad, function(index, fila) {
                        if (fila.id_departamento == id_depto) {
                            html_selectCiudad += "<option value='" + fila.id + "'>" + fila.nombre + "</option>";
                        }
                    });
                    $("#" + select_ciudad).html(html_selectCiudad).change();
                });
                $("#" + select_depto).html(html_selectDepartamento).change();
            }
        }
    });
}


function geo_detalle_visita(id_check, repor_super = "false") {
    $("#cargar_visita_detalle").show();
    $("#modal_geo_visita").modal("show");
    loader_dw = $("#modal_geo_visita").loadingIndicator({ useImage: !1 }).data("loadingIndicator");
    loader_dw.show();
    let img = "";
    let zona;
    $.post("funciones/controlador.php", {
        accion: "geo_detalle_visita",
        id_check: id_check,
        repor_super: repor_super
    }, function(data, textStatus) {
        data = JSON.parse(data);
        let info_visita = data.datos_visita;
        $.each(info_visita, function(index, row) {
            $("#id_pos").html(row.punto);
            $("#distri_nombre").html(row.nom_distri);
            $("#nom_vende").html(row.nombre_usuario);
            $("#fecha_visita").html(row.fecha_check);
            $("#hora_ini_visita").html(row.hora_ini);
            $("#hora_fin_visita").html(row.hora_fin);
            if (row.fecha_sincroniza != "N/A") {
                $(".fecha_sincroniza").show();
                $(".fecha_sincroniza_l").show();
                $(".fecha_sincroniza").html(row.fecha_sincroniza);
            } else {
                $(".fecha_sincroniza").hide();
                $(".fecha_sincroniza_l").hide();
            }
            if (row.hora_sincroniza != "N/A") {
                $(".hora_sincroniza").show();
                $(".hora_sincroniza_l").show();
                $(".hora_sincroniza").html(row.hora_sincroniza);
            } else {
                $(".hora_sincroniza").hide();
                $(".hora_sincroniza_l").hide();
            }
            $("#tiempo_visita").html(row.tiempo_visi);
            $("#motivo_visita").html(row.motivo_visi);
            $("#obs_visita").html(row.obs_visi);
            $("#nombrepdv").html(row.nombre_pdv);
            $("#circuito").html(row.circuito);
            $("#ruta").html(row.ruta);
            $("#obs_visita").show();

            if (row.posicion_simulada == "1") {
                $("#geo-referencia-simulada").show();
            } else {
                $("#geo-referencia-simulada").hide();
            }

            $("#visita_realizada_punto").html(row.visitaEnPunto);
            $("#visita_realizada_qr").html(row.qr);

            if (row.observacion_qr != "") {
                $("#obs_qr").html(row.observacion_qr);
                $("#obs_qr").show();
            } else {
                $("#obs_qr").hide();
            }

            zona = row.id_zona;
            if (row.tipo == 1) {
                $("#extraruta_check").attr("style", "display: none;");
                $("#duplicada_check").attr("style", "display: none;");
                $("#ruta_check").attr("style", "");
            } else if (row.tipo == 2) {
                $("#ruta_check").attr("style", "display: none;");
                $("#duplicada_check").attr("style", "display: none;");
                $("#extraruta_check").attr("style", "");
            } else if (row.tipo == 3) {
                $("#ruta_check").attr("style", "display: none;");
                $("#extraruta_check").attr("style", "display: none;");
                $("#duplicada_check").attr("style", "");
            } else {
                $("#ruta_check").attr("style", "display: none;");
                $("#duplicada_check").attr("style", "display: none;");
                $("#extraruta_check").attr("style", "display: none;");
            }
            if (row.obs_visi == "") {
                $("#obs_visita").hide();
            }
            $("#mapa_visita").attr("src", "mapas/usuario-punto.php?longPoint=" + parseFloat(row.long_pdv) + "&latPoint=" + parseFloat(row.lat_pdv) + "&longUser=" + parseFloat(row.lng) + "&latUser=" + parseFloat(row.lat) + "&precisionUser=" + parseFloat(row.margen_e));
            img = "static/img/not_img.png";
            if (row.evidencia_validar == "1") {
                if (row.img_evidencia != "") {
                    img = "data:image/jpg;base64," + row.img_evidencia;
                }
                $("#ver_evidencia").show();
            } else {
                $("#ver_evidencia").hide();
            }
            $("#no_imagen").html("");
            if (row.evidencia_eliminada == 1) {
                $("#no_imagen").html("El usuario: <span style=\"color:red\">" + $("#nom_vende").text() + "</span> ha eliminado la imagen");
            }
            $("#img_evidencia").html("<img id=\"img_evid\" src = \"" + img + "\" border=\"0\" style=\"width: 250px;\" />");
        });
        if ($.fn.DataTable.isDataTable("#resumen_visita")) {
            dtable2.clear();
            dtable2.destroy();
        }
        let columnas = [
            { "data": "pn" },
            { "data": "producto" },
            { "data": "tipo_compra" },
            { "data": "cantidad" },
            { "data": "pedido" }
        ];
        columnas_defect = [];
        funtion_table = "";
        dtable2 = $("#resumen_visita").DataTable({
            data: data.datos_ventas,
            bFilter: !1,
            columns: columnas,
            columnDefs: columnas_defect,
            fnDrawCallback: funtion_table
        });
        id_checks.push(id_check);
        cargar_frecuencia(zona, id_check);
        loader_dw.hide();
        $(".loading-indicator-wrapper").css("display", "none");
    });
}


function cargar_frecuencia(zona,id_check){
  $.post('funciones/controlador.php',
    {
      accion: 'cargar_frecuencia',
      zona:zona

    },
    function(data, textStatus) {
      data = JSON.parse(data);
        var frecuencia='';
        if (data[0]['lunes']==1) {
          if(frecuencia!=''){
            frecuencia+=', ';
          }
          frecuencia+='lunes'
        }
        if (data[0]['martes']==1) {
          if(frecuencia!=''){
            frecuencia+=', ';
          }
          frecuencia+='martes'
        }
        if (data[0]['miercoles']==1) {
          if(frecuencia!=''){
            frecuencia+=', ';
          }
          frecuencia+='miercoles'
        }
        if (data[0]['jueves']==1) {
          if(frecuencia!=''){
            frecuencia+=', ';
          }
          frecuencia+='jueves'
        }
        if (data[0]['viernes']==1) {
          if(frecuencia!=''){
            frecuencia+=', ';
          }
          frecuencia+='viernes'
        }
        if (data[0]['sabado']==1) {
          if(frecuencia!=''){
            frecuencia+=', ';
          }
          frecuencia+='sabado'
        }
        if (data[0]['domingo']==1) {
          if(frecuencia!=''){
            frecuencia+=', ';
          }
          frecuencia+='domingo'
        }
        if(frecuencia==''){

          frecuencia=data[0]['fecha'];
        }
        $("#frecuencia").html(frecuencia);
        obtener_proceso(id_check);
    }
  );
}


function obtener_proceso(id_check){
    $.post('funciones/controlador.php',
      {
        accion: 'obtener_proceso',
        id_check:id_check
      },function(data, textStatus) {
         data = JSON.parse(data);
        if ($.fn.DataTable.isDataTable('#proceso')) {
          dtable3.clear();
          dtable3.destroy();
        }
  
        var columnas = [
          { "data": "PROCESO" },
          { "data": "hora_proceso" }
        ];
  
        columnas_defect = [];
  
        funtion_table = function () {};
  
        dtable3 = $('#proceso').DataTable({
           "order": [[ 1, "asc" ]],
            data: data,
            bFilter: false,
            columns: columnas,
            columnDefs:columnas_defect,
            fnDrawCallback: funtion_table
        } );
  
  
        loader_dw.hide();
  
      });
    //////////////////////////////////
  }

  /* NOTIFICACIONES CONTACTO */

$("#notiNews2").click(function () {
    notificaciones_contacto();
});

function count_noti_contacto() {

    $.post('modulos/vista_market_place_v1.0.0/controlador.php',
        {
            accion: 'consultar_notificacion'
        },
        function (data, textStatus) {

            var datos = JSON.parse(data);
            var notiNum = parseInt(datos['cant_noti']);

            if (notiNum !== null && notiNum != "" && notiNum > 0) {
                if ($("#id_notif").length > 0) {
                    $("#id_notif").html(notiNum);
                } else {
                    $("#notiNews2").append('<span class="badge red" id = "id_notif" style="margin: 7px 25px 0;">' + notiNum + '</span>');
                }
            } else {
                $("#id_notif").remove();
            }
        });
}

function notificaciones_contacto() {

    $("#showNoti").slideToggle();
    var html = "";

    $.post('modulos/vista_market_place_v1.0.0/controlador.php',
        {
            accion: 'consultar_notificacion'
        },
        function (data, textStatus) {

            var datos = JSON.parse(data);
            var info = datos['carga_noti'];

            $.each(info, function (index, fila) {

                var back = "#fff";
                var negrita = "400";
                var size = "12px";
                var text = "<i class='fas fa-envelope-open' style='font-size:15px;'></i>";
                var title = "Leído";

                if (fila.revisado == 0) {
                    back = "#DFDFDF";
                    negrita = "bold";
                    size = "14px";
                    text = "<i class='fas fa-envelope' style='font-size:15px;'></i>";
                    title = "No leído";
                }
                html += '<li class="view_deta" title="'+title+'" style="font-weight: ' + negrita + ';background: ' + back + '" data-id_notif = "' + fila.id + '" id = "notf_' + fila.id + '"><a href="#" style="color: #555;font-size: 13px;" id = "detaNoti_' + fila.id + '"><div style="float:left;width: 205px;font-size:' + size + ';">' + fila.titulo + '</div><div style="position: relative;font-size: 10px;top: 0px;float:left;">' + text + '</div><i class="fa fa-angle-left pull-right" id="icon-arrow-noti_' + fila.id + '"></i></a>';
                html += '<ul class="menu" id="ul_noty_' + fila.id + '" style="width: 100%;height: 160px;padding: 10px;display:none;background: #fff;">hola</ul>';
                html += '</li>';
            });

            $("#ul_noty").html(html);

            $(".view_deta").click(function () {
                var id_notif = $(this).data("id_notif");
                view_deta(info, id_notif);
            });

            if ($("#showNoti").is(":visible")) {
                $(".main-sidebar, .main-footer, .content-wrapper").click(function () {
                    $("#showNoti").slideUp();
                });
            }
        }
    );
}

function view_deta(info, id_notif) {

    var html = "";

    if ($("#ul_noty_" + id_notif).is(":visible")) {
        $("#ul_noty_" + id_notif).slideUp();
        $("#icon-arrow-noti_" + id_notif).removeAttr("style");
        $("#detaNoti_" + id_notif).css("border-bottom", "0");
    } else {
        $("#ul_noty_" + id_notif).slideDown();
        $("#icon-arrow-noti_" + id_notif).removeAttr("style");
        $("#icon-arrow-noti_" + id_notif).css("transform", "rotate(-90deg)");
        $("#detaNoti_" + id_notif).css("border-bottom", "1px solid rgb(195, 195, 195)");
        $("#notf_" + id_notif).css("background", "#fff");

        $.each(info, function (index, fila) {
            if (fila.id == id_notif) {
                var style_content = "border: 1px solid #e4e4e4;padding: 5px;-webkit-box-shadow: 0 1px 2px rgba(0,0,0,.2);box-shadow: 0 1px 2px rgba(0,0,0,.2);-webkit-border-radius: 3px;border-radius: 3px;";
                var style_t = "font-size: 14px;padding: 5px;padding: 5px;font-weight: bold;";
                var style_m = "font-size: 13px;padding: 5px;padding: 5px;";
                var style_ti = "font-size: 11px;padding: 5px;padding: 5px;text-align:right;";

                html += '<div style="' + style_content + '">';
                html += '<div style="' + style_t + '">' + fila.titulo + '</div>';
                html += '<div style="' + style_m + '"><img src="static/img/referencias/publicaciones/' + fila.nombre_imagen + '"  id="tipo_acc" class="img-thumbnail" style="width:60px;height:50px"></div>';
                // html += '<div><figcaption style="text-align:center;" onclick="eliminar_solicitud(' + fila.id + ')"> <i class="fas fa-trash-alt" style="cursor:pointer;float: left;margin-top: 5px;margin-left:1px;font-size: 20px;" title="Descartar solicitud"></i> </figcaption><figcaption style="text-align:center;" onclick="responder_solicitud(' + fila.id + ')"> <i class="fas fa-reply-all" style="cursor:pointer;float: left;margin-top: 5px;margin-left:10px;font-size: 20px;" title="Responder solicitud"></i> </figcaption><i class="glyphicon glyphicon-plus-sign desDeta" style="float: left;margin-top: 4px;margin-left:10px;font-size: 20px;cursor: pointer;position:relative;"></i></div>';
                html += '<div><figcaption style="text-align:center;" onclick="eliminar_solicitud(' + fila.id + ')"> <i class="fas fa-trash-alt" style="cursor:pointer;float: left;margin-top: 10px;margin-left:12px;font-size: 20px;" title="Descartar solicitud"></i> </figcaption><figcaption style="text-align:center;" onclick="responder_solicitud(' + fila.id + ')"> <i class="fas fa-reply-all" style="cursor:pointer;float: left;margin-top: 10px;margin-left:10px;font-size: 20px;" title="Responder solicitud"></i> </figcaption></div>';
                html += '<div style="' + style_ti + ';margin-top: -63px;"><strong>Usuario: </strong>' + fila.nombre + '</div>';
                html += '<div style="' + style_ti + '"><strong>Celular: </strong>' + fila.celular + '</div>';
                html += '<div style="' + style_ti + '">' + fila.fecha + ' - ' + fila.hora + '</div>';
                html += '<div style="' + style_ti + '">' + fila.ubicacion + '</div>';
                html += '</div>';
            }
        });
        $("#ul_noty_" + id_notif).html(html);
        leer_notificacion(id_notif);
    }
}

function leer_notificacion(id_notif) {

    $.post('modulos/vista_market_place_v1.0.0/controlador.php',
        {
            accion: 'leer_notificacion',
            id_notif: id_notif
        },
        function (data, textStatus) {
            if (data > 0) {
                if ($("#id_notif").length > 0) {
                    count_noti_contacto();
                }
            }
        });
}

function eliminar_solicitud(id) {

    BootstrapDialog.confirm("¿Esta seguro que desea descartar la solicitud?", function (result) {
        if (result) {

            $.post('modulos/vista_market_place_v1.0.0/controlador.php',
                {
                    accion: 'eliminar_solicitud',
                    id: id
                }, function (data, textStatus) {
                    if (data == 1) {
                        Notificacion("La solicitud no se ha podido descartar", "error");
                    } else {
                        Notificacion("La solicitud se descarto correctamente", "success");
                        notificaciones_contacto();
                    }
                });
        }
    });
}

//////////////////////////////////

function vaciar_carrito(){

    
    BootstrapDialog.confirm("¿Esta seguro de vaciar el carrito de compras?", function (result) {
        if (result) {

            $.post('funciones/controlador.php',{
                    accion: 'vaciar_carrito'
                }, function (data, textStatus) {
                    if(data == "1"){ 
                        Notificacion("No tienes productos en el carrito", "warnign");
                    }else{
                        Notificacion("Carrito vaciado exitosamente", "success"); 
                        $("#num_pro_car").html("0");
                        $("#tbl_carrito tbody tr").remove();
                    }
                });
        }
    });
   

}

function ver_carrito(){

    $("#ver_carrito_compras").show();
    $("#modal-carrito-productos").modal("show");
    loader_dw = $("#modal-carrito-productos").loadingIndicator({ useImage: !1 }).data("loadingIndicator");
    loader_dw.show();
    loader_dw.hide();
    $("#tbl_carrito tbody tr").remove();
 
    $.post("funciones/controlador.php", {
        accion: "ver_carrito"
    }, function(data, textStatus) {
        data = JSON.parse(data);
        var subtotal = 0;
        var subtotal_all = 0;
        if(data != 0){
            $.each(data,function(i){
            if(data[i] != null){ 
                subtotal = data[i].cantidad.toString().replace(/\D/g, '') * data[i].precio.toString().replace(/\D/g, '');
                subtotal_all += data[i].cantidad.toString().replace(/\D/g, '') * data[i].precio.toString().replace(/\D/g, '');
                
                $("#tbl_carrito tbody").append("<tr class='producto'><td><h3>"+data[i].nombre+"</h3></td><td class='text-center'><a class='trsn'><img src='"+data[i].imagen+"' style='100px;height: 100px'; ></a></td><td class='mob-hide' style='vertical-align: inherit;'><span class='order-product-price'>"+data[i].precio+"</span></td> <td><div class='input-group' style='width: fit-content;margin-top: 35px;'><span class='input-group-addon' style='cursor: pointer;' onclick='cantidades_modal(0,$($(this)))'>-</span><input type='text' class='form-control-sm' style='width: 70px;text-align:center;vertical-align: text-top;' value='"+data[i].cantidad+"' autocomplete='off' onblur='cantidades_defecto($(this),"+data[i].id+")'  onkeyup='formato(this),cambiar_cantidades($(this),"+data[i].id+")'><span class='input-group-addon' style='cursor: pointer;' onclick='cantidades_modal(1,$(this))'>+</span></div></td><td style='vertical-align: inherit;'><span class='order-product-subtotal'>$ "+formato_numero(subtotal,0,',','.')+"</span></td><td style='vertical-align: inherit;' ><button type='button' onclick='eliminar_producto($(this),"+data[i].id+")' class='btn btn-sm btn-danger delete'><i class='glyphicon glyphicon-trash' title='Eliminar de Mis Compras'></i></button></td></tr>"); 
            }
            });
            $("#tbl_carrito tbody").append("<tr><td colspan='5' align='right'><strong>Total Compra:</strong><span id='total_all'> $ "+formato_numero(subtotal_all,0,',','.')+"</span></td><td></td></tr>");
        }else{
            $("#tbl_carrito tbody").append("<tr><td colspan='6' align='center'><h3 style='margin-top:100px' class='animate__animated animate__shakeY'>Tu carrito de compras está vacio. Agrega productos ahora<h3></td></tr>");
        }
       
    });
}

function agregar_carrito(id){

    $("#cargar_carrito_compras").show();
    $("#modal-carrito").modal("show");
    loader_dw = $("#modal-carrito").loadingIndicator({ useImage: !1 }).data("loadingIndicator");
    loader_dw.show();
    loader_dw.hide();
    $("#circle").css("background","#fffff");
    $("#txt_cantidades").val(1);
    $("#nombre_producto").html("");
    $("#precio_producto").html("0");
    $("#marca").html("");
    
    $.post("funciones/controlador.php", {
        accion: "datos_referencia",
        id:id
    }, function(data, textStatus) {
        data = JSON.parse(data);

        $("#imgLarge-modal img").remove();

        $("#id_update-modal").val(data[0]["id"]);
        $("#id_referencia-modal").val(data[0]["id_referencia"]);
        
        $("#nombre_producto").html(data[0]["producto"]);
        $("#precio_producto").html("$ " + formato_numero(data[0]["valor_referencia"], 0, ',', '.'));
        $("#marca").html("<strong>Marca:</strong> " +data[0]["marca"]);
        $("#atributos").html("<strong>Atributos:</strong> " +data[0]["atributos"]);
        $(".description-content-modal").html(data[0]["descripcion"]);
        
        $("#circle").css("background",data[0]["color"]);
        cargar_imagenes_modal(data[0]["id_referencia"]);
    });
}
  

  function cargar_imagenes_modal(id) {
  
    $(".gallery-body-modal").remove();    
    $(".gallerySlide-modal").remove();
    imgCont_edit = 0;
  
    $.post('funciones/controlador.php',
        {
            accion: 'cargar_imagenes',
            id: id
        },
        function (data) {
            data = JSON.parse(data);
            var i = 1;
            $.each(data, function (index, row) {
                imgCont_edit++;
                var imgSmall = $('<div class="gallery-body-modal">  <img src="./../claro/distribuidor/static/img/referencias/productos/' + row.nombre + '" alt="Foto del usuario" class="selectImg-modal selectImg-opacityx selectImg-hover-opacity-off" onclick="currentDiv_modal(' + imgCont_edit + ')" style="width:100%;height:100%;border: 2px solid #000;cursor:pointer;border-radius: 10px;">  </div>');
                if (i == 1) {
                    var imgLarge = $('<img id="foto_producto" src="./../claro/distribuidor/static/img/referencias/productos/' + row.nombre + '" alt="Foto del usuario" class="image-preview-edit gallerySlide-modal" style="width:100%; display:block">');
                    $("#not_img_modal").hide();
                } else {
                    var imgLarge = $('<img src="./../claro/distribuidor/static/img/referencias/productos/' + row.nombre + '" alt="Foto del usuario" class="image-preview-edit gallerySlide-modal" style="width:100%; display:none">');
                    $("#not_img_modal").hide();
                }
                  
                $(imgSmall).insertBefore("#add-photo-container-modal");
                $(imgLarge).insertBefore("#imgLarge-modal");
                $("#not_img_modal").hide();
                i = i +1;
            });
        });
  }


  function cantidades(accion){

    if(accion == 0){
       if(parseInt($("#txt_cantidades").val()) == 1){
           return false;
       }
       $("#txt_cantidades").val(parseInt($("#txt_cantidades").val()) - 1);
    }else{
        $("#txt_cantidades").val(parseInt($("#txt_cantidades").val()) + 1);
    }
  }

  function cantidades_modal(accion,elem){
    
    var cantidadad = 0;
    if(accion == 0){
       if(parseInt(elem.next().val()) == 1){
           return false;
       }
       elem.next().val(parseInt(elem.next().val()) - 1);
       cantidadad = elem.next().val();
    }else{
        elem.prev().val(parseInt(elem.prev().val()) + 1);
        cantidadad = elem.prev().val();
    }


        var precio_uniatario = elem.closest("td").prev().html().replace(/\D/g,'');
        var sub_total = formato_numero(cantidadad * precio_uniatario,0,',','.'); 
        elem.closest("td").next().html("$ "+sub_total);

        var total_all = 0;
        var cantidades = 0;
        var precio_uni = 0;
       
        $("#tbl_carrito tbody tr.producto").each(function(i){
            cantidades = ($(this).find("td").eq(3).find(":input[type='text']").val().replace(/\D/g,'') == "") ? 0 : $(this).find("td").eq(3).find(":input[type='text']").val().replace(/\D/g,'');
            precio_uni = ($(this).find("td").eq(2).html().replace(/\D/g,'') == "") ? 0 : $(this).find("td").eq(2).html().replace(/\D/g,'');
            total_all += cantidades * precio_uni;
        });
        $("#total_all").html(" $ "+ formato_numero( total_all,0,',','.'));

  }

  function add_carrito(){

    var id         = $("#id_update-modal").val();
    var id_referencia = $("#id_referencia-modal").val();
    var cantidad   = $("#txt_cantidades").val();
    var nombre     = $("#nombre_producto").html();
    var precio     = $("#precio_producto").html();
    var marca      = $("#marca").html();
    var imagen     = $("#foto_producto").attr("src");
    
    $.post('funciones/controlador.php',
    {
        accion: 'add_carrito',
        id: id,
        id_referencia: id_referencia,
        cantidad: cantidad,
        nombre: nombre,
        precio: precio,
        marca: marca,
        imagen: imagen
    },
    function (data) {
         $("#modal-carrito").modal("hide");
         $("#num_pro_car").html(data);
    });


  }


  function cambiar_cantidades(elem,id2){

    var cantidad2 = elem.val().replace(/\D/g,'');
    $.post('funciones/controlador.php',
    {
        accion: 'add_carrito',
        id2: id2,
        cantidad2: cantidad2
    },
    function (data) {
        var precio_uniatario = elem.closest("td").prev().html().replace(/\D/g,'');
        var sub_total = formato_numero(cantidad2 * precio_uniatario,0,',','.'); 
        elem.closest("td").next().html("$ "+sub_total);
        $("#num_pro_car").html(data);

        var total_all = 0;
        var cantidades = 0;
        var precio_uni = 0;
       
        $("#tbl_carrito tbody tr.producto").each(function(i){
            cantidades = ($(this).find("td").eq(3).find(":input[type='text']").val().replace(/\D/g,'') == "") ? 0 : $(this).find("td").eq(3).find(":input[type='text']").val().replace(/\D/g,'');
            precio_uni = ($(this).find("td").eq(2).html().replace(/\D/g,'') == "") ? 0 : $(this).find("td").eq(2).html().replace(/\D/g,'');
            total_all += cantidades * precio_uni;
        });
        $("#total_all").html(" $ "+ formato_numero( total_all,0,',','.'));
    });
  }


  function cantidades_defecto(elem,id2){
 
  if(elem.val() == ""){ 
    var cantidad2 = 1;
    $.post('funciones/controlador.php',
    {
        accion: 'add_carrito',
        id2: id2,
        cantidad2: cantidad2
    },
    function (data) {
        var precio_uniatario = elem.closest("td").prev().html().replace(/\D/g,'');
        var sub_total = formato_numero(cantidad2 * precio_uniatario,0,',','.'); 
        elem.closest("td").next().html(sub_total);
        elem.val(1);
        $("#num_pro_car").html(data);
    });
   }

  }

  function eliminar_producto(elem,id3){
     
    $.post('funciones/controlador.php',{
        accion: 'add_carrito',
        id3: id3
    },
    function (data) {
        elem.closest("tr").remove();
        $("#num_pro_car").html(data);

        var total_all = 0;
        var cantidades = 0;
        var precio_uni = 0;
       
        $("#tbl_carrito tbody tr.producto").each(function(i){
            cantidades = ($(this).find("td").eq(3).find(":input[type='text']").val().replace(/\D/g,'') == "") ? 0 : $(this).find("td").eq(3).find(":input[type='text']").val().replace(/\D/g,'');
            precio_uni = ($(this).find("td").eq(2).html().replace(/\D/g,'') == "") ? 0 : $(this).find("td").eq(2).html().replace(/\D/g,'');
            total_all += cantidades * precio_uni;
        });
        $("#total_all").html(" $ "+ formato_numero( total_all,0,',','.'));
    });
  }
  
  function hacer_pedido(){
     
    var fila = $("#tbl_carrito tbody tr").find("td").eq(1).html();
    if (fila === undefined) {
        Notificacion("No hay productos para hacer la compra","warning");
        return false;
    }
    
    $("#btn_pedido").prop("disabled", true);
    $.post('funciones/controlador.php',
    {
        accion: 'hacer_pedido'
    },
    function (data) {
        if(data == 0){ 
            Notificacion("Pedido agregado exitosamente","success");
            setTimeout(function(){ window.location.reload(); }, 2000);
        }else{
            $("#btn_pedido").prop("disabled", false);
        }
    });

  }
  
  function currentDiv_modal(n) {
    showDivs_modal(slideIndex = n);
  }
  
  function showDivs_modal(n) {
    var i;
    var x = document.getElementsByClassName("gallerySlide-modal");
    var dots = document.getElementsByClassName("selectImg-modal");
    if (n > x.length) { slideIndex = 1 }
    if (n < 1) { slideIndex = x.length }
    for (i = 0; i < x.length; i++) {
        x[i].style.display = "none";
    }
    for (i = 0; i < dots.length; i++) {
        dots[i].className = dots[i].className.replace(" selectImg-opacity-off", "");
    }
    x[slideIndex - 1].style.display = "block";
    dots[slideIndex - 1].className += " selectImg-opacity-off";
  }

  function formato(input) {

	var num = input.value.replace(/\./g, '');
	if (!isNaN(num)) {
		num = num.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g, '$1.');
		num = num.split('').reverse().join('').replace(/^[\.]/, '');
		input.value = num;
	} else {
		input.value = input.value.replace(/[^\d\.]*/g, '');
	}
}
  
 
