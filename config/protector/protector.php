<?php
  require("LogAtack.php");
  
  class Protector
  {	
	  var $XML_LOG; 
	  var $SHOW_ERRORS;  
	  var $lg;  
		  
	  public function Protector($log_path, $show_errors){
		  $this->XML_LOG = $log_path;
		  $this->SHOW_ERRORS=$show_errors;
		  $this->lg = new LogAtack($this->XML_LOG); 
		 /* if ($this->SHOW_ERRORS){
			  error_reporting(E_ERROR | E_WARNING | E_PARSE); 
			  ini_set(display_errors, "1"); 
		  }else{
			   ini_set(display_errors, "0");
			  ini_set(log_errors, "1");  
		  }*/
	  }
	    
	  public function isMalicious(){		
		 $sqli = false;
		 $num_bad_words1 = $this->CheckGet();
		 $num_bad_words2 = $this->CheckPost();
		  if ($num_bad_words1[0] > 1){ 
			  $this->lg->LogData($num_bad_words1[0], 1,$num_bad_words1[1]);
			  $sqli = true;
		  }
		  if ($num_bad_words2[0] > 1){ 
				$this->lg->LogData($num_bad_words2[0], 2,$num_bad_words2[1]);
			  $sqli = true;
		  } 
		  return $sqli;
	  }
  
	  private function CheckPost() {
		$num_bad_words = 0;
		$palabras_bad = "bad_words[ ";
		foreach ($_POST as $campo => $input) {
		  if (is_array($input)) {
			foreach ($input as $datos => $valor) {
			  if (is_array($input) || is_object($input)) {
				foreach ((array) $valor as $datos_sub => $valor_sub) {
				  if (is_array($valor_sub)) {
					$valor_sub = implode(",", $valor_sub);
				}
				  $valor_sub = htmlentities((string) $valor_sub, ENT_QUOTES, 'ISO-8859-1');
				  $array_palabras = $this->wordExists($valor_sub);
				  $num_bad_words = $num_bad_words + $array_palabras[0];
				  $palabras_bad .= $array_palabras[1];
				}
			  } else {
				$valor = htmlentities((string) $valor, ENT_QUOTES, 'ISO-8859-1');
				$array_palabras = $this->wordExists($valor);
				$num_bad_words = $num_bad_words + $array_palabras[0];
				$palabras_bad .= $array_palabras[1];
			  }
			}
		  } else {
			$array_palabras = $this->wordExists($input);
			$num_bad_words = $num_bad_words + $array_palabras[0];
			$palabras_bad .= $array_palabras[1];
		  }
	
		}
	
		$palabras_bad .= " ]";
		return array($num_bad_words, $palabras_bad);
	  }
	  
	  private function CheckGet(){
		  $num_bad_words = 0;
		  $palabras_bad = "bad_words[ ";	
			  foreach($_GET as $campo => $input){
				 $array_palabras = $this->wordExists($input);
				   $num_bad_words = $num_bad_words + $array_palabras[0];
				   $palabras_bad .= $array_palabras[1]; 
			  }
			 $palabras_bad .= " ]";		
		  return array($num_bad_words,$palabras_bad);		 				  	
	  }
	  
		  
	  private function wordExists($input){	 
	   	      $palabrasbad = "";
		  $num_bad_words = 0;	
		  //////// aqui se toman las palabras al comienzo de la inyeccion 
		  $baddelim1 = "[^a-z]*";  
		  $baddelim2 = "[^a-z]+";
	  $badwords= array("select", "show", "insert", "update", "delete", " drop", "truncate", "create", "load_file", "exec",  "--",  "'", "`", '"',  "___", "show", "isnull", "null","cast", "0x20", "schema", "sleep", "distinct", "information","@@version", "@@datadir", "version","benchmark", "--", "varchar", "convert", "char", "limit", "information_schema","table_name", "from", "where", "order", "like", "elt", "boolean","concat", "floor", "end","=");
		  foreach($badwords as $badword){ 
			  $expression = "/".$baddelim1.strtolower($badword).$baddelim2."/";
			  if (preg_match ($expression, strtolower($input))) {
				  $num_bad_words++;
				  $palabrasbad .= $badword." + ";
			  }
		  }	
		
		//////// aqui se toman las palabras al final de la inyeccion		  
		  $baddelim1 = "[^a-z]+";  
		  $baddelim2 = "[^a-z]*";
		  $badwords= array("select", "show", "insert", "update", "delete", " drop", "truncate", "create", "load_file", "exec",  "--",  "'", "`", '"',  "___", "show", "isnull", "null",  "0x20", "schema", "sleep", "distinct", "information","@@version", "@@datadir", "version","benchmark", "--", "varchar", "convert", "limit", "information_schema","table_name", "from", "where", "order", "like", "elt", "boolean","concat", "floor", "end", "rand","=");
		  foreach($badwords as $badword){ 
			  $expression = "/".$baddelim1.strtolower($badword).$baddelim2."/";
			  if (preg_match ($expression, strtolower($input))) {
				  $num_bad_words++;
				  $palabrasbad .= $badword." + ";
			  }
		  }	
		  
		  //////// aqui se toman las palabras solas de la inyeccion
		  $baddelim1 = "[^a-z-ZñÑáéíóúÁÉÍÓÚ]+";  
		  $baddelim2 = "[^a-z-ZñÑáéíóúÁÉÍÓÚ]+";
		  $badwords= array("select", "show", "insert", "update", "delete", " drop", "truncate", "create", "load_file", "exec",  "--",  "'", "`", '"',  "___", "show", "isnull", "null", "cast", "0x20", "schema", "sleep", "distinct", "information","@@version", "@@datadir", "user", "version","benchmark", "--", "varchar", "convert", "char", "limit", "information_schema","table_name", "from", "where", "order", "like", "elt", "boolean","concat", "floor",  "end", "rand","or","=","and");
		  foreach($badwords as $badword){ 
			  $expression = "/".$baddelim1.strtolower($badword).$baddelim2."/";
			  if (preg_match ($expression, strtolower($input))) {
				  $num_bad_words++;
				  $palabrasbad .= $badword." + ";
			  }
		  }	 
		  return array($num_bad_words,$palabrasbad);
	  }
	  
	  
	  private function isIDInjection($campo,$input){
		  $reg="/^idexo/";		  
			  if(preg_match($reg, $campo)){
				  if(!$this->stringIsNumberNotZero($input) || $input == ''){
					  return true;   
				  }   
			  }	
			  
		  return false;
	  }
	  
	  
	  private function stringIsNumberNotZero( $string ){
	  $i=0;
		  while ( $i < strlen($string) ){
			  if ($string{$i} == "0" && $i==0)
				  return false;
			  if ( $string{$i} != "0" && 
				  $string{$i} != "1" && 
				  $string{$i} != "2" && 
				  $string{$i} != "3" && 
				  $string{$i} != "4" && 
				  $string{$i} != "5" && 
				  $string{$i} != "6" && 
				  $string{$i} != "7" && 
				  $string{$i} != "8" && 
				  $string{$i} != "9"  ) 
			  return false;
			  $i++;
		  }
	  
	  return true;
	  }
	  	  
  } 
?>
