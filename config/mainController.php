<?php
require 'config.php';
require 'mainTemplate.php';
require("log_inc.php");
//ini_set('display_errors', false);

register_shutdown_function(function(){
	/*$error = error_get_last();
		if(null !== $error) {
		// $error ( [type] , [message] , [file] , [line] )
			echo "<div style='font-size:11px'><b>Mensaje: </b>".$error['message']."<br>";
			echo "<b>Archivo: </b>".$error['file']."<br>";
			echo "<b>Linea: </b>".$error['line']."<br></div>";
		}*/
	});
class mainController
{

	function __construct(){
		//GUARDADO DEL LOG DE USUARIO
			if((isset($_SESSION[$GLOBALS["SESION_POS"]]) && $_SESSION[$GLOBALS["SESION_POS"]])){
				if(isset($GLOBALS["ESTADO_LOG_USUARIO"]) && intval($GLOBALS["ESTADO_LOG_USUARIO"])==1){
					$oSession = unserialize($_SESSION[$GLOBALS["SESION_POS"]]);
					$idUsuario = $oSession->VSidpos;
					$usuario = $oSession->VSlogin;
					$cedula = $oSession->VScedula;
					$id_distribuidor = $oSession->VSid_distri;
					$navegador = $oSession->VSnavegador;
					$log_usu = new Log();
					$log_usu -> audi($idUsuario,$usuario,$cedula,$id_distribuidor,$navegador,$_SESSION["mod"],json_encode($_POST));
				}else{
					unset($_SESSION["mod"]);
				}
			}else{
				if(!isset($_SESSION["rpss"])){
					die(json_encode(array("estadoSesion"=>201),true));
				}
			}
		//GUARDADO DEL LOG DE USUARIO FIN
	}

    function Crear_Archivos_Modulo($modulo,$datos_ejemplo,$descripcion)
	{
			$rutaTemplate = "../../static/templates/";
			//Creacion de la carpeta
			$Template = new Template();
			$carpetaModulo="../".$modulo;

			if(!file_exists($carpetaModulo))
			{
				$Template->makeDir($carpetaModulo);
				if($datos_ejemplo == "N")
				{
					//Creacion de la vista.!
					$vista = $Template->getFile($rutaTemplate."vista_se.html");
					$vista = str_replace("[[modulo]]",strtolower($modulo), $vista);
					$vista = str_replace("[[descripcion_modulo]]",$descripcion, $vista);
					$archivoVista = $carpetaModulo."/vista.html";
					$Template->putFile($archivoVista,$vista);

					//Creacion del Controlador.!
					$controlador = $Template->getFile($rutaTemplate."controlador_se.php");
					$controlador = str_replace("[[modulo]]",strtolower($modulo), $controlador);
					$archivoControlador = $carpetaModulo."/controlador.php";
					$Template->putFile($archivoControlador,$controlador);

					//Creacion del Modelo.!
					$modelo = $Template->getFile($rutaTemplate."modelo_se.php");
					$modelo = str_replace("[[modulo]]",strtolower($modulo), $modelo);
					$archivoModelo = $carpetaModulo."/modelo.php";
					$Template->putFile($archivoModelo,$modelo);

					//Creacion del Js!
					$JS = $Template->getFile($rutaTemplate."controlador_js_se.js");
					$JS = str_replace("[[modulo]]",strtolower($modulo), $JS);
					$archivoJS= $carpetaModulo."/controlador.js";
					$Template->putFile($archivoJS,$JS);

				}
				 try{
					$fecha = date("Y-m-d");
					$hora = date("H:i:s");
					$file1 = false;
					$file2 = false;
					$ip = $_SERVER['REMOTE_ADDR'];
					$datos = "----------- CREACION DE MODULO ------------ \nNombre Modulo: $modulo \nFecha Creacion: $fecha \nHora: $hora \nDireccion I.P: $ip \n \n";

					if(!file_exists("../../config/log_mod/log.txt")) {
						$Template->putFile("../../config/log_mod/log.txt","");
						$archivo=fopen("../../config/log_mod/log.txt","w");
						fwrite($archivo,$datos. PHP_EOL);
						fclose($archivo);
					}else{
						$archivo=fopen("../../config/log_mod/log.txt","a");
						fwrite($archivo,$datos. PHP_EOL);
						fclose($archivo);
					}

				}
				catch(Exception $e){
				  // continue
				}
				return "S";
			}
			else
			{
				return "R";
			}


	}


}
?>