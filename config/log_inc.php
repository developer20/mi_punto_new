<?php
class Log {
	
	function audi($idUsuario="",$usuario = "",$cedula = "",$id_distribuidor= "",$navegador = "", $modulo = "",$data=""){ 

		if(!isset($GLOBALS["LOG_POS"])){
			return;
		}

		if(!file_exists($GLOBALS["LOG_POS"])){
			return;
		}

		$s_logFile = $GLOBALS["LOG_POS"]."pos_".date("Ymd").".log";

		if(file_exists($s_logFile)){

			$ddf = fopen($s_logFile,'a'); 
			//CABECERAS
			//Fecha,Hora,Id_Usuario,Cedula_Usuario,Nombre_Usuario,Id_Perfil,Nombre_Perfil,Ip,Modulo,Datos
			
			fwrite($ddf,date("Y")."-".date("m")."-".date("d")."|".date("H").":".date("i").":".date("s")."|".$idUsuario."|".$cedula."|".$usuario."|".$id_distribuidor."|".$navegador."|".$_SERVER["REMOTE_ADDR"]."|$modulo|$data".PHP_EOL);
			fclose($ddf);	

		}else{
			
			$ddf = fopen($s_logFile,'x+'); 
			
			fwrite($ddf,date("Y")."-".date("m")."-".date("d")."|".date("H").":".date("i").":".date("s")."|".$idUsuario."|".$cedula."|".$usuario."|".$id_distribuidor."|".$navegador."|".$_SERVER["REMOTE_ADDR"]."|$modulo|$data".PHP_EOL);
			fclose($ddf);
		}

	}

	

}