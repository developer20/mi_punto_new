<?php
include("var_config.php");
include("protector/protector.php");

$prot= new Protector("C:/logs_mov_colombia/log_protector_puntorec.txt", false);

if($prot->isMalicious()){
	//header("location: ../../index.php");  //si se encuentra inyecion se va por ac
	$response = array("draw" =>0,
			"recordsTotal" => "p",
			"recordsFiltered" => 0,
			"data" => array());
	$response = json_encode($response);
	die($response);
	//echo "ataque";
}

include('adodb/adodb.inc.php');
class BD
{

var $servidor;
var $usuario;
var $password;
var $dtbs;
var $conexion;
var $respuesta;
var $filas;
var $db;
var $res;

/*CONSTRUCTOR DE LA CLASE BASE DE DATOS QUE INICIALIZA LAS VARIABLES DE LA CLASE*/
 FUNCTION BD(){
	$bd=$GLOBALS["BD_POS"];
    $this->servidor=$GLOBALS["SERVER"];
	$this->usuario=$GLOBALS["SERVER_USER"];
	$this->password=$GLOBALS["SERVER_PASS"];
	$this->dtbs=$bd;
 }

/*FUNCION QUE CUENTA EL NUMERO DE REGISTROS DE UNA CONSULTA DADA*/
 FUNCTION numreg($consulta){
    RETURN $consulta->RecordCount();
 }

/*FUNCION CONECTAR, QUE REALIZA LA CONEXION A LA BASE DE DATOS Y LA SELECION DE LA MISMA */
 FUNCTION conectar(){
    $this->db= NewADOConnection("mysql");
	$this->conexion=$this->db->Connect($this->servidor,$this->usuario,$this->password,$this->dtbs)or die("Unable to connect!");
	$this->setCharset();
    RETURN $this->conexion;}

/*FUNCION CONSULTAR QUE PERMITE REALIZAR LAS CONSULTAS A LA BASE DE DATOS Y RETORNA LA RESPUESTA*/
 FUNCTION consultar($sql){
    $this->res = $this->db->Execute($sql);
	if(!$this->res){
		$this->escribir_log($sql,$this->msn_error()) ;
	}
	RETURN $this->res;
}

public function last_insert_id () {
    return $this->db->Insert_ID();
  }

  public function setCharset ($parameter = "utf8") {
    return $this->db->setCharset($parameter);
  }

/// funcion para escribir log en caso de error
FUNCTION escribir_log($sql,$error){
    try{
		$fecha = date("Y-m-d");
		$hora = date("H:i:s");
		$file1 =false;
		$ruta = "/Applications/MAMP/htdocs/movistar_uruguay/puntorecarga/config/";
		if(file_exists($ruta)){
			$file1 = true;
		}
		if($file1){

		    if(!file_exists($ruta."/logs_sql/log.txt")) {
				$archivo=fopen($ruta."/logs_sql/log.txt","w");
				fwrite($archivo,$fecha."|".$hora."|".$sql."|".$error."|".$GLOBALS["BD_NAME"]. PHP_EOL);
				fclose($archivo);
			}else{
				$archivo=fopen($ruta."/logs_sql/log.txt","a");
				fwrite($archivo,$fecha."|".$hora."|".$sql."|".$error."|".$GLOBALS["BD_NAME"]. PHP_EOL);
				fclose($archivo);
			}
		}
	}
	catch(Exception $e){
	  // continue
	}
}

function msn_error(){
	RETURN $this->db->ErrorMsg();
 }

/*FUNCION QUE DEVUELVE EL ID DE LA SECUENCIA*/
 FUNCTION id($sec){
    RETURN $this->db->GenID($sec);}

/* FUNCION PARA EXTRAER LA IMAGEN*/
 FUNCTION ManejoArchivo($img,$TamImag){
   if ($img != "none" ){
   	$fp =fopen($img, "rb");
    $imagen =fread($fp, $TamImag);
    $imagen =addslashes($imagen);
    RETURN $imagen;
    fclose($fp);}}

/* FUNCION PARA EVITAR LA INYECCION DE SQL*/
 FUNCTION InyeccionSql($cadena){
	$invalido=array(";"=>" ","'"=>" "," alter "=>" "," drop "=>" "," select "=>" "," from "=>" "," where "=>" "," insert "=>" "," delete "=>" "," * "=>" "," or "=>""," and "
	=>" ","%27"=>" ","table"=>" ");
	$correcto=strtr($cadena,$invalido);
	$correcto=strip_tags($correcto);
	RETURN $correcto;}

/*FUNCION DESCONECTAR QUE CIERRA LA CONEXION CON LA BASE DE DATOS*/
 FUNCTION desconectar(){
    $this->db->Close();
 }


 FUNCTION numupdate(){
   RETURN $this->db->Affected_Rows();
 }

 function devolver_array($sql)
 {
    $this->db->SetFetchMode(ADODB_FETCH_ASSOC);
  	$this->res=$this->db->GetAll($sql);
	if(!$this->res){
		if(strlen($this->msn_error()) > 0){
		   $this->escribir_log($sql,$this->msn_error());
		}
	}
    RETURN $this->res;
 }

//FUNCION PARA CARGAR LA CONFIGURACION GENERAL DEL SISTEMA
	function config_general_sistema(){

		$c_config_gen = "SELECT plataforma_pos, pago_comi, estado_codigo_comi, min_digidos_docu, max_digidos_docu FROM {$GLOBALS["BD_NAME"]}.configuracion_general limit 1;";
		$r_config_gen = $this->devolver_array($c_config_gen);
		if(count($r_config_gen)>0){
			return $r_config_gen[0];
		}else{
			return array("plataforma_pos" => 0,"pago_comi" => 0,"estado_codigo_comi" => 0,"min_digidos_docu" => 0,"max_digidos_docu" => 0);
		}
	}

	function credenciales($id_distri,$id_pro,$codigoBolsa,$folder){

		$user = "";
		$pass = "";
		$t_id = "";
		$com = "";

		$credencial = $codigoBolsa."_".$id_distri."-".$id_pro.".mx";
		$fichero = $GLOBALS["R_CREDENCIAL"].$folder."/".$credencial;

		if(file_exists($fichero)){

			$fichero = fopen($fichero,'r');

			while ( ($linea = fgets($fichero)) !== false) {

				$datos = explode("|",$linea);

				$user = $datos[0];  //user recarga
				$pass = $datos[1]; //pass recarga
				$t_id = $datos[2];  // terminal id recarga
				$com = $datos[3];  // comercio recarga
			}

			fclose($fichero);

		}

		return array("user" => $user, "pass" => $pass,"t_id"=>$t_id,"com"=>$com);
	}

	// fin credenciales

	public function comprimir_archivo ($nombre_zip, $nombre_archivo, $encabezado, $datos, $llaves) {
		//Recibe el nombre del archivo csv
		$archivo = $nombre_archivo;
	
		//Inicia la creacion del archivo
		$ar = fopen($archivo, "w") or die("1");
	
		fwrite($ar, $encabezado);
	
		//Recorre los datos
		if (count($datos) > 0) {
		  for ($i = 0; $i < count($datos); $i ++) {
			fwrite($ar, "\n");
	
			//Recorre la llave de los datos
			for ($j = 0; $j < count($llaves); $j ++) {
			  fwrite($ar, $datos[$i][$llaves[$j]].";");
			}
		  }
		}
	
		//Se cierra la creación del archivo
		fclose($ar);
	
		$size = filesize($archivo);// En byte
		$byte = 1000000;// 1 megabyte
		$megabyte = round($size / $byte, 1);
	
		if ($megabyte >= $GLOBALS["MEGABYTESDESCARGA"]) {
		  //Se crea el objeto zip
		  $zip = new ZipArchive();
	
		  $filename = $nombre_zip;
	
		  if ($zip->open($filename, ZIPARCHIVE::CREATE) === true) {
			$zip->addFile($archivo);
			$zip->close();
		  } else {
			echo 'Error creando '.$filename;
		  }
	
		  if (file_exists($filename)) {
			//Se descarga el archivo zip
			header("Content-Encoding: UTF-8");
			header('Content-type: "application/zip"');
			header('Content-Disposition: attachment; filename="'.$filename.'.zip"');
	
			readfile($filename);
	
			//Se borra el archivo zip
			unlink($filename);
		  }
		} else {
		  header("Content-Encoding: UTF-8");
		  header("Content-type: text/csv; charset=UTF-8");
		  header("Content-Disposition: attachment; filename=".$archivo.";");
		  //echo "\xEF\xBB\xBF";
		  readfile($archivo);
		}
	
		//Se borra el archivo CSV que se genera dentro del modulo
		unlink($archivo);
	  }


	  //FUNCION PARA TRAER LA BD DEL DISTRIBUIDOR

	  function bd_distri(){

        $oSession = unserialize($_SESSION[$GLOBALS["SESION_POS"]]);
		$idDistri = $oSession->VSid_distri;
        			
		$sql_dis = "SELECT nombre_corto FROM {$GLOBALS["BD_NAME"]}.distribuidores WHERE id = $idDistri";
		$query_dis = $this->devolver_array($sql_dis);
		$nombre_corto = $query_dis[0]["nombre_corto"];
		$bd = "{$GLOBALS["BD_DIS"]}" . $nombre_corto . "_" . $idDistri;

        return $bd;
    }

}
?>
