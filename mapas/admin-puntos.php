<?php
// Parámetros enviados por GET
$tipo = (isset($_GET["tipo"])) ? $_GET["tipo"] : "";
$lng = (isset($_GET["lng"])) ? $_GET["lng"] : "0";
$lat = (isset($_GET["lat"])) ? $_GET["lat"] : "0";

// Valida los parámetro
if ($tipo === "2" && ($lng === "0" || $lat === "0")) {
  // Finaliza la ejecución
  exit;
}
?>
<!doctype html>
<html lang="es">
  <head>
    <meta charset="utf-8" />
    <title>Mapa: ver/definir posición del punto</title>
    <link type="text/css" rel="stylesheet" href="../static/css/openlayers/openlayers.css" />
    <link type="text/css" rel="stylesheet" href="../static/css/bootstrap.min.css" />
    <script type="text/javascript" src="../static/js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="../static/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../static/js/openlayers/openlayers.js"></script>

    <style type="text/css">
      body {
        padding: 0;
        margin: 0;
        overflow: hidden;
        font-family: Helvetica, Arial, sans-serif;
      }

      #map {
        width: 100vw;
        height: 100vh;
      }
    </style>
  </head>

  <body>
    <div id="map"></div>

    <!--suppress JSAnnotator -->
    <script type="text/javascript">
      // Coordenadas del punto
      const longPoint = parseFloat(<?php echo $lng; ?>);
      const latPoint = parseFloat(<?php echo $lat; ?>);

      // Información del punto
      let point;
      let coords;

      // Valida las coordenadas
      if (longPoint !== 0 && latPoint !== 0) {
        // Crea el vector del punto
        point = new ol.layer.Vector({
          source: new ol.source.Vector({
            features: [
              new ol.Feature({
                geometry: new ol.geom.Point(
                  ol.proj.transform([
                    longPoint,
                    latPoint
                  ], "EPSG:4326", "EPSG:3857")
                )
              })
            ]
          }), style: new ol.style.Style({
            image: new ol.style.Icon({
              anchor: [
                0.5,
                0.99
              ],
              anchorXUnits: "fraction",
              anchorYUnits: "fraction",
              src: "../static/img/openlayers/spotlight/scale-3.png",
              scale: 0.5
            })
          })
        });
      } else {
        // Crea el vector del punto
        point = new ol.layer.Vector({});
      }

      /**
       * Inicializa el mapa
       */
      const map = new ol.Map({
        target: "map",
        interactions: ol.interaction.defaults({
          doubleClickZoom: false
        }), layers: [
          new ol.layer.Tile({
            source: new ol.source.OSM()
          }),

          point
        ], view: new ol.View({
          center: ol.proj.fromLonLat([
            -65.6437256,
            -21.586949
          ]),
          zoom: 3,
          maxZoom: 20
        })
      });

      // Valida las coordenadas
      if (longPoint !== 0 && latPoint !== 0) {
        let extent = ol.extent.boundingExtent([
          [
            longPoint,
            latPoint
          ]
        ]);
        extent = ol.proj.transformExtent(extent, ol.proj.get("EPSG:4326"), ol.proj.get("EPSG:3857"));

        map.getView().fit(extent, map.getSize());
      }

      // Si se hace doble clic en el mapa
      map.on("dblclick", function (event) {
        // Obtiene las coordenadas y las transforma
        coords = ol.proj.transform(event.coordinate, "EPSG:3857", "EPSG:4326");

        // Remueve el vector
        map.removeLayer(point);

        // Crea el nuevo punto con las nuevas coordenadas
        point = new ol.layer.Vector({
          source: new ol.source.Vector({
            features: [
              new ol.Feature({
                type: "marker",
                geometry: new ol.geom.Point(
                  ol.proj.transform(coords, "EPSG:4326", "EPSG:3857")
                )
              })
            ]
          }), style: new ol.style.Style({
            image: new ol.style.Icon({
              anchor: [
                0.5,
                0.99
              ],
              anchorXUnits: "fraction",
              anchorYUnits: "fraction",
              src: "../static/img/openlayers/spotlight/scale-3.png",
              scale: 0.5
            })
          })
        });

        $("#map").attr("data-lng", coords[0]);
        $("#map").attr("data-lat", coords[1]);

        // Añade el vector del punto
        map.addLayer(point);
      });
    </script>
  </body>
</html>
