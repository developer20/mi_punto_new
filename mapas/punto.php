<?php
require_once "../config/mainModel.php";

session_start();

if (!isset($_GET["id_pos"])) {
  exit;
}

$BD = new BD();
$BD->conectar();

/**
 * OBTIENE EL PUNTO
 */
$sql_punto =
  "SELECT 
    idpos, p.cod_cum, razon, estado_com, latitud, longitud, 
    detalle_direccion, t.descripcion AS terri, z.descripcion AS zona 
   FROM {$GLOBALS["BD_POS"]}.puntos p
   INNER JOIN {$GLOBALS["BD_NAME"]}.territorios AS t ON (t.id = p.territorio)
   INNER JOIN {$GLOBALS["BD_NAME"]}.zonas AS z ON (z.id = p.zona)
   WHERE idpos={$_GET["id_pos"]}";

$query_punto = $BD->devolver_array($sql_punto)[0];

/**
 * OBTIENE LA CATEGORIA
 */
$sql_cat = "SELECT id, descripcion FROM {$GLOBALS["BD_POS"]}.categorias WHERE id={$query_punto["estado_com"]}";
$query_cat = $BD->devolver_array($sql_cat)[0];
?>
<!doctype html>
<html lang="es">
  <head>
    <meta charset="utf-8" />
    <title>Mapa: punto</title>
    <link rel="stylesheet" href="../static/css/openlayers/openlayers.css" type="text/css" />
    <script type="text/javascript" src="../static/js/openlayers/openlayers.js"></script>
    <script type="text/javascript" src="../template/js/jquery.min.js"></script>

    <!-- estilos -->
    <style type="text/css">
      body {
        padding: 0;
        margin: 0;
        overflow: hidden;
        font-family: Helvetica, Arial, sans-serif;
      }

      #map {
        width: 100vw;
        height: 100vh;
      }

      table {
        border-collapse: collapse;
        min-width: 100%;
        font-size: 13px;
      }

      table, th, td {
        border: 1px solid #ddd;
        margin-bottom: 1px;
      }

      /* POPUP DEL MAPA */
      .popup {
        min-width: 90px;
        max-width: 250px;
        position: absolute;
        background-color: #fff;
        filter: drop-shadow(0 0 3px rgba(0, 0, 0, 0.2));
        padding: 15px;
        border-radius: 10px;
        bottom: 12px;
        left: -48px;
      }

      .popup:after, .popup:before {
        top: 100%;
        border: solid transparent;
        content: " ";
        height: 0;
        width: 0;
        position: absolute;
        pointer-events: none;
      }

      .popup:after {
        border-top-color: #fff;
        border-width: 10px;
        left: 48px;
        margin-left: -10px;
      }

      .popup:before {
        border-top-color: #ccc;
        border-width: 11px;
        left: 48px;
        margin-left: -11px;
      }

      .popup-close {
        text-decoration: none;
        position: absolute;
        top: -14px;
        right: -14px;
        line-height: 14px;
        background-color: #fff;
        border-radius: 100%;
        padding: 6px;
        filter: drop-shadow(0 0 3px rgba(0, 0, 0, 0.2));
      }

      .popup-close:after {
        content: "X";
        line-height: 20px;
        font-size: 20px;
        padding: 5px;
        color: #616161;
      }

      .popup-close:after:hover {
        content: "X";
        line-height: 16px;
        font-size: 16px;
        padding: 7px;
        color: #3b3b3b;
      }
    </style>
  </head>

  <body>
    <!-- mapa -->
    <div id="map"></div>

    <!-- popup dentro del mapa -->
    <div id="popup" class="popup">
      <a href="#" id="close-popup" class="popup-close"></a>
      <div id="popup-content"></div>
    </div>

    <!--suppress JSAnnotator -->
    <script type="text/javascript">
      // ELEMENTOS DEL POPUP
      const elementPopup = document.getElementById("popup");
      const closePopup = document.getElementById("close-popup");

      // DATOS DEL PUNTO
      const data = <?php echo json_encode($query_punto); ?>;

      // POPUP
      const popup = new ol.Overlay({
        element: elementPopup,
        positioning: "bottom-center",
        stopEvent: false
      });

      // CERRAR POPUP
      closePopup.onclick = () => {
        /**
         * Modifica la posición del popup
         */
        popup.setPosition(undefined);

        /**
         * Retira el foco del elemento
         */
        closePopup.blur();

        return false;
      };

      // Punto
      const point = new ol.layer.Vector({
        source: new ol.source.Vector({
          features: [
            new ol.Feature({
              data: data,
              geometry: new ol.geom.Point(
                ol.proj.transform([
                  parseFloat(<?php echo $query_punto["longitud"]; ?>),
                  parseFloat(<?php echo $query_punto["latitud"]; ?>)
                ], "EPSG:4326", "EPSG:3857")
              )
            })
          ]
        }), style: new ol.style.Style({
          image: new ol.style.Icon({
            anchor: [
              0.5,
              0.99
            ],
            anchorXUnits: "fraction",
            anchorYUnits: "fraction",
            src: "../static/img/openlayers/spotlight/scale-3.png",
            scale: 0.5
          })
        })
      });

      /**
       * Inicializa el mapa
       */
      const map = new ol.Map({
        target: "map",
        overlays: [
          popup
        ],
        layers: [
          new ol.layer.Tile({
            source: new ol.source.OSM()
          }),

          point
        ], view: new ol.View({
          center: ol.proj.fromLonLat([
            0,
            0
          ]),
          zoom: 2,
          maxZoom: 22
        })
      });

      let extent = ol.extent.boundingExtent([[parseFloat(<?php echo $query_punto["longitud"]; ?>), parseFloat(<?php echo $query_punto["latitud"]; ?>)]]);
          extent = ol.proj.transformExtent(extent, ol.proj.get("EPSG:4326"), ol.proj.get("EPSG:3857"));

      map.getView().fit(extent, map.getSize());

      // CADA QUE SE HAGA CLIC EN EL MAPA
      map.on("click", function (event) {
        const feature = map.forEachFeatureAtPixel(event.pixel, (feature) => {
          return feature;
        });

        if (feature && feature.values_.data) {
          // Obtiene coordenadas del elemento
          const geometry = feature.getGeometry();
          const coordinates = geometry.getCoordinates();

          // Contenido del popup
          let html  = "<table>";
              html += " <thead style=\"text-align: center;\">";
              html += "   <tr colspan=\"2\">";
              html += "     <th colspan=\"2\">INFORMACIÓN PDV</th>";
              html += "   </tr>";
              html += " </thead>";
              html += " <tbody>";
              html += "   <tr>";
              html += "     <td>RAZON SOCIAL</td>";
              html += "     <td>" + feature.values_.data['razon'] + "</td>";
              html += "   <tr>";
              html += "   <tr>";
              html += "     <td>ID PDV</td>";
              html += "     <td>" + feature.values_.data['idpos'] + "</td>";
              html += "   <tr>";
              html += "   <tr>";
              html += "     <td>COD. UNICO TIENDA</td>";
              html += "     <td>" + feature.values_.data['cod_cum'] + "</td>";
              html += "   <tr>";
              html += "   <tr>";
              html += "     <td>ESTADO COMERCIAL</td>";
              html += "     <td><?php echo $query_cat['descripcion']; ?></td>";
              html += "   <tr>";
              html += "   <tr>";
              html += "     <td>DIRECCIÓN</td>";
              html += "     <td>" + feature.values_.data['detalle_direccion'] + "</td>";
              html += "   <tr>";
              html += "   <tr>";
              html += "     <td>RUTA</td>";
              html += "     <td>" + feature.values_.data['terri'] + "</td>";
              html += "   <tr>";
              html += "   <tr>";
              html += "     <td>CIRCUITO</td>";
              html += "     <td>" + feature.values_.data['zona'] + "</td>";
              html += "   <tr>";
              html += " </tbody>";
              html += "</table>";

          // Modifica el contenido del popup
          $("#popup #popup-content").html(html);

          // Define la posición del popup
          popup.setPosition(coordinates);
        } else {
          // Oculta el popup
          popup.setPosition(undefined);
        }
      });
    </script>
  </body>
</html>