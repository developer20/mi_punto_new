<?php
require_once "../config/mainModel.php";

session_start();

if (!isset($_GET["tipo"])) {
  exit;
}

$BD = new BD();
$BD->conectar();

$tipo = $_GET["tipo"];
$id_tipo = $_GET['id_tipo'];
$pto_idpos = $_GET['pto_idpos'];
$pto_cedu = $_GET['pto_cedu'];
$pto_name = $_GET['pto_name'];
$pto_state = $_GET['pto_state'];
$pto_depar = $_GET['pto_depar'];
$pto_ciud = $_GET['pto_ciud'];
$pto_circ = $_GET['pto_circ'];
$pto_ruta = $_GET['pto_ruta'];
$pto_est_com = $_GET['pto_est_com'];
$pto_distri = $_GET['pto_distri'];
$invalidPuntos = "";

$sql_puntos = "SELECT 
    idpos, p.cod_cum, latitud, longitud, estado_com, razon, detalle_direccion, 
    t.descripcion AS terri, z.descripcion AS zona 
   FROM {$GLOBALS["BD_POS"]}.puntos AS p 
   INNER JOIN {$GLOBALS["BD_NAME"]}.territorios t ON t.id = p.territorio
   INNER JOIN {$GLOBALS["BD_NAME"]}.zonas z ON z.id = p.zona";

switch ($tipo) {
  // Zonas
  case '3':
    $sql_puntos .= " AND p.zona = '$id_tipo'";
    break;

  // Territorios
  case '2':
    $sql_puntos .= " AND p.territorio = '$id_tipo'";
    break;

  // Regional
  case '1':
    $sql_puntos .= " AND p.territorio in (SELECT id_territorio FROM {$GLOBALS["BD_NAME"]}.territorios_distribuidor t WHERE t.id_regional = '$id_tipo') ";
    break;
}

if ($pto_idpos != "") {
  $sql_puntos .= " AND p.idpos = '$pto_idpos' ";
}

if ($pto_cedu != "") {
  $sql_puntos .= " AND p.cedula LIKE '%$pto_cedu%' ";
}

if ($pto_name != "") {
  $sql_puntos .= " AND p.razon LIKE '%$pto_name%' ";
}

if ($pto_state != "") {
  $sql_puntos .= " AND p.estado = '$pto_state' ";
}

if ($pto_depar != "") {
  $sql_puntos .= " AND p.depto = '$pto_depar' ";
}

if ($pto_ciud != "") {
  $sql_puntos .= " AND p.ciudad = '$pto_ciud' ";
}

if ($pto_circ != "") {
  $sql_puntos .= " AND p.territorio = '$pto_circ' ";
}

if ($pto_ruta != "" && $pto_ruta != null) {
  $sql_puntos .= " AND p.zona = $pto_ruta";
}

if ($pto_est_com != "") {
  $sql_puntos .= " AND p.estado_com = $pto_est_com";
}

if ($pto_distri != 0) {
  $sql_puntos .= " AND p.territorio in (SELECT id_territorio FROM {$GLOBALS["BD_NAME"]}.territorios_distribuidor t WHERE t.id_distri = $pto_distri )";
  $validPuntos = "SELECT MAX(id),id_pos FROM {$GLOBALS["BD_POS"]}.solicitud_pdv WHERE id_distri = '$pto_distri' AND estado != 1 AND tipo_solicitud = 1 GROUP BY id_pos ORDER BY id";
  $resValidPuntos = $BD->devolver_array($validPuntos);

  foreach ($resValidPuntos as $data) {
    $invalidPuntos .= $data['id_pos'] . ",";
  }

  $invalidPuntos = substr($invalidPuntos, 0, - 1);

  if ($invalidPuntos != "") {
    $sql_puntos .= " AND p.idpos NOT IN ($invalidPuntos) ";
  }
}

$query_puntos1 = $BD->consultar($sql_puntos);
$total_registros = $BD->numreg($query_puntos1);

$sql_puntos .= " AND p.latitud != 0 and p.longitud != 0";
$query_puntos2 = $BD->devolver_array($sql_puntos);
$position_registros = count($query_puntos2);

$coords = array();

foreach ($query_puntos2 AS $key => $value) {
  // Coordenadas
  $coords[] = array(floatval($value["longitud"]), floatval($value["latitud"]));

  // Obtiene la categoria
  $sql_cat = "SELECT id, descripcion FROM {$GLOBALS["BD_POS"]}.categorias WHERE id={$value["estado_com"]}";
  $query_cat = $BD->devolver_array($sql_cat)[0];

  // Valida la categoria
  if ($query_cat["id"] > 1) {
    $icon = "red";
  } else {
    $icon = "blue";
  }

  $query_puntos2[$key]["icon"] = $icon;
  $query_puntos2[$key]["cat_des"] = $query_cat["descripcion"];
}
?>
<!doctype html>
<html lang="es">
  <head>
    <meta charset="utf-8" />
    <title>Mapa: todos los puntos</title>
    <link rel="stylesheet" href="../static/css/openlayers/openlayers.css" type="text/css" />
    <script type="text/javascript" src="../static/js/openlayers/openlayers.js"></script>
    <script type="text/javascript" src="../template/js/jquery.min.js"></script>
    <script type="text/javascript" src="../static/js/html2canvas.js"></script>

    <!-- estilos -->
    <style type="text/css">
      body {
        padding: 0;
        margin: 0;
        overflow: hidden;
        font-family: Helvetica, Arial, sans-serif;
      }

      #map {
        width: 100vw;
        height: 100vh;
      }

      .info {
        font-size: 12px;
        font-weight: normal;
        margin-left: 2px;
        float: left;
        border-radius: 7px;
        width: 49.8%;
        background-color: #e4e4e4;
        margin-bottom: 2px;
        height: 42px;
      }

      .verde {
        float: left;
        border-radius: 7px 0 0 7px;
        height: 42px;
        width: 46px;
        background-color: #cacaca;
      }

      table {
        border-collapse: collapse;
        min-width: 100%;
        font-size: 13px;
      }

      table, th, td {
        border: 1px solid #ddd;
        margin-bottom: 1px;
      }

      th, td {
        padding: 4px;
      }

      .gris {
        float: right;
        border-radius: 0 7px 7px 0;
        height: 42px;
        width: 46px;
        margin-top: -14px;
        background-color: #cacaca;
        cursor: pointer;
      }

      .minititulo {
        margin-top: 14px;
        margin-left: 60px;
        color: #585858;
        font-style: normal;
        font-variant-ligatures: normal;
        font-variant-caps: normal;
        font-variant-numeric: normal;
        font-weight: normal;
        font-stretch: normal;
        font-size: 14px;
        line-height: normal;
      }

      .btn-primary {
        color: dimgray;
        background-color: #E6E6E6;
        border-color: dimgray;
        width: 33%;
      }

      .btn {
        display: inline-block;
        padding: 6px 12px;
        margin-bottom: 2px;
        font-size: 14px;
        font-weight: 400;
        line-height: 1.42857143;
        text-align: center;
        white-space: nowrap;
        vertical-align: middle;
        touch-action: manipulation;
        cursor: pointer;
        user-select: none;
        background-image: none;
        border: 1px solid transparent;
        border-radius: 4px;
        outline: none;
      }

      .btn .badge {
        position: relative;
        top: -1px;
      }

      .btn-primary .badge {
        color: #fff;
        background-color: #EF3829;
      }

      .badge {
        display: inline-block;
        min-width: 10px;
        padding: 7px 9px;
        font-size: 12px;
        font-weight: 700;
        line-height: 1;
        color: #fff;
        text-align: center;
        white-space: nowrap;
        vertical-align: middle;
        background-color: #777;
        border-radius: 10px;
      }

      /* POPUP DEL MAPA */
      .popup {
        min-width: 90px;
        max-width: 250px;
        position: absolute;
        background-color: #fff;
        filter: drop-shadow(0 0 3px rgba(0, 0, 0, 0.2));
        padding: 15px;
        border-radius: 10px;
        bottom: 12px;
        left: -48px;
      }

      .popup:after, .popup:before {
        top: 100%;
        border: solid transparent;
        content: " ";
        height: 0;
        width: 0;
        position: absolute;
        pointer-events: none;
      }

      .popup:after {
        border-top-color: #fff;
        border-width: 10px;
        left: 48px;
        margin-left: -10px;
      }

      .popup:before {
        border-top-color: #ccc;
        border-width: 11px;
        left: 48px;
        margin-left: -11px;
      }

      .popup-close {
        text-decoration: none;
        position: absolute;
        top: -14px;
        right: -14px;
        line-height: 14px;
        background-color: #fff;
        border-radius: 100%;
        padding: 6px;
        filter: drop-shadow(0 0 3px rgba(0, 0, 0, 0.2));
      }

      .popup-close:after {
        content: "X";
        line-height: 20px;
        font-size: 20px;
        padding: 5px;
        color: #616161;
      }

      .popup-close:after:hover {
        content: "X";
        line-height: 16px;
        font-size: 16px;
        padding: 7px;
        color: #3b3b3b;
      }

      /* BOTÓN EXPORTAR MAPA */
      #button-export-map {
        color: white;
        background-color: #EF3829;
        position: absolute;
        left: 50%;
        right: 0;
        bottom: 5px;
        height: 40px;
        cursor: pointer;
        border-radius: 4px;
        font-size: 16px;
      }
      #button-export-map:hover {
        background-color: #8f2517;
      }
    </style>

    <!-- scripts -->
    <script type="text/javascript">
      // EXPORTA CSV
      const exportCsv = tipo => {
        if (tipo != "") {
          window.open("../modulos/reporte_puntos/reporte_csv_mapa.php?tipo_export=" + tipo + '&tipo=<?php echo $tipo; ?>&id_tipo=<?php echo $id_tipo; ?>&pto_idpos=<?php echo $pto_idpos; ?>&pto_cedu=<?php echo $pto_cedu; ?>&pto_name=<?php echo $pto_name; ?>&pto_state=<?php echo $pto_state; ?>&pto_depar=<?php echo $pto_depar; ?>&pto_ciud=<?php echo $pto_ciud; ?>&pto_circ=<?php echo $pto_circ; ?>&pto_ruta=<?php echo $pto_ruta; ?>&pto_est_com=<?php echo $pto_est_com; ?>&pto_distri=<?php echo $pto_distri; ?>');
        }
      };

      // EXPORTA MAPA
      const exportMap = () => {
        html2canvas($("#map"), {
          useCORS: true,

          onrendered: (canvas) => {
            const a = document.createElement("a");

            a.href = canvas.toDataURL("image/jpeg").replace("image/jpeg", "image/octet-stream");
            a.download = "CAPTURA-MAPA.jpg";
            a.click();
          }
        });
      }
    </script>
  </head>

  <body>
    <!-- estados -->
    <div style="text-align: center; width: 100%;">
      <div class="info" style="display: inline-block;">
        <div class="verde">
          <img src="../static/img/openlayers/dot/blue.png" style="max-height: 25px; margin-top: 10px;" alt="">
        </div>

        <div class="minititulo">
          <span>ESTADO VENDE</span>

          <!-- exporta csv -->
          <div class="gris" onclick="exportCsv(1);">
            <img src="../static/img/csv.png" style="max-height: 25px; margin-top: 10px;" alt=""></div>
        </div>
      </div>

      <div class="info" style="display: inline-block;">
        <div class="verde">
          <img src="../static/img/openlayers/dot/red.png" style="max-height: 25px; margin-top: 10px;" alt="">
        </div>

        <div class="minititulo">
          <span>OTROS ESTADOS</span>

          <!-- exporta csv -->
          <div class="gris" onclick="exportCsv(2);">
            <img src="../static/img/csv.png" style="max-height: 25px; margin-top: 10px;" alt=""></div>
        </div>
      </div>
    </div>

    <!-- puntos -->
    <div style="text-align: center;">
      <button class="btn btn-primary" type="button">
        <span>TOTAL PUNTOS</span>
        <span class="badge"><?php echo $total_registros; ?></span>

        <!-- exporta csv -->
        <a href="javascript: exportCsv(3);" style="float: right;">
          <img src="../static/img/csv.png" style="max-height: 25px;" alt="" />
        </a>
      </button>

      <button class="btn btn-primary" type="button">
        <span>PUNTOS GEOREFERENCIADOS</span>
        <span class="badge"><?php echo $position_registros; ?></span>

        <!-- exporta csv -->
        <a href="javascript: exportCsv(4);" style="float: right;">
          <img src="../static/img/csv.png" style="max-height: 25px;" alt="" />
        </a>
      </button>

      <button class="btn btn-primary" type="button">
        <span>PUNTOS SIN GEOREFERENCIAR</span>
        <span class="badge"><?php echo $total_registros - $position_registros; ?></span>

        <!-- exporta csv -->
        <a href="javascript: exportCsv(5);" style="float: right;">
          <img src="../static/img/csv.png" style="max-height: 25px;" alt="">
        </a>
      </button>
    </div>

    <!-- mapa -->
    <div id="map"></div>

    <!-- popup dentro del mapa -->
    <div id="popup" class="popup">
      <a href="#" id="close-popup" class="popup-close"></a>
      <div id="popup-content"></div>
    </div>

    <!-- exportar mapa -->
    <input type="button" id="button-export-map" value="Exportar Mapa" onclick="exportMap();" />

    <!--suppress JSAnnotator -->
    <script type="text/javascript">
      // ELEMENTOS DEL POPUP
      const elementPopup = document.getElementById("popup");
      const closePopup = document.getElementById("close-popup");

      // POPUP
      const popup = new ol.Overlay({
        element: elementPopup,
        positioning: "bottom-center",
        stopEvent: false
      });

      // CERRAR POPUP
      closePopup.onclick = () => {
        /**
         * Modifica la posición del popup
         */
        popup.setPosition(undefined);

        /**
         * Retira el foco del elemento
         */
        closePopup.blur();

        return false;
      };

      /**
       * Inicializa el mapa
       */
      const map = new ol.Map({
        target: "map",
        overlays: [
          popup
        ],
        layers: [
          new ol.layer.Tile({
            source: new ol.source.OSM()
          })
        ], view: new ol.View({
          center: ol.proj.fromLonLat([
            0,
            0
          ]),
          zoom: 2,
          maxZoom: 22
        })
      });

      let extent = ol.extent.boundingExtent(<?php echo json_encode($coords); ?>);
          extent = ol.proj.transformExtent(extent, ol.proj.get("EPSG:4326"), ol.proj.get("EPSG:3857"));

      map.getView().fit(extent, map.getSize());

      // CADA QUE SE HAGA CLIC EN EL MAPA
      map.on("click", function (event) {
        const feature = map.forEachFeatureAtPixel(event.pixel, (feature) => {
          return feature;
        });

        if (feature && feature.values_.data) {
          // Obtiene coordenadas del elemento
          const geometry = feature.getGeometry();
          const coordinates = geometry.getCoordinates();

          // Contenido del popup
          let html  = "<table>";
              html += " <thead style=\"text-align: center;\">";
              html += "   <tr colspan=\"2\">";
              html += "     <th colspan=\"2\">INFORMACIÓN PDV</th>";
              html += "   </tr>";
              html += " </thead>";
              html += " <tbody>";
              html += "   <tr>";
              html += "     <td>RAZON SOCIAL</td>";
              html += "     <td>" + feature.values_.data['razon'] + "</td>";
              html += "   <tr>";
              html += "   <tr>";
              html += "     <td>ID PDV</td>";
              html += "     <td>" + feature.values_.data['idpos'] + "</td>";
              html += "   <tr>";
              html += "   <tr>";
              html += "     <td>COD. UNICO TIENDA</td>";
              html += "     <td>" + feature.values_.data['cod_cum'] + "</td>";
              html += "   <tr>";
              html += "   <tr>";
              html += "     <td>ESTADO COMERCIAL</td>";
              html += "     <td>" + feature.values_.data['cat_des'] + "</td>";
              html += "   <tr>";
              html += "   <tr>";
              html += "     <td>DIRECCIÓN</td>";
              html += "     <td>" + feature.values_.data['detalle_direccion'] + "</td>";
              html += "   <tr>";
              html += "   <tr>";
              html += "     <td>RUTA</td>";
              html += "     <td>" + feature.values_.data['terri'] + "</td>";
              html += "   <tr>";
              html += "   <tr>";
              html += "     <td>CIRCUITO</td>";
              html += "     <td>" + feature.values_.data['zona'] + "</td>";
              html += "   <tr>";
              html += " </tbody>";
              html += "</table>";

          // Modifica el contenido del popup
          $("#popup #popup-content").html(html);

          // Define la posición del popup
          popup.setPosition(coordinates);
        } else {
          // Oculta el popup
          popup.setPosition(undefined);
        }
      });

      // DATOS DE LOS PUNTOS
      const data = <?php echo json_encode($query_puntos2); ?>;

      // RECORRE LOS DATOS DE LOS PUNTOS
      $.each (data, (key, value) => {
        // Vector: punto de marcación
        const point = new ol.layer.Vector({
          source: new ol.source.Vector({
            features: [
              new ol.Feature({
                data: value,
                geometry: new ol.geom.Point(
                  ol.proj.transform([
                    parseFloat(value["longitud"]),
                    parseFloat(value["latitud"])
                  ], "EPSG:4326", "EPSG:3857")
                )
              })
            ]
          }), style: new ol.style.Style({
            image: new ol.style.Icon({
              anchor: [
                0.5,
                0.9
              ],
              anchorXUnits: "fraction",
              anchorYUnits: "fraction",
              src: "../static/img/openlayers/dot/" + value["icon"] + ".png"
            })
          })
        });

        // Añade el vector del punto de marcación
        map.addLayer(point);
      });
    </script>
  </body>
</html>