<?php
session_start();

require_once "../config/Session.php";
require_once "../config/mainModel.php";

$BD = new BD();
$BD->conectar();

$fecha_ini = $_GET['fecha_ini'];
$fecha_fin = $_GET['fecha_fin'];
$regional = intval($_GET['regional']);
$idCircuito = intval($_GET['idCircuito']);
$idRuta = intval($_GET['idRuta']);
$tipo = $_GET['tipo'];

// Valida que se los parámetros de las fechas y el tipo existan
if (isset($fecha_ini) && isset($fecha_fin) && isset($tipo)) {
  // Datos de la sesión
  $oSession = unserialize($_SESSION[$GLOBALS["SESION_DIS"]]);
  $idDistri = $oSession->VSidDistri;
  $id_user = $oSession->VSid;

  // Permisos DCS
  $permisos_regio = $BD->Permisos_Dcs_Regionales($id_user);
  $permisos_terr = $BD->Permisos_Dcs_Territorios();
  $permisos_zona = $BD->Permisos_Dcs_Zonas();

  // Condicionales para las consultas
  $perms = " AND ter.id_distri = $idDistri";
  $perms2 = " AND simpro.distri = $idDistri";

  if ($regional > 0) {
    $perms .= " AND ter.id_regional = $regional";
    $perms2 .= " AND simpro.regional = $regional";
  }

  if ($idCircuito > 0) {
    $perms .= " AND pos.territorio = $idCircuito";
    $perms2 .= " AND simpro.territorio = $idCircuito";
  }

  if ($idRuta > 0) {
    $perms .= " AND pos.zona = $idRuta";
    $perms2 .= " AND simpro.zona = $idRuta";
  }

  if ($permisos_regio != "") {
    $perms .= " AND ter.id_regional in ($permisos_regio)";
    $perms2 .= " AND simpro.regional in ($permisos_regio)";
  }

  if ($permisos_terr != "") {
    $perms .= " AND pos.territorio in ($permisos_terr)";
    $perms2 .= " AND simpro.territorio in ($permisos_terr)";
  }

  if ($permisos_zona != "") {
    $perms .= " AND pos.zona in ($permisos_zona)";
    $perms2 .= " AND simpro.zona in ($permisos_zona)";
  }

  switch ($tipo) {
    case 1:
      $sql = "select idpos,latitud,longitud from {$GLOBALS["BD_POS"]}.puntos as pos 
			  inner join {$GLOBALS["BD_NAME"]}.territorios_distribuidor as ter on (ter.id_territorio = pos.territorio)
			  where latitud != 0 and longitud != 0 and (fecha_crea >= '$fecha_ini' and fecha_crea <= '$fecha_fin') $perms";
      break;

    case 2:
      $sql = "select pos.idpos,pos.latitud,pos.longitud from {$GLOBALS["BD_POS"]}.puntos as pos
			  inner join {$GLOBALS["BD_NAME"]}.simcards as simpro on (pos.idpos = simpro.id_pos and (simpro.fecha_ac >= '$fecha_ini' and simpro.fecha_ac <= '$fecha_fin') and simpro.activo = 1) 
			  where pos.latitud > 0 and pos.longitud > 0 $perms2";
      break;

    case 3:
      $sql = "select pos.idpos,pos.latitud,pos.longitud from {$GLOBALS["BD_POS"]}.puntos as pos
			  inner join {$GLOBALS["BD_NAME"]}.simcards as simpro on (pos.idpos = simpro.id_pos and (simpro.fecha_venta >= '$fecha_ini' and simpro.fecha_venta <= '$fecha_fin') and simpro.estado = 1)
			  where pos.latitud > 0 and pos.longitud > 0 $perms2";
      break;

    case 4:
      $sql = "select pos.idpos,pos.latitud,pos.longitud from {$GLOBALS["BD_POS"]}.puntos as pos
			  inner join {$GLOBALS["BD_NAME"]}.simcards as simpro on (pos.idpos = simpro.id_pos and simpro.activo = 1)
			  inner join {$GLOBALS["BD_NAME"]}.csv_recarga as rec on (simpro.movil = rec.NUM_CELULAR and (rec.FEC_RECARGA >= '$fecha_ini' and rec.FEC_RECARGA >= '$fecha_fin'))
			  where pos.latitud > 0 and pos.longitud > 0 $perms2";
      break;
  }

  // Ejecuta la consulta
  $query = $BD->devolver_array($sql);

  if (count($query) < 1) {
    // Muestra mensaje de error
    echo "<div style=\"font-family: Helvetica, Arial, sans-serif; font-size: 1.17em; text-align: center;  position: absolute; left: 50%; top: 50%; transform: translate(-50%, -50%);\">No se encontraron resultados</div>";

    exit;
  }
} else {
  // Finaliza la ejecución
  exit;
}
?>
<!doctype html>
<html lang="es">
  <head>
    <meta charset="utf-8" />
    <title>Mapa de calor</title>
    <link type="text/css" rel="stylesheet" href="../static/css/openlayers/openlayers.css" />
    <script type="text/javascript" src="../static/js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="../static/js/openlayers/openlayers.js"></script>

    <style type="text/css">
      body {
        padding: 0;
        margin: 0;
        overflow: hidden;
      }

      #map {
        width: 100vw;
        height: 100vh;
      }
    </style>
  </head>

  <body>
    <div id="map"></div>

    <script type="text/javascript">
      // Coordenadas
      const lon1 = -75.5652965;
      const lat1 = 6.2175421;
      const lon2 = -75.558941;
      const lat2 = 6.214593;

      // Crea el mapa
      const map = new ol.Map({
        layers: [
          new ol.layer.Tile({
            source: new ol.source.OSM()
          })
        ],
        target: "map",
        view: new ol.View({
          center: [
            0,
            0
          ],
          zoom: 3,
          maxZoom: 22
        })
      });

      /**
       * Coordenadas
       * @type {Array}
       */
      let coords = [];

      /**
       * Vector de calor
       */
      let vector_heatmap;

      <?php
      // Recorre los resultados de la consulta
      foreach ($query AS $key => $value) {
      ?>

      // Añade las coordenadas
      coords.push([<?php echo $value["longitud"]; ?>, <?php echo $value["latitud"]; ?>]);

      // Añade un nuevo vector
      vector_heatmap = new ol.layer.Heatmap({
        source: new ol.source.Vector({
          features: [
            new ol.Feature({
              geometry: new ol.geom.Point(
                ol.proj.transform([
                  parseFloat(<?php echo $value["longitud"]; ?>),
                  parseFloat(<?php echo $value["latitud"]; ?>)
                ], "EPSG:4326", "EPSG:3857")
              )
            })
          ]
        }),

        blur: 10,
        radius: 6
      });

      // Añade el vector de calor
      map.addLayer(vector_heatmap);

      <?php
      }
      ?>

      // Ajusta el centro con base a las coordenadas de todos los puntos del cluster
      let extent = ol.extent.boundingExtent(coords);
          extent = ol.proj.transformExtent(extent, ol.proj.get("EPSG:4326"), ol.proj.get("EPSG:3857"));

      map.getView().fit(extent, map.getSize());
    </script>
  </body>
</html>
