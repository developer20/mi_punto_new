<?php
require_once "../../config/mainModel.php";

session_start();

$BD = new BD();
$BD->conectar();

$sql = "SELECT idpos, razon, estado_com, latitud, longitud FROM {$GLOBALS["BD_POS"]}.puntos WHERE idpos = {$_GET["id_pos"]}";
$query = $BD->consultar($sql);

if ($query) {
  $BD->setCharset("utf8");
  $sql = "SELECT idpos, razon, estado_com, latitud, longitud FROM {$GLOBALS["BD_POS"]}.puntos_solicitud WHERE idpos = {$_GET["id_pos"]} AND id_solicitud = {$_GET["num_soli"]}";
  $query2 = $BD->consultar($sql);
  
  // Coordenadas actuales
  $current_longPoint = $query->fields["longitud"];
  $current_latPoint = $query->fields["latitud"];
  
  // Coordenadas nuevas
  $new_longPoint = $query2->fields["longitud"];
  $new_latPoint = $query2->fields["latitud"];
} else {
  exit;
}
?>
<!doctype html>
<html lang="es">
  <head>
    <meta charset="utf-8" />
    <title>Mapa: puntos</title>
    <link rel="stylesheet" href="../../static/css/openlayers/openlayers.css" type="text/css" />
    <script type="text/javascript" src="../../static/js/openlayers/openlayers.js"></script>
    <script type="text/javascript" src="../../static/js/jquery-3.3.1.min.js"></script>
    
    <style type="text/css">
      body {
        padding: 0;
        margin: 0;
        overflow: hidden;
        font-family: Helvetica, Arial, sans-serif;
      }
      
      #map {
        width: 100vw;
        height: 100vh;
      }
      
      #position {
        font-weight: bold;
      }
      
      .ol-popup {
        min-width: 66px;
        width: 250px;
        max-width: 250px;
        position: absolute;
        background-color: #fff;
        -webkit-filter: drop-shadow(0 0 3px rgba(0, 0, 0, 0.2));
        filter: drop-shadow(0 0 3px rgba(0, 0, 0, 0.2));
        padding: 15px;
        border-radius: 10px;
        bottom: 8px;
        left: -49px;
      }
      
      .ol-popup:after, .ol-popup:before {
        top: 100%;
        border: solid transparent;
        content: " ";
        height: 0;
        width: 0;
        position: absolute;
        pointer-events: none;
      }
      
      .ol-popup:after {
        border-top-color: #fff;
        border-width: 10px;
        left: 48px;
        margin-left: -10px;
      }
      
      .ol-popup:before {
        border-top-color: #ccc;
        border-width: 11px;
        left: 48px;
        margin-left: -11px;
      }
      
      .ol-popup-closer {
        text-decoration: none;
        position: absolute;
        top: 5px;
        right: 8px;
      }
      
      .ol-popup-closer:after {
        content: "X";
        line-height: 16px;
        font-size: 16px;
        padding: 7px;
        color: #616161;
      }
      
      .ol-popup-closer:after:hover {
        content: "X";
        line-height: 16px;
        font-size: 16px;
        padding: 7px;
        color: #3b3b3b;
      }
    </style>
  </head>
  
  <body>
    <div id="map"></div>
    
    <!-- popup -->
    <div id="popup" class="ol-popup">
      <a href="#" id="close-popup" class="ol-popup-closer"></a>
      <div id="popup-content">
        <p id="position"></p>
        <p>Razon: <?php echo $query->fields["razon"]; ?></p>
        <p>ID PDV: <?php echo $query->fields["idpos"]; ?></p>
      </div>
    </div>
    
    <!--suppress JSAnnotator -->
    <script type="text/javascript">
      // Coordenadas actuales
      const current_longPoint = <?php echo $current_longPoint; ?>;
      const current_latPoint = <?php echo $current_latPoint; ?>;

      // Coordenadas nuevas
      const new_longPoint = <?php echo $new_longPoint; ?>;
      const new_latPoint = <?php echo $new_latPoint; ?>;

      // Popup
      const elementPopup = document.getElementById("popup");
      const closePopup = document.getElementById("close-popup");

      // Ubicación actual del punto
      const currentPosition = new ol.layer.Vector({
        source: new ol.source.Vector({
          features: [
            new ol.Feature({
              id: "current-position",
              geometry: new ol.geom.Point(
                ol.proj.transform([
                  parseFloat(current_longPoint),
                  parseFloat(current_latPoint)
                ], "EPSG:4326", "EPSG:3857")
              )
            })
          ]
        }), style: new ol.style.Style({
          image: new ol.style.Icon({
            anchor: [
              0.5,
              0.99
            ],
            anchorXUnits: "fraction",
            anchorYUnits: "fraction",
            src: "../../static/img/openlayers/spotlight/scale-2.png"
          })
        })
      });

      // Ubicación nueva del punto
      const newPosition = new ol.layer.Vector({
        source: new ol.source.Vector({
          features: [
            new ol.Feature({
              id: "new-position",
              geometry: new ol.geom.Point(
                ol.proj.transform([
                  parseFloat(new_longPoint),
                  parseFloat(new_latPoint)
                ], "EPSG:4326", "EPSG:3857")
              )
            })
          ]
        }), style: new ol.style.Style({
          image: new ol.style.Icon({
            anchor: [
              0.5,
              0.99
            ],
            anchorXUnits: "fraction",
            anchorYUnits: "fraction",
            src: "../../static/img/openlayers/spotlight-blue/scale-2.png"
          })
        })
      });

      // Popup
      const popup = new ol.Overlay({
        element: elementPopup,
        positioning: "bottom-center",
        stopEvent: false
      });

      /**
       * Inicializa el mapa
       */
      const map = new ol.Map({
        target: "map",
        overlays: [
          popup
        ],

        layers: [
          new ol.layer.Tile({
            source: new ol.source.OSM()
          }),

          currentPosition,
          newPosition
        ], view: new ol.View({
          center: ol.proj.fromLonLat([
            new_longPoint,
            new_latPoint
          ]),
          zoom: 10,
          maxZoom: 19
        })
      });

      // Cerrar popup
      closePopup.onclick = () => {
        popup.setPosition(undefined);
        closePopup.blur();
        return false;
      };

      // Cada que se haga clic en el mapa
      map.on("singleclick", function (evt) {
        const feature = map.forEachFeatureAtPixel(evt.pixel, (feature, layer) => {
          return feature;
        });

        if (feature) {
          const geometry = feature.getGeometry();
          const coord = geometry.getCoordinates();

          // Define la posición del popup
          popup.setPosition(coord);

          if (feature.values_.id === "new-position") {
            $("#popup #position").html("Nueva posición");
          } else {
            $("#popup #position").html("Posición actual");
          }
        } else {
          // Oculta el popup
          popup.setPosition(undefined);
        }
      });

      let extent = ol.extent.boundingExtent([[current_longPoint, current_latPoint], [new_longPoint,new_latPoint]]);
      extent = ol.proj.transformExtent(extent, ol.proj.get("EPSG:4326"), ol.proj.get("EPSG:3857"));

      map.getView().fit(extent,map.getSize());
    </script>
  </body>
</html>
