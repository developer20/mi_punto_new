<?php
// Parámetros enviados por GET
$longPoint = (isset($_GET["longPoint"])) ? $_GET["longPoint"] : "";
$latPoint = (isset($_GET["latPoint"])) ? $_GET["latPoint"] : "";
$longUser = (isset($_GET["longUser"])) ? $_GET["longUser"] : "";
$latUser = (isset($_GET["latUser"])) ? $_GET["latUser"] : "";
$precisionUser = (isset($_GET["precisionUser"])) ? $_GET["precisionUser"] : "";

// Valida los parámetro
if ($longUser === "0" || $latUser === "0" || $longPoint === "0" || $latPoint === "0") {
  // Muestra mensaje de error
  echo "<div style=\"font-family: Helvetica, Arial, sans-serif; font-size: 1.17em; text-align: center;  position: absolute; left: 50%; top: 50%; transform: translate(-50%, -50%);\">Visita sin Georeferencia</div>";
  
  // Finaliza la ejecución
  exit;
} else if ($longPoint === "" || $latPoint === "" || $longUser === "" || $latUser === "" || $precisionUser === "") {
  // Finaliza la ejecución
  exit;
}
?>
<!doctype html>
<html lang="es">
  <head>
    <meta charset="utf-8" />
    <title>Mapa: usuario - punto</title>
    <link rel="stylesheet" href="../static/css/openlayers/openlayers.css" type="text/css" />
    <script type="text/javascript" src="../static/js/openlayers/openlayers.js"></script>

    <style type="text/css">
      body {
        padding: 0;
        margin: 0;
        overflow: hidden;
        font-family: Helvetica, Arial, sans-serif;
      }

      #map {
        width: 100vw;
        height: 100vh;
      }
    </style>
  </head>

  <body>
    <div id="map"></div>

    <!--suppress JSAnnotator -->
    <script type="text/javascript">
      // Coordenadas del punto
      const longPoint = <?php echo $longPoint; ?>;
      const latPoint = <?php echo $latPoint; ?>;

      // Coordenadas del usuario
      const longUser = <?php echo $longUser; ?>;
      const latUser = <?php echo $latUser; ?>;
      const precision = <?php echo $precisionUser; ?>;

      const newLong = (longPoint + longUser) / 2;
      const newLat = (latPoint + latUser) / 2;

      // Radio de precisión
      const precisionRadio = new ol.layer.Vector({
        source: new ol.source.Vector({
          features: [
            new ol.Feature({
              geometry: new ol.geom.Circle(ol.proj.fromLonLat([
                longUser,
                latUser
              ]), precision)
            })
          ]
        }), style: new ol.style.Style({
          stroke: new ol.style.Stroke({
            color: "rgba(75, 150, 243, 1)",
            width: 2
          }), fill: new ol.style.Fill({
            color: "rgba(75, 150, 243, 0.3)"
          })
        })
      });

      // Punto de venta
      const pointSale = new ol.layer.Vector({
        source: new ol.source.Vector({
          features: [
            new ol.Feature({
              geometry: new ol.geom.Point(
                ol.proj.transform([
                  parseFloat(longPoint),
                  parseFloat(latPoint)
                ], "EPSG:4326", "EPSG:3857")
              )
            })
          ]
        }), style: new ol.style.Style({
          image: new ol.style.Icon({
            anchor: [
              0.5,
              0.99
            ],
            anchorXUnits: "fraction",
            anchorYUnits: "fraction",
            src: "../static/img/openlayers/pin-store.png",
            scale: 0.3
          })
        })
      });

      // Punto del usuario
      const pointUser = new ol.layer.Vector({
        source: new ol.source.Vector({
          features: [
            new ol.Feature({
              geometry: new ol.geom.Point(
                ol.proj.transform([
                  parseFloat(longUser),
                  parseFloat(latUser)
                ], "EPSG:4326", "EPSG:3857")
              )
            })
          ]
        }), style: new ol.style.Style({
          image: new ol.style.Icon({
            anchor: [
              0.5,
              0.99
            ],
            anchorXUnits: "fraction",
            anchorYUnits: "fraction",
            src: "../static/img/openlayers/pin-user.png",
            scale: 0.3
          })
        })
      });

      // Punto medio entre el usuario y el punto
      const pointMedium = new ol.layer.Vector({
        source: new ol.source.Vector({
          features: [
            new ol.Feature({
              geometry: new ol.geom.Point(
                ol.proj.transform([
                  parseFloat(newLong),
                  parseFloat(newLat)
                ], "EPSG:4326", "EPSG:3857")
              )
            })
          ]
        }), style: new ol.style.Style({
          image: new ol.style.Icon({
            anchor: [
              0.5,
              0.9
            ],
            anchorXUnits: "fraction",
            anchorYUnits: "fraction",
            src: "../static/img/openlayers/spotlight/scale-2.png"
          })
        })
      });

      // Obtiene la distancia en kilometros de las coordenadas
      const kilometersDistance = () => {
        // Se ejecutara para calcular el radio de una coordenada
        const radio = (x) => {
          return x * Math.PI / 180;
        };

        // Radio de la tierra en kilometros
        const earthRadio = 6378.137;

        // Obtiene el radio de las coordenadas
        const radioLat = radio(latUser - latPoint);
        const radioLong = radio(longUser - longPoint);

        // Realiza los calculos
        let calcA = Math.sin(radioLat / 2) * Math.sin(radioLat / 2) + Math.cos(radio(latPoint)) * Math.cos(radio(latUser)) * Math.sin(radioLong / 2) * Math.sin(radioLong / 2);
        let calcB = 2 * Math.atan2(Math.sqrt(calcA), Math.sqrt(1 - calcA));
        let calcC = earthRadio * calcB;

        return calcC.toFixed(3);
      };

      /**
       * Inicializa el mapa
       */
      const map = new ol.Map({
        target: "map",
        layers: [
          new ol.layer.Tile({
            source: new ol.source.OSM()
          }),

          precisionRadio,
          pointSale,
          pointUser/*,
           pointMedium*/
        ], view: new ol.View({
          center: ol.proj.fromLonLat([
            newLong,
            newLat
          ]),
          zoom: 10,
          maxZoom: 20
        })
      });

      let extent = ol.extent.boundingExtent([[longPoint, latPoint], [longUser, latUser]]);
          extent = ol.proj.transformExtent(extent, ol.proj.get("EPSG:4326"), ol.proj.get("EPSG:3857"));

      map.getView().fit(extent,map.getSize());
    </script>
  </body>
</html>
